﻿namespace Hausdorff2
{
    internal class HausdorffPoint
    {
        public int X { get; set; }
        public int Y { get; set; }
    }
}
