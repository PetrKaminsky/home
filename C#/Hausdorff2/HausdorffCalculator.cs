﻿namespace Hausdorff2
{
    internal class HausdorffCalculator
    {
        public static IEnumerable<HausdorffPoint> GeneratePoints(int iterationCount)
        {
            var result = new List<HausdorffPoint>();
            var baseNumber = (int)Math.Pow(3, iterationCount - 1);
            result.AddRange(GeneratePoints(
                iterationCount,
                baseNumber,
                new HausdorffPoint { X = 0, Y = 0 },
                new HausdorffPoint { X = baseNumber, Y = baseNumber }));
            result.AddRange(GeneratePoints(
                iterationCount,
                baseNumber,
                new HausdorffPoint { X = baseNumber, Y = baseNumber },
                new HausdorffPoint { X = 2 * baseNumber, Y = 0 }));
            result.AddRange(GeneratePoints(
                iterationCount,
                baseNumber,
                new HausdorffPoint { X = 2 * baseNumber, Y = 0 },
                new HausdorffPoint { X = baseNumber, Y = -baseNumber }));
            result.AddRange(GeneratePoints(
                iterationCount,
                baseNumber,
                new HausdorffPoint { X = baseNumber, Y = -baseNumber },
                new HausdorffPoint { X = 0, Y = 0 }));

            return result;
        }

        private static IEnumerable<HausdorffPoint> GeneratePoints(
            int iterationCount,
            int baseNumber,
            HausdorffPoint fromPoint,
            HausdorffPoint toPoint)
        {
            if (iterationCount <= 1)
            {
                return new List<HausdorffPoint>() { fromPoint };
            }

            var result = new List<HausdorffPoint>();
            var rightDirectionCoefficient = fromPoint.X < toPoint.X ? 1 : -1;
            var upDirectionCoefficient = fromPoint.Y < toPoint.Y ? 1 : -1;
            var newIterationCount = iterationCount - 1;
            var newBaseNumber = baseNumber / 3;

            // ... right/up
            var oldX = fromPoint.X;
            var oldY = fromPoint.Y;
            var newX = oldX + rightDirectionCoefficient * newBaseNumber;
            var newY = oldY + upDirectionCoefficient * newBaseNumber;
            result.AddRange(GeneratePoints(
                newIterationCount,
                newBaseNumber,
                new HausdorffPoint { X = oldX, Y = oldY },
                new HausdorffPoint { X = newX, Y = newY }));

            // ... left/up
            oldX = newX;
            oldY = newY;
            newX = oldX - rightDirectionCoefficient * newBaseNumber;
            newY = oldY + upDirectionCoefficient * newBaseNumber;
            result.AddRange(GeneratePoints(
                newIterationCount,
                newBaseNumber,
                new HausdorffPoint { X = oldX, Y = oldY },
                new HausdorffPoint { X = newX, Y = newY }));

            // ... right/up
            oldX = newX;
            oldY = newY;
            newX = oldX + rightDirectionCoefficient * newBaseNumber;
            newY = oldY + upDirectionCoefficient * newBaseNumber;
            result.AddRange(GeneratePoints(
                newIterationCount,
                newBaseNumber,
                new HausdorffPoint { X = oldX, Y = oldY },
                new HausdorffPoint { X = newX, Y = newY }));

            // ... right/down
            oldX = newX;
            oldY = newY;
            newX = oldX + rightDirectionCoefficient * newBaseNumber;
            newY = oldY - upDirectionCoefficient * newBaseNumber;
            result.AddRange(GeneratePoints(
                newIterationCount,
                newBaseNumber,
                new HausdorffPoint { X = oldX, Y = oldY },
                new HausdorffPoint { X = newX, Y = newY }));

            // ... left/down
            oldX = newX;
            oldY = newY;
            newX = oldX - rightDirectionCoefficient * newBaseNumber;
            newY = oldY - upDirectionCoefficient * newBaseNumber;
            result.AddRange(GeneratePoints(
                newIterationCount,
                newBaseNumber,
                new HausdorffPoint { X = oldX, Y = oldY },
                new HausdorffPoint { X = newX, Y = newY }));

            // ... right/down
            oldX = newX;
            oldY = newY;
            newX = oldX + rightDirectionCoefficient * newBaseNumber;
            newY = oldY - upDirectionCoefficient * newBaseNumber;
            result.AddRange(GeneratePoints(
                newIterationCount,
                newBaseNumber,
                new HausdorffPoint { X = oldX, Y = oldY },
                new HausdorffPoint { X = newX, Y = newY }));

            // ... right/up
            oldX = newX;
            oldY = newY;
            newX = oldX + rightDirectionCoefficient * newBaseNumber;
            newY = oldY + upDirectionCoefficient * newBaseNumber;
            result.AddRange(GeneratePoints(
                newIterationCount,
                newBaseNumber,
                new HausdorffPoint { X = oldX, Y = oldY },
                new HausdorffPoint { X = newX, Y = newY }));

            // ... left/up
            oldX = newX;
            oldY = newY;
            newX = oldX - rightDirectionCoefficient * newBaseNumber;
            newY = oldY + upDirectionCoefficient * newBaseNumber;
            result.AddRange(GeneratePoints(
                newIterationCount,
                newBaseNumber,
                new HausdorffPoint { X = oldX, Y = oldY },
                new HausdorffPoint { X = newX, Y = newY }));

            // ... right/up
            oldX = newX;
            oldY = newY;
            newX = oldX + rightDirectionCoefficient * newBaseNumber;
            newY = oldY + upDirectionCoefficient * newBaseNumber;
            result.AddRange(GeneratePoints(
                newIterationCount,
                newBaseNumber,
                new HausdorffPoint { X = oldX, Y = oldY },
                new HausdorffPoint { X = newX, Y = newY }));

            return result;
        }
    }
}
