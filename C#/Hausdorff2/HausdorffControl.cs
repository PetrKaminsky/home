﻿namespace Hausdorff2
{
    internal class HausdorffControl : Panel
    {
        public void Redraw(int iterationCount, IEnumerable<Point> logicalPoints)
        {
            var graphics = CreateGraphics();
            // erase
            graphics.Clear(Color.White);
            graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            var dimension = Math.Min(Width, Height);
            var frameSize = dimension / 24;
            var pen = new Pen(Color.Black);
            pen.StartCap = pen.EndCap = System.Drawing.Drawing2D.LineCap.Round;
            pen.Width = dimension / (int)Math.Pow(3, iterationCount) / 2;

            // get limits
            var minPointX = logicalPoints.Min(point => point.X);
            var minPointY = logicalPoints.Min(point => point.Y);
            var maxPointX = logicalPoints.Max(point => point.X);
            var maxPointY = logicalPoints.Max(point => point.Y);
            var minControlX = Width / 2 - dimension / 2 + frameSize;
            var minControlY = Height / 2 - dimension / 2 + frameSize;
            var maxControlX = Width / 2 + dimension / 2 - frameSize;
            var maxControlY = Height / 2 + dimension / 2 - frameSize;

            // transform points
            var controlPoints = logicalPoints.Select(point =>
            {
                var x = (point.X - minPointX) * (maxControlX - minControlX) / (maxPointX - minPointX) + minControlX;
                var y = (point.Y - minPointY) * (maxControlY - minControlY) / (maxPointY - minPointY) + minControlY;
                return new Point(x, y);
            }).ToList();
            var pointCount = controlPoints.Count;

            // draw fragments
            for (var i = 0; i < pointCount; i++)
            {
                DrawFragment(
                    graphics,
                    pen,
                    controlPoints,
                    pointCount,
                    i);
            }
        }

        private static void DrawFragment(
            Graphics graphics,
            Pen pen,
            IList<Point> points,
            int pointCount,
            int startIndex)
        {
            //var pointFrom = points[startIndex % pointCount];
            //var pointTo = points[(startIndex + 1) % pointCount];
            //graphics.DrawLine(pen, pointFrom, pointTo);
            var point1 = points[startIndex % pointCount];
            var point2 = points[(startIndex + 1) % pointCount];
            var point3 = points[(startIndex + 2) % pointCount];
            var upLeftArc = point1.X == point3.X;
            var topBotomArc = point1.Y == point3.Y;
            var startAngle = 0F;
            if (upLeftArc)
            {
                if (point2.X > point1.X)
                {
                    startAngle = 315F;
                }
                else
                {
                    startAngle = 135F;
                }
            }
            else if (topBotomArc)
            {
                if (point2.Y > point1.Y)
                {
                    startAngle = 45F;
                }
                else
                {
                    startAngle = 225F;
                }
            }
            var sweepAngle = 90F;
            var radius = 0F;
            if (upLeftArc)
            {
                // TODO
            }
            else if (topBotomArc)
            {
                // TODO
            }
            // TODO - rectangle
            // TODO - draw arc
        }
    }
}
