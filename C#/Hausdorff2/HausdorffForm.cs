namespace Hausdorff2
{
    public partial class HausdorffForm : Form
    {
        public HausdorffForm()
        {
            InitializeComponent();

            comboBoxIterations.DataSource = IterationDataModel.Items;
            comboBoxIterations.SelectedValueChanged += ComboBoxIterationsSelectedValueChanged;
            buttonRedraw.Click += ButtonRedrawClick;
            hausdorffControl.Resize += HausdorffControlResize;
        }

        private void HausdorffControlResize(object? sender, EventArgs e)
        {
            RedrawHausdorffControl();
        }

        private void ButtonRedrawClick(object? sender, EventArgs e)
        {
            RedrawHausdorffControl();
        }

        private void ComboBoxIterationsSelectedValueChanged(object? sender, EventArgs e)
        {
            RedrawHausdorffControl();
        }

        private void RedrawHausdorffControl()
        {
            var iterationCount = (int)comboBoxIterations.SelectedItem;
            var hausdorffPoints = HausdorffCalculator.GeneratePoints(iterationCount);
            var logicalPoints = hausdorffPoints.ToPoints();
            hausdorffControl.Redraw(iterationCount, logicalPoints);
        }
    }

    internal static class PointExtensions
    {
        internal static IEnumerable<Point> ToPoints(this IEnumerable<HausdorffPoint> points) =>
            points.Select(p => new Point(p.X, p.Y));
    }
}
