﻿namespace Hausdorff2
{
    partial class HausdorffForm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            splitContainer = new SplitContainer();
            comboBoxIterations = new ComboBox();
            buttonRedraw = new Button();
            hausdorffControl = new HausdorffControl();
            ((System.ComponentModel.ISupportInitialize)splitContainer).BeginInit();
            splitContainer.Panel1.SuspendLayout();
            splitContainer.Panel2.SuspendLayout();
            splitContainer.SuspendLayout();
            SuspendLayout();
            // 
            // splitContainer
            // 
            splitContainer.Dock = DockStyle.Fill;
            splitContainer.Location = new Point(0, 0);
            splitContainer.Name = "splitContainer";
            splitContainer.Orientation = Orientation.Horizontal;
            // 
            // splitContainer.Panel1
            // 
            splitContainer.Panel1.Controls.Add(buttonRedraw);
            splitContainer.Panel1.Controls.Add(comboBoxIterations);
            // 
            // splitContainer.Panel2
            // 
            splitContainer.Panel2.Controls.Add(hausdorffControl);
            splitContainer.Size = new Size(800, 450);
            splitContainer.SplitterDistance = 36;
            splitContainer.TabIndex = 0;
            // 
            // comboBoxIterations
            // 
            comboBoxIterations.FormattingEnabled = true;
            comboBoxIterations.Location = new Point(3, 3);
            comboBoxIterations.Name = "comboBoxIterations";
            comboBoxIterations.Size = new Size(166, 23);
            comboBoxIterations.TabIndex = 0;
            // 
            // buttonRedraw
            // 
            buttonRedraw.Location = new Point(175, 3);
            buttonRedraw.Name = "buttonRedraw";
            buttonRedraw.Size = new Size(152, 23);
            buttonRedraw.TabIndex = 1;
            buttonRedraw.Text = "Redraw";
            buttonRedraw.UseVisualStyleBackColor = true;
            // 
            // hausdorffControl
            // 
            hausdorffControl.BackColor = Color.White;
            hausdorffControl.Dock = DockStyle.Fill;
            hausdorffControl.Location = new Point(0, 0);
            hausdorffControl.Name = "hausdorffControl";
            hausdorffControl.Size = new Size(800, 410);
            hausdorffControl.TabIndex = 0;
            // 
            // Form1
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(800, 450);
            Controls.Add(splitContainer);
            Name = "Form1";
            Text = "Form1";
            splitContainer.Panel1.ResumeLayout(false);
            splitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)splitContainer).EndInit();
            splitContainer.ResumeLayout(false);
            ResumeLayout(false);
        }

        #endregion

        private SplitContainer splitContainer;
        private Button buttonRedraw;
        private ComboBox comboBoxIterations;
        private HausdorffControl hausdorffControl;
    }
}