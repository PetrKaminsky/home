﻿using System;
using System.Windows.Forms;

namespace XmlRemoteTest
{
    static class Program
    {
        public const string TRAN_SWITCH = "-tran";
        public const string BT_SWITCH = "-bt";
        public const string CNT_SWITCH = "-cnt";

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [MTAThread]
        static void Main(string[] args)
        {
            if (args.Length > 0)
            {
                try
                {
                    int port = int.Parse(args[1]);
                    Config.SerializationType = (SerializationType)Enum.Parse(typeof(SerializationType), args[2]);
                    switch (args[0])
                    {
                        case TRAN_SWITCH:
                            TranRemoteWorker.Run(port);
                            break;

                        case BT_SWITCH:
                            BtRemoteWorker.Run(port);
                            break;

                        case CNT_SWITCH:
                            CntRemoteWorker.Run(port);
                            break;
                    }
                }
                catch { }

                return;
            }

            Config.WorkerType = WorkerType.REMOTE;
            Config.SerializationType = SerializationType.JSON;

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new XmlRemoteTestForm());

            TranWorkerInstance.Release();
            BtWorkerInstance.Release();
            CntWorkerInstance.Release();
        }
    }
}
