﻿using System;
using System.Windows.Forms;
using System.Collections.Generic;

namespace XmlRemoteTest
{
    public partial class XmlRemoteTestForm : Form
    {
        public XmlRemoteTestForm()
        {
            InitializeComponent();
        }

        private void buttonCreateTransaction_Click(object sender, EventArgs e)
        {
            try
            {
                using (TranWorkerInstance wi = new TranWorkerInstance())
                {
                    ITranWorker w = TranWorkerInstance.Get();
                    w.CreateTransaction();
                }
            }
            catch (Exception ex)
            {
                showException(ex);
            }
        }

        private void buttonSetTransactionAmount_Click(object sender, EventArgs e)
        {
            try
            {
                using (TranWorkerInstance wi = new TranWorkerInstance())
                {
                    ITranWorker w = TranWorkerInstance.Get();
                    w.SetTransactionAmount(100m);
                }
            }
            catch (Exception ex)
            {
                showException(ex);
            }
        }

        private void buttonDoTransaction_Click(object sender, EventArgs e)
        {
            try
            {
                using (TranWorkerInstance wi = new TranWorkerInstance())
                {
                    ITranWorker w = TranWorkerInstance.Get();
                    bool retVal = w.DoTransaction();
                }
            }
            catch (Exception ex)
            {
                showException(ex);
            }
        }

        private static void showException(Exception ex)
        {
            MessageBox.Show(ex.ToString(), ex.GetType().Name);
        }

        private void buttonRefreshDevices_Click(object sender, EventArgs e)
        {
            try
            {
                using (BtWorkerInstance wi = new BtWorkerInstance())
                {
                    IBtWorker w = BtWorkerInstance.Get();
                    w.RefreshDevices();
                }
            }
            catch (Exception ex)
            {
                showException(ex);
            }
        }

        private void buttonPairDevice_Click(object sender, EventArgs e)
        {
            try
            {
                using (BtWorkerInstance wi = new BtWorkerInstance())
                {
                    IBtWorker w = BtWorkerInstance.Get();
                    w.PairDevice("001122334455", "0000");
                }
            }
            catch (Exception ex)
            {
                showException(ex);
            }
        }

        private void buttonGetDevices_Click(object sender, EventArgs e)
        {
            try
            {
                using (BtWorkerInstance wi = new BtWorkerInstance())
                {
                    IBtWorker w = BtWorkerInstance.Get();
                    string[] retVal = w.GetDevices();
                }
            }
            catch (Exception ex)
            {
                showException(ex);
            }
        }

        private void buttonStartCounter_Click(object sender, EventArgs e)
        {
            try
            {
                using (CntWorkerInstance wi = new CntWorkerInstance())
                {
                    ICntWorker w = CntWorkerInstance.Get();
                    w.Start();
                }
            }
            catch (Exception ex)
            {
                showException(ex);
            }
        }

        private void buttonStopCounter_Click(object sender, EventArgs e)
        {
            try
            {
                using (CntWorkerInstance wi = new CntWorkerInstance())
                {
                    ICntWorker w = CntWorkerInstance.Get();
                    w.Stop();
                }
            }
            catch (Exception ex)
            {
                showException(ex);
            }
        }
    }
}
