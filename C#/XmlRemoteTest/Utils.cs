﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Diagnostics;
using System.IO;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using System.Text;
using System.Reflection;
using System.Threading;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;

namespace XmlRemoteTest
{
    #region Worker

    public abstract class Worker : IDisposable
    {
        protected virtual void Init() { }
        protected virtual void Cleanup() { }

        protected Worker()
        {
            this.Init();
        }

        public void Dispose()
        {
            this.Cleanup();
        }
    }

    #endregion

    #region RemoteProxy

    public abstract class RemoteProxy : Worker
    {
        private TcpClient client = null;
        private Process wrkProcess = null;
        private RemoteSerializer serializer = null;
        private AbstractParser parser = null;
        private Thread threadSocketRead = null;

        private static void copyFiles(string fromDir, string fileMask, string toDir)
        {
            fromDir = fromDir.Replace("file:\\", string.Empty);
            DirectoryInfo di = new DirectoryInfo(fromDir);
            Utils.EnsureDir(toDir);
            foreach (FileInfo fiSrc in di.GetFiles(fileMask))
            {
                FileInfo fiDst = new FileInfo(Path.Combine(toDir, fiSrc.Name));
                if (fiDst.Exists)
                {
                    // clear read-only attribute
                    if ((fiDst.Attributes & FileAttributes.ReadOnly) == FileAttributes.ReadOnly)
                        fiDst.Attributes &= ~FileAttributes.ReadOnly;
                    // delete
                    fiDst.Delete();
                }
                fiSrc.CopyTo(fiDst.FullName);
            }
        }

        protected override void Init()
        {
            // copy EXE & DLLs to TEMP
            string srcFullExeName = Utils.ExeFileName;
            string fromDir = Path.GetDirectoryName(srcFullExeName);
            string toDir = this.DestDir;
            string srcExeName = Path.GetFileName(srcFullExeName);
            copyFiles(fromDir, srcExeName, toDir);
            copyFiles(fromDir, "*.dll", toDir);
            string dstFullExeName = Path.Combine(toDir, srcExeName);
            // create server socket
            TcpListener svr = new TcpListener(IPAddress.Loopback, 0);
            svr.Start();
            int localPort = ((IPEndPoint)svr.LocalEndpoint).Port;
            // run worker
            this.wrkProcess = new Process();
            this.wrkProcess.StartInfo.FileName = dstFullExeName;
            this.wrkProcess.StartInfo.Arguments = this.CmdSwitch + " " +
                localPort.ToString() + " " +
                Config.SerializationType;
            this.wrkProcess.Start();
            // accept client
            this.client = svr.AcceptTcpClient();
            // stop server socket
            svr.Stop();
            // create parser
            this.parser = AbstractParser.Create(Config.SerializationType);
            // create read thread
            this.threadSocketRead = new Thread(new ThreadStart(this.socketReadThreadProc));
            this.threadSocketRead.Start();
        }

        protected virtual string DestDir
        { get { return "\\Temp"; } }

        protected abstract string CmdSwitch { get; }

        protected override void Cleanup()
        {
            if (this.client.Client.Connected)
                this.client.GetStream().Close();
            this.client.Close();
            if (!this.wrkProcess.WaitForExit(1000))
                this.wrkProcess.Kill();
            this.wrkProcess.Close();
            this.threadSocketRead.Abort();
        }

        protected RemoteResponse remoteCall(RemoteRequest req)
        {
            RemoteResponse resp = this.remoteCallInternal(req);
            if (!string.IsNullOrEmpty(resp.ErrorText))
                throw new Exception(resp.ErrorText);
            return resp;
        }

        protected RemoteSerializer Serializer
        {
            get
            {
                if (this.serializer == null)
                    this.serializer = this.CreateSerializer();
                return this.serializer;
            }
        }

        protected abstract RemoteSerializer CreateSerializer();

        private RemoteResponse remoteCallInternal(RemoteRequest req)
        {
            // serialize request
            byte[] xmlReq = this.Serializer.SerializeRequest(req);

            Debug.WriteLine("remoteCallInternal(), request: " + Encoding.UTF8.GetString(xmlReq));

            byte[] xmlResp = this.getResponse(xmlReq);

            Debug.WriteLine("remoteCallInternal(), response: " + Encoding.UTF8.GetString(xmlResp));

            // deserialize response
            return this.Serializer.DeserializeResponse(xmlResp);
        }

        private byte[] getResponse(byte[] xmlReq)
        {
            // send request
            this.writeRequest(xmlReq);
            // read response
            return this.readResponse();
        }

        private void writeRequest(byte[] xmlReq)
        {
            NetworkStream s = this.client.GetStream();
            s.Write(xmlReq, 0, xmlReq.Length);
            s.Flush();
        }

        private byte[] readResponse()
        {
            List<byte> resp = new List<byte>();
            ManualResetEvent waitEvent = new ManualResetEvent(false);
            AbstractParser.ObjectFoundDelegate xmlFoundHandler = new AbstractParser.ObjectFoundDelegate(delegate(byte[] bytes)
            {
                resp.AddRange(bytes);
                waitEvent.Set();
            });
            this.parser.OnObjectFound += xmlFoundHandler;
            waitEvent.WaitOne();
            this.parser.OnObjectFound -= xmlFoundHandler;
            return resp.ToArray();
        }

        private void socketReadThreadProc()
        {
            try
            {
                NetworkStream s = this.client.GetStream();
                byte[] buff = new byte[16];
                while (true)
                {
                    int bytesRead = s.Read(buff, 0, buff.Length);
                    if (bytesRead > 0)
                    {
                        this.parser.AddBytes(Utils.GetSubArray(buff, 0, bytesRead));
                    }
                }
            }
            catch { }
        }
    }

    #endregion

    #region RemoteWorker

    public abstract class RemoteWorker
    {
        private int svrPort = -1;
        private TcpClient client = null;
        private RemoteSerializer serializer = null;
        private AbstractParser parser = null;

        public RemoteWorker(int port)
        {
            this.svrPort = port;
            this.client = new TcpClient();
            this.parser = AbstractParser.Create(Config.SerializationType);
            this.parser.OnObjectFound += new AbstractParser.ObjectFoundDelegate(this.parser_OnXmlFound);
        }

        private void parser_OnXmlFound(byte[] bytes)
        {
            NetworkStream s = this.client.GetStream();

            Debug.WriteLine("processRequests(), request: " + Encoding.UTF8.GetString(bytes));

            // complete request is in req
            byte[] xmlResp = this.getResponse(bytes);

            Debug.WriteLine("processRequests(), response: " + Encoding.UTF8.GetString(xmlResp));

            // send response back
            s.Write(xmlResp, 0, xmlResp.Length);
            s.Flush();
        }

        protected void run()
        {
            this.connectAndProcessRequests();
            this.disconnect();
        }

        private void connectAndProcessRequests()
        {
            try
            {
                this.client.Connect(IPAddress.Loopback, this.svrPort);
                this.processRequests();
            }
            catch { }
        }

        private void disconnect()
        {
            try
            {
                if (this.client.Client.Connected)
                    this.client.GetStream().Close();
                this.client.Close();
            }
            catch { }
        }

        private void processRequests()
        {
            NetworkStream s = this.client.GetStream();
            byte[] buff = new byte[16];
            while (true)
            {
                int bytesRead = s.Read(buff, 0, buff.Length);
                if (bytesRead == 0)
                    return;
                else if (bytesRead > 0)
                    this.parser.AddBytes(Utils.GetSubArray(buff, 0, bytesRead));
            }
        }

        private RemoteSerializer Serializer
        {
            get
            {
                if (this.serializer == null)
                    this.serializer = this.CreateSerializer();
                return this.serializer;
            }
        }

        protected abstract RemoteSerializer CreateSerializer();

        private byte[] getResponse(byte[] xmlReq)
        {
            RemoteRequest req = this.Serializer.DeserializeRequest(xmlReq);
            RemoteResponse resp = this.call(req);
            return this.Serializer.SerializeResponse(resp);
        }

        private RemoteResponse call(RemoteRequest req)
        {
            return req.Process(this);
        }
    }

    #endregion

    #region RemoteSerializer

    public abstract class RemoteSerializer
    {
        private Type requestType;
        private Type responseType;
        private AbstractSerializer serializer = AbstractSerializer.Create(Config.SerializationType);

        public RemoteSerializer(Type reqType, Type respType)
        {
            this.requestType = reqType;
            this.responseType = respType;
        }

        public byte[] SerializeRequest(RemoteRequest req)
        {
            return serialize(req, this.requestType);
        }

        public byte[] SerializeResponse(RemoteResponse resp)
        {
            return serialize(resp, this.responseType);
        }

        private byte[] serialize(object o, Type t)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                this.serializer.serialize(ms, o, t);
                return ms.ToArray();
            }
        }

        public RemoteRequest DeserializeRequest(byte[] xmlReq)
        {
            return (RemoteRequest)deserialize(xmlReq, this.requestType);
        }

        public RemoteResponse DeserializeResponse(byte[] xmlResp)
        {
            return (RemoteResponse)deserialize(xmlResp, this.responseType);
        }

        public object deserialize(byte[] xml, Type t)
        {
            using (MemoryStream ms = new MemoryStream(xml))
            { return this.serializer.deserialize(ms, t); }
        }

        abstract class AbstractSerializer
        {
            internal static AbstractSerializer Create(SerializationType serializationType)
            {
                if (serializationType == SerializationType.XML)
                    return new XmlImplementation();
                return new JsonImplementation();
            }

            internal abstract object deserialize(Stream s, Type t);
            internal abstract void serialize(Stream s, object o, Type t);

            class XmlImplementation : AbstractSerializer
            {
                internal override object deserialize(Stream s, Type t)
                {
                    XmlSerializer sr = new XmlSerializer(t);
                    return sr.Deserialize(s);
                }

                internal override void serialize(Stream s, object o, Type t)
                {
                    XmlSerializer sr = new XmlSerializer(t);
                    sr.Serialize(s, o);
                }
            }

            class JsonImplementation : AbstractSerializer
            {
                internal override object deserialize(Stream s, Type t)
                {
                    DataContractJsonSerializer sr = new DataContractJsonSerializer(t);
                    return sr.ReadObject(s);
                }

                internal override void serialize(Stream s, object o, Type t)
                {
                    DataContractJsonSerializer sr = new DataContractJsonSerializer(t);
                    sr.WriteObject(s, o);
                }
            }
        }
    }

    #endregion

    #region RemoteRequest

    [KnownType(typeof(BtRemoteRequest.RefreshDevices))]
    [KnownType(typeof(BtRemoteRequest.PairDevice))]
    [KnownType(typeof(BtRemoteRequest.GetDevices))]
    [KnownType(typeof(CntRemoteRequest.Start))]
    [KnownType(typeof(CntRemoteRequest.Stop))]
    [KnownType(typeof(TranRemoteRequest.CreateTransaction))]
    [KnownType(typeof(TranRemoteRequest.SetTransactionAmount))]
    [KnownType(typeof(TranRemoteRequest.DoTransaction))]
    public abstract class RemoteRequest
    {
        public RemoteResponse Process(RemoteWorker worker)
        {
            RemoteResponse resp = this.CreateResponse();
            try
            {
                this.ProcessInternal(worker, resp);
            }
            catch (Exception ex)
            {
                resp.Error(ex);
            }
            return resp;
        }

        protected abstract RemoteResponse CreateResponse();
        protected abstract void ProcessInternal(
            RemoteWorker worker,
            RemoteResponse resp);
    }

    #endregion

    #region RemoteResponse

    [KnownType(typeof(BtRemoteResponse.RefreshDevices))]
    [KnownType(typeof(BtRemoteResponse.PairDevice))]
    [KnownType(typeof(BtRemoteResponse.GetDevices))]
    [KnownType(typeof(CntRemoteResponse.Start))]
    [KnownType(typeof(CntRemoteResponse.Stop))]
    [KnownType(typeof(TranRemoteResponse.CreateTransaction))]
    [KnownType(typeof(TranRemoteResponse.SetTransactionAmount))]
    [KnownType(typeof(TranRemoteResponse.DoTransaction))]
    public abstract class RemoteResponse
    {
        public string ErrorText;

        public void Error(Exception ex)
        {
            this.ErrorText = ex.GetType().Name + ", " + ex.Message;
        }
    }

    #endregion

    #region ITranWorker

    public interface ITranWorker
    {
        void CreateTransaction();
        void SetTransactionAmount(decimal tranAmount);
        bool DoTransaction();
    }

    #endregion

    #region TranWorker

    public abstract class TranWorker : Worker
    {
        public static ITranWorker Create(WorkerType workerType)
        {
            if (workerType == WorkerType.REMOTE)
                return new TranRemoteProxy();
            return new TranLocalWorker();
        }

        #region TranRemoteProxy

        class TranRemoteProxy : RemoteProxy, ITranWorker
        {
            protected override string DestDir
            { get { return Path.Combine(base.DestDir, "Tran"); } }

            protected override RemoteSerializer CreateSerializer()
            {
                return new TranRemoteSerializer();
            }

            protected override string CmdSwitch
            { get { return Program.TRAN_SWITCH; } }

            #region ITranWorker implementation

            public void CreateTransaction()
            {
                TranRemoteRequest.CreateTransaction req = new TranRemoteRequest.CreateTransaction();
                TranRemoteResponse.CreateTransaction resp = (TranRemoteResponse.CreateTransaction)this.remoteCall(req);
            }

            public void SetTransactionAmount(decimal tranAmount)
            {
                TranRemoteRequest.SetTransactionAmount req = new TranRemoteRequest.SetTransactionAmount();
                req.TranAmount = tranAmount;
                TranRemoteResponse.SetTransactionAmount resp = (TranRemoteResponse.SetTransactionAmount)this.remoteCall(req);
            }

            public bool DoTransaction()
            {
                TranRemoteRequest.DoTransaction req = new TranRemoteRequest.DoTransaction();
                TranRemoteResponse.DoTransaction resp = (TranRemoteResponse.DoTransaction)this.remoteCall(req);
                return resp.Result;
            }

            #endregion
        }

        #endregion

        #region TranLocalWorker

        class TranLocalWorker : Worker, ITranWorker
        {
            #region ITranWorker implementation

            public void CreateTransaction()
            {
                Debug.WriteLine("LocalWorker.CreateTransaction()");
            }

            public void SetTransactionAmount(decimal tranAmount)
            {
                Debug.WriteLine("LocalWorker.SetTransactionAmount(), tranAmount: " + tranAmount);
            }

            public bool DoTransaction()
            {
                bool retVal = new Random().Next(2) == 1;

                Debug.WriteLine("LocalWorker.DoTransaction(), retVal: " + retVal);

                return retVal;
            }

            #endregion
        }

        #endregion
    }

    #endregion

    #region TranRemoteWorker

    public class TranRemoteWorker : RemoteWorker, ITranWorker, IDisposable
    {
        private ITranWorker worker = null;

        public TranRemoteWorker(int port)
            : base(port)
        {
            this.worker = TranWorker.Create(WorkerType.LOCAL);
        }

        public void Dispose()
        {
            if (this.worker != null)
            {
                ((TranWorker)this.worker).Dispose();
                this.worker = null;
            }
        }

        public static void Run(int port)
        {
            using (TranRemoteWorker w = new TranRemoteWorker(port))
            { w.run(); }
        }

        protected override RemoteSerializer CreateSerializer()
        {
            return new TranRemoteSerializer();
        }

        #region ITranWorker implementation

        public void CreateTransaction()
        {
            this.worker.CreateTransaction();
        }

        public void SetTransactionAmount(decimal tranAmount)
        {
            this.worker.SetTransactionAmount(tranAmount);
        }

        public bool DoTransaction()
        {
            return this.worker.DoTransaction();
        }

        #endregion
    }

    #endregion

    #region TranWorkerInstance

    public class TranWorkerInstance : IDisposable
    {
        private static ITranWorker instance = null;

        private static ITranWorker ensureInstance()
        {
            if (instance == null)
                instance = TranWorker.Create(Config.WorkerType);
            return instance;
        }

        public static ITranWorker Get()
        {
            return TranWorkerInstance.ensureInstance();
        }

        public static void Release()
        {
            if (instance != null)
            {
                ((Worker)instance).Dispose();
                instance = null;
            }
        }

        public void Dispose() { }
    }

    #endregion

    #region TranRemoteSerializer

    public class TranRemoteSerializer : RemoteSerializer
    {
        public TranRemoteSerializer() : base(typeof(TranRemoteRequest), typeof(TranRemoteResponse)) { }
    }

    #endregion

    #region TranRemoteRequest

    [XmlInclude(typeof(TranRemoteRequest.CreateTransaction))]
    [XmlInclude(typeof(TranRemoteRequest.SetTransactionAmount))]
    [XmlInclude(typeof(TranRemoteRequest.DoTransaction))]
    public abstract class TranRemoteRequest : RemoteRequest
    {
        #region CreateTransaction

        public class CreateTransaction : TranRemoteRequest
        {
            protected override RemoteResponse CreateResponse()
            {
                return new TranRemoteResponse.CreateTransaction();
            }

            protected override void ProcessInternal(
                RemoteWorker worker,
                RemoteResponse resp)
            {
                ((TranRemoteWorker)worker).CreateTransaction();
            }
        }

        #endregion

        #region SetTransactionAmount

        public class SetTransactionAmount : TranRemoteRequest
        {
            public decimal TranAmount;

            protected override RemoteResponse CreateResponse()
            {
                return new TranRemoteResponse.SetTransactionAmount();
            }

            protected override void ProcessInternal(
                RemoteWorker worker,
                RemoteResponse resp)
            {
                ((TranRemoteWorker)worker).SetTransactionAmount(this.TranAmount);
            }
        }

        #endregion

        #region DoTransaction

        public class DoTransaction : TranRemoteRequest
        {
            protected override RemoteResponse CreateResponse()
            {
                return new TranRemoteResponse.DoTransaction();
            }

            protected override void ProcessInternal(
                RemoteWorker worker,
                RemoteResponse resp)
            {
                TranRemoteResponse.DoTransaction r = (TranRemoteResponse.DoTransaction)resp;
                r.Result = ((TranRemoteWorker)worker).DoTransaction();
            }
        }

        #endregion
    }

    #endregion

    #region TranRemoteResponse

    [XmlInclude(typeof(TranRemoteResponse.CreateTransaction))]
    [XmlInclude(typeof(TranRemoteResponse.SetTransactionAmount))]
    [XmlInclude(typeof(TranRemoteResponse.DoTransaction))]
    public abstract class TranRemoteResponse : RemoteResponse
    {
        #region CreateTransaction

        public class CreateTransaction : TranRemoteResponse
        {
        }

        #endregion

        #region SetTransactionAmount

        public class SetTransactionAmount : TranRemoteResponse
        {
        }

        #endregion

        #region DoTransaction

        public class DoTransaction : TranRemoteResponse
        {
            public bool Result;
        }

        #endregion
    }

    #endregion

    #region Config

    public enum WorkerType
    {
        LOCAL,
        REMOTE
    }

    public enum SerializationType
    {
        XML,
        JSON
    }

    public static class Config
    {
        public static WorkerType WorkerType { get; set; }
        public static SerializationType SerializationType { get; set; }
    }

    #endregion

    #region Utils

    public class Utils
    {
        private Utils() { }

        public static string ExeFileName
        { get { return Assembly.GetExecutingAssembly().GetName().CodeBase; } }

        public static byte[] GetSubArray(byte[] array, int startIndex, int count)
        {
            byte[] retVal = new byte[count];
            Array.Copy(array, startIndex, retVal, 0, count);
            return retVal;
        }

        public static void EnsureDir(string dir)
        {
            if (!Directory.Exists(dir))
                Directory.CreateDirectory(dir);
        }
    }

    #endregion

    #region IBtWorker

    public interface IBtWorker
    {
        void RefreshDevices();
        void PairDevice(string address, string pin);
        string[] GetDevices();
    }

    #endregion

    #region BtWorker

    public abstract class BtWorker : Worker
    {
        public static IBtWorker Create(WorkerType workerType)
        {
            if (workerType == WorkerType.REMOTE)
                return new BtRemoteProxy();
            return new BtLocalWorker();
        }

        #region BtRemoteProxy

        class BtRemoteProxy : RemoteProxy, IBtWorker
        {
            protected override string CmdSwitch
            { get { return Program.BT_SWITCH; } }

            protected override RemoteSerializer CreateSerializer()
            {
                return new BtRemoteSerializer();
            }

            protected override string DestDir
            { get { return Path.Combine(base.DestDir, "Bt"); } }

            #region IBtWorker implementation

            public void RefreshDevices()
            {
                BtRemoteRequest.RefreshDevices req = new BtRemoteRequest.RefreshDevices();
                BtRemoteResponse.RefreshDevices resp = (BtRemoteResponse.RefreshDevices)this.remoteCall(req);
            }

            public void PairDevice(string address, string pin)
            {
                BtRemoteRequest.PairDevice req = new BtRemoteRequest.PairDevice();
                req.Address = address;
                req.Pin = pin;
                BtRemoteResponse.PairDevice resp = (BtRemoteResponse.PairDevice)this.remoteCall(req);
            }

            public string[] GetDevices()
            {
                BtRemoteRequest.GetDevices req = new BtRemoteRequest.GetDevices();
                BtRemoteResponse.GetDevices resp = (BtRemoteResponse.GetDevices)this.remoteCall(req);
                return resp.Addresses;
            }

            #endregion
        }

        #endregion

        #region BtLocalWorker

        class BtLocalWorker : Worker, IBtWorker
        {
            #region IBtWorker implementation

            public void RefreshDevices()
            {
                Debug.WriteLine("BtLocalWorker.RefreshDevices()");
            }

            public void PairDevice(string address, string pin)
            {
                Debug.WriteLine(string.Format("BtLocalWorker.PairDevice(), address: {0}, pin: {1}",
                    address,
                    pin));
            }

            public string[] GetDevices()
            {
                List<string> retVal = new List<string>();
                retVal.Add("112233445566");
                retVal.Add("223344556677");
                retVal.Add("334455667788");

                Debug.WriteLine("BtLocalWorker.GetDevices(), count: " + retVal.Count);

                return retVal.ToArray();
            }

            #endregion
        }

        #endregion
    }

    #endregion

    #region BtRemoteRequest

    [XmlInclude(typeof(BtRemoteRequest.RefreshDevices))]
    [XmlInclude(typeof(BtRemoteRequest.PairDevice))]
    [XmlInclude(typeof(BtRemoteRequest.GetDevices))]
    public abstract class BtRemoteRequest : RemoteRequest
    {
        #region RefreshDevices

        public class RefreshDevices : BtRemoteRequest
        {
            protected override RemoteResponse CreateResponse()
            {
                return new BtRemoteResponse.RefreshDevices();
            }

            protected override void ProcessInternal(
                RemoteWorker worker,
                RemoteResponse resp)
            {
                ((BtRemoteWorker)worker).RefreshDevices();
            }
        }

        #endregion

        #region PairDevice

        public class PairDevice : BtRemoteRequest
        {
            public string Address;
            public string Pin;

            protected override RemoteResponse CreateResponse()
            {
                return new BtRemoteResponse.PairDevice();
            }

            protected override void ProcessInternal(
                RemoteWorker worker,
                RemoteResponse resp)
            {
                ((BtRemoteWorker)worker).PairDevice(
                    this.Address,
                    this.Pin);
            }
        }

        #endregion

        #region GetDevices

        public class GetDevices : BtRemoteRequest
        {
            protected override RemoteResponse CreateResponse()
            {
                return new BtRemoteResponse.GetDevices();
            }

            protected override void ProcessInternal(
                RemoteWorker worker,
                RemoteResponse resp)
            {
                BtRemoteResponse.GetDevices r = (BtRemoteResponse.GetDevices)resp;
                r.Addresses = ((BtRemoteWorker)worker).GetDevices();
            }
        }

        #endregion
    }

    #endregion

    #region BtRemoteResponse

    [XmlInclude(typeof(BtRemoteResponse.RefreshDevices))]
    [XmlInclude(typeof(BtRemoteResponse.PairDevice))]
    [XmlInclude(typeof(BtRemoteResponse.GetDevices))]
    public abstract class BtRemoteResponse : RemoteResponse
    {
        #region RefreshDevices

        public class RefreshDevices : BtRemoteResponse
        {
        }

        #endregion

        #region PairDevice

        public class PairDevice : BtRemoteResponse
        {
        }

        #endregion

        #region GetDevices

        public class GetDevices : BtRemoteResponse
        {
            public string[] Addresses;
        }

        #endregion
    }

    #endregion

    #region BtRemoteSerializer

    public class BtRemoteSerializer : RemoteSerializer
    {
        public BtRemoteSerializer() : base(typeof(BtRemoteRequest), typeof(BtRemoteResponse)) { }
    }

    #endregion

    #region BtRemoteWorker

    public class BtRemoteWorker : RemoteWorker, IBtWorker, IDisposable
    {
        private IBtWorker worker = null;

        public BtRemoteWorker(int port)
            : base(port)
        {
            this.worker = BtWorker.Create(WorkerType.LOCAL);
        }

        public void Dispose()
        {
            if (this.worker != null)
            {
                ((BtWorker)this.worker).Dispose();
                this.worker = null;
            }
        }

        public static void Run(int port)
        {
            using (BtRemoteWorker w = new BtRemoteWorker(port))
            { w.run(); }
        }

        protected override RemoteSerializer CreateSerializer()
        {
            return new BtRemoteSerializer();
        }

        #region IBtWorker implementation

        public void RefreshDevices()
        {
            this.worker.RefreshDevices();
        }

        public void PairDevice(string address, string pin)
        {
            this.worker.PairDevice(address, pin);
        }

        public string[] GetDevices()
        {
            return this.worker.GetDevices();
        }

        #endregion
    }

    #endregion

    #region BtWorkerInstance

    public class BtWorkerInstance : IDisposable
    {
        private static IBtWorker instance = null;

        private static IBtWorker ensureInstance()
        {
            if (instance == null)
                instance = BtWorker.Create(Config.WorkerType);
            return instance;
        }

        public static IBtWorker Get()
        {
            return BtWorkerInstance.ensureInstance();
        }

        public static void Release()
        {
            if (instance != null)
            {
                ((Worker)instance).Dispose();
                instance = null;
            }
        }

        public void Dispose() { }
    }

    #endregion

    #region AbstractParser

    public abstract class AbstractParser : IDisposable
    {
        public delegate void ObjectFoundDelegate(byte[] bytes);
        private List<byte> buffer = new List<byte>();
        public event ObjectFoundDelegate OnObjectFound;

        public void Dispose()
        {
            this.Clear();
        }

        public void AddBytes(byte[] bytes)
        {
            lock (this)
            {
                this.buffer.AddRange(bytes);

                this.processBuffer();
            }
        }

        private void objectFound(byte[] bytes)
        {
            if (this.OnObjectFound != null)
                this.OnObjectFound(bytes);
        }

        private void processBuffer()
        {
            byte[] bufferBytes = this.buffer.ToArray();
            int bufferLength = bufferBytes.Length;
            int startIndex = 0;
            for (int index = 0; index < bufferLength; index++)
            {
                int count = index - startIndex + 1;
                string txtPart = Encoding.UTF8.GetString(bufferBytes, startIndex, count);
                if (!string.IsNullOrEmpty(txtPart))
                {
                    if (txtPart.EndsWith(this.terminalSymbol))
                    {
                        if (this.checkText(txtPart))
                        {
                            this.objectFound(Utils.GetSubArray(bufferBytes, startIndex, count));

                            this.buffer.RemoveRange(0, count);
                            startIndex = index + 1;
                        }
                    }
                }
            }
        }

        abstract protected bool checkText(string txt);

        public void Clear()
        {
            lock (this)
                this.buffer.Clear();
        }

        abstract protected string terminalSymbol { get; }

        public static AbstractParser Create(SerializationType serializationType)
        {
            if (serializationType == SerializationType.XML)
                return new XmlImplementation();
            return new JsonImplementation();
        }

        #region XmlImplementation

        class XmlImplementation : AbstractParser
        {
            protected override string terminalSymbol
            { get { return ">"; } }

            protected override bool checkText(string txt)
            {
                try
                {
                    XmlDocument xd = new XmlDocument();
                    xd.LoadXml(txt);

                    return true;
                }
                catch { }

                return false;
            }
        }

        #endregion

        #region JsonImplementation

        class JsonImplementation : AbstractParser
        {
            protected override string terminalSymbol
            { get { return "}"; } }

            protected override bool checkText(string txt)
            {
                byte[] bytes = Encoding.UTF8.GetBytes(txt);

                if (this.foundType(bytes, typeof(RemoteRequest)))
                    return true;

                if (this.foundType(bytes, typeof(RemoteResponse)))
                    return true;

                return false;
            }

            private bool foundType(byte[] bytes, Type t)
            {
                try
                {
                    using (MemoryStream ms = new MemoryStream(bytes))
                    {
                        DataContractJsonSerializer sr = new DataContractJsonSerializer(t);
                        object o = sr.ReadObject(ms);

                        return true;
                    }
                }
                catch { }

                return false;
            }
        }

        #endregion
    }

    #endregion

    #region ICntWorker

    public interface ICntWorker
    {
        void Start();
        void Stop();
    }

    #endregion

    #region CntWorker

    public abstract class CntWorker : Worker
    {
        public static ICntWorker Create(WorkerType workerType)
        {
            if (workerType == WorkerType.REMOTE)
                return new CntRemoteProxy();
            return new CntLocalWorker();
        }

        #region CntRemoteProxy

        class CntRemoteProxy : RemoteProxy, ICntWorker
        {
            protected override string CmdSwitch
            { get { return Program.CNT_SWITCH; } }

            protected override RemoteSerializer CreateSerializer()
            {
                return new CntRemoteSerializer();
            }

            protected override string DestDir
            { get { return Path.Combine(base.DestDir, "Cnt"); } }

            #region IBtWorker implementation

            public void Start()
            {
                CntRemoteRequest.Start req = new CntRemoteRequest.Start();
                CntRemoteResponse.Start resp = (CntRemoteResponse.Start)this.remoteCall(req);
            }

            public void Stop()
            {
                CntRemoteRequest.Stop req = new CntRemoteRequest.Stop();
                CntRemoteResponse.Stop resp = (CntRemoteResponse.Stop)this.remoteCall(req);
            }

            #endregion
        }

        #endregion

        #region CntLocalWorker

        class CntLocalWorker : Worker, ICntWorker
        {
            #region IBtWorker implementation

            public void Start()
            {
                Debug.WriteLine("CntLocalWorker.Start()");

                // TODO
            }

            public void Stop()
            {
                Debug.WriteLine("CntLocalWorker.Stop()");

                // TODO
            }

            #endregion
        }

        #endregion
    }

    #endregion

    #region CntRemoteRequest

    [XmlInclude(typeof(CntRemoteRequest.Start))]
    [XmlInclude(typeof(CntRemoteRequest.Stop))]
    public abstract class CntRemoteRequest : RemoteRequest
    {
        #region Start

        public class Start : CntRemoteRequest
        {
            protected override RemoteResponse CreateResponse()
            {
                return new CntRemoteResponse.Start();
            }

            protected override void ProcessInternal(
                RemoteWorker worker,
                RemoteResponse resp)
            {
                ((CntRemoteWorker)worker).Start();
            }
        }

        #endregion

        #region Stop

        public class Stop : CntRemoteRequest
        {
            protected override RemoteResponse CreateResponse()
            {
                return new CntRemoteResponse.Stop();
            }

            protected override void ProcessInternal(
                RemoteWorker worker,
                RemoteResponse resp)
            {
                ((CntRemoteWorker)worker).Stop();
            }
        }

        #endregion
    }

    #endregion

    #region CntRemoteResponse

    [XmlInclude(typeof(CntRemoteResponse.Start))]
    [XmlInclude(typeof(CntRemoteResponse.Stop))]
    public abstract class CntRemoteResponse : RemoteResponse
    {
        #region Start

        public class Start : CntRemoteResponse
        {
        }

        #endregion

        #region Stop

        public class Stop : CntRemoteResponse
        {
        }

        #endregion
    }

    #endregion

    #region CntRemoteSerializer

    public class CntRemoteSerializer : RemoteSerializer
    {
        public CntRemoteSerializer() : base(typeof(CntRemoteRequest), typeof(CntRemoteResponse)) { }
    }

    #endregion

    #region CntRemoteWorker

    public class CntRemoteWorker : RemoteWorker, ICntWorker, IDisposable
    {
        private ICntWorker worker = null;

        public CntRemoteWorker(int port)
            : base(port)
        {
            this.worker = CntWorker.Create(WorkerType.LOCAL);
        }

        public void Dispose()
        {
            if (this.worker != null)
            {
                ((CntWorker)this.worker).Dispose();
                this.worker = null;
            }
        }

        public static void Run(int port)
        {
            using (CntRemoteWorker w = new CntRemoteWorker(port))
            { w.run(); }
        }

        protected override RemoteSerializer CreateSerializer()
        {
            return new CntRemoteSerializer();
        }

        #region ICntWorker implementation

        public void Start()
        {
            this.worker.Start();
        }

        public void Stop()
        {
            this.worker.Stop();
        }

        #endregion
    }

    #endregion

    #region CntWorkerInstance

    public class CntWorkerInstance : IDisposable
    {
        private static ICntWorker instance = null;

        private static ICntWorker ensureInstance()
        {
            if (instance == null)
                instance = CntWorker.Create(Config.WorkerType);
            return instance;
        }

        public static ICntWorker Get()
        {
            return CntWorkerInstance.ensureInstance();
        }

        public static void Release()
        {
            if (instance != null)
            {
                ((Worker)instance).Dispose();
                instance = null;
            }
        }

        public void Dispose() { }
    }

    #endregion
}
