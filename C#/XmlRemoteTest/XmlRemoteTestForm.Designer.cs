﻿namespace XmlRemoteTest
{
    partial class XmlRemoteTestForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonCreateTransaction = new System.Windows.Forms.Button();
            this.buttonDoTransaction = new System.Windows.Forms.Button();
            this.buttonSetTransactionAmount = new System.Windows.Forms.Button();
            this.groupBoxTransaction = new System.Windows.Forms.GroupBox();
            this.groupBoxBluetooth = new System.Windows.Forms.GroupBox();
            this.buttonGetDevices = new System.Windows.Forms.Button();
            this.buttonPairDevice = new System.Windows.Forms.Button();
            this.buttonRefreshDevices = new System.Windows.Forms.Button();
            this.groupBoxCounter = new System.Windows.Forms.GroupBox();
            this.buttonStartCounter = new System.Windows.Forms.Button();
            this.buttonStopCounter = new System.Windows.Forms.Button();
            this.groupBoxTransaction.SuspendLayout();
            this.groupBoxBluetooth.SuspendLayout();
            this.groupBoxCounter.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonCreateTransaction
            // 
            this.buttonCreateTransaction.Location = new System.Drawing.Point(6, 19);
            this.buttonCreateTransaction.Name = "buttonCreateTransaction";
            this.buttonCreateTransaction.Size = new System.Drawing.Size(188, 23);
            this.buttonCreateTransaction.TabIndex = 0;
            this.buttonCreateTransaction.Text = "Create";
            this.buttonCreateTransaction.UseVisualStyleBackColor = true;
            this.buttonCreateTransaction.Click += new System.EventHandler(this.buttonCreateTransaction_Click);
            // 
            // buttonDoTransaction
            // 
            this.buttonDoTransaction.Location = new System.Drawing.Point(6, 77);
            this.buttonDoTransaction.Name = "buttonDoTransaction";
            this.buttonDoTransaction.Size = new System.Drawing.Size(188, 23);
            this.buttonDoTransaction.TabIndex = 1;
            this.buttonDoTransaction.Text = "Do";
            this.buttonDoTransaction.UseVisualStyleBackColor = true;
            this.buttonDoTransaction.Click += new System.EventHandler(this.buttonDoTransaction_Click);
            // 
            // buttonSetTransactionAmount
            // 
            this.buttonSetTransactionAmount.Location = new System.Drawing.Point(6, 48);
            this.buttonSetTransactionAmount.Name = "buttonSetTransactionAmount";
            this.buttonSetTransactionAmount.Size = new System.Drawing.Size(188, 23);
            this.buttonSetTransactionAmount.TabIndex = 3;
            this.buttonSetTransactionAmount.Text = "Set Amount";
            this.buttonSetTransactionAmount.UseVisualStyleBackColor = true;
            this.buttonSetTransactionAmount.Click += new System.EventHandler(this.buttonSetTransactionAmount_Click);
            // 
            // groupBoxTransaction
            // 
            this.groupBoxTransaction.Controls.Add(this.buttonCreateTransaction);
            this.groupBoxTransaction.Controls.Add(this.buttonDoTransaction);
            this.groupBoxTransaction.Controls.Add(this.buttonSetTransactionAmount);
            this.groupBoxTransaction.Location = new System.Drawing.Point(12, 12);
            this.groupBoxTransaction.Name = "groupBoxTransaction";
            this.groupBoxTransaction.Size = new System.Drawing.Size(200, 110);
            this.groupBoxTransaction.TabIndex = 4;
            this.groupBoxTransaction.TabStop = false;
            this.groupBoxTransaction.Text = "Transaction";
            // 
            // groupBoxBluetooth
            // 
            this.groupBoxBluetooth.Controls.Add(this.buttonGetDevices);
            this.groupBoxBluetooth.Controls.Add(this.buttonPairDevice);
            this.groupBoxBluetooth.Controls.Add(this.buttonRefreshDevices);
            this.groupBoxBluetooth.Location = new System.Drawing.Point(12, 128);
            this.groupBoxBluetooth.Name = "groupBoxBluetooth";
            this.groupBoxBluetooth.Size = new System.Drawing.Size(200, 110);
            this.groupBoxBluetooth.TabIndex = 5;
            this.groupBoxBluetooth.TabStop = false;
            this.groupBoxBluetooth.Text = "Bluetooth";
            // 
            // buttonGetDevices
            // 
            this.buttonGetDevices.Location = new System.Drawing.Point(6, 77);
            this.buttonGetDevices.Name = "buttonGetDevices";
            this.buttonGetDevices.Size = new System.Drawing.Size(188, 23);
            this.buttonGetDevices.TabIndex = 2;
            this.buttonGetDevices.Text = "Get Devices";
            this.buttonGetDevices.UseVisualStyleBackColor = true;
            this.buttonGetDevices.Click += new System.EventHandler(this.buttonGetDevices_Click);
            // 
            // buttonPairDevice
            // 
            this.buttonPairDevice.Location = new System.Drawing.Point(6, 48);
            this.buttonPairDevice.Name = "buttonPairDevice";
            this.buttonPairDevice.Size = new System.Drawing.Size(188, 23);
            this.buttonPairDevice.TabIndex = 1;
            this.buttonPairDevice.Text = "Pair";
            this.buttonPairDevice.UseVisualStyleBackColor = true;
            this.buttonPairDevice.Click += new System.EventHandler(this.buttonPairDevice_Click);
            // 
            // buttonRefreshDevices
            // 
            this.buttonRefreshDevices.Location = new System.Drawing.Point(6, 19);
            this.buttonRefreshDevices.Name = "buttonRefreshDevices";
            this.buttonRefreshDevices.Size = new System.Drawing.Size(188, 23);
            this.buttonRefreshDevices.TabIndex = 0;
            this.buttonRefreshDevices.Text = "Refresh";
            this.buttonRefreshDevices.UseVisualStyleBackColor = true;
            this.buttonRefreshDevices.Click += new System.EventHandler(this.buttonRefreshDevices_Click);
            // 
            // groupBoxCounter
            // 
            this.groupBoxCounter.Controls.Add(this.buttonStopCounter);
            this.groupBoxCounter.Controls.Add(this.buttonStartCounter);
            this.groupBoxCounter.Location = new System.Drawing.Point(12, 244);
            this.groupBoxCounter.Name = "groupBoxCounter";
            this.groupBoxCounter.Size = new System.Drawing.Size(200, 81);
            this.groupBoxCounter.TabIndex = 6;
            this.groupBoxCounter.TabStop = false;
            this.groupBoxCounter.Text = "Counter";
            // 
            // buttonStartCounter
            // 
            this.buttonStartCounter.Location = new System.Drawing.Point(6, 19);
            this.buttonStartCounter.Name = "buttonStartCounter";
            this.buttonStartCounter.Size = new System.Drawing.Size(188, 23);
            this.buttonStartCounter.TabIndex = 1;
            this.buttonStartCounter.Text = "Start";
            this.buttonStartCounter.UseVisualStyleBackColor = true;
            this.buttonStartCounter.Click += new System.EventHandler(this.buttonStartCounter_Click);
            // 
            // buttonStopCounter
            // 
            this.buttonStopCounter.Location = new System.Drawing.Point(6, 48);
            this.buttonStopCounter.Name = "buttonStopCounter";
            this.buttonStopCounter.Size = new System.Drawing.Size(188, 23);
            this.buttonStopCounter.TabIndex = 2;
            this.buttonStopCounter.Text = "Stop";
            this.buttonStopCounter.UseVisualStyleBackColor = true;
            this.buttonStopCounter.Click += new System.EventHandler(this.buttonStopCounter_Click);
            // 
            // XmlRemoteTestForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(223, 339);
            this.Controls.Add(this.groupBoxCounter);
            this.Controls.Add(this.groupBoxBluetooth);
            this.Controls.Add(this.groupBoxTransaction);
            this.Name = "XmlRemoteTestForm";
            this.Text = "XmlRemoteTest";
            this.groupBoxTransaction.ResumeLayout(false);
            this.groupBoxBluetooth.ResumeLayout(false);
            this.groupBoxCounter.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonCreateTransaction;
        private System.Windows.Forms.Button buttonDoTransaction;
        private System.Windows.Forms.Button buttonSetTransactionAmount;
        private System.Windows.Forms.GroupBox groupBoxTransaction;
        private System.Windows.Forms.GroupBox groupBoxBluetooth;
        private System.Windows.Forms.Button buttonRefreshDevices;
        private System.Windows.Forms.Button buttonPairDevice;
        private System.Windows.Forms.Button buttonGetDevices;
        private System.Windows.Forms.GroupBox groupBoxCounter;
        private System.Windows.Forms.Button buttonStartCounter;
        private System.Windows.Forms.Button buttonStopCounter;
    }
}

