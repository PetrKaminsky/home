﻿using System;
using System.Diagnostics;
using System.Net.Sockets;
using System.Net;
using System.Text;
using NLog;

namespace RemoteTest
{
    #region LocalWorker

    public abstract class LocalWorker : IWorker
    {
        private readonly Logger logger = LogManager.GetLogger(nameof(LocalWorker));
        private readonly int svrPort = -1;
        private TcpClient client = null;
        private DataSerializer serializer = null;
        private readonly AbstractParser parser = null;

        public LocalWorker(int port)
        {
            svrPort = port;
            client = new TcpClient();
            parser = AbstractParser.Create(Config.SerializationType);
            parser.OnObjectFound += new AbstractParser.ObjectFoundDelegate(OnObjectFound);
        }

        private void OnObjectFound(byte[] bytes)
        {
            logger.Info($"{nameof(OnObjectFound)}, {nameof(bytes)}: {bytes.Length}");

            var s = client.GetStream();

            Debug.WriteLine($"{nameof(LocalWorker)}.{nameof(OnObjectFound)}, request: {Encoding.UTF8.GetString(bytes)}");

            // complete request is in req
            var resp = GetResponse(bytes);

            Debug.WriteLine($"{nameof(LocalWorker)}.{nameof(OnObjectFound)}, response: {Encoding.UTF8.GetString(resp)}");

            // send response back
            s.Write(resp, 0, resp.Length);
            s.Flush();
        }

        protected void Run()
        {
            ConnectAndProcessRequests();
            Disconnect();
        }

        private void ConnectAndProcessRequests()
        {
            try
            {
                client.Connect(IPAddress.Loopback, svrPort);
                ProcessRequests();
            }
            catch { }
        }

        private void Disconnect()
        {
            try
            {
                if (client.Client.Connected)
                {
                    client.GetStream().Close();
                }
                client.Close();
            }
            catch { }
            client.Dispose();
            client = null;
        }

        private void ProcessRequests()
        {
            var s = client.GetStream();
            var buff = new byte[256];
            while (true)
            {
                var bytesRead = s.Read(buff, 0, buff.Length);
                if (bytesRead == 0)
                {
                    return;
                }
                else if (bytesRead > 0)
                {
                    parser.AddBytes(Utils.GetSubArray(buff, 0, bytesRead));
                }
            }
        }

        private DataSerializer Serializer
        {
            get
            {
                if (serializer == null)
                {
                    serializer = CreateSerializer();
                }
                return serializer;
            }
        }

        protected abstract DataSerializer CreateSerializer();

        private byte[] GetResponse(byte[] bytes)
        {
            logger.Info($"{nameof(GetResponse)}, {nameof(bytes)}: {bytes.Length}");

            var req = Serializer.DeserializeRequest(bytes);
            var resp = Call(req);
            return Serializer.SerializeResponse(resp);
        }

        private DataResponse Call(DataRequest req)
        {
            logger.Info($"{nameof(Call)}, {nameof(req)}: {req.GetType().Name}");

            return req.Process(this);
        }
    }

    #endregion

    #region TransactionLocalWorker

    public class TransactionLocalWorker : LocalWorker, ITransactionWorker
    {
        private readonly Logger logger = LogManager.GetLogger(nameof(TransactionLocalWorker));
        private ITransactionWorker worker = null;

        public TransactionLocalWorker(int port)
            : base(port)
        {
            worker = new TransactionWorkerImplementation();
        }

        public void Dispose()
        {
            if (worker != null)
            {
                worker.Dispose();
                worker = null;
            }
        }

        public static void Run(int port)
        {
            using (var w = new TransactionLocalWorker(port))
            {
                w.Run();
            }
        }

        protected override DataSerializer CreateSerializer()
        {
            return new TransactionSerializer();
        }

        #region ITransactionWorker implementation

        public void CreateTransaction()
        {
            logger.Info($"{nameof(CreateTransaction)}");

            Log.Elapsed(logger, "worker.CreateTransaction()", () =>
            {
                worker.CreateTransaction();
            });
        }

        public void SetTransactionAmount(decimal tranAmount)
        {
            logger.Info($"{nameof(SetTransactionAmount)} - {nameof(tranAmount)}: {tranAmount}");

            Log.Elapsed(logger, "worker.SetTransactionAmount()", () =>
            {
                worker.SetTransactionAmount(tranAmount);
            });
        }

        public bool DoTransaction()
        {
            logger.Info($"{nameof(DoTransaction)}");

            var retVal = false;

            Log.Elapsed(logger, "worker.DoTransaction()", () =>
            {
                retVal = worker.DoTransaction();
            });

            logger.Info($"{nameof(retVal)}: {retVal}");

            return retVal;
        }

        #endregion
    }

    #endregion

    #region TransactionWorkerImplementation

    public class TransactionWorkerImplementation : Worker, ITransactionWorker
    {
        private readonly Logger logger = LogManager.GetLogger(nameof(TransactionWorkerImplementation));

        #region ITranWorker implementation

        public void CreateTransaction()
        {
            logger.Info($"{nameof(CreateTransaction)}");
        }

        public void SetTransactionAmount(decimal tranAmount)
        {
            logger.Info($"{nameof(SetTransactionAmount)} - {nameof(tranAmount)}: {tranAmount}");
        }

        public bool DoTransaction()
        {
            var retVal = new Random().Next(2) == 1;

            logger.Info($"{nameof(DoTransaction)}, {nameof(retVal)}: {retVal}");

            return retVal;
        }

        #endregion
    }

    #endregion
}
