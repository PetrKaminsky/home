using RemoteTest;
using System;
using System.Windows.Forms;

namespace GuiProxy
{
    internal static class Program
    {
        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [MTAThread]
        static void Main()
        {
            // To customize application configuration such as set high DPI settings or default font,
            // see https://aka.ms/applicationconfiguration.

            Config.SerializationType = SerializationType.Json;
            Utils.ExeFileName = "C:\\Users\\czpekam1\\source\\RemoteTest\\Local\\bin\\Debug\\Local.exe";

            ApplicationConfiguration.Initialize();
            Application.Run(new RemoteTestForm());

            TransactionWorkerInstance.Release();
        }
    }
}