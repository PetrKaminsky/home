﻿namespace RemoteTest
{
    partial class RemoteTestForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            buttonCreateTransaction = new System.Windows.Forms.Button();
            buttonDoTransaction = new System.Windows.Forms.Button();
            buttonSetTransactionAmount = new System.Windows.Forms.Button();
            groupBoxTransaction = new System.Windows.Forms.GroupBox();
            groupBoxTransaction.SuspendLayout();
            SuspendLayout();
            // 
            // buttonCreateTransaction
            // 
            buttonCreateTransaction.Location = new System.Drawing.Point(7, 22);
            buttonCreateTransaction.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            buttonCreateTransaction.Name = "buttonCreateTransaction";
            buttonCreateTransaction.Size = new System.Drawing.Size(219, 27);
            buttonCreateTransaction.TabIndex = 0;
            buttonCreateTransaction.Text = "Create";
            buttonCreateTransaction.UseVisualStyleBackColor = true;
            buttonCreateTransaction.Click += ButtonCreateTransaction_Click;
            // 
            // buttonDoTransaction
            // 
            buttonDoTransaction.Location = new System.Drawing.Point(7, 89);
            buttonDoTransaction.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            buttonDoTransaction.Name = "buttonDoTransaction";
            buttonDoTransaction.Size = new System.Drawing.Size(219, 27);
            buttonDoTransaction.TabIndex = 1;
            buttonDoTransaction.Text = "Do";
            buttonDoTransaction.UseVisualStyleBackColor = true;
            buttonDoTransaction.Click += ButtonDoTransaction_Click;
            // 
            // buttonSetTransactionAmount
            // 
            buttonSetTransactionAmount.Location = new System.Drawing.Point(7, 55);
            buttonSetTransactionAmount.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            buttonSetTransactionAmount.Name = "buttonSetTransactionAmount";
            buttonSetTransactionAmount.Size = new System.Drawing.Size(219, 27);
            buttonSetTransactionAmount.TabIndex = 3;
            buttonSetTransactionAmount.Text = "Set Amount";
            buttonSetTransactionAmount.UseVisualStyleBackColor = true;
            buttonSetTransactionAmount.Click += ButtonSetTransactionAmount_Click;
            // 
            // groupBoxTransaction
            // 
            groupBoxTransaction.Controls.Add(buttonCreateTransaction);
            groupBoxTransaction.Controls.Add(buttonDoTransaction);
            groupBoxTransaction.Controls.Add(buttonSetTransactionAmount);
            groupBoxTransaction.Location = new System.Drawing.Point(14, 14);
            groupBoxTransaction.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            groupBoxTransaction.Name = "groupBoxTransaction";
            groupBoxTransaction.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            groupBoxTransaction.Size = new System.Drawing.Size(233, 127);
            groupBoxTransaction.TabIndex = 4;
            groupBoxTransaction.TabStop = false;
            groupBoxTransaction.Text = "Transaction";
            // 
            // RemoteTestForm
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            ClientSize = new System.Drawing.Size(261, 153);
            Controls.Add(groupBoxTransaction);
            Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            Name = "RemoteTestForm";
            Text = "GUI/Proxy";
            groupBoxTransaction.ResumeLayout(false);
            ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.Button buttonCreateTransaction;
        private System.Windows.Forms.Button buttonDoTransaction;
        private System.Windows.Forms.Button buttonSetTransactionAmount;
        private System.Windows.Forms.GroupBox groupBoxTransaction;
    }
}

