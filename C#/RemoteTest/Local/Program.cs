﻿using RemoteTest;
using System;

namespace Local
{
    internal static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [MTAThread]
        static void Main(string[] args)
        {
            if (args.Length > 0)
            {
                try
                {
                    int port = int.Parse(args[0]);
                    Config.SerializationType = (SerializationType)Enum.Parse(typeof(SerializationType), args[1]);
                    TransactionLocalWorker.Run(port);
                }
                catch { }

                return;
            }
        }
    }
}
