﻿using NLog;
using System;
using System.Windows.Forms;

namespace RemoteTest
{
    public partial class RemoteTestForm : Form
    {
        private readonly Logger logger = LogManager.GetLogger(nameof(RemoteTestForm));

        public RemoteTestForm()
        {
            InitializeComponent();
        }

        private void ButtonCreateTransaction_Click(object sender, EventArgs e)
        {
            logger.Info($"{nameof(ButtonCreateTransaction_Click)}");

            try
            {
                ITransactionWorker worker = null;

                Log.Elapsed(logger, "TransactionWorkerInstance.GetWorker()", () =>
                {
                    worker = TransactionWorkerInstance.GetWorker();
                });

                Log.Elapsed(logger, "worker.CreateTransaction()", () =>
                {
                    worker.CreateTransaction();
                });
            }
            catch (Exception ex)
            {
                ShowException(ex);
            }
        }

        private void ButtonSetTransactionAmount_Click(object sender, EventArgs e)
        {
            logger.Info($"{nameof(ButtonSetTransactionAmount_Click)}");

            try
            {
                ITransactionWorker worker = null;

                Log.Elapsed(logger, "TransactionWorkerInstance.GetWorker()", () =>
                {
                    worker = TransactionWorkerInstance.GetWorker();
                });

                var amount = (decimal)(Math.Round(new Random().NextDouble() * 10, 2) + 10);

                logger.Debug($"{nameof(amount)}: {amount}");

                Log.Elapsed(logger, "worker.SetTransactionAmount()", () =>
                {
                    worker.SetTransactionAmount(amount);
                });
            }
            catch (Exception ex)
            {
                ShowException(ex);
            }
        }

        private void ButtonDoTransaction_Click(object sender, EventArgs e)
        {
            logger.Info($"{nameof(ButtonDoTransaction_Click)}");

            try
            {
                ITransactionWorker worker = null;

                Log.Elapsed(logger, "TransactionWorkerInstance.GetWorker()", () =>
                {
                    worker = TransactionWorkerInstance.GetWorker();
                });

                var retVal = false;

                Log.Elapsed(logger, "worker.DoTransaction()", () =>
                {
                    retVal = worker.DoTransaction();
                });

                logger.Debug($"{nameof(retVal)}: {retVal}");
            }
            catch (Exception ex)
            {
                ShowException(ex);
            }
        }

        private static void ShowException(Exception ex)
        {
            MessageBox.Show(ex.ToString(), ex.GetType().Name);
        }
    }
}
