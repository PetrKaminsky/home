﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Diagnostics;
using System.IO;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using System.Text;
using System.Threading;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using NLog;

namespace RemoteTest
{
    #region Worker

    public abstract class Worker : IDisposable
    {
        protected virtual void Init() { }
        protected virtual void Cleanup() { }

        protected Worker()
        {
            Init();
        }

        public void Dispose()
        {
            Cleanup();
        }
    }

    #endregion

    #region IRemoteProxy

    public interface IRemoteProxy
    {
        bool IsAlive { get; }
    }

    #endregion

    #region RemoteProxy

    public abstract class RemoteProxy : Worker, IRemoteProxy
    {
        private TcpClient client = null;
        private Process wrkProcess = null;
        private DataSerializer serializer = null;
        private AbstractParser parser = null;
        private Thread threadSocketRead = null;
        private bool isAlive = false;

        protected override void Init()
        {
            var dstFullExeName = Utils.ExeFileName;
            // create server socket
            var svr = new TcpListener(IPAddress.Loopback, 0);
            svr.Start();
            var localPort = ((IPEndPoint)svr.LocalEndpoint).Port;
            // run worker
            wrkProcess = new Process();
            wrkProcess.StartInfo.FileName = dstFullExeName;
            wrkProcess.StartInfo.Arguments = $"{localPort} {Config.SerializationType}";
            wrkProcess.Start();
            // accept client
            client = svr.AcceptTcpClient();
            // stop server socket
            svr.Stop();
            // create parser
            parser = AbstractParser.Create(Config.SerializationType);
            // create read thread
            threadSocketRead = new Thread(new ThreadStart(SocketReadThreadProc));
            threadSocketRead.Start();
        }

        protected override void Cleanup()
        {
            if (client.Client.Connected)
            {
                client.GetStream().Close();
            }
            client.Close();
            client.Dispose();
            client = null;
            if (!wrkProcess.WaitForExit(1000))
            {
                wrkProcess.Kill();
            }
            wrkProcess.Close();
            wrkProcess.Dispose();
            wrkProcess = null;
            threadSocketRead = null;
        }

        protected DataResponse RemoteCall(DataRequest req)
        {
            var resp = RemoteCallInternal(req);
            if (!string.IsNullOrEmpty(resp.ErrorText))
            {
                throw new Exception(resp.ErrorText);
            }
            return resp;
        }

        protected DataSerializer Serializer
        {
            get
            {
                if (serializer == null)
                {
                    serializer = CreateSerializer();
                }
                return serializer;
            }
        }

        public bool IsAlive
        {
            get
            {
                lock (this)
                {
                    return isAlive;
                }
            }
        }

        protected abstract DataSerializer CreateSerializer();

        private DataResponse RemoteCallInternal(DataRequest req)
        {
            // serialize request
            var byteReq = Serializer.SerializeRequest(req);

            Debug.WriteLine($"{nameof(RemoteProxy)}.{nameof(RemoteCallInternal)}, request: {Encoding.UTF8.GetString(byteReq)}");

            var byteResp = GetResponse(byteReq);

            Debug.WriteLine($"{nameof(RemoteProxy)}.{nameof(RemoteCallInternal)}, response: {Encoding.UTF8.GetString(byteResp)}");

            // deserialize response
            return Serializer.DeserializeResponse(byteResp);
        }

        private byte[] GetResponse(byte[] xmlReq)
        {
            // send request
            WriteRequest(xmlReq);
            // read response
            return ReadResponse();
        }

        private void WriteRequest(byte[] xmlReq)
        {
            var s = client.GetStream();
            s.Write(xmlReq, 0, xmlReq.Length);
            s.Flush();
        }

        private byte[] ReadResponse()
        {
            var resp = new List<byte>();
            var waitEvent = new ManualResetEvent(false);
            var onObjectFound = new AbstractParser.ObjectFoundDelegate(delegate (byte[] bytes)
            {
                resp.AddRange(bytes);
                waitEvent.Set();
            });
            parser.OnObjectFound += onObjectFound;
            waitEvent.WaitOne();
            parser.OnObjectFound -= onObjectFound;
            return resp.ToArray();
        }

        private void SocketReadThreadProc()
        {
            lock (this)
            {
                isAlive = true;
            }

            try
            {
                var s = client.GetStream();
                var buff = new byte[256];
                while (true)
                {
                    var bytesRead = s.Read(buff, 0, buff.Length);
                    if (bytesRead > 0)
                    {
                        parser.AddBytes(Utils.GetSubArray(buff, 0, bytesRead));
                    }
                }
            }
            catch { }

            lock (this)
            {
                isAlive = false;
            }
        }
    }

    #endregion

    #region DataSerializer

    public abstract class DataSerializer
    {
        private readonly Type requestType;
        private readonly Type responseType;
        private readonly AbstractSerializer serializer;

        internal DataSerializer(Type reqType, Type respType, AbstractSerializer ser)
        {
            requestType = reqType;
            responseType = respType;
            serializer = ser;
        }

        public byte[] SerializeRequest(DataRequest req)
        {
            return Serialize(req, requestType);
        }

        public byte[] SerializeResponse(DataResponse resp)
        {
            return Serialize(resp, responseType);
        }

        private byte[] Serialize(object o, Type t)
        {
            using (var ms = new MemoryStream())
            {
                serializer.Serialize(ms, o, t);
                return ms.ToArray();
            }
        }

        public DataRequest DeserializeRequest(byte[] byteReq)
        {
            return (DataRequest)Deserialize(byteReq, requestType);
        }

        public DataResponse DeserializeResponse(byte[] byteResp)
        {
            return (DataResponse)Deserialize(byteResp, responseType);
        }

        public object Deserialize(byte[] xml, Type t)
        {
            using (var ms = new MemoryStream(xml))
            {
                return serializer.Deserialize(ms, t);
            }
        }

        internal abstract class AbstractSerializer
        {
            internal static AbstractSerializer Create(SerializationType serializationType)
            {
                if (serializationType == SerializationType.Xml)
                {
                    return new XmlImplementation();
                }
                return new JsonImplementation();
            }

            internal abstract object Deserialize(Stream s, Type t);
            internal abstract void Serialize(Stream s, object o, Type t);

            class XmlImplementation : AbstractSerializer
            {
                internal override object Deserialize(Stream s, Type t)
                {
                    var sr = new XmlSerializer(t);
                    return sr.Deserialize(s);
                }

                internal override void Serialize(Stream s, object o, Type t)
                {
                    var sr = new XmlSerializer(t);
                    sr.Serialize(s, o);
                }
            }

            class JsonImplementation : AbstractSerializer
            {
                internal override object Deserialize(Stream s, Type t)
                {
                    var sr = new DataContractJsonSerializer(t);
                    return sr.ReadObject(s);
                }

                internal override void Serialize(Stream s, object o, Type t)
                {
                    var sr = new DataContractJsonSerializer(t);
                    sr.WriteObject(s, o);
                }
            }
        }
    }

    #endregion

    #region DataRequest

    [KnownType(typeof(TransactionRequest.CreateTransactionRequest))]
    [KnownType(typeof(TransactionRequest.SetTransactionAmountRequest))]
    [KnownType(typeof(TransactionRequest.DoTransactionRequest))]
    public abstract class DataRequest
    {
        public DataResponse Process(IWorker worker)
        {
            var resp = CreateResponse();
            try
            {
                ProcessInternal(worker, resp);
            }
            catch (Exception ex)
            {
                resp.Error(ex);
            }
            return resp;
        }

        protected abstract DataResponse CreateResponse();
        protected abstract void ProcessInternal(
            IWorker worker,
            DataResponse resp);
    }

    #endregion

    #region DataResponse

    [KnownType(typeof(TransactionResponse.CreateTransactionResponse))]
    [KnownType(typeof(TransactionResponse.SetTransactionAmountResponse))]
    [KnownType(typeof(TransactionResponse.DoTransactionResponse))]
    public abstract class DataResponse
    {
        public string ErrorText;

        public void Error(Exception ex)
        {
            ErrorText = $"{ex.GetType().Name}, {ex.Message}";
        }
    }

    #endregion

    #region IWorker

    public interface IWorker
    { }

    #endregion

    #region ITransactionWorker

    public interface ITransactionWorker : IWorker, IDisposable
    {
        void CreateTransaction();
        void SetTransactionAmount(decimal tranAmount);
        bool DoTransaction();
    }

    #endregion

    #region TransactionRemoteProxyImplementation

    public class TransactionRemoteProxyImplementation : RemoteProxy, ITransactionWorker
    {
        protected override DataSerializer CreateSerializer()
        {
            return new TransactionSerializer();
        }

        #region ITransactionWorker implementation

        public void CreateTransaction()
        {
            Debug.WriteLine($"{nameof(TransactionRemoteProxyImplementation)}.{nameof(CreateTransaction)} - before");

            var req = new TransactionRequest.CreateTransactionRequest();
            var resp = (TransactionResponse.CreateTransactionResponse)RemoteCall(req);

            Debug.WriteLine($"{nameof(TransactionRemoteProxyImplementation)}.{nameof(CreateTransaction)} - after");
        }

        public void SetTransactionAmount(decimal tranAmount)
        {
            Debug.WriteLine($"{nameof(TransactionRemoteProxyImplementation)}.{nameof(SetTransactionAmount)} - before, {nameof(tranAmount)}: {tranAmount}");

            var req = new TransactionRequest.SetTransactionAmountRequest();
            req.TranAmount = tranAmount;
            var resp = (TransactionResponse.SetTransactionAmountResponse)RemoteCall(req);

            Debug.WriteLine($"{nameof(TransactionRemoteProxyImplementation)}.{nameof(SetTransactionAmount)} - after");
        }

        public bool DoTransaction()
        {
            Debug.WriteLine($"{nameof(TransactionRemoteProxyImplementation)}.{nameof(DoTransaction)} - before");

            var req = new TransactionRequest.DoTransactionRequest();
            var resp = (TransactionResponse.DoTransactionResponse)RemoteCall(req);
            var retVal = resp.Result;

            Debug.WriteLine($"{nameof(TransactionRemoteProxyImplementation)}.{nameof(DoTransaction)} - after, {nameof(retVal)}: {retVal}");

            return retVal;
        }

        #endregion
    }

    #endregion

    #region TransactionWorkerInstance

    public class TransactionWorkerInstance
    {
        private static ITransactionWorker instance = null;

        private static ITransactionWorker EnsureInstance()
        {
            if (instance == null ||
                !(instance as IRemoteProxy).IsAlive)
            {
                instance = new TransactionRemoteProxyImplementation();
            }
            return instance;
        }

        public static ITransactionWorker GetWorker()
        {
            return EnsureInstance();
        }

        public static void Release()
        {
            if (instance != null)
            {
                (instance as Worker).Dispose();
                instance = null;
            }
        }
    }

    #endregion

    #region TransactionSerializer

    public class TransactionSerializer : DataSerializer
    {
        public TransactionSerializer()
            : base(typeof(TransactionRequest), typeof(TransactionResponse), AbstractSerializer.Create(Config.SerializationType))
        { }
    }

    #endregion

    #region TransactionRequest

    [XmlInclude(typeof(CreateTransactionRequest))]
    [XmlInclude(typeof(SetTransactionAmountRequest))]
    [XmlInclude(typeof(DoTransactionRequest))]
    public abstract class TransactionRequest : DataRequest
    {
        #region CreateTransactionRequest

        public class CreateTransactionRequest : TransactionRequest
        {
            protected override DataResponse CreateResponse()
            {
                return new TransactionResponse.CreateTransactionResponse();
            }

            protected override void ProcessInternal(
                IWorker worker,
                DataResponse resp)
            {
                ((ITransactionWorker)worker).CreateTransaction();
            }
        }

        #endregion

        #region SetTransactionAmountRequest

        public class SetTransactionAmountRequest : TransactionRequest
        {
            public decimal TranAmount { get; set; }

            protected override DataResponse CreateResponse()
            {
                return new TransactionResponse.SetTransactionAmountResponse();
            }

            protected override void ProcessInternal(
                IWorker worker,
                DataResponse resp)
            {
                ((ITransactionWorker)worker).SetTransactionAmount(TranAmount);
            }
        }

        #endregion

        #region DoTransactionRequest

        public class DoTransactionRequest : TransactionRequest
        {
            protected override DataResponse CreateResponse()
            {
                return new TransactionResponse.DoTransactionResponse();
            }

            protected override void ProcessInternal(
                IWorker worker,
                DataResponse resp)
            {
                var r = (TransactionResponse.DoTransactionResponse)resp;
                r.Result = ((ITransactionWorker)worker).DoTransaction();
            }
        }

        #endregion
    }

    #endregion

    #region TransactionResponse

    [XmlInclude(typeof(CreateTransactionResponse))]
    [XmlInclude(typeof(SetTransactionAmountResponse))]
    [XmlInclude(typeof(DoTransactionResponse))]
    public abstract class TransactionResponse : DataResponse
    {
        #region CreateTransactionResponse

        public class CreateTransactionResponse : TransactionResponse
        {
        }

        #endregion

        #region SetTransactionAmountResponse

        public class SetTransactionAmountResponse : TransactionResponse
        {
        }

        #endregion

        #region DoTransactionResponse

        public class DoTransactionResponse : TransactionResponse
        {
            public bool Result { get; set; }
        }

        #endregion
    }

    #endregion

    #region Config

    public enum WorkerType
    {
        Unknoown,
        Local,
        RemoteProxy,
    }

    public enum SerializationType
    {
        Unknown,
        Xml,
        Json,
    }

    public static class Config
    {
        public static SerializationType SerializationType { get; set; }
    }

    #endregion

    #region Utils

    public static class Utils
    {
        private static string _exeFileName;

        public static string ExeFileName
        {
            get
            {
                return _exeFileName;
            }
            set
            {
                _exeFileName = value;
            }
        }

        public static byte[] GetSubArray(byte[] array, int startIndex, int count)
        {
            var retVal = new byte[count];
            Array.Copy(array, startIndex, retVal, 0, count);
            return retVal;
        }

        public static void EnsureDir(string dir)
        {
            if (!Directory.Exists(dir))
                Directory.CreateDirectory(dir);
        }
    }

    #endregion

    #region AbstractParser

    public abstract class AbstractParser : IDisposable
    {
        public delegate void ObjectFoundDelegate(byte[] bytes);
        private readonly List<byte> buffer = new List<byte>();
        public event ObjectFoundDelegate OnObjectFound;

        public void Dispose()
        {
            Clear();
        }

        public void AddBytes(byte[] bytes)
        {
            lock (this)
            {
                buffer.AddRange(bytes);

                ProcessBuffer();
            }
        }

        private void ObjectFound(byte[] bytes)
        {
            OnObjectFound?.Invoke(bytes);
        }

        private void ProcessBuffer()
        {
            var bufferBytes = buffer.ToArray();
            var bufferLength = bufferBytes.Length;
            var startIndex = 0;
            for (var index = 0; index < bufferLength; index++)
            {
                var count = index - startIndex + 1;
                var txtPart = Encoding.UTF8.GetString(bufferBytes, startIndex, count);
                if (!string.IsNullOrEmpty(txtPart))
                {
                    if (txtPart.EndsWith(TerminalSymbol))
                    {
                        if (CheckText(txtPart))
                        {
                            ObjectFound(Utils.GetSubArray(bufferBytes, startIndex, count));

                            buffer.RemoveRange(0, count);
                            startIndex = index + 1;
                        }
                    }
                }
            }
        }

        abstract protected bool CheckText(string txt);

        public void Clear()
        {
            lock (this)
            {
                buffer.Clear();
            }
        }

        abstract protected string TerminalSymbol { get; }

        public static AbstractParser Create(SerializationType serializationType)
        {
            if (serializationType == SerializationType.Xml)
            {
                return new XmlImplementation();
            }
            return new JsonImplementation();
        }

        #region XmlImplementation

        class XmlImplementation : AbstractParser
        {
            protected override string TerminalSymbol
            { get { return ">"; } }

            protected override bool CheckText(string txt)
            {
                try
                {
                    XmlDocument xd = new XmlDocument();
                    xd.LoadXml(txt);

                    return true;
                }
                catch { }

                return false;
            }
        }

        #endregion

        #region JsonImplementation

        class JsonImplementation : AbstractParser
        {
            protected override string TerminalSymbol
            { get { return "}"; } }

            protected override bool CheckText(string txt)
            {
                var bytes = Encoding.UTF8.GetBytes(txt);

                if (FoundType(bytes, typeof(DataRequest)))
                {
                    return true;
                }

                if (FoundType(bytes, typeof(DataResponse)))
                {
                    return true;
                }

                return false;
            }

            private bool FoundType(byte[] bytes, Type t)
            {
                try
                {
                    using (var ms = new MemoryStream(bytes))
                    {
                        var sr = new DataContractJsonSerializer(t);
                        var o = sr.ReadObject(ms);

                        return true;
                    }
                }
                catch { }

                return false;
            }
        }

        #endregion
    }

    #endregion

    public class Log
    {
        public static void Elapsed(Logger logger, string message, Action action)
        {
            logger.Debug($"{message} - before");

            var stopwatch = Stopwatch.StartNew();

            action?.Invoke();

            stopwatch.Stop();

            logger.Debug($"{message} - after, elapsed: {stopwatch.ElapsedMilliseconds} msec");
        }
    }
}
