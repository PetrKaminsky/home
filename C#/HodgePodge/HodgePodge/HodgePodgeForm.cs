﻿using System;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HodgePodge
{
    public partial class HodgePodgeForm : Form
    {
        public HodgePodgeForm()
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            GlobalData data = GlobalData.Instance;
            int width = Width;
            int height = Height;

            if (!data.Initialized ||
                    width != data.Width ||
                    height != data.Height)
            {
                data.Initialize(width, height);
            }
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            var g = e.Graphics;

            GlobalData data = GlobalData.Instance;
            if (data.Initialized)
            {
                int maxValue = Parameters.Instance.MaxValue;
                int width = Width;
                int height = Height;

                for (int j = 0; j < height; j++)
                {
                    for (int i = 0; i < width; i++)
                    {
                        byte value = data.GetValue(i, j);
                        int normalizedValue = GetNormalizedColorValue(value, maxValue);
                        var color = Color.FromArgb(
                                normalizedValue,
                                normalizedValue,
                                normalizedValue);
                        var pen = new Pen(color);
                        g.DrawRectangle(pen, i, j, 1, 1);
                    }
                }
            }

            ScheduleCalculation();
        }

        private static int GetNormalizedColorValue(int value, int maxValue)
        {
            return 255 * value / maxValue;
        }

        private void RedrawContents()
        {
            Invalidate();
        }

        private async void ScheduleCalculation()
        {
            await Task.Delay(100);
            await CalculateAsync();

            GlobalData.Instance.SwitchCurrentData();
            RedrawContents();
        }

        private Task CalculateAsync()
        {
            return Task.Factory.StartNew(() =>
            {
                GlobalData.Instance.CalculateNextGeneration();
            });
        }
    }
}
