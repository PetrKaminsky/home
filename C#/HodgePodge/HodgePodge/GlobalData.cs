﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HodgePodge
{
    public class GlobalData
    {
        private static GlobalData instance = new GlobalData();

        private GlobalData() { }

        public static GlobalData Instance
        { get { return instance; } }

        private int width = 0;
        private int height = 0;
        private byte[] data1 = null;
        private byte[] data2 = null;
        private byte[] currentData = null;
        private int maxValue = 0;
        private int constK1 = 0;
        private int constK2 = 0;
        private int constG = 0;

        public int Width
        { get { return width; } }

        public int Height
        { get { return height; } }

        public bool Initialized
        { get { return width != 0 && height != 0; } }

        public void Initialize(int w, int h)
        {
            width = w;
            height = h;

            AllocateMemory();

            RestartGeneration();
        }

        public void RestartGeneration()
        {
            currentData = data1;

            ReloadParameters();
            RandomizeFirstGeneration();
        }

        private void AllocateMemory()
        {
            int size = width * height;
            data1 = new byte[size];
            data2 = new byte[size];
        }

        private void RandomizeFirstGeneration()
        {
            int bound = maxValue + 1;
            Random random = new Random();
            for (int i = 0; i < currentData.Length; i++)
            {
                currentData[i] = (byte)random.Next(bound);
            }
        }

        private void ReloadParameters()
        {
            maxValue = Parameters.Instance.MaxValue;
            constK1 = Parameters.Instance.ConstK1;
            constK2 = Parameters.Instance.ConstK2;
            constG = Parameters.Instance.ConstG;
        }
        public void CalculateNextGeneration()
        {
            if (!Initialized)
            {
                return;
            }

            byte[] sourceData = currentData;
            byte[] destinationData = currentData == data1 ? data2 : data1;
            for (int j = 0; j < height; j++)
            {
                for (int i = 0; i < width; i++)
                {
                    SetValue(
                            destinationData,
                            i,
                            j,
                            (byte)CalculateNewValue(
                                    sourceData,
                                    i,
                                    j));
                }
            }
        }

        private int CalculateNewValue(byte[] data, int x, int y)
        {
            int currState = GetValue(data, x, y), newState = 0;
            if (IsHealthy(currState))
            {
                newState = CalculateNewValueOfHealthy(data, x, y);
            }
            else if (IsInfected(currState))
            {
                newState = CalculateNewValueOfInfected(data, x, y);
            }

            return newState;
        }

        private bool IsHealthy(int state)
        {
            return state == 0;
        }

        private bool IsInfected(int state)
        {
            return (state > 0 && state < maxValue);
        }

        private bool IsIll(int state)
        {
            return state == maxValue;
        }

        private int CalculateNewValueOfHealthy(byte[] data, int x, int y)
        {
            int state, infectedCount = 0, illCount = 0;
            int minX = x - 1, maxX = x + 1;
            int minY = y - 1, maxY = y + 1;
            for (int j = minY; j <= maxY; j++)
            {
                for (int i = minX; i <= maxX; i++)
                {
                    if (j == y && i == x)
                    {
                        continue;
                    }
                    state = GetValue(data, GetRealX(i), GetRealY(j));
                    if (IsInfected(state))
                    {
                        infectedCount++;
                    }
                    else if (IsIll(state))
                    {
                        illCount++;
                    }
                }
            }
            state = (int)((double)infectedCount / constK1 + (double)illCount / constK2);
            if (state > maxValue)
            {
                state = maxValue;
            }

            return state;
        }

        private int CalculateNewValueOfInfected(byte[] data, int x, int y)
        {
            int state, infectedCount = 0, sumState = 0;
            int minX = x - 1, maxX = x + 1;
            int minY = y - 1, maxY = y + 1;
            for (int j = minY; j <= maxY; j++)
            {
                for (int i = minX; i <= maxX; i++)
                {
                    state = GetValue(data, GetRealX(i), GetRealY(j));
                    if (IsInfected(state))
                    {
                        infectedCount++;
                    }
                    sumState += state;
                }
            }
            state = sumState / infectedCount + constG;
            if (state > maxValue)
            {
                state = maxValue;
            }

            return state;
        }

        public void SwitchCurrentData()
        {
            currentData = currentData == data1 ? data2 : data1;
        }

        private int GetIndex(int x, int y)
        {
            return x + width * y;
        }

        private int GetRealX(int x)
        {
            if (x < 0)
            {
                do
                {
                    x += width;
                }
                while (x < 0);
            }
            else if (x >= width)
            {
                do
                {
                    x -= width;
                }
                while (x >= width);
            }

            return x;
        }

        private int GetRealY(int y)
        {
            if (y < 0)
            {
                do
                {
                    y += height;
                }
                while (y < 0);
            }
            else if (y >= height)
            {
                do
                {
                    y -= height;
                }
                while (y >= height);
            }

            return y;
        }

        public byte GetValue(int x, int y)
        {
            return currentData[GetIndex(x, y)];
        }

        private byte GetValue(byte[] data, int x, int y)
        {
            return data[GetIndex(x, y)];
        }

        private void SetValue(byte[] data, int x, int y, byte b)
        {
            data[GetIndex(x, y)] = b;
        }
    }
}
