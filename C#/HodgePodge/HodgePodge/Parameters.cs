﻿namespace HodgePodge
{
    public class Parameters
    {
        private static Parameters instance = new Parameters();

        private Parameters() { }

        public static Parameters Instance
        { get { return instance; } }

        private const int defaultMaxValue = 31;
        private const int defaultConstK1 = 3;
        private const int defaultConstK2 = 3;
        private const int defaultConstG = 6;
		private int maxValue = defaultMaxValue;
		private int constK1 = defaultConstK1;
		private int constK2 = defaultConstK2;
		private int constG = defaultConstG;

        public int MaxValue
		{
			get { return maxValue; }
			set { maxValue = value; }
		}
        public int ConstK1
		{
			get { return constK1;}
			set { constK1 = value; }
		}
        public int ConstK2
		{
			get { return constK2;}
			set { constK2 = value; }
		}
        public int ConstG
		{
			get { return constG;}
			set { constG = value; }
		}
    }
}
