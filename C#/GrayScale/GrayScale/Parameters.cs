﻿using System;

namespace GrayScale
{
    public class Parameters
    {
        private static Parameters instance = new Parameters();

        private Parameters() { }

        public static Parameters Instance
        { get { return instance; } }

		private Orientation orientation = Orientation.Horizontal;

		public Orientation Orientation
		{
			get { return orientation; }
			set { orientation = value; }
		}

        public void ToggleOrientation()
        {
            switch (Orientation)
            {
                case Orientation.Horizontal:
                    Orientation = Orientation.Vertical;
                    break;

                case Orientation.Vertical:
                    Orientation = Orientation.Horizontal;
                    break;

                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}
