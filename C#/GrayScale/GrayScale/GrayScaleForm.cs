﻿using System.Drawing;
using System.Windows.Forms;

namespace GrayScale
{
    public partial class GrayScaleForm : Form
    {
        public GrayScaleForm()
        {
            InitializeComponent();
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            var g = e.Graphics;

            int width = Width;
            int height = Height;
            switch (Parameters.Instance.Orientation)
            {
                case Orientation.Horizontal:
                    for (int i = 0; i < width; i++)
                    {
                        int normalizedValue = GetNormalizedColorValue(i, width);
                        var color = Color.FromArgb(
                                normalizedValue,
                                normalizedValue,
                                normalizedValue);
                        var pen = new Pen(color);
                        g.DrawRectangle(pen, i, 0, 1, height);
                    }
                    break;

                case Orientation.Vertical:
                    for (int i = 0; i < height; i++)
                    {
                        int normalizedValue = GetNormalizedColorValue(i, height);
                        var color = Color.FromArgb(
                                normalizedValue,
                                normalizedValue,
                                normalizedValue);
                        var pen = new Pen(color);
                        g.DrawRectangle(pen, 0, i, width, 1);
                    }
                    break;
            }
        }

        protected override void OnMouseClick(MouseEventArgs e)
        {
            base.OnMouseClick(e);

            if (e.Button == MouseButtons.Right)
            {
                Parameters.Instance.ToggleOrientation();
                Invalidate();
            }
        }

        private static int GetNormalizedColorValue(int value, int maxValue)
        {
            return 255 * value / maxValue;
        }
    }
}
