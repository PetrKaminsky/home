#ifndef __HANOITOWER_H__
#define __HANOITOWER_H__

enum Rod { rodA, rodB, rodC };

void moveTower(int height, Rod from, Rod to);
std::string toString(Rod rod);

#endif

