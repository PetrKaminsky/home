#include <iostream>
#include <sstream>

#include "HanoiTower.h"

using namespace std;

int main(int argc, char *argv[])
{
	int towerHeight = 3;

	if (argc > 1)
	{
		stringstream ss;
		ss << argv[1];
		ss >> towerHeight;
	}

	cout << "Tower height: " << towerHeight << endl;

	moveTower(towerHeight, rodA, rodB);

	return 0;
}

void moveTower(int height, Rod from, Rod to)
{
	if (height >= 1)
	{
		Rod temp;
		if ((from == rodA && to == rodB) ||
			(from == rodB && to == rodA))
		{
			temp = rodC;
		}
		else if ((from == rodA && to == rodC) ||
			(from == rodC && to == rodA))
		{
			temp = rodB;
		}
		else if ((from == rodB && to == rodC) ||
			(from == rodC && to == rodB))
		{
			temp = rodA;
		}
		
		// move higher part to temporal rod
		moveTower(height - 1, from, temp);

		// move lowest disk
		cout << "(" << height << "): " << toString(from) << " --> " << toString(to) << endl;

		// move higher part to destination rod
		moveTower(height - 1, temp, to);
	}
}

string toString(Rod rod)
{
	switch (rod)
	{
		case rodA: return "A";
		case rodB: return "B";
		case rodC: return "C";
		default: throw new string("Unknown rod");
	}
}

