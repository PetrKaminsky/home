#include "stdafx.h"
#include "TilePuzzle.h"
#include "AutoSolver.h"
#include <algorithm>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

/////////////////////////////////////////////////////////////////////////////

CAutoSolver::CAutoSolver()
{
	m_nSize = -1;
	m_pDesk = NULL;
	m_nEmptyRow = -1;
	m_nEmptyCol = -1;
}

CAutoSolver::~CAutoSolver()
{
	FreeDesk();
}

void CAutoSolver::InitFromDesk(const CDesk &Desk)
{
	m_nSize = Desk.GetSize();
	InitDesk(Desk.GetDesk());
	FindTile(
		CDesk::s_nEmpty,
		m_nEmptyRow,
		m_nEmptyCol);
}

void CAutoSolver::Reset()
{
	m_nSize = -1;
	FreeDesk();
	m_AutoMoveQueue.clear();
	m_nEmptyRow = -1;
	m_nEmptyCol = -1;
}

void CAutoSolver::SolveStep()
{
	int i, n = m_nSize - 1;
	for (i = 0; i < n; i++)
	{
		if (!IsRowSolved(i))
		{
			SolveRow(i);
			return;
		}
		if (!IsColSolved(i))
		{
			SolveCol(i);
			return;
		}
	}
}

bool CAutoSolver::GetMove(int &nMove)
{
	if (m_AutoMoveQueue.empty())
		return false;

	nMove = m_AutoMoveQueue.front();
	m_AutoMoveQueue.pop_front();

	return true;
}

bool CAutoSolver::IsRowSolved(int nRow)
{
	int nCol;
	for (nCol = nRow; nCol < m_nSize; nCol++)
	{
		int nIndex = GetIndex(nRow, nCol);
		if (m_pDesk[nIndex] != (nIndex + 1))
			return false;
	}

	return true;
}

bool CAutoSolver::IsColSolved(int nCol)
{
	int nRow;
	for (nRow = nCol; nRow < m_nSize; nRow++)
	{
		int nIndex = GetIndex(nRow, nCol);
		if (m_pDesk[nIndex] != (nIndex + 1))
			return false;
	}

	return true;
}

void CAutoSolver::SolveRow(int nRow)
{
	int nCol;
	for (nCol = nRow; nCol < m_nSize; nCol++)
	{
		int nIndex = GetIndex(nRow, nCol);
		if (m_pDesk[nIndex] != (nIndex + 1))
		{
			SolveTile_Row(nRow, nCol);
			return;
		}
	}
}

void CAutoSolver::SolveCol(int nCol)
{
	int nRow;
	for (nRow = nCol; nRow < m_nSize; nRow++)
	{
		int nIndex = GetIndex(nRow, nCol);
		if (m_pDesk[nIndex] != (nIndex + 1))
		{
			SolveTile_Col(nRow, nCol);
			return;
		}
	}
}

int CAutoSolver::GetIndex(int nRow, int nCol)
{
	return nCol + nRow * m_nSize;
}

void CAutoSolver::SolveTile_Row(int nRow, int nCol)
{
	int nDesiredTile = GetIndex(nRow, nCol) + 1;
	int nStartRow, nStartCol;
	FindTile(nDesiredTile, nStartRow, nStartCol);
	if (nCol == (m_nSize - 1))
	{
		// last tile
		if ((m_pDesk[GetIndex(nRow, nCol)] == CDesk::s_nEmpty) &&
			(m_pDesk[GetIndex(nRow + 1, nCol)] == nDesiredTile))
		{
			// up
			GenerUp();
		}
		else
		{
			GenerTileMove_Row(
				nStartRow,
				nStartCol,
				nRow + 1,
				nCol);
			GenerEmptyMove_Row(
				nRow + 1,
				nCol,
				nRow + 2,
				nCol);
			GenerDown();
			GenerDown();
			GenerRight();
			GenerUp();
			GenerLeft();
			GenerUp();
			GenerRight();
			GenerDown();
			GenerDown();
			GenerLeft();
			GenerUp();
		}
	}
	else
	{
		GenerTileMove_Row(
			nStartRow,
			nStartCol,
			nRow,
			nCol);
	}
}

void CAutoSolver::SolveTile_Col(int nRow, int nCol)
{
	int nDesiredTile = GetIndex(nRow, nCol) + 1;
	int nStartRow, nStartCol;
	FindTile(nDesiredTile, nStartRow, nStartCol);
	if (nRow == (m_nSize - 1))
	{
		// last tile
		if ((m_pDesk[GetIndex(nRow, nCol)] == CDesk::s_nEmpty) &&
			(m_pDesk[GetIndex(nRow, nCol + 1)] == nDesiredTile))
		{
			// left
			GenerLeft();
		}
		else
		{
			GenerTileMove_Col(
				nStartRow,
				nStartCol,
				nRow,
				nCol + 1);
			GenerEmptyMove_Col(
				nRow,
				nCol + 1,
				nRow,
				nCol + 2);
			GenerRight();
			GenerRight();
			GenerDown();
			GenerLeft();
			GenerUp();
			GenerLeft();
			GenerDown();
			GenerRight();
			GenerRight();
			GenerUp();
			GenerLeft();
		}
	}
	else
	{
		GenerTileMove_Col(
			nStartRow,
			nStartCol,
			nRow,
			nCol);
	}
}

void CAutoSolver::FindTile(
	int nTile,
	int &nRow,
	int &nCol)
{
	for (nRow = 0; nRow < m_nSize; nRow++)
		for (nCol = 0; nCol < m_nSize; nCol++)
			if (m_pDesk[GetIndex(nRow, nCol)] == nTile)
				return;
}

void CAutoSolver::GenerTileMoveByOne_Row(
	int nStartRow,
	int nStartCol,
	int nEndRow,
	int nEndCol)
{
	if ((nStartRow == nEndRow) &&
		(nStartCol == nEndCol))
		return;
	GenerEmptyMove_Row(
		nStartRow,
		nStartCol,
		nEndRow,
		nEndCol);
	if (nStartRow == nEndRow)
	{
		if (nStartCol < nEndCol)
		{
			// right
			GenerRight();
		}
		else if (nStartCol > nEndCol)
		{
			// left
			GenerLeft();
		}
	}
	else
	{
		if (nStartRow < nEndRow)
		{
			// down
			GenerDown();
		}
		else if (nStartRow > nEndRow)
		{
			// up
			GenerUp();
		}
	}
}

void CAutoSolver::GenerEmptyMove_Row(
	int nTileRow,
	int nTileCol,
	int nEndRow,
	int nEndCol)
{
	int nCurrRow = m_nEmptyRow, nCurrCol = m_nEmptyCol;
	if (nCurrRow < nEndRow)
	{
		// empty down
		while (nCurrRow != nEndRow)
		{
			if (((nCurrRow + 1) == nTileRow) && (nCurrCol == nTileCol))
				break;
			// empty down, i.e. tile up
			GenerUp();
			nCurrRow++;
		}
		if (nCurrRow == nEndRow)
			while (nCurrCol != nEndCol)
				if (nCurrCol < nEndCol)
				{
					// empty right
					if ((nCurrRow == nTileRow) && ((nCurrCol + 1) == nTileCol))
						break;
					// empty right, i.e. tile left
					GenerLeft();
					nCurrCol++;
				}
				else
				{
					// empty left
					if ((nCurrRow == nTileRow) && ((nCurrCol - 1) == nTileCol))
						break;
					// empty left, i.e. tile right
					GenerRight();
					nCurrCol--;
				}
	}
	else
	{
		while (nCurrCol != nEndCol)
			if (nCurrCol < nEndCol)
			{
				// empty right
				if ((nCurrRow == nTileRow) && ((nCurrCol + 1) == nTileCol))
					break;
				// empty right, i.e. tile left
				GenerLeft();
				nCurrCol++;
			}
			else
			{
				// empty left
				if ((nCurrRow == nTileRow) && ((nCurrCol - 1) == nTileCol))
					break;
				// empty left, i.e. tile right
				GenerRight();
				nCurrCol--;
			}
		if (nCurrCol == nEndCol)
			// empty up
			while (nCurrRow != nEndRow)
			{
				if (((nCurrRow - 1) == nTileRow) && (nCurrCol == nTileCol))
					break;
				// empty up, i.e. tile down
				GenerDown();
				nCurrRow--;
			}
	}
	MoveEmptyAround(
		nTileRow,
		nTileCol,
		nCurrRow,
		nCurrCol,
		nEndRow,
		nEndCol);
}

void CAutoSolver::GenerUp()
{
	ShiftUp();
	if (!m_AutoMoveQueue.empty())
		if (m_AutoMoveQueue.back() == AUTO_DOWN)
		{
			m_AutoMoveQueue.pop_back();
			return;
		}
	m_AutoMoveQueue.push_back(AUTO_UP);
}

void CAutoSolver::GenerDown()
{
	ShiftDown();
	if (!m_AutoMoveQueue.empty())
		if (m_AutoMoveQueue.back() == AUTO_UP)
		{
			m_AutoMoveQueue.pop_back();
			return;
		}
	m_AutoMoveQueue.push_back(AUTO_DOWN);
}

void CAutoSolver::GenerLeft()
{
	ShiftLeft();
	if (!m_AutoMoveQueue.empty())
		if (m_AutoMoveQueue.back() == AUTO_RIGHT)
		{
			m_AutoMoveQueue.pop_back();
			return;
		}
	m_AutoMoveQueue.push_back(AUTO_LEFT);
}

void CAutoSolver::GenerRight()
{
	ShiftRight();
	if (!m_AutoMoveQueue.empty())
		if (m_AutoMoveQueue.back() == AUTO_LEFT)
		{
			m_AutoMoveQueue.pop_back();
			return;
		}
	m_AutoMoveQueue.push_back(AUTO_RIGHT);
}

void CAutoSolver::MoveEmptyAround(
	int nTileRow,
	int nTileCol,
	int nStartRow,
	int nStartCol,
	int nEndRow,
	int nEndCol)
{
	if (nStartRow < nTileRow)
	{
		// empty is up
		if (nEndRow < nTileRow)
		{
			// should be up, do nothing
		}
		else if (nEndRow > nTileRow)
		{
			// should be down
			MoveEmptyAroundUpDown(nTileRow, nTileCol);
		}
		else if (nEndCol < nTileCol)
		{
			// should be left
			MoveEmptyAroundUpLeft(nTileRow, nTileCol);
		}
		else if (nEndCol > nTileCol)
		{
			// should be right
			MoveEmptyAroundUpRight(nTileRow, nTileCol);
		}
	}
	else if (nStartRow > nTileRow)
	{
		// empty is down
		if (nEndRow < nTileRow)
		{
			// should be up
			MoveEmptyAroundDownUp(nTileRow, nTileCol);
		}
		else if (nEndRow > nTileRow)
		{
			// should be down, do nothing
		}
		else if (nEndCol < nTileCol)
		{
			// should be left
			MoveEmptyAroundDownLeft(nTileRow, nTileCol);
		}
		else if (nEndCol > nTileCol)
		{
			// should be right
			MoveEmptyAroundDownRight(nTileRow, nTileCol);
		}
	}
	else if (nStartCol < nTileCol)
	{
		// empty is left
		if (nEndRow < nTileRow)
		{
			// should be up
			MoveEmptyAroundLeftUp(nTileRow, nTileCol);
		}
		else if (nEndRow > nTileRow)
		{
			// should be down
			MoveEmptyAroundLeftDown(nTileRow, nTileCol);
		}
		else if (nEndCol < nTileCol)
		{
			// should be left, do nothing
		}
		else if (nEndCol > nTileCol)
		{
			// should be right
			MoveEmptyAroundLeftRight(nTileRow, nTileCol);
		}
	}
	else if (nStartCol > nTileCol)
	{
		// empty is right
		if (nEndRow < nTileRow)
		{
			// should be up
			MoveEmptyAroundRightUp(nTileRow, nTileCol);
		}
		else if (nEndRow > nTileRow)
		{
			// should be down
			MoveEmptyAroundRightDown(nTileRow, nTileCol);
		}
		else if (nEndCol < nTileCol)
		{
			// should be left
			MoveEmptyAroundRightLeft(nTileRow, nTileCol);
		}
		else if (nEndCol > nTileCol)
		{
			// should be right, do nothing
		}
	}
}

void CAutoSolver::MoveEmptyAroundUpDown(int nTileRow, int nTileCol)
{
	if (nTileCol == (m_nSize - 1))
	{
		GenerRight();
		GenerUp();
		GenerUp();
		GenerLeft();
	}
	else
	{
		GenerLeft();
		GenerUp();
		GenerUp();
		GenerRight();
	}
}

void CAutoSolver::MoveEmptyAroundUpLeft(int nTileRow, int nTileCol)
{
	if ((nTileCol == (m_nSize - 1)) ||
		(nTileRow == (m_nSize - 1)))
	{
		GenerRight();
		GenerUp();
	}
	else
	{
		GenerLeft();
		GenerUp();
		GenerUp();
		GenerRight();
		GenerRight();
		GenerDown();
	}
}

void CAutoSolver::MoveEmptyAroundUpRight(int nTileRow, int nTileCol)
{
	GenerLeft();
	GenerUp();
}

void CAutoSolver::MoveEmptyAroundDownUp(int nTileRow, int nTileCol)
{
	if (nTileCol == (m_nSize - 1))
	{
		GenerRight();
		GenerDown();
		GenerDown();
		GenerLeft();
	}
	else
	{
		GenerLeft();
		GenerDown();
		GenerDown();
		GenerRight();
	}
}

void CAutoSolver::MoveEmptyAroundDownLeft(int nTileRow, int nTileCol)
{
	GenerRight();
	GenerDown();
}

void CAutoSolver::MoveEmptyAroundDownRight(int nTileRow, int nTileCol)
{
	GenerLeft();
	GenerDown();
}

void CAutoSolver::MoveEmptyAroundLeftUp(int nTileRow, int nTileCol)
{
	if ((nTileCol == (m_nSize - 1)) ||
		(nTileRow == (m_nSize - 1)))
	{
		GenerDown();
		GenerLeft();
	}
	else
	{
		GenerUp();
		GenerLeft();
		GenerLeft();
		GenerDown();
		GenerDown();
		GenerRight();
	}
}

void CAutoSolver::MoveEmptyAroundLeftDown(int nTileRow, int nTileCol)
{
	GenerUp();
	GenerLeft();
}

void CAutoSolver::MoveEmptyAroundLeftRight(int nTileRow, int nTileCol)
{
	if (nTileRow == (m_nSize - 1))
	{
		GenerDown();
		GenerLeft();
		GenerLeft();
		GenerUp();
	}
	else
	{
		GenerUp();
		GenerLeft();
		GenerLeft();
		GenerDown();
	}
}

void CAutoSolver::MoveEmptyAroundRightUp(int nTileRow, int nTileCol)
{
	GenerDown();
	GenerRight();
}

void CAutoSolver::MoveEmptyAroundRightDown(int nTileRow, int nTileCol)
{
	GenerUp();
	GenerRight();
}

void CAutoSolver::MoveEmptyAroundRightLeft(int nTileRow, int nTileCol)
{
	if (nTileRow == (m_nSize - 1))
	{
		GenerDown();
		GenerRight();
		GenerRight();
		GenerUp();
	}
	else
	{
		GenerUp();
		GenerRight();
		GenerRight();
		GenerDown();
	}
}

void CAutoSolver::ShiftUp()
{
	int nRow = m_nEmptyRow, nCol = m_nEmptyCol;
	int nFromRow = nRow + 1;
	int nFromCol = nCol;
	int nToRow = nRow;
	int nToCol = nCol;
	int nFromInd = GetIndex(nFromRow, nFromCol);
	int nToInd = GetIndex(nToRow, nToCol);
	std::swap(m_pDesk[nFromInd], m_pDesk[nToInd]);
	m_nEmptyRow = nFromRow;
	m_nEmptyCol = nFromCol;
}

void CAutoSolver::ShiftDown()
{
	int nRow = m_nEmptyRow, nCol = m_nEmptyCol;
	int nFromRow = nRow - 1;
	int nFromCol = nCol;
	int nToRow = nRow;
	int nToCol = nCol;
	int nFromInd = GetIndex(nFromRow, nFromCol);
	int nToInd = GetIndex(nToRow, nToCol);
	std::swap(m_pDesk[nFromInd], m_pDesk[nToInd]);
	m_nEmptyRow = nFromRow;
	m_nEmptyCol = nFromCol;
}

void CAutoSolver::ShiftLeft()
{
	int nRow = m_nEmptyRow, nCol = m_nEmptyCol;
	int nFromRow = nRow;
	int nFromCol = nCol + 1;
	int nToRow = nRow;
	int nToCol = nCol;
	int nFromInd = GetIndex(nFromRow, nFromCol);
	int nToInd = GetIndex(nToRow, nToCol);
	std::swap(m_pDesk[nFromInd], m_pDesk[nToInd]);
	m_nEmptyRow = nFromRow;
	m_nEmptyCol = nFromCol;
}

void CAutoSolver::ShiftRight()
{
	int nRow = m_nEmptyRow, nCol = m_nEmptyCol;
	int nFromRow = nRow;
	int nFromCol = nCol - 1;
	int nToRow = nRow;
	int nToCol = nCol;
	int nFromInd = GetIndex(nFromRow, nFromCol);
	int nToInd = GetIndex(nToRow, nToCol);
	std::swap(m_pDesk[nFromInd], m_pDesk[nToInd]);
	m_nEmptyRow = nFromRow;
	m_nEmptyCol = nFromCol;
}

void CAutoSolver::FreeDesk()
{
	if (m_pDesk != NULL)
	{
		delete []m_pDesk;
		m_pDesk = NULL;
	}
}

void CAutoSolver::InitDesk(const int *pDesk)
{
	FreeDesk();
	int nTiles = m_nSize * m_nSize;
	m_pDesk = new int[nTiles];
	CopyMemory(
		m_pDesk,
		pDesk,
		nTiles * sizeof(*m_pDesk));
}

void CAutoSolver::GenerTileMove_Row(
	int nStartRow,
	int nStartCol,
	int nEndRow,
	int nEndCol)
{
	GenerTileMoveLeftRight_Row(
		nStartRow,
		nStartCol,
		nEndCol);
	GenerTileMoveUpDown_Row(
		nStartRow,
		nEndRow,
		nEndCol);
}

void CAutoSolver::GenerTileMoveLeftRight_Row(
	int nRow,
	int nStartCol,
	int nEndCol)
{
	while (nEndCol != nStartCol)
	{
		int nCurrCol = nStartCol;
		if (nCurrCol < nEndCol)
			nCurrCol++;
		else
			nCurrCol--;
		GenerTileMoveByOne_Row(
			nRow,
			nStartCol,
			nRow,
			nCurrCol);
		nStartCol = nCurrCol;
	}
}

void CAutoSolver::GenerTileMoveUpDown_Row(
	int nStartRow,
	int nEndRow,
	int nCol)
{
	while (nEndRow != nStartRow)
	{
		int nCurrRow = nStartRow;
		if (nCurrRow < nEndRow)
			nCurrRow++;
		else
			nCurrRow--;
		GenerTileMoveByOne_Row(
			nStartRow,
			nCol,
			nCurrRow,
			nCol);
		nStartRow = nCurrRow;
	}
}

void CAutoSolver::GenerTileMove_Col(
	int nStartRow,
	int nStartCol,
	int nEndRow,
	int nEndCol)
{
	GenerTileMoveUpDown_Col(
		nStartRow,
		nEndRow,
		nStartCol);
	GenerTileMoveLeftRight_Col(
		nEndRow,
		nStartCol,
		nEndCol);
}

void CAutoSolver::GenerTileMoveLeftRight_Col(
	int nRow,
	int nStartCol,
	int nEndCol)
{
	while (nEndCol != nStartCol)
	{
		int nCurrCol = nStartCol;
		if (nCurrCol < nEndCol)
			nCurrCol++;
		else
			nCurrCol--;
		GenerTileMoveByOne_Col(
			nRow,
			nStartCol,
			nRow,
			nCurrCol);
		nStartCol = nCurrCol;
	}
}

void CAutoSolver::GenerTileMoveUpDown_Col(
	int nStartRow,
	int nEndRow,
	int nCol)
{
	while (nEndRow != nStartRow)
	{
		int nCurrRow = nStartRow;
		if (nCurrRow < nEndRow)
			nCurrRow++;
		else
			nCurrRow--;
		GenerTileMoveByOne_Col(
			nStartRow,
			nCol,
			nCurrRow,
			nCol);
		nStartRow = nCurrRow;
	}
}

void CAutoSolver::GenerTileMoveByOne_Col(
	int nStartRow,
	int nStartCol,
	int nEndRow,
	int nEndCol)
{
	if ((nStartRow == nEndRow) &&
		(nStartCol == nEndCol))
		return;
	GenerEmptyMove_Col(
		nStartRow,
		nStartCol,
		nEndRow,
		nEndCol);
	if (nStartRow == nEndRow)
	{
		if (nStartCol < nEndCol)
		{
			// right
			GenerRight();
		}
		else if (nStartCol > nEndCol)
		{
			// left
			GenerLeft();
		}
	}
	else
	{
		if (nStartRow < nEndRow)
		{
			// down
			GenerDown();
		}
		else if (nStartRow > nEndRow)
		{
			// up
			GenerUp();
		}
	}
}

void CAutoSolver::GenerEmptyMove_Col(
	int nTileRow,
	int nTileCol,
	int nEndRow,
	int nEndCol)
{
	int nCurrRow = m_nEmptyRow, nCurrCol = m_nEmptyCol;
	if (nCurrCol < nEndCol)
	{
		// empty right
		while (nCurrCol != nEndCol)
		{
			if ((nCurrRow == nTileRow) && ((nCurrCol + 1) == nTileCol))
				break;
			// empty right, i.e. tile left
			GenerLeft();
			nCurrCol++;
		}
		if (nCurrCol == nEndCol)
			while (nCurrRow != nEndRow)
				if (nCurrRow < nEndRow)
				{
					// empty down
					if (((nCurrRow + 1) == nTileRow) && (nCurrCol == nTileCol))
						break;
					// empty down, i.e. tile up
					GenerUp();
					nCurrRow++;
				}
				else
				{
					// empty up
					if (((nCurrRow - 1) == nTileRow) && (nCurrCol == nTileCol))
						break;
					// empty up, i.e. tile down
					GenerDown();
					nCurrRow--;
				}
	}
	else
	{
		while (nCurrRow != nEndRow)
			if (nCurrRow < nEndRow)
			{
				// empty down
				if (((nCurrRow + 1) == nTileRow) && (nCurrCol == nTileCol))
					break;
				// empty down, i.e. tile up
				GenerUp();
				nCurrRow++;
			}
			else
			{
				// empty up
				if (((nCurrRow - 1) == nTileRow) && (nCurrCol == nTileCol))
					break;
				// empty up, i.e. tile down
				GenerDown();
				nCurrRow--;
			}
		if (nCurrRow == nEndRow)
			// empty left
			while (nCurrCol != nEndCol)
			{
				if ((nCurrRow == nTileRow) && ((nCurrCol - 1) == nTileCol))
					break;
				// empty left, i.e. tile right
				GenerRight();
				nCurrCol--;
			}
	}
	MoveEmptyAround(
		nTileRow,
		nTileCol,
		nCurrRow,
		nCurrCol,
		nEndRow,
		nEndCol);
}

bool CAutoSolver::IsMove()
{
	return !m_AutoMoveQueue.empty();
}
