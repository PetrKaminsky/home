#include "stdafx.h"
#include "TilePuzzle.h"
#include "MainFrm.h"
#include "Globals.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTilePuzzleApp

BEGIN_MESSAGE_MAP(CTilePuzzleApp, CWinApp)
	//{{AFX_MSG_MAP(CTilePuzzleApp)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// The one and only CTilePuzzleApp object

CTilePuzzleApp theApp;
CRegPropertySerializer *CTilePuzzleApp::s_pPropertySerializer = NULL;

/////////////////////////////////////////////////////////////////////////////
// CTilePuzzleApp initialization

BOOL CTilePuzzleApp::InitInstance()
{
	SYSTEMTIME st;
	::GetSystemTime(&st);
	FILETIME ft;
	::SystemTimeToFileTime(&st, &ft);
	srand(ft.dwLowDateTime);

	CGlobals::SipOff();

	s_pPropertySerializer = new CRegPropertySerializer(_T("HKEY_LOCAL_MACHINE\\Software\\Kamen\\TilePuzzle"));
	CMainFrame* pFrame = new CMainFrame;
	m_pMainWnd = pFrame;
	pFrame->LoadFrame(IDR_MAINFRAME);
	pFrame->ShowWindow(m_nCmdShow);
	pFrame->UpdateWindow();

	return TRUE;
}

int CTilePuzzleApp::ExitInstance()
{
	delete s_pPropertySerializer;

	return CWinApp::ExitInstance();
}
