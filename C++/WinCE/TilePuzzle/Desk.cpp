#include "stdafx.h"
#include "TilePuzzle.h"
#include "Desk.h"
#include <vector>
#include <algorithm>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////

int CDesk::s_nEmpty = -1;

CDesk::CDesk()
:	m_LastSize(CTilePuzzleApp::s_pPropertySerializer, _T("Size"), 4),
	m_LastTiles(CTilePuzzleApp::s_pPropertySerializer, _T("Tiles"), _T(""))
{
	m_nSize = -1;
	m_pDesk = NULL;
	m_nEmptyX = -1;
	m_nEmptyY = -1;
}

CDesk::~CDesk()
{
	CleanUp();
}

void CDesk::Init(int nSize)
{
	CleanUp();

	m_nSize = nSize;
	int nTiles = m_nSize * m_nSize;
	m_pDesk = new int[nTiles];
	InitFill();
	BackupState();
}

void CDesk::InitFill()
{
	int i, nCount = (m_nSize * m_nSize) - 1;
	for (i = 0; i < nCount; i++)
		m_pDesk[i] = i + 1;
	m_pDesk[nCount] = s_nEmpty;
	m_nEmptyX = m_nSize - 1;
	m_nEmptyY = m_nSize - 1;
}

void CDesk::CleanUp()
{
	if (m_pDesk != NULL)
		delete []m_pDesk;
	m_nSize = -1;
}

void CDesk::GetTile(
	int nX,
	int nY,
	int &nTile,
	bool &bPosOK)
{
	int nPos = GetTilePos(nX, nY);
	nTile = m_pDesk[nPos];
	bPosOK = IsTileSolved(nTile, nPos);
}

void CDesk::ShiftUp()
{
	int nX = m_nEmptyX, nY = m_nEmptyY;
	if (nY >= (m_nSize - 1))
		return;
	int nFromX = nX;
	int nFromY = nY + 1;
	int nToX = nX;
	int nToY = nY;
	int nFromPos = GetTilePos(nFromX, nFromY);
	int nToPos = GetTilePos(nToX, nToY);
	m_MoveQueue.push_back(
		CMove(
			m_pDesk[nFromPos],
			IsTileSolved(m_pDesk[nFromPos], nToPos),
			nFromX,
			nFromY,
			nToX,
			nToY));
	std::swap(m_pDesk[nFromPos], m_pDesk[nToPos]);
	m_nEmptyX = nFromX;
	m_nEmptyY = nFromY;
	BackupState();
}

void CDesk::ShiftDown()
{
	int nX = m_nEmptyX, nY = m_nEmptyY;
	if (nY < 1)
		return;
	int nFromX = nX;
	int nFromY = nY - 1;
	int nToX = nX;
	int nToY = nY;
	int nFromPos = GetTilePos(nFromX, nFromY);
	int nToPos = GetTilePos(nToX, nToY);
	m_MoveQueue.push_back(
		CMove(
			m_pDesk[nFromPos],
			IsTileSolved(m_pDesk[nFromPos], nToPos),
			nFromX,
			nFromY,
			nToX,
			nToY));
	std::swap(m_pDesk[nFromPos], m_pDesk[nToPos]);
	m_nEmptyX = nFromX;
	m_nEmptyY = nFromY;
	BackupState();
}

void CDesk::ShiftLeft()
{
	int nX = m_nEmptyX, nY = m_nEmptyY;
	if (nX >= (m_nSize - 1))
		return;
	int nFromX = nX + 1;
	int nFromY = nY;
	int nToX = nX;
	int nToY = nY;
	int nFromPos = GetTilePos(nFromX, nFromY);
	int nToPos = GetTilePos(nToX, nToY);
	m_MoveQueue.push_back(
		CMove(
			m_pDesk[nFromPos],
			IsTileSolved(m_pDesk[nFromPos], nToPos),
			nFromX,
			nFromY,
			nToX,
			nToY));
	std::swap(m_pDesk[nFromPos], m_pDesk[nToPos]);
	m_nEmptyX = nFromX;
	m_nEmptyY = nFromY;
	BackupState();
}

void CDesk::ShiftRight()
{
	int nX = m_nEmptyX, nY = m_nEmptyY;
	if (nX < 1)
		return;
	int nFromX = nX - 1;
	int nFromY = nY;
	int nToX = nX;
	int nToY = nY;
	int nFromPos = GetTilePos(nFromX, nFromY);
	int nToPos = GetTilePos(nToX, nToY);
	m_MoveQueue.push_back(
		CMove(
			m_pDesk[nFromPos],
			IsTileSolved(m_pDesk[nFromPos], nToPos),
			nFromX,
			nFromY,
			nToX,
			nToY));
	std::swap(m_pDesk[nFromPos], m_pDesk[nToPos]);
	m_nEmptyX = nFromX;
	m_nEmptyY = nFromY;
	BackupState();
}

void CDesk::Shuffle()
{
	InitFill();
	typedef std::vector<int> TIntVector;
	typedef TIntVector::iterator TIntVectorIterator;
	int i, nCount = (m_nSize * m_nSize) - 1;
	TIntVector SrcIdx;
	for (i = 0; i < nCount; i++)
		SrcIdx.push_back(i);
	TIntVector DstIdx(SrcIdx);
	std::random_shuffle(DstIdx.begin(), DstIdx.end());
	TIntVectorIterator it1 = SrcIdx.begin(), it2 = DstIdx.begin();
	int nSwapCount = 0;
	while (it1 != SrcIdx.end())
	{
		if (*it1 != *it2)
		{
			std::swap(m_pDesk[*it1], m_pDesk[*it2]);
			nSwapCount++;
		}
		it1++;
		it2++;
	}
	// odd swap count
	if ((nSwapCount % 2) == 1)
		std::swap(m_pDesk[0], m_pDesk[nCount - 1]);
	BackupState();
}

bool CDesk::IsSolved()
{
	int i, nCount = (m_nSize * m_nSize) - 1;
	for (i = 0; i < nCount; i++)
		if (m_pDesk[i] != (i + 1))
			return false;

	return true;
}

bool CDesk::GetMove(CMove &Move)
{
	if (m_MoveQueue.empty())
		return false;

	Move = m_MoveQueue.front();
	m_MoveQueue.pop_front();

	return true;
}

int CDesk::GetTilePos(int nX, int nY)
{
	return nX + nY * m_nSize;
}

bool CDesk::IsTileSolved(int nTile, int nPos)
{
	return (nTile != s_nEmpty) && (nTile == (nPos + 1));
}

void CDesk::ClickTile(int nX, int nY)
{
	int nDestX = m_nEmptyX;
	int nDestY = m_nEmptyY;
	if ((nDestX == nX) ||
		(nDestY == nY))
	{
		if ((nDestX == nX) &&
			(nDestY == nY))
			return;
		while (nDestX != nX)
		{
			if (nX > nDestX)
			{
				ShiftLeft();
				nX--;
			}
			else
			{
				ShiftRight();
				nX++;
			}
		}
		while (nDestY != nY)
		{
			if (nY > nDestY)
			{
				ShiftUp();
				nY--;
			}
			else
			{
				ShiftDown();
				nY++;
			}
		}
	}
}

bool CDesk::RestoreState()
{
	m_nSize = m_LastSize.GetValue();
	CString sTiles = m_LastTiles.GetValue();
	return DeserializeTiles(sTiles);
}

void CDesk::BackupState()
{
	m_LastSize.SetValue(m_nSize);
	CString sTiles;
	SerializeTiles(sTiles);
	m_LastTiles.SetValue(sTiles);
}

void CDesk::SerializeTiles(CString &sTiles)
{
	sTiles.Empty();
	int i, n = m_nSize * m_nSize;
	for (i = 0; i < n; i++)
	{
		CString sTile;
		sTile.Format(_T("%d"), m_pDesk[i]);
		if (i != 0)
			sTiles += _T(',');
		sTiles += sTile;
	}
}

bool CDesk::DeserializeTiles(LPCTSTR szTiles)
{
	CString sTiles(szTiles), sTile;
	int nTiles = m_nSize * m_nSize, i = 0, nInd, nTile;
	m_pDesk = new int[nTiles];
	while (true)
	{
		if (i == nTiles)
			break;
		if (sTiles.IsEmpty())
			break;
		nInd = sTiles.Find(_T(','));
		if (nInd != -1)
		{
			sTile = sTiles.Left(nInd);
			sTiles = sTiles.Mid(nInd + 1);
		}
		else
		{
			sTile = sTiles;
			sTiles.Empty();
		}
		if (_stscanf(sTile, _T("%d"), &nTile) != 1)
			return false;
		m_pDesk[i] = nTile;
		if (nTile == s_nEmpty)
		{
			m_nEmptyX = i % m_nSize;
			m_nEmptyY = i / m_nSize;
		}
		i++;
	}
	return (i == nTiles) && (sTiles.IsEmpty());
}

//////////////////////////////////////////////////////////////////////

CMove::CMove()
{
	m_nTile = CDesk::s_nEmpty;
	m_bTileSolved = false;
	m_nFromX = CDesk::s_nEmpty;
	m_nFromY = CDesk::s_nEmpty;
	m_nToX = CDesk::s_nEmpty;
	m_nToY = CDesk::s_nEmpty;
}

CMove::CMove(
	int nTile,
	bool bTileSolved,
	int nFromX,
	int nFromY,
	int nToX,
	int nToY)
{
	m_nTile = nTile;
	m_bTileSolved = bTileSolved;
	m_nFromX = nFromX;
	m_nFromY = nFromY;
	m_nToX = nToX;
	m_nToY = nToY;
}
