#if !defined(AFX_AUTOSOLVER_H__A4691702_B8E8_49B0_B263_7D57FD0504AE__INCLUDED_)
#define AFX_AUTOSOLVER_H__A4691702_B8E8_49B0_B263_7D57FD0504AE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//////////////////////////////////////////////////////////////////////

#include "Desk.h"

class CAutoSolver
{
public:
	CAutoSolver();
	~CAutoSolver();

	void InitFromDesk(const CDesk &Desk);
	void Reset();
	bool GetMove(int &nMove);
	void SolveStep();
	bool IsMove();

	enum { AUTO_UP, AUTO_DOWN, AUTO_LEFT, AUTO_RIGHT };

private:
	int m_nSize;
	int *m_pDesk;
	std::deque<int> m_AutoMoveQueue;
	int m_nEmptyRow;
	int m_nEmptyCol;

	bool IsRowSolved(int nRow);
	bool IsColSolved(int nCol);
	void SolveRow(int nRow);
	void SolveCol(int nCol);
	int GetIndex(int nRow, int nCol);
	void SolveTile_Row(int nRow, int nCol);
	void SolveTile_Col(int nRow, int nCol);
	void FindTile(
		int nTile,
		int &nRow,
		int &nCol);
	void GenerTileMoveByOne_Row(
		int nStartRow,
		int nStartCol,
		int nEndRow,
		int nEndCol);
	void GenerEmptyMove_Row(
		int nTileRow,
		int nTileCol,
		int nEndRow,
		int nEndCol);
	void GenerUp();
	void GenerDown();
	void GenerLeft();
	void GenerRight();
	void MoveEmptyAround(
		int nTileRow,
		int nTileCol,
		int nStartRow,
		int nStartCol,
		int nEndRow,
		int nEndCol);
	void MoveEmptyAroundUpDown(int nTileRow, int nTileCol);
	void MoveEmptyAroundUpLeft(int nTileRow, int nTileCol);
	void MoveEmptyAroundUpRight(int nTileRow, int nTileCol);
	void MoveEmptyAroundDownUp(int nTileRow, int nTileCol);
	void MoveEmptyAroundDownLeft(int nTileRow, int nTileCol);
	void MoveEmptyAroundDownRight(int nTileRow, int nTileCol);
	void MoveEmptyAroundLeftUp(int nTileRow, int nTileCol);
	void MoveEmptyAroundLeftDown(int nTileRow, int nTileCol);
	void MoveEmptyAroundLeftRight(int nTileRow, int nTileCol);
	void MoveEmptyAroundRightUp(int nTileRow, int nTileCol);
	void MoveEmptyAroundRightDown(int nTileRow, int nTileCol);
	void MoveEmptyAroundRightLeft(int nTileRow, int nTileCol);
	void ShiftUp();
	void ShiftDown();
	void ShiftLeft();
	void ShiftRight();
	void FreeDesk();
	void InitDesk(const int *pDesk);
	void GenerTileMove_Row(
		int nStartRow,
		int nStartCol,
		int nEndRow,
		int nEndCol);
	void GenerTileMoveLeftRight_Row(
		int nRow,
		int nStartCol,
		int nEndCol);
	void GenerTileMoveUpDown_Row(
		int nStartRow,
		int nEndRow,
		int nCol);
	void GenerTileMove_Col(
		int nStartRow,
		int nStartCol,
		int nEndRow,
		int nEndCol);
	void GenerTileMoveLeftRight_Col(
		int nRow,
		int nStartCol,
		int nEndCol);
	void GenerTileMoveUpDown_Col(
		int nStartRow,
		int nEndRow,
		int nCol);
	void GenerTileMoveByOne_Col(
		int nStartRow,
		int nStartCol,
		int nEndRow,
		int nEndCol);
	void GenerEmptyMove_Col(
		int nTileRow,
		int nTileCol,
		int nEndRow,
		int nEndCol);
};

#endif // !defined(AFX_AUTOSOLVER_H__A4691702_B8E8_49B0_B263_7D57FD0504AE__INCLUDED_)
