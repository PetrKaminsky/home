#if !defined(AFX_TILEPUZZLE_H__FCEDB356_5138_4B1C_A0C7_EE99ECF7B657__INCLUDED_)
#define AFX_TILEPUZZLE_H__FCEDB356_5138_4B1C_A0C7_EE99ECF7B657__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols
#include "Property.h"

/////////////////////////////////////////////////////////////////////////////

class CTilePuzzleApp : public CWinApp
{
public:
	static CRegPropertySerializer *s_pPropertySerializer;

	//{{AFX_VIRTUAL(CTilePuzzleApp)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	//}}AFX_VIRTUAL

	//{{AFX_MSG(CTilePuzzleApp)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////
//{{AFX_INSERT_LOCATION}}
// Microsoft eMbedded Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TILEPUZZLE_H__FCEDB356_5138_4B1C_A0C7_EE99ECF7B657__INCLUDED_)
