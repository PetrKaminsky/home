#if !defined(AFX_CHILDVIEW_H__84253C09_C784_4CB7_8801_532B112FC98B__INCLUDED_)
#define AFX_CHILDVIEW_H__84253C09_C784_4CB7_8801_532B112FC98B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////

#include "AutoSolver.h"

#define WM_TEST_SOLVED (WM_USER + 1)
#define SMOOTH_TIMER 1
#define SMOOTH_TIMER_PERIOD 10
#define AUTO_TIMER 2
#define AUTO_TIMER_PERIOD 300

class CSmoothMove
{
private:
	CRect m_rTileRect;
	CRect m_rRedrawRect;
	int m_nTile;
	bool m_bTileSolved;

public:
	CSmoothMove();
	CSmoothMove(
		const CRect &rTileRect,
		const CRect &rRedrawRect,
		int nTile,
		bool bTileSolved);

	CRect GetTileRect() const
		{ return m_rTileRect; }
	CRect GetRedrawRect() const 
		{ return m_rRedrawRect; }
	int GetTile() const
		{ return m_nTile; }
	bool GetTileSolved() const
		{ return m_bTileSolved; }
};

/////////////////////////////////////////////////////////////////////////////

#include <map>

class CClickItemMap
{
public:
	void AddItem(
		int nFrom,
		int nTo,
		int nSeq);
	bool FindTile(
		const CPoint &ptClick,
		int &nX,
		int &nY);
	void Reset();

private:
	typedef std::map<int, int> TIntervalMap;
	typedef TIntervalMap::iterator TIntervalMapIterator;
	typedef TIntervalMap::value_type TIntervalMapValueType;
	TIntervalMap m_List;
};

/////////////////////////////////////////////////////////////////////////////

class CChildView : public CWnd
{
public:
	CChildView();

	int Create(CWnd *pParent);

	//{{AFX_VIRTUAL(CChildView)
	protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//}}AFX_VIRTUAL

protected:
	//{{AFX_MSG(CChildView)
	afx_msg void OnPaint();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg BOOL OnEraseBkgnd(CDC *pDC);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnKeyUp();
	afx_msg void OnKeyDown();
	afx_msg void OnKeyLeft();
	afx_msg void OnKeyRight();
	afx_msg void OnShuffle();
	afx_msg void OnDesk3x3();
	afx_msg void OnDesk4x4();
	afx_msg void OnDesk5x5();
	afx_msg void OnDesk6x6();
	afx_msg void OnDesk7x7();
	afx_msg void OnDesk8x8();
	afx_msg void OnDesk9x9();
	afx_msg void OnDesk10x10();
	afx_msg void OnAutoMode();
	afx_msg void OnUpdateAutoMode(CCmdUI *pCmdUI);
	afx_msg void OnUpdateKeyUp(CCmdUI *pCmdUI);
	afx_msg void OnUpdateKeyDown(CCmdUI *pCmdUI);
	afx_msg void OnUpdateKeyLeft(CCmdUI *pCmdUI);
	afx_msg void OnUpdateKeyRight(CCmdUI *pCmdUI);
	afx_msg void OnUpdateDesk3x3(CCmdUI *pCmdUI);
	afx_msg void OnUpdateDesk4x4(CCmdUI *pCmdUI);
	afx_msg void OnUpdateDesk5x5(CCmdUI *pCmdUI);
	afx_msg void OnUpdateDesk6x6(CCmdUI *pCmdUI);
	afx_msg void OnUpdateDesk7x7(CCmdUI *pCmdUI);
	afx_msg void OnUpdateDesk8x8(CCmdUI *pCmdUI);
	afx_msg void OnUpdateDesk9x9(CCmdUI *pCmdUI);
	afx_msg void OnUpdateDesk10x10(CCmdUI *pCmdUI);
	afx_msg void OnUpdateShuffle(CCmdUI *pCmdUI);
	afx_msg void OnRecognizeGesture(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnDesk11x11();
	afx_msg void OnUpdateDesk11x11(CCmdUI* pCmdUI);
	//}}AFX_MSG
	afx_msg LRESULT OnTestSolved(WPARAM wParam, LPARAM lParam);
	DECLARE_MESSAGE_MAP()

private:
	CDesk m_Desk;
	int m_nSize;
	int m_nBorder;
	CBitmap m_bmpOff;
	bool m_bSolving;
	bool m_bSmoothTimer;
	std::deque<CSmoothMove> m_SmoothMoveQueue;
	bool m_bAutoMode;
	CAutoSolver m_AutoSolver;
	COLORREF m_clrTileBgNotSolved;
	COLORREF m_clrTileBgSolved;
	COLORREF m_clrTileTxNotSolved;
	COLORREF m_clrTileTxSolved;
	COLORREF m_clrDeskBg;
	COLORREF m_clrTileFrame;
	bool m_bFirstPaint;
	CClickItemMap m_ClickItemMap;
	int m_nWidth;
	int m_nHeight;
	CFont m_Font;
	CPen m_Pen;

	void OnCreate();
	void DrawTile(
		CDC &dc,
		CRect &r,
		int nTile,
		bool bTileSolved);
	BOOL LoadAccelTable();
	void RedrawOff();
	void PostTestSolved();
	bool GiveUp();
	void OnSmoothMoveTimer();
	void GenerSmoothMoves(const CMove &Move);
	void CalcRects(
		const CMove &Move,
		CRect &rFrom,
		CRect &rTo,
		CRect &rRedraw);
	bool GetSmoothMove(CSmoothMove &SmoothMove);
	void ShiftUp();
	void ShiftDown();
	void ShiftLeft();
	void ShiftRight();
	void ProcessDeskMoves();
	void Redraw();
	void OnAutoTimer();
	void AutoSolverStart();
	void AutoSolverStop();
	void OnDesk(int nSize);
	void OnUpdateDesk(CCmdUI *pCmdUI, int nSize);
	void EnsureBitmap();
	void InvalidateOffscreen();
	CRect GetRect();
	void DiscardSmoothMoves();
	void EnsureFont(CDC &dc);
	void DeleteFont();
	void DeleteBitmap();
	int GetSmoothMoveSteps();
	int GetDistance();
	int GetFontSize();
	void DeletePen();
	void EnsurePen(CDC &dc);
	int GetPenWidth();
	int GetBorderWidth();
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft eMbedded Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CHILDVIEW_H__84253C09_C784_4CB7_8801_532B112FC98B__INCLUDED_)
