#include "stdafx.h"
#include "TilePuzzle.h"
#include "ChildView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////

CChildView::CChildView()
{
	m_nSize = -1;
	m_nBorder = -1;
	m_bSolving = false;
	m_bSmoothTimer = false;
	m_bAutoMode = false;
	m_clrTileBgNotSolved = RGB(232, 96, 0);
	m_clrTileBgSolved = RGB(240, 192, 0);
	m_clrTileTxNotSolved = RGB(232, 232, 232);
	m_clrTileTxSolved = RGB(0, 0, 0);
	m_clrDeskBg = RGB(0, 128, 192);
	m_clrTileFrame = RGB(0, 0, 0);
	m_bFirstPaint = true;
	m_nWidth = -1;
	m_nHeight = -1;
}

BEGIN_MESSAGE_MAP(CChildView,CWnd)
	//{{AFX_MSG_MAP(CChildView)
	ON_WM_PAINT()
	ON_WM_TIMER()
	ON_WM_ERASEBKGND()
	ON_WM_LBUTTONDOWN()
	ON_COMMAND(ID_KEY_UP, OnKeyUp)
	ON_COMMAND(ID_KEY_DOWN, OnKeyDown)
	ON_COMMAND(ID_KEY_LEFT, OnKeyLeft)
	ON_COMMAND(ID_KEY_RIGHT, OnKeyRight)
	ON_COMMAND(ID_SHUFFLE, OnShuffle)
	ON_COMMAND(ID_DESK_3_3, OnDesk3x3)
	ON_COMMAND(ID_DESK_4_4, OnDesk4x4)
	ON_COMMAND(ID_DESK_5_5, OnDesk5x5)
	ON_COMMAND(ID_DESK_6_6, OnDesk6x6)
	ON_COMMAND(ID_DESK_7_7, OnDesk7x7)
	ON_COMMAND(ID_DESK_8_8, OnDesk8x8)
	ON_COMMAND(ID_DESK_9_9, OnDesk9x9)
	ON_COMMAND(ID_DESK_10_10, OnDesk10x10)
	ON_COMMAND(ID_AUTO_MODE, OnAutoMode)
	ON_UPDATE_COMMAND_UI(ID_AUTO_MODE, OnUpdateAutoMode)
	ON_UPDATE_COMMAND_UI(ID_KEY_UP, OnUpdateKeyUp)
	ON_UPDATE_COMMAND_UI(ID_KEY_DOWN, OnUpdateKeyDown)
	ON_UPDATE_COMMAND_UI(ID_KEY_LEFT, OnUpdateKeyLeft)
	ON_UPDATE_COMMAND_UI(ID_KEY_RIGHT, OnUpdateKeyRight)
	ON_UPDATE_COMMAND_UI(ID_DESK_3_3, OnUpdateDesk3x3)
	ON_UPDATE_COMMAND_UI(ID_DESK_4_4, OnUpdateDesk4x4)
	ON_UPDATE_COMMAND_UI(ID_DESK_5_5, OnUpdateDesk5x5)
	ON_UPDATE_COMMAND_UI(ID_DESK_6_6, OnUpdateDesk6x6)
	ON_UPDATE_COMMAND_UI(ID_DESK_7_7, OnUpdateDesk7x7)
	ON_UPDATE_COMMAND_UI(ID_DESK_8_8, OnUpdateDesk8x8)
	ON_UPDATE_COMMAND_UI(ID_DESK_9_9, OnUpdateDesk9x9)
	ON_UPDATE_COMMAND_UI(ID_DESK_10_10, OnUpdateDesk10x10)
	ON_UPDATE_COMMAND_UI(ID_SHUFFLE, OnUpdateShuffle)
	ON_NOTIFY_REFLECT(NM_RECOGNIZEGESTURE, OnRecognizeGesture) 
	ON_WM_SIZE()
	ON_WM_LBUTTONDOWN()
	ON_COMMAND(ID_DESK_11_11, OnDesk11x11)
	ON_UPDATE_COMMAND_UI(ID_DESK_11_11, OnUpdateDesk11x11)
	//}}AFX_MSG_MAP
	ON_MESSAGE(WM_TEST_SOLVED, OnTestSolved)
END_MESSAGE_MAP()

int CChildView::GetDistance()
{
	return 2 + m_nSize / m_Desk.GetSize() / 50;
}

int CChildView::GetSmoothMoveSteps()
{
	switch (m_Desk.GetSize())
	{
	case 3:
		return 7;
	case 4:
		return 6;
	case 5:
	case 6:
		return 5;
	case 7:
	case 8:
		return 4;
	}
	return 3;
}

int CChildView::Create(CWnd *pParent)
{
	return CWnd::Create(NULL, NULL, WS_CHILD | WS_VISIBLE,
		CRect(0, 0, 0, 0), pParent, AFX_IDW_PANE_FIRST, NULL);
}

BOOL CChildView::PreCreateWindow(CREATESTRUCT& cs)
{
	if (!CWnd::PreCreateWindow(cs))
		return FALSE;

	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW);

	return TRUE;
}

void CChildView::OnPaint()
{
	if (m_bFirstPaint)
	{
		OnCreate();
		m_bFirstPaint = false;
	}
	CPaintDC dc(this); // device context for painting
	CDC dcMem;
	dcMem.CreateCompatibleDC(&dc);
	CRect r = dc.m_ps.rcPaint;
	EnsureBitmap();
	CBitmap *pOldBmp = dcMem.SelectObject(&m_bmpOff);
	dc.BitBlt(r.left,
		r.top,
		r.right - r.left,
		r.bottom - r.top,
		&dcMem,
		r.left,
		r.top,
		SRCCOPY);
	dcMem.SelectObject(pOldBmp);
	dcMem.DeleteDC();
}

void CChildView::InvalidateOffscreen()
{
	m_nSize = -1;
	m_nBorder = -1;
	DeleteBitmap();
	DeleteFont();
	DeletePen();
	EnsureBitmap();
	Redraw();
}

void CChildView::OnKeyUp()
{
	if (m_bAutoMode)
		return;
	if (!m_bSolving)
		return;
	if (m_bAutoMode)
		return;
	ShiftUp();
}

void CChildView::OnKeyDown()
{
	if (m_bAutoMode)
		return;
	if (!m_bSolving)
		return;
	if (m_bAutoMode)
		return;
	ShiftDown();
}

void CChildView::OnKeyLeft()
{
	if (m_bAutoMode)
		return;
	if (!m_bSolving)
		return;
	if (m_bAutoMode)
		return;
	ShiftLeft();
}

void CChildView::OnKeyRight()
{
	if (m_bAutoMode)
		return;
	if (!m_bSolving)
		return;
	if (m_bAutoMode)
		return;
	ShiftRight();
}

void CChildView::OnShuffle()
{
	if (m_bAutoMode)
		return;
	if (!GiveUp())
		return;
	CWaitCursor wc;
	m_Desk.Shuffle();
	Redraw();
	m_bSolving = true;
}

void CChildView::OnDesk3x3()
{
	OnDesk(3);
}

void CChildView::OnDesk4x4()
{
	OnDesk(4);
}

void CChildView::OnDesk5x5()
{
	OnDesk(5);
}

void CChildView::OnDesk6x6()
{
	OnDesk(6);
}

void CChildView::OnDesk7x7()
{
	OnDesk(7);
}

void CChildView::OnDesk8x8()
{
	OnDesk(8);
}

void CChildView::OnDesk9x9()
{
	OnDesk(9);
}

void CChildView::OnDesk10x10()
{
	OnDesk(10);
}

void CChildView::OnDesk11x11()
{
	OnDesk(11);
}

void CChildView::OnCreate()
{
	if (m_Desk.RestoreState())
		m_bSolving = !m_Desk.IsSolved();
	else
		m_Desk.Init(4);
	Redraw();
}

BOOL CChildView::OnEraseBkgnd(CDC *pDC)
{
	return TRUE;
}

void CChildView::DrawTile(
	CDC &dc,
	CRect &r,
	int nTile,
	bool bTileSolved)
{
	COLORREF clrBk(bTileSolved ? m_clrTileBgSolved : m_clrTileBgNotSolved);
	int nDefl = GetPenWidth() / 2;
	r.DeflateRect(nDefl, nDefl);
	dc.FillSolidRect(r, clrBk);

	EnsurePen(dc);
	CPen *pOldPen = dc.SelectObject(&m_Pen);
	dc.MoveTo(r.left, r.top);
	dc.LineTo(r.left, r.bottom);
	dc.LineTo(r.right, r.bottom);
	dc.LineTo(r.right, r.top);
	dc.LineTo(r.left, r.top);
	dc.SelectObject(pOldPen);

	CString sTile;
	sTile.Format(_T("%d"), nTile);
	EnsureFont(dc);
	COLORREF clrTx(bTileSolved ? m_clrTileTxSolved : m_clrTileTxNotSolved);
	dc.SetTextColor(clrTx);
	CFont *pOldFont = dc.SelectObject(&m_Font);
	r.DeflateRect(nDefl, nDefl);
	dc.DrawText(
		sTile,
		r,
		DT_SINGLELINE | DT_VCENTER | DT_CENTER);
	dc.SelectObject(pOldFont);
}

void CChildView::EnsureBitmap()
{
	CClientDC dc(this);
	CRect r = GetRect();
	if (m_bmpOff.GetSafeHandle() == NULL)
		m_bmpOff.CreateCompatibleBitmap(
			&dc, 
			r.Width(), 
			r.Height());
	if (m_nSize == -1)
		m_nSize = (r.Width() < r.Height()) ? r.Width() : r.Height();
	if (m_nBorder == -1)
		m_nBorder = GetBorderWidth();
}

void CChildView::RedrawOff()
{
	CClientDC dc(this);
	CRect r = GetRect();
	CDC dcMem;
	dcMem.CreateCompatibleDC(&dc);
	EnsureBitmap();
	CBitmap *pOldBmp = dcMem.SelectObject(&m_bmpOff);
	CBrush br;
	br.CreateSolidBrush(m_clrDeskBg);
	dcMem.FillRect(&r, &br);

	int nX1 = m_nBorder;
	int nY1 = m_nBorder;
	int nX2 = m_nSize - m_nBorder;
	int nY2 = m_nSize - m_nBorder;
	int nWidth = nX2 - nX1;
	int nHeight = nY2 - nY1;
	int nSub = GetDistance();

	bool bTileSolved;
	int i, j, nTile;
	int nSize = m_Desk.GetSize();
	int nTileWidth = nWidth / nSize;
	int nTileHeight = nHeight / nSize;
	m_ClickItemMap.Reset();
	for (j = 0; j < nSize; j++)
	{
		m_ClickItemMap.AddItem(nY1 + (j * nTileHeight) + nSub, nY1 + ((j + 1) * nTileHeight) - nSub, j);
		for (i = 0; i < nSize; i++)
		{
			m_Desk.GetTile(i, j, nTile, bTileSolved);
			if (nTile != CDesk::s_nEmpty)
			{
				CRect r(
					CPoint(
						nX1 + (i * nTileWidth) + nSub,
						nY1 + (j * nTileHeight) + nSub), 
					CSize(
						nTileWidth - 2 * nSub,
						nTileHeight - 2 * nSub));
				DrawTile(
					dcMem,
					r,
					nTile,
					bTileSolved);
			}
		}
	}

	dcMem.SelectObject(pOldBmp);
	br.DeleteObject();
	dcMem.DeleteDC();
}

void CChildView::PostTestSolved()
{
	PostMessage(WM_TEST_SOLVED);
}

LRESULT CChildView::OnTestSolved(WPARAM wParam, LPARAM lParam)
{
	if (m_Desk.IsSolved())
	{
		m_bSolving = false;
		if (!m_bAutoMode)
			if (AfxMessageBox(IDS_NEW_GAME, MB_YESNO | MB_ICONINFORMATION | MB_DEFBUTTON1) == IDYES)
				OnShuffle();
	}
	return 0;
}

bool CChildView::GiveUp()
{
	if (m_bSolving)
		if (AfxMessageBox(IDS_GIVE_UP, MB_YESNO | MB_ICONINFORMATION | MB_DEFBUTTON2) == IDNO)
			return false;
	return true;
}

void CChildView::OnTimer(UINT nIDEvent)
{
	if (nIDEvent == SMOOTH_TIMER)
		OnSmoothMoveTimer();
	else if (nIDEvent == AUTO_TIMER)
		OnAutoTimer();

	CWnd::OnTimer(nIDEvent);
}

void CChildView::OnSmoothMoveTimer()
{
	CSmoothMove SmoothMove;
	if (GetSmoothMove(SmoothMove))
	{
		CClientDC dc(this);
		CDC dcMem;
		dcMem.CreateCompatibleDC(&dc);
		EnsureBitmap();
		CBitmap *pOldBmp = dcMem.SelectObject(&m_bmpOff);
		CBrush br;
		br.CreateSolidBrush(m_clrDeskBg);
		dcMem.FillRect(SmoothMove.GetRedrawRect(), &br);
		DrawTile(
			dcMem,
			SmoothMove.GetTileRect(),
			SmoothMove.GetTile(),
			SmoothMove.GetTileSolved());

		dcMem.SelectObject(pOldBmp);
		InvalidateRect(SmoothMove.GetRedrawRect());
	}
	else
	{
		KillTimer(SMOOTH_TIMER);
		m_bSmoothTimer = false;
	}
}

void CChildView::GenerSmoothMoves(const CMove &Move)
{
	CRect rFrom, rTo, rRedraw;
	CalcRects(
		Move,
		rFrom,
		rTo,
		rRedraw);
	int nPos, nStep;
	int nTile = Move.GetTile();
	bool bTileSolved = Move.IsTileSolved();
	int nSteps = GetSmoothMoveSteps();
	if (Move.GetFromX() == Move.GetToX())
	{
		if (Move.GetFromY() < Move.GetToY())
		{
			// down moves
			nPos = rFrom.top;
			nStep = (rTo.top - rFrom.top) / nSteps;
			do
			{
				nPos += nStep;
				if (nPos > rTo.top)
					nPos = rTo.top;
				CRect rTile(
					CPoint(rFrom.left, nPos),
					CSize(rFrom.Width(), rFrom.Height()));
				m_SmoothMoveQueue.push_back(
					CSmoothMove(
						rTile,
						rRedraw,
						nTile,
						bTileSolved));
			} while (nPos < rTo.top);
		}
		else
		{
			// up moves
			nPos = rFrom.top;
			nStep = (rFrom.top - rTo.top) / nSteps;
			do
			{
				nPos -= nStep;
				if (nPos < rTo.top)
					nPos = rTo.top;
				CRect rTile(
					CPoint(rFrom.left, nPos),
					CSize(rFrom.Width(), rFrom.Height()));
				m_SmoothMoveQueue.push_back(
					CSmoothMove(
						rTile,
						rRedraw,
						nTile,
						bTileSolved));
			} while (nPos > rTo.top);
		}
	}
	else
	{
		if (Move.GetFromX() < Move.GetToX())
		{
			// right moves
			nPos = rFrom.left;
			nStep = (rTo.left - rFrom.left) / nSteps;
			do
			{
				nPos += nStep;
				if (nPos > rTo.left)
					nPos = rTo.left;
				CRect rTile(
					CPoint(nPos, rFrom.top),
					CSize(rFrom.Width(), rFrom.Height()));
				m_SmoothMoveQueue.push_back(
					CSmoothMove(
						rTile,
						rRedraw,
						nTile,
						bTileSolved));
			} while (nPos < rTo.left);
		}
		else
		{
			// left moves
			nPos = rFrom.left;
			nStep = (rFrom.left - rTo.left) / nSteps;
			do
			{
				nPos -= nStep;
				if (nPos < rTo.left)
					nPos = rTo.left;
				CRect rTile(
					CPoint(nPos, rFrom.top),
					CSize(rFrom.Width(), rFrom.Height()));
				m_SmoothMoveQueue.push_back(
					CSmoothMove(
						rTile,
						rRedraw,
						nTile,
						bTileSolved));
			} while (nPos > rTo.left);
		}
	}

	if (!m_bSmoothTimer)
	{
		SetTimer(SMOOTH_TIMER, SMOOTH_TIMER_PERIOD, NULL);
		m_bSmoothTimer = true;
	}
}

void CChildView::CalcRects(
	const CMove &Move,
	CRect &rFrom,
	CRect &rTo,
	CRect &rRedraw)
{
	int nX1 = m_nBorder;
	int nY1 = m_nBorder;
	int nX2 = m_nSize - m_nBorder;
	int nY2 = m_nSize - m_nBorder;
	int nWidth = nX2 - nX1;
	int nHeight = nY2 - nY1;
	int nSub = GetDistance();
	int nSize = m_Desk.GetSize();
	int nTileWidth = nWidth / nSize;
	int nTileHeight = nHeight / nSize;

	rFrom = CRect(
		CPoint(nX1 + (Move.GetFromX() * nTileWidth) + nSub, nY1 + (Move.GetFromY() * nTileHeight) + nSub),
		CSize(nTileWidth - 2 * nSub, nTileHeight - 2 * nSub));
	rTo = CRect(
		CPoint(nX1 + (Move.GetToX() * nTileWidth) + nSub, nY1 + (Move.GetToY() * nTileHeight) + nSub),
		CSize(nTileWidth - 2 * nSub, nTileHeight - 2 * nSub));
	rRedraw.UnionRect(rFrom, rTo);
	rRedraw.InflateRect(1, 1);
}

bool CChildView::GetSmoothMove(CSmoothMove &SmoothMove)
{
	if (m_SmoothMoveQueue.empty())
		return false;

	SmoothMove = m_SmoothMoveQueue.front();
	m_SmoothMoveQueue.pop_front();

	return true;
}

void CChildView::ShiftUp()
{
	m_Desk.ShiftUp();
	ProcessDeskMoves();
}

void CChildView::ShiftDown()
{
	m_Desk.ShiftDown();
	ProcessDeskMoves();
}

void CChildView::ShiftLeft()
{
	m_Desk.ShiftLeft();
	ProcessDeskMoves();
}

void CChildView::ShiftRight()
{
	m_Desk.ShiftRight();
	ProcessDeskMoves();
}

void CChildView::ProcessDeskMoves()
{
	CMove Move;
	while (m_Desk.GetMove(Move))
		GenerSmoothMoves(Move);
	PostTestSolved();
}

void CChildView::Redraw()
{
	RedrawOff();
	Invalidate();
}

void CChildView::OnAutoMode()
{
	if (!m_bSolving)
		return;
	if (!m_bAutoMode)
	{
		m_bAutoMode = true;
		AutoSolverStart();
	}
	else
	{
		m_bAutoMode = false;
		AutoSolverStop();
	}
}

void CChildView::OnAutoTimer()
{
	if (!m_bAutoMode)
	{
		AutoSolverStop();
		return;
	}
	int nMove;
	if (m_AutoSolver.GetMove(nMove))
	{
		switch (nMove)
		{
			case CAutoSolver::AUTO_UP:
				ShiftUp();
				break;

			case CAutoSolver::AUTO_DOWN:
				ShiftDown();
				break;

			case CAutoSolver::AUTO_LEFT:
				ShiftLeft();
				break;

			case CAutoSolver::AUTO_RIGHT:
				ShiftRight();
				break;
		}
	}
	else
	{
		m_AutoSolver.SolveStep();
		if (m_AutoSolver.IsMove())
			return;
		m_bAutoMode = false;
		AutoSolverStop();
	}
}

void CChildView::AutoSolverStart()
{
	m_AutoSolver.InitFromDesk(m_Desk);
	m_AutoSolver.SolveStep();
	if (m_AutoSolver.IsMove())
		SetTimer(AUTO_TIMER, AUTO_TIMER_PERIOD, NULL);
	else
	{
		m_bAutoMode = false;
		AutoSolverStop();
	}
}

void CChildView::AutoSolverStop()
{
	KillTimer(AUTO_TIMER);
	m_AutoSolver.Reset();
}

void CChildView::OnLButtonDown(UINT nFlags, CPoint point)
{
	if (!m_bAutoMode && m_bSolving)
	{
		int nX, nY;
		if (m_ClickItemMap.FindTile(
			point,
			nX,
			nY))
		{
			m_Desk.ClickTile(nX, nY);
			ProcessDeskMoves();
		}
	}
	Default();
}

void CChildView::OnUpdateAutoMode(CCmdUI *pCmdUI)
{
	pCmdUI->SetCheck(m_bAutoMode ? 1 : 0);
	pCmdUI->Enable(m_bSolving ? TRUE : FALSE);
}

void CChildView::OnUpdateKeyUp(CCmdUI *pCmdUI)
{
	pCmdUI->Enable((m_bAutoMode || !m_bSolving) ? FALSE : TRUE);
}

void CChildView::OnUpdateKeyDown(CCmdUI *pCmdUI)
{
	pCmdUI->Enable((m_bAutoMode || !m_bSolving) ? FALSE : TRUE);
}

void CChildView::OnUpdateKeyLeft(CCmdUI *pCmdUI)
{
	pCmdUI->Enable((m_bAutoMode || !m_bSolving) ? FALSE : TRUE);
}

void CChildView::OnUpdateKeyRight(CCmdUI *pCmdUI)
{
	pCmdUI->Enable((m_bAutoMode || !m_bSolving) ? FALSE : TRUE);
}

void CChildView::OnUpdateDesk3x3(CCmdUI *pCmdUI)
{
	OnUpdateDesk(pCmdUI, 3);
}

void CChildView::OnUpdateDesk4x4(CCmdUI *pCmdUI)
{
	OnUpdateDesk(pCmdUI, 4);
}

void CChildView::OnUpdateDesk5x5(CCmdUI *pCmdUI)
{
	OnUpdateDesk(pCmdUI, 5);
}

void CChildView::OnUpdateDesk6x6(CCmdUI *pCmdUI)
{
	OnUpdateDesk(pCmdUI, 6);
}

void CChildView::OnUpdateDesk7x7(CCmdUI *pCmdUI)
{
	OnUpdateDesk(pCmdUI, 7);
}

void CChildView::OnUpdateDesk8x8(CCmdUI *pCmdUI)
{
	OnUpdateDesk(pCmdUI, 8);
}

void CChildView::OnUpdateDesk9x9(CCmdUI *pCmdUI)
{
	OnUpdateDesk(pCmdUI, 9);
}

void CChildView::OnUpdateDesk10x10(CCmdUI *pCmdUI)
{
	OnUpdateDesk(pCmdUI, 10);
}

void CChildView::OnUpdateShuffle(CCmdUI *pCmdUI)
{
	pCmdUI->Enable(m_bAutoMode ? FALSE : TRUE);
}

void CChildView::OnUpdateDesk11x11(CCmdUI* pCmdUI)
{
	OnUpdateDesk(pCmdUI, 11);
}

void CChildView::OnDesk(int nSize)
{
	if (m_bAutoMode)
		return;
	if (m_Desk.GetSize() == nSize)
		return;
	if (!GiveUp())
		return;
	CWaitCursor wc;
	m_Desk.Init(nSize);
	DeleteFont();
	DeletePen();
	if (m_bSolving)
		m_Desk.Shuffle();
	Redraw();
}

void CChildView::OnUpdateDesk(CCmdUI *pCmdUI, int nSize)
{
	pCmdUI->SetCheck((m_Desk.GetSize() == nSize) ? 1 : 0);
	pCmdUI->Enable(m_bAutoMode ? FALSE : TRUE);
}

void CChildView::OnRecognizeGesture(NMHDR *pNMHDR, LRESULT *pResult)
{
	*pResult = TRUE;
}

void CChildView::OnSize(UINT nType, int cx, int cy)
{
	if (cx == 0 &&
		cy == 0)
	{
		return;
	}

	if (m_nWidth == -1 &&
		m_nHeight == -1)
	{
		m_nWidth = cx;
		m_nHeight = cy;
	}
	else if (m_nWidth != cx ||
		m_nHeight < cy)
	{
		// orientation changed or SIP dismissed
		m_nWidth = cx;
		m_nHeight = cy;
		DiscardSmoothMoves();
		InvalidateOffscreen();
	}
}

CRect CChildView::GetRect()
{
	CRect r(0, 0, m_nWidth, m_nHeight);
	if (r.IsRectEmpty())
		GetClientRect(&r);
	return r;
}

void CChildView::DiscardSmoothMoves()
{
	m_SmoothMoveQueue.clear();
}

void CChildView::EnsureFont(CDC &dc)
{
	if (m_Font.GetSafeHandle() != NULL)
		return;

    LOGFONT logFont;
    ZeroMemory(&logFont, sizeof(LOGFONT));
    logFont.lfCharSet = DEFAULT_CHARSET;
    logFont.lfHeight = GetFontSize();
    lstrcpy(logFont.lfFaceName, _T("Tahoma"));
	logFont.lfQuality = CLEARTYPE_QUALITY;

    m_Font.CreateFontIndirect(&logFont);
}

int CChildView::GetFontSize()
{
	return (int)(0.65 * (m_nSize - 2 * m_nBorder - (m_Desk.GetSize() - 1) * GetDistance()) / m_Desk.GetSize());
}

void CChildView::DeleteFont()
{
	if (m_Font.GetSafeHandle() != NULL)
		m_Font.DeleteObject();
}

void CChildView::DeleteBitmap()
{
	if (m_bmpOff.GetSafeHandle() != NULL)
		m_bmpOff.DeleteObject();
}

void CChildView::DeletePen()
{
	if (m_Pen.GetSafeHandle() != NULL)
		m_Pen.DeleteObject();
}

void CChildView::EnsurePen(CDC &dc)
{
	if (m_Pen.GetSafeHandle() != NULL)
		return;

	m_Pen.CreatePen(PS_SOLID, GetPenWidth(), m_clrTileFrame);
}

int CChildView::GetPenWidth()
{
	return 1 + m_nSize / m_Desk.GetSize() / 27;
}

int CChildView::GetBorderWidth()
{
	return m_nSize / 80;
}

/////////////////////////////////////////////////////////////////////////////

CSmoothMove::CSmoothMove()
{
	m_nTile = CDesk::s_nEmpty;
	m_bTileSolved = false;
}

CSmoothMove::CSmoothMove(
	const CRect &rTileRect,
	const CRect &rRedrawRect,
	int nTile,
	bool bTileSolved)
:	m_rTileRect(rTileRect),
	m_rRedrawRect(rRedrawRect)
{
	m_nTile = nTile;
	m_bTileSolved = bTileSolved;
}

/////////////////////////////////////////////////////////////////////////////

void CClickItemMap::AddItem(
	int nFrom,
	int nTo,
	int nSeq)
{
	m_List[nFrom] = CDesk::s_nEmpty;
	m_List[nTo + 1] = nSeq;
}

bool CClickItemMap::FindTile(
	const CPoint &ptClick,
	int &nX,
	int &nY)
{
	TIntervalMapIterator itUpperX = m_List.upper_bound(ptClick.x);
	if (itUpperX == m_List.end())
	{
		return false;
	}
	int nUpperX = itUpperX->second;
	if (nUpperX == CDesk::s_nEmpty)
	{
		return false;
	}
	nX = nUpperX;
	TIntervalMapIterator itUpperY = m_List.upper_bound(ptClick.y);
	if (itUpperY == m_List.end())
	{
		return false;
	}
	int nUpperY = itUpperY->second;
	if (nUpperY == CDesk::s_nEmpty)
	{
		return false;
	}
	nY = nUpperY;
	return true;
}

void CClickItemMap::Reset()
{
	m_List.clear();
}

/////////////////////////////////////////////////////////////////////////////

std::_Lockit::_Lockit()
{
}

std::_Lockit::~_Lockit()
{
}
