#include "stdafx.h"
#include "SockFwd.h"
#include "ChildView.h"
#include "Globals.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CChldView

CChldView::CChldView()
	: CFormView(-1)
{
	m_nID = -1;
}

CChldView::CChldView(int nID)
	: CFormView(nID)
{
	m_nID = nID;
}

int CChldView::Create(CWnd *pParent)
{
	return CFormView::Create(NULL, NULL, WS_CHILD | WS_VISIBLE,
		CRect(0, 0, 0, 0), pParent, m_nID, NULL);
}

/////////////////////////////////////////////////////////////////////////////
// CChldView1

CChldView1::CChldView1()
:	CChldView(CChldView1::IDD),
	m_Column0Width(CSockFwdApp::s_pPropertySerializer, _T("Message\\Column0"), 100),
	m_Column1Width(CSockFwdApp::s_pPropertySerializer, _T("Message\\Column1"), 100)
{
	//{{AFX_DATA_INIT(CChldView1)
	//}}AFX_DATA_INIT
}

void CChldView1::DoDataExchange(CDataExchange* pDX)
{
	CChldView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CChldView1)
	DDX_Control(pDX, IDC_LIST_MESSAGE, m_ctrlMessage);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CChldView1, CChldView)
	//{{AFX_MSG_MAP(CChldView1)
	ON_WM_DESTROY()
	ON_WM_SIZE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CChldView1 message handlers

void CChldView1::OnInitialUpdate()
{
	CChldView::OnInitialUpdate();

	m_ctrlMessage.InsertColumn(0, CString((LPCTSTR)IDS_MESSAGE_COLUMN0), LVCFMT_LEFT, m_Column0Width.GetValue());
	m_ctrlMessage.InsertColumn(1, CString((LPCTSTR)IDS_MESSAGE_COLUMN1), LVCFMT_LEFT, m_Column1Width.GetValue());
	m_ctrlMessage.SetExtendedStyle(m_ctrlMessage.GetExtendedStyle() | LVS_EX_FULLROWSELECT | LVS_EX_HEADERDRAGDROP);
}

void CChldView1::OnDestroy()
{
	BackupProperties();

	CChldView::OnDestroy();
}

void CChldView1::OnSize(UINT nType, int cx, int cy)
{
	CChldView::OnSize(nType, cx, cy);

	if (cx == 0 &&
		cy == 0)
	{
		return;
	}

	if (m_ctrlMessage.GetSafeHwnd() == NULL)
	{
		return;
	}

	CRect r;
	GetClientRect(&r);

	CGlobals::ResizeControl(&m_ctrlMessage, r.right - 1, r.bottom - 1);
}

void CChldView1::BackupProperties()
{
	m_Column0Width.SetValue(m_ctrlMessage.GetColumnWidth(0));
	m_Column1Width.SetValue(m_ctrlMessage.GetColumnWidth(1));
}

void CChldView1::Log(LPCTSTR szMessage, LPCTSTR szMessage2)
{
	int nItem = m_ctrlMessage.GetItemCount();
	m_ctrlMessage.InsertItem(nItem, szMessage);
	if (szMessage2 != NULL)
		m_ctrlMessage.SetItemText(nItem, 1, szMessage2);
}

/////////////////////////////////////////////////////////////////////////////
// CChldView2

CChldView2::CChldView2()
:	CChldView(CChldView2::IDD),
	m_Column0Width(CSockFwdApp::s_pPropertySerializer, _T("Forward\\Column0"), 100),
	m_Column1Width(CSockFwdApp::s_pPropertySerializer, _T("Forward\\Column1"), 100),
	m_Column2Width(CSockFwdApp::s_pPropertySerializer, _T("Forward\\Column2"), 100),
	m_Column3Width(CSockFwdApp::s_pPropertySerializer, _T("Forward\\Column3"), 30),
	m_Column4Width(CSockFwdApp::s_pPropertySerializer, _T("Forward\\Column4"), 30)
{
	//{{AFX_DATA_INIT(CChldView2)
	//}}AFX_DATA_INIT
}

void CChldView2::DoDataExchange(CDataExchange* pDX)
{
	CChldView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CChldView2)
	DDX_Control(pDX, IDC_LIST_FORWARD, m_ctrlForward);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CChldView2, CChldView)
	//{{AFX_MSG_MAP(CChldView2)
	ON_WM_DESTROY()
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_BUTTON_NEW, OnButtonNew)
	ON_BN_CLICKED(IDC_BUTTON_EDIT, OnButtonEdit)
	ON_BN_CLICKED(IDC_BUTTON_DEL, OnButtonDel)
	ON_BN_CLICKED(IDC_BUTTON_DISC, OnButtonDisc)
	ON_BN_CLICKED(IDC_BUTTON_STOP, OnButtonStop)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CChldView2 message handlers

void CChldView2::OnInitialUpdate()
{
	CChldView::OnInitialUpdate();

	m_ctrlForward.InsertColumn(0, CString((LPCTSTR)IDS_FORWARD_COLUMN3), LVCFMT_LEFT, m_Column3Width.GetValue());
	m_ctrlForward.InsertColumn(1, CString((LPCTSTR)IDS_FORWARD_COLUMN0), LVCFMT_LEFT, m_Column0Width.GetValue());
	m_ctrlForward.InsertColumn(2, CString((LPCTSTR)IDS_FORWARD_COLUMN4), LVCFMT_LEFT, m_Column4Width.GetValue());
	m_ctrlForward.InsertColumn(3, CString((LPCTSTR)IDS_FORWARD_COLUMN1), LVCFMT_LEFT, m_Column1Width.GetValue());
	m_ctrlForward.InsertColumn(4, CString((LPCTSTR)IDS_FORWARD_COLUMN2), LVCFMT_LEFT, m_Column2Width.GetValue());
	m_ctrlForward.SetExtendedStyle(m_ctrlForward.GetExtendedStyle() | LVS_EX_FULLROWSELECT | LVS_EX_HEADERDRAGDROP);
}

void CChldView2::OnDestroy()
{
	BackupProperties();

	CFormView::OnDestroy();
}

void CChldView2::OnSize(UINT nType, int cx, int cy)
{
	CFormView::OnSize(nType, cx, cy);

	if (cx == 0 &&
		cy == 0)
	{
		return;
	}

	if (m_ctrlForward.GetSafeHwnd() == NULL)
	{
		return;
	}

	CRect r;
	GetClientRect(&r);

	CGlobals::ResizeControl(&m_ctrlForward, r.right - 1, r.bottom - 1);
}

void CChldView2::OnButtonNew()
{
	CFwdDlg dlg(this);
	dlg.m_eMode = CFwdDlg::NEW;
	if (dlg.DoModal() == IDOK)
	{
		int nLocFamily = dlg.m_nLocFamily;
		short nLocPort = dlg.m_nLocPort;
		int nRemFamily = dlg.m_nRemFamily;
		CString sRemAddr = dlg.m_sRemAddr;
		short nRemPort = dlg.m_nRemPort;
		if (CSockFwdApp::s_pForwarders->NewForwarder(
			nLocFamily,
			nLocPort,
			nRemFamily,
			sRemAddr,
			nRemPort) != NULL)
		{
			UpdateItem(
				-1,
				nLocFamily,
				nLocPort,
				nRemFamily,
				sRemAddr,
				nRemPort);
		}
	}
}

void CChldView2::OnButtonEdit()
{
	int nItem = m_ctrlForward.GetNextItem(-1, LVNI_SELECTED);
	if (nItem == -1)
		return;

	int nLocFamily = CGlobals::FromString(m_ctrlForward.GetItemText(nItem, 0));
	short nLocPort = CGlobals::FromString(m_ctrlForward.GetItemText(nItem, 1));
	int nRemFamily = CGlobals::FromString(m_ctrlForward.GetItemText(nItem, 2));
	CString sRemAddr = m_ctrlForward.GetItemText(nItem, 3);
	short nRemPort = CGlobals::FromString(m_ctrlForward.GetItemText(nItem, 4));
	CFwdDlg dlg(this);
	dlg.m_eMode = CFwdDlg::EDIT;
	dlg.m_nLocFamily = nLocFamily;
	dlg.m_nLocPort = nLocPort;
	dlg.m_nRemFamily = nRemFamily;
	dlg.m_sRemAddr = sRemAddr;
	dlg.m_nRemPort = nRemPort;
	if (dlg.DoModal() == IDOK)
	{
		nRemFamily = dlg.m_nRemFamily;
		sRemAddr = dlg.m_sRemAddr;
		nRemPort = dlg.m_nRemPort;
		if (CSockFwdApp::s_pForwarders->EditForwarder(
			nLocFamily,
			nLocPort,
			nRemFamily,
			sRemAddr,
			nRemPort) != NULL)
		{
			UpdateItem(
				nItem,
				nLocFamily,
				nLocPort,
				nRemFamily,
				sRemAddr,
				nRemPort);
		}
	}
}

void CChldView2::OnButtonDel()
{
	int nItem = m_ctrlForward.GetNextItem(-1, LVNI_SELECTED);
	if (nItem == -1)
		return;

	int nLocFamily = CGlobals::FromString(m_ctrlForward.GetItemText(nItem, 0));
	short nLocPort = CGlobals::FromString(m_ctrlForward.GetItemText(nItem, 1));
	CSockFwdApp::s_pForwarders->DeleteForwarder(nLocFamily, nLocPort);
	m_ctrlForward.DeleteItem(nItem);
}

void CChldView2::BackupProperties()
{
	m_Column0Width.SetValue(m_ctrlForward.GetColumnWidth(1));
	m_Column1Width.SetValue(m_ctrlForward.GetColumnWidth(3));
	m_Column2Width.SetValue(m_ctrlForward.GetColumnWidth(4));
	m_Column3Width.SetValue(m_ctrlForward.GetColumnWidth(0));
	m_Column4Width.SetValue(m_ctrlForward.GetColumnWidth(2));
}

void CChldView2::UpdateItem(
	int nItem,
	int nLocFamily,
	short nLocPort,
	int nRemFamily,
	LPCTSTR szRemAddr,
	short nRemPort)
{
	if (nItem == -1)
	{
		nItem = m_ctrlForward.InsertItem(m_ctrlForward.GetItemCount(), CGlobals::ToString(nLocFamily));
	}
	m_ctrlForward.SetItemText(nItem, 1, CGlobals::ToString(nLocPort));
	m_ctrlForward.SetItemText(nItem, 2, CGlobals::ToString(nRemFamily));
	m_ctrlForward.SetItemText(nItem, 3, szRemAddr);
	m_ctrlForward.SetItemText(nItem, 4, CGlobals::ToString(nRemPort));
}

void CChldView2::OnButtonDisc()
{
	int nItem = m_ctrlForward.GetNextItem(-1, LVNI_SELECTED);
	if (nItem == -1)
		return;

	int nLocFamily = CGlobals::FromString(m_ctrlForward.GetItemText(nItem, 0));
	short nLocPort = CGlobals::FromString(m_ctrlForward.GetItemText(nItem, 1));
	CSockFwdApp::s_pForwarders->DisconnectForwarder(nLocFamily, nLocPort);
}

void CChldView2::OnButtonStop()
{
	int nItem = m_ctrlForward.GetNextItem(-1, LVNI_SELECTED);
	if (nItem == -1)
		return;

	int nLocFamily = CGlobals::FromString(m_ctrlForward.GetItemText(nItem, 0));
	short nLocPort = CGlobals::FromString(m_ctrlForward.GetItemText(nItem, 1));
	CSockFwdApp::s_pForwarders->StopForwarder(nLocFamily, nLocPort);
}

/////////////////////////////////////////////////////////////////////////////
// CFwdDlg dialog

CFwdDlg::CFwdDlg(CWnd* pParent /*=NULL*/)
:	CDialog(CFwdDlg::IDD, pParent),
	m_eMode(UNKNOWN),
	m_nLocFamily(AF_INET),
	m_nRemFamily(AF_INET)
{
	//{{AFX_DATA_INIT(CFwdDlg)
	m_nLocPort = 0;
	m_sRemAddr = _T("");
	m_nRemPort = 0;
	//}}AFX_DATA_INIT
}

void CFwdDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CFwdDlg)
	DDX_Control(pDX, IDC_EDIT_LOC_PORT, m_ctrlLocPort);
	DDX_Text(pDX, IDC_EDIT_LOC_PORT, m_nLocPort);
	DDV_MinMaxInt(pDX, m_nLocPort, 0, 32767);
	DDX_Text(pDX, IDC_EDIT_REM_ADDR, m_sRemAddr);
	DDX_Text(pDX, IDC_EDIT_REM_PORT, m_nRemPort);
	DDV_MinMaxInt(pDX, m_nRemPort, 0, 32767);
	DDX_Control(pDX, IDC_RADIO_LOC_IPV4, m_ctrlLocIpv4);
	DDX_Control(pDX, IDC_RADIO_LOC_IPV6, m_ctrlLocIpv6);
	DDX_Control(pDX, IDC_RADIO_REM_IPV4, m_ctrlRemIpv4);
	DDX_Control(pDX, IDC_RADIO_REM_IPV6, m_ctrlRemIpv6);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CFwdDlg, CDialog)
	//{{AFX_MSG_MAP(CFwdDlg)
	ON_BN_CLICKED(IDC_RADIO_REM_IPV4, OnRadioRemFamily)
	ON_BN_CLICKED(IDC_RADIO_REM_IPV6, OnRadioRemFamily)
	ON_BN_CLICKED(IDC_RADIO_LOC_IPV4, OnRadioLocFamily)
	ON_BN_CLICKED(IDC_RADIO_LOC_IPV6, OnRadioLocFamily)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CFwdDlg message handlers

BOOL CFwdDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_ctrlLocPort.EnableWindow(m_eMode == NEW ? TRUE : FALSE);
	m_ctrlLocIpv4.EnableWindow(m_eMode == NEW ? TRUE : FALSE);
	m_ctrlLocIpv6.EnableWindow(m_eMode == NEW ? TRUE : FALSE);
	m_ctrlLocIpv4.SetCheck(m_nLocFamily == AF_INET ? BST_CHECKED : BST_UNCHECKED);
	m_ctrlLocIpv6.SetCheck(m_nLocFamily == AF_INET6 ? BST_CHECKED : BST_UNCHECKED);
	m_ctrlRemIpv4.SetCheck(m_nRemFamily == AF_INET ? BST_CHECKED : BST_UNCHECKED);
	m_ctrlRemIpv6.SetCheck(m_nRemFamily == AF_INET6 ? BST_CHECKED : BST_UNCHECKED);

	return TRUE;
}

void CFwdDlg::OnRadioRemFamily()
{
	if (m_ctrlRemIpv4.GetCheck() == BST_CHECKED)
		m_nRemFamily = AF_INET;
	else if (m_ctrlRemIpv6.GetCheck() == BST_CHECKED)
		m_nRemFamily = AF_INET6;
}

void CFwdDlg::OnRadioLocFamily()
{
	if (m_ctrlLocIpv4.GetCheck() == BST_CHECKED)
		m_nLocFamily = AF_INET;
	else if (m_ctrlLocIpv6.GetCheck() == BST_CHECKED)
		m_nLocFamily = AF_INET6;
}
