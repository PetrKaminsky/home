#if !defined(AFX_FORWARDER_H__6D7E2283_6EB1_4EF3_BDFF_5AC5B6BB764E__INCLUDED_)
#define AFX_FORWARDER_H__6D7E2283_6EB1_4EF3_BDFF_5AC5B6BB764E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Socket.h"

#include <set>
#include <map>

using std::set;
using std::map;

/////////////////////////////////////////////////////////////////////////////
// CRemAddr

class CRemAddr
{
public:
	CRemAddr();

	int GetFamily() { return m_nFamily; }
	LPCTSTR GetAddress() { return m_sAddress; }
	short GetPort() { return m_nPort; }
	void Set(
		int nFamily,
		LPCTSTR szAddress,
		short nPort);

private:
	int m_nFamily;
	CString m_sAddress;
	short m_nPort;
};

/////////////////////////////////////////////////////////////////////////////
// CChannel

class CChannel
{
public:
	typedef enum
	{
		FWD_CREATED,
		FWD_CONNECTING,
		FWD_CONNECTED,
		FWD_ERROR,
		FWD_STOP
	} EFwdState;

	CChannel(CWinSock *pCliSock);
	~CChannel();

	bool Connect(
		int nRemFamily,
		LPCTSTR szRemAddr,
		short nRemPort);
	SOCKET GetFwdSockHandle() { return m_FwdSock.GetHandle(); }
	SOCKET GetCliSockHandle() { return m_pCliSock->GetHandle(); }
	bool Read(SOCKET sock);
	bool Write(SOCKET sock);
	bool Connect(SOCKET sock);
	void Stop();

private:
	CWinSock *m_pCliSock;
	CWinSock m_FwdSock;
	CWinSockBuffer m_CliBuff;
	CWinSockBuffer m_FwdBuff;
	CRemAddr m_FwdAddr;
	EFwdState m_eFwdState;

	void SetConnected();
	bool ReadFromCli();
	bool ReadFromFwd();
	bool WriteToCli();
	bool WriteToFwd();
};

/////////////////////////////////////////////////////////////////////////////
// CForwarder

typedef set<CChannel *> TChannelSet;
typedef TChannelSet::iterator TChannelSetIterator;

class CForwarder
{
public:
	~CForwarder();

	void SetRemoteAddress(
		int nFamily,
		LPCTSTR szAddress,
		short nPort);
	int GetRemFamily() { return m_RemAddr.GetFamily(); }
	LPCTSTR GetRemAddress() { return m_RemAddr.GetAddress(); }
	short GetRemPort() { return m_RemAddr.GetPort(); }
	bool Listen(short &nPort, int nFamily);
	SOCKET GetSockHandle() { return m_SrvSock.GetHandle(); }
	CChannel *CreateChannel(CWinSock *pCliSock);
	CWinSock *Accept();
	void DestroyChannel(CChannel *pChannel);
	TChannelSetIterator GetChannelsBegin() { return m_Channels.begin(); }
	TChannelSetIterator GetChannelsEnd() { return m_Channels.end(); }
	void StopChannels();

private:
	CRemAddr m_RemAddr;
	CWinSock m_SrvSock;
	TChannelSet m_Channels;
};

/////////////////////////////////////////////////////////////////////////////
// CFamilyPort

class CFamilyPort
{
public:
	CFamilyPort(int nFamily, short nPort);

	operator int();

private:
	int m_nFamily;
	short m_nPort;
};

/////////////////////////////////////////////////////////////////////////////
// CForwarders

typedef map<int, CForwarder *> TPortForwarderMap;
typedef TPortForwarderMap::iterator TPortForwarderIterator;

typedef map<SOCKET, CForwarder *> TSockForwarderMap;
typedef TSockForwarderMap::iterator TSockForwarderMapIterator;

typedef map<SOCKET, CChannel *> TSockChannelMap;
typedef TSockChannelMap::iterator TSockChannelMapIterator;

class CForwarders
{
public:
	~CForwarders();

	CForwarder *NewForwarder(
		int nLocFamily,
		short &nLocPort,
		int nRemFamily,
		LPCTSTR szRemAddr,
		short nRemPort);
	CForwarder *EditForwarder(
		int nLocFamily,
		short nLocPort,
		int nRemFamily,
		LPCTSTR szRemAddr,
		short nRemPort);
	void DeleteForwarder(int nLocFamily, short nLocPort);
	void DisconnectForwarder(int nLocFamily, short nLocPort);
	void StopForwarder(int nLocFamily, short nLocPort);
	void OnAccept(SOCKET sock, int nErrCode);
	void OnRead(SOCKET sock, int nErrCode);
	void OnWrite(SOCKET sock, int nErrCode);
	void OnClose(SOCKET sock, int nErrCode);
	void OnConnect(SOCKET sock, int nErrCode);

private:
	TPortForwarderMap m_PortForwarders;
	TSockForwarderMap m_ServerForwarders;
	TSockForwarderMap m_ChannelForwarders;
	TSockChannelMap m_Channels;

	void DestroyChannel(SOCKET sock);
	void DestroyChannels(CForwarder *pForwarder);
};

/////////////////////////////////////////////////////////////////////////////
//{{AFX_INSERT_LOCATION}}
// Microsoft eMbedded Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FORWARDER_H__6D7E2283_6EB1_4EF3_BDFF_5AC5B6BB764E__INCLUDED_)
