#if !defined(AFX_CHILDVIEW_H__1E35EC8A_2A98_4D9A_BA34_4766D793D4B0__INCLUDED_)
#define AFX_CHILDVIEW_H__1E35EC8A_2A98_4D9A_BA34_4766D793D4B0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CChldView form view

class CChldView : public CFormView
{
public:
	CChldView();
	CChldView(int nID);

	int Create(CWnd *pParent);

private:
	int m_nID;
};

/////////////////////////////////////////////////////////////////////////////
// CChldView1 form view

class CChldView1 : public CChldView, public ILog
{
public:
	CChldView1();

	//{{AFX_DATA(CChldView1)
	enum { IDD = IDD_FORM_MSG };
	CListCtrl	m_ctrlMessage;
	//}}AFX_DATA

	//{{AFX_VIRTUAL(CChldView1)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

protected:
	//{{AFX_MSG(CChldView1)
	afx_msg void OnDestroy();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	CRegPropertyInt m_Column0Width;
	CRegPropertyInt m_Column1Width;

	void BackupProperties();
	void Log(LPCTSTR szMessage, LPCTSTR szMessage2 = NULL);
};

/////////////////////////////////////////////////////////////////////////////
// CChldView2 form view

class CChldView2 : public CChldView
{
public:
	CChldView2();

	//{{AFX_DATA(CChldView2)
	enum { IDD = IDD_FORM_CLI };
	CListCtrl	m_ctrlForward;
	//}}AFX_DATA

	//{{AFX_VIRTUAL(CChldView2)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

protected:
	//{{AFX_MSG(CChldView2)
	afx_msg void OnDestroy();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnButtonNew();
	afx_msg void OnButtonEdit();
	afx_msg void OnButtonDel();
	afx_msg void OnButtonDisc();
	afx_msg void OnButtonStop();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	CRegPropertyInt m_Column0Width;
	CRegPropertyInt m_Column1Width;
	CRegPropertyInt m_Column2Width;
	CRegPropertyInt m_Column3Width;
	CRegPropertyInt m_Column4Width;

	void BackupProperties();
	void UpdateItem(
		int nItem,
		int nLocFamily,
		short nLocPort,
		int nRemFamily,
		LPCTSTR szRemAddr,
		short nRemPort);
};

/////////////////////////////////////////////////////////////////////////////
// CFwdDlg dialog

class CFwdDlg : public CDialog
{
public:
	CFwdDlg(CWnd* pParent);   // standard constructor

	enum { NEW, EDIT, UNKNOWN } m_eMode;

	int m_nLocFamily;
	int m_nRemFamily;

	//{{AFX_DATA(CFwdDlg)
	enum { IDD = IDD_DIALOG_CLI };
	CEdit	m_ctrlLocPort;
	short	m_nLocPort;
	CString	m_sRemAddr;
	short	m_nRemPort;
	CButton m_ctrlLocIpv4;
	CButton m_ctrlLocIpv6;
	CButton m_ctrlRemIpv4;
	CButton m_ctrlRemIpv6;
	//}}AFX_DATA

	//{{AFX_VIRTUAL(CFwdDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

protected:
	//{{AFX_MSG(CFwdDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnRadioRemFamily();
	afx_msg void OnRadioLocFamily();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////
//{{AFX_INSERT_LOCATION}}
// Microsoft eMbedded Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CHILDVIEW_H__1E35EC8A_2A98_4D9A_BA34_4766D793D4B0__INCLUDED_)
