#include "stdafx.h"
#include "Forwarder.h"
#include "SockFwd.h"
#include "Globals.h"

/////////////////////////////////////////////////////////////////////////////

CChannel::CChannel(CWinSock *pCliSock)
:	m_pCliSock(pCliSock),
	m_eFwdState(FWD_CREATED)
{
}

CChannel::~CChannel()
{
	if (m_pCliSock != NULL)
	{
		delete m_pCliSock;
		m_pCliSock = NULL;
	}
}

bool CChannel::Connect(
	int nRemFamily,
	LPCTSTR szRemAddr,
	short nRemPort)
{
	CString s;
	m_FwdAddr.Set(nRemFamily, szRemAddr, nRemPort);
	if (m_FwdSock.Create(nRemFamily))
	{
		m_FwdSock.AsyncSelect(
			CWinSockGlobalData::s_hSockWnd,
			CWinSockGlobalData::s_nSockMsgId,
			FD_CONNECT | FD_CLOSE);

		if (m_FwdSock.Connect(szRemAddr, nRemPort, nRemFamily))
		{
			SetConnected();

			return true;
		}
		else
		{
			int nErr = CWinSock::GetLastError();
			if (nErr != WSAEWOULDBLOCK)
			{
				s.Format(IDS_FORMAT_ERROR, nErr);
				CWinSockGlobalData::s_pLogImpl->Log(_T("CWinSock::Connect()"), s);
				m_eFwdState = FWD_ERROR;
			}
			else
			{
				m_eFwdState = FWD_CONNECTING;

				return true;
			}
		}
	}
	else
	{
		s.Format(IDS_FORMAT_ERROR, CWinSock::GetLastError());
		CWinSockGlobalData::s_pLogImpl->Log(_T("CWinSock::Create()"), s);
	}

	return false;
}

bool CChannel::Read(SOCKET sock)
{
	if (sock == m_pCliSock->GetHandle())
		return ReadFromCli();
	else if (sock == m_FwdSock.GetHandle())
		return ReadFromFwd();

	return false;
}

bool CChannel::Write(SOCKET sock)
{
	if (sock == m_pCliSock->GetHandle())
		return WriteToCli();
	else if (sock == m_FwdSock.GetHandle())
		return WriteToFwd();

	return false;
}

bool CChannel::Connect(SOCKET sock)
{
	if (sock == m_FwdSock.GetHandle())
		SetConnected();
	else
		return false;

	return true;
}

void CChannel::SetConnected()
{
	m_eFwdState = FWD_CONNECTED;
	long nMask = FD_READ | FD_CLOSE;
	if (!m_FwdBuff.IsEmpty())
		nMask |= FD_WRITE;
	m_FwdSock.AsyncSelect(
		CWinSockGlobalData::s_hSockWnd,
		CWinSockGlobalData::s_nSockMsgId,
		nMask);
	CString s, sAddr;
	short nPort;
	if (m_FwdSock.GetPeerName(sAddr, nPort))
		s.Format(IDS_FORMAT_CLIENT, sAddr, nPort);
	CWinSockGlobalData::s_pLogImpl->Log(CString((LPCTSTR)IDS_CONNECTED), s);
}

bool CChannel::ReadFromCli()
{
	bool bOK = true;
	int nTotalBytes = m_pCliSock->GetAvailableReadCount();
	if (nTotalBytes > 0)
	{
		BYTE *pBuff = new BYTE[nTotalBytes];
		int nRecvBytes = m_pCliSock->Receive(pBuff, nTotalBytes);
		if (nRecvBytes == SOCKET_ERROR)
		{
			int nErr = CWinSock::GetLastError();
			if (nErr != WSAEWOULDBLOCK)
			{
				CString s;
				s.Format(IDS_FORMAT_ERROR, nErr);
				CWinSockGlobalData::s_pLogImpl->Log(_T("CWinSock::Receive()"), s);
				bOK = false;
			}
		}
		else
		{
			// TODO - log data

			if (m_eFwdState != FWD_STOP)
				m_FwdBuff.AppendData(pBuff, nRecvBytes);
		}
		delete []pBuff;
	}
	if (bOK)
		if (m_eFwdState == FWD_CONNECTED)
			if (!m_FwdBuff.IsEmpty())
				m_FwdSock.AsyncSelect(
					CWinSockGlobalData::s_hSockWnd,
					CWinSockGlobalData::s_nSockMsgId,
					FD_READ | FD_WRITE | FD_CLOSE);

	return bOK;
}

bool CChannel::ReadFromFwd()
{
	bool bOK = true;
	int nTotalBytes = m_FwdSock.GetAvailableReadCount();
	if (nTotalBytes > 0)
	{
		BYTE *pBuff = new BYTE[nTotalBytes];
		int nRecvBytes = m_FwdSock.Receive(pBuff, nTotalBytes);
		if (nRecvBytes == SOCKET_ERROR)
		{
			int nErr = CWinSock::GetLastError();
			if (nErr != WSAEWOULDBLOCK)
			{
				CString s;
				s.Format(IDS_FORMAT_ERROR, nErr);
				CWinSockGlobalData::s_pLogImpl->Log(_T("CWinSock::Receive()"), s);
				bOK = false;
			}
		}
		else
		{
			// TODO - log data

			if (m_eFwdState != FWD_STOP)
				m_CliBuff.AppendData(pBuff, nRecvBytes);
		}
		delete []pBuff;
	}
	if (bOK)
		if (!m_CliBuff.IsEmpty())
			m_pCliSock->AsyncSelect(
				CWinSockGlobalData::s_hSockWnd,
				CWinSockGlobalData::s_nSockMsgId,
				FD_READ | FD_WRITE | FD_CLOSE);

	return bOK;
}

bool CChannel::WriteToCli()
{
	bool bOK = true;
	if (!m_CliBuff.IsEmpty())
	{
		int nSentBytes = m_pCliSock->Send(m_CliBuff.GetData(), m_CliBuff.GetDataLen());
		if (nSentBytes > 0)
			m_CliBuff.RemoveHeadCount(nSentBytes);
		else if (nSentBytes == SOCKET_ERROR)
		{
			int nErr = CWinSock::GetLastError();
			if (nErr != WSAEWOULDBLOCK)
			{
				CString s;
				s.Format(IDS_FORMAT_ERROR, nErr);
				CWinSockGlobalData::s_pLogImpl->Log(_T("CWinSock::Send()"), s);
				bOK = false;
			}
		}
	}
	if (bOK)
		if (m_CliBuff.IsEmpty())
			m_pCliSock->AsyncSelect(
				CWinSockGlobalData::s_hSockWnd,
				CWinSockGlobalData::s_nSockMsgId,
				FD_READ | FD_CLOSE);

	return bOK;
}

bool CChannel::WriteToFwd()
{
	bool bOK = true;
	if (!m_FwdBuff.IsEmpty())
	{
		int nSentBytes = m_FwdSock.Send(m_FwdBuff.GetData(), m_FwdBuff.GetDataLen());
		if (nSentBytes > 0)
			m_FwdBuff.RemoveHeadCount(nSentBytes);
		else if (nSentBytes == SOCKET_ERROR)
		{
			int nErr = CWinSock::GetLastError();
			if (nErr != WSAEWOULDBLOCK)
			{
				CString s;
				s.Format(IDS_FORMAT_ERROR, nErr);
				CWinSockGlobalData::s_pLogImpl->Log(_T("CWinSock::Send()"), s);
				bOK = false;
			}
		}
	}
	if (bOK)
		if (m_FwdBuff.IsEmpty())
			m_FwdSock.AsyncSelect(
				CWinSockGlobalData::s_hSockWnd,
				CWinSockGlobalData::s_nSockMsgId,
				FD_READ | FD_CLOSE);

	return bOK;
}

void CChannel::Stop()
{
	m_eFwdState = FWD_STOP;
}

/////////////////////////////////////////////////////////////////////////////

CRemAddr::CRemAddr()
{
	m_nFamily = AF_UNSPEC;
	m_nPort = 0;
}

void CRemAddr::Set(
	int nFamily,
	LPCTSTR szAddress,
	short nPort)
{
	m_nFamily = nFamily;
	m_sAddress = szAddress;
	m_nPort = nPort;
}

/////////////////////////////////////////////////////////////////////////////

CForwarder::~CForwarder()
{
	TChannelSetIterator it = m_Channels.begin();
	while (it != m_Channels.end())
	{
		delete *it;
		it++;
	}
}

void CForwarder::SetRemoteAddress(
	int nFamily,
	LPCTSTR szAddress,
	short nPort)
{
	m_RemAddr.Set(nFamily, szAddress, nPort);
}

bool CForwarder::Listen(short &nPort, int nFamily)
{
	CString s;
	if (m_SrvSock.Create(nFamily))
	{
		if (m_SrvSock.Bind(nPort, nFamily))
		{
			if (m_SrvSock.Listen())
			{
				m_SrvSock.AsyncSelect(
					CWinSockGlobalData::s_hSockWnd,
					CWinSockGlobalData::s_nSockMsgId,
					FD_ACCEPT);

				CString sAddr;
				if (m_SrvSock.GetSockName(sAddr, nPort))
				{
					CWinSockGlobalData::s_pLogImpl->Log(
						CString((LPCTSTR)IDS_LISTENING),
						CGlobals::ToString(nPort) + _T(" [") +
							CString((LPCTSTR)(nFamily == AF_INET ? IDS_FAMILY_IPV4 : IDS_FAMILY_IPV6)) +
							_T("]"));
				}
				else
				{
					s.Format(IDS_FORMAT_ERROR, CWinSock::GetLastError());
					CWinSockGlobalData::s_pLogImpl->Log(_T("CWinSock::GetSockName()"), s);
				}
				return true;
			}
			else
			{
				s.Format(IDS_FORMAT_ERROR, CWinSock::GetLastError());
				CWinSockGlobalData::s_pLogImpl->Log(_T("CWinSock::Listen()"), s);
			}
		}
		else
		{
			s.Format(IDS_FORMAT_ERROR, CWinSock::GetLastError());
			CWinSockGlobalData::s_pLogImpl->Log(_T("CWinSock::Bind()"), s);
		}
	}
	else
	{
		s.Format(IDS_FORMAT_ERROR, CWinSock::GetLastError());
		CWinSockGlobalData::s_pLogImpl->Log(_T("CWinSock::Create()"), s);
	}
	return false;
}

CChannel *CForwarder::CreateChannel(CWinSock *pCliSock)
{
	CChannel *pChannel = new CChannel(pCliSock);
	if (pChannel->Connect(
		GetRemFamily(),
		GetRemAddress(),
		GetRemPort()))
	{
		m_Channels.insert(pChannel);
	}
	else
	{
		delete pChannel;
		pChannel = NULL;
	}
	return pChannel;
}

CWinSock *CForwarder::Accept()
{
	short nPort;
	CString s, sAddr;
	CWinSock *pCliSock = m_SrvSock.Accept(sAddr, nPort);
	if (pCliSock != NULL)
	{
		s.Format(IDS_FORMAT_CLIENT, sAddr, nPort);
		CWinSockGlobalData::s_pLogImpl->Log(CString((LPCTSTR)IDS_ACCEPTED), s);
	}
	else
	{
		s.Format(IDS_FORMAT_ERROR, CWinSock::GetLastError());
		CWinSockGlobalData::s_pLogImpl->Log(_T("CWinSock::Accept()"), s);
	}
	return pCliSock;
}

void CForwarder::DestroyChannel(CChannel *pChannel)
{
	m_Channels.erase(pChannel);
	delete pChannel;
}

void CForwarder::StopChannels()
{
	TChannelSetIterator it = m_Channels.begin();
	while (it != m_Channels.end())
	{
		CChannel *pChannel = *it;
		pChannel->Stop();
		it++;
	}
}

/////////////////////////////////////////////////////////////////////////////

CForwarders::~CForwarders()
{
	TPortForwarderIterator it = m_PortForwarders.begin();
	while (it != m_PortForwarders.end())
	{
		delete it->second;
		it++;
	}
}

CForwarder *CForwarders::NewForwarder(
	int nLocFamily,
	short &nLocPort,
	int nRemFamily,
	LPCTSTR szRemAddr,
	short nRemPort)
{
	if (nLocPort != 0)
	{
		TPortForwarderIterator it = m_PortForwarders.find(CFamilyPort(nLocFamily, nLocPort));
		if (it != m_PortForwarders.end())
			return NULL;
	}
	CForwarder *pForwarder = new CForwarder;
	if (pForwarder->Listen(nLocPort, nLocFamily))
	{
		pForwarder->SetRemoteAddress(
			nRemFamily,
			szRemAddr,
			nRemPort);
		m_PortForwarders[CFamilyPort(nLocFamily, nLocPort)] = pForwarder;
		m_ServerForwarders[pForwarder->GetSockHandle()] = pForwarder;
	}
	else
	{
		delete pForwarder;
		pForwarder = NULL;
	}
	return pForwarder;
}

CForwarder *CForwarders::EditForwarder(
	int nLocFamily,
	short nLocPort,
	int nRemFamily,
	LPCTSTR szRemAddr,
	short nRemPort)
{
	TPortForwarderIterator it = m_PortForwarders.find(CFamilyPort(nLocFamily, nLocPort));
	if (it == m_PortForwarders.end())
		return NULL;

	CForwarder *pForwarder = it->second;
	pForwarder->SetRemoteAddress(
		nRemFamily,
		szRemAddr,
		nRemPort);
	return pForwarder;
}

void CForwarders::DeleteForwarder(int nLocFamily, short nLocPort)
{
	int nFamilyPort = CFamilyPort(nLocFamily, nLocPort);
	TPortForwarderIterator it = m_PortForwarders.find(nFamilyPort);
	if (it != m_PortForwarders.end())
	{
		CForwarder *pForwarder = it->second;
		if (pForwarder != NULL)
		{
			m_PortForwarders.erase(nFamilyPort);
			m_ServerForwarders.erase(pForwarder->GetSockHandle());
			DestroyChannels(pForwarder);
			delete pForwarder;
		}
	}
}

void CForwarders::DisconnectForwarder(int nLocFamily, short nLocPort)
{
	int nFamilyPort = CFamilyPort(nLocFamily, nLocPort);
	TPortForwarderIterator it = m_PortForwarders.find(nFamilyPort);
	if (it != m_PortForwarders.end())
	{
		CForwarder *pForwarder = it->second;
		if (pForwarder != NULL)
			DestroyChannels(pForwarder);
	}
}

void CForwarders::StopForwarder(int nLocFamily, short nLocPort)
{
	int nFamilyPort = CFamilyPort(nLocFamily, nLocPort);
	TPortForwarderIterator it = m_PortForwarders.find(nFamilyPort);
	if (it != m_PortForwarders.end())
	{
		CForwarder *pForwarder = it->second;
		if (pForwarder != NULL)
			pForwarder->StopChannels();
	}
}

void CForwarders::OnAccept(SOCKET sock, int nErrCode)
{
	if (nErrCode != 0)
	{
		CString s;
		s.Format(IDS_FORMAT_ERROR, nErrCode);
		CWinSockGlobalData::s_pLogImpl->Log(_T("CWinSock::OnAccept()"), s);
	}

	TSockForwarderMapIterator it = m_ServerForwarders.find(sock);
	if (it == m_ServerForwarders.end())
		return;

	CForwarder *pForwarder = it->second;
	CWinSock *pCliSock = pForwarder->Accept();
	if (pCliSock != NULL)
	{
		pCliSock->AsyncSelect(
			CWinSockGlobalData::s_hSockWnd,
			CWinSockGlobalData::s_nSockMsgId,
			FD_READ | FD_CLOSE);
		CChannel *pChannel = pForwarder->CreateChannel(pCliSock);
		if (pChannel != NULL)
		{
			SOCKET sockCli = pCliSock->GetHandle();
			SOCKET sockFwd = pChannel->GetFwdSockHandle();
			m_Channels[sockCli] = pChannel;
			m_Channels[sockFwd] = pChannel;
			m_ChannelForwarders[sockCli] = pForwarder;
			m_ChannelForwarders[sockFwd] = pForwarder;
		}
	}
}

void CForwarders::OnRead(SOCKET sock, int nErrCode)
{
	if (nErrCode != 0)
	{
		CString s;
		s.Format(IDS_FORMAT_ERROR, nErrCode);
		CWinSockGlobalData::s_pLogImpl->Log(_T("CWinSock::OnRead()"), s);

		DestroyChannel(sock);

		return;
	}

	TSockChannelMapIterator it = m_Channels.find(sock);
	if (it != m_Channels.end())
	{
		CChannel *pChannel = it->second;
		if (!pChannel->Read(sock))
			DestroyChannel(sock);
	}
}

void CForwarders::OnWrite(SOCKET sock, int nErrCode)
{
	if (nErrCode != 0)
	{
		CString s;
		s.Format(IDS_FORMAT_ERROR, nErrCode);
		CWinSockGlobalData::s_pLogImpl->Log(_T("CWinSock::OnWrite()"), s);

		DestroyChannel(sock);

		return;
	}

	TSockChannelMapIterator it = m_Channels.find(sock);
	if (it != m_Channels.end())
	{
		CChannel *pChannel = it->second;
		if (!pChannel->Write(sock))
			DestroyChannel(sock);
	}
}

void CForwarders::OnClose(SOCKET sock, int nErrCode)
{
	if (nErrCode != 0)
	{
		CString s;
		s.Format(IDS_FORMAT_ERROR, nErrCode);
		CWinSockGlobalData::s_pLogImpl->Log(_T("CWinSock::OnClose()"), s);
	}

	CWinSockGlobalData::s_pLogImpl->Log(CString((LPCTSTR)IDS_DISCONNECTED));

	DestroyChannel(sock);
}

void CForwarders::OnConnect(SOCKET sock, int nErrCode)
{
	if (nErrCode != 0)
	{
		CString s;
		s.Format(IDS_FORMAT_ERROR, nErrCode);
		CWinSockGlobalData::s_pLogImpl->Log(_T("CWinSock::OnConnect()"), s);

		DestroyChannel(sock);

		return;
	}

	TSockChannelMapIterator it = m_Channels.find(sock);
	if (it != m_Channels.end())
	{
		CChannel *pChannel = it->second;
		if (!pChannel->Connect(sock))
			DestroyChannel(sock);
	}
}

void CForwarders::DestroyChannel(SOCKET sock)
{
	TSockChannelMapIterator itChn = m_Channels.find(sock);
	if (itChn != m_Channels.end())
	{
		CChannel *pChannel = itChn->second;
		SOCKET sockCli = pChannel->GetCliSockHandle();
		SOCKET sockFwd = pChannel->GetFwdSockHandle();
		m_Channels.erase(sockCli);
		m_Channels.erase(sockFwd);
		TSockForwarderMapIterator itFwd = m_ChannelForwarders.find(sock);
		if (itFwd != m_ChannelForwarders.end())
		{
			CForwarder *pForwarder = itFwd->second;
			pForwarder->DestroyChannel(pChannel);
		}
		m_ChannelForwarders.erase(sockCli);
		m_ChannelForwarders.erase(sockFwd);
	}
}

void CForwarders::DestroyChannels(CForwarder *pForwarder)
{
	TChannelSetIterator it = pForwarder->GetChannelsBegin();
	while (it != pForwarder->GetChannelsEnd())
	{
		CChannel *pChannel = *it;
		SOCKET sockCli = pChannel->GetCliSockHandle();
		DestroyChannel(sockCli);
		it++;
	}
}

/////////////////////////////////////////////////////////////////////////////

CFamilyPort::CFamilyPort(int nFamily, short nPort)
:	m_nFamily(nFamily),
	m_nPort(nPort)
{
}

CFamilyPort::operator int()
{
	return ((m_nFamily & 0xffff) << 16) | m_nPort;
}
