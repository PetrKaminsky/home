#if !defined(AFX_SOCKFWD_H__313A5621_5135_461C_A471_5157B54C9DAE__INCLUDED_)
#define AFX_SOCKFWD_H__313A5621_5135_461C_A471_5157B54C9DAE__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols
#include "Property.h"
#include "Forwarder.h"

/////////////////////////////////////////////////////////////////////////////
// CSockFwdApp:
//

class CSockFwdApp : public CWinApp
{
public:
	static CRegPropertySerializer *s_pPropertySerializer;
	static CForwarders *s_pForwarders;

	//{{AFX_VIRTUAL(CSockFwdApp)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	//}}AFX_VIRTUAL

	//{{AFX_MSG(CSockFwdApp)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

class ILog
{
public:
	virtual void Log(LPCTSTR szMessage, LPCTSTR szMessage2 = NULL) = 0;
};

/////////////////////////////////////////////////////////////////////////////

class CWinSockGlobalData
{
public:
	static HWND s_hSockWnd;
	static UINT s_nSockMsgId;
	static ILog *s_pLogImpl;
};

/////////////////////////////////////////////////////////////////////////////
//{{AFX_INSERT_LOCATION}}
// Microsoft eMbedded Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SOCKFWD_H__313A5621_5135_461C_A471_5157B54C9DAE__INCLUDED_)
