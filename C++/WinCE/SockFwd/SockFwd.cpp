#include "stdafx.h"
#include "SockFwd.h"
#include "MainFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define MAJOR_VER 2
#define MINOR_VER 0

/////////////////////////////////////////////////////////////////////////////
// CSockFwdApp

BEGIN_MESSAGE_MAP(CSockFwdApp, CWinApp)
	//{{AFX_MSG_MAP(CSockFwdApp)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// The one and only CSockFwdApp object

CSockFwdApp theApp;
CRegPropertySerializer *CSockFwdApp::s_pPropertySerializer = NULL;
CForwarders *CSockFwdApp::s_pForwarders = NULL;

/////////////////////////////////////////////////////////////////////////////
// CSockFwdApp initialization

BOOL CSockFwdApp::InitInstance()
{
	WSADATA wsaData;
	bool bWsaErr = false;
	if (::WSAStartup(MAKEWORD(MAJOR_VER, MINOR_VER), &wsaData) != 0)
		bWsaErr = true;
	else if (LOBYTE(wsaData.wVersion) != MAJOR_VER || HIBYTE(wsaData.wVersion) != MINOR_VER)
		bWsaErr = true;
	if (bWsaErr)
	{
		AfxMessageBox(IDP_SOCKETS_INIT_FAILED);
		return FALSE;
	}

	s_pPropertySerializer = new CRegPropertySerializer(_T("HKEY_LOCAL_MACHINE\\Software\\Kamen\\SockFwd"));
	s_pForwarders = new CForwarders;
	CMainFrame* pFrame = new CMainFrame;
	m_pMainWnd = pFrame;
	pFrame->LoadFrame(IDR_MAINFRAME, WS_OVERLAPPEDWINDOW);
	pFrame->ShowWindow(m_nCmdShow);
	pFrame->UpdateWindow();

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CSockFwdApp message handlers

int CSockFwdApp::ExitInstance()
{
	delete s_pPropertySerializer;
	delete s_pForwarders;
	::WSACleanup();

	return CWinApp::ExitInstance();
}

/////////////////////////////////////////////////////////////////////////////

HWND CWinSockGlobalData::s_hSockWnd = NULL;
UINT CWinSockGlobalData::s_nSockMsgId = 0;
ILog *CWinSockGlobalData::s_pLogImpl = NULL;
