#if !defined(AFX_MAINFRM_H__716EBCA3_1832_4EFD_B865_41AF8EECC32F__INCLUDED_)
#define AFX_MAINFRM_H__716EBCA3_1832_4EFD_B865_41AF8EECC32F__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "ChildView.h"

/////////////////////////////////////////////////////////////////////////////
// CMainFrame

class CMainFrame : public CFrameWnd
{
public:
	CMainFrame();

protected: 
	DECLARE_DYNAMIC(CMainFrame)

	virtual ~CMainFrame();

public:
	//{{AFX_VIRTUAL(CMainFrame)
	virtual BOOL OnCmdMsg(UINT nID, int nCode, void* pExtra, AFX_CMDHANDLERINFO* pHandlerInfo);
	//}}AFX_VIRTUAL

protected:  // control bar embedded members
	CCeCommandBar	m_wndCommandBar;
	CChldView *m_pView1;
	CChldView *m_pView2;

	CView *m_pActiveView;

	void ActivateForm(CFormView *pForm);

protected:
	//{{AFX_MSG(CMainFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSetFocus(CWnd *pOldWnd);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnMsg();
	afx_msg void OnUpdateMsg(CCmdUI* pCmdUI);
	afx_msg void OnClient();
	afx_msg void OnUpdateClient(CCmdUI* pCmdUI);
	//}}AFX_MSG
	afx_msg LRESULT OnSocketMessage(WPARAM, LPARAM);
	DECLARE_MESSAGE_MAP()

private:
	void DestroyView(CChldView *&pView);
};

/////////////////////////////////////////////////////////////////////////////
//{{AFX_INSERT_LOCATION}}
// Microsoft eMbedded Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MAINFRM_H__716EBCA3_1832_4EFD_B865_41AF8EECC32F__INCLUDED_)
