#include "stdafx.h"
#include "SockFwd.h"
#include "MainFrm.h"
#include "Forwarder.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define WM_SOCKET (WM_USER + 1)

/////////////////////////////////////////////////////////////////////////////
// CMainFrame

IMPLEMENT_DYNAMIC(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	//{{AFX_MSG_MAP(CMainFrame)
	ON_WM_CREATE()
	ON_WM_SETFOCUS()
	ON_WM_SIZE()
	ON_COMMAND(ID_MSG, OnMsg)
	ON_UPDATE_COMMAND_UI(ID_MSG, OnUpdateMsg)
	ON_COMMAND(ID_CLIENT, OnClient)
	ON_UPDATE_COMMAND_UI(ID_CLIENT, OnUpdateClient)
	//}}AFX_MSG_MAP
	ON_MESSAGE(WM_SOCKET, OnSocketMessage)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMainFrame construction/destruction

CMainFrame::CMainFrame()
{
	m_pView1 = NULL;
	m_pView2 = NULL;

	m_pActiveView = NULL;
}

CMainFrame::~CMainFrame()
{
	DestroyView(m_pView1);
	DestroyView(m_pView2);
}

void CMainFrame::DestroyView(CChldView *&pView)
{
	if (pView != NULL)
	{
		pView->DestroyWindow();
		delete pView;
		pView = NULL;
	}
}

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	CWinSockGlobalData::s_hSockWnd = GetSafeHwnd();
	CWinSockGlobalData::s_nSockMsgId = WM_SOCKET;

	m_pView1 = new CChldView1;
	m_pView1->Create(this);
	m_pView2 = new CChldView2;
	m_pView2->Create(this);

	CWinSockGlobalData::s_pLogImpl = (CChldView1 *)m_pView1;

	if (!m_wndCommandBar.Create(this) ||
		!m_wndCommandBar.InsertMenuBar(IDR_MAINFRAME))
	{
		TRACE0("Failed to create CommandBar\n");
		return -1;      // fail to create
	}

	m_wndCommandBar.SetBarStyle(m_wndCommandBar.GetBarStyle() |
		CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_FIXED);

	OnMsg();

	return 0;
}

/////////////////////////////////////////////////////////////////////////////
// CMainFrame message handlers

void CMainFrame::OnSetFocus(CWnd *pOldWnd)
{
	m_pActiveView->SetFocus();
}

BOOL CMainFrame::OnCmdMsg(UINT nID, int nCode, void *pExtra, AFX_CMDHANDLERINFO *pHandlerInfo)
{
	if (m_pActiveView->OnCmdMsg(nID, nCode, pExtra, pHandlerInfo))
		return TRUE;

	return CFrameWnd::OnCmdMsg(nID, nCode, pExtra, pHandlerInfo);
}

void CMainFrame::OnSize(UINT nType, int cx, int cy) 
{
	CFrameWnd::OnSize(nType, cx, cy);
	
// @@@@ PocketPC
	CRect r;
	GetClientRect(&r);
	if (m_pActiveView != NULL)
		m_pActiveView->MoveWindow(&r);
// @@@@ PocketPC
// @@@@ CE.NET
// @@@@ CE.NET
}

void CMainFrame::ActivateForm(CFormView *pForm)
{
	SetActiveView(pForm);
// @@@@ PocketPC
	CRect r;
	GetClientRect(&r);
// @@@@ PocketPC
// @@@@ CE.NET
//	CRect rc;
//	GetClientRect(&rc);
//	CRect rcBar;
//	m_wndCommandBar.GetWindowRect(rcBar);
//	CRect r(rc.left, rcBar.Height(), rc.right, rc.bottom);
// @@@@ CE.NET
	pForm->MoveWindow(&r);
	if (m_pActiveView != NULL && m_pActiveView != pForm)
	{
		CRect r0;
		r0.SetRectEmpty();
		m_pActiveView->MoveWindow(&r0);
	}
	m_pActiveView = pForm;
}

void CMainFrame::OnMsg()
{
	ActivateForm(m_pView1);
}

void CMainFrame::OnUpdateMsg(CCmdUI *pCmdUI)
{
	pCmdUI->Enable(TRUE);
	pCmdUI->SetCheck(m_pActiveView == m_pView1 ? 1 : 0);
}

void CMainFrame::OnClient()
{
	ActivateForm(m_pView2);
}

void CMainFrame::OnUpdateClient(CCmdUI *pCmdUI)
{
	pCmdUI->Enable(TRUE);
	pCmdUI->SetCheck(m_pActiveView == m_pView2 ? 1 : 0);
}

LRESULT CMainFrame::OnSocketMessage(WPARAM wParam, LPARAM lParam)
{
	SOCKET sock = (SOCKET)wParam;
	int nErrCode = WSAGETSELECTERROR(lParam);
	int nEvent = WSAGETSELECTEVENT(lParam);
	switch (nEvent)
	{
	case FD_ACCEPT:
		CSockFwdApp::s_pForwarders->OnAccept(sock, nErrCode);
		break;

	case FD_READ:
		CSockFwdApp::s_pForwarders->OnRead(sock, nErrCode);
		break;

	case FD_WRITE:
		CSockFwdApp::s_pForwarders->OnWrite(sock, nErrCode);
		break;

	case FD_CLOSE:
		CSockFwdApp::s_pForwarders->OnClose(sock, nErrCode);
		break;

	case FD_CONNECT:
		CSockFwdApp::s_pForwarders->OnConnect(sock, nErrCode);
		break;
	}
	return (LRESULT)0;
}
