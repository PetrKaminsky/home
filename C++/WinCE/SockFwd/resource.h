//{{NO_DEPENDENCIES}}
// Microsoft eMbedded Visual C++ generated include file.
// Used by SockFwd.rc
//
#define IDP_SOCKETS_INIT_FAILED         104
#define IDR_MAINFRAME                   128
#define IDS_MESSAGE_COLUMN0             129
#define IDD_FORM_MSG                    130
#define IDS_MESSAGE_COLUMN1             130
#define IDD_FORM_CLI                    131
#define IDS_FORWARD_COLUMN0             131
#define IDS_FORWARD_COLUMN1             132
#define IDD_DIALOG_CLI                  132
#define IDS_FORWARD_COLUMN2             133
#define IDS_LISTENING                   134
#define IDS_FORMAT_ERROR                135
#define IDS_FORMAT_CLIENT               136
#define IDS_ACCEPTED                    137
#define IDS_CONNECTED                   138
#define IDS_DISCONNECTED                139
#define IDS_FORWARD_COLUMN3             140
#define IDS_FORWARD_COLUMN4             141
#define IDS_FAMILY_IPV4                 142
#define IDS_FAMILY_IPV6                 143
#define IDC_BUTTON_NEW                  1000
#define IDC_BUTTON_EDIT                 1001
#define IDC_BUTTON_DEL                  1002
#define IDC_LIST_FORWARD                1003
#define IDC_EDIT_LOC_PORT               1004
#define IDC_LIST_MESSAGE                1005
#define IDC_EDIT_REM_ADDR               1005
#define IDC_EDIT_REM_PORT               1006
#define IDC_RADIO_LOC_IPV4              1007
#define IDC_RADIO_LOC_IPV6              1008
#define IDC_RADIO_REM_IPV4              1009
#define IDC_RADIO_REM_IPV6              1010
#define IDC_BUTTON_DISC                 1011
#define IDC_BUTTON_STOP                 1012
#define ID_MSG                          32771
#define ID_CLIENT                       32772

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        142
#define _APS_NEXT_COMMAND_VALUE         32773
#define _APS_NEXT_CONTROL_VALUE         1013
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
