#include "stdafx.h"
#include "Mandel.h"
#include "MainFrm.h"
#include "Reg.h"
#include "Globals.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMandelApp

BEGIN_MESSAGE_MAP(CMandelApp, CWinApp)
	//{{AFX_MSG_MAP(CMandelApp)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// The one and only CMandelApp object

CMandelApp theApp;
CRegPropertySerializer *CMandelApp::s_pPropertySerializer = NULL;

/////////////////////////////////////////////////////////////////////////////
// CMandelApp initialization

BOOL CMandelApp::InitInstance()
{
	CGlobals::SipOff();

	::SetThreadPriority(::GetCurrentThread(), THREAD_PRIORITY_BELOW_NORMAL);

	s_pPropertySerializer = new CRegPropertySerializer(_T("HKEY_LOCAL_MACHINE\\Software\\Kamen\\Mandel"));
	CMainFrame* pFrame = new CMainFrame;
	m_pMainWnd = pFrame;
	pFrame->LoadFrame(IDR_MAINFRAME, WS_OVERLAPPEDWINDOW);
	pFrame->ShowWindow(m_nCmdShow);
	pFrame->UpdateWindow();

	return TRUE;
}

int CMandelApp::ExitInstance()
{
	delete s_pPropertySerializer;

	return CWinApp::ExitInstance();
}

/////////////////////////////////////////////////////////////////////////////

CMandelObj::CMandelObj(CMandelConfig &MandelConfig)
:	m_MandelConfig(MandelConfig)
{
	m_dRealMin = m_dImagMin = -1.0;
	m_dRealSize = m_dImagSize = 2.0;
	m_nMaxValue = 8;
}

void CMandelObj::Set(
	double dRealMin,
	double dImagMin,
	double dRealSize,
	double dImagRealRatio,
	int nMaxValue)
{
	m_dRealMin = dRealMin;
	m_dImagMin = dImagMin;
	m_dRealSize = dRealSize;
	m_dImagSize = m_dRealSize * dImagRealRatio;
	m_nMaxValue = nMaxValue;
	m_MandelConfig.SaveCoords(
		m_dRealMin,
		m_dImagMin,
		m_dRealSize);
}

int CMandelObj::GetMandelValue(double dRealRel, double dImagRel)
{
	return CalcMandelValue(
		m_dRealMin + dRealRel * m_dRealSize,
		m_dImagMin + dImagRel * m_dImagSize);
}

int CMandelObj::CalcMandelValue(double dReal, double dImag)
{
	int nRetVal = 0;
	double dRealCurr = dReal;
	double dImagCurr = dImag;
	while (true)
	{
		if (nRetVal >= m_nMaxValue)
			break;
		double dRealSqr = dRealCurr * dRealCurr;
		double dImagSqr = dImagCurr * dImagCurr;
		if (sqrt(dRealSqr + dImagSqr) >= 2)
			break;
		double dRealNew = dRealSqr - dImagSqr + dReal;
		double dImagNew = 2 * dRealCurr * dImagCurr + dImag;
		dRealCurr = dRealNew;
		dImagCurr = dImagNew;
		nRetVal++;
	}
	return nRetVal;
}

void CMandelObj::ZoomIn(double dBy)
{
	Zoom(1.0 + dBy);
}

void CMandelObj::ZoomOut(double dBy)
{
	Zoom(1.0 / (1.0 + dBy));
}

void CMandelObj::Zoom(double dBy)
{
	double dRealCenter = m_dRealMin + m_dRealSize / 2;
	double dImagCenter = m_dImagMin + m_dImagSize / 2;
	m_dRealSize /= dBy;
	m_dImagSize /= dBy;
	m_dRealMin = dRealCenter - m_dRealSize / 2;
	m_dImagMin = dImagCenter - m_dImagSize / 2;
	m_MandelConfig.SaveCoords(
		m_dRealMin,
		m_dImagMin,
		m_dRealSize);
}

void CMandelObj::MoveLeft(double dBy)
{
	MoveRight(-dBy);
}

void CMandelObj::MoveRight(double dBy)
{
	m_dRealMin += (m_dRealSize * dBy);
	m_MandelConfig.SaveCoords(
		m_dRealMin,
		m_dImagMin,
		m_dRealSize);
}

void CMandelObj::MoveUp(double dBy)
{
	m_dImagMin += (m_dImagSize * dBy);
	m_MandelConfig.SaveCoords(
		m_dRealMin,
		m_dImagMin,
		m_dRealSize);
}

void CMandelObj::MoveDown(double dBy)
{
	MoveUp(-dBy);
}

void CMandelObj::CenterZoomIn(
	double dMoveRightByRel,
	double dMoveUpByRel,
	double dZoomInBy)
{
	MoveRight(dMoveRightByRel - 0.5);
	MoveUp(dMoveUpByRel - 0.5);
	ZoomIn(dZoomInBy);
}

void CMandelObj::SetCoords(
	double dRealMin,
	double dImagMin,
	double dRealSize,
	double dImagRealRatio)
{
	m_dRealMin = dRealMin;
	m_dImagMin = dImagMin;
	m_dRealSize = dRealSize;
	m_dImagSize = m_dRealSize * dImagRealRatio;
	m_MandelConfig.SaveCoords(
		m_dRealMin,
		m_dImagMin,
		m_dRealSize);
}

void CMandelObj::SetMaxValue(int nMaxValue)
{
	m_nMaxValue = nMaxValue;
	m_MandelConfig.SaveMaxValue(m_nMaxValue);
}

/////////////////////////////////////////////////////////////////////////////

const double CMandelConfig::s_dDefaultRealMin = -2.5;
const double CMandelConfig::s_dDefaultImagMin = -1.95;
const double CMandelConfig::s_dDefaultRealSize = 3.5;
const int CMandelConfig::s_nDefaultMaxValue = 64;
const COLORREF CMandelConfig::s_clrDefaultBaseColour = RGB(0, 0, 0);
const bool CMandelConfig::s_bDefaultShowButtons = true;
const int CMandelConfig::s_nDefaultColourShades = 16;
const int CMandelConfig::s_nDefaultShiftColour = 0;
const int CMandelConfig::s_nDefaultMoveByPctg = 50;
const int CMandelConfig::s_nDefaultZoomByPctg = 100;

CMandelConfig::CMandelConfig()
:	m_RealMin(CMandelApp::s_pPropertySerializer, _T("RealMin"), s_dDefaultRealMin),
	m_ImagMin(CMandelApp::s_pPropertySerializer, _T("ImagMin"), s_dDefaultImagMin),
	m_RealSize(CMandelApp::s_pPropertySerializer, _T("RealSize"), s_dDefaultRealSize),
	m_MaxValue(CMandelApp::s_pPropertySerializer, _T("MaxValue"), s_nDefaultMaxValue),
	m_BaseColour(CMandelApp::s_pPropertySerializer, _T("BaseColour"), s_clrDefaultBaseColour),
	m_ShowButtons(CMandelApp::s_pPropertySerializer, _T("ShowButtons"), s_bDefaultShowButtons),
	m_ColourShades(CMandelApp::s_pPropertySerializer, _T("ColourShades"), s_nDefaultColourShades),
	m_ShiftColour(CMandelApp::s_pPropertySerializer, _T("ShiftColour"), s_nDefaultShiftColour),
	m_MoveBy(CMandelApp::s_pPropertySerializer, _T("MoveBy"), s_nDefaultMoveByPctg),
	m_ZoomBy(CMandelApp::s_pPropertySerializer, _T("ZoomBy"), s_nDefaultZoomByPctg)
{
	m_dRealMin = s_dDefaultRealMin;
	m_dImagMin = s_dDefaultImagMin;
	m_dRealSize = s_dDefaultRealSize;
	m_nMaxValue = s_nDefaultMaxValue;
	m_clrBase = s_clrDefaultBaseColour;
	m_bShowButtons = s_bDefaultShowButtons;
	m_nColourShades = s_nDefaultColourShades;
	m_nShiftColour = s_nDefaultShiftColour;
	m_nMoveByPctg = s_nDefaultMoveByPctg;
	m_nZoomByPctg = s_nDefaultZoomByPctg;
	m_nNormRed = 0;
	m_nNormGreen = 0;
	m_nNormBlue = 0;
	NormalizeColour();
}

void CMandelConfig::Load()
{
	m_dRealMin = m_RealMin.GetValue();
	m_dImagMin = m_ImagMin.GetValue();
	m_dRealSize = m_RealSize.GetValue();
	m_nMaxValue = m_MaxValue.GetValue();
	m_clrBase = (COLORREF)m_BaseColour.GetValue();
	m_bShowButtons = m_ShowButtons.GetValue();
	m_nColourShades = m_ColourShades.GetValue();
	m_nShiftColour = m_ShiftColour.GetValue();
	m_nMoveByPctg = m_MoveBy.GetValue();
	m_nZoomByPctg = m_ZoomBy.GetValue();
	NormalizeColour();
}

void CMandelConfig::SaveCoords(
	double dRealMin,
	double dImagMin,
	double dRealSize)
{
	m_dRealMin = dRealMin;
	m_dImagMin = dImagMin;
	m_dRealSize = dRealSize;
	m_RealMin.SetValue(m_dRealMin);
	m_ImagMin.SetValue(m_dImagMin);
	m_RealSize.SetValue(m_dRealSize);
}

void CMandelConfig::RestoreDefaultCoords()
{
	m_dRealMin = s_dDefaultRealMin;
	m_dImagMin = s_dDefaultImagMin;
	m_dRealSize = s_dDefaultRealSize;
}

void CMandelConfig::SaveColours(
	COLORREF clrBase,
	int nColourShades,
	int nShiftColour)
{
	m_clrBase = clrBase;
	m_nColourShades = nColourShades;
	m_nShiftColour = nShiftColour;
	m_BaseColour.SetValue(m_clrBase);
	m_ColourShades.SetValue(m_nColourShades);
	m_ShiftColour.SetValue(m_nShiftColour);
	NormalizeColour();
}

void CMandelConfig::SaveMaxValue(int nMaxValue)
{
	m_nMaxValue = nMaxValue;
	m_MaxValue.SetValue(m_nMaxValue);
}

void CMandelConfig::SaveShowButtons(bool bShowButtons)
{
	m_bShowButtons = bShowButtons;
	m_ShowButtons.SetValue(m_bShowButtons);
}

COLORREF CMandelConfig::CalcColour(int nValue)
{
	if (nValue < 0)
		return RGB(0, 0, 0);
	int nCorrValue = ((nValue + GetShiftColour()) * (256 / GetColourShades())) % 256;
	return RGB(
		nCorrValue * m_nNormRed / 255,
		nCorrValue * m_nNormGreen / 255,
		nCorrValue * m_nNormBlue / 255);
}

void CMandelConfig::SavePctgs(int nMoveByPctg, int nZoomByPctg)
{
	m_nMoveByPctg = nMoveByPctg;
	m_nZoomByPctg = nZoomByPctg;
	m_MoveBy.SetValue(m_nMoveByPctg);
	m_ZoomBy.SetValue(m_nZoomByPctg);
}

void CMandelConfig::NormalizeColour()
{
	int nRed = GetRValue(m_clrBase);
	int nGreen = GetGValue(m_clrBase);
	int nBlue = GetBValue(m_clrBase);
	int nMax = max(nRed, max(nGreen, nBlue));
	if (nMax == 0)
		nRed = nGreen = nBlue = nMax = 255;
	m_nNormRed = 255 * nRed / nMax;
	m_nNormGreen = 255 * nGreen / nMax;
	m_nNormBlue = 255 * nBlue / nMax;
}
