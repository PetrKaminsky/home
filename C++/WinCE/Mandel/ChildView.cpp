#include "stdafx.h"
#include "Mandel.h"
#include "ChildView.h"
#include "Globals.h"
#include <commdlg.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMandelView

IMPLEMENT_DYNCREATE(CMandelView, CFormView)

CMandelView::CMandelView()
:	CFormView(CMandelView::IDD),
	m_ctrlMandel(
		m_MandelObj,
		m_MandelArray,
		m_MandelConfig),
	m_MandelObj(m_MandelConfig)
{
	//{{AFX_DATA_INIT(CMandelView)
	//}}AFX_DATA_INIT
	m_bShowButtons = true;
	m_nWidth = -1;
	m_nHeight = -1;
}

CMandelView::~CMandelView()
{
	m_ctrlMandel.DestroyWindow();
}

void CMandelView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CMandelView)
	DDX_Control(pDX, IDC_BUTTON_UPRIGHT, m_btnUpRight);
	DDX_Control(pDX, IDC_BUTTON_UPLEFT, m_btnUpLeft);
	DDX_Control(pDX, IDC_BUTTON_DOWNRIGHT, m_btnDownRight);
	DDX_Control(pDX, IDC_BUTTON_DOWNLEFT, m_btnDownLeft);
	DDX_Control(pDX, IDC_BUTTON_ZOOMOUT, m_btnZoomOut);
	DDX_Control(pDX, IDC_BUTTON_ZOOMIN, m_btnZoomIn);
	DDX_Control(pDX, IDC_BUTTON_DOWN, m_btnDown);
	DDX_Control(pDX, IDC_BUTTON_UP, m_btnUp);
	DDX_Control(pDX, IDC_BUTTON_RIGHT, m_btnRight);
	DDX_Control(pDX, IDC_BUTTON_LEFT, m_btnLeft);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CMandelView, CFormView)
	//{{AFX_MSG_MAP(CMandelView)
	ON_BN_CLICKED(IDC_BUTTON_LEFT, OnButtonLeft)
	ON_BN_CLICKED(IDC_BUTTON_RIGHT, OnButtonRight)
	ON_BN_CLICKED(IDC_BUTTON_UP, OnButtonUp)
	ON_BN_CLICKED(IDC_BUTTON_DOWN, OnButtonDown)
	ON_BN_CLICKED(IDC_BUTTON_ZOOMIN, OnButtonZoomin)
	ON_BN_CLICKED(IDC_BUTTON_ZOOMOUT, OnButtonZoomout)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BUTTON_UPLEFT, OnButtonUpLeft)
	ON_BN_CLICKED(IDC_BUTTON_UPRIGHT, OnButtonUpRight)
	ON_BN_CLICKED(IDC_BUTTON_DOWNLEFT, OnButtonDownLeft)
	ON_BN_CLICKED(IDC_BUTTON_DOWNRIGHT, OnButtonDownRight)
	ON_COMMAND(ID_MOVE_DOWN, OnMoveDown)
	ON_COMMAND(ID_MOVE_LEFT, OnMoveLeft)
	ON_COMMAND(ID_MOVE_RIGHT, OnMoveRight)
	ON_COMMAND(ID_MOVE_UP, OnMoveUp)
	ON_UPDATE_COMMAND_UI(ID_SHOW_BUTTONS, OnUpdateShowButtons)
	ON_COMMAND(ID_SHOW_BUTTONS, OnShowButtons)
	ON_COMMAND(ID_ZOOM_IN, OnZoomIn)
	ON_COMMAND(ID_ZOOM_OUT, OnZoomOut)
	ON_COMMAND(ID_RESTART, OnRestart)
	ON_COMMAND(ID_SETTINGS, OnSettings)
	ON_WM_SIZE()
	//}}AFX_MSG_MAP
	ON_MESSAGE(WM_CALC_MANDEL, OnCalcMandel)
	ON_MESSAGE(WM_CENTER_ZOOM, OnCenterZoom)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMandelView message handlers

int CMandelView::Create(CWnd *pParent)
{
	return CFormView::Create(
		NULL,
		NULL,
		WS_CHILD | WS_VISIBLE,
		CRect(0, 0, 0, 0),
		pParent,
		AFX_IDW_PANE_FIRST,
		NULL);
}

void CMandelView::OnButtonLeft()
{
	m_MandelObj.MoveLeft(m_MandelConfig.GetMoveBy());
	RestartCalc();
}

void CMandelView::OnButtonRight()
{
	m_MandelObj.MoveRight(m_MandelConfig.GetMoveBy());
	RestartCalc();
}

void CMandelView::OnButtonUp()
{
	m_MandelObj.MoveUp(m_MandelConfig.GetMoveBy());
	RestartCalc();
}

void CMandelView::OnButtonDown()
{
	m_MandelObj.MoveDown(m_MandelConfig.GetMoveBy());
	RestartCalc();
}

void CMandelView::OnButtonZoomin()
{
	m_MandelObj.ZoomIn(m_MandelConfig.GetZoomBy());
	RestartCalc();
}

void CMandelView::OnButtonZoomout()
{
	m_MandelObj.ZoomOut(m_MandelConfig.GetZoomBy());
	RestartCalc();
}

void CMandelView::OnButtonUpLeft()
{
	double dMoveBy = m_MandelConfig.GetMoveBy();
	m_MandelObj.MoveUp(dMoveBy);
	m_MandelObj.MoveLeft(dMoveBy);
	RestartCalc();
}

void CMandelView::OnButtonUpRight()
{
	double dMoveBy = m_MandelConfig.GetMoveBy();
	m_MandelObj.MoveUp(dMoveBy);
	m_MandelObj.MoveRight(dMoveBy);
	RestartCalc();
}

void CMandelView::OnButtonDownLeft()
{
	double dMoveBy = m_MandelConfig.GetMoveBy();
	m_MandelObj.MoveDown(dMoveBy);
	m_MandelObj.MoveLeft(dMoveBy);
	RestartCalc();
}

void CMandelView::OnButtonDownRight()
{
	double dMoveBy = m_MandelConfig.GetMoveBy();
	m_MandelObj.MoveDown(dMoveBy);
	m_MandelObj.MoveRight(dMoveBy);
	RestartCalc();
}

void CMandelView::OnMoveDown()
{
	OnButtonDown();
}

void CMandelView::OnMoveLeft()
{
	OnButtonLeft();
}

void CMandelView::OnMoveRight()
{
	OnButtonRight();
}

void CMandelView::OnMoveUp()
{
	OnButtonUp();
}

void CMandelView::OnUpdateShowButtons(CCmdUI* pCmdUI)
{
	pCmdUI->SetCheck(m_bShowButtons ? 1 : 0);
}

void CMandelView::OnShowButtons()
{
	m_bShowButtons = !m_bShowButtons;
	m_MandelConfig.SaveShowButtons(m_bShowButtons);
	ShowButtons();
}

void CMandelView::ShowButtons()
{
	m_btnDown.ShowWindow(m_bShowButtons ? SW_SHOW : SW_HIDE);
	m_btnUp.ShowWindow(m_bShowButtons ? SW_SHOW : SW_HIDE);
	m_btnRight.ShowWindow(m_bShowButtons ? SW_SHOW : SW_HIDE);
	m_btnLeft.ShowWindow(m_bShowButtons ? SW_SHOW : SW_HIDE);
	m_btnUpRight.ShowWindow(m_bShowButtons ? SW_SHOW : SW_HIDE);
	m_btnUpLeft.ShowWindow(m_bShowButtons ? SW_SHOW : SW_HIDE);
	m_btnDownRight.ShowWindow(m_bShowButtons ? SW_SHOW : SW_HIDE);
	m_btnDownLeft.ShowWindow(m_bShowButtons ? SW_SHOW : SW_HIDE);
	m_btnZoomOut.ShowWindow(m_bShowButtons ? SW_SHOW : SW_HIDE);
	m_btnZoomIn.ShowWindow(m_bShowButtons ? SW_SHOW : SW_HIDE);

	if (m_bShowButtons)
	{
		CRect r, r1;
		GetClientRect(&r);

		m_btnDown.GetClientRect(&r1);
		m_btnDown.ClientToScreen(&r1);
		int nVertShift = r.Height() - r1.bottom - 3;
		r1.OffsetRect(0, nVertShift);
		m_btnDown.MoveWindow(r1);

		m_btnUp.GetClientRect(&r1);
		m_btnUp.ClientToScreen(&r1);
		r1.OffsetRect(0, nVertShift);
		m_btnUp.MoveWindow(r1);

		m_btnRight.GetClientRect(&r1);
		m_btnRight.ClientToScreen(&r1);
		r1.OffsetRect(0, nVertShift);
		m_btnRight.MoveWindow(r1);

		m_btnLeft.GetClientRect(&r1);
		m_btnLeft.ClientToScreen(&r1);
		r1.OffsetRect(0, nVertShift);
		m_btnLeft.MoveWindow(r1);

		m_btnUpRight.GetClientRect(&r1);
		m_btnUpRight.ClientToScreen(&r1);
		r1.OffsetRect(0, nVertShift);
		m_btnUpRight.MoveWindow(r1);

		m_btnUpLeft.GetClientRect(&r1);
		m_btnUpLeft.ClientToScreen(&r1);
		r1.OffsetRect(0, nVertShift);
		m_btnUpLeft.MoveWindow(r1);

		m_btnDownRight.GetClientRect(&r1);
		m_btnDownRight.ClientToScreen(&r1);
		r1.OffsetRect(0, nVertShift);
		m_btnDownRight.MoveWindow(r1);

		m_btnDownLeft.GetClientRect(&r1);
		m_btnDownLeft.ClientToScreen(&r1);
		r1.OffsetRect(0, nVertShift);
		m_btnDownLeft.MoveWindow(r1);

		m_btnZoomOut.GetClientRect(&r1);
		m_btnZoomOut.ClientToScreen(&r1);
		r1.OffsetRect(0, nVertShift);
		m_btnZoomOut.MoveWindow(r1);

		m_btnZoomIn.GetClientRect(&r1);
		m_btnZoomIn.ClientToScreen(&r1);
		r1.OffsetRect(0, nVertShift);
		m_btnZoomIn.MoveWindow(r1);
	}
}

void CMandelView::OnZoomIn()
{
	OnButtonZoomin();
}

void CMandelView::OnZoomOut()
{
	OnButtonZoomout();
}

void CMandelView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();

	CRect r;
	GetClientRect(&r);
	m_MandelConfig.Load();

	m_btnDown.SetIcon(IDI_ICON_DOWN, 16, 16);
	m_btnUp.SetIcon(IDI_ICON_UP, 16, 16);
	m_btnRight.SetIcon(IDI_ICON_RIGHT, 16, 16);
	m_btnLeft.SetIcon(IDI_ICON_LEFT, 16, 16);
	m_btnUpRight.SetIcon(IDI_ICON_UPRIGHT, 16, 16);
	m_btnUpLeft.SetIcon(IDI_ICON_UPLEFT, 16, 16);
	m_btnDownRight.SetIcon(IDI_ICON_DOWNRIGHT, 16, 16);
	m_btnDownLeft.SetIcon(IDI_ICON_DOWNLEFT, 16, 16);
	m_btnZoomOut.SetIcon(IDI_ICON_ZOOMOUT, 16, 16);
	m_btnZoomIn.SetIcon(IDI_ICON_ZOOMIN, 16, 16);
	m_bShowButtons = m_MandelConfig.ShowButtons();

	m_ctrlMandel.SubclassDlgItem(IDC_CUSTOM_MANDEL, this);
	m_ctrlMandel.MoveWindow(&r);
	m_MandelObj.Set(
		m_MandelConfig.GetRealMin(),
		m_MandelConfig.GetImagMin(),
		m_MandelConfig.GetRealSize(),
		(double)r.Height() / r.Width(),
		m_MandelConfig.GetMaxValue());
	m_MandelArray.Init(r.Width(), r.Height());
	m_nMaxX = r.Width();
	m_nMaxY = r.Height();

	ShowButtons();

	RestartCalc();
}

LRESULT CMandelView::OnCalcMandel(WPARAM wParam, LPARAM lParam)
{
	KillTimer(CALC_TIMER);
	StepCalc();
	return 0;
}

void CMandelView::PostCalcMandel()
{
	SetTimer(CALC_TIMER, 1, NULL);
}

void CMandelView::RestartCalc()
{
	m_MandelArray.Clear();
	KillTimer(CALC_TIMER);
	m_ctrlMandel.Erase();
	m_nCurrY = 0;
	PostCalcMandel();
}

void CMandelView::StepCalc()
{
	int nCurrX;
	for (nCurrX = 0; nCurrX < m_nMaxX; nCurrX++)
	{
		double dRealRel = (double)nCurrX / m_nMaxX;
		double dImagRel = 1.0 - (double)m_nCurrY / m_nMaxY;
		int nMandValue = m_MandelObj.GetMandelValue(dRealRel, dImagRel);
		m_MandelArray.SetValue(nCurrX, m_nCurrY, nMandValue);
	}
	m_ctrlMandel.PaintLine(m_nCurrY);
	m_nCurrY++;
	if (m_nCurrY < m_nMaxY)
		PostCalcMandel();
}

void CMandelView::OnTimer(UINT nIDEvent) 
{
	if (nIDEvent == CALC_TIMER)
		PostMessage(WM_CALC_MANDEL);
}

LRESULT CMandelView::OnCenterZoom(WPARAM wParam, LPARAM lParam)
{
	int x = wParam;
	int y = lParam;
	m_MandelObj.CenterZoomIn(
		(double)x / m_nMaxX,
		(double)(m_nMaxY - y) / m_nMaxY,
		m_MandelConfig.GetZoomBy());
	RestartCalc();
	return 0;
}

void CMandelView::OnRestart()
{
	CRect r;
	m_ctrlMandel.GetClientRect(&r);
	m_MandelConfig.RestoreDefaultCoords();
	m_MandelObj.SetCoords(
		m_MandelConfig.GetRealMin(),
		m_MandelConfig.GetImagMin(),
		m_MandelConfig.GetRealSize(),
		(double)r.Height() / r.Width());
	RestartCalc();
}

void CMandelView::OnSettings()
{
	CSettingsDlg dlg(
		this,
		m_MandelConfig.GetMaxValue(),
		m_MandelConfig.GetClrBase(),
		m_MandelConfig.GetColourShades(),
		m_MandelConfig.GetShiftColour(),
		m_MandelConfig.GetMoveByPctg(),
		m_MandelConfig.GetZoomByPctg());
	if (dlg.DoModal())
	{
		int nMaxValue = dlg.GetMaxValue();
		COLORREF clrBase = dlg.GetClrBase();
		int nColourShades = dlg.GetColourShades();
		int nShiftColour = dlg.GetShiftColour();
		int nMoveByPctg = dlg.GetMoveByPctg();
		int nZoomByPctg = dlg.GetZoomByPctg();
		if (nMoveByPctg != m_MandelConfig.GetMoveByPctg() ||
			nZoomByPctg != m_MandelConfig.GetZoomByPctg())
			m_MandelConfig.SavePctgs(nMoveByPctg, nZoomByPctg);
		bool bChangedColor = false;
		if (clrBase != m_MandelConfig.GetClrBase() ||
			nColourShades != m_MandelConfig.GetColourShades() ||
			nShiftColour != m_MandelConfig.GetShiftColour())
		{
			m_MandelConfig.SaveColours(
				clrBase,
				nColourShades,
				nShiftColour);
			bChangedColor = true;
		}
		if (nMaxValue != m_MandelConfig.GetMaxValue())
		{
			m_MandelObj.SetMaxValue(nMaxValue);
			RestartCalc();
			return;
		}
		if (bChangedColor)
		{
			m_ctrlMandel.Update();
			m_ctrlMandel.Paint();
		}
	}
}

void CMandelView::OnSize(UINT nType, int cx, int cy)
{
	CFormView::OnSize(nType, cx, cy);

	if (cx == 0 && cy == 0)
		return;

	if (m_ctrlMandel.GetSafeHwnd() == NULL)
		return;

	if (m_nWidth == -1 &&
		m_nHeight == -1)
	{
		m_nWidth = cx;
		m_nHeight = cy;
	}
	else if (m_nWidth != cx ||
		m_nHeight < cy)
	{
		// orientation changed or SIP dismissed
		m_nWidth = cx;
		m_nHeight = cy;
		m_ctrlMandel.MoveWindow(0, 0, cx, cy);
		m_MandelArray.Init(cx, cy);
		m_nMaxX = cx;
		m_nMaxY = cy;
		m_MandelObj.SetCoords(
			m_MandelConfig.GetRealMin(),
			m_MandelConfig.GetImagMin(),
			m_MandelConfig.GetRealSize(),
			(double)cy / cx);
		RestartCalc();
	}
}

/////////////////////////////////////////////////////////////////////////////
// CMandelCtrl

CMandelCtrl::CMandelCtrl(
		CMandelObj &MandelObj,
		CMandelArray &MandelArray,
		CMandelConfig &MandelConfig)
:	m_MandelObj(MandelObj),
	m_MandelArray(MandelArray),
	m_MandelConfig(MandelConfig)
{
}

BEGIN_MESSAGE_MAP(CMandelCtrl, CWnd)
	//{{AFX_MSG_MAP(CMandelCtrl)
	ON_WM_PAINT()
	ON_WM_ERASEBKGND()
	ON_WM_CREATE()
	ON_WM_LBUTTONDOWN()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMandelCtrl message handlers

void CMandelCtrl::OnPaint()
{
	CPaintDC dc(this);
	CRect r = dc.m_ps.rcPaint;
	int x, y;
	for (y = r.top; y < r.bottom; y++)
		for (x = r.left; x < r.right; x++)
		{
			int nValue = m_MandelArray.GetValue(x, y);
			if (nValue == CMandelArray::s_nEmpty)
			{
				CBrush br;
				br.CreateSolidBrush(RGB(0, 0, 0));
				CRect r2(r.left, y, r.right, r.bottom);
				dc.FillRect(&r2, &br);
				return;
			}
			COLORREF clr = m_MandelConfig.CalcColour(nValue);
			dc.SetPixel(x, y, clr);
/*
			CBrush br;
			br.CreateSolidBrush(clr);
			CRect r1(x, y, x + 1, y + 1);
			dc.FillRect(&r1, &br);
*/
		}
/*
	CPaintDC dc(this);
	CDC dcMem;
	dcMem.CreateCompatibleDC(&dc);
	CRect r = dc.m_ps.rcPaint;
	CBitmap *pOldBmp = dcMem.SelectObject(GetBitmap());
	dc.BitBlt(r.left,
		r.top,
		r.right - r.left,
		r.bottom - r.top,
		&dcMem,
		r.left,
		r.top,
		SRCCOPY);
	dcMem.SelectObject(pOldBmp);
*/
}

void CMandelCtrl::Erase()
{
	m_Rect.SetRectEmpty();
	Invalidate();
//	PumpMessages();
/*
	CRect r = GetRect();
	CDC dcMem;
	CClientDC *pDC = new CClientDC(this);
	dcMem.CreateCompatibleDC(pDC);
	delete pDC;
	CBitmap *pOldBmp = dcMem.SelectObject(GetBitmap());
	CBrush br;
	br.CreateSolidBrush(RGB(0, 0, 0));
	dcMem.FillRect(&r, &br);
	dcMem.SelectObject(pOldBmp);
	Invalidate();
	PumpMessages();
*/
}

void CMandelCtrl::PaintLine(int y)
{
	CRect r2(0, y, GetRect().Width(), y + 1);
	InvalidateRect(r2);
//	PumpMessages();
/*
	CRect r = GetRect();
	CDC dcMem;
	CClientDC *pDC = new CClientDC(this);
	dcMem.CreateCompatibleDC(pDC);
	delete pDC;
	CBitmap *pOldBmp = dcMem.SelectObject(GetBitmap());
	int x, n = r.Width();
	for (x = 0; x < n; x++)
	{
		int nValue = m_MandelArray.GetValue(x, y);
		COLORREF clr = CalcColor(nValue);
		CBrush br;
		br.CreateSolidBrush(clr);
		CRect r1(x, y, x + 1, y + 1);
		dcMem.FillRect(&r1, &br);
	}
	dcMem.SelectObject(pOldBmp);
	CRect r2(0, y, r.Width(), y + 1);
	InvalidateRect(r2);
	PumpMessages();
*/
}

void CMandelCtrl::Paint()
{
/*
	CRect r = GetRect();
	CDC dcMem;
	CClientDC *pDC = new CClientDC(this);
	dcMem.CreateCompatibleDC(pDC);
	delete pDC;
	CBitmap *pOldBmp = dcMem.SelectObject(GetBitmap());
	int x, y, nX = r.Width(), nY = r.Height();
	for (y = 0; y < nY; y++)
		for (x = 0; x < nX; x++)
		{
			int nValue = m_MandelArray.GetValue(x, y);
			COLORREF clr = CalcColor(nValue);
			CBrush br;
			br.CreateSolidBrush(clr);
			CRect r1(x, y, x + 1, y + 1);
			dcMem.FillRect(&r1, &br);
		}
	dcMem.SelectObject(pOldBmp);
	Invalidate();
	PumpMessages();
*/
}

void CMandelCtrl::Update()
{
	Invalidate();
//	PumpMessages();
}

BOOL CMandelCtrl::OnEraseBkgnd(CDC* pDC)
{
	CBrush br;
	br.CreateSolidBrush(RGB(0, 0, 0));
	CRect r = GetRect();
	pDC->FillRect(&r, &br);
	return TRUE;
//	return CWnd::OnEraseBkgnd(pDC);
/*
	return TRUE;
*/
}

CRect &CMandelCtrl::GetRect()
{
	if (m_Rect.IsRectEmpty())
		GetClientRect(&m_Rect);
	return m_Rect;
}

CBitmap *CMandelCtrl::GetBitmap()
{
	if (m_bmpOff.GetSafeHandle() == NULL)
	{
		CRect r = GetRect();
		CClientDC dc(this);
		m_bmpOff.CreateCompatibleBitmap(&dc, r.Width(), r.Height());
	}
	return &m_bmpOff;
}

void CMandelCtrl::OnLButtonDown(UINT nFlags, CPoint point)
{
	GetParent()->PostMessage(WM_CENTER_ZOOM, point.x, point.y);

	CWnd::OnLButtonDown(nFlags, point);
}

void CMandelCtrl::PumpMessages()
{
	MSG msg;
	HWND hWnd = GetSafeHwnd();
	while (::PeekMessage(&msg, hWnd,  0, 0, PM_REMOVE) == TRUE)
	{
		::TranslateMessage(&msg); 
		::DispatchMessage(&msg); 
	}
}

//////////////////////////////////////////////////////////////////////
// CMandelArray Class

CMandelArray::CMandelArray()
{
	m_pArray = NULL;
	m_nWidth = 0;
	m_nHeight = 0;
}

CMandelArray::~CMandelArray()
{
	if (m_pArray != m_pArray)
	{
		delete []m_pArray;
		m_pArray = NULL;
	}
}

void CMandelArray::Init(int nWidth, int nHeight)
{
	if (m_pArray != m_pArray)
		delete []m_pArray;
	m_nWidth = nWidth;
	m_nHeight = nHeight;
	m_pArray = new int [m_nWidth * m_nHeight];
	Clear();
}

void CMandelArray::Clear()
{
	int i, n = m_nWidth * m_nHeight;
	for (i = 0; i < n; i++)
		m_pArray[i] = s_nEmpty;
}

int CMandelArray::GetValue(int x, int y)
{
	return m_pArray[x + y * m_nWidth];
}

void CMandelArray::SetValue(int x, int y, int Value)
{
	m_pArray[x + y * m_nWidth] = Value;
}

const int CMandelArray::s_nEmpty = -1;

/////////////////////////////////////////////////////////////////////////////
// CSettingsDlg dialog

CSettingsDlg::CSettingsDlg(
		CWnd *pParent,
		int nMaxValue,
		COLORREF clrBase,
		int nColourShades,
		int nShiftColour,
		int nMoveByPctg,
		int nZoomByPctg)
:	CDialog(CSettingsDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSettingsDlg)
	//}}AFX_DATA_INIT
	m_nMaxValue = nMaxValue;
	m_clrBase = clrBase;
	m_nClrShades = nColourShades;
	m_nShiftColour = nShiftColour;
	m_nMoveByPctg = nMoveByPctg;
	m_nZoomByPctg = nZoomByPctg;
}

void CSettingsDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSettingsDlg)
	DDX_Control(pDX, IDC_COMBO_ZOOM_BY, m_ctrlZoomBy);
	DDX_Control(pDX, IDC_COMBO_MOVE_BY, m_ctrlMoveBy);
	DDX_Control(pDX, IDC_COMBO_SHIFT_COL, m_ctrlShiftClr);
	DDX_Control(pDX, IDC_COMBO_CLR_SHD, m_ctrlClrShades);
	DDX_Control(pDX, IDC_COMBO_MAX_ITER, m_ctrlMaxIter);
	DDX_Control(pDX, IDC_STATIC_COLOR, m_ctrlColor);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CSettingsDlg, CDialog)
	//{{AFX_MSG_MAP(CSettingsDlg)
	ON_WM_CTLCOLOR()
	ON_WM_LBUTTONDOWN()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSettingsDlg message handlers

BOOL CSettingsDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	SetComboCurValue(m_ctrlMaxIter, m_nMaxValue);
	SetComboCurValue(m_ctrlClrShades, m_nClrShades);
	SetComboValue(m_ctrlShiftClr, m_nShiftColour);
	SetComboCurValue(m_ctrlMoveBy, m_nMoveByPctg);
	SetComboCurValue(m_ctrlZoomBy, m_nZoomByPctg);

	m_Brush.CreateSolidBrush(m_clrBase);

	return TRUE;
}

HBRUSH CSettingsDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	if (pWnd->GetSafeHwnd() == m_ctrlColor.GetSafeHwnd())
		return m_Brush;

	return CDialog::OnCtlColor(pDC, pWnd, nCtlColor);
}

void CSettingsDlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	CRect r;
	m_ctrlColor.GetWindowRect(&r);
	ClientToScreen(&point);
	if (r.PtInRect(point))
	{
		CHOOSECOLOR cc;
		ZeroMemory(&cc, sizeof(cc));
		cc.lStructSize = sizeof(cc);
		cc.hwndOwner = this->GetSafeHwnd();
		cc.rgbResult = m_clrBase;
		COLORREF Cust[16];
		ZeroMemory(&Cust, sizeof(Cust));
		cc.lpCustColors = Cust;
		cc.Flags = CC_FULLOPEN | CC_RGBINIT | CC_ANYCOLOR;
		if (::ChooseColor(&cc) != FALSE)
		{
			m_clrBase = cc.rgbResult;
			m_Brush.DeleteObject();
			m_Brush.CreateSolidBrush(m_clrBase);
			m_ctrlColor.Invalidate();
		}
		return;
	}

	CDialog::OnLButtonDown(nFlags, point);
}

void CSettingsDlg::OnOK()
{
	GetComboCurValue(m_ctrlMaxIter, m_nMaxValue);
	GetComboCurValue(m_ctrlClrShades, m_nClrShades);
	GetComboValue(m_ctrlShiftClr, m_nShiftColour);
	GetComboCurValue(m_ctrlMoveBy, m_nMoveByPctg);
	GetComboCurValue(m_ctrlZoomBy, m_nZoomByPctg);

	CDialog::OnOK();
}

void CSettingsDlg::SetComboCurValue(CComboBox &cb, int nValue)
{
	int nItem = cb.FindString(-1, CGlobals::ToString(nValue));
	if (nItem != CB_ERR)
		cb.SetCurSel(nItem);
}

void CSettingsDlg::GetComboCurValue(CComboBox &cb, int &nValue)
{
	int nItem = cb.GetCurSel();
	if (nItem != CB_ERR)
	{
		CString s;
		cb.GetLBText(nItem, s);
		nValue = CGlobals::FromString(s);
	}
}

void CSettingsDlg::SetComboValue(CComboBox &cb, int nValue)
{
	cb.SetWindowText(CGlobals::ToString(nValue));
}

void CSettingsDlg::GetComboValue(CComboBox &cb, int &nValue)
{
	CString s;
	cb.GetWindowText(s);
	nValue = CGlobals::FromString(s);
}
