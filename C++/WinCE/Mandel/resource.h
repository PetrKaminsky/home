//{{NO_DEPENDENCIES}}
// Microsoft eMbedded Visual C++ generated include file.
// Used by Mandel.rc
//
#define IDD_MANDELVIEW_FORM             103
#define IDR_MAINFRAME                   128
#define IDI_ICON_LEFT                   130
#define IDI_ICON_RIGHT                  131
#define IDD_DIALOG_SETTINGS             131
#define IDI_ICON_UP                     132
#define IDI_ICON_DOWN                   133
#define IDI_ICON_ZOOMIN                 134
#define IDI_ICON_ZOOMOUT                135
#define IDI_ICON_UPLEFT                 136
#define IDI_ICON_UPRIGHT                137
#define IDI_ICON_DOWNLEFT               138
#define IDI_ICON_DOWNRIGHT              139
#define IDC_BUTTON_LEFT                 1000
#define IDC_BUTTON_RIGHT                1001
#define IDC_BUTTON_UP                   1002
#define IDC_BUTTON_DOWN                 1003
#define IDC_BUTTON_ZOOMIN               1004
#define IDC_BUTTON_ZOOMOUT              1005
#define IDC_CUSTOM_MANDEL               1006
#define IDC_BUTTON_UPLEFT               1007
#define IDC_BUTTON_DOWNLEFT             1008
#define IDC_BUTTON_UPRIGHT              1009
#define IDC_BUTTON_DOWNRIGHT            1010
#define IDC_COMBO_MAX_ITER              1011
#define IDC_STATIC_COLOR                1012
#define IDC_COMBO_CLR_SHD               1014
#define IDC_COMBO_SHIFT_COL             1015
#define IDC_COMBO_MOVE_BY               1016
#define IDC_COMBO_ZOOM_BY               1017
#define ID_MOVE_LEFT                    32771
#define ID_MOVE_RIGHT                   32772
#define ID_MOVE_UP                      32773
#define ID_MOVE_DOWN                    32774
#define ID_ZOOM_IN                      32775
#define ID_ZOOM_OUT                     32776
#define ID_SHOW_BUTTONS                 32777
#define ID_RESTART                      32778
#define ID_SETTINGS                     32779

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        133
#define _APS_NEXT_COMMAND_VALUE         32780
#define _APS_NEXT_CONTROL_VALUE         1018
#define _APS_NEXT_SYMED_VALUE           104
#endif
#endif
