#include "stdafx.h"
#include "PictureButton.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPictureButton

CPictureButton::CPictureButton()
{
	m_hIcon = NULL;
	m_bIsFlat = FALSE;
}

CPictureButton::~CPictureButton()
{
	if (m_hIcon != NULL)
		DestroyIcon(m_hIcon);
}

BEGIN_MESSAGE_MAP(CPictureButton, CButton)
	//{{AFX_MSG_MAP(CPictureButton)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

///////////////////////////////////////////////////////////////////////////////

void CPictureButton::SetIcon(UINT nIconID, int cx, int cy)
{
	m_hIcon = (HICON)LoadImage(
		AfxGetResourceHandle(),
		MAKEINTRESOURCE(nIconID),
		IMAGE_ICON,
		cx,
		cy,
		0);
	m_iconSize.cx = cx;
	m_iconSize.cy = cy;
}

void CPictureButton::DrawItem(LPDRAWITEMSTRUCT lpDis) 
{
	VERIFY(lpDis->CtlType == ODT_BUTTON);
	
	UINT uStyle = DFCS_BUTTONPUSH;
	CRect r = lpDis->rcItem;
	
	if (lpDis->itemState & ODS_SELECTED)
	{
		uStyle |= DFCS_PUSHED;
		r.OffsetRect(1,1);
	}
	
// Draw the button frame.
	if (m_bIsFlat)
	{
		::FillRect(lpDis->hDC, &lpDis->rcItem, (HBRUSH)GetStockObject(LTGRAY_BRUSH));
		::DrawEdge( lpDis->hDC, &lpDis->rcItem, EDGE_ETCHED, BF_RECT);
	}
	else
		::DrawFrameControl(lpDis->hDC, &lpDis->rcItem, DFC_BUTTON, uStyle);
	
// Get the button's text.
	CString str;
	CSize strSize;
	
	GetWindowText(str);
	::GetTextExtentPoint(lpDis->hDC, str, str.GetLength(), &strSize);
	
	SetTextColor(lpDis->hDC, ::GetSysColor(IsWindowEnabled() ? COLOR_BTNTEXT : COLOR_GRAYTEXT));
	SetBkMode(lpDis->hDC, TRANSPARENT);

// draw icon & text	
	if (m_hIcon != NULL)
	{
		int x, y;
		x = r.left + (r.Width() - m_iconSize.cx - (str.IsEmpty() ? 0 : (strSize.cx + 3)))/2;
		y = r.top + (r.Height() - m_iconSize.cy)/2;
		::DrawIconEx(lpDis->hDC, x, y, m_hIcon, 0, 0, 0, NULL, DI_NORMAL); 
		
		if (!str.IsEmpty())
		{
			r.left = x + m_iconSize.cx + 3;
			::DrawText(lpDis->hDC, str, str.GetLength(), &r, DT_LEFT|DT_VCENTER|DT_SINGLELINE);
		}
	}
	else
		::DrawText(lpDis->hDC, str, str.GetLength(), &r, DT_CENTER|DT_VCENTER|DT_SINGLELINE);
}
