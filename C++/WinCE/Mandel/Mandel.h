#if !defined(AFX_MANDEL_H__045600DE_CDF5_44B8_B0F4_7E9398D2FF71__INCLUDED_)
#define AFX_MANDEL_H__045600DE_CDF5_44B8_B0F4_7E9398D2FF71__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols
#include "Property.h"

int CalcMandelValue(double dReal, double dImag, int nMaxValue);

/////////////////////////////////////////////////////////////////////////////
// CMandelApp:
//

class CMandelApp : public CWinApp
{
public:
	static CRegPropertySerializer *s_pPropertySerializer;

	//{{AFX_VIRTUAL(CMandelApp)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	//}}AFX_VIRTUAL

	//{{AFX_MSG(CMandelApp)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

class CMandelConfig;

class CMandelObj
{
public:
	CMandelObj(CMandelConfig &m_MandelConfig);

	void Set(
		double dRealMin,
		double dImagMin,
		double dRealSize,
		double dImagRealRatio,
		int nMaxValue);
	int GetMandelValue(double dRealRel, double dImagRel);
	int CalcMandelValue(double dReal, double dImag);
	void ZoomIn(double dBy);
	void ZoomOut(double dBy);
	void MoveLeft(double dBy);
	void MoveRight(double dBy);
	void MoveUp(double dBy);
	void MoveDown(double dBy);
	void CenterZoomIn(
		double dMoveRightByRel,
		double dMoveUpByRel,
		double dZoomInBy);
	void SetCoords(
		double dRealMin,
		double dImagMin,
		double dRealSize,
		double dImagRealRatio);
	void SetMaxValue(int nMaxValue);

private:
	double m_dRealMin;
	double m_dImagMin;
	double m_dRealSize;
	double m_dImagSize;
	int m_nMaxValue;
	CMandelConfig &m_MandelConfig;

	void Zoom(double dBy);
};

/////////////////////////////////////////////////////////////////////////////

class CMandelConfig
{
public:
	CMandelConfig();

	void Load();
	void SaveCoords(
		double dRealMin,
		double dImagMin,
		double dRealSize);
	void SaveColours(
		COLORREF clrBase,
		int nColourShades,
		int nShiftColour);
	void SaveMaxValue(int nMaxValue);
	void SaveShowButtons(bool bShowButtons);
	void SavePctgs(int nMoveByPctg, int nZoomByPctg);
	void RestoreDefaultCoords();
	COLORREF CalcColour(int nValue);

	double GetRealMin() { return m_dRealMin; }
	double GetImagMin() { return m_dImagMin; }
	double GetRealSize() { return m_dRealSize; }
	int GetMaxValue() { return m_nMaxValue; }
	COLORREF GetClrBase() { return m_clrBase; }
	bool ShowButtons() { return m_bShowButtons; }
	int GetColourShades() { return m_nColourShades; }
	int GetShiftColour() { return m_nShiftColour; }
	double GetMoveBy() { return (double)m_nMoveByPctg / 100; }
	double GetZoomBy() { return (double)m_nZoomByPctg / 100; }
	int GetMoveByPctg() { return m_nMoveByPctg; }
	int GetZoomByPctg() { return m_nZoomByPctg; }

private:
	double m_dRealMin;
	double m_dImagMin;
	double m_dRealSize;
	int m_nMaxValue;
	COLORREF m_clrBase;
	bool m_bShowButtons;
	int m_nColourShades;
	int m_nShiftColour;
	int m_nMoveByPctg;
	int m_nZoomByPctg;
	CRegPropertyDouble m_RealMin;
	CRegPropertyDouble m_ImagMin;
	CRegPropertyDouble m_RealSize;
	CRegPropertyInt m_MaxValue;
	CRegPropertyInt m_BaseColour;
	CRegPropertyBool m_ShowButtons;
	CRegPropertyInt m_ColourShades;
	CRegPropertyInt m_ShiftColour;
	CRegPropertyInt m_MoveBy;
	CRegPropertyInt m_ZoomBy;
	BYTE m_nNormRed;
	BYTE m_nNormGreen;
	BYTE m_nNormBlue;

	void NormalizeColour();

	static const double s_dDefaultRealMin;
	static const double s_dDefaultImagMin;
	static const double s_dDefaultRealSize;
	static const int s_nDefaultMaxValue;
	static const COLORREF s_clrDefaultBaseColour;
	static const bool s_bDefaultShowButtons;
	static const int s_nDefaultColourShades;
	static const int s_nDefaultShiftColour;
	static const int s_nDefaultMoveByPctg;
	static const int s_nDefaultZoomByPctg;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft eMbedded Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MANDEL_H__045600DE_CDF5_44B8_B0F4_7E9398D2FF71__INCLUDED_)
