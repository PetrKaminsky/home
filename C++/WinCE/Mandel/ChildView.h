#if !defined(AFX_CHILDVIEW_H__44174A9C_0F48_4DEA_8FF7_53041B1532AF__INCLUDED_)
#define AFX_CHILDVIEW_H__44174A9C_0F48_4DEA_8FF7_53041B1532AF__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "PictureButton.h"

/////////////////////////////////////////////////////////////////////////////
// CMandelArray

class CMandelArray  
{
public:
	CMandelArray();
	virtual ~CMandelArray();

	void Init(int nWidth, int nHeight);
	void Clear();
	int GetValue(int x, int y);
	void SetValue(int x, int y, int Value);
	static const int s_nEmpty;

private:
	int *m_pArray;
	int m_nWidth;
	int m_nHeight;
};

/////////////////////////////////////////////////////////////////////////////
// CMandelCtrl window

class CMandelCtrl : public CWnd
{
public:
	CMandelCtrl(
		CMandelObj &MandelObj,
		CMandelArray &MandelArray,
		CMandelConfig &MandelConfig);

	void Erase();
	void PaintLine(int y);
	void Paint();
	void Update();

	//{{AFX_VIRTUAL(CMandelCtrl)
	//}}AFX_VIRTUAL

protected:
	//{{AFX_MSG(CMandelCtrl)
	afx_msg void OnPaint();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	CMandelObj &m_MandelObj;
	CBitmap m_bmpOff;
	CMandelArray &m_MandelArray;
	CRect m_Rect;
	CMandelConfig &m_MandelConfig;

	CRect &GetRect();
	CBitmap *GetBitmap();
	void PumpMessages();
};

/////////////////////////////////////////////////////////////////////////////
// CMandelView form view

#define WM_CALC_MANDEL (WM_USER + 1)
#define WM_CENTER_ZOOM (WM_USER + 2)

#define CALC_TIMER 1

class CMandelView : public CFormView
{
protected:
	DECLARE_DYNCREATE(CMandelView)

public:
	CMandelView();
	virtual ~CMandelView();

	int Create(CWnd *pParent);

	//{{AFX_DATA(CMandelView)
	enum { IDD = IDD_MANDELVIEW_FORM };
	CPictureButton	m_btnUpRight;
	CPictureButton	m_btnUpLeft;
	CPictureButton	m_btnDownRight;
	CPictureButton	m_btnDownLeft;
	CPictureButton	m_btnZoomOut;
	CPictureButton	m_btnZoomIn;
	CPictureButton	m_btnDown;
	CPictureButton	m_btnUp;
	CPictureButton	m_btnRight;
	CPictureButton	m_btnLeft;
	//}}AFX_DATA

	//{{AFX_VIRTUAL(CMandelView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

protected:
	//{{AFX_MSG(CMandelView)
	afx_msg void OnButtonLeft();
	afx_msg void OnButtonRight();
	afx_msg void OnButtonUp();
	afx_msg void OnButtonDown();
	afx_msg void OnButtonZoomin();
	afx_msg void OnButtonZoomout();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnButtonUpLeft();
	afx_msg void OnButtonUpRight();
	afx_msg void OnButtonDownLeft();
	afx_msg void OnButtonDownRight();
	afx_msg void OnMoveDown();
	afx_msg void OnMoveLeft();
	afx_msg void OnMoveRight();
	afx_msg void OnMoveUp();
	afx_msg void OnUpdateShowButtons(CCmdUI* pCmdUI);
	afx_msg void OnShowButtons();
	afx_msg void OnZoomIn();
	afx_msg void OnZoomOut();
	afx_msg void OnRestart();
	afx_msg void OnSettings();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	//}}AFX_MSG
	afx_msg LRESULT OnCalcMandel(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnCenterZoom(WPARAM wParam, LPARAM lParam);
	DECLARE_MESSAGE_MAP()

private:
	CMandelCtrl	m_ctrlMandel;
	CMandelObj m_MandelObj;
	int m_nCurrY;
	int m_nMaxX;
	int m_nMaxY;
	CMandelArray m_MandelArray;
	bool m_bShowButtons;
	CMandelConfig m_MandelConfig;
	int m_nWidth;
	int m_nHeight;

	void PostCalcMandel();
	void RestartCalc();
	void StepCalc();
	void ShowButtons();
};

/////////////////////////////////////////////////////////////////////////////
// CSettingsDlg dialog

class CSettingsDlg : public CDialog
{
public:
	CSettingsDlg(
		CWnd *pParent,
		int nMaxValue,
		COLORREF clrBase,
		int nColourShades,
		int nShiftColour,
		int nMoveByPctg,
		int nZoomByPctg);

	int GetMaxValue() { return m_nMaxValue; }
	COLORREF GetClrBase() { return m_clrBase; }
	int GetColourShades() { return m_nClrShades; }
	int GetShiftColour() { return m_nShiftColour; }
	int GetMoveByPctg() { return m_nMoveByPctg; }
	int GetZoomByPctg() { return m_nZoomByPctg; }

	//{{AFX_DATA(CSettingsDlg)
	enum { IDD = IDD_DIALOG_SETTINGS };
	CComboBox	m_ctrlZoomBy;
	CComboBox	m_ctrlMoveBy;
	CComboBox	m_ctrlShiftClr;
	CComboBox	m_ctrlClrShades;
	CComboBox	m_ctrlMaxIter;
	CStatic	m_ctrlColor;
	//}}AFX_DATA

	//{{AFX_VIRTUAL(CSettingsDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

protected:
	//{{AFX_MSG(CSettingsDlg)
	virtual BOOL OnInitDialog();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	int m_nMaxValue;
	COLORREF m_clrBase;
	int m_nClrShades;
	int m_nShiftColour;
	CBrush m_Brush;
	int m_nMoveByPctg;
	int m_nZoomByPctg;

	static void SetComboCurValue(CComboBox &cb, int nValue);
	static void GetComboCurValue(CComboBox &cb, int &nValue);
	static void SetComboValue(CComboBox &cb, int nValue);
	static void GetComboValue(CComboBox &cb, int &nValue);
};

/////////////////////////////////////////////////////////////////////////////
//{{AFX_INSERT_LOCATION}}
// Microsoft eMbedded Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CHILDVIEW_H__44174A9C_0F48_4DEA_8FF7_53041B1532AF__INCLUDED_)
