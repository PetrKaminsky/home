#include "stdafx.h"
#include "Mandel.h"
#include "MainFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMainFrame

IMPLEMENT_DYNAMIC(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	//{{AFX_MSG_MAP(CMainFrame)
	ON_WM_CREATE()
	ON_WM_SETFOCUS()
	ON_WM_SIZE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////

CMainFrame::CMainFrame() :
	m_pView(NULL)
{
}

CMainFrame::~CMainFrame()
{
	if (m_pView != NULL)
	{
		m_pView->DestroyWindow();
		delete m_pView;
		m_pView = NULL;
	}
}

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	// create a view to occupy the client area of the frame

	m_pView = new CMandelView;
	if (!m_pView->Create(this))
	{
		TRACE0("Failed to create view window\n");
		return -1;
	}
	
	if (!m_wndCommandBar.Create(this) ||
		!m_wndCommandBar.InsertMenuBar(IDR_MAINFRAME))
	{
		TRACE0("Failed to create CommandBar\n");
		return -1;      // fail to create
	}

	m_wndCommandBar.SetBarStyle(m_wndCommandBar.GetBarStyle() |
		CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_FIXED);

	ActivateForm();

	return 0;
}

/////////////////////////////////////////////////////////////////////////////
// CMainFrame message handlers

void CMainFrame::OnSetFocus(CWnd* pOldWnd)
{
	m_pView->SetFocus();
}

BOOL CMainFrame::OnCmdMsg(UINT nID, int nCode, void* pExtra, AFX_CMDHANDLERINFO* pHandlerInfo)
{
	if (m_pView->OnCmdMsg(nID, nCode, pExtra, pHandlerInfo))
		return TRUE;

	return CFrameWnd::OnCmdMsg(nID, nCode, pExtra, pHandlerInfo);
}

void CMainFrame::OnSize(UINT nType, int cx, int cy) 
{
	CFrameWnd::OnSize(nType, cx, cy);

// @@@@ PocketPC
	CRect r;
	GetClientRect(&r);
	m_pView->MoveWindow(&r);
// @@@@ PocketPC
// @@@@ CE.NET
// @@@@ CE.NET
}

void CMainFrame::ActivateForm()
{
// @@@@ PocketPC
	CRect r;
	GetClientRect(&r);
// @@@@ PocketPC
// @@@@ CE.NET
//	CRect rc;
//	GetClientRect(&rc);
//	CRect rcBar;
//	m_wndCommandBar.GetWindowRect(rcBar);
//	CRect r(rc.left, rcBar.Height(), rc.right, rc.bottom);
// @@@@ CE.NET
	m_pView->MoveWindow(&r);
}
