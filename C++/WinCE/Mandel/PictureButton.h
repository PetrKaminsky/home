#if !defined(AFX_PICTUREBUTTON_H__E37DF5BD_8F84_4C7B_BE7A_C56502B9130B__INCLUDED_)
#define AFX_PICTUREBUTTON_H__E37DF5BD_8F84_4C7B_BE7A_C56502B9130B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CPictureButton

class CPictureButton : public CButton
{
	HICON	m_hIcon;
	CSize	m_iconSize;
	BOOL	m_bIsFlat;

public:
	CPictureButton();
	virtual ~CPictureButton();

	void SetIcon(UINT nIconID, int cx, int cy);
	void SetFlat(BOOL bFlat = TRUE)	{ m_bIsFlat = bFlat; }

	//{{AFX_VIRTUAL(CPictureButton)
	public:
	virtual void DrawItem(LPDRAWITEMSTRUCT lpDis);
	//}}AFX_VIRTUAL

protected:
	//{{AFX_MSG(CPictureButton)
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()

};

/////////////////////////////////////////////////////////////////////////////
//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PICTUREBUTTON_H__E37DF5BD_8F84_4C7B_BE7A_C56502B9130B__INCLUDED_)
