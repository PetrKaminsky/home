//{{NO_DEPENDENCIES}}
// Microsoft eMbedded Visual C++ generated include file.
// Used by FtpCli.rc
//
#define IDD_FORM_PROXY                  103
#define IDD_FORM_SERVER                 104
#define IDD_FORM_USER                   105
#define IDD_FORM_CONN                   106
#define IDR_MAINFRAME                   128
#define IDS_ERROR_EMPTY_SERVER          129
#define IDS_ERROR_EMPTY_PORT            130
#define IDI_ICON_FOLDER                 131
#define IDS_ERROR_INVALID_PORT          131
#define IDI_ICON_FILE                   132
#define IDS_ERROR_EMPTY_USER            132
#define IDS_ERROR_INVALID_PROXY         133
#define IDD_DIALOG_SEL_FILE             134
#define IDD_DIALOG_FILE_INFO            136
#define IDD_DIALOG_FILE_GET             137
#define IDD_DIALOG_FILE_PUT             138
#define IDD_DIALOG_FILE_REN             139
#define IDD_DIALOG_DIR_CREATE           140
#define IDD_DIALOG_DIR_CHANGE           141
#define IDC_RADIO_USER_ANON             1000
#define IDC_RADIO_USER                  1001
#define IDC_EDIT_USER                   1002
#define IDC_EDIT_SERVER                 1003
#define IDC_RADIO_PORT_STD              1004
#define IDC_RADIO_PORT                  1005
#define IDC_EDIT_PORT                   1006
#define IDC_CHECK_PASSIVE               1007
#define IDC_RADIO_DIRECT                1008
#define IDC_RADIO_PRE                   1009
#define IDC_RADIO_PRE_NOAUTO            1011
#define IDC_RADIO_PROXY                 1012
#define IDC_EDIT_PROXY                  1013
#define IDC_BUTTON_CONNECT              1014
#define IDC_BUTTON_DISC                 1015
#define IDC_EDIT_PASSWORD               1016
#define IDC_LIST_FILES                  1017
#define IDC_BUTTON_ROOT                 1018
#define IDC_BUTTON_UP                   1019
#define IDC_EDIT_CUR_DIR                1020
#define IDC_EDIT_BYPASS                 1021
#define IDC_BUTTON_BROWSE               1022
#define IDC_STATIC_STATS                1023
#define IDC_EDIT_DIR                    1028
#define IDC_LIST_PROPS                  1032
#define IDC_EDIT_REMOTE                 1033
#define IDC_EDIT_LOCAL                  1034
#define IDC_BUTTON_ACTION               1036
#define IDC_PROGRESS_GET                1037
#define IDC_STATIC_PCTG                 1038
#define IDC_STATIC_BYTES                1039
#define IDC_CHECK_BINARY                1040
#define IDC_PROGRESS_PUT                1041
#define IDC_EDIT_OLD                    1042
#define ID_BUTTON_GO                    1044
#define IDC_EDIT_NEW                    1045
#define IDC_EDIT_REM_DIR                1046
#define ID_FRM_SERVER                   32771
#define ID_FRM_USER                     32772
#define ID_FRM_PROXY                    32773
#define ID_FRM_CONNECT                  32774
#define ID_FTP_FILE_GET                 32775
#define ID_FTP_PROPS                    32776
#define ID_FTP_FILE_DEL                 32777
#define ID_FTP_FILE_REN                 32778
#define ID_FTP_DIR_CREATE               32779
#define ID_FTP_DIR_REMOVE               32780
#define ID_FTP_DIR_CHANGE               32781
#define ID_FTP_FILE_PUT                 32782
#define ID_FTP_SEL_DIR_CHANGE           32783
#define ID_FTP_REFRESH                  32784

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        143
#define _APS_NEXT_COMMAND_VALUE         32785
#define _APS_NEXT_CONTROL_VALUE         1047
#define _APS_NEXT_SYMED_VALUE           107
#endif
#endif
