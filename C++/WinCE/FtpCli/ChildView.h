#if !defined(AFX_CHILDVIEW_H__722DBF03_5613_4E19_A150_3EAAC1723112__INCLUDED_)
#define AFX_CHILDVIEW_H__722DBF03_5613_4E19_A150_3EAAC1723112__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "FileSelDlg.h"

/////////////////////////////////////////////////////////////////////////////

class CProxyInfo
{
public:
	virtual DWORD GetInternetOpenAccessType() = 0;
	virtual LPCTSTR GetInternetOpenProxy() = 0;
	virtual LPCTSTR GetInternetOpenProxyBypass() = 0;
};

/////////////////////////////////////////////////////////////////////////////

class CServerInfo
{
public:
	virtual LPCTSTR GetInternetConnectServerName() = 0;
	virtual INTERNET_PORT GetInternetConnectServerPort() = 0;
	virtual DWORD GetInternetConnectFlags() = 0;
};

/////////////////////////////////////////////////////////////////////////////

class CUserInfo
{
public:
	virtual LPCTSTR GetInternetConnectUserName() = 0;
	virtual LPCTSTR GetInternetConnectPassword() = 0;
};

/////////////////////////////////////////////////////////////////////////////

class CFtpInfoClient
{
public:
	CFtpInfoClient();

	void SetProxyInfo(CProxyInfo *pProxyInfo);
	void SetServerInfo(CServerInfo *pServerInfo);
	void SetUserInfo(CUserInfo *pUserInfo);

protected:
	CProxyInfo *m_pProxyInfo;
	CServerInfo *m_pServerInfo;
	CUserInfo *m_pUserInfo;
};

/////////////////////////////////////////////////////////////////////////////

class CInfoException : public CException
{
	DECLARE_DYNAMIC(CInfoException)

public:
	CInfoException(LPCTSTR szDesc);
	CInfoException(UINT nDescID);

	CString m_strDescription;
};

/////////////////////////////////////////////////////////////////////////////

class CChildForm : public CFormView
{
public:
	CChildForm();
	CChildForm(int nID);

	int Create(CWnd *pParent);

protected:
	int m_nID;
};

/////////////////////////////////////////////////////////////////////////////
// CServerForm form view

class CServerForm
:	public CChildForm,
	public CServerInfo
{
public:
	CServerForm();

	//{{AFX_DATA(CServerForm)
	enum { IDD = IDD_FORM_SERVER };
	CEdit	m_ctrlPort;
	CEdit	m_ctrlServer;
	CButton	m_ctrlPassive;
	CButton m_ctrlRadioPort;
	CButton m_ctrlRadioPortStd;
	//}}AFX_DATA

	//{{AFX_VIRTUAL(CServerForm)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

private:
	CString m_sServerName;
	CRegPropertyString m_LastServer;
	CRegPropertyString m_LastPort;
	CRegPropertyBool m_LastRadioStd;
	CRegPropertyBool m_LastRadioPort;
	CRegPropertyBool m_LastPassive;

	void UpdateControls();
	LPCTSTR GetInternetConnectServerName();
	INTERNET_PORT GetInternetConnectServerPort();
	DWORD GetInternetConnectFlags();
	void BackupProperties();

protected:
	//{{AFX_MSG(CServerForm)
	afx_msg void OnRadioPort();
	afx_msg void OnRadioPortStd();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnDestroy();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////
// CUserForm form view

class CUserForm
:	public CChildForm,
	public CUserInfo
{
public:
	CUserForm();

	//{{AFX_DATA(CUserForm)
	enum { IDD = IDD_FORM_USER };
	CEdit	m_ctrlPassword;
	CEdit	m_ctrlUser;
	CButton m_ctrlRadioUserAnon;
	CButton m_ctrlRadioUser;
	//}}AFX_DATA

	//{{AFX_VIRTUAL(CUserForm)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

private:
	CString m_sUserName;
	CString m_sPassword;
	CRegPropertyString m_LastUser;
	CRegPropertyString m_LastPassword;
	CRegPropertyBool m_LastRadioAnon;
	CRegPropertyBool m_LastRadioUser;

	void UpdateControls();
	LPCTSTR GetInternetConnectUserName();
	LPCTSTR GetInternetConnectPassword();
	void BackupProperties();

protected:
	//{{AFX_MSG(CUserForm)
	afx_msg void OnRadioUser();
	afx_msg void OnRadioUserAnon();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnDestroy();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////
// CProxyForm form view

class CProxyForm
:	public CChildForm,
	public CProxyInfo
{
public:
	CProxyForm();

	//{{AFX_DATA(CProxyForm)
	enum { IDD = IDD_FORM_PROXY };
	CEdit	m_ctrlBypass;
	CEdit	m_ctrlProxy;
	CButton m_ctrlRadioDirect;
	CButton m_ctrlRadioPre;
	CButton m_ctrlRadioPreNoauto;
	CButton m_ctrlRadioProxy;
	//}}AFX_DATA

	//{{AFX_VIRTUAL(CProxyForm)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

private:
	CString m_sProxy;
	CString m_sProxyBypass;
	CRegPropertyString m_LastProxy;
	CRegPropertyString m_LastBypass;
	CRegPropertyBool m_LastRadioDirect;
	CRegPropertyBool m_LastRadioPre;
	CRegPropertyBool m_LastRadioPreNoauto;
	CRegPropertyBool m_LastRadioProxy;

	void UpdateControls();
	DWORD GetInternetOpenAccessType();
	LPCTSTR GetInternetOpenProxy();
	LPCTSTR GetInternetOpenProxyBypass();
	void BackupProperties();

protected:
	//{{AFX_MSG(CProxyForm)
	afx_msg void OnRadioDirect();
	afx_msg void OnRadioPre();
	afx_msg void OnRadioPreNoauto();
	afx_msg void OnRadioProxy();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnDestroy();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////
// CConnForm form view

class CConnForm
:	public CChildForm,
	public CFtpInfoClient
{
public:
	CConnForm();
	virtual ~CConnForm();

	//{{AFX_DATA(CConnForm)
	enum { IDD = IDD_FORM_CONN };
	CListCtrl	m_ctrlFiles;
	CEdit	m_ctrlCurDir;
	CButton	m_ctrlUp;
	CButton	m_ctrlRoot;
	CButton	m_ctrlDisconnect;
	CButton	m_ctrlConnect;
	//}}AFX_DATA

	//{{AFX_VIRTUAL(CConnForm)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

private:
	bool m_bConnected;
	HINTERNET m_hInet;
	HINTERNET m_hFtp;
	CImageList m_ImageList;
	CString m_sLastLocalDir;
	CIcon m_FolderIcon;
	CIcon m_FileIcon;

	void UpdateControls();
	void Disconnect();
	void Connect();
	void FillList();
	void AddListItem(LPCTSTR szName, bool bDir);
	void EmptyList();
	void ChangeDir(LPCTSTR szDir);
	CString GetCurDir();
	void SetCurDir(LPCTSTR szDir);
	void RefreshList();
	int GetSelectedItem();
	bool IsItemFolder(int nItem);
	bool IsItemFile(int nItem);
	void PrepareImageList();

protected:
	//{{AFX_MSG(CConnForm)
	afx_msg void OnButtonConnect();
	afx_msg void OnButtonDisconnect();
	afx_msg void OnButtonRoot();
	afx_msg void OnButtonUp();
	afx_msg void OnFtpDirCreate();
	afx_msg void OnUpdateFtpDirCreate(CCmdUI* pCmdUI);
	afx_msg void OnFtpDirChange();
	afx_msg void OnUpdateFtpDirChange(CCmdUI* pCmdUI);
	afx_msg void OnFtpDirRemove();
	afx_msg void OnUpdateFtpDirRemove(CCmdUI* pCmdUI);
	afx_msg void OnFtpFileDel();
	afx_msg void OnUpdateFtpFileDel(CCmdUI* pCmdUI);
	afx_msg void OnFtpFileGet();
	afx_msg void OnUpdateFtpFileGet(CCmdUI* pCmdUI);
	afx_msg void OnFtpFilePut();
	afx_msg void OnUpdateFtpFilePut(CCmdUI* pCmdUI);
	afx_msg void OnFtpFileRen();
	afx_msg void OnUpdateFtpFileRen(CCmdUI* pCmdUI);
	afx_msg void OnFtpProps();
	afx_msg void OnUpdateFtpProps(CCmdUI* pCmdUI);
	afx_msg void OnFtpSelDirChange();
	afx_msg void OnUpdateFtpSelDirChange(CCmdUI* pCmdUI);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnFtpRefresh();
	afx_msg void OnUpdateFtpRefresh(CCmdUI* pCmdUI);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////
//{{AFX_INSERT_LOCATION}}
// Microsoft eMbedded Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CHILDVIEW_H__722DBF03_5613_4E19_A150_3EAAC1723112__INCLUDED_)
