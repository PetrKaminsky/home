#include "stdafx.h"
#include "FtpCli.h"
#include "FileSelDlg.h"
#include "Globals.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CFileSelDlg dialog

CFileSelDlg::CFileSelDlg(
	CWnd *pParent,
	LPCTSTR szStartDir,
	LPCTSTR szFileMask,
	bool bDirsOnly)
:	CDialog(CFileSelDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CFileSelDlg)
	//}}AFX_DATA_INIT
	if (szStartDir != NULL)
		m_sCurDir = szStartDir;
	if (m_sCurDir.IsEmpty())
		m_sCurDir = _T("\\");
	if (m_sCurDir.GetLength() > 1)
		m_sCurDir.TrimRight(_T('\\'));
	if (szFileMask != NULL)
		m_sFileMask = szFileMask;
	if (m_sFileMask.IsEmpty())
		m_sFileMask = _T("*");
	m_bDirsOnly = bDirsOnly;
}

void CFileSelDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CFileSelDlg)
	DDX_Control(pDX, IDC_LIST_FILES, m_ctrlFiles);
	DDX_Control(pDX, IDC_EDIT_DIR, m_ctrlDir);
	DDX_Control(pDX, IDC_BUTTON_UP, m_ctrlUp);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CFileSelDlg, CDialog)
	//{{AFX_MSG_MAP(CFileSelDlg)
	ON_NOTIFY(NM_CLICK, IDC_LIST_FILES, OnClickListFiles)
	ON_BN_CLICKED(IDC_BUTTON_ROOT, OnButtonRoot)
	ON_BN_CLICKED(IDC_BUTTON_UP, OnButtonUp)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CFileSelDlg message handlers

void CFileSelDlg::OnOK()
{
	if (m_bDirsOnly)
	{
		CDialog::OnOK();
		return;
	}
	int nItem = m_ctrlFiles.GetNextItem(-1, LVIS_SELECTED);
	if (nItem != -1)
	{
		bool bFolder = m_ctrlFiles.GetItemData(nItem) == 0;
		if (!bFolder)
		{
			m_sSelFileName = m_ctrlFiles.GetItemText(nItem, 0);
			CDialog::OnOK();
		}
	}
}

void CFileSelDlg::OnClickListFiles(NMHDR *pNMHDR, LRESULT *pResult)
{
	NMLISTVIEW *pItem = (NMLISTVIEW *)pNMHDR;
	int nItem = pItem->iItem;
	if (nItem != -1)
	{
		bool bFolder = m_ctrlFiles.GetItemData(nItem) == 0;
		if (bFolder)
		{
			CString sDir(m_ctrlFiles.GetItemText(nItem, 0));
			ChangeSubdir(sDir);
		}
	}
	*pResult = 0;
}

void CFileSelDlg::OnButtonRoot()
{
	ChangeRoot();
}

void CFileSelDlg::OnButtonUp()
{
	ChangeUp();
}

BOOL CFileSelDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	PrepareImageList();

	ChangeDir(m_sCurDir);

	return TRUE;
}

void CFileSelDlg::FillList()
{
	WIN32_FIND_DATA wfd;
	CString sFileMask(m_sCurDir);
	if (sFileMask[sFileMask.GetLength() - 1] != _T('\\'))
		sFileMask += _T('\\');
	sFileMask += m_sFileMask;
	CWaitCursor wc;
	HANDLE hFind = FindFirstFile(
		sFileMask,
		&wfd);
	if (hFind != INVALID_HANDLE_VALUE)
	{
		CStringSet setDir;
		CStringSet setFile;
		do
		{
			if ((wfd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) != 0)
				setDir.Insert(wfd.cFileName);
			else if (!m_bDirsOnly)
				setFile.Insert(wfd.cFileName);
			if (FindNextFile(hFind, &wfd) == FALSE)
			{
				DWORD dwError = GetLastError();
				if (dwError != ERROR_NO_MORE_FILES)
				{
					CFtpCliApp::PrintWininetError(_T("FindNextFile()"), dwError);
				}
				break;
			}
		}
		while (true);
		FindClose(hFind);
		LPCTSTR szDir = NULL;
		if (setDir.Begin())
			while ((szDir = setDir.Next()) != NULL)
				AddListItem(szDir, true);
		LPCTSTR szFile = NULL;
		if (setFile.Begin())
			while ((szFile = setFile.Next()) != NULL)
				AddListItem(szFile, false);
	}
	else
	{
		DWORD dwError = GetLastError();
		if (dwError != ERROR_NO_MORE_FILES)
		{
			CFtpCliApp::PrintWininetError(_T("FindFirstFile()"), dwError);
		}
	}
}

void CFileSelDlg::AddListItem(LPCTSTR szFileName, bool bFolder)
{
	int nIndex = m_ctrlFiles.GetItemCount();
	int nImageIndex = bFolder ? 0 : 1;
	m_ctrlFiles.InsertItem(
		nIndex,
		szFileName,
		nImageIndex);
	m_ctrlFiles.SetItemData(nIndex, nImageIndex);
}

void CFileSelDlg::EmptyList()
{
	m_ctrlFiles.DeleteAllItems();
}

void CFileSelDlg::ChangeDir(LPCTSTR szDir)
{
	SetCurDir(szDir);
	UpdateControls();
	EmptyList();
	FillList();
}

void CFileSelDlg::SetCurDir(LPCTSTR szDir)
{
	m_sCurDir = szDir;
	m_ctrlDir.SetWindowText(m_sCurDir);
}

void CFileSelDlg::UpdateControls()
{
	m_ctrlUp.EnableWindow(m_sCurDir != _T("\\") ? TRUE : FALSE);
}

CString CFileSelDlg::GetFilePath()
{
	CString sResult(m_sCurDir);
	if (sResult[sResult.GetLength() - 1] != _T('\\'))
		sResult += _T('\\');
	sResult += m_sSelFileName;
	return sResult;
}

void CFileSelDlg::PrepareImageList()
{
	int cx = ::GetSystemMetrics(SM_CXSMICON);
	int cy = ::GetSystemMetrics(SM_CYSMICON);
	m_FolderIcon.Load(IDI_ICON_FOLDER, cx, cy);
	m_FileIcon.Load(IDI_ICON_FILE, cx, cy);
	m_ImageList.Create(
		cx,
		cy,
		TRUE,
		2,
		1);
	m_ImageList.Add(m_FolderIcon);
	m_ImageList.Add(m_FileIcon);

	m_ctrlFiles.SetImageList(&m_ImageList, LVSIL_SMALL);
}

void CFileSelDlg::ChangeUp()
{
	CString sDir(m_sCurDir);
	int nIndex = sDir.ReverseFind(_T('\\'));
	if (nIndex != -1)
	{
		if (nIndex == 0)
			sDir = sDir.Left(1);
		else
			sDir = sDir.Left(nIndex);
	}
	ChangeDir(sDir);
}

void CFileSelDlg::ChangeRoot()
{
	ChangeDir(_T("\\"));
}

void CFileSelDlg::ChangeSubdir(LPCTSTR szDir)
{
	CString sDir(m_sCurDir);
	if (sDir[sDir.GetLength() - 1] != _T('\\'))
		sDir += _T('\\');
	sDir += szDir;
	ChangeDir(sDir);
}

CString CFileSelDlg::GetCurDir()
{
	return m_sCurDir;
}

/////////////////////////////////////////////////////////////////////////////

bool LessLPCTSTR::operator ()(LPCTSTR x, LPCTSTR y) const
{
	return _tcsicmp(x, y) < 0;
}

/////////////////////////////////////////////////////////////////////////////

CStringSet::~CStringSet()
{
	TStringSet::iterator it = begin();
	while (it != end())
	{
		free((void *)*it);
		it++;
	}
	clear();
}

void CStringSet::Insert(LPCTSTR szString)
{
	TStringSet::iterator it = find(szString);
	if (it != end())
		return;
	insert(_tcsdup(szString));
}

bool CStringSet::Begin()
{
	m_Iter = begin();
	if (m_Iter == end())
		return false;
	return true;
}

LPCTSTR CStringSet::Next()
{
	if (m_Iter == end())
		return NULL;
	LPCTSTR szResult = *m_Iter;
	m_Iter++;
	return szResult;
}

/////////////////////////////////////////////////////////////////////////////

std::_Lockit::_Lockit()
{
}

std::_Lockit::~_Lockit()
{
}
