#include "stdafx.h"
#include "FtpCli.h"
#include "ChildView.h"
#include "FileOpDlg.h"
#include "FileSelDlg.h"
#include "Globals.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////

CChildForm::CChildForm()
:	CFormView(-1)
{
	m_nID = -1;
}

CChildForm::CChildForm(int nID)
:	CFormView(nID)
{
	m_nID = nID;
}

int CChildForm::Create(CWnd *pParent)
{
	return CFormView::Create(NULL, NULL, WS_CHILD | WS_VISIBLE,
		CRect(0, 0, 0, 0), pParent, m_nID, NULL);
}

/////////////////////////////////////////////////////////////////////////////
// CServerForm

CServerForm::CServerForm()
:	CChildForm(CServerForm::IDD),
	m_LastServer(CFtpCliApp::s_pPropertySerializer, _T("Server\\Server"), _T("")),
	m_LastPort(CFtpCliApp::s_pPropertySerializer, _T("Server\\Port"), _T("")),
	m_LastRadioStd(CFtpCliApp::s_pPropertySerializer, _T("Server\\RadioStd"), true),
	m_LastRadioPort(CFtpCliApp::s_pPropertySerializer, _T("Server\\RadioPort"), false),
	m_LastPassive(CFtpCliApp::s_pPropertySerializer, _T("Server\\Passive"), false)
{
	//{{AFX_DATA_INIT(CServerForm)
	//}}AFX_DATA_INIT
}

void CServerForm::DoDataExchange(CDataExchange* pDX)
{
	CChildForm::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CServerForm)
	DDX_Control(pDX, IDC_EDIT_PORT, m_ctrlPort);
	DDX_Control(pDX, IDC_EDIT_SERVER, m_ctrlServer);
	DDX_Control(pDX, IDC_CHECK_PASSIVE, m_ctrlPassive);
	DDX_Control(pDX, IDC_RADIO_PORT, m_ctrlRadioPort);
	DDX_Control(pDX, IDC_RADIO_PORT_STD, m_ctrlRadioPortStd);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CServerForm, CChildForm)
	//{{AFX_MSG_MAP(CServerForm)
	ON_BN_CLICKED(IDC_RADIO_PORT, OnRadioPort)
	ON_BN_CLICKED(IDC_RADIO_PORT_STD, OnRadioPortStd)
	ON_WM_SIZE()
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CServerForm message handlers

void CServerForm::OnInitialUpdate()
{
	CChildForm::OnInitialUpdate();

	m_ctrlServer.SetWindowText(m_LastServer.GetValue());
	m_ctrlPort.SetWindowText(m_LastPort.GetValue());
	m_ctrlRadioPortStd.SetCheck(m_LastRadioStd.GetValue() ? BST_CHECKED : BST_UNCHECKED);
	m_ctrlRadioPort.SetCheck(m_LastRadioPort.GetValue() ? BST_CHECKED : BST_UNCHECKED);
	m_ctrlPassive.SetCheck(m_LastPassive.GetValue() ? BST_CHECKED : BST_UNCHECKED);

	UpdateControls();

	m_ctrlServer.SetFocus();
}

void CServerForm::OnRadioPort()
{
	UpdateControls();
	m_ctrlPort.SetFocus();
}

void CServerForm::OnRadioPortStd()
{
	UpdateControls();
}

void CServerForm::UpdateControls()
{
	if (m_ctrlRadioPort.GetCheck() == BST_CHECKED)
	{
		m_ctrlPort.EnableWindow(TRUE);
	}
	else
	{
		m_ctrlPort.SetWindowText(_T(""));
		m_ctrlPort.EnableWindow(FALSE);
	}
	BackupProperties();
}

LPCTSTR CServerForm::GetInternetConnectServerName()
{
	m_ctrlServer.GetWindowText(m_sServerName);
	if (m_sServerName.IsEmpty())
		THROW (new CInfoException(IDS_ERROR_EMPTY_SERVER));
	return m_sServerName;
}

INTERNET_PORT CServerForm::GetInternetConnectServerPort()
{
	if (m_ctrlRadioPortStd.GetCheck() == BST_CHECKED)
		return INTERNET_DEFAULT_FTP_PORT;
	CString sPort;
	m_ctrlPort.GetWindowText(sPort);
	if (sPort.IsEmpty())
		THROW (new CInfoException(IDS_ERROR_EMPTY_PORT));
	INTERNET_PORT nPort;
	if (_stscanf(sPort, _T("%hd"), &nPort) < 1)
		THROW (new CInfoException(IDS_ERROR_INVALID_PORT));
	return nPort;
}

DWORD CServerForm::GetInternetConnectFlags()
{
	return (m_ctrlPassive.GetCheck() == BST_CHECKED) ? INTERNET_FLAG_PASSIVE : 0;
}

void CServerForm::OnSize(UINT nType, int cx, int cy)
{
	CChildForm::OnSize(nType, cx, cy);
	
	if (cx == 0 &&
		cy == 0)
	{
		return;
	}

	if (m_ctrlServer.GetSafeHwnd() == NULL)
	{
		return;
	}

	CRect r;
	GetClientRect(&r);

	CGlobals::ResizeControl(&m_ctrlServer, r.right - 2);
}

void CServerForm::OnDestroy()
{
	BackupProperties();

	CChildForm::OnDestroy();
}

void CServerForm::BackupProperties()
{
	CString s;
	m_ctrlServer.GetWindowText(s);
	m_LastServer.SetValue(s);
	m_ctrlPort.GetWindowText(s);
	m_LastPort.SetValue(s);
	m_LastRadioStd.SetValue(m_ctrlRadioPortStd.GetCheck() == BST_CHECKED);
	m_LastRadioPort.SetValue(m_ctrlRadioPort.GetCheck() == BST_CHECKED);
	m_LastPassive.SetValue(m_ctrlPassive.GetCheck() == BST_CHECKED);
}

/////////////////////////////////////////////////////////////////////////////
// CUserForm

CUserForm::CUserForm()
:	CChildForm(CUserForm::IDD),
	m_LastUser(CFtpCliApp::s_pPropertySerializer, _T("User\\User"), _T("")),
	m_LastPassword(CFtpCliApp::s_pPropertySerializer, _T("User\\Password"), _T("")),
	m_LastRadioAnon(CFtpCliApp::s_pPropertySerializer, _T("User\\RadioAnon"), true),
	m_LastRadioUser(CFtpCliApp::s_pPropertySerializer, _T("User\\RadioUser"), false)
{
	//{{AFX_DATA_INIT(CUserForm)
	//}}AFX_DATA_INIT
}

void CUserForm::DoDataExchange(CDataExchange* pDX)
{
	CChildForm::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CUserForm)
	DDX_Control(pDX, IDC_EDIT_PASSWORD, m_ctrlPassword);
	DDX_Control(pDX, IDC_EDIT_USER, m_ctrlUser);
	DDX_Control(pDX, IDC_RADIO_USER_ANON, m_ctrlRadioUserAnon);
	DDX_Control(pDX, IDC_RADIO_USER, m_ctrlRadioUser);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CUserForm, CChildForm)
	//{{AFX_MSG_MAP(CUserForm)
	ON_BN_CLICKED(IDC_RADIO_USER, OnRadioUser)
	ON_BN_CLICKED(IDC_RADIO_USER_ANON, OnRadioUserAnon)
	ON_WM_SIZE()
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CUserForm message handlers

void CUserForm::OnInitialUpdate()
{
	CChildForm::OnInitialUpdate();

	m_ctrlUser.SetWindowText(m_LastUser.GetValue());
	m_ctrlPassword.SetWindowText(m_LastPassword.GetValue());
	m_ctrlRadioUserAnon.SetCheck(m_LastRadioAnon.GetValue() ? BST_CHECKED : BST_UNCHECKED);
	m_ctrlRadioUser.SetCheck(m_LastRadioUser.GetValue() ? BST_CHECKED : BST_UNCHECKED);

	UpdateControls();
}

void CUserForm::OnRadioUser()
{
	UpdateControls();
	m_ctrlUser.SetFocus();
}

void CUserForm::OnRadioUserAnon()
{
	UpdateControls();
}

void CUserForm::UpdateControls()
{
	if (m_ctrlRadioUser.GetCheck() == BST_CHECKED)
	{
		m_ctrlUser.EnableWindow(TRUE);
		m_ctrlPassword.EnableWindow(TRUE);
	}
	else
	{
		m_ctrlUser.SetWindowText(_T(""));
		m_ctrlPassword.SetWindowText(_T(""));
		m_ctrlUser.EnableWindow(FALSE);
		m_ctrlPassword.EnableWindow(FALSE);
	}
	BackupProperties();
}

LPCTSTR CUserForm::GetInternetConnectUserName()
{
	if (m_ctrlRadioUserAnon.GetCheck() == BST_CHECKED)
		return NULL;
	m_ctrlUser.GetWindowText(m_sUserName);
	if (m_sUserName.IsEmpty())
		THROW (new CInfoException(IDS_ERROR_EMPTY_USER));
	return m_sUserName;
}

LPCTSTR CUserForm::GetInternetConnectPassword()
{
	if (m_ctrlRadioUserAnon.GetCheck() == BST_CHECKED)
		return NULL;
	m_ctrlPassword.GetWindowText(m_sPassword);
	return m_sPassword.IsEmpty() ? NULL : (LPCTSTR)m_sPassword;
}

void CUserForm::OnSize(UINT nType, int cx, int cy)
{
	CChildForm::OnSize(nType, cx, cy);

	if (cx == 0 &&
		cy == 0)
	{
		return;
	}

	if (m_ctrlUser.GetSafeHwnd() == NULL ||
		m_ctrlPassword.GetSafeHwnd() == NULL)
	{
		return;
	}

	CRect r;
	GetClientRect(&r);

	CGlobals::ResizeControl(&m_ctrlUser, r.right - 2);
	CGlobals::ResizeControl(&m_ctrlPassword, r.right - 2);
}

void CUserForm::OnDestroy()
{
	BackupProperties();

	CChildForm::OnDestroy();
}

void CUserForm::BackupProperties()
{
	CString s;
	m_ctrlUser.GetWindowText(s);
	m_LastUser.SetValue(s);
	m_ctrlPassword.GetWindowText(s);
	m_LastPassword.SetValue(s);
	m_LastRadioAnon.SetValue(m_ctrlRadioUserAnon.GetCheck() == BST_CHECKED);
	m_LastRadioUser.SetValue(m_ctrlRadioUser.GetCheck() == BST_CHECKED);
}

/////////////////////////////////////////////////////////////////////////////
// CProxyForm

CProxyForm::CProxyForm()
:	CChildForm(CProxyForm::IDD),
	m_LastProxy(CFtpCliApp::s_pPropertySerializer, _T("Proxy\\Proxy"), _T("")),
	m_LastBypass(CFtpCliApp::s_pPropertySerializer, _T("Proxy\\Bypass"), _T("")),
	m_LastRadioDirect(CFtpCliApp::s_pPropertySerializer, _T("Proxy\\RadioDirect"), true),
	m_LastRadioPre(CFtpCliApp::s_pPropertySerializer, _T("Proxy\\RadioPre"), false),
	m_LastRadioPreNoauto(CFtpCliApp::s_pPropertySerializer, _T("Proxy\\RadioPreNoauto"), false),
	m_LastRadioProxy(CFtpCliApp::s_pPropertySerializer, _T("Proxy\\RadioProxy"), false)
{
	//{{AFX_DATA_INIT(CProxyForm)
	//}}AFX_DATA_INIT
}

void CProxyForm::DoDataExchange(CDataExchange* pDX)
{
	CChildForm::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CProxyForm)
	DDX_Control(pDX, IDC_EDIT_BYPASS, m_ctrlBypass);
	DDX_Control(pDX, IDC_EDIT_PROXY, m_ctrlProxy);
	DDX_Control(pDX, IDC_RADIO_DIRECT, m_ctrlRadioDirect);
	DDX_Control(pDX, IDC_RADIO_PRE, m_ctrlRadioPre);
	DDX_Control(pDX, IDC_RADIO_PRE_NOAUTO, m_ctrlRadioPreNoauto);
	DDX_Control(pDX, IDC_RADIO_PROXY, m_ctrlRadioProxy);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CProxyForm, CChildForm)
	//{{AFX_MSG_MAP(CProxyForm)
	ON_BN_CLICKED(IDC_RADIO_DIRECT, OnRadioDirect)
	ON_BN_CLICKED(IDC_RADIO_PRE, OnRadioPre)
	ON_BN_CLICKED(IDC_RADIO_PRE_NOAUTO, OnRadioPreNoauto)
	ON_BN_CLICKED(IDC_RADIO_PROXY, OnRadioProxy)
	ON_WM_SIZE()
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CProxyForm message handlers

void CProxyForm::OnInitialUpdate()
{
	CChildForm::OnInitialUpdate();

	m_ctrlProxy.SetWindowText(m_LastProxy.GetValue());
	m_ctrlBypass.SetWindowText(m_LastBypass.GetValue());
	m_ctrlRadioDirect.SetCheck(m_LastRadioDirect.GetValue() ? BST_CHECKED : BST_UNCHECKED);
	m_ctrlRadioPre.SetCheck(m_LastRadioPre.GetValue() ? BST_CHECKED : BST_UNCHECKED);
	m_ctrlRadioPreNoauto.SetCheck(m_LastRadioPreNoauto.GetValue() ? BST_CHECKED : BST_UNCHECKED);
	m_ctrlRadioProxy.SetCheck(m_LastRadioProxy.GetValue() ? BST_CHECKED : BST_UNCHECKED);

	UpdateControls();
}

void CProxyForm::OnRadioDirect()
{
	UpdateControls();
}

void CProxyForm::OnRadioPre()
{
	UpdateControls();
}

void CProxyForm::OnRadioPreNoauto()
{
	UpdateControls();
}

void CProxyForm::OnRadioProxy()
{
	UpdateControls();
	m_ctrlProxy.SetFocus();
}

void CProxyForm::UpdateControls()
{
	if (m_ctrlRadioProxy.GetCheck() == BST_CHECKED)
	{
		m_ctrlProxy.EnableWindow(TRUE);
		m_ctrlBypass.EnableWindow(TRUE);
	}
	else
	{
		m_ctrlProxy.SetWindowText(_T(""));
		m_ctrlBypass.SetWindowText(_T(""));
		m_ctrlProxy.EnableWindow(FALSE);
		m_ctrlBypass.EnableWindow(FALSE);
	}
	BackupProperties();
}

DWORD CProxyForm::GetInternetOpenAccessType()
{
	if (m_ctrlRadioDirect.GetCheck() == BST_CHECKED)
		return INTERNET_OPEN_TYPE_DIRECT;
	else if (m_ctrlRadioPre.GetCheck() == BST_CHECKED)
		return INTERNET_OPEN_TYPE_PRECONFIG;
	else if (m_ctrlRadioPreNoauto.GetCheck() == BST_CHECKED)
		return INTERNET_OPEN_TYPE_PRECONFIG_WITH_NO_AUTOPROXY;
	else if (m_ctrlRadioProxy.GetCheck() == BST_CHECKED)
		return INTERNET_OPEN_TYPE_PROXY;
	THROW (new CInfoException(IDS_ERROR_INVALID_PROXY));
	return 0;
}

LPCTSTR CProxyForm::GetInternetOpenProxy()
{
	m_ctrlProxy.GetWindowText(m_sProxy);
	return m_sProxy.IsEmpty() ? NULL : (LPCTSTR)m_sProxy;
}

LPCTSTR CProxyForm::GetInternetOpenProxyBypass()
{
	m_ctrlBypass.GetWindowText(m_sProxyBypass);
	return m_sProxyBypass.IsEmpty() ? NULL : (LPCTSTR)m_sProxyBypass;
}

void CProxyForm::OnSize(UINT nType, int cx, int cy)
{
	CChildForm::OnSize(nType, cx, cy);
	
	if (cx == 0 &&
		cy == 0)
	{
		return;
	}

	if (m_ctrlProxy.GetSafeHwnd() == NULL ||
		m_ctrlBypass.GetSafeHwnd() == NULL)
	{
		return;
	}

	CRect r;
	GetClientRect(&r);

	CGlobals::ResizeControl(&m_ctrlProxy, r.right - 2);
	CGlobals::ResizeControl(&m_ctrlBypass, r.right - 2);
}

void CProxyForm::OnDestroy()
{
	BackupProperties();

	CChildForm::OnDestroy();
}

void CProxyForm::BackupProperties()
{
	CString s;
	m_ctrlProxy.GetWindowText(s);
	m_LastProxy.SetValue(s);
	m_ctrlBypass.GetWindowText(s);
	m_LastBypass.SetValue(s);
	m_LastRadioDirect.SetValue(m_ctrlRadioDirect.GetCheck() == BST_CHECKED);
	m_LastRadioPre.SetValue(m_ctrlRadioPre.GetCheck() == BST_CHECKED);
	m_LastRadioPreNoauto.SetValue(m_ctrlRadioPreNoauto.GetCheck() == BST_CHECKED);
	m_LastRadioProxy.SetValue(m_ctrlRadioProxy.GetCheck() == BST_CHECKED);
}

/////////////////////////////////////////////////////////////////////////////

CFtpInfoClient::CFtpInfoClient()
{
	m_pProxyInfo = NULL;
	m_pServerInfo = NULL;
	m_pUserInfo = NULL;
}

void CFtpInfoClient::SetProxyInfo(CProxyInfo *pProxyInfo)
{
	m_pProxyInfo = pProxyInfo;
}

void CFtpInfoClient::SetServerInfo(CServerInfo *pServerInfo)
{
	m_pServerInfo = pServerInfo;
}

void CFtpInfoClient::SetUserInfo(CUserInfo *pUserInfo)
{
	m_pUserInfo = pUserInfo;
}

/////////////////////////////////////////////////////////////////////////////

IMPLEMENT_DYNAMIC(CInfoException, CException)

CInfoException::CInfoException(LPCTSTR szDesc)
:	m_strDescription(szDesc)
{
}

CInfoException::CInfoException(UINT nDescID)
:	m_strDescription((LPCTSTR)nDescID)
{
}

/////////////////////////////////////////////////////////////////////////////
// CConnForm

CConnForm::CConnForm()
:	CChildForm(CConnForm::IDD)
{
	//{{AFX_DATA_INIT(CConnForm)
	//}}AFX_DATA_INIT
	m_bConnected = false;
	m_hInet = NULL;
	m_hFtp = NULL;
}

CConnForm::~CConnForm()
{
	Disconnect();
}

void CConnForm::DoDataExchange(CDataExchange* pDX)
{
	CChildForm::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CConnForm)
	DDX_Control(pDX, IDC_LIST_FILES, m_ctrlFiles);
	DDX_Control(pDX, IDC_EDIT_CUR_DIR, m_ctrlCurDir);
	DDX_Control(pDX, IDC_BUTTON_UP, m_ctrlUp);
	DDX_Control(pDX, IDC_BUTTON_ROOT, m_ctrlRoot);
	DDX_Control(pDX, IDC_BUTTON_DISC, m_ctrlDisconnect);
	DDX_Control(pDX, IDC_BUTTON_CONNECT, m_ctrlConnect);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CConnForm, CChildForm)
	//{{AFX_MSG_MAP(CConnForm)
	ON_BN_CLICKED(IDC_BUTTON_CONNECT, OnButtonConnect)
	ON_BN_CLICKED(IDC_BUTTON_DISC, OnButtonDisconnect)
	ON_BN_CLICKED(IDC_BUTTON_ROOT, OnButtonRoot)
	ON_BN_CLICKED(IDC_BUTTON_UP, OnButtonUp)
	ON_COMMAND(ID_FTP_DIR_CREATE, OnFtpDirCreate)
	ON_UPDATE_COMMAND_UI(ID_FTP_DIR_CREATE, OnUpdateFtpDirCreate)
	ON_COMMAND(ID_FTP_DIR_CHANGE, OnFtpDirChange)
	ON_UPDATE_COMMAND_UI(ID_FTP_DIR_CHANGE, OnUpdateFtpDirChange)
	ON_COMMAND(ID_FTP_DIR_REMOVE, OnFtpDirRemove)
	ON_UPDATE_COMMAND_UI(ID_FTP_DIR_REMOVE, OnUpdateFtpDirRemove)
	ON_COMMAND(ID_FTP_FILE_DEL, OnFtpFileDel)
	ON_UPDATE_COMMAND_UI(ID_FTP_FILE_DEL, OnUpdateFtpFileDel)
	ON_COMMAND(ID_FTP_FILE_GET, OnFtpFileGet)
	ON_UPDATE_COMMAND_UI(ID_FTP_FILE_GET, OnUpdateFtpFileGet)
	ON_COMMAND(ID_FTP_FILE_PUT, OnFtpFilePut)
	ON_UPDATE_COMMAND_UI(ID_FTP_FILE_PUT, OnUpdateFtpFilePut)
	ON_COMMAND(ID_FTP_FILE_REN, OnFtpFileRen)
	ON_UPDATE_COMMAND_UI(ID_FTP_FILE_REN, OnUpdateFtpFileRen)
	ON_COMMAND(ID_FTP_PROPS, OnFtpProps)
	ON_UPDATE_COMMAND_UI(ID_FTP_PROPS, OnUpdateFtpProps)
	ON_COMMAND(ID_FTP_SEL_DIR_CHANGE, OnFtpSelDirChange)
	ON_UPDATE_COMMAND_UI(ID_FTP_SEL_DIR_CHANGE, OnUpdateFtpSelDirChange)
	ON_WM_SIZE()
	ON_COMMAND(ID_FTP_REFRESH, OnFtpRefresh)
	ON_UPDATE_COMMAND_UI(ID_FTP_REFRESH, OnUpdateFtpRefresh)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CConnForm message handlers

void CConnForm::OnInitialUpdate()
{
	CChildForm::OnInitialUpdate();

	PrepareImageList();
	UpdateControls();
}

void CConnForm::PrepareImageList()
{
	int cx = ::GetSystemMetrics(SM_CXSMICON);
	int cy = ::GetSystemMetrics(SM_CYSMICON);
	m_FolderIcon.Load(IDI_ICON_FOLDER, cx, cy);
	m_FileIcon.Load(IDI_ICON_FILE, cx, cy);
	m_ImageList.Create(
		cx,
		cy,
		TRUE,
		2,
		1);
	m_ImageList.Add(m_FolderIcon);
	m_ImageList.Add(m_FileIcon);

	m_ctrlFiles.SetImageList(&m_ImageList, LVSIL_SMALL);
}

void CConnForm::OnButtonConnect()
{
	Connect();
	UpdateControls();
	if (m_bConnected)
		FillList();
}

void CConnForm::OnButtonDisconnect()
{
	Disconnect();
	UpdateControls();
	EmptyList();
	m_ctrlCurDir.SetWindowText(CString());
}

void CConnForm::OnButtonRoot()
{
	ChangeDir(_T("/"));
}

void CConnForm::OnButtonUp()
{
	ChangeDir(_T(".."));
}

void CConnForm::OnFtpDirCreate()
{
	if (!m_bConnected)
		return;
	CDirCreateDlg dlg(this);
	if (dlg.DoModal() != IDOK)
		return;
	CString sFolderName(dlg.GetRemDir());
	if (!sFolderName.IsEmpty())
		if (::FtpCreateDirectory(
				m_hFtp,
				sFolderName) == FALSE)
		{
			CFtpCliApp::PrintWininetError(_T("FtpCreateDirectory()"));
		}
		else
		{
			RefreshList();
		}
}

void CConnForm::OnUpdateFtpDirCreate(CCmdUI *pCmdUI)
{
	BOOL bEnable = FALSE;
	if (m_bConnected)
		bEnable = TRUE;
	pCmdUI->Enable(bEnable);
}

void CConnForm::OnFtpDirChange()
{
	if (!m_bConnected)
		return;
	CDirChangeDlg dlg(this);
	if (dlg.DoModal() != IDOK)
		return;
	CString sFolderName(dlg.GetRemDir());
	if (!sFolderName.IsEmpty())
		ChangeDir(sFolderName);
}

void CConnForm::OnUpdateFtpDirChange(CCmdUI *pCmdUI)
{
	BOOL bEnable = FALSE;
	if (m_bConnected)
		bEnable = TRUE;
	pCmdUI->Enable(bEnable);
}

void CConnForm::OnFtpDirRemove()
{
	if (!m_bConnected)
		return;
	int nItem = GetSelectedItem();
	if (nItem == -1)
		return;
	if (!IsItemFolder(nItem))
		return;
	CString sFolderName = m_ctrlFiles.GetItemText(nItem, 0);
	CWaitCursor wc;
	CString s;
	s.Format(_T("Are you sure to remove remote directory \'%s\'?"), sFolderName);
	if (AfxMessageBox(s, MB_YESNO) != IDYES)
		return;
	wc.Restore();
	if (::FtpRemoveDirectory(
			m_hFtp,
			sFolderName) == FALSE)
	{
		CFtpCliApp::PrintWininetError(_T("FtpRemoveDirectory()"));
	}
	else
	{
		RefreshList();
	}
}

void CConnForm::OnUpdateFtpDirRemove(CCmdUI *pCmdUI)
{
	BOOL bEnable = FALSE;
	if (m_bConnected)
	{
		int nItem = GetSelectedItem();
		if (nItem != -1)
			if (IsItemFolder(nItem))
				bEnable = TRUE;
	}
	pCmdUI->Enable(bEnable);
}

void CConnForm::OnFtpFileDel()
{
	if (!m_bConnected)
		return;
	int nItem = GetSelectedItem();
	if (nItem == -1)
		return;
	if (!IsItemFile(nItem))
		return;
	CString sRemoteFileName = m_ctrlFiles.GetItemText(nItem, 0);
	CWaitCursor wc;
	CString s;
	s.Format(_T("Are you sure to remove remote file \'%s\'?"), sRemoteFileName);
	if (AfxMessageBox(s, MB_YESNO) != IDYES)
		return;
	wc.Restore();
	if (::FtpDeleteFile(
			m_hFtp,
			sRemoteFileName) == FALSE)
	{
		CFtpCliApp::PrintWininetError(_T("FtpDeleteFile()"));
	}
	else
	{
		RefreshList();
	}
}

void CConnForm::OnUpdateFtpFileDel(CCmdUI *pCmdUI)
{
	BOOL bEnable = FALSE;
	if (m_bConnected)
	{
		int nItem = GetSelectedItem();
		if (nItem != -1)
			if (IsItemFile(nItem))
				bEnable = TRUE;
	}
	pCmdUI->Enable(bEnable);
}

void CConnForm::OnFtpFileGet()
{
	if (!m_bConnected)
		return;
	int nItem = GetSelectedItem();
	if (nItem == -1)
		return;
	if (!IsItemFile(nItem))
		return;
	CString sFileName = m_ctrlFiles.GetItemText(nItem, 0);
	CFileGetDlg dlg(this, m_hFtp, sFileName, m_sLastLocalDir);
	dlg.DoModal();
	m_sLastLocalDir = dlg.GetSelLocalDir();
}

void CConnForm::OnUpdateFtpFileGet(CCmdUI *pCmdUI)
{
	BOOL bEnable = FALSE;
	if (m_bConnected)
	{
		int nItem = GetSelectedItem();
		if (nItem != -1)
			if (IsItemFile(nItem))
				bEnable = TRUE;
	}
	pCmdUI->Enable(bEnable);
}

void CConnForm::OnFtpFilePut()
{
	if (!m_bConnected)
		return;
	CString sCurDir;
	m_ctrlCurDir.GetWindowText(sCurDir);
	CFilePutDlg dlg(this, m_hFtp, sCurDir);
	dlg.DoModal();
	CWaitCursor wc;
	RefreshList();
}

void CConnForm::OnUpdateFtpFilePut(CCmdUI *pCmdUI)
{
	BOOL bEnable = FALSE;
	if (m_bConnected)
		bEnable = TRUE;
	pCmdUI->Enable(bEnable);
}

void CConnForm::OnFtpFileRen()
{
	if (!m_bConnected)
		return;
	int nItem = GetSelectedItem();
	if (nItem == -1)
		return;
	CString sFileName = m_ctrlFiles.GetItemText(nItem, 0);
	CFileRenDlg dlg(this, m_hFtp, sFileName);
	dlg.DoModal();
	CWaitCursor wc;
	RefreshList();
}

void CConnForm::OnUpdateFtpFileRen(CCmdUI *pCmdUI)
{
	BOOL bEnable = FALSE;
	if (m_bConnected)
		if (GetSelectedItem() != -1)
			bEnable = TRUE;
	pCmdUI->Enable(bEnable);
}

void CConnForm::OnFtpProps()
{
	if (!m_bConnected)
		return;
	int nItem = GetSelectedItem();
	if (nItem == -1)
		return;
	WIN32_FIND_DATA wfd;
	CString sRemoteFileName = m_ctrlFiles.GetItemText(nItem, 0);
	HINTERNET hFind = NULL;
	{
		CWaitCursor wc;
		hFind = ::FtpFindFirstFile(
			m_hFtp,
			sRemoteFileName,
			&wfd,
			0,
			0);
	}
	if (hFind != NULL)
	{
		::InternetCloseHandle(hFind);
		CString sCurDir;
		m_ctrlCurDir.GetWindowText(sCurDir);
		CFileInfoDlg dlg(this, wfd, sCurDir);
		dlg.DoModal();
	}
	else
	{
		DWORD dwError = ::GetLastError();
		if (dwError != ERROR_NO_MORE_FILES)
		{
			CFtpCliApp::PrintWininetError(_T("FtpFindFirstFile()"), dwError);
		}
	}
}

void CConnForm::OnUpdateFtpProps(CCmdUI *pCmdUI)
{
	BOOL bEnable = FALSE;
	if (m_bConnected)
		if (GetSelectedItem() != -1)
			bEnable = TRUE;
	pCmdUI->Enable(bEnable);
}

void CConnForm::RefreshList()
{
	EmptyList();
	FillList();
}

void CConnForm::UpdateControls()
{
	if (m_bConnected)
	{
		m_ctrlConnect.EnableWindow(FALSE);
		m_ctrlDisconnect.EnableWindow(TRUE);
		CString sCurDir(GetCurDir());
		m_ctrlUp.EnableWindow(sCurDir != _T("/") ? TRUE : FALSE);
		m_ctrlRoot.EnableWindow(TRUE);
		m_ctrlCurDir.SetWindowText(sCurDir);
	}
	else
	{
		m_ctrlConnect.EnableWindow(TRUE);
		m_ctrlDisconnect.EnableWindow(FALSE);
		m_ctrlUp.EnableWindow(FALSE);
		m_ctrlRoot.EnableWindow(FALSE);
	}
}

void CConnForm::Disconnect()
{
	if (m_hFtp != NULL)
	{
		::InternetCloseHandle(m_hFtp);
		m_hFtp = NULL;
	}
	if (m_hInet)
	{
		::InternetCloseHandle(m_hInet);
		m_hInet = NULL;
	}
	m_bConnected = false;
}

void CConnForm::Connect()
{
	TRY
	{
		CWaitCursor wait;
		DWORD dwAccessType = m_pProxyInfo->GetInternetOpenAccessType();
		LPCTSTR szProxy = m_pProxyInfo->GetInternetOpenProxy();
		LPCTSTR szProxyBypass = m_pProxyInfo->GetInternetOpenProxyBypass();
		TRACE(_T("AccessType: %d\n"), dwAccessType);
		TRACE(_T("Proxy: %s\n"), szProxy == NULL ? _T("(NULL)") : szProxy);
		TRACE(_T("ProxyBypass: %s\n"), szProxyBypass == NULL ? _T("(NULL)") : szProxyBypass);
		m_hInet = ::InternetOpen(
				_T("FTP Client"),
				dwAccessType,
				szProxy,
				szProxyBypass,
				0);
		if (m_hInet != NULL)
		{
			LPCTSTR szServerName = m_pServerInfo->GetInternetConnectServerName();
			INTERNET_PORT nServerPort = m_pServerInfo->GetInternetConnectServerPort();
			LPCTSTR szUserName = m_pUserInfo->GetInternetConnectUserName();
			LPCTSTR szPassword = m_pUserInfo->GetInternetConnectPassword();
			DWORD dwFlags = m_pServerInfo->GetInternetConnectFlags();
			TRACE(_T("ServerName: %s\n"), szServerName == NULL ? _T("(NULL)") : szServerName);
			TRACE(_T("ServerPort: %d\n"), nServerPort);
			TRACE(_T("UserName: %s\n"), szUserName == NULL ? _T("(NULL)") : szUserName);
			TRACE(_T("Password: %s\n"), szPassword == NULL ? _T("(NULL)") : szPassword);
			TRACE(_T("Flags: %d\n"), dwFlags);
			m_hFtp = ::InternetConnect(
				m_hInet,
				szServerName,
				nServerPort,
				szUserName,
				szPassword,
				INTERNET_SERVICE_FTP,
				dwFlags,
				0);
			if (m_hFtp != NULL)
				m_bConnected = true;
			else
			{
				CFtpCliApp::PrintWininetError(_T("InternetConnect()"));
				Disconnect();
			}
		}
		else
		{
			CFtpCliApp::PrintWininetError(_T("InternetOpen()"));
		}
	}
	CATCH (CInfoException, pEx)
	{
		AfxMessageBox(pEx->m_strDescription);
		Disconnect();
	}
	END_CATCH
}

void CConnForm::FillList()
{
	CWaitCursor wait;
	WIN32_FIND_DATA wfd;
	HINTERNET hFind = ::FtpFindFirstFile(
		m_hFtp,
		_T("*"),
		&wfd,
		0,
		0);
	if (hFind != NULL)
	{
		CStringSet setDir;
		CStringSet setFile;
		do
		{
			if ((wfd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) != 0)
				setDir.Insert(wfd.cFileName);
			else
				setFile.Insert(wfd.cFileName);
			if (::InternetFindNextFile(hFind, &wfd) == FALSE)
			{
				DWORD dwError = ::GetLastError();
				if (dwError != ERROR_NO_MORE_FILES)
				{
					CFtpCliApp::PrintWininetError(_T("InternetFindNextFile()"), dwError);
				}
				break;
			}
		}
		while (true);
		::InternetCloseHandle(hFind);
		LPCTSTR szName = NULL;
		if (setDir.Begin())
			while ((szName = setDir.Next()) != NULL)
				AddListItem(szName, true);
		if (setFile.Begin())
			while ((szName = setFile.Next()) != NULL)
				AddListItem(szName, false);
	}
	else
	{
		DWORD dwError = ::GetLastError();
		if (dwError != ERROR_NO_MORE_FILES)
		{
			CFtpCliApp::PrintWininetError(_T("FtpFindFirstFile()"), dwError);
		}
	}
}

void CConnForm::AddListItem(LPCTSTR szName, bool bDir)
{
	int nIndex = m_ctrlFiles.GetItemCount();
	int nImageIndex = bDir ? 0 : 1;
	m_ctrlFiles.InsertItem(
		nIndex,
		szName,
		nImageIndex);
	m_ctrlFiles.SetItemData(nIndex, nImageIndex);
}

void CConnForm::EmptyList()
{
	m_ctrlFiles.DeleteAllItems();
}

void CConnForm::ChangeDir(LPCTSTR szDir)
{
	SetCurDir(szDir);
	UpdateControls();
	CWaitCursor wc;
	RefreshList();
}

CString CConnForm::GetCurDir()
{
	CString sResult;
	TCHAR szDir[MAX_PATH];
	DWORD dwCount = sizeof(szDir) / sizeof(TCHAR);
	TCHAR *pBuff = NULL;
	if (::FtpGetCurrentDirectory(
		m_hFtp,
		szDir,
		&dwCount) == FALSE)
	{
		pBuff = new TCHAR [dwCount + 1];
		if (pBuff != NULL)
		{
			if (::FtpGetCurrentDirectory(
				m_hFtp,
				pBuff,
				&dwCount) == TRUE)
			{
				sResult = CString(pBuff, dwCount);
			}
			else
			{
				CFtpCliApp::PrintWininetError(_T("FtpGetCurrentDirectory()"));
			}
			delete []pBuff;
		}
	}
	else
	{
		sResult = CString(szDir, dwCount);
	}
	return sResult;
}

void CConnForm::SetCurDir(LPCTSTR szDir)
{
	if (::FtpSetCurrentDirectory(m_hFtp, szDir) == FALSE)
	{
		CFtpCliApp::PrintWininetError(_T("FtpSetCurrentDirectory()"));
	}
}

int CConnForm::GetSelectedItem()
{
	return m_ctrlFiles.GetNextItem(-1, LVNI_SELECTED);
}

bool CConnForm::IsItemFolder(int nItem)
{
	return m_ctrlFiles.GetItemData(nItem) == 0;
}

bool CConnForm::IsItemFile(int nItem)
{
	return m_ctrlFiles.GetItemData(nItem) == 1;
}

void CConnForm::OnFtpSelDirChange()
{
	if (!m_bConnected)
		return;
	int nItem = GetSelectedItem();
	if (nItem == -1)
		return;
	if (!IsItemFolder(nItem))
		return;
	CString sFileName = m_ctrlFiles.GetItemText(nItem, 0);
	ChangeDir(sFileName);
}

void CConnForm::OnUpdateFtpSelDirChange(CCmdUI *pCmdUI)
{
	BOOL bEnable = FALSE;
	if (m_bConnected)
	{
		int nItem = GetSelectedItem();
		if (nItem != -1)
			if (IsItemFolder(nItem))
				bEnable = TRUE;
	}
	pCmdUI->Enable(bEnable);
}

void CConnForm::OnSize(UINT nType, int cx, int cy) 
{
	CChildForm::OnSize(nType, cx, cy);
	
	if (cx == 0 &&
		cy == 0)
	{
		return;
	}

	if (m_ctrlFiles.GetSafeHwnd() == NULL ||
		m_ctrlCurDir.GetSafeHwnd() == NULL)
	{
		return;
	}

	CRect r;
	GetClientRect(&r);

	CGlobals::ResizeControl(&m_ctrlFiles, r.right - 2, r.bottom - 2);
	CGlobals::ResizeControl(&m_ctrlCurDir, r.right - 2);
}

void CConnForm::OnFtpRefresh()
{
	CWaitCursor wc;
	RefreshList();
}

void CConnForm::OnUpdateFtpRefresh(CCmdUI *pCmdUI)
{
	pCmdUI->Enable(m_bConnected ? TRUE : FALSE);
}
