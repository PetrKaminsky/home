#include "stdafx.h"
#include "FtpCli.h"
#include "MainFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CFtpCliApp

BEGIN_MESSAGE_MAP(CFtpCliApp, CWinApp)
	//{{AFX_MSG_MAP(CFtpCliApp)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// The one and only CFtpCliApp object

CFtpCliApp theApp;
CRegPropertySerializer *CFtpCliApp::s_pPropertySerializer = NULL;

/////////////////////////////////////////////////////////////////////////////
// CFtpCliApp initialization

BOOL CFtpCliApp::InitInstance()
{
	s_pPropertySerializer = new CRegPropertySerializer(_T("HKEY_LOCAL_MACHINE\\Software\\Kamen\\FtpCli"));
	CMainFrame* pFrame = new CMainFrame;
	m_pMainWnd = pFrame;
	pFrame->LoadFrame(IDR_MAINFRAME, WS_OVERLAPPEDWINDOW);
	pFrame->ShowWindow(m_nCmdShow);
	pFrame->UpdateWindow();

	return TRUE;
}

int CFtpCliApp::ExitInstance()
{
	delete s_pPropertySerializer;

	return CWinApp::ExitInstance();
}

void CFtpCliApp::PrintWininetError(LPCTSTR szMethod, DWORD dwError)
{
	LPTSTR szMsg = NULL;
	if (dwError == ERROR_INTERNET_EXTENDED_ERROR)
	{
		DWORD dwExtErr = 0, dwBuffLen = 0;
		if (::InternetGetLastResponseInfo(
			&dwExtErr,
			szMsg,
			&dwBuffLen) == FALSE)
		{
			if (::GetLastError() == ERROR_INSUFFICIENT_BUFFER &&
				dwBuffLen > 0)
			{
				szMsg = new TCHAR[dwBuffLen + 1];
				if (::InternetGetLastResponseInfo(
					&dwExtErr,
					szMsg,
					&dwBuffLen) == FALSE)
				{
					*szMsg = 0;
				}
				else
				{
					szMsg[dwBuffLen] = 0;
				}
			}
		}
	}
	CGlobals::PrintLastError(
		szMethod,
		dwError,
		szMsg,
		_T("wininet.dll"));
	if (szMsg != NULL)
	{
		delete []szMsg;
	}
}
