#if !defined(AFX_FTPCLI_H__33298B2D_56D3_4C8D_9FBB_47859F0F8441__INCLUDED_)
#define AFX_FTPCLI_H__33298B2D_56D3_4C8D_9FBB_47859F0F8441__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols
#include "Property.h"

#define WM_PROGRESS (WM_USER + 1)

/////////////////////////////////////////////////////////////////////////////
// CFtpCliApp:
//

class CFtpCliApp : public CWinApp
{
public:
	static CRegPropertySerializer *s_pPropertySerializer;

	static void PrintWininetError(LPCTSTR szMethod, DWORD dwError = ::GetLastError());

	//{{AFX_VIRTUAL(CFtpCliApp)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	//}}AFX_VIRTUAL

	//{{AFX_MSG(CFtpCliApp)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////
//{{AFX_INSERT_LOCATION}}
// Microsoft eMbedded Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FTPCLI_H__33298B2D_56D3_4C8D_9FBB_47859F0F8441__INCLUDED_)
