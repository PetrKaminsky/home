#if !defined(AFX_FILEOPDLG_H__A1D33EA0_EC76_4A1F_A718_8A6F112A1EE7__INCLUDED_)
#define AFX_FILEOPDLG_H__A1D33EA0_EC76_4A1F_A718_8A6F112A1EE7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CFileGetDlg dialog

class CFileGetDlg : public CDialog
{
public:
	CFileGetDlg(
		CWnd *pParent,
		HINTERNET hFtp,
		LPCTSTR szRemoteFile,
		LPCTSTR szLocalDir);

	LPCTSTR GetSelLocalDir() { return m_sLocalDir; }

	//{{AFX_DATA(CFileGetDlg)
	enum { IDD = IDD_DIALOG_FILE_GET };
	CStatic	m_ctrlStats;
	CEdit	m_ctrlLocalDir;
	CButton	m_ctrlBinary;
	CStatic	m_ctrlPctg;
	CStatic	m_ctrlBytes;
	CEdit	m_ctrlRemoteFile;
	CProgressCtrl	m_ctrlProgress;
	CButton	m_ctrlButtonBrowse;
	CButton	m_ctrlButtonAction;
	//}}AFX_DATA

	//{{AFX_VIRTUAL(CFileGetDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

protected:
	//{{AFX_MSG(CFileGetDlg)
	virtual void OnCancel();
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	afx_msg void OnButtonBrowse();
	afx_msg void OnButtonAction();
	//}}AFX_MSG
	afx_msg LRESULT OnProgress(WPARAM wParam, LPARAM lParam);
	DECLARE_MESSAGE_MAP()

private:
	enum { AFTER_INIT, FILE_SELECTED, TRANSFERRING, DONE } m_State;
	HINTERNET m_hFtp;
	LPCTSTR m_szRemoteFile;
	HANDLE m_hLocal;
	DWORD m_dwTotalBytes;
	DWORD m_dwCurrBytes;
	HINTERNET m_hRemote;
	CString m_sLocalDir;
	COleDateTime m_StartTime;
	COleDateTimeSpan m_ElapsedTime;

	void UpdateControls();
	void InitControls();
	void OnGo();
	bool ExistsLocal(LPCTSTR szLocalFile);
	DWORD GetRemoteSize();
	void Close();
	void Progress();
	void InitCounters();
	void UpdateCounters(DWORD dwBytes);
	void UpdatePctg(int nPctg);
	void UpdateBytes();
	void UpdateStats();
	void PumpMessages();
	CString GetLocalFile();
	void Open();
};

/////////////////////////////////////////////////////////////////////////////
// CFilePutDlg dialog

class CFilePutDlg : public CDialog
{
public:
	CFilePutDlg(
		CWnd *pParent,
		HINTERNET hFtp,
		LPCTSTR szRemoteDir);

	//{{AFX_DATA(CFilePutDlg)
	enum { IDD = IDD_DIALOG_FILE_PUT };
	CStatic	m_ctrlStats;
	CEdit	m_ctrlLocalFile;
	CEdit	m_ctrlRemoteDir;
	CStatic	m_ctrlPctg;
	CStatic	m_ctrlBytes;
	CProgressCtrl	m_ctrlProgress;
	CButton	m_ctrlBinary;
	CButton	m_ctrlButtonBrowse;
	CButton	m_ctrlButtonAction;
	//}}AFX_DATA

	//{{AFX_VIRTUAL(CFilePutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

protected:

	//{{AFX_MSG(CFilePutDlg)
	virtual void OnCancel();
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	afx_msg void OnButtonAction();
	afx_msg void OnButtonBrowse();
	//}}AFX_MSG
	afx_msg LRESULT OnProgress(WPARAM wParam, LPARAM lParam);
	DECLARE_MESSAGE_MAP()

private:
	enum { AFTER_INIT, FILE_SELECTED, TRANSFERRING, DONE } m_State;
	HINTERNET m_hFtp;
	LPCTSTR m_szRemoteDir;
	HANDLE m_hLocal;
	DWORD m_dwTotalBytes;
	DWORD m_dwCurrBytes;
	HINTERNET m_hRemote;
	COleDateTime m_StartTime;
	COleDateTimeSpan m_ElapsedTime;

	void UpdateControls();
	void InitControls();
	void OnGo();
	bool ExistsRemote(LPCTSTR szRemoteFile);
	DWORD GetLocalSize();
	void Close();
	void Progress();
	void InitCounters();
	void UpdateCounters(DWORD dwBytes);
	void UpdatePctg(int nPctg);
	void UpdateBytes();
	void UpdateStats();
	void PumpMessages();
	CString GetRemoteFile();
	void DeleteRemoteFile(LPCTSTR szRemoteFile);
	void Open();
	CString GetLocalFile();
};

/////////////////////////////////////////////////////////////////////////////
// CFileRenDlg dialog

class CFileRenDlg : public CDialog
{
public:
	CFileRenDlg(
		CWnd *pParent,
		HINTERNET hFtp,
		LPCTSTR szRemoteFile);

	//{{AFX_DATA(CFileRenDlg)
	enum { IDD = IDD_DIALOG_FILE_REN };
	CEdit	m_ctrlOld;
	CEdit	m_ctrlNew;
	CButton	m_btnGo;
	//}}AFX_DATA

	//{{AFX_VIRTUAL(CFileRenDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

protected:

	//{{AFX_MSG(CFileRenDlg)
	afx_msg void OnButtonGo();
	virtual BOOL OnInitDialog();
	afx_msg void OnChangeEditNew();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	HINTERNET m_hFtp;
	LPCTSTR m_szRemoteFile;

	void UpdateControls();
	void InitControls();
};

/////////////////////////////////////////////////////////////////////////////
// CDirCreateDlg dialog

class CDirCreateDlg : public CDialog
{
public:
	CDirCreateDlg(CWnd *pParent = NULL);

	LPCTSTR GetRemDir() { return m_sRemDir; }

	//{{AFX_DATA(CDirCreateDlg)
	enum { IDD = IDD_DIALOG_DIR_CREATE };
	CString	m_sRemDir;
	//}}AFX_DATA

	//{{AFX_VIRTUAL(CDirCreateDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

protected:
	//{{AFX_MSG(CDirCreateDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////
// CFileInfoDlg dialog

class CFileInfoDlg : public CDialog
{
public:
	CFileInfoDlg(
		CWnd* pParent,
		WIN32_FIND_DATA &wfd,
		LPCTSTR szLocation);

	//{{AFX_DATA(CFileInfoDlg)
	enum { IDD = IDD_DIALOG_FILE_INFO };
	CListCtrl	m_ctrlProps;
	//}}AFX_DATA

	//{{AFX_VIRTUAL(CFileInfoDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

protected:

	//{{AFX_MSG(CFileInfoDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	WIN32_FIND_DATA &m_Wfd;
	CString m_sLocation;

	void FillValues();
	void FillSize();
	void FillFilename();
	void FillLocation();
	void FillCreatetime();
	void FillWritetime();
	void FillAccesstime();
	void PrepareList();
	void AddProperty(LPCTSTR szProperty, LPCTSTR szValue);
	CString GetFormatTime(SYSTEMTIME &tm);
};

/////////////////////////////////////////////////////////////////////////////
// CDirChangeDlg dialog

class CDirChangeDlg : public CDialog
{
public:
	CDirChangeDlg(CWnd* pParent = NULL);

	LPCTSTR GetRemDir() { return m_sRemDir; }

	//{{AFX_DATA(CDirChangeDlg)
	enum { IDD = IDD_DIALOG_DIR_CHANGE };
	CString	m_sRemDir;
	//}}AFX_DATA

	//{{AFX_VIRTUAL(CDirChangeDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

protected:
	//{{AFX_MSG(CDirChangeDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////
//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FILEOPDLG_H__A1D33EA0_EC76_4A1F_A718_8A6F112A1EE7__INCLUDED_)
