#include "stdafx.h"
#include "FtpCli.h"
#include "MainFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMainFrame

IMPLEMENT_DYNAMIC(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	//{{AFX_MSG_MAP(CMainFrame)
	ON_WM_CREATE()
	ON_WM_SETFOCUS()
	ON_WM_SIZE()
	ON_COMMAND(ID_FRM_SERVER, OnFrmServer)
	ON_COMMAND(ID_FRM_USER, OnFrmUser)
	ON_COMMAND(ID_FRM_PROXY, OnFrmProxy)
	ON_COMMAND(ID_FRM_CONNECT, OnFrmConnect)
	ON_UPDATE_COMMAND_UI(ID_FRM_CONNECT, OnUpdateFrmConnect)
	ON_UPDATE_COMMAND_UI(ID_FRM_PROXY, OnUpdateFrmProxy)
	ON_UPDATE_COMMAND_UI(ID_FRM_SERVER, OnUpdateFrmServer)
	ON_UPDATE_COMMAND_UI(ID_FRM_USER, OnUpdateFrmUser)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMainFrame construction/destruction

CMainFrame::CMainFrame()
{
	m_pServerForm = NULL;
	m_pUserForm = NULL;
	m_pProxyForm = NULL;
	m_pConnForm = NULL;

	m_pActiveView = NULL;
}

CMainFrame::~CMainFrame()
{
	DestroyView((CWnd *&)m_pServerForm);
	DestroyView((CWnd *&)m_pUserForm);
	DestroyView((CWnd *&)m_pProxyForm);
	DestroyView((CWnd *&)m_pConnForm);
}

void CMainFrame::DestroyView(CWnd *&pView)
{
	if (pView != NULL)
	{
		pView->DestroyWindow();
		delete pView;
		pView = NULL;
	}
}

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	m_pServerForm = new CServerForm;
	m_pServerForm->Create(this);
	m_pUserForm = new CUserForm;
	m_pUserForm->Create(this);
	m_pProxyForm = new CProxyForm;
	m_pProxyForm->Create(this);
	m_pConnForm = new CConnForm();
	m_pConnForm->Create(this);
	m_pConnForm->SetProxyInfo(m_pProxyForm);
	m_pConnForm->SetServerInfo(m_pServerForm);
	m_pConnForm->SetUserInfo(m_pUserForm);
	
	if (!m_wndCommandBar.Create(this) ||
		!m_wndCommandBar.InsertMenuBar(IDR_MAINFRAME))
	{
		TRACE0("Failed to create CommandBar\n");
		return -1;      // fail to create
	}

	m_wndCommandBar.SetBarStyle(m_wndCommandBar.GetBarStyle() |
		CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_FIXED);

	ActivateForm(m_pServerForm);

	return 0;
}

/////////////////////////////////////////////////////////////////////////////
// CMainFrame message handlers

void CMainFrame::OnSetFocus(CWnd *pOldWnd)
{
	m_pActiveView->SetFocus();
}

BOOL CMainFrame::OnCmdMsg(UINT nID, int nCode, void* pExtra, AFX_CMDHANDLERINFO* pHandlerInfo)
{
	if (m_pActiveView->OnCmdMsg(nID, nCode, pExtra, pHandlerInfo))
		return TRUE;

	return CFrameWnd::OnCmdMsg(nID, nCode, pExtra, pHandlerInfo);
}

void CMainFrame::OnSize(UINT nType, int cx, int cy)
{
	CFrameWnd::OnSize(nType, cx, cy);

// @@@@ PocketPC
	CRect r;
	GetClientRect(&r);
	if (m_pActiveView != NULL)
		m_pActiveView->MoveWindow(&r);
// @@@@ PocketPC
// @@@@ CE.NET
// @@@@ CE.NET
}

void CMainFrame::ActivateForm(CFormView *pForm)
{
	SetActiveView(pForm);
// @@@@ PocketPC
	CRect r;
	GetClientRect(&r);
// @@@@ PocketPC
// @@@@ CE.NET
//	CRect rc;
//	GetClientRect(&rc);
//	CRect rcBar;
//	m_wndCommandBar.GetWindowRect(rcBar);
//	CRect r(rc.left, rcBar.Height(), rc.right, rc.bottom);
// @@@@ CE.NET
	pForm->MoveWindow(&r);
	if (m_pActiveView != NULL && m_pActiveView != pForm)
	{
		CRect r0;
		r0.SetRectEmpty();
		m_pActiveView->MoveWindow(&r0);
	}
	m_pActiveView = pForm;
}

void CMainFrame::OnFrmServer()
{
	ActivateForm(m_pServerForm);
}

void CMainFrame::OnFrmUser()
{
	ActivateForm(m_pUserForm);
}

void CMainFrame::OnFrmProxy()
{
	ActivateForm(m_pProxyForm);
}

void CMainFrame::OnFrmConnect()
{
	ActivateForm(m_pConnForm);
}

void CMainFrame::OnUpdateFrmConnect(CCmdUI *pCmdUI)
{
	pCmdUI->Enable(TRUE);
	pCmdUI->SetCheck(m_pActiveView == m_pConnForm ? 1 : 0);
}

void CMainFrame::OnUpdateFrmProxy(CCmdUI *pCmdUI)
{
	pCmdUI->Enable(TRUE);
	pCmdUI->SetCheck(m_pActiveView == m_pProxyForm ? 1 : 0);
}

void CMainFrame::OnUpdateFrmServer(CCmdUI *pCmdUI)
{
	pCmdUI->Enable(TRUE);
	pCmdUI->SetCheck(m_pActiveView == m_pServerForm ? 1 : 0);
}

void CMainFrame::OnUpdateFrmUser(CCmdUI *pCmdUI)
{
	pCmdUI->Enable(TRUE);
	pCmdUI->SetCheck(m_pActiveView == m_pUserForm ? 1 : 0);
}
