#if !defined(AFX_FILESELDLG_H__69A84A19_3170_4AAD_92EC_C00AA2534010__INCLUDED_)
#define AFX_FILESELDLG_H__69A84A19_3170_4AAD_92EC_C00AA2534010__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <set>
#include "Globals.h"

class LessLPCTSTR : public std::less<LPCTSTR>
{
public:
	bool operator ()(LPCTSTR x, LPCTSTR y) const;
};

/////////////////////////////////////////////////////////////////////////////

typedef std::set<LPCTSTR, LessLPCTSTR> TStringSet;

class CStringSet : public TStringSet
{
public:
	virtual ~CStringSet();	

	void Insert(LPCTSTR szString);
	bool Begin();
	LPCTSTR Next();

private:
	TStringSet::iterator m_Iter;
};

/////////////////////////////////////////////////////////////////////////////
// CFileSelDlg dialog

class CFileSelDlg : public CDialog
{
public:
	CFileSelDlg(
		CWnd *pParent = NULL,
		LPCTSTR szStartDir = NULL,
		LPCTSTR szFileMask = NULL,
		bool bDirsOnly = false);

	//{{AFX_DATA(CFileSelDlg)
	enum { IDD = IDD_DIALOG_SEL_FILE };
	CListCtrl	m_ctrlFiles;
	CEdit	m_ctrlDir;
	CButton	m_ctrlUp;
	//}}AFX_DATA

	CString GetFilePath();
	CString GetCurDir();

	//{{AFX_VIRTUAL(CFileSelDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

protected:
	//{{AFX_MSG(CFileSelDlg)
	virtual void OnOK();
	afx_msg void OnClickListFiles(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnButtonRoot();
	afx_msg void OnButtonUp();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	CImageList m_ImageList;
	CString m_sCurDir;
	CString m_sFileMask;
	CString m_sSelFileName;
	bool m_bDirsOnly;
	CIcon m_FolderIcon;
	CIcon m_FileIcon;

	void FillList();
	void AddListItem(LPCTSTR szFileName, bool bFolder);
	void EmptyList();
	void ChangeDir(LPCTSTR szDir);
	void ChangeSubdir(LPCTSTR szDir);
	void ChangeUp();
	void ChangeRoot();
	void SetCurDir(LPCTSTR szDir);
	void UpdateControls();
	void PrepareImageList();
};

/////////////////////////////////////////////////////////////////////////////
//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FILESELDLG_H__69A84A19_3170_4AAD_92EC_C00AA2534010__INCLUDED_)
