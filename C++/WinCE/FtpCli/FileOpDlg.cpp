#include "stdafx.h"
#include "ftpcli.h"
#include "FileOpDlg.h"
#include "FileSelDlg.h"
#include "Globals.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CFileGetDlg dialog

CFileGetDlg::CFileGetDlg(
		CWnd *pParent,
		HINTERNET hFtp,
		LPCTSTR szRemoteFile,
		LPCTSTR szLocalDir)
:	CDialog(CFileGetDlg::IDD, pParent),
	m_sLocalDir(szLocalDir)
{
	//{{AFX_DATA_INIT(CFileGetDlg)
	//}}AFX_DATA_INIT
	m_hFtp = hFtp;
	m_szRemoteFile = szRemoteFile;
	m_State = AFTER_INIT;
	m_hLocal = INVALID_HANDLE_VALUE;
	m_dwTotalBytes = 0;
	m_dwCurrBytes = 0;
	m_hRemote = NULL;
}

void CFileGetDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CFileGetDlg)
	DDX_Control(pDX, IDC_STATIC_STATS, m_ctrlStats);
	DDX_Control(pDX, IDC_EDIT_LOCAL, m_ctrlLocalDir);
	DDX_Control(pDX, IDC_CHECK_BINARY, m_ctrlBinary);
	DDX_Control(pDX, IDC_STATIC_PCTG, m_ctrlPctg);
	DDX_Control(pDX, IDC_STATIC_BYTES, m_ctrlBytes);
	DDX_Control(pDX, IDC_EDIT_REMOTE, m_ctrlRemoteFile);
	DDX_Control(pDX, IDC_PROGRESS_GET, m_ctrlProgress);
	DDX_Control(pDX, IDC_BUTTON_BROWSE, m_ctrlButtonBrowse);
	DDX_Control(pDX, IDC_BUTTON_ACTION, m_ctrlButtonAction);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CFileGetDlg, CDialog)
	//{{AFX_MSG_MAP(CFileGetDlg)
	ON_BN_CLICKED(IDC_BUTTON_BROWSE, OnButtonBrowse)
	ON_BN_CLICKED(IDC_BUTTON_ACTION, OnButtonAction)
	//}}AFX_MSG_MAP
	ON_MESSAGE(WM_PROGRESS, OnProgress)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CFileGetDlg message handlers

void CFileGetDlg::OnCancel()
{
	OnOK();
}

void CFileGetDlg::OnOK()
{
	Close();
	if (m_State == TRANSFERRING)
		::DeleteFile(GetLocalFile());
	PumpMessages();
	CDialog::OnOK();
}

BOOL CFileGetDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	
	InitControls();
	if (!m_sLocalDir.IsEmpty())
	{
		m_ctrlLocalDir.SetWindowText(m_sLocalDir);
		m_State = FILE_SELECTED;
	}
	UpdateControls();

	return TRUE;
}

void CFileGetDlg::OnButtonBrowse()
{
	CFileSelDlg dlg(this, NULL, NULL, true);
	if (dlg.DoModal() == IDOK)
	{
		m_sLocalDir = dlg.GetCurDir();
		m_ctrlLocalDir.SetWindowText(m_sLocalDir);
		m_State = FILE_SELECTED;
		UpdateControls();
	}
}

void CFileGetDlg::OnButtonAction()
{
	switch (m_State)
	{
	case FILE_SELECTED:
		OnGo();
		break;

	case TRANSFERRING:
	case DONE:
		OnOK();
		break;
	}
}

void CFileGetDlg::UpdateControls()
{
	switch (m_State)
	{
	case AFTER_INIT:
		m_ctrlPctg.ShowWindow(SW_HIDE);
		m_ctrlBytes.ShowWindow(SW_HIDE);
		m_ctrlProgress.ShowWindow(SW_HIDE);
		m_ctrlButtonBrowse.EnableWindow(TRUE);
		m_ctrlButtonAction.ShowWindow(SW_HIDE);
		m_ctrlBinary.ShowWindow(SW_HIDE);
		break;

	case FILE_SELECTED:
		m_ctrlButtonAction.ShowWindow(SW_SHOW);
		m_ctrlButtonAction.SetWindowText(_T("Go!"));
		m_ctrlBinary.ShowWindow(SW_SHOW);
		break;

	case TRANSFERRING:
		m_ctrlButtonBrowse.EnableWindow(FALSE);
		m_ctrlButtonAction.SetWindowText(_T("Cancel"));
		m_ctrlBinary.EnableWindow(FALSE);
		if (m_dwTotalBytes != 0)
		{
			m_ctrlPctg.ShowWindow(SW_SHOW);
			m_ctrlBytes.ShowWindow(SW_SHOW);
			m_ctrlProgress.ShowWindow(SW_SHOW);
		}
		break;

	case DONE:
		m_ctrlButtonAction.SetWindowText(_T("Done"));
		m_ctrlPctg.ShowWindow(SW_HIDE);
		m_ctrlBytes.ShowWindow(SW_HIDE);
		m_ctrlProgress.ShowWindow(SW_HIDE);
		m_ctrlStats.ShowWindow(SW_SHOW);
		break;
	}
}

void CFileGetDlg::InitControls()
{
	m_ctrlRemoteFile.SetWindowText(m_szRemoteFile);
	m_ctrlBinary.SetCheck(BST_CHECKED);
}

void CFileGetDlg::OnGo()
{
	CString sLocalFile(GetLocalFile());
	if (ExistsLocal(sLocalFile))
		if (AfxMessageBox(_T("Local file exists, Replace?"), MB_ICONEXCLAMATION | MB_YESNO) == IDNO)
			return;
	m_dwTotalBytes = GetRemoteSize();
	Open();
	if (m_hLocal == INVALID_HANDLE_VALUE ||
		m_hRemote == NULL)
	{
		Close();
		::DeleteFile(sLocalFile);
		return;
	}
	m_State = TRANSFERRING;
	UpdateControls();
	InitCounters();
	Progress();
}

void CFileGetDlg::Open()
{
	bool bBinary = m_ctrlBinary.GetCheck() == BST_CHECKED;
	m_hRemote = ::FtpOpenFile(
		m_hFtp,
		m_szRemoteFile,
		GENERIC_READ,
		(bBinary ? FTP_TRANSFER_TYPE_BINARY : FTP_TRANSFER_TYPE_ASCII) |
			INTERNET_FLAG_DONT_CACHE,
		0);
	if (m_hRemote == NULL)
	{
		CFtpCliApp::PrintWininetError(_T("FtpOpenFile()"));
	}
	m_hLocal = ::CreateFile(
		GetLocalFile(),
		GENERIC_WRITE,
		0,
		NULL,
		CREATE_ALWAYS,
		0,
		NULL);
	if (m_hLocal == INVALID_HANDLE_VALUE)
	{
		CGlobals::PrintLastError(_T("CreateFile()"));
	}
}

bool CFileGetDlg::ExistsLocal(LPCTSTR szLocalFile)
{
	bool bResult = false;
	WIN32_FIND_DATA wfd;
	HANDLE hFind = ::FindFirstFile(
		szLocalFile,
		&wfd);
	if (hFind != INVALID_HANDLE_VALUE)
	{
		::FindClose(hFind);
		bResult = true;
	}
	return bResult;
}

DWORD CFileGetDlg::GetRemoteSize()
{
	DWORD dwResult = 0;
	WIN32_FIND_DATA wfd;
	CString sWildRemote(m_szRemoteFile);
	sWildRemote.Replace(_T(' '), _T('?'));
	sWildRemote += _T('*');
	HINTERNET hFind = ::FtpFindFirstFile(
		m_hFtp,
		sWildRemote,
		&wfd,
		0,
		0);
	if (hFind != NULL)
	{
		while (true)
		{
			if (_tcscmp(wfd.cFileName, m_szRemoteFile) == 0)
			{
				dwResult = wfd.nFileSizeLow;
				break;
			}
			if (::InternetFindNextFile(
				hFind,
				&wfd) == FALSE)
			{
				DWORD dwError = ::GetLastError();
				if (dwError != ERROR_NO_MORE_FILES)
				{
					CFtpCliApp::PrintWininetError(_T("InternetFindNextFile()"), dwError);
				}
				break;
			}
		}
		::InternetCloseHandle(hFind);
	}
	else
	{
		CFtpCliApp::PrintWininetError(_T("FtpFindFirstFile()"));
	}
	return dwResult;
}

void CFileGetDlg::Close()
{
	if (m_hLocal != INVALID_HANDLE_VALUE)
	{
		::CloseHandle(m_hLocal);
		m_hLocal = INVALID_HANDLE_VALUE;
	}
	if (m_hRemote != NULL)
	{
		::InternetCloseHandle(m_hRemote);
		m_hRemote = NULL;
	}
}

LRESULT CFileGetDlg::OnProgress(WPARAM wParam, LPARAM lParam)
{
	bool bContinue = false;
	BYTE Buffer[4096];
	DWORD dwRead = 0;
	if (::InternetReadFile(
		m_hRemote,
		Buffer,
		sizeof(Buffer),
		&dwRead) != FALSE)
	{
		if (dwRead != 0)
		{
			DWORD dwWritten = 0;
			if (::WriteFile(
				m_hLocal,
				Buffer,
				dwRead,
				&dwWritten,
				NULL) != FALSE)
			{
				UpdateCounters(dwRead);
				PumpMessages();
				bContinue = true;
			}
			else
			{
				CGlobals::PrintLastError(_T("WriteFile()"));
			}
		}
		else
		{
			m_State = DONE;
			m_ElapsedTime = COleDateTime::GetCurrentTime() - m_StartTime;
			UpdateStats();
		}
	}
	else
	{
		CFtpCliApp::PrintWininetError(_T("InternetReadFile()"));
	}
	if (bContinue)
	{
		Progress();
	}
	else
	{
		Close();
		UpdateControls();
		PumpMessages();
	}
	return 0;
}

void CFileGetDlg::Progress()
{
	PostMessage(WM_PROGRESS);
}

void CFileGetDlg::InitCounters()
{
	if (m_dwTotalBytes != 0)
	{
		m_ctrlProgress.SetRange(0, 100);
		int nPctg = 0;
		m_ctrlProgress.SetPos(nPctg);
		UpdatePctg(nPctg);
		UpdateBytes();
	}
	m_StartTime = COleDateTime::GetCurrentTime();
}

void CFileGetDlg::UpdateCounters(DWORD dwBytes)
{
	if (m_dwTotalBytes != 0)
	{
		m_dwCurrBytes += dwBytes;
		int nPctg = 100 * ((double)m_dwCurrBytes / m_dwTotalBytes);
		m_ctrlProgress.SetPos(nPctg);
		UpdatePctg(nPctg);
		UpdateBytes();
	}
}

void CFileGetDlg::UpdatePctg(int nPctg)
{
	CString sPctg;
	sPctg.Format(_T("%d %%"), nPctg);
	m_ctrlPctg.SetWindowText(sPctg);
}

void CFileGetDlg::UpdateBytes()
{
	CString sBytes;
	sBytes.Format(
		_T("%s of %s"),
		CGlobals::FormatSize(m_dwCurrBytes),
		CGlobals::FormatSize(m_dwTotalBytes));
	m_ctrlBytes.SetWindowText(sBytes);
}

void CFileGetDlg::PumpMessages()
{
	MSG msg;
	HWND hWnd = GetSafeHwnd();
	while (::PeekMessage(&msg, hWnd,  0, 0, PM_REMOVE) == TRUE)
	{
		if (!::IsDialogMessage(hWnd, &msg))
		{
			::TranslateMessage(&msg); 
			::DispatchMessage(&msg); 
		}
	}
}

CString CFileGetDlg::GetLocalFile()
{
	CString sLocalFile;
	m_ctrlLocalDir.GetWindowText(sLocalFile);
	if (sLocalFile[sLocalFile.GetLength() - 1] != _T('\\'))
		sLocalFile += _T('\\');
	sLocalFile += m_szRemoteFile;
	return sLocalFile;
}

void CFileGetDlg::UpdateStats()
{
	CString sTime, sSpeed;
	sTime.Format(
		_T("%02d:%02d:%02d"),
		m_ElapsedTime.GetHours(),
		m_ElapsedTime.GetMinutes(),
		m_ElapsedTime.GetSeconds());
	double secs = m_ElapsedTime.GetTotalSeconds();
	if (secs > 0)
	{
		double speed = (double)m_dwCurrBytes/secs;
		sSpeed.Format(_T("%s/s"), CGlobals::FormatSize((DWORD)speed));
		sTime += _T(", ");
		sTime += sSpeed;
	}
	m_ctrlStats.SetWindowText(sTime);
}

/////////////////////////////////////////////////////////////////////////////
// CFilePutDlg dialog

CFilePutDlg::CFilePutDlg(
		CWnd *pParent,
		HINTERNET hFtp,
		LPCTSTR szRemoteDir)
:	CDialog(CFilePutDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CFilePutDlg)
	//}}AFX_DATA_INIT
	m_hFtp = hFtp;
	m_szRemoteDir = szRemoteDir;
	m_State = AFTER_INIT;
	m_hLocal = INVALID_HANDLE_VALUE;
	m_dwTotalBytes = 0;
	m_dwCurrBytes = 0;
	m_hRemote = NULL;
}

void CFilePutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CFilePutDlg)
	DDX_Control(pDX, IDC_STATIC_STATS, m_ctrlStats);
	DDX_Control(pDX, IDC_EDIT_LOCAL, m_ctrlLocalFile);
	DDX_Control(pDX, IDC_EDIT_REMOTE, m_ctrlRemoteDir);
	DDX_Control(pDX, IDC_STATIC_PCTG, m_ctrlPctg);
	DDX_Control(pDX, IDC_STATIC_BYTES, m_ctrlBytes);
	DDX_Control(pDX, IDC_PROGRESS_PUT, m_ctrlProgress);
	DDX_Control(pDX, IDC_CHECK_BINARY, m_ctrlBinary);
	DDX_Control(pDX, IDC_BUTTON_BROWSE, m_ctrlButtonBrowse);
	DDX_Control(pDX, IDC_BUTTON_ACTION, m_ctrlButtonAction);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CFilePutDlg, CDialog)
	//{{AFX_MSG_MAP(CFilePutDlg)
	ON_BN_CLICKED(IDC_BUTTON_ACTION, OnButtonAction)
	ON_BN_CLICKED(IDC_BUTTON_BROWSE, OnButtonBrowse)
	//}}AFX_MSG_MAP
	ON_MESSAGE(WM_PROGRESS, OnProgress)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CFilePutDlg message handlers

void CFilePutDlg::OnCancel()
{
	OnOK();
}

void CFilePutDlg::OnOK()
{
	Close();
	if (m_State == TRANSFERRING)
		DeleteRemoteFile(GetRemoteFile());
	PumpMessages();
	CDialog::OnOK();
}

BOOL CFilePutDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	
	InitControls();
	UpdateControls();

	return TRUE;
}

void CFilePutDlg::OnButtonAction()
{
	switch (m_State)
	{
	case FILE_SELECTED:
		OnGo();
		break;

	case TRANSFERRING:
	case DONE:
		OnOK();
		break;
	}
}

void CFilePutDlg::OnButtonBrowse()
{
	CFileSelDlg dlg(this);
	if (dlg.DoModal() == IDOK)
	{
		m_ctrlLocalFile.SetWindowText(dlg.GetFilePath());
		m_State = FILE_SELECTED;
		UpdateControls();
	}
}

void CFilePutDlg::UpdateControls()
{
	switch (m_State)
	{
	case AFTER_INIT:
		m_ctrlPctg.ShowWindow(SW_HIDE);
		m_ctrlBytes.ShowWindow(SW_HIDE);
		m_ctrlProgress.ShowWindow(SW_HIDE);
		m_ctrlButtonBrowse.EnableWindow(TRUE);
		m_ctrlButtonAction.ShowWindow(SW_HIDE);
		m_ctrlBinary.ShowWindow(SW_HIDE);
		break;

	case FILE_SELECTED:
		m_ctrlButtonAction.ShowWindow(SW_SHOW);
		m_ctrlButtonAction.SetWindowText(_T("Go!"));
		m_ctrlBinary.ShowWindow(SW_SHOW);
		break;

	case TRANSFERRING:
		m_ctrlButtonBrowse.EnableWindow(FALSE);
		m_ctrlButtonAction.SetWindowText(_T("Cancel"));
		m_ctrlBinary.EnableWindow(FALSE);
		if (m_dwTotalBytes != 0)
		{
			m_ctrlPctg.ShowWindow(SW_SHOW);
			m_ctrlBytes.ShowWindow(SW_SHOW);
			m_ctrlProgress.ShowWindow(SW_SHOW);
		}
		break;

	case DONE:
		m_ctrlButtonAction.SetWindowText(_T("Done"));
		m_ctrlPctg.ShowWindow(SW_HIDE);
		m_ctrlBytes.ShowWindow(SW_HIDE);
		m_ctrlProgress.ShowWindow(SW_HIDE);
		m_ctrlStats.ShowWindow(SW_SHOW);
		break;
	}
}

void CFilePutDlg::InitControls()
{
	m_ctrlRemoteDir.SetWindowText(m_szRemoteDir);
	m_ctrlBinary.SetCheck(BST_CHECKED);
}

void CFilePutDlg::OnGo()
{
	CString sRemoteFile(GetRemoteFile());
	if (ExistsRemote(sRemoteFile))
		if (AfxMessageBox(_T("Remote file exists, Replace?"), MB_ICONEXCLAMATION | MB_YESNO) == IDNO)
			return;
	m_dwTotalBytes = GetLocalSize();
	Open();
	if (m_hLocal == INVALID_HANDLE_VALUE ||
		m_hRemote == NULL)
	{
		Close();
		DeleteRemoteFile(sRemoteFile);
		return;
	}
	m_State = TRANSFERRING;
	UpdateControls();
	InitCounters();
	Progress();
}

void CFilePutDlg::Open()
{
	m_hLocal = ::CreateFile(
		GetLocalFile(),
		GENERIC_READ,
		FILE_SHARE_READ | FILE_SHARE_WRITE,
		NULL,
		OPEN_EXISTING,
		0,
		NULL);
	if (m_hLocal == INVALID_HANDLE_VALUE)
	{
		CGlobals::PrintLastError(_T("CreateFile()"));
	}
	bool bBinary = m_ctrlBinary.GetCheck() == BST_CHECKED;
	m_hRemote = ::FtpOpenFile(
		m_hFtp,
		GetRemoteFile(),
		GENERIC_WRITE,
		(bBinary ? FTP_TRANSFER_TYPE_BINARY : FTP_TRANSFER_TYPE_ASCII) |
			INTERNET_FLAG_DONT_CACHE,
		0);
	if (m_hRemote == NULL)
	{
		CFtpCliApp::PrintWininetError(_T("FtpOpenFile()"));
	}
}

CString CFilePutDlg::GetLocalFile()
{
	CString sResult;
	m_ctrlLocalFile.GetWindowText(sResult);
	return sResult;
}

bool CFilePutDlg::ExistsRemote(LPCTSTR szRemoteFile)
{
	bool bResult = false;
	WIN32_FIND_DATA wfd;
	CString sWildRemote(szRemoteFile);
	sWildRemote.Replace(_T(' '), _T('?'));
	sWildRemote += _T('*');
	HINTERNET hFind = ::FtpFindFirstFile(
		m_hFtp,
		sWildRemote,
		&wfd,
		0,
		0);
	if (hFind != NULL)
	{
		while (true)
		{
			if (_tcscmp(wfd.cFileName, szRemoteFile) == 0)
			{
				bResult = true;
				break;
			}
			if (::InternetFindNextFile(
				hFind,
				&wfd) == FALSE)
			{
				DWORD dwError = ::GetLastError();
				if (dwError != ERROR_NO_MORE_FILES)
				{
					CFtpCliApp::PrintWininetError(_T("InternetFindNextFile()"), dwError);
				}
				break;
			}
		}
		::InternetCloseHandle(hFind);
	}
	else
	{
		DWORD dwError = ::GetLastError();
		if (dwError != ERROR_NO_MORE_FILES)
		{
			CFtpCliApp::PrintWininetError(_T("FtpFindFirstFile()"), dwError);
		}
	}
	return bResult;
}

DWORD CFilePutDlg::GetLocalSize()
{
	DWORD dwResult = 0;
	WIN32_FIND_DATA wfd;
	HANDLE hFind = ::FindFirstFile(
		GetLocalFile(),
		&wfd);
	if (hFind != INVALID_HANDLE_VALUE)
	{
		::FindClose(hFind);
		dwResult = wfd.nFileSizeLow;
	}
	else
	{
		CFtpCliApp::PrintWininetError(_T("FindFirstFile()"));
	}
	return dwResult;
}

void CFilePutDlg::Close()
{
	if (m_hLocal != INVALID_HANDLE_VALUE)
	{
		::CloseHandle(m_hLocal);
		m_hLocal = INVALID_HANDLE_VALUE;
	}
	if (m_hRemote != NULL)
	{
		::InternetCloseHandle(m_hRemote);
		m_hRemote = NULL;
	}
}

LRESULT CFilePutDlg::OnProgress(WPARAM wParam, LPARAM lParam)
{
	bool bContinue = false;
	BYTE Buffer[4096];
	DWORD dwRead = 0;
	if (::ReadFile(
		m_hLocal,
		Buffer,
		sizeof(Buffer),
		&dwRead,
		NULL) != FALSE)
	{
		if (dwRead != 0)
		{
			DWORD dwWritten = 0;
			if (::InternetWriteFile(
				m_hRemote,
				Buffer,
				dwRead,
				&dwWritten) != FALSE)
			{
				UpdateCounters(dwRead);
				PumpMessages();
				bContinue = true;
			}
			else
			{
				CFtpCliApp::PrintWininetError(_T("InternetWriteFile()"));
			}
		}
		else
		{
			m_State = DONE;
			m_ElapsedTime = COleDateTime::GetCurrentTime() - m_StartTime;
			UpdateStats();
		}
	}
	else
	{
		CGlobals::PrintLastError(_T("ReadFile()"));
	}
	if (bContinue)
	{
		Progress();
	}
	else
	{
		Close();
		UpdateControls();
		PumpMessages();
	}
	return 0;
}

void CFilePutDlg::Progress()
{
	PostMessage(WM_PROGRESS);
}

void CFilePutDlg::InitCounters()
{
	if (m_dwTotalBytes != 0)
	{
		m_ctrlProgress.SetRange(0, 100);
		int nPctg = 0;
		m_ctrlProgress.SetPos(nPctg);
		UpdatePctg(nPctg);
		UpdateBytes();
	}
	m_StartTime = COleDateTime::GetCurrentTime();
}

void CFilePutDlg::UpdateCounters(DWORD dwBytes)
{
	if (m_dwTotalBytes != 0)
	{
		m_dwCurrBytes += dwBytes;
		int nPctg = 100 * ((double)m_dwCurrBytes / m_dwTotalBytes);
		m_ctrlProgress.SetPos(nPctg);
		UpdatePctg(nPctg);
		UpdateBytes();
	}
}

void CFilePutDlg::UpdatePctg(int nPctg)
{
	CString sPctg;
	sPctg.Format(_T("%d %%"), nPctg);
	m_ctrlPctg.SetWindowText(sPctg);
}

void CFilePutDlg::UpdateBytes()
{
	CString sBytes;
	sBytes.Format(
		_T("%s of %s"),
		CGlobals::FormatSize(m_dwCurrBytes),
		CGlobals::FormatSize(m_dwTotalBytes));
	m_ctrlBytes.SetWindowText(sBytes);
}

void CFilePutDlg::PumpMessages()
{
	MSG msg;
	HWND hWnd = GetSafeHwnd();
	while (::PeekMessage(&msg, hWnd,  0, 0, PM_REMOVE) == TRUE)
	{
		if (!::IsDialogMessage(hWnd, &msg))
		{
			::TranslateMessage(&msg); 
			::DispatchMessage(&msg); 
		}
	}
}

CString CFilePutDlg::GetRemoteFile()
{
	CString sResult;
	m_ctrlLocalFile.GetWindowText(sResult);
	int nIndex = sResult.ReverseFind(_T('\\'));
	if (nIndex != -1)
		sResult = sResult.Mid(nIndex + 1);
	return sResult;
}

void CFilePutDlg::DeleteRemoteFile(LPCTSTR szRemoteFile)
{
	if (::FtpDeleteFile(
			m_hFtp,
			szRemoteFile) == FALSE)
	{
		CFtpCliApp::PrintWininetError(_T("FtpDeleteFile()"));
	}
}

void CFilePutDlg::UpdateStats()
{
	CString sTime, sSpeed;
	sTime.Format(
		_T("%02d:%02d:%02d"),
		m_ElapsedTime.GetHours(),
		m_ElapsedTime.GetMinutes(),
		m_ElapsedTime.GetSeconds());
	double secs = m_ElapsedTime.GetTotalSeconds();
	if (secs > 0)
	{
		double speed = (double)m_dwCurrBytes/secs;
		sSpeed.Format(_T("%s/s"), CGlobals::FormatSize((DWORD)speed));
		sTime += _T(", ");
		sTime += sSpeed;
	}
	m_ctrlStats.SetWindowText(sTime);
}

/////////////////////////////////////////////////////////////////////////////
// CFileRenDlg dialog

CFileRenDlg::CFileRenDlg(
		CWnd *pParent,
		HINTERNET hFtp,
		LPCTSTR szRemoteFile)
:	CDialog(CFileRenDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CFileRenDlg)
	//}}AFX_DATA_INIT
	m_hFtp = hFtp;
	m_szRemoteFile = szRemoteFile;
}

void CFileRenDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CFileRenDlg)
	DDX_Control(pDX, IDC_EDIT_OLD, m_ctrlOld);
	DDX_Control(pDX, IDC_EDIT_NEW, m_ctrlNew);
	DDX_Control(pDX, ID_BUTTON_GO, m_btnGo);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CFileRenDlg, CDialog)
	//{{AFX_MSG_MAP(CFileRenDlg)
	ON_BN_CLICKED(ID_BUTTON_GO, OnButtonGo)
	ON_EN_CHANGE(IDC_EDIT_NEW, OnChangeEditNew)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CFileRenDlg message handlers

void CFileRenDlg::OnButtonGo()
{
	CString sNew;
	m_ctrlNew.GetWindowText(sNew);
	if (sNew == m_szRemoteFile)
	{
		CDialog::OnOK();
	}
	else if (::FtpRenameFile(
			m_hFtp,
			m_szRemoteFile,
			sNew) == FALSE)
	{
		CFtpCliApp::PrintWininetError(_T("FtpRenameFile()"));
	}
	else
	{
		CDialog::OnOK();
	}
}

BOOL CFileRenDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	InitControls();
	UpdateControls();
	m_ctrlNew.SetFocus();

	return FALSE;
}

void CFileRenDlg::UpdateControls()
{
	CString sNew;
	m_ctrlNew.GetWindowText(sNew);
	m_btnGo.EnableWindow(sNew.IsEmpty() ? FALSE : TRUE);
}

void CFileRenDlg::InitControls()
{
	m_ctrlOld.SetWindowText(m_szRemoteFile);
	m_ctrlNew.SetWindowText(m_szRemoteFile);
}

void CFileRenDlg::OnChangeEditNew()
{
	UpdateControls();
}

/////////////////////////////////////////////////////////////////////////////
// CDirCreateDlg dialog

CDirCreateDlg::CDirCreateDlg(CWnd *pParent /*=NULL*/)
:	CDialog(CDirCreateDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDirCreateDlg)
	m_sRemDir = _T("");
	//}}AFX_DATA_INIT
}

void CDirCreateDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDirCreateDlg)
	DDX_Text(pDX, IDC_EDIT_REM_DIR, m_sRemDir);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CDirCreateDlg, CDialog)
	//{{AFX_MSG_MAP(CDirCreateDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CFileInfoDlg dialog

CFileInfoDlg::CFileInfoDlg(
	CWnd* pParent,
	WIN32_FIND_DATA &wfd,
	LPCTSTR szLocation)
:	CDialog(CFileInfoDlg::IDD, pParent),
	m_Wfd(wfd),
	m_sLocation(szLocation)
{
	//{{AFX_DATA_INIT(CFileInfoDlg)
	//}}AFX_DATA_INIT
}

void CFileInfoDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CFileInfoDlg)
	DDX_Control(pDX, IDC_LIST_PROPS, m_ctrlProps);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CFileInfoDlg, CDialog)
	//{{AFX_MSG_MAP(CFileInfoDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CFileInfoDlg message handlers

BOOL CFileInfoDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	PrepareList();
	FillValues();

	return TRUE;
}

void CFileInfoDlg::FillValues()
{
	FillLocation();
	FillFilename();
	FillSize();
	FillCreatetime();
	FillWritetime();
	FillAccesstime();
}

void CFileInfoDlg::FillSize()
{
	AddProperty(_T("Size"), CGlobals::FormatSize(m_Wfd.nFileSizeLow));
}

void CFileInfoDlg::FillFilename()
{
	AddProperty(_T("Name"), m_Wfd.cFileName);
}

void CFileInfoDlg::FillLocation()
{
	AddProperty(_T("Location"), m_sLocation);
}

void CFileInfoDlg::FillCreatetime()
{
	if (m_Wfd.ftCreationTime.dwHighDateTime == 0 &&
		m_Wfd.ftCreationTime.dwLowDateTime == 0)
		return;
	SYSTEMTIME tm;
	if (FileTimeToSystemTime(&(m_Wfd.ftCreationTime), &tm) == FALSE)
		return;
	AddProperty(_T("Created"), GetFormatTime(tm));
}

void CFileInfoDlg::FillWritetime()
{
	if (m_Wfd.ftLastWriteTime.dwHighDateTime == 0 &&
		m_Wfd.ftLastWriteTime.dwLowDateTime == 0)
		return;
	SYSTEMTIME tm;
	if (FileTimeToSystemTime(&(m_Wfd.ftLastWriteTime), &tm) == FALSE)
		return;
	AddProperty(_T("Written"), GetFormatTime(tm));
}

void CFileInfoDlg::FillAccesstime()
{
	if (m_Wfd.ftLastAccessTime.dwHighDateTime == 0 &&
		m_Wfd.ftLastAccessTime.dwLowDateTime == 0)
		return;
	SYSTEMTIME tm;
	if (FileTimeToSystemTime(&(m_Wfd.ftLastAccessTime), &tm) == FALSE)
		return;
	AddProperty(_T("Accessed"), GetFormatTime(tm));
}

void CFileInfoDlg::PrepareList()
{
	m_ctrlProps.InsertColumn(0, _T("Property"), LVCFMT_LEFT, 70);
	m_ctrlProps.InsertColumn(1, _T("Value"), LVCFMT_LEFT, 150);
}

void CFileInfoDlg::AddProperty(LPCTSTR szProperty, LPCTSTR szValue)
{
	int nItem = m_ctrlProps.GetItemCount();
	m_ctrlProps.InsertItem(nItem, szProperty);
	m_ctrlProps.SetItemText(nItem, 1, szValue);
}

CString CFileInfoDlg::GetFormatTime(SYSTEMTIME &tm)
{
	CString sResult;
	sResult.Format(
		_T("%02d.%02d.%04d %02d:%02d:%02d.%03d"),
		tm.wDay,
		tm.wMonth,
		tm.wYear,
		tm.wHour,
		tm.wMinute,
		tm.wSecond,
		tm.wMilliseconds);
	return sResult;
}

/////////////////////////////////////////////////////////////////////////////
// CDirChangeDlg dialog

CDirChangeDlg::CDirChangeDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CDirChangeDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDirChangeDlg)
	m_sRemDir = _T("");
	//}}AFX_DATA_INIT
}

void CDirChangeDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDirChangeDlg)
	DDX_Text(pDX, IDC_EDIT_REM_DIR, m_sRemDir);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CDirChangeDlg, CDialog)
	//{{AFX_MSG_MAP(CDirChangeDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDirChangeDlg message handlers
