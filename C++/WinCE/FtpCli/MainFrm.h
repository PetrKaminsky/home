#if !defined(AFX_MAINFRM_H__A7CE93B7_FF59_4C4D_A66B_B04953F8BBF9__INCLUDED_)
#define AFX_MAINFRM_H__A7CE93B7_FF59_4C4D_A66B_B04953F8BBF9__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "ChildView.h"

/////////////////////////////////////////////////////////////////////////////

class CMainFrame : public CFrameWnd
{
public:
	CMainFrame();

protected: 
	DECLARE_DYNAMIC(CMainFrame)

	virtual ~CMainFrame();

public:
	//{{AFX_VIRTUAL(CMainFrame)
	virtual BOOL OnCmdMsg(UINT nID, int nCode, void* pExtra, AFX_CMDHANDLERINFO* pHandlerInfo);
	//}}AFX_VIRTUAL

protected:  // control bar embedded members
	CCeCommandBar	m_wndCommandBar;
	CServerForm *m_pServerForm;
	CUserForm *m_pUserForm;
	CProxyForm *m_pProxyForm;
	CConnForm *m_pConnForm;

	CView *m_pActiveView;

	void ActivateForm(CFormView *pForm);

protected:
	//{{AFX_MSG(CMainFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSetFocus(CWnd *pOldWnd);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnFrmServer();
	afx_msg void OnFrmUser();
	afx_msg void OnFrmProxy();
	afx_msg void OnFrmConnect();
	afx_msg void OnUpdateFrmConnect(CCmdUI* pCmdUI);
	afx_msg void OnUpdateFrmProxy(CCmdUI* pCmdUI);
	afx_msg void OnUpdateFrmServer(CCmdUI* pCmdUI);
	afx_msg void OnUpdateFrmUser(CCmdUI* pCmdUI);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	void DestroyView(CWnd *&pView);
};

/////////////////////////////////////////////////////////////////////////////
//{{AFX_INSERT_LOCATION}}
// Microsoft eMbedded Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MAINFRM_H__A7CE93B7_FF59_4C4D_A66B_B04953F8BBF9__INCLUDED_)
