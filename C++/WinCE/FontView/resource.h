//{{NO_DEPENDENCIES}}
// Microsoft eMbedded Visual C++ generated include file.
// Used by FontView.rc
//
#define IDR_MAINFRAME                   128
#define IDD_FORM_PROP                   129
#define IDS_WEIGHT_DONTCARE             129
#define IDD_FORM_PREVIEW                130
#define IDS_WEIGHT_THIN                 130
#define IDS_WEIGHT_EXTRALIGHT           131
#define IDD_FORM_HIDPI                  131
#define IDS_WEIGHT_LIGHT                132
#define IDS_WEIGHT_NORMAL               133
#define IDS_WEIGHT_MEDIUM               134
#define IDS_WEIGHT_SEMIBOLD             135
#define IDS_WEIGHT_BOLD                 136
#define IDS_WEIGHT_EXTRABOLD            137
#define IDS_WEIGHT_HEAVY                138
#define IDS_CHARSET_ANSI                139
#define IDS_CHARSET_BALTIC              140
#define IDS_CHARSET_CHINESEBIG5         141
#define IDS_CHARSET_DEFAULT             142
#define IDS_CHARSET_EASTEUROPE          143
#define IDS_CHARSET_GB2312              144
#define IDS_CHARSET_GREEK               145
#define IDS_CHARSET_HANGEUL             146
#define IDS_CHARSET_MAC                 147
#define IDS_CHARSET_OEM                 148
#define IDS_CHARSET_RUSSIAN             149
#define IDS_CHARSET_SHIFTJIS            150
#define IDS_CHARSET_SYMBOL              151
#define IDS_CHARSET_TURKISH             152
#define IDS_CHARSET_JOHAB               153
#define IDS_CHARSET_HEBREW              154
#define IDS_CHARSET_ARABIC              155
#define IDS_CHARSET_THAI                156
#define IDS_CHARSET_VIETNAMESE          157
#define IDS_PRECIS_DEFAULT              158
#define IDS_PRECIS_RASTER               159
#define IDS_PRECIS_STRING               160
#define IDS_QUALITY_ANTIALIASED         161
#define IDS_QUALITY_NONANTIALIASED      162
#define IDS_QUALITY_CLEARTYPE_COMPAT    163
#define IDS_QUALITY_CLEARTYPE           164
#define IDS_QUALITY_DEFAULT             165
#define IDS_QUALITY_DRAFT               166
#define IDS_FAMILY_DECORATIVE           167
#define IDS_FAMILY_DONTCARE             168
#define IDS_FAMILY_MODERN               169
#define IDS_FAMILY_ROMAN                170
#define IDS_FAMILY_SCRIPT               171
#define IDS_FAMILY_SWISS                172
#define IDS_PITCH_DEFAULT               173
#define IDS_PITCH_FIXED                 174
#define IDS_PITCH_VARIABLE              175
#define IDC_COMBO_FACE                  1000
#define IDC_EDIT_HEIGHT                 1001
#define IDC_EDIT_WIDTH                  1002
#define IDC_EDIT_ESCAPE                 1007
#define IDC_EDIT_ORIENT                 1008
#define IDC_COMBO_WEIGHT                1009
#define IDC_CHECK_ITALIC                1010
#define IDC_CHECK_UNDER                 1011
#define IDC_CHECK_STRIKE                1012
#define IDC_COMBO_CHARSET               1013
#define IDC_COMBO_OUTP                  1014
#define IDC_CHECK_CLIPP_CHAR            1015
#define IDC_CHECK_CLIPP_STR             1016
#define IDC_COMBO_QUAL                  1017
#define IDC_COMBO_FAMILY                1020
#define IDC_COMBO_PITCH                 1021
#define IDC_CHECK_SUPP                  1026
#define IDC_EDIT_POINTS                 1027
#define IDC_EDIT_PIXELS                 1028
#define IDC_EDIT_DPI                    1029
#define ID_PROPERTIES                   32771
#define ID_PREVIEW                      32772
#define ID_HIDPI                        32773

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        132
#define _APS_NEXT_COMMAND_VALUE         32774
#define _APS_NEXT_CONTROL_VALUE         1030
#define _APS_NEXT_SYMED_VALUE           100
#endif
#endif
