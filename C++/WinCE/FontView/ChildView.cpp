#include "stdafx.h"
#include "FontView.h"
#include "ChildView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CFontClient

CFontClient::CFontClient()
{
	m_pPropServer = NULL;
}

void CFontClient::ConnectServer(CFontPropServer *pServer)
{
	m_pPropServer = pServer;
}

/////////////////////////////////////////////////////////////////////////////
// CFontPropServer

CFontPropServer::CFontPropServer()
{
	m_pClient = NULL;
}

void CFontPropServer::ConnectClient(CFontClient *pClient)
{
	m_pClient = pClient;
}

/////////////////////////////////////////////////////////////////////////////
// CFontHidpiServer

CFontHidpiServer::CFontHidpiServer()
{
	m_bInitialized = false;
	m_pSHGetUIMetrics = NULL;
	m_hAygshellDll = NULL;
}

CFontHidpiServer::~CFontHidpiServer()
{
	if (m_pSHGetUIMetrics != NULL)
		m_pSHGetUIMetrics = NULL;
	if (m_hAygshellDll != NULL)
	{
		::FreeLibrary(m_hAygshellDll);
		m_hAygshellDll = NULL;
	}
}

bool CFontHidpiServer::IsHidpiSupported()
{
	Initialize();
	return m_pSHGetUIMetrics != NULL;
}

void CFontHidpiServer::Initialize()
{
	if (!m_bInitialized)
	{
		m_hAygshellDll = ::LoadLibrary(_T("Aygshell.dll"));
		if (m_hAygshellDll != NULL)
		{
			m_pSHGetUIMetrics = (SHGetUIMetricsPtr)::GetProcAddress( 
				m_hAygshellDll, 
				_T("SHGetUIMetrics"));
		}
		m_bInitialized = true;
	}
}

int CFontHidpiServer::GetPointFontSize()
{
	Initialize();
	int nResult = 0;
		if (m_pSHGetUIMetrics != NULL)
			m_pSHGetUIMetrics(
				SHUIM_FONTSIZE_POINT,
				&nResult,
				sizeof(nResult),
				NULL);
	return nResult;
}

int CFontHidpiServer::GetPixelFontSize()
{
	Initialize();
	int nResult = 0;
		if (m_pSHGetUIMetrics != NULL)
			m_pSHGetUIMetrics(
				SHUIM_FONTSIZE_PIXEL,
				&nResult,
				sizeof(nResult),
				NULL);
	return nResult;
}

int CFontHidpiServer::GetDpi()
{
	HDC hdc = ::GetDC(NULL);
	INT ans = ::GetDeviceCaps(hdc, LOGPIXELSX);
	::ReleaseDC(NULL, hdc);
	return ans;
}

/////////////////////////////////////////////////////////////////////////////
// CChildView

CChildView::CChildView()
:	CFormView(-1)
{
	m_nID = -1;
}

CChildView::CChildView(int nID)
:	CFormView(nID)
{
	m_nID = nID;
}

int CChildView::Create(CWnd *pParent)
{
	return CFormView::Create(NULL, NULL, WS_CHILD | WS_VISIBLE,
		CRect(0, 0, 0, 0), pParent, m_nID, NULL);
}

/////////////////////////////////////////////////////////////////////////////
// CPropView

CPropView::CPropView()
:	CChildView(CPropView::IDD),
	m_LastHeight(CFontViewApp::s_pPropertySerializer, _T("Height"), 0),
	m_LastWidth(CFontViewApp::s_pPropertySerializer, _T("Width"), 0),
	m_LastEscape(CFontViewApp::s_pPropertySerializer, _T("Escape"), 0),
	m_LastOrient(CFontViewApp::s_pPropertySerializer, _T("Orient"), 0),
	m_LastFaceName(CFontViewApp::s_pPropertySerializer, _T("FaceName"), _T("")),
	m_LastWeight(CFontViewApp::s_pPropertySerializer, _T("Weight"), _T("")),
	m_LastCharset(CFontViewApp::s_pPropertySerializer, _T("Charset"), _T("")),
	m_LastOutPrec(CFontViewApp::s_pPropertySerializer, _T("OutPrec"), _T("")),
	m_LastQuality(CFontViewApp::s_pPropertySerializer, _T("Quality"), _T("")),
	m_LastFamily(CFontViewApp::s_pPropertySerializer, _T("Family"), _T("")),
	m_LastPitch(CFontViewApp::s_pPropertySerializer, _T("Pitch"), _T("")),
	m_LastItalic(CFontViewApp::s_pPropertySerializer, _T("Italic"), false),
	m_LastStrikeout(CFontViewApp::s_pPropertySerializer, _T("Strikeout"), false),
	m_LastUnderline(CFontViewApp::s_pPropertySerializer, _T("Underline"), false),
	m_LastClipChar(CFontViewApp::s_pPropertySerializer, _T("ClipChar"), false),
	m_LastClipStroke(CFontViewApp::s_pPropertySerializer, _T("ClipStroke"), false)
{
	//{{AFX_DATA_INIT(CPropView)
	m_nHeight = m_LastHeight.GetValue();
	m_nWidth = m_LastWidth.GetValue();
	m_nEscape = m_LastEscape.GetValue();
	m_nOrient = m_LastOrient.GetValue();
	m_bItalic = m_LastItalic.GetValue() ? TRUE : FALSE;
	m_bStrikeout = m_LastStrikeout.GetValue() ? TRUE : FALSE;
	m_bUnderline = m_LastUnderline.GetValue() ? TRUE : FALSE;
	m_bClipCharPrec = m_LastClipChar.GetValue() ? TRUE : FALSE;
	m_bClipStrPrec = m_LastClipStroke.GetValue() ? TRUE : FALSE;
	//}}AFX_DATA_INIT
}

void CPropView::DoDataExchange(CDataExchange* pDX)
{
	CChildView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPropView)
	DDX_Control(pDX, IDC_COMBO_PITCH, m_ctrlPitch);
	DDX_Control(pDX, IDC_COMBO_FAMILY, m_ctrlFamily);
	DDX_Control(pDX, IDC_COMBO_QUAL, m_ctrlQuality);
	DDX_Control(pDX, IDC_COMBO_OUTP, m_ctrlOutPrec);
	DDX_Control(pDX, IDC_COMBO_CHARSET, m_ctrlCharset);
	DDX_Control(pDX, IDC_COMBO_WEIGHT, m_ctrlWeight);
	DDX_Control(pDX, IDC_COMBO_FACE, m_ctrlFaceName);
	DDX_Text(pDX, IDC_EDIT_HEIGHT, m_nHeight);
	DDX_Text(pDX, IDC_EDIT_WIDTH, m_nWidth);
	DDX_Text(pDX, IDC_EDIT_ESCAPE, m_nEscape);
	DDX_Text(pDX, IDC_EDIT_ORIENT, m_nOrient);
	DDX_Check(pDX, IDC_CHECK_ITALIC, m_bItalic);
	DDX_Check(pDX, IDC_CHECK_STRIKE, m_bStrikeout);
	DDX_Check(pDX, IDC_CHECK_UNDER, m_bUnderline);
	DDX_Check(pDX, IDC_CHECK_CLIPP_CHAR, m_bClipCharPrec);
	DDX_Check(pDX, IDC_CHECK_CLIPP_STR, m_bClipStrPrec);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CPropView, CChildView)
	//{{AFX_MSG_MAP(CPropView)
	ON_EN_CHANGE(IDC_EDIT_HEIGHT, OnChangeEditHeight)
	ON_EN_CHANGE(IDC_EDIT_WIDTH, OnChangeEditWidth)
	ON_CBN_SELCHANGE(IDC_COMBO_FACE, OnSelchangeComboFace)
	ON_EN_CHANGE(IDC_EDIT_ESCAPE, OnChangeEditEscape)
	ON_EN_CHANGE(IDC_EDIT_ORIENT, OnChangeEditOrient)
	ON_CBN_SELCHANGE(IDC_COMBO_WEIGHT, OnSelchangeComboWeight)
	ON_BN_CLICKED(IDC_CHECK_ITALIC, OnCheckItalic)
	ON_BN_CLICKED(IDC_CHECK_UNDER, OnCheckUnder)
	ON_BN_CLICKED(IDC_CHECK_STRIKE, OnCheckStrike)
	ON_CBN_SELCHANGE(IDC_COMBO_CHARSET, OnSelchangeComboCharset)
	ON_CBN_SELCHANGE(IDC_COMBO_OUTP, OnSelchangeComboOutp)
	ON_BN_CLICKED(IDC_CHECK_CLIPP_CHAR, OnCheckClippChar)
	ON_BN_CLICKED(IDC_CHECK_CLIPP_STR, OnCheckClippStr)
	ON_CBN_SELCHANGE(IDC_COMBO_QUAL, OnSelchangeComboQual)
	ON_CBN_SELCHANGE(IDC_COMBO_FAMILY, OnSelchangeComboFamily)
	ON_CBN_SELCHANGE(IDC_COMBO_PITCH, OnSelchangeComboPitch)
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPropView message handlers

void CPropView::OnInitialUpdate()
{
	CChildView::OnInitialUpdate();

	::EnumFonts(
		GetWindowDC()->m_hDC,
		NULL,
		(FONTENUMPROC)EnumFontsProc,
		(LPARAM)this);

	FillMaps();
	FillCombos();

	m_ctrlFaceName.SelectString(-1, m_LastFaceName.GetValue());
	m_ctrlWeight.SelectString(-1, m_LastWeight.GetValue());
	m_ctrlCharset.SelectString(-1, m_LastCharset.GetValue());
	m_ctrlOutPrec.SelectString(-1, m_LastOutPrec.GetValue());
	m_ctrlQuality.SelectString(-1, m_LastQuality.GetValue());
	m_ctrlFamily.SelectString(-1, m_LastFamily.GetValue());
	m_ctrlPitch.SelectString(-1, m_LastPitch.GetValue());

	NotifyChange();
}

void CPropView::OnNextFont(
	LOGFONT *lplf,
	TEXTMETRIC *lptm,
	DWORD dwType)
{
	if (m_ctrlFaceName.FindString(-1, lplf->lfFaceName) == CB_ERR)
		m_ctrlFaceName.AddString(lplf->lfFaceName);
}

int CPropView::EnumFontsProc(
		LOGFONT *lplf,
		TEXTMETRIC *lptm,
		DWORD dwType,
		LPARAM lpData)
{
	CPropView *pInst = (CPropView *)lpData;
	pInst->OnNextFont(
		lplf,
		lptm,
		dwType);
	return 1;
}

CString CPropView::GetFaceName()
{
	CString sResult;
	m_ctrlFaceName.GetWindowText(sResult);
	return sResult;
}

void CPropView::NotifyChange()
{
	if (m_pClient != NULL)
		m_pClient->OnFontChange();
	BackupProperties();
}

void CPropView::OnChangeEditHeight()
{
	if (UpdateData() != FALSE)
		NotifyChange();
}

void CPropView::OnChangeEditWidth()
{
	if (UpdateData() != FALSE)
		NotifyChange();
}

void CPropView::OnSelchangeComboFace()
{
	NotifyChange();
}

int CPropView::GetHeight()
{
	return m_nHeight;
}

int CPropView::GetWidth()
{
	return m_nWidth;
}

void CPropView::OnChangeEditEscape()
{
	if (UpdateData() != FALSE)
		NotifyChange();
}

void CPropView::OnChangeEditOrient()
{
	if (UpdateData() != FALSE)
		NotifyChange();
}

int CPropView::GetEscapement()
{
	return m_nEscape;
}

int CPropView::GetOrientation()
{
	return m_nOrient;
}

int CPropView::GetWeight()
{
	CString s;
	m_ctrlWeight.GetWindowText(s);
	return WeightFromText(s);
}

int CPropView::WeightFromText(LPCTSTR szWeight)
{
	TStringIntMapIterator it = m_WeightMap.find(szWeight);
	if (it != m_WeightMap.end())
		return it->second;
	return FW_DONTCARE;
}

void CPropView::OnSelchangeComboWeight()
{
	NotifyChange();
}

bool CPropView::GetItalic()
{
	return m_bItalic == FALSE ? false : true;
}

bool CPropView::GetUnderline()
{
	return m_bUnderline == FALSE ? false : true;
}

bool CPropView::GetStrikeOut()
{
	return m_bStrikeout == FALSE ? false : true;
}

void CPropView::OnCheckItalic()
{
	UpdateData();
	NotifyChange();
}

void CPropView::OnCheckUnder()
{
	UpdateData();
	NotifyChange();
}

void CPropView::OnCheckStrike()
{
	UpdateData();
	NotifyChange();
}

int CPropView::GetCharset()
{
	CString s;
	m_ctrlCharset.GetWindowText(s);
	return CharsetFromText(s);
}

int CPropView::CharsetFromText(LPCTSTR szCharset)
{
	TStringIntMapIterator it = m_CharsetMap.find(szCharset);
	if (it != m_CharsetMap.end())
		return it->second;
	return DEFAULT_CHARSET;
}

void CPropView::OnSelchangeComboCharset()
{
	NotifyChange();
}

int CPropView::GetOutPrecision()
{
	CString s;
	m_ctrlOutPrec.GetWindowText(s);
	return OutPrecisionFromText(s);
}

int CPropView::OutPrecisionFromText(LPCTSTR szOutPrecision)
{
	TStringIntMapIterator it = m_OutPrecMap.find(szOutPrecision);
	if (it != m_OutPrecMap.end())
		return it->second;
	return OUT_DEFAULT_PRECIS;
}

void CPropView::OnSelchangeComboOutp()
{
	NotifyChange();
}

void CPropView::OnCheckClippChar()
{
	UpdateData();
	NotifyChange();
}

void CPropView::OnCheckClippStr()
{
	UpdateData();
	NotifyChange();
}

int CPropView::GetClipPrecision()
{
	int nResult = CLIP_DEFAULT_PRECIS;
	if (m_bClipCharPrec != FALSE)
		nResult |= CLIP_CHARACTER_PRECIS;
	if (m_bClipStrPrec != FALSE)
		nResult |= CLIP_STROKE_PRECIS;
	return nResult;
}

int CPropView::GetQuality()
{
	CString s;
	m_ctrlQuality.GetWindowText(s);
	return QualityFromText(s);
}

int CPropView::QualityFromText(LPCTSTR szQuality)
{
	TStringIntMapIterator it = m_QualityMap.find(szQuality);
	if (it != m_QualityMap.end())
		return it->second;
	return DEFAULT_QUALITY;
}

void CPropView::OnSelchangeComboQual()
{
	NotifyChange();
}

int CPropView::GetFamily()
{
	CString s;
	m_ctrlFamily.GetWindowText(s);
	return FamilyFromText(s);
}

int CPropView::FamilyFromText(LPCTSTR szFamily)
{
	TStringIntMapIterator it = m_FamilyMap.find(szFamily);
	if (it != m_FamilyMap.end())
		return it->second;
	return FF_DONTCARE;
}

void CPropView::OnSelchangeComboFamily()
{
	NotifyChange();
}

int CPropView::GetPitch()
{
	CString s;
	m_ctrlPitch.GetWindowText(s);
	return PitchFromText(s);
}

void CPropView::OnSelchangeComboPitch()
{
	NotifyChange();
}

int CPropView::PitchFromText(LPCTSTR szPitch)
{
	TStringIntMapIterator it = m_PitchMap.find(szPitch);
	if (it != m_PitchMap.end())
		return it->second;
	return DEFAULT_PITCH;
}

void CPropView::FillMaps()
{
	FillWeightMap();
	FillCharsetMap();
	FillOutPrecMap();
	FillQualityMap();
	FillFamilyMap();
	FillPitchMap();
}

void CPropView::FillWeightMap()
{
	m_WeightMap.insert(TStringIntMapValueType((LPCTSTR)IDS_WEIGHT_DONTCARE, FW_DONTCARE));
	m_WeightMap.insert(TStringIntMapValueType((LPCTSTR)IDS_WEIGHT_THIN, FW_THIN));
	m_WeightMap.insert(TStringIntMapValueType((LPCTSTR)IDS_WEIGHT_EXTRALIGHT, FW_EXTRALIGHT));
	m_WeightMap.insert(TStringIntMapValueType((LPCTSTR)IDS_WEIGHT_LIGHT, FW_LIGHT));
	m_WeightMap.insert(TStringIntMapValueType((LPCTSTR)IDS_WEIGHT_NORMAL, FW_NORMAL));
	m_WeightMap.insert(TStringIntMapValueType((LPCTSTR)IDS_WEIGHT_MEDIUM, FW_MEDIUM));
	m_WeightMap.insert(TStringIntMapValueType((LPCTSTR)IDS_WEIGHT_SEMIBOLD, FW_SEMIBOLD));
	m_WeightMap.insert(TStringIntMapValueType((LPCTSTR)IDS_WEIGHT_BOLD, FW_BOLD));
	m_WeightMap.insert(TStringIntMapValueType((LPCTSTR)IDS_WEIGHT_EXTRABOLD, FW_EXTRABOLD));
	m_WeightMap.insert(TStringIntMapValueType((LPCTSTR)IDS_WEIGHT_HEAVY, FW_HEAVY));
}

void CPropView::FillCharsetMap()
{
	m_CharsetMap.insert(TStringIntMapValueType((LPCTSTR)IDS_CHARSET_ANSI, ANSI_CHARSET));
	m_CharsetMap.insert(TStringIntMapValueType((LPCTSTR)IDS_CHARSET_BALTIC, BALTIC_CHARSET));
	m_CharsetMap.insert(TStringIntMapValueType((LPCTSTR)IDS_CHARSET_CHINESEBIG5, CHINESEBIG5_CHARSET));
	m_CharsetMap.insert(TStringIntMapValueType((LPCTSTR)IDS_CHARSET_DEFAULT, DEFAULT_CHARSET));
	m_CharsetMap.insert(TStringIntMapValueType((LPCTSTR)IDS_CHARSET_EASTEUROPE, EASTEUROPE_CHARSET));
	m_CharsetMap.insert(TStringIntMapValueType((LPCTSTR)IDS_CHARSET_GB2312, GB2312_CHARSET));
	m_CharsetMap.insert(TStringIntMapValueType((LPCTSTR)IDS_CHARSET_GREEK, GREEK_CHARSET));
	m_CharsetMap.insert(TStringIntMapValueType((LPCTSTR)IDS_CHARSET_HANGEUL, HANGEUL_CHARSET));
	m_CharsetMap.insert(TStringIntMapValueType((LPCTSTR)IDS_CHARSET_MAC, MAC_CHARSET));
	m_CharsetMap.insert(TStringIntMapValueType((LPCTSTR)IDS_CHARSET_OEM, OEM_CHARSET));
	m_CharsetMap.insert(TStringIntMapValueType((LPCTSTR)IDS_CHARSET_RUSSIAN, RUSSIAN_CHARSET));
	m_CharsetMap.insert(TStringIntMapValueType((LPCTSTR)IDS_CHARSET_SHIFTJIS, SHIFTJIS_CHARSET));
	m_CharsetMap.insert(TStringIntMapValueType((LPCTSTR)IDS_CHARSET_SYMBOL, SYMBOL_CHARSET));
	m_CharsetMap.insert(TStringIntMapValueType((LPCTSTR)IDS_CHARSET_TURKISH, TURKISH_CHARSET));
	m_CharsetMap.insert(TStringIntMapValueType((LPCTSTR)IDS_CHARSET_JOHAB, JOHAB_CHARSET));
	m_CharsetMap.insert(TStringIntMapValueType((LPCTSTR)IDS_CHARSET_HEBREW, HEBREW_CHARSET));
	m_CharsetMap.insert(TStringIntMapValueType((LPCTSTR)IDS_CHARSET_ARABIC, ARABIC_CHARSET));
	m_CharsetMap.insert(TStringIntMapValueType((LPCTSTR)IDS_CHARSET_THAI, THAI_CHARSET));
	m_CharsetMap.insert(TStringIntMapValueType((LPCTSTR)IDS_CHARSET_VIETNAMESE, VIETNAMESE_CHARSET));
}

void CPropView::FillOutPrecMap()
{
	m_OutPrecMap.insert(TStringIntMapValueType((LPCTSTR)IDS_PRECIS_DEFAULT, OUT_DEFAULT_PRECIS));
	m_OutPrecMap.insert(TStringIntMapValueType((LPCTSTR)IDS_PRECIS_RASTER, OUT_RASTER_PRECIS));
	m_OutPrecMap.insert(TStringIntMapValueType((LPCTSTR)IDS_PRECIS_STRING, OUT_STRING_PRECIS));
}

void CPropView::FillQualityMap()
{
	m_QualityMap.insert(TStringIntMapValueType((LPCTSTR)IDS_QUALITY_ANTIALIASED, ANTIALIASED_QUALITY));
	m_QualityMap.insert(TStringIntMapValueType((LPCTSTR)IDS_QUALITY_NONANTIALIASED, NONANTIALIASED_QUALITY));
	m_QualityMap.insert(TStringIntMapValueType((LPCTSTR)IDS_QUALITY_CLEARTYPE_COMPAT, CLEARTYPE_COMPAT_QUALITY));
	m_QualityMap.insert(TStringIntMapValueType((LPCTSTR)IDS_QUALITY_CLEARTYPE, CLEARTYPE_QUALITY));
	m_QualityMap.insert(TStringIntMapValueType((LPCTSTR)IDS_QUALITY_DEFAULT, DEFAULT_QUALITY));
	m_QualityMap.insert(TStringIntMapValueType((LPCTSTR)IDS_QUALITY_DRAFT, DRAFT_QUALITY));
}

void CPropView::FillFamilyMap()
{
	m_FamilyMap.insert(TStringIntMapValueType((LPCTSTR)IDS_FAMILY_DECORATIVE, FF_DECORATIVE));
	m_FamilyMap.insert(TStringIntMapValueType((LPCTSTR)IDS_FAMILY_DONTCARE, FF_DONTCARE));
	m_FamilyMap.insert(TStringIntMapValueType((LPCTSTR)IDS_FAMILY_MODERN, FF_MODERN));
	m_FamilyMap.insert(TStringIntMapValueType((LPCTSTR)IDS_FAMILY_ROMAN, FF_ROMAN));
	m_FamilyMap.insert(TStringIntMapValueType((LPCTSTR)IDS_FAMILY_SCRIPT, FF_SCRIPT));
	m_FamilyMap.insert(TStringIntMapValueType((LPCTSTR)IDS_FAMILY_SWISS, FF_SWISS));
}

void CPropView::FillPitchMap()
{
	m_PitchMap.insert(TStringIntMapValueType((LPCTSTR)IDS_PITCH_DEFAULT, DEFAULT_PITCH));
	m_PitchMap.insert(TStringIntMapValueType((LPCTSTR)IDS_PITCH_FIXED, FIXED_PITCH));
	m_PitchMap.insert(TStringIntMapValueType((LPCTSTR)IDS_PITCH_VARIABLE, VARIABLE_PITCH));
}

void CPropView::FillCombos()
{
	FillWeightCombo();
	FillCharsetCombo();
	FillOutPrecCombo();
	FillQualityCombo();
	FillFamilyCombo();
	FillPitchCombo();
}

void CPropView::FillWeightCombo()
{
	TStringIntMapIterator it = m_WeightMap.begin();
	while (it != m_WeightMap.end())
	{
		m_ctrlWeight.AddString(it->first);
		it++;
	}
}

void CPropView::FillCharsetCombo()
{
	TStringIntMapIterator it = m_CharsetMap.begin();
	while (it != m_CharsetMap.end())
	{
		m_ctrlCharset.AddString(it->first);
		it++;
	}
}

void CPropView::FillOutPrecCombo()
{
	TStringIntMapIterator it = m_OutPrecMap.begin();
	while (it != m_OutPrecMap.end())
	{
		m_ctrlOutPrec.AddString(it->first);
		it++;
	}
}

void CPropView::FillQualityCombo()
{
	TStringIntMapIterator it = m_QualityMap.begin();
	while (it != m_QualityMap.end())
	{
		m_ctrlQuality.AddString(it->first);
		it++;
	}
}

void CPropView::FillFamilyCombo()
{
	TStringIntMapIterator it = m_FamilyMap.begin();
	while (it != m_FamilyMap.end())
	{
		m_ctrlFamily.AddString(it->first);
		it++;
	}
}

void CPropView::FillPitchCombo()
{
	TStringIntMapIterator it = m_PitchMap.begin();
	while (it != m_PitchMap.end())
	{
		m_ctrlPitch.AddString(it->first);
		it++;
	}
}

void CPropView::BackupProperties()
{
	m_LastHeight.SetValue(m_nHeight);
	m_LastWidth.SetValue(m_nWidth);
	m_LastEscape.SetValue(m_nEscape);
	m_LastOrient.SetValue(m_nOrient);
	CString s;
	m_ctrlFaceName.GetWindowText(s);
	m_LastFaceName.SetValue(s);
	m_ctrlWeight.GetWindowText(s);
	m_LastWeight.SetValue(s);
	m_ctrlCharset.GetWindowText(s);
	m_LastCharset.SetValue(s);
	m_ctrlOutPrec.GetWindowText(s);
	m_LastOutPrec.SetValue(s);
	m_ctrlQuality.GetWindowText(s);
	m_LastQuality.SetValue(s);
	m_ctrlFamily.GetWindowText(s);
	m_LastFamily.SetValue(s);
	m_ctrlPitch.GetWindowText(s);
	m_LastPitch.SetValue(s);
	m_LastItalic.SetValue(m_bItalic != FALSE);
	m_LastStrikeout.SetValue(m_bStrikeout != FALSE);
	m_LastUnderline.SetValue(m_bUnderline != FALSE);
	m_LastClipChar.SetValue(m_bClipCharPrec != FALSE);
	m_LastClipStroke.SetValue(m_bClipStrPrec != FALSE);
}

void CPropView::OnDestroy()
{
	BackupProperties();

	CChildView::OnDestroy();
}

/////////////////////////////////////////////////////////////////////////////
// CPrevwView

BEGIN_MESSAGE_MAP(CPrevwView, CEditView)
	//{{AFX_MSG_MAP(CPrevwView)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPrevwView message handlers

void CPrevwView::OnFontChange()
{
	LOGFONT lf;
	ZeroMemory(&lf, sizeof(lf));
	_tcscpy(lf.lfFaceName, m_pPropServer->GetFaceName());
	lf.lfHeight = m_pPropServer->GetHeight();
	lf.lfWidth = m_pPropServer->GetWidth();
	lf.lfEscapement = m_pPropServer->GetEscapement();
	lf.lfOrientation = m_pPropServer->GetOrientation();
	lf.lfWeight = m_pPropServer->GetWeight();
	lf.lfItalic = m_pPropServer->GetItalic() ? TRUE : FALSE;
	lf.lfUnderline = m_pPropServer->GetUnderline() ? TRUE : FALSE;
	lf.lfStrikeOut = m_pPropServer->GetStrikeOut() ? TRUE : FALSE;
	lf.lfCharSet = m_pPropServer->GetCharset();
	lf.lfOutPrecision = m_pPropServer->GetOutPrecision();
	lf.lfClipPrecision = m_pPropServer->GetClipPrecision();
	lf.lfQuality = m_pPropServer->GetQuality();
	lf.lfPitchAndFamily = m_pPropServer->GetPitch() | m_pPropServer->GetFamily();
	m_Font.DeleteObject();
	m_Font.CreateFontIndirect(&lf);
	SetFont(&m_Font);
}

void CPrevwView::OnInitialUpdate()
{
	CEditView::OnInitialUpdate();

	SetWindowText(
		_T("0123456789\r\n")
		_T("abcdefghijklmnopqrstuvwxyz\r\n")
		_T("ABCDEFGHIJKLMNOPQRSTUVWXYZ\r\n")
		_T("���������������������\r\n")
		_T("�������ż�����������ݎ"));
}

int CPrevwView::Create(CWnd *pParent)
{
	return CEditView::Create(NULL, NULL, WS_CHILD | WS_VISIBLE,
		CRect(0, 0, 0, 0), pParent, CPrevwView::IDD);
}

/////////////////////////////////////////////////////////////////////////////

std::_Lockit::_Lockit()
{
}

std::_Lockit::~_Lockit()
{
}

/////////////////////////////////////////////////////////////////////////////
// CHidpiView

CHidpiView::CHidpiView()
:	CChildView(CHidpiView::IDD)
{
	//{{AFX_DATA_INIT(CHidpiView)
	m_bHidpi = FALSE;
	m_nDpi = 0;
	m_sPixels = _T("");
	m_sPoints = _T("");
	//}}AFX_DATA_INIT
}

void CHidpiView::DoDataExchange(CDataExchange* pDX)
{
	CChildView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CHidpiView)
	DDX_Check(pDX, IDC_CHECK_SUPP, m_bHidpi);
	DDX_Text(pDX, IDC_EDIT_DPI, m_nDpi);
	DDX_Text(pDX, IDC_EDIT_PIXELS, m_sPixels);
	DDX_Text(pDX, IDC_EDIT_POINTS, m_sPoints);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CHidpiView, CChildView)
	//{{AFX_MSG_MAP(CHidpiView)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CHidpiView message handlers

void CHidpiView::OnInitialUpdate() 
{
	CChildView::OnInitialUpdate();

	bool bHidpi = IsHidpiSupported();
	m_bHidpi = bHidpi ? TRUE : FALSE;
	if (bHidpi)
	{
		m_sPixels.Format(_T("%d"), GetPixelFontSize());
		m_sPoints.Format(_T("%d"), GetPointFontSize());
	}
	m_nDpi = GetDpi();

	UpdateData(FALSE);
}
