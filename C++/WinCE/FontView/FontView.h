#if !defined(AFX_FONTVIEW_H__E96F0550_DF9B_4470_92A6_8CEEF92D141B__INCLUDED_)
#define AFX_FONTVIEW_H__E96F0550_DF9B_4470_92A6_8CEEF92D141B__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols
#include "Property.h"

/////////////////////////////////////////////////////////////////////////////
// CFontViewApp:
//

class CFontViewApp : public CWinApp
{
public:
	static CRegPropertySerializer *s_pPropertySerializer;

	//{{AFX_VIRTUAL(CFontViewApp)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	//}}AFX_VIRTUAL

	//{{AFX_MSG(CFontViewApp)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft eMbedded Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FONTVIEW_H__E96F0550_DF9B_4470_92A6_8CEEF92D141B__INCLUDED_)
