#include "stdafx.h"
#include "FontView.h"
#include "MainFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CFontViewApp

BEGIN_MESSAGE_MAP(CFontViewApp, CWinApp)
	//{{AFX_MSG_MAP(CFontViewApp)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// The one and only CFontViewApp object

CFontViewApp theApp;
CRegPropertySerializer *CFontViewApp::s_pPropertySerializer = NULL;

/////////////////////////////////////////////////////////////////////////////
// CFontViewApp initialization

BOOL CFontViewApp::InitInstance()
{
	s_pPropertySerializer = new CRegPropertySerializer(_T("HKEY_LOCAL_MACHINE\\Software\\Kamen\\FontView"));
	CMainFrame* pFrame = new CMainFrame;
	m_pMainWnd = pFrame;
	pFrame->LoadFrame(IDR_MAINFRAME, WS_OVERLAPPEDWINDOW);
	pFrame->ShowWindow(m_nCmdShow);
	pFrame->UpdateWindow();

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CFontViewApp message handlers

int CFontViewApp::ExitInstance()
{
	delete s_pPropertySerializer;

	return CWinApp::ExitInstance();
}
