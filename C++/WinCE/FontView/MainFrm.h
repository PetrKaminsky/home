#if !defined(AFX_MAINFRM_H__00EBC5DE_2C45_4168_B55B_0DF29DFB59D3__INCLUDED_)
#define AFX_MAINFRM_H__00EBC5DE_2C45_4168_B55B_0DF29DFB59D3__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "ChildView.h"

/////////////////////////////////////////////////////////////////////////////

class CMainFrame : public CFrameWnd
{
public:
	CMainFrame();

protected: 
	DECLARE_DYNAMIC(CMainFrame)

	virtual ~CMainFrame();

public:
	//{{AFX_VIRTUAL(CMainFrame)
	public:
	virtual BOOL OnCmdMsg(UINT nID, int nCode, void* pExtra, AFX_CMDHANDLERINFO* pHandlerInfo);
	//}}AFX_VIRTUAL

protected:  // control bar embedded members
	CCeCommandBar m_wndCommandBar;
	CPropView *m_pPropView;
	CPrevwView *m_pPrevwView;
	CHidpiView *m_pHidpiView;
	CView *m_pActiveView;

	void ActivateView(CView *pView);

	//{{AFX_MSG(CMainFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnProperties();
	afx_msg void OnUpdateProperties(CCmdUI* pCmdUI);
	afx_msg void OnPreview();
	afx_msg void OnUpdatePreview(CCmdUI* pCmdUI);
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	afx_msg void OnHidpi();
	afx_msg void OnUpdateHidpi(CCmdUI* pCmdUI);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	void DestroyView(CView *&pView);
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft eMbedded Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MAINFRM_H__00EBC5DE_2C45_4168_B55B_0DF29DFB59D3__INCLUDED_)
