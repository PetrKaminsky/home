#if !defined(AFX_STDAFX_H__5959C53B_0953_451D_9E35_0420A4A892A1__INCLUDED_)
#define AFX_STDAFX_H__5959C53B_0953_451D_9E35_0420A4A892A1__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#if (_WIN32_WCE <= 200)
#error : This project does not support MFCCE 2.00 or earlier, because it requires CControlBar, available only in MFCCE 2.01 or later
#endif

#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers

#pragma warning ( disable : 4786 4291 )

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions

#if defined(_WIN32_WCE) && (_WIN32_WCE >= 211) && (_AFXDLL)
#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#endif

#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT

//{{AFX_INSERT_LOCATION}}
// Microsoft eMbedded Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__5959C53B_0953_451D_9E35_0420A4A892A1__INCLUDED_)
