#if !defined(AFX_CHILDVIEW_H__246FD1BA_D8B5_4C3A_9259_480EDA0C0E8A__INCLUDED_)
#define AFX_CHILDVIEW_H__246FD1BA_D8B5_4C3A_9259_480EDA0C0E8A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <map>

class CFontPropServer;

/////////////////////////////////////////////////////////////////////////////
// CFontClient Interface

class CFontClient
{
public:
	CFontClient();

	void ConnectServer(CFontPropServer *pServer);
	virtual void OnFontChange() = 0;

protected:
	CFontPropServer *m_pPropServer;
};

/////////////////////////////////////////////////////////////////////////////
// CFontPropServer Interface

class CFontPropServer
{
public:
	CFontPropServer();

	void ConnectClient(CFontClient *pClient);
	virtual CString GetFaceName() = 0;
	virtual int GetHeight() = 0;
	virtual int GetWidth() = 0;
	virtual int GetEscapement() = 0;
	virtual int GetOrientation() = 0;
	virtual int GetWeight() = 0;
	virtual bool GetItalic() = 0;
	virtual bool GetUnderline() = 0;
	virtual bool GetStrikeOut() = 0;
	virtual int GetCharset() = 0;
	virtual int GetOutPrecision() = 0;
	virtual int GetClipPrecision() = 0;
	virtual int GetQuality() = 0;
	virtual int GetPitch() = 0;
	virtual int GetFamily() = 0;

protected:
	CFontClient *m_pClient;
};

/////////////////////////////////////////////////////////////////////////////
// CFontHidpiServer Interface

#ifndef SH_UIMETRIC_CHANGE
typedef enum
{
	SHUIM_INVALID = 0,
	SHUIM_FONTSIZE_POINT,
	SHUIM_FONTSIZE_PIXEL,
	SHUIM_FONTSIZE_PERCENTAGE,
} SHUIMETRIC;
#endif

typedef HRESULT (*SHGetUIMetricsPtr)(
	SHUIMETRIC shuim,
	PVOID pvBuffer,
	DWORD cbBufferSize,
	DWORD *pcbRequired);

class CFontHidpiServer
{
public:
	CFontHidpiServer();
	virtual ~CFontHidpiServer();

	bool IsHidpiSupported();
	int GetPointFontSize();
	int GetPixelFontSize();
	int GetDpi();

private:
	bool m_bInitialized;
	SHGetUIMetricsPtr m_pSHGetUIMetrics;
	HMODULE m_hAygshellDll;

	void Initialize();
};

/////////////////////////////////////////////////////////////////////////////
// CChildView form view

class CChildView : public CFormView
{
public:
	CChildView();
	CChildView(int nID);

	int Create(CWnd *pParent);

private:
	int m_nID;
};

/////////////////////////////////////////////////////////////////////////////
// CPropView form view

using std::map;

typedef map<CString, int> TStringIntMap;
typedef TStringIntMap::iterator TStringIntMapIterator;
typedef TStringIntMap::value_type TStringIntMapValueType;

class CPropView
:	public CChildView,
	public CFontPropServer
{
public:
	CPropView();

	void OnNextFont(
		LOGFONT *lplf,
		TEXTMETRIC *lptm,
		DWORD dwType);

	//{{AFX_DATA(CPropView)
	enum { IDD = IDD_FORM_PROP };
	CComboBox	m_ctrlPitch;
	CComboBox	m_ctrlFamily;
	CComboBox	m_ctrlQuality;
	CComboBox	m_ctrlOutPrec;
	CComboBox	m_ctrlCharset;
	CComboBox	m_ctrlWeight;
	CComboBox	m_ctrlFaceName;
	int		m_nHeight;
	int		m_nWidth;
	int		m_nEscape;
	int		m_nOrient;
	BOOL	m_bItalic;
	BOOL	m_bStrikeout;
	BOOL	m_bUnderline;
	BOOL	m_bClipCharPrec;
	BOOL	m_bClipStrPrec;
	//}}AFX_DATA

	//{{AFX_VIRTUAL(CPropView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

protected:
	//{{AFX_MSG(CPropView)
	afx_msg void OnChangeEditHeight();
	afx_msg void OnChangeEditWidth();
	afx_msg void OnSelchangeComboFace();
	afx_msg void OnChangeEditEscape();
	afx_msg void OnChangeEditOrient();
	afx_msg void OnSelchangeComboWeight();
	afx_msg void OnCheckItalic();
	afx_msg void OnCheckUnder();
	afx_msg void OnCheckStrike();
	afx_msg void OnSelchangeComboCharset();
	afx_msg void OnSelchangeComboOutp();
	afx_msg void OnCheckClippChar();
	afx_msg void OnCheckClippStr();
	afx_msg void OnSelchangeComboQual();
	afx_msg void OnSelchangeComboFamily();
	afx_msg void OnSelchangeComboPitch();
	afx_msg void OnDestroy();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	TStringIntMap m_WeightMap;
	TStringIntMap m_CharsetMap;
	TStringIntMap m_OutPrecMap;
	TStringIntMap m_QualityMap;
	TStringIntMap m_FamilyMap;
	TStringIntMap m_PitchMap;
	CRegPropertyInt m_LastHeight;
	CRegPropertyInt m_LastWidth;
	CRegPropertyInt m_LastEscape;
	CRegPropertyInt m_LastOrient;
	CRegPropertyString m_LastFaceName;
	CRegPropertyString m_LastWeight;
	CRegPropertyString m_LastCharset;
	CRegPropertyString m_LastOutPrec;
	CRegPropertyString m_LastQuality;
	CRegPropertyString m_LastFamily;
	CRegPropertyString m_LastPitch;
	CRegPropertyBool m_LastItalic;
	CRegPropertyBool m_LastStrikeout;
	CRegPropertyBool m_LastUnderline;
	CRegPropertyBool m_LastClipChar;
	CRegPropertyBool m_LastClipStroke;

	static int CALLBACK EnumFontsProc(
		LOGFONT *lplf,
		TEXTMETRIC *lptm,
		DWORD dwType,
		LPARAM lpData);
	CString GetFaceName();
	int GetHeight();
	int GetWidth();
	int GetEscapement();
	int GetOrientation();
	int GetWeight();
	bool GetItalic();
	bool GetUnderline();
	bool GetStrikeOut();
	int GetCharset();
	int GetOutPrecision();
	int GetClipPrecision();
	int GetQuality();
	int GetPitch();
	int GetFamily();
	void NotifyChange();
	int WeightFromText(LPCTSTR szWeight);
	int CharsetFromText(LPCTSTR szCharset);
	int OutPrecisionFromText(LPCTSTR szCharset);
	int QualityFromText(LPCTSTR szQuality);
	int FamilyFromText(LPCTSTR szFamily);
	int PitchFromText(LPCTSTR szPitch);
	void FillMaps();
	void FillWeightMap();
	void FillCharsetMap();
	void FillOutPrecMap();
	void FillQualityMap();
	void FillFamilyMap();
	void FillPitchMap();
	void FillCombos();
	void FillWeightCombo();
	void FillCharsetCombo();
	void FillOutPrecCombo();
	void FillQualityCombo();
	void FillFamilyCombo();
	void FillPitchCombo();
	void BackupProperties();
};

/////////////////////////////////////////////////////////////////////////////
// CPrevwView view

class CPrevwView
:	public CEditView,
	public CFontClient
{
public:
	int Create(CWnd *pParent);
	enum { IDD = IDD_FORM_PREVIEW };

	//{{AFX_VIRTUAL(CPrevwView)
	public:
	virtual void OnInitialUpdate();
	//}}AFX_VIRTUAL

protected:
	//{{AFX_MSG(CPrevwView)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	void OnFontChange();
	CFont m_Font;
};

/////////////////////////////////////////////////////////////////////////////
// CHidpiView form view

class CHidpiView
:	public CChildView,
	public CFontHidpiServer
{
public:
	CHidpiView();

	//{{AFX_DATA(CHidpiView)
	enum { IDD = IDD_FORM_HIDPI };
	BOOL	m_bHidpi;
	int		m_nDpi;
	CString	m_sPixels;
	CString	m_sPoints;
	//}}AFX_DATA

public:
	//{{AFX_VIRTUAL(CHidpiView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

	//{{AFX_MSG(CHidpiView)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////
//{{AFX_INSERT_LOCATION}}
// Microsoft eMbedded Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CHILDVIEW_H__246FD1BA_D8B5_4C3A_9259_480EDA0C0E8A__INCLUDED_)
