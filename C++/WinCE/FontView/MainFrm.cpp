#include "stdafx.h"
#include "FontView.h"
#include "MainFrm.h"
#include "ChildView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMainFrame

IMPLEMENT_DYNAMIC(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	//{{AFX_MSG_MAP(CMainFrame)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_COMMAND(ID_PROPERTIES, OnProperties)
	ON_UPDATE_COMMAND_UI(ID_PROPERTIES, OnUpdateProperties)
	ON_COMMAND(ID_PREVIEW, OnPreview)
	ON_UPDATE_COMMAND_UI(ID_PREVIEW, OnUpdatePreview)
	ON_WM_SETFOCUS()
	ON_COMMAND(ID_HIDPI, OnHidpi)
	ON_UPDATE_COMMAND_UI(ID_HIDPI, OnUpdateHidpi)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMainFrame construction/destruction

CMainFrame::CMainFrame()
{
	m_pPropView = NULL;
	m_pPrevwView = NULL;
	m_pHidpiView = NULL;

	m_pActiveView = NULL;
}

CMainFrame::~CMainFrame()
{
	DestroyView((CView *&)m_pPropView);
	DestroyView((CView *&)m_pPrevwView);
	DestroyView((CView *&)m_pHidpiView);
}

void CMainFrame::DestroyView(CView *&pView)
{
	if (pView != NULL)
	{
		pView->DestroyWindow();
		delete pView;
		pView = NULL;
	}
}

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	m_pPropView = new CPropView;
	m_pPropView->Create(this);
	m_pPrevwView = new CPrevwView;
	m_pPrevwView->Create(this);
	m_pHidpiView = new CHidpiView;
	m_pHidpiView->Create(this);

	if (!m_wndCommandBar.Create(this) ||
		!m_wndCommandBar.InsertMenuBar(IDR_MAINFRAME))
	{
		TRACE0("Failed to create CommandBar\n");
		return -1;      // fail to create
	}

	m_wndCommandBar.SetBarStyle(m_wndCommandBar.GetBarStyle() |
		CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_FIXED);

	CFontPropServer *pServer = (CFontPropServer *)m_pPropView;
	CFontClient *pClient = (CFontClient *)m_pPrevwView;

	pServer->ConnectClient(pClient);
	pClient->ConnectServer(pServer);

	ActivateView(m_pPropView);

	return 0;
}

/////////////////////////////////////////////////////////////////////////////
// CMainFrame message handlers

void CMainFrame::OnSize(UINT nType, int cx, int cy)
{
	CFrameWnd::OnSize(nType, cx, cy);

// @@@@ PocketPC
	CRect r;
	GetClientRect(&r);
	if (m_pActiveView != NULL)
		m_pActiveView->MoveWindow(&r);
// @@@@ PocketPC
// @@@@ CE.NET
// @@@@ CE.NET
}

void CMainFrame::ActivateView(CView *pView)
{
	SetActiveView(pView);
// @@@@ PocketPC
	CRect r;
	GetClientRect(&r);
// @@@@ PocketPC
// @@@@ CE.NET
//	CRect rc;
//	GetClientRect(&rc);
//	CRect rcBar;
//	m_wndCommandBar.GetWindowRect(rcBar);
//	CRect r(rc.left, rcBar.Height(), rc.right, rc.bottom);
// @@@@ CE.NET
	pView->MoveWindow(&r);
	if (m_pActiveView != NULL && m_pActiveView != pView)
	{
		CRect r0;
		r0.SetRectEmpty();
		m_pActiveView->MoveWindow(&r0);
	}
	m_pActiveView = pView;
}

void CMainFrame::OnProperties()
{
	ActivateView(m_pPropView);
}

void CMainFrame::OnUpdateProperties(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(TRUE);
	pCmdUI->SetCheck(m_pActiveView == m_pPropView ? 1 : 0);
}

void CMainFrame::OnPreview()
{
	ActivateView(m_pPrevwView);
}

void CMainFrame::OnUpdatePreview(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(TRUE);
	pCmdUI->SetCheck(m_pActiveView == m_pPrevwView ? 1 : 0);
}

void CMainFrame::OnSetFocus(CWnd* pOldWnd)
{
	m_pActiveView->SetFocus();
}

BOOL CMainFrame::OnCmdMsg(UINT nID, int nCode, void* pExtra, AFX_CMDHANDLERINFO* pHandlerInfo)
{
	if (m_pActiveView->OnCmdMsg(nID, nCode, pExtra, pHandlerInfo))
		return TRUE;
	return CFrameWnd::OnCmdMsg(nID, nCode, pExtra, pHandlerInfo);
}

void CMainFrame::OnHidpi()
{
	ActivateView(m_pHidpiView);
}

void CMainFrame::OnUpdateHidpi(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(TRUE);
	pCmdUI->SetCheck(m_pActiveView == m_pHidpiView ? 1 : 0);
}
