#if !defined(AFX_CHILDVIEW_H__99335461_6CC1_4678_BC6B_720D64EEB8E8__INCLUDED_)
#define AFX_CHILDVIEW_H__99335461_6CC1_4678_BC6B_720D64EEB8E8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CSimView form view

class CSimView : public CFormView
{
public:
	CSimView();
	virtual ~CSimView();

	int Create(CWnd *pParent);
	void ResizeControl(CWnd *pCtrl, int nWidth);
	void ResizeControl(CWnd *pCtrl, int nWidth, int nHeight);

public:
	//{{AFX_DATA(CSimView)
	enum { IDD = IDD_DIALOG_SIM };
	CListCtrl	m_ctrlMsg;
	CEdit	m_ctrlPin;
	//}}AFX_DATA

	//{{AFX_VIRTUAL(CSimView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

protected:
	//{{AFX_MSG(CSimView)
	afx_msg void OnButtonSet();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnDestroy();
	//}}AFX_MSG
	afx_msg LRESULT OnSimMessage(WPARAM wParam, LPARAM lParam);
	DECLARE_MESSAGE_MAP()

private:
	HSIM m_hSim;
	CRegPropertyInt m_Column0Width;
	CRegPropertyString m_LastPin;

	static void SimCallback(
		DWORD dwNotifyCode,                     // @parm Indicates type of notification received
		const void *pData,                      // @parm Points to data structure specific to the notification
		DWORD dwDataSize,                       // @parm Size of data structure in bytes
		DWORD dwParam							// @parm Parameter passed to simInititialize
		);
	void AppendMsg(LPCTSTR szMessage);
	void AppendMsg(UINT nMessageID);
	static CString GetSimErrorText(HRESULT hr);
	static CString GetPhoneLockText(DWORD dwState);
	static CString GetStandardErrorText(HRESULT hr);
	static void PrintError(LPCTSTR szMessage);
	void BackupProperties();
	static CString GetWindowText(CWnd *pWnd);
};

/////////////////////////////////////////////////////////////////////////////
//{{AFX_INSERT_LOCATION}}
// Microsoft eMbedded Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CHILDVIEW_H__99335461_6CC1_4678_BC6B_720D64EEB8E8__INCLUDED_)
