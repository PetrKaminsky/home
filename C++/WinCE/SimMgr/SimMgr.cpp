#include "stdafx.h"
#include "SimMgr.h"
#include "MainFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSimMgrApp

BEGIN_MESSAGE_MAP(CSimMgrApp, CWinApp)
	//{{AFX_MSG_MAP(CSimMgrApp)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// The one and only CSimMgrApp object

CSimMgrApp theApp;
CRegPropertySerializer *CSimMgrApp::s_pPropertySerializer = NULL;

/////////////////////////////////////////////////////////////////////////////
// CSimMgrApp initialization

BOOL CSimMgrApp::InitInstance()
{
	s_pPropertySerializer = new CRegPropertySerializer(_T("HKEY_LOCAL_MACHINE\\Software\\Kamen\\SimMgr"));
	CMainFrame* pFrame = new CMainFrame;
	m_pMainWnd = pFrame;
	pFrame->LoadFrame(IDR_MAINFRAME, WS_OVERLAPPEDWINDOW);
	pFrame->ShowWindow(m_nCmdShow);
	pFrame->UpdateWindow();

	return TRUE;
}

int CSimMgrApp::ExitInstance()
{
	delete s_pPropertySerializer;

	return CWinApp::ExitInstance();
}
