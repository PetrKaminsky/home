#include "stdafx.h"
#include "SimMgr.h"
#include "ChildView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define WM_SIM (WM_USER + 1)

/////////////////////////////////////////////////////////////////////////////
// CSimView

CSimView::CSimView()
:	CFormView(CSimView::IDD),
	m_Column0Width(CSimMgrApp::s_pPropertySerializer, _T("Column0"), 1000),
	m_LastPin(CSimMgrApp::s_pPropertySerializer, _T("Pin"), _T(""))
{
	//{{AFX_DATA_INIT(CSimView)
	//}}AFX_DATA_INIT

	m_hSim = NULL;
}

CSimView::~CSimView()
{
	if (m_hSim != NULL)
	{
		HRESULT hr = ::SimDeinitialize(m_hSim);
		if (hr != S_OK)
			PrintError(_T("SimInitialize: ") + GetSimErrorText(hr));
		m_hSim = NULL;
	}
}

void CSimView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSimView)
	DDX_Control(pDX, IDC_LIST_MSG, m_ctrlMsg);
	DDX_Control(pDX, IDC_EDIT_PIN, m_ctrlPin);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CSimView, CFormView)
	//{{AFX_MSG_MAP(CSimView)
	ON_BN_CLICKED(IDC_BUTTON_SET, OnButtonSet)
	ON_WM_SIZE()
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
	ON_MESSAGE(WM_SIM, OnSimMessage)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSimView message handlers

int CSimView::Create(CWnd *pParent)
{
	return CFormView::Create(NULL, NULL, WS_CHILD | WS_VISIBLE,
		CRect(0, 0, 0, 0), pParent, AFX_IDW_PANE_FIRST, NULL);
}

void CSimView::ResizeControl(CWnd *pCtrl, int nWidth)
{
	CRect r;
	pCtrl->GetWindowRect(&r);
	ScreenToClient(&r);
	r.right = nWidth;
	pCtrl->MoveWindow(&r);
}

void CSimView::ResizeControl(CWnd *pCtrl, int nWidth, int nHeight)
{
	CRect r;
	pCtrl->GetWindowRect(&r);
	ScreenToClient(&r);
	r.right = nWidth;
	r.bottom = nHeight;
	pCtrl->MoveWindow(&r);
}

void CSimView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();

	m_ctrlMsg.InsertColumn(0, CString((LPCTSTR)IDS_COLUMN0), LVCFMT_LEFT, m_Column0Width.GetValue());
	m_ctrlMsg.SetExtendedStyle(m_ctrlMsg.GetExtendedStyle() | LVS_EX_FULLROWSELECT | LVS_EX_HEADERDRAGDROP);
	m_ctrlPin.SetWindowText(m_LastPin.GetValue());

	HRESULT hr = ::SimInitialize(
		SIM_INIT_SIMCARD_NOTIFICATIONS,
		CSimView::SimCallback,
		(DWORD)this,
		&m_hSim);
	if (hr != S_OK)
		PrintError(_T("SimInitialize: ") + GetSimErrorText(hr));
}

LRESULT CSimView::OnSimMessage(WPARAM wParam, LPARAM lParam)
{
	CString s;
	DWORD dwOn;

	AppendMsg(IDS_SIM_MESSAGE);
	switch (wParam)
	{
	case SIM_NOTIFY_CARD_REMOVED:	// SIM card was removed; lpData is NULL
		AppendMsg(IDS_SIM_NOTIFY_CARD_REMOVED);
		break;

	case SIM_NOTIFY_FILE_REFRESH:	// Files on the SIM were refreshed; lpData points to a SIMFILEREFRESH structure
		AppendMsg(IDS_SIM_NOTIFY_FILE_REFRESH);
		break;

	case SIM_NOTIFY_MSG_STORED:		// A message was stored to the SIM; lpData points to a SIMMESSAGECHANGE structure
		AppendMsg(IDS_SIM_NOTIFY_MSG_STORED);
		break;

	case SIM_NOTIFY_MSG_DELETED:	// A message was removed from the SIM; lpData points to a SIMMESSAGECHANGE structure
		AppendMsg(IDS_SIM_NOTIFY_MSG_DELETED);
		break;

	case SIM_NOTIFY_PBE_STORED:		// A phone book entry was stored to the SIM; lpData points to a SIMPBECHANGE structure
		AppendMsg(IDS_SIM_NOTIFY_PBE_STORED);
		break;

	case SIM_NOTIFY_PBE_DELETED:	// A phone book entry was removed from the SIM; lpData points to a SIMPBECHANGE structure
		AppendMsg(IDS_SIM_NOTIFY_PBE_DELETED);
		break;

	case SIM_NOTIFY_MSG_RECEIVED:	// Class 2 SMS was sent directly to the SIM; lpData points to a SIMMESSAGECHANGE structure
		AppendMsg(IDS_SIM_NOTIFY_MSG_RECEIVED);
		break;

	case SIM_NOTIFY_RADIOOFF:		// The Radio has been turned off but AT interpreter is still on; lpData is NULL 
		AppendMsg(IDS_SIM_NOTIFY_RADIOOFF);
		break;

	case SIM_NOTIFY_RADIOON:		// The Radio is present and is now on; lpData is NULL 
		AppendMsg(CString((LPCTSTR)IDS_SIM_NOTIFY_RADIOON));
		OnButtonSet();
		break;

	case SIM_NOTIFY_RADIOPRESENT:	// A Radio Module/Driver has been installed; lpData is points to a DWORD which is 0 if the radio is OFF and 1 if the radio is ON
		dwOn = *((DWORD *)lParam);
		s.Format(CString((LPCTSTR)IDS_SIM_NOTIFY_RADIOPRESENT), dwOn == 1 ? CString((LPCTSTR)IDS_RADIO_ON) : CString((LPCTSTR)IDS_RADIO_OFF)), 
		AppendMsg(s);
		if (dwOn == 1)
			OnButtonSet();
		break;

	case SIM_NOTIFY_RADIOREMOVED:	// A Radio Module/Driver has been removed; lpData is NULL
		AppendMsg(IDS_SIM_NOTIFY_RADIOREMOVED);
		break;

	default:
		s.Format(IDS_SIM_NOTIFY_DEFAULT, wParam);
		AppendMsg(s);
	}

	return 0;
}

void CSimView::SimCallback(
	DWORD dwNotifyCode,
	const void *pData,
	DWORD dwDataSize,
	DWORD dwParam)
{
	((CSimView *)dwParam)->PostMessage(
		WM_SIM,
		(WPARAM)dwNotifyCode,
		(LPARAM)pData);
}

CString CSimView::GetSimErrorText(HRESULT hr)
{
	CString sResult;
	switch (hr)
	{
	case SIM_E_SIMFAILURE:
		sResult = CString((LPCTSTR)IDS_SIM_E_SIMFAILURE);
		break;

	case SIM_E_SIMBUSY:
		sResult = CString((LPCTSTR)IDS_SIM_E_SIMBUSY);
		break;

	case SIM_E_SIMWRONG:
		sResult = CString((LPCTSTR)IDS_SIM_E_SIMWRONG);
		break;

	case SIM_E_NOSIMMSGSTORAGE:
		sResult = CString((LPCTSTR)IDS_SIM_E_NOSIMMSGSTORAGE);
		break;

	case SIM_E_SIMTOOLKITBUSY:
		sResult = CString((LPCTSTR)IDS_SIM_E_SIMTOOLKITBUSY);
		break;

	case SIM_E_SIMDOWNLOADERROR:
		sResult = CString((LPCTSTR)IDS_SIM_E_SIMDOWNLOADERROR);
		break;

	case SIM_E_SIMNOTINSERTED:
		sResult = CString((LPCTSTR)IDS_SIM_E_SIMNOTINSERTED);
		break;

	case SIM_E_PHSIMPINREQUIRED:
		sResult = CString((LPCTSTR)IDS_SIM_E_PHSIMPINREQUIRED);
		break;

	case SIM_E_PHFSIMPINREQUIRED:
		sResult = CString((LPCTSTR)IDS_SIM_E_PHFSIMPINREQUIRED);
		break;

	case SIM_E_PHFSIMPUKREQUIRED:
		sResult = CString((LPCTSTR)IDS_SIM_E_PHFSIMPUKREQUIRED);
		break;

	case SIM_E_SIMPINREQUIRED:
		sResult = CString((LPCTSTR)IDS_SIM_E_SIMPINREQUIRED);
		break;

	case SIM_E_SIMPUKREQUIRED:
		sResult = CString((LPCTSTR)IDS_SIM_E_SIMPUKREQUIRED);
		break;

	case SIM_E_INCORRECTPASSWORD:
		sResult = CString((LPCTSTR)IDS_SIM_E_INCORRECTPASSWORD);
		break;

	case SIM_E_SIMPIN2REQUIRED:
		sResult = CString((LPCTSTR)IDS_SIM_E_SIMPIN2REQUIRED);
		break;

	case SIM_E_SIMPUK2REQUIRED:
		sResult = CString((LPCTSTR)IDS_SIM_E_SIMPUK2REQUIRED);
		break;

	case SIM_E_NETWKPINREQUIRED:
		sResult = CString((LPCTSTR)IDS_SIM_E_NETWKPINREQUIRED);
		break;

	case SIM_E_NETWKPUKREQUIRED:
		sResult = CString((LPCTSTR)IDS_SIM_E_NETWKPUKREQUIRED);
		break;

	case SIM_E_SUBSETPINREQUIRED:
		sResult = CString((LPCTSTR)IDS_SIM_E_SUBSETPINREQUIRED);
		break;

	case SIM_E_SUBSETPUKREQUIRED:
		sResult = CString((LPCTSTR)IDS_SIM_E_SUBSETPUKREQUIRED);
		break;

	case SIM_E_SVCPINREQUIRED:
		sResult = CString((LPCTSTR)IDS_SIM_E_SVCPINREQUIRED);
		break;

	case SIM_E_SVCPUKREQUIRED:
		sResult = CString((LPCTSTR)IDS_SIM_E_SVCPUKREQUIRED);
		break;

	case SIM_E_CORPPINREQUIRED:
		sResult = CString((LPCTSTR)IDS_SIM_E_CORPPINREQUIRED);
		break;

	case SIM_E_CORPPUKREQUIRED:
		sResult = CString((LPCTSTR)IDS_SIM_E_CORPPUKREQUIRED);
		break;

	case SIM_E_MEMORYFULL:
		sResult = CString((LPCTSTR)IDS_SIM_E_MEMORYFULL);
		break;

	case SIM_E_INVALIDINDEX:
		sResult = CString((LPCTSTR)IDS_SIM_E_INVALIDINDEX);
		break;

	case SIM_E_NOTFOUND:
		sResult = CString((LPCTSTR)IDS_SIM_E_NOTFOUND);
		break;

	case SIM_E_MEMORYFAILURE:
		sResult = CString((LPCTSTR)IDS_SIM_E_MEMORYFAILURE);
		break;

	case SIM_E_SIMMSGSTORAGEFULL:
		sResult = CString((LPCTSTR)IDS_SIM_E_SIMMSGSTORAGEFULL);
		break;

	case SIM_E_EMPTYINDEX:
		sResult = CString((LPCTSTR)IDS_SIM_E_EMPTYINDEX);
		break;

	case SIM_E_NOTREADY:
		sResult = CString((LPCTSTR)IDS_SIM_E_NOTREADY);
		break;

	case SIM_E_SECURITYFAILURE:
		sResult = CString((LPCTSTR)IDS_SIM_E_SECURITYFAILURE);
		break;

	case SIM_E_BUFFERTOOSMALL:
		sResult = CString((LPCTSTR)IDS_SIM_E_BUFFERTOOSMALL);
		break;

	case SIM_E_NOTTEXTMESSAGE:
		sResult = CString((LPCTSTR)IDS_SIM_E_NOTTEXTMESSAGE);
		break;

	case SIM_E_NOSIM:
		sResult = CString((LPCTSTR)IDS_SIM_E_NOSIM);
		break;

	case SIM_E_NETWORKERROR:
		sResult = CString((LPCTSTR)IDS_SIM_E_NETWORKERROR);
		break;

	case SIM_E_MOBILEERROR:
		sResult = CString((LPCTSTR)IDS_SIM_E_MOBILEERROR);
		break;

	case SIM_E_UNSUPPORTED:
		sResult = CString((LPCTSTR)IDS_SIM_E_UNSUPPORTED);
		break;

	case SIM_E_BADPARAM:
		sResult = CString((LPCTSTR)IDS_SIM_E_BADPARAM);
		break;

	case SIM_E_UNDETERMINED:
		sResult = CString((LPCTSTR)IDS_SIM_E_UNDETERMINED);
		break;

	case SIM_E_RADIONOTPRESENT:
		sResult = CString((LPCTSTR)IDS_SIM_E_RADIONOTPRESENT);
		break;

	case SIM_E_RADIOOFF:
		sResult = CString((LPCTSTR)IDS_SIM_E_RADIOOFF);
		break;
	}
	if (sResult.IsEmpty())
		sResult = GetStandardErrorText(hr);
	return sResult;
}

void CSimView::AppendMsg(LPCTSTR szMessage)
{
	m_ctrlMsg.InsertItem(m_ctrlMsg.GetItemCount(), szMessage);
}

void CSimView::AppendMsg(UINT nMessageID)
{
	AppendMsg(CString((LPCTSTR)nMessageID));
}

void CSimView::OnButtonSet()
{
	CString sPin = GetWindowText(&m_ctrlPin);
	if (sPin.IsEmpty())
		return;
	DWORD dwState;
	HRESULT hr = ::SimGetPhoneLockedState(
		m_hSim,
		&dwState);
	if (hr == S_OK)
	{
		AppendMsg(GetPhoneLockText(dwState));
		if (dwState == SIM_LOCKEDSTATE_SIM_PIN)
		{
			hr = ::SimUnlockPhone(
				m_hSim,
				(LPTSTR)(LPCTSTR)sPin,
				NULL);
			if (hr != S_OK)
				AppendMsg(_T("SimUnlockPhone: ") + GetSimErrorText(hr));
		}
	}
	else
		AppendMsg(_T("SimGetPhoneLockedState: ") + GetSimErrorText(hr));
	BackupProperties();
}

CString CSimView::GetPhoneLockText(DWORD dwState)
{
	CString sResult;
	switch (dwState)
	{
	case SIM_LOCKEDSTATE_UNKNOWN:
		sResult = CString((LPCTSTR)IDS_SIM_LOCKEDSTATE_UNKNOWN);
		break;

	case SIM_LOCKEDSTATE_READY:
		sResult = CString((LPCTSTR)IDS_SIM_LOCKEDSTATE_READY);
		break;

	case SIM_LOCKEDSTATE_SIM_PIN:
		sResult = CString((LPCTSTR)IDS_SIM_LOCKEDSTATE_SIM_PIN);
		break;

	case SIM_LOCKEDSTATE_SIM_PUK:
		sResult = CString((LPCTSTR)IDS_SIM_LOCKEDSTATE_SIM_PUK);
		break;

	case SIM_LOCKEDSTATE_PH_SIM_PIN:
		sResult = CString((LPCTSTR)IDS_SIM_LOCKEDSTATE_PH_SIM_PIN);
		break;

	case SIM_LOCKEDSTATE_PH_FSIM_PIN:
		sResult = CString((LPCTSTR)IDS_SIM_LOCKEDSTATE_PH_FSIM_PIN);
		break;

	case SIM_LOCKEDSTATE_PH_FSIM_PUK:
		sResult = CString((LPCTSTR)IDS_SIM_LOCKEDSTATE_PH_FSIM_PUK);
		break;

	case SIM_LOCKEDSTATE_SIM_PIN2:
		sResult = CString((LPCTSTR)IDS_SIM_LOCKEDSTATE_SIM_PIN2);
		break;

	case SIM_LOCKEDSTATE_SIM_PUK2:
		sResult = CString((LPCTSTR)IDS_SIM_LOCKEDSTATE_SIM_PUK2);
		break;

	case SIM_LOCKEDSTATE_PH_NET_PIN:
		sResult = CString((LPCTSTR)IDS_SIM_LOCKEDSTATE_PH_NET_PIN);
		break;

	case SIM_LOCKEDSTATE_PH_NET_PUK:
		sResult = CString((LPCTSTR)IDS_SIM_LOCKEDSTATE_PH_NET_PUK);
		break;

	case SIM_LOCKEDSTATE_PH_NETSUB_PIN:
		sResult = CString((LPCTSTR)IDS_SIM_LOCKEDSTATE_PH_NETSUB_PIN);
		break;

	case SIM_LOCKEDSTATE_PH_NETSUB_PUK:
		sResult = CString((LPCTSTR)IDS_SIM_LOCKEDSTATE_PH_NETSUB_PUK);
		break;

	case SIM_LOCKEDSTATE_PH_SP_PIN:
		sResult = CString((LPCTSTR)IDS_SIM_LOCKEDSTATE_PH_SP_PIN);
		break;

	case SIM_LOCKEDSTATE_PH_SP_PUK:
		sResult = CString((LPCTSTR)IDS_SIM_LOCKEDSTATE_PH_SP_PUK);
		break;

	case SIM_LOCKEDSTATE_PH_CORP_PIN:
		sResult = CString((LPCTSTR)IDS_SIM_LOCKEDSTATE_PH_CORP_PIN);
		break;

	case SIM_LOCKEDSTATE_PH_CORP_PUK:
		sResult = CString((LPCTSTR)IDS_SIM_LOCKEDSTATE_PH_CORP_PUK);
		break;

	default:
		sResult.Format(IDS_SIM_LOCKEDSTATE_DEFAULT, dwState);
	}
	return sResult;
}

CString CSimView::GetStandardErrorText(HRESULT hr)
{
	CString sResult;
	LPVOID lpMsgBuf;
	::FormatMessage( 
		FORMAT_MESSAGE_ALLOCATE_BUFFER | 
			FORMAT_MESSAGE_FROM_SYSTEM | 
			FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL,
		hr,
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPTSTR)&lpMsgBuf,
		0,
		NULL);
	if (lpMsgBuf)
	{
		sResult = (LPCTSTR)lpMsgBuf;
		::LocalFree(lpMsgBuf);
		sResult.Replace(_T("\r\n"), _T(" "));
		sResult.TrimRight(_T(' '));
	}
	return sResult;
}

void CSimView::PrintError(LPCTSTR szMessage)
{
	::AfxMessageBox(szMessage);
}

void CSimView::OnSize(UINT nType, int cx, int cy)
{
	CFormView::OnSize(nType, cx, cy);

	if (cx == 0 &&
		cy == 0)
	{
		return;
	}

	if (m_ctrlMsg.GetSafeHwnd() == NULL)
	{
		return;
	}

	CRect r;
	GetClientRect(&r);

	ResizeControl(&m_ctrlMsg, r.right - 1, r.bottom - 1);
}

void CSimView::OnDestroy()
{
	BackupProperties();

	CFormView::OnDestroy();
}

void CSimView::BackupProperties()
{
	m_Column0Width.SetValue(m_ctrlMsg.GetColumnWidth(0));
	m_LastPin.SetValue(GetWindowText(&m_ctrlPin));
}

CString CSimView::GetWindowText(CWnd *pWnd)
{
	CString sResult;
	pWnd->GetWindowText(sResult);
	return sResult;
}
