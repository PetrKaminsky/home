#if !defined(AFX_SIMMGR_H__3D600A5A_2BF3_4177_8F88_EB99790788D2__INCLUDED_)
#define AFX_SIMMGR_H__3D600A5A_2BF3_4177_8F88_EB99790788D2__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols
#include "Property.h"

/////////////////////////////////////////////////////////////////////////////
// CSimMgrApp:
//

class CSimMgrApp : public CWinApp
{
public:
	static CRegPropertySerializer *s_pPropertySerializer;

	//{{AFX_VIRTUAL(CSimMgrApp)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	//}}AFX_VIRTUAL

	//{{AFX_MSG(CSimMgrApp)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft eMbedded Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SIMMGR_H__3D600A5A_2BF3_4177_8F88_EB99790788D2__INCLUDED_)
