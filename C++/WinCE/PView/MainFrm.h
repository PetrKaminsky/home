#if !defined(AFX_MAINFRM_H__E4895D7D_2C37_46E6_9ADF_ABA24F3317A2__INCLUDED_)
#define AFX_MAINFRM_H__E4895D7D_2C37_46E6_9ADF_ABA24F3317A2__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "ChildView.h"

/////////////////////////////////////////////////////////////////////////////
// CMainFrame

class CMainFrame : public CFrameWnd, public CActiveViewInfo
{
public:
	CMainFrame();

protected:
	DECLARE_DYNCREATE(CMainFrame)

	virtual ~CMainFrame();

public:
	//{{AFX_VIRTUAL(CMainFrame)
	public:
	virtual BOOL OnCmdMsg(UINT nID, int nCode, void* pExtra, AFX_CMDHANDLERINFO* pHandlerInfo);
	//}}AFX_VIRTUAL

protected:  // control bar embedded members
	CCeCommandBar	m_wndCommandBar;
	CProcView *m_pView1;
	CModView *m_pView2;
	CThrView *m_pView3;
	CMemView *m_pView4;
	CView *m_pActiveView;

protected:
	//{{AFX_MSG(CMainFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnProcesses();
	afx_msg void OnModules();
	afx_msg void OnThreads();
	afx_msg void OnMemory();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	void DestroyView(CWnd *&pView);
	void ActivateForm(CFormView *pForm);
	CView *GetActiveView();
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft eMbedded Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MAINFRM_H__E4895D7D_2C37_46E6_9ADF_ABA24F3317A2__INCLUDED_)
