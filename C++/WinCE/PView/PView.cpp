#include "stdafx.h"
#include "PView.h"
#include "MainFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPViewApp

BEGIN_MESSAGE_MAP(CPViewApp, CWinApp)
	//{{AFX_MSG_MAP(CPViewApp)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// The one and only CPViewApp object

CPViewApp theApp;
CRegPropertySerializer *CPViewApp::s_pPropertySerializer = NULL;

/////////////////////////////////////////////////////////////////////////////
// CPViewApp initialization

BOOL CPViewApp::InitInstance()
{
	s_pPropertySerializer = new CRegPropertySerializer(_T("HKEY_LOCAL_MACHINE\\Software\\Kamen\\Pview"));
	CMainFrame* pFrame = new CMainFrame;
	m_pMainWnd = pFrame;
	pFrame->LoadFrame(IDR_MAINFRAME, WS_OVERLAPPEDWINDOW);
	pFrame->ShowWindow(m_nCmdShow);
	pFrame->UpdateWindow();

	return TRUE;
}

int CPViewApp::ExitInstance() 
{
	delete s_pPropertySerializer;

	return CWinApp::ExitInstance();
}
