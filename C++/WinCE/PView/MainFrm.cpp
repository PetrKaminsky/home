#include "stdafx.h"
#include "PView.h"
#include "MainFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMainFrame

IMPLEMENT_DYNCREATE(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	//{{AFX_MSG_MAP(CMainFrame)
	ON_WM_CREATE()
	ON_WM_SETFOCUS()
	ON_WM_SIZE()
	ON_COMMAND(ID_PROCESSES, OnProcesses)
	ON_COMMAND(ID_MODULES, OnModules)
	ON_COMMAND(ID_THREADS, OnThreads)
	ON_COMMAND(ID_MEMORY, OnMemory)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMainFrame construction/destruction

CMainFrame::CMainFrame()
{
	m_pView1 = NULL;
	m_pView2 = NULL;
	m_pView3 = NULL;
	m_pView4 = NULL;

	m_pActiveView = NULL;
}

CMainFrame::~CMainFrame()
{
	DestroyView((CWnd *&)m_pView1);
	DestroyView((CWnd *&)m_pView2);
	DestroyView((CWnd *&)m_pView3);
	DestroyView((CWnd *&)m_pView4);
}

void CMainFrame::DestroyView(CWnd *&pView)
{
	if (pView != NULL)
	{
		pView->DestroyWindow();
		delete pView;
		pView = NULL;
	}
}

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	m_pView1 = new CProcView;
	m_pView1->Create(this);
	m_pView1->SetActiveViewInfo(this);
	m_pView2 = new CModView;
	m_pView2->Create(this);
	m_pView2->SetActiveViewInfo(this);
	m_pView2->SetSelectedProcessInfo(m_pView1);
	m_pView3 = new CThrView;
	m_pView3->Create(this);
	m_pView3->SetActiveViewInfo(this);
	m_pView3->SetSelectedProcessInfo(m_pView1);
	m_pView4 = new CMemView;
	m_pView4->Create(this);
	m_pView4->SetActiveViewInfo(this);
	m_pView4->SetSelectedProcessInfo(m_pView1);

	if (!m_wndCommandBar.Create(this) ||
	    !m_wndCommandBar.InsertMenuBar(IDR_MAINFRAME))
	{
		TRACE0("Failed to create CommandBar\n");
		return -1;      // fail to create
	}

	m_wndCommandBar.SetBarStyle(m_wndCommandBar.GetBarStyle() |
		CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_FIXED);

	ActivateForm(m_pView1);

	return 0;
}

/////////////////////////////////////////////////////////////////////////////
// CMainFrame message handlers

BOOL CMainFrame::OnCmdMsg(UINT nID, int nCode, void* pExtra, AFX_CMDHANDLERINFO* pHandlerInfo) 
{
	if (m_pActiveView->OnCmdMsg(nID, nCode, pExtra, pHandlerInfo))
		return TRUE;
	
	return CFrameWnd::OnCmdMsg(nID, nCode, pExtra, pHandlerInfo);
}

void CMainFrame::OnSetFocus(CWnd* pOldWnd) 
{
	m_pActiveView->SetFocus();
}

void CMainFrame::OnSize(UINT nType, int cx, int cy) 
{
	CFrameWnd::OnSize(nType, cx, cy);
	
// @@@@ PocketPC
	CRect r;
	GetClientRect(&r);
	if (m_pActiveView != NULL)
		m_pActiveView->MoveWindow(&r);
// @@@@ PocketPC
// @@@@ CE.NET
// @@@@ CE.NET
}

void CMainFrame::ActivateForm(CFormView *pForm)
{
	SetActiveView(pForm);
// @@@@ PocketPC
	CRect r;
	GetClientRect(&r);
// @@@@ PocketPC
// @@@@ CE.NET
//	CRect rc;
//	GetClientRect(&rc);
//	CRect rcBar;
//	m_wndCommandBar.GetWindowRect(rcBar);
//	CRect r(rc.left, rcBar.Height(), rc.right, rc.bottom);
// @@@@ CE.NET
	pForm->MoveWindow(&r);
	if (m_pActiveView != NULL && m_pActiveView != pForm)
	{
		CRect r0;
		r0.SetRectEmpty();
		m_pActiveView->MoveWindow(&r0);
	}
	m_pActiveView = pForm;
}

void CMainFrame::OnProcesses()
{
	ActivateForm(m_pView1);
}

void CMainFrame::OnModules()
{
	ActivateForm(m_pView2);
}

void CMainFrame::OnThreads()
{
	ActivateForm(m_pView3);
}

void CMainFrame::OnMemory()
{
	ActivateForm(m_pView4);
}

CView *CMainFrame::GetActiveView()
{
	return m_pActiveView;
}
