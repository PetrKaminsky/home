#if !defined(AFX_PVIEW_H__F254219E_B775_4983_A63D_0520C41C4321__INCLUDED_)
#define AFX_PVIEW_H__F254219E_B775_4983_A63D_0520C41C4321__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols
#include "Property.h"

/////////////////////////////////////////////////////////////////////////////
// CPViewApp:
//

class CPViewApp : public CWinApp
{
public:
	static CRegPropertySerializer *s_pPropertySerializer;

	//{{AFX_VIRTUAL(CPViewApp)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	//}}AFX_VIRTUAL

	//{{AFX_MSG(CPViewApp)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft eMbedded Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PVIEW_H__F254219E_B775_4983_A63D_0520C41C4321__INCLUDED_)
