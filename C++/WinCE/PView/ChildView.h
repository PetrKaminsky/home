#if !defined(AFX_CHILDVIEW_H__6A428DAD_0E2F_4744_AE5D_AC0D2BBFDE02__INCLUDED_)
#define AFX_CHILDVIEW_H__6A428DAD_0E2F_4744_AE5D_AC0D2BBFDE02__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CActiveViewInfo
{
public:
	virtual CView *GetActiveView() = 0;
};

class CSelectedProcessInfo
{
public:
	virtual bool ProcessSelected() = 0;
	virtual DWORD GetSelectedProcId() = 0;
	virtual DWORD GetMemoryBase() = 0;
};

/////////////////////////////////////////////////////////////////////////////
// CChildView form view

class CChildView : public CFormView
{
public:
	CChildView(int nID);

	int Create(CWnd *pParent);
	void SetActiveViewInfo(CActiveViewInfo *pActiveViewInfo);

protected:
	CActiveViewInfo *m_pActiveViewInfo;

	static CString GetModulePath(HMODULE hMod);

private:
	int m_nID;
};

/////////////////////////////////////////////////////////////////////////////
// CProcView form view

class CProcView
:	public CChildView,
	public CSelectedProcessInfo
{
public:
	CProcView();

	//{{AFX_DATA(CProcView)
	enum { IDD = IDD_FORM_PROCESS };
	CListCtrl	m_ctrlProc;
	//}}AFX_DATA

	//{{AFX_VIRTUAL(CProcView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

protected:
	//{{AFX_MSG(CProcView)
	afx_msg void OnRefresh();
	afx_msg void OnKill();
	afx_msg void OnUpdateKill(CCmdUI* pCmdUI);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnUpdateProcesses(CCmdUI* pCmdUI);
	afx_msg void OnUpdateModules(CCmdUI* pCmdUI);
	afx_msg void OnUpdateThreads(CCmdUI* pCmdUI);
	afx_msg void OnUpdateMemory(CCmdUI* pCmdUI);
	afx_msg void OnDestroy();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	CRegPropertyInt m_Column0Width;
	CRegPropertyInt m_Column1Width;
	CRegPropertyInt m_Column2Width;
	CRegPropertyInt m_Column3Width;
	CRegPropertyInt m_Column4Width;

	void Refresh();
	bool ProcessSelected();
	int GetSelectedItem();
	DWORD GetSelectedProcId();
	DWORD GetMemoryBase();
};

/////////////////////////////////////////////////////////////////////////////
// CSelectedProcessConsumer class

class CSelectedProcessConsumer
{
public:
	CSelectedProcessConsumer();

	void SetSelectedProcessInfo(CSelectedProcessInfo *pSelectedProcessInfo);

protected:
	CSelectedProcessInfo *m_pSelectedProcessInfo;
};

/////////////////////////////////////////////////////////////////////////////
// CModView form view

class CModView
:	public CChildView,
	public CSelectedProcessConsumer
{
public:
	CModView();

	//{{AFX_DATA(CModView)
	enum { IDD = IDD_FORM_MODULE };
	CListCtrl	m_ctrlMod;
	//}}AFX_DATA

	//{{AFX_VIRTUAL(CModView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnActivateView(BOOL bActivate, CView* pActivateView, CView* pDeactiveView);
	//}}AFX_VIRTUAL

protected:
	//{{AFX_MSG(CModView)
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnUpdateModules(CCmdUI* pCmdUI);
	afx_msg void OnUpdateProcesses(CCmdUI* pCmdUI);
	afx_msg void OnUpdateThreads(CCmdUI* pCmdUI);
	afx_msg void OnRefresh();
	afx_msg void OnUpdateMemory(CCmdUI* pCmdUI);
	afx_msg void OnDestroy();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	CRegPropertyInt m_Column0Width;
	CRegPropertyInt m_Column1Width;
	CRegPropertyInt m_Column2Width;
	CRegPropertyInt m_Column3Width;
	CRegPropertyInt m_Column4Width;

	void Refresh();
};

/////////////////////////////////////////////////////////////////////////////
// CThrView form view

class CThrView
:	public CChildView,
	public CSelectedProcessConsumer
{
public:
	CThrView();

	//{{AFX_DATA(CThrView)
	enum { IDD = IDD_FORM_THREAD };
	CListCtrl	m_ctrlThr;
	//}}AFX_DATA

	//{{AFX_VIRTUAL(CThrView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnActivateView(BOOL bActivate, CView* pActivateView, CView* pDeactiveView);
	//}}AFX_VIRTUAL

protected:
	// Generated message map functions
	//{{AFX_MSG(CThrView)
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnUpdateProcesses(CCmdUI* pCmdUI);
	afx_msg void OnUpdateModules(CCmdUI* pCmdUI);
	afx_msg void OnUpdateThreads(CCmdUI* pCmdUI);
	afx_msg void OnRefresh();
	afx_msg void OnUpdateMemory(CCmdUI* pCmdUI);
	afx_msg void OnDestroy();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	CRegPropertyInt m_Column0Width;
	CRegPropertyInt m_Column1Width;
	CRegPropertyInt m_Column2Width;

	void Refresh();
};

/////////////////////////////////////////////////////////////////////////////
// CMemView form view

class CMemView
:	public CChildView,
	public CSelectedProcessConsumer
{
public:
	CMemView();
	virtual ~CMemView();

	//{{AFX_DATA(CMemView)
	enum { IDD = IDD_FORM_MEMORY };
	CListCtrl	m_ctrlMem;
	//}}AFX_DATA

	//{{AFX_VIRTUAL(CMemView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnActivateView(BOOL bActivate, CView* pActivateView, CView* pDeactiveView);
	//}}AFX_VIRTUAL

protected:
	//{{AFX_MSG(CMemView)
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnUpdateMemory(CCmdUI* pCmdUI);
	afx_msg void OnUpdateProcesses(CCmdUI* pCmdUI);
	afx_msg void OnUpdateModules(CCmdUI* pCmdUI);
	afx_msg void OnUpdateThreads(CCmdUI* pCmdUI);
	afx_msg void OnRefresh();
	afx_msg void OnGetdispinfoListMem(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnDestroy();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	DWORD m_nBaseAddress;
	DWORD m_nChunkSize;
	const UINT m_nTimerID;
	HANDLE m_hActiveProcess;
	CRegPropertyInt m_Column0Width;
	CRegPropertyInt m_Column1Width;
	CRegPropertyInt m_Column2Width;

	void Refresh();
	int GetProcIdBytes(DWORD nProcId, DWORD nAddress, int nSize, LPBYTE pBuff);
	HANDLE GetProcessHandle(DWORD nProcId);
	void RestartTimer();
};

/////////////////////////////////////////////////////////////////////////////
//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CHILDVIEW_H__6A428DAD_0E2F_4744_AE5D_AC0D2BBFDE02__INCLUDED_)
