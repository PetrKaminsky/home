#include "stdafx.h"
#include "pview.h"
#include "ChildView.h"
#include "Globals.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CChildView

CChildView::CChildView(int nID)
:	CFormView(nID)
{
	m_nID = nID;
}

int CChildView::Create(CWnd *pParent)
{
	return CFormView::Create(NULL, NULL, WS_CHILD | WS_VISIBLE,
		CRect(0, 0, 0, 0), pParent, m_nID, NULL);
}

void CChildView::SetActiveViewInfo(CActiveViewInfo *pActiveViewInfo)
{
	m_pActiveViewInfo = pActiveViewInfo;
}

CString CChildView::GetModulePath(HMODULE hMod)
{
	CString sResult;
	TCHAR *pFileName = NULL;
	int nSizeIncr = 32;
	int nSize = nSizeIncr;
	while (true)
	{
		pFileName = new TCHAR[nSize];
		int nCopiedSize = ::GetModuleFileName(hMod, pFileName, nSize);
		if (nCopiedSize == 0)
		{
			CGlobals::PrintLastError(_T("GetModuleFileName()"));
			break;
		}
		if (nCopiedSize < (nSize - 1))
		{
			sResult = (LPCTSTR)pFileName;
			break;
		}
		if (pFileName != NULL)
			delete []pFileName;
		nSize += nSizeIncr;
	}
	if (pFileName != NULL)
		delete []pFileName;
	return sResult;
}

/////////////////////////////////////////////////////////////////////////////
// CProcView

CProcView::CProcView()
:	CChildView(CProcView::IDD),
	m_Column0Width(CPViewApp::s_pPropertySerializer, _T("ProcView\\Column0"), 100),
	m_Column1Width(CPViewApp::s_pPropertySerializer, _T("ProcView\\Column1"), 90),
	m_Column2Width(CPViewApp::s_pPropertySerializer, _T("ProcView\\Column2"), 60),
	m_Column3Width(CPViewApp::s_pPropertySerializer, _T("ProcView\\Column3"), 90),
	m_Column4Width(CPViewApp::s_pPropertySerializer, _T("ProcView\\Column4"), 200)
{
	//{{AFX_DATA_INIT(CChildView)
	//}}AFX_DATA_INIT
}

void CProcView::DoDataExchange(CDataExchange* pDX)
{
	CChildView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CProcView)
	DDX_Control(pDX, IDC_LIST_PROC, m_ctrlProc);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CProcView, CChildView)
	//{{AFX_MSG_MAP(CProcView)
	ON_COMMAND(ID_REFRESH, OnRefresh)
	ON_COMMAND(ID_KILL, OnKill)
	ON_UPDATE_COMMAND_UI(ID_KILL, OnUpdateKill)
	ON_WM_SIZE()
	ON_UPDATE_COMMAND_UI(ID_PROCESSES, OnUpdateProcesses)
	ON_UPDATE_COMMAND_UI(ID_MODULES, OnUpdateModules)
	ON_UPDATE_COMMAND_UI(ID_THREADS, OnUpdateThreads)
	ON_UPDATE_COMMAND_UI(ID_MEMORY, OnUpdateMemory)
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CProcView message handlers

void CProcView::OnInitialUpdate()
{
	CChildView::OnInitialUpdate();

	m_ctrlProc.InsertColumn(0, CString((LPCTSTR)IDS_PROCVIEW_COLUMN0), LVCFMT_LEFT, m_Column0Width.GetValue());
	m_ctrlProc.InsertColumn(1, CString((LPCTSTR)IDS_PROCVIEW_COLUMN1), LVCFMT_LEFT, m_Column1Width.GetValue());
	m_ctrlProc.InsertColumn(2, CString((LPCTSTR)IDS_PROCVIEW_COLUMN2), LVCFMT_RIGHT, m_Column2Width.GetValue());
	m_ctrlProc.InsertColumn(3, CString((LPCTSTR)IDS_PROCVIEW_COLUMN3), LVCFMT_LEFT, m_Column3Width.GetValue());
	m_ctrlProc.InsertColumn(4, CString((LPCTSTR)IDS_PROCVIEW_COLUMN4), LVCFMT_LEFT, m_Column4Width.GetValue());
	m_ctrlProc.SetExtendedStyle(m_ctrlProc.GetExtendedStyle() | LVS_EX_FULLROWSELECT | LVS_EX_HEADERDRAGDROP);

	Refresh();
}

void CProcView::OnRefresh()
{
	Refresh();
}

void CProcView::OnKill()
{
	DWORD dwProcID = GetSelectedProcId();
	HANDLE hProc = ::OpenProcess(
		0,
		FALSE,
		dwProcID);
	if (hProc != NULL)
	{
		::TerminateProcess(hProc, -1);
		::CloseHandle(hProc);

		Refresh();
	}
}

void CProcView::OnUpdateKill(CCmdUI *pCmdUI)
{
	pCmdUI->Enable(ProcessSelected() ? TRUE : FALSE);
}

void CProcView::OnUpdateProcesses(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(TRUE);
	pCmdUI->SetCheck(m_pActiveViewInfo->GetActiveView() == this ? 1 : 0);
}

void CProcView::OnUpdateModules(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(ProcessSelected() ? TRUE : FALSE);
	pCmdUI->SetCheck(0);
}

void CProcView::OnUpdateThreads(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(ProcessSelected() ? TRUE : FALSE);
	pCmdUI->SetCheck(0);
}

void CProcView::Refresh()
{
	CWaitCursor wc;
	m_ctrlProc.DeleteAllItems();
	HANDLE hSnapshot = ::CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	if (hSnapshot != INVALID_HANDLE_VALUE)
	{
		PROCESSENTRY32 pe;
		ZeroMemory(&pe, sizeof(pe));
		pe.dwSize = sizeof(pe);
		int nItem = 0;
		if (::Process32First(hSnapshot, &pe) != FALSE)
		{
			while (true)
			{
				m_ctrlProc.InsertItem(nItem, pe.szExeFile);
				CString s;
				s.Format(_T("0x%08X"), pe.th32ProcessID);
				m_ctrlProc.SetItemData(nItem, pe.th32ProcessID);
				m_ctrlProc.SetItemText(nItem, 1, s);
				s.Format(_T("%d"), pe.cntThreads);
				m_ctrlProc.SetItemText(nItem, 2, s);
				s.Format(_T("0x%08X"), pe.th32MemoryBase);
				m_ctrlProc.SetItemText(nItem, 3, s);
				m_ctrlProc.SetItemText(nItem, 4, GetModulePath((HMODULE)pe.th32ProcessID));
				nItem++;
				if (::Process32Next(hSnapshot, &pe) == FALSE)
				{
					DWORD dwErr = ::GetLastError();
					if (dwErr != ERROR_NO_MORE_FILES)
					{
						CGlobals::PrintLastError(_T("Process32Next()"));
					}
					break;
				}
			}
		}
		else
		{
			DWORD dwErr = ::GetLastError();
			if (dwErr != ERROR_NO_MORE_FILES)
			{
				CGlobals::PrintLastError(_T("Process32First()"), dwErr);
			}
		}
		if (::CloseToolhelp32Snapshot(hSnapshot) == FALSE)
		{
			CGlobals::PrintLastError(_T("CloseToolhelp32Snapshot()"));
		}
	}
	else
	{
		CGlobals::PrintLastError(_T("CreateToolhelp32Snapshot()"));
	}
}

bool CProcView::ProcessSelected()
{
	return GetSelectedItem() != -1;
}

void CProcView::OnSize(UINT nType, int cx, int cy)
{
	CChildView::OnSize(nType, cx, cy);

	if (cx == 0 && cy == 0)
		return;

	if (m_ctrlProc.GetSafeHwnd() == NULL)
		return;

	CRect r;
	GetClientRect(&r);

	CGlobals::ResizeControl(&m_ctrlProc, r.right - 1, r.bottom - 1);
}

int CProcView::GetSelectedItem()
{
	return m_ctrlProc.GetNextItem(-1, LVNI_SELECTED);
}

DWORD CProcView::GetSelectedProcId()
{
	return m_ctrlProc.GetItemData(GetSelectedItem());
}

DWORD CProcView::GetMemoryBase()
{
	DWORD nBaseAddr = 0;
	DWORD nSelProcId = GetSelectedProcId();
	HANDLE hSnapshot = ::CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	if (hSnapshot != INVALID_HANDLE_VALUE)
	{
		PROCESSENTRY32 pe;
		ZeroMemory(&pe, sizeof(pe));
		pe.dwSize = sizeof(pe);
		if (::Process32First(hSnapshot, &pe) != FALSE)
		{
			while (true)
			{
				if (pe.th32ProcessID == nSelProcId)
				{
					nBaseAddr = pe.th32MemoryBase;
					break;
				}
				if (::Process32Next(hSnapshot, &pe) == FALSE)
				{
					DWORD dwErr = ::GetLastError();
					if (dwErr != ERROR_NO_MORE_FILES)
					{
						CGlobals::PrintLastError(_T("Process32Next()"));
					}
					break;
				}
			}
		}
		else
		{
			DWORD dwErr = ::GetLastError();
			if (dwErr != ERROR_NO_MORE_FILES)
			{
				CGlobals::PrintLastError(_T("Process32First()"), dwErr);
			}
		}
		if (::CloseToolhelp32Snapshot(hSnapshot) == FALSE)
		{
			CGlobals::PrintLastError(_T("CloseToolhelp32Snapshot()"));
		}
	}
	else
	{
		CGlobals::PrintLastError(_T("CreateToolhelp32Snapshot()"));
	}
	return nBaseAddr;
}

void CProcView::OnUpdateMemory(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(ProcessSelected() ? TRUE : FALSE);
	pCmdUI->SetCheck(0);
}

void CProcView::OnDestroy()
{
	m_Column0Width.SetValue(m_ctrlProc.GetColumnWidth(0));
	m_Column1Width.SetValue(m_ctrlProc.GetColumnWidth(1));
	m_Column2Width.SetValue(m_ctrlProc.GetColumnWidth(2));
	m_Column3Width.SetValue(m_ctrlProc.GetColumnWidth(3));
	m_Column4Width.SetValue(m_ctrlProc.GetColumnWidth(4));

	CChildView::OnDestroy();
}

/////////////////////////////////////////////////////////////////////////////
// CSelectedProcessConsumer class

CSelectedProcessConsumer::CSelectedProcessConsumer()
{
	m_pSelectedProcessInfo = NULL;
}

void CSelectedProcessConsumer::SetSelectedProcessInfo(CSelectedProcessInfo *pSelectedProcessInfo)
{
	m_pSelectedProcessInfo = pSelectedProcessInfo;
}

/////////////////////////////////////////////////////////////////////////////
// CModView

CModView::CModView()
:	CChildView(CModView::IDD),
	m_Column0Width(CPViewApp::s_pPropertySerializer, _T("ModView\\Column0"), 130),
	m_Column1Width(CPViewApp::s_pPropertySerializer, _T("ModView\\Column1"), 90),
	m_Column2Width(CPViewApp::s_pPropertySerializer, _T("ModView\\Column2"), 90),
	m_Column3Width(CPViewApp::s_pPropertySerializer, _T("ModView\\Column3"), 90),
	m_Column4Width(CPViewApp::s_pPropertySerializer, _T("ModView\\Column4"), 200)
{
	//{{AFX_DATA_INIT(CModView)
	//}}AFX_DATA_INIT
}

void CModView::DoDataExchange(CDataExchange* pDX)
{
	CChildView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CModView)
	DDX_Control(pDX, IDC_LIST_MOD, m_ctrlMod);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CModView, CChildView)
	//{{AFX_MSG_MAP(CModView)
	ON_WM_SIZE()
	ON_UPDATE_COMMAND_UI(ID_MODULES, OnUpdateModules)
	ON_UPDATE_COMMAND_UI(ID_PROCESSES, OnUpdateProcesses)
	ON_UPDATE_COMMAND_UI(ID_THREADS, OnUpdateThreads)
	ON_COMMAND(ID_REFRESH, OnRefresh)
	ON_UPDATE_COMMAND_UI(ID_MEMORY, OnUpdateMemory)
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CModView message handlers

void CModView::OnInitialUpdate()
{
	CChildView::OnInitialUpdate();
	
	m_ctrlMod.InsertColumn(0, CString((LPCTSTR)IDS_MODVIEW_COLUMN0), LVCFMT_LEFT, m_Column0Width.GetValue());
	m_ctrlMod.InsertColumn(1, CString((LPCTSTR)IDS_MODVIEW_COLUMN1), LVCFMT_LEFT, m_Column1Width.GetValue());
	m_ctrlMod.InsertColumn(2, CString((LPCTSTR)IDS_MODVIEW_COLUMN2), LVCFMT_LEFT, m_Column2Width.GetValue());
	m_ctrlMod.InsertColumn(3, CString((LPCTSTR)IDS_MODVIEW_COLUMN3), LVCFMT_LEFT, m_Column3Width.GetValue());
	m_ctrlMod.InsertColumn(4, CString((LPCTSTR)IDS_MODVIEW_COLUMN4), LVCFMT_LEFT, m_Column4Width.GetValue());
	m_ctrlMod.SetExtendedStyle(m_ctrlMod.GetExtendedStyle() | LVS_EX_FULLROWSELECT | LVS_EX_HEADERDRAGDROP);
}

void CModView::OnSize(UINT nType, int cx, int cy) 
{
	CChildView::OnSize(nType, cx, cy);

	if (cx == 0 && cy == 0)
		return;

	if (m_ctrlMod.GetSafeHwnd() == NULL)
		return;

	CRect r;
	GetClientRect(&r);

	CGlobals::ResizeControl(&m_ctrlMod, r.right - 1, r.bottom - 1);
}

void CModView::OnUpdateModules(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(m_pSelectedProcessInfo->ProcessSelected() ? TRUE : FALSE);
	pCmdUI->SetCheck(m_pActiveViewInfo->GetActiveView() == this ? 1 : 0);
}

void CModView::OnUpdateProcesses(CCmdUI* pCmdUI) 
{
	pCmdUI->SetCheck(0);
}

void CModView::OnUpdateThreads(CCmdUI* pCmdUI) 
{
	pCmdUI->SetCheck(0);
}

void CModView::OnActivateView(BOOL bActivate, CView* pActivateView, CView* pDeactiveView)
{
	if (bActivate != FALSE)
		Refresh();

	CChildView::OnActivateView(bActivate, pActivateView, pDeactiveView);
}

void CModView::Refresh()
{
	CWaitCursor wc;
	DWORD nSelProcId = m_pSelectedProcessInfo->GetSelectedProcId();
	m_ctrlMod.DeleteAllItems();
	HANDLE hSnapshot = ::CreateToolhelp32Snapshot(TH32CS_SNAPMODULE, nSelProcId);
	if (hSnapshot != INVALID_HANDLE_VALUE)
	{
		MODULEENTRY32 me;
		ZeroMemory(&me, sizeof(me));
		me.dwSize = sizeof(me);
		int nItem = 0;
		if (::Module32First(hSnapshot, &me) != FALSE)
		{
			while (true)
			{
				m_ctrlMod.InsertItem(nItem, me.szModule);
				CString s;
				s.Format(_T("0x%08X"), me.th32ModuleID);
				m_ctrlMod.SetItemText(nItem, 1, s);
				s.Format(_T("0x%08X"), me.modBaseAddr);
				m_ctrlMod.SetItemText(nItem, 2, s);
				s.Format(_T("0x%08X"), me.modBaseSize);
				m_ctrlMod.SetItemText(nItem, 3, s);
				m_ctrlMod.SetItemText(nItem, 4, GetModulePath(me.hModule));
				nItem++;
				if (::Module32Next(hSnapshot, &me) == FALSE)
				{
					DWORD dwErr = ::GetLastError();
					if (dwErr != ERROR_NO_MORE_FILES)
					{
						CGlobals::PrintLastError(_T("Module32Next()"), dwErr);
					}
					break;
				}
			}
		}
		else
		{
			DWORD dwErr = ::GetLastError();
			if (dwErr != ERROR_NO_MORE_FILES)
			{
				CGlobals::PrintLastError(_T("Module32First()"));
			}
		}
		if (::CloseToolhelp32Snapshot(hSnapshot) == FALSE)
		{
			CGlobals::PrintLastError(_T("CloseToolhelp32Snapshot()"));
		}
	}
	else
	{
		CGlobals::PrintLastError(_T("CreateToolhelp32Snapshot()"));
	}
}

void CModView::OnRefresh()
{
	Refresh();
}

void CModView::OnUpdateMemory(CCmdUI* pCmdUI)
{
	pCmdUI->SetCheck(0);
}

void CModView::OnDestroy()
{
	m_Column0Width.SetValue(m_ctrlMod.GetColumnWidth(0));
	m_Column1Width.SetValue(m_ctrlMod.GetColumnWidth(1));
	m_Column2Width.SetValue(m_ctrlMod.GetColumnWidth(2));
	m_Column3Width.SetValue(m_ctrlMod.GetColumnWidth(3));
	m_Column4Width.SetValue(m_ctrlMod.GetColumnWidth(4));

	CChildView::OnDestroy();
}

/////////////////////////////////////////////////////////////////////////////
// CThrView

CThrView::CThrView()
:	CChildView(CThrView::IDD),
	m_Column0Width(CPViewApp::s_pPropertySerializer, _T("ThrView\\Column0"), 90),
	m_Column1Width(CPViewApp::s_pPropertySerializer, _T("ThrView\\Column1"), 90),
	m_Column2Width(CPViewApp::s_pPropertySerializer, _T("ThrView\\Column2"), 55)
{
	//{{AFX_DATA_INIT(CThrView)
	//}}AFX_DATA_INIT
}

void CThrView::DoDataExchange(CDataExchange* pDX)
{
	CChildView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CThrView)
	DDX_Control(pDX, IDC_LIST_THR, m_ctrlThr);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CThrView, CChildView)
	//{{AFX_MSG_MAP(CThrView)
	ON_WM_SIZE()
	ON_UPDATE_COMMAND_UI(ID_PROCESSES, OnUpdateProcesses)
	ON_UPDATE_COMMAND_UI(ID_MODULES, OnUpdateModules)
	ON_UPDATE_COMMAND_UI(ID_THREADS, OnUpdateThreads)
	ON_COMMAND(ID_REFRESH, OnRefresh)
	ON_UPDATE_COMMAND_UI(ID_MEMORY, OnUpdateMemory)
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CThrView message handlers

void CThrView::Refresh()
{
	CWaitCursor wc;
	DWORD nSelProcId = m_pSelectedProcessInfo->GetSelectedProcId();
	m_ctrlThr.DeleteAllItems();
	HANDLE hSnapshot = ::CreateToolhelp32Snapshot(TH32CS_SNAPTHREAD, 0);
	if (hSnapshot != INVALID_HANDLE_VALUE)
	{
		THREADENTRY32 te;
		ZeroMemory(&te, sizeof(te));
		te.dwSize = sizeof(te);
		int nItem = 0;
		if (::Thread32First(hSnapshot, &te) != FALSE)
		{
			while (true)
			{
				if (te.th32OwnerProcessID == nSelProcId)
				{
					CString s;
					s.Format(_T("0x%08X"), te.th32ThreadID);
					m_ctrlThr.InsertItem(nItem, s);
					s.Format(_T("0x%08X"), te.th32CurrentProcessID);
					m_ctrlThr.SetItemText(nItem, 1, s);
					s.Format(_T("%d"), te.tpBasePri);
					m_ctrlThr.SetItemText(nItem, 2, s);
					nItem++;
				}
				if (::Thread32Next(hSnapshot, &te) == FALSE)
				{
					DWORD dwErr = ::GetLastError();
					if (dwErr != ERROR_NO_MORE_FILES)
					{
						CGlobals::PrintLastError(_T("Thread32Next()"), dwErr);
					}
					break;
				}
			}
		}
		else
		{
			DWORD dwErr = ::GetLastError();
			if (dwErr != ERROR_NO_MORE_FILES)
			{
				CGlobals::PrintLastError(_T("Thread32First()"), dwErr);
			}
		}
		if (::CloseToolhelp32Snapshot(hSnapshot) == FALSE)
		{
			CGlobals::PrintLastError(_T("CloseToolhelp32Snapshot()"));
		}
	}
	else
	{
		CGlobals::PrintLastError(_T("CreateToolhelp32Snapshot()"));
	}
}

void CThrView::OnActivateView(BOOL bActivate, CView* pActivateView, CView* pDeactiveView)
{
	if (bActivate != FALSE)
		Refresh();

	CChildView::OnActivateView(bActivate, pActivateView, pDeactiveView);
}

void CThrView::OnInitialUpdate()
{
	CChildView::OnInitialUpdate();

	m_ctrlThr.InsertColumn(0, CString((LPCTSTR)IDS_THRVIEW_COLUMN0), LVCFMT_LEFT, m_Column0Width.GetValue());
	m_ctrlThr.InsertColumn(1, CString((LPCTSTR)IDS_THRVIEW_COLUMN1), LVCFMT_LEFT, m_Column1Width.GetValue());
	m_ctrlThr.InsertColumn(2, CString((LPCTSTR)IDS_THRVIEW_COLUMN2), LVCFMT_RIGHT, m_Column2Width.GetValue());
	m_ctrlThr.SetExtendedStyle(m_ctrlThr.GetExtendedStyle() | LVS_EX_FULLROWSELECT | LVS_EX_HEADERDRAGDROP);
}

void CThrView::OnSize(UINT nType, int cx, int cy)
{
	CChildView::OnSize(nType, cx, cy);

	if (cx == 0 && cy == 0)
		return;

	if (m_ctrlThr.GetSafeHwnd() == NULL)
		return;

	CRect r;
	GetClientRect(&r);

	CGlobals::ResizeControl(&m_ctrlThr, r.right - 1, r.bottom - 1);
}

void CThrView::OnUpdateProcesses(CCmdUI* pCmdUI)
{
	pCmdUI->SetCheck(0);
}

void CThrView::OnUpdateModules(CCmdUI* pCmdUI)
{
	pCmdUI->SetCheck(0);
}

void CThrView::OnUpdateThreads(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(m_pSelectedProcessInfo->ProcessSelected() ? TRUE : FALSE);
	pCmdUI->SetCheck(m_pActiveViewInfo->GetActiveView() == this ? 1 : 0);
}

void CThrView::OnRefresh()
{
	Refresh();
}

void CThrView::OnUpdateMemory(CCmdUI* pCmdUI)
{
	pCmdUI->SetCheck(0);
}

void CThrView::OnDestroy()
{
	m_Column0Width.SetValue(m_ctrlThr.GetColumnWidth(0));
	m_Column1Width.SetValue(m_ctrlThr.GetColumnWidth(1));
	m_Column2Width.SetValue(m_ctrlThr.GetColumnWidth(2));

	CChildView::OnDestroy();
}

/////////////////////////////////////////////////////////////////////////////
// CMemView

CMemView::CMemView()
:	CChildView(CMemView::IDD),
	m_nTimerID(1),
	m_Column0Width(CPViewApp::s_pPropertySerializer, _T("MemView\\Column0"), 90),
	m_Column1Width(CPViewApp::s_pPropertySerializer, _T("MemView\\Column1"), 160),
	m_Column2Width(CPViewApp::s_pPropertySerializer, _T("MemView\\Column2"), 90)
{
	//{{AFX_DATA_INIT(CMemView)
	//}}AFX_DATA_INIT
	m_nBaseAddress = 0;
	m_nChunkSize = 8;
	m_hActiveProcess = NULL;
}

CMemView::~CMemView()
{
	if (m_hActiveProcess != NULL)
	{
		::CloseHandle(m_hActiveProcess);
		m_hActiveProcess = NULL;
	}
}

void CMemView::DoDataExchange(CDataExchange* pDX)
{
	CChildView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CMemView)
	DDX_Control(pDX, IDC_LIST_MEM, m_ctrlMem);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CMemView, CChildView)
	//{{AFX_MSG_MAP(CMemView)
	ON_WM_SIZE()
	ON_UPDATE_COMMAND_UI(ID_MEMORY, OnUpdateMemory)
	ON_UPDATE_COMMAND_UI(ID_PROCESSES, OnUpdateProcesses)
	ON_UPDATE_COMMAND_UI(ID_MODULES, OnUpdateModules)
	ON_UPDATE_COMMAND_UI(ID_THREADS, OnUpdateThreads)
	ON_COMMAND(ID_REFRESH, OnRefresh)
	ON_NOTIFY(LVN_GETDISPINFO, IDC_LIST_MEM, OnGetdispinfoListMem)
	ON_WM_TIMER()
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMemView message handlers

void CMemView::OnInitialUpdate()
{
	CChildView::OnInitialUpdate();

	m_ctrlMem.InsertColumn(0, CString((LPCTSTR)IDS_MEMVIEW_COLUMN0), LVCFMT_LEFT, m_Column0Width.GetValue());
	m_ctrlMem.InsertColumn(1, CString((LPCTSTR)IDS_MEMVIEW_COLUMN1), LVCFMT_LEFT, m_Column1Width.GetValue());
	m_ctrlMem.InsertColumn(2, CString((LPCTSTR)IDS_MEMVIEW_COLUMN2), LVCFMT_LEFT, m_Column2Width.GetValue());
	m_ctrlMem.SetExtendedStyle(m_ctrlMem.GetExtendedStyle() | LVS_EX_FULLROWSELECT | LVS_EX_HEADERDRAGDROP);
	m_ctrlMem.SetItemCount(1 << 18);
}

void CMemView::OnActivateView(BOOL bActivate, CView* pActivateView, CView* pDeactiveView)
{
	if (bActivate != FALSE)
	{
		m_nBaseAddress = m_pSelectedProcessInfo->GetMemoryBase();
		m_ctrlMem.EnsureVisible(0, FALSE);
		Refresh();
	}

	CChildView::OnActivateView(bActivate, pActivateView, pDeactiveView);
}

void CMemView::OnSize(UINT nType, int cx, int cy)
{
	CChildView::OnSize(nType, cx, cy);

	if (cx == 0 && cy == 0)
		return;

	if (m_ctrlMem.GetSafeHwnd() == NULL)
		return;

	CRect r;
	GetClientRect(&r);

	CGlobals::ResizeControl(&m_ctrlMem, r.right - 1, r.bottom - 1);
}

void CMemView::OnUpdateMemory(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(m_pSelectedProcessInfo->ProcessSelected() ? TRUE : FALSE);
	pCmdUI->SetCheck(m_pActiveViewInfo->GetActiveView() == this ? 1 : 0);
}

void CMemView::OnUpdateProcesses(CCmdUI* pCmdUI)
{
	pCmdUI->SetCheck(0);
}

void CMemView::OnUpdateModules(CCmdUI* pCmdUI)
{
	pCmdUI->SetCheck(0);
}

void CMemView::OnUpdateThreads(CCmdUI* pCmdUI)
{
	pCmdUI->SetCheck(0);
}

void CMemView::OnRefresh()
{
	Refresh();
}

void CMemView::Refresh()
{
	m_ctrlMem.Invalidate();
}

void CMemView::OnGetdispinfoListMem(NMHDR* pNMHDR, LRESULT* pResult)
{
	LV_DISPINFO *pDispInfo = (LV_DISPINFO *)pNMHDR;
	LVITEM *pItem = &(pDispInfo->item);
	if (pItem->mask & LVIF_TEXT)
	{
		CString s;
		int nBuffLen;
		DWORD dwAddress = m_nBaseAddress + pItem->iItem * m_nChunkSize;
		switch (pItem->iSubItem)
		{
			case 0:
				s.Format(_T("0x%08X"), dwAddress);
				break;

			case 1:
			case 2:
			{
				BYTE *pBuff = new BYTE[m_nChunkSize];
				int nByteLen = GetProcIdBytes(
					m_pSelectedProcessInfo->GetSelectedProcId(),
					dwAddress,
					m_nChunkSize,
					pBuff);
				switch (pItem->iSubItem)
				{
					case 1:
						s = CGlobals::TransformToHexBytes(pBuff, nByteLen);
						break;

					case 2:
						s = CGlobals::TransformToText(pBuff, nByteLen);
						break;
				}
				delete []pBuff;
				break;
			}
		}
		if (s.IsEmpty() == FALSE)
		{
			nBuffLen = min(s.GetLength(), pItem->cchTextMax - 1);
			_tcsncpy(pItem->pszText, s, nBuffLen);
			pItem->pszText[nBuffLen] = _T('\0');
		}
	}

	*pResult = 0;
}

int CMemView::GetProcIdBytes(DWORD nProcId, DWORD nAddress, int nSize, LPBYTE pBuff)
{
	int nResult = 0;
	HANDLE hProcess = GetProcessHandle(nProcId);
	if (hProcess != NULL)
	{
		::ReadProcessMemory(
			hProcess,
			(LPCVOID)nAddress,
			pBuff,
			nSize,
			(DWORD *)&nResult);

	}
	return nResult;
}

void CMemView::OnTimer(UINT nIDEvent)
{
	if (nIDEvent == m_nTimerID)
		if (m_hActiveProcess != NULL)
		{
			::CloseHandle(m_hActiveProcess);
			m_hActiveProcess = NULL;
		}

	CChildView::OnTimer(nIDEvent);
}

HANDLE CMemView::GetProcessHandle(DWORD nProcId)
{
	RestartTimer();
	if (m_hActiveProcess == NULL)
		m_hActiveProcess = ::OpenProcess(0, FALSE, nProcId);
	return m_hActiveProcess;
}

void CMemView::RestartTimer()
{
	KillTimer(m_nTimerID);
	SetTimer(m_nTimerID, 1000, NULL);
}

void CMemView::OnDestroy()
{
	KillTimer(m_nTimerID);
	m_Column0Width.SetValue(m_ctrlMem.GetColumnWidth(0));
	m_Column1Width.SetValue(m_ctrlMem.GetColumnWidth(1));
	m_Column2Width.SetValue(m_ctrlMem.GetColumnWidth(2));

	CChildView::OnDestroy();
}
