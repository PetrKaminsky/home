//{{NO_DEPENDENCIES}}
// Microsoft eMbedded Visual C++ generated include file.
// Used by PView.rc
//
#define IDR_MAINFRAME                   128
#define IDS_PROCVIEW_COLUMN0            129
#define IDD_FORM_PROCESS                130
#define IDS_PROCVIEW_COLUMN1            130
#define IDD_FORM_THREAD                 131
#define IDS_PROCVIEW_COLUMN2            131
#define IDD_FORM_MODULE                 132
#define IDS_PROCVIEW_COLUMN3            132
#define IDD_FORM_MEMORY                 133
#define IDS_PROCVIEW_COLUMN4            133
#define IDS_THRVIEW_COLUMN0             134
#define IDS_THRVIEW_COLUMN1             135
#define IDS_THRVIEW_COLUMN2             136
#define IDS_MODVIEW_COLUMN0             137
#define IDS_MODVIEW_COLUMN1             138
#define IDS_MODVIEW_COLUMN2             139
#define IDS_MODVIEW_COLUMN3             140
#define IDS_MODVIEW_COLUMN4             141
#define IDS_MEMVIEW_COLUMN0             142
#define IDS_MEMVIEW_COLUMN1             143
#define IDS_MEMVIEW_COLUMN2             144
#define IDC_LIST_PROC                   1000
#define IDC_LIST_MOD                    1001
#define IDC_LIST_THR                    1002
#define IDC_LIST_MEM                    1003
#define ID_REFRESH                      32771
#define ID_KILL                         32772
#define ID_THREADS                      32773
#define ID_MODULES                      32774
#define ID_PROCESSES                    32775
#define ID_MEMORY                       32776

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        134
#define _APS_NEXT_COMMAND_VALUE         32779
#define _APS_NEXT_CONTROL_VALUE         1004
#define _APS_NEXT_SYMED_VALUE           104
#endif
#endif
