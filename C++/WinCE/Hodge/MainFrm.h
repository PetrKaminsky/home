#if !defined(AFX_MAINFRM_H__0B373DC6_9E3C_4E65_BBAA_582C922915B4__INCLUDED_)
#define AFX_MAINFRM_H__0B373DC6_9E3C_4E65_BBAA_582C922915B4__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "ChildView.h"

/////////////////////////////////////////////////////////////////////////////
// CMainFrame class

class CMainFrame : public CFrameWnd
{
protected: 
	DECLARE_DYNAMIC(CMainFrame)

	CMainFrame();
	virtual ~CMainFrame();

public:
	//{{AFX_VIRTUAL(CMainFrame)
	virtual BOOL OnCmdMsg(UINT nID, int nCode, void* pExtra, AFX_CMDHANDLERINFO* pHandlerInfo);
	//}}AFX_VIRTUAL

protected:  // control bar embedded members
	CCeCommandBar	m_wndCommandBar;
	CChildView    m_wndView;

protected:
	//{{AFX_MSG(CMainFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSetFocus(CWnd *pOldWnd);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft eMbedded Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MAINFRM_H__0B373DC6_9E3C_4E65_BBAA_582C922915B4__INCLUDED_)
