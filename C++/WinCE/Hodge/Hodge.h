#if !defined(AFX_HODGE_H__390CCE5B_F266_4B7A_87C2_383DB853533A__INCLUDED_)
#define AFX_HODGE_H__390CCE5B_F266_4B7A_87C2_383DB853533A__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols
#include "Property.h"

#define WM_SEND_HODGE_WND (WM_USER + 1)
#define WM_STOP_THREAD (WM_USER + 2)
#define WM_COMPUTE (WM_USER + 3)
#define WM_FINISHED (WM_USER + 4)
#define WM_RESTART (WM_USER + 5)
#define WM_IGNORE (WM_USER + 6)
#define WM_PAINT_HODGE (WM_USER + 7)

/////////////////////////////////////////////////////////////////////////////
// CHodgeApp

class CHodgeApp : public CWinApp
{
public:

	//{{AFX_VIRTUAL(CHodgeApp)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	//}}AFX_VIRTUAL

	//{{AFX_MSG(CHodgeApp)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////
// CParameters

class CParameters
{
public:
	CParameters();

	int GetWidth();
	int GetHeight();
	int GetDepth();
	int GetConstK1();
	int GetConstK2();
	int GetConstG();
	COLORREF GetColor();
	void Lock();
	void Unlock();
	void SetWidth(int nWidth);
	void SetHeight(int nHeight);
	void SetDepth(int nDepth);
	void SetConstK1(int nConstK1);
	void SetConstK2(int nConstK2);
	void SetConstG(int nConstG);
	void SetColor(COLORREF clrBase);
	void Load();
	void Save();

private:
	int m_nWidth;
	int m_nHeight;
	int m_nDepth;
	int m_nConstK1;
	int m_nConstK2;
	int m_nConstG;
	COLORREF m_clrBase;
	CCriticalSection m_CritSect;
	CRegPropertySerializer m_PropertySerializer;
	CRegPropertyInt m_Depth;
	CRegPropertyInt m_ConstK1;
	CRegPropertyInt m_ConstK2;
	CRegPropertyInt m_ConstG;
	CRegPropertyInt m_Color;

	static const int s_nDefaultDepth;
	static const int s_nDefaultConstK1;
	static const int s_nDefaultConstK2;
	static const int s_nDefaultConstG;
	static const int s_nDefaultColor;
};

extern CParameters g_Params;

/////////////////////////////////////////////////////////////////////////////
//{{AFX_INSERT_LOCATION}}
// Microsoft eMbedded Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_HODGE_H__390CCE5B_F266_4B7A_87C2_383DB853533A__INCLUDED_)
