#include "stdafx.h"
#include "Hodge.h"
#include "MainFrm.h"
#include "Reg.h"
#include "Globals.h"
#include "Thread.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CHodgeApp

BEGIN_MESSAGE_MAP(CHodgeApp, CWinApp)
	//{{AFX_MSG_MAP(CHodgeApp)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// The one and only CHodgeApp object

CHodgeApp theApp;
CParameters g_Params;

/////////////////////////////////////////////////////////////////////////////
// CHodgeApp initialization

BOOL CHodgeApp::InitInstance()
{
	SYSTEMTIME st;
	::GetSystemTime(&st);
	FILETIME ft;
	::SystemTimeToFileTime(&st, &ft);
	srand(ft.dwLowDateTime);

	CGlobals::SipOff();

	::SetThreadPriority(::GetCurrentThread(), THREAD_PRIORITY_BELOW_NORMAL);

	CHodgeThread::s_hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);

	g_Params.Lock();
	g_Params.Load();
	g_Params.Unlock();

	CMainFrame *pFrame = new CMainFrame;
	m_pMainWnd = pFrame;
	pFrame->LoadFrame(IDR_MAINFRAME, WS_OVERLAPPEDWINDOW);
	pFrame->ShowWindow(m_nCmdShow);
	pFrame->UpdateWindow();

	return TRUE;
}

int CHodgeApp::ExitInstance()
{
	CloseHandle(CHodgeThread::s_hEvent);
	CHodgeThread::s_pThrWnd = NULL;

	return CWinApp::ExitInstance();
}

/////////////////////////////////////////////////////////////////////////////

const int CParameters::s_nDefaultDepth = 31;
const int CParameters::s_nDefaultConstK1 = 3;
const int CParameters::s_nDefaultConstK2 = 3;
const int CParameters::s_nDefaultConstG = 6;
const int CParameters::s_nDefaultColor = RGB(255, 0, 0);

CParameters::CParameters()
:	m_PropertySerializer(_T("HKEY_LOCAL_MACHINE\\Software\\Kamen\\Hodge")),
	m_Depth(&m_PropertySerializer, _T("Depth"), s_nDefaultDepth),
	m_ConstK1(&m_PropertySerializer, _T("ConstK1"), s_nDefaultConstK1),
	m_ConstK2(&m_PropertySerializer, _T("ConstK2"), s_nDefaultConstK2),
	m_ConstG(&m_PropertySerializer, _T("ConstG"), s_nDefaultConstG),
	m_Color(&m_PropertySerializer, _T("Color"), s_nDefaultColor)
{
	m_nWidth = 0;
	m_nHeight = 0;
	m_nDepth = 0;
	m_nConstK1 = 0;
	m_nConstK2 = 0;
	m_nConstG = 0;
	m_clrBase = RGB(0, 0, 0);
}

int CParameters::GetWidth()
{
	return m_nWidth;
}

int CParameters::GetHeight()
{
	return m_nHeight;
}

int CParameters::GetDepth()
{
	return m_nDepth;
}

int CParameters::GetConstK1()
{
	return m_nConstK1;
}

int CParameters::GetConstK2()
{
	return m_nConstK2;
}

int CParameters::GetConstG()
{
	return m_nConstG;
}

COLORREF CParameters::GetColor()
{
	return m_clrBase;
}

void CParameters::Lock()
{
	m_CritSect.Lock();
}

void CParameters::Unlock()
{
	m_CritSect.Unlock();
}

void CParameters::SetWidth(int nWidth)
{
	m_nWidth = nWidth;
}

void CParameters::SetHeight(int nHeight)
{
	m_nHeight = nHeight;
}

void CParameters::SetDepth(int nDepth)
{
	m_nDepth = nDepth;
}

void CParameters::SetConstK1(int nConstK1)
{
	m_nConstK1 = nConstK1;
}

void CParameters::SetConstK2(int nConstK2)
{
	m_nConstK2 = nConstK2;
}

void CParameters::SetConstG(int nConstG)
{
	m_nConstG = nConstG;
}

void CParameters::SetColor(COLORREF clrBase)
{
	m_clrBase = clrBase;
}

void CParameters::Load()
{
	m_nDepth = m_Depth.GetValue();
	m_nConstK1 = m_ConstK1.GetValue();
	m_nConstK2 = m_ConstK2.GetValue();
	m_nConstG = m_ConstG.GetValue();
	m_clrBase = (COLORREF)m_Color.GetValue();
}

void CParameters::Save()
{
	m_Depth.SetValue(m_nDepth);
	m_ConstK1.SetValue(m_nConstK1);
	m_ConstK2.SetValue(m_nConstK2);
	m_ConstG.SetValue(m_nConstG);
	m_Color.SetValue((int)m_clrBase);
}
