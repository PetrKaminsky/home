#if !defined(AFX_CHILDVIEW_H__6D18A203_900A_4DBE_A183_3F48BF10B146__INCLUDED_)
#define AFX_CHILDVIEW_H__6D18A203_900A_4DBE_A183_3F48BF10B146__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CChildView window

class CChildView : public CWnd
{
public:
	CChildView();

	//{{AFX_VIRTUAL(CChildView)
	protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//}}AFX_VIRTUAL

protected:
	//{{AFX_MSG(CChildView)
	afx_msg void OnPaint();
	afx_msg void OnMenuProps();
	afx_msg void OnMenuRestart();
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	//}}AFX_MSG
	afx_msg LRESULT OnFinished(WPARAM wParam, LPARAM);
	afx_msg LRESULT OnIgnore(WPARAM, LPARAM);
	afx_msg LRESULT OnRestart(WPARAM wParam, LPARAM);
	afx_msg LRESULT OnPaintHodge(WPARAM, LPARAM);

	DECLARE_MESSAGE_MAP()

private:
	void InitBitmap();
	void Compute();
	COLORREF GenerateColor(BYTE *pData, int x, int y);
	COLORREF Transform(int nVal);
	void StartThread();
	void StopThread();
	void Ignore();
	void NormalizeColor(COLORREF clr);
	void PostPaintHodge();
	void RestartPaint();
	void StepPaint();
	void PaintLine(int y);

	CBitmap m_bmpOff;
	int m_nWidth;
	int m_nHeight;
	int m_nDepth;
	bool m_bIgnoreData;
	COLORREF m_clrBase;
	BYTE m_nNormRed;
	BYTE m_nNormGreen;
	BYTE m_nNormBlue;
	BYTE *m_pCurrData;
	int m_nCurrY;
};

/////////////////////////////////////////////////////////////////////////////
// CPropsDlg dialog

class CPropsDlg : public CDialog
{
public:
	CPropsDlg(CWnd *pParent);

	//{{AFX_DATA(CPropsDlg)
	enum { IDD = IDD_DIALOG_PROPS };
	CStatic	m_ctrlColor;
	int		m_nDepth;
	int		m_nG;
	int		m_nK1;
	int		m_nK2;
	//}}AFX_DATA
	COLORREF m_clrBase;

	//{{AFX_VIRTUAL(CPropsDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

protected:

	//{{AFX_MSG(CPropsDlg)
	virtual BOOL OnInitDialog();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	CBrush m_Brush;
};

/////////////////////////////////////////////////////////////////////////////
//{{AFX_INSERT_LOCATION}}
// Microsoft eMbedded Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CHILDVIEW_H__6D18A203_900A_4DBE_A183_3F48BF10B146__INCLUDED_)
