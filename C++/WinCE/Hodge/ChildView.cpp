#include "stdafx.h"
#include "Hodge.h"
#include "ChildView.h"
#include "Thread.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define PAINT_TIMER 1

/////////////////////////////////////////////////////////////////////////////
// CChildView

CChildView::CChildView()
{
	g_Params.Lock();
	m_nWidth = 0;
	m_nHeight = 0;
	m_nDepth = g_Params.GetDepth();
	m_bIgnoreData = false;
	m_nNormRed = 0;
	m_nNormGreen = 0;
	m_nNormBlue = 0;
	NormalizeColor(g_Params.GetColor());
	g_Params.Unlock();
	m_pCurrData = NULL;
	m_nCurrY = 0;
}

BEGIN_MESSAGE_MAP(CChildView, CWnd)
	//{{AFX_MSG_MAP(CChildView)
	ON_WM_PAINT()
	ON_COMMAND(ID_PROPS, OnMenuProps)
	ON_COMMAND(ID_RESTART, OnMenuRestart)
	ON_WM_CREATE()
	ON_WM_DESTROY()
	ON_WM_TIMER()
	ON_WM_ERASEBKGND()
	ON_WM_SIZE()
	//}}AFX_MSG_MAP
	ON_MESSAGE(WM_FINISHED, OnFinished)
	ON_MESSAGE(WM_IGNORE, OnIgnore)
	ON_MESSAGE(WM_RESTART, OnRestart)
	ON_MESSAGE(WM_PAINT_HODGE, OnPaintHodge)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CChildView message handlers

BOOL CChildView::PreCreateWindow(CREATESTRUCT& cs)
{
	if (!CWnd::PreCreateWindow(cs))
		return FALSE;

	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(
		CS_HREDRAW|CS_VREDRAW|CS_DBLCLKS, 
		NULL,
		0,//HBRUSH(COLOR_WINDOW + 1),
		NULL);

	return TRUE;
}

void CChildView::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	CRect r;
	if (m_bIgnoreData)
	{
		GetClientRect(&r);
		dc.FillSolidRect(&r, RGB(0, 0, 0));
	}
	else
	{
		CDC memdc;
		memdc.CreateCompatibleDC(&dc);
		CBitmap *poldbmp = memdc.SelectObject(&m_bmpOff);
		r = dc.m_ps.rcPaint;
		dc.BitBlt(r.left,
			r.top,
			r.right - r.left,
			r.bottom - r.top,
			&memdc,
			r.left,
			r.top,
			SRCCOPY);
		memdc.SelectObject(poldbmp);
	}
}

void CChildView::OnMenuProps()
{
	CPropsDlg dlg(this);
	g_Params.Lock();
	dlg.m_nDepth = g_Params.GetDepth();
	dlg.m_nK1 = g_Params.GetConstK1();
	dlg.m_nK2 = g_Params.GetConstK2();
	dlg.m_nG = g_Params.GetConstG();
	dlg.m_clrBase = g_Params.GetColor();
	g_Params.Unlock();
	if (dlg.DoModal() == IDOK)
	{
		g_Params.Lock();
		g_Params.SetDepth(m_nDepth = dlg.m_nDepth);
		g_Params.SetConstK1(dlg.m_nK1);
		g_Params.SetConstK2(dlg.m_nK2);
		g_Params.SetConstG(dlg.m_nG);
		g_Params.SetColor(dlg.m_clrBase);
		g_Params.Save();
		g_Params.Unlock();
		NormalizeColor(dlg.m_clrBase);

		Ignore();
	}
}

void CChildView::OnMenuRestart()
{
	m_bIgnoreData = true;
	CHodgeThread::s_pThrWnd->PostMessage(WM_RESTART);
}

LRESULT CChildView::OnFinished(WPARAM wParam, LPARAM)
{
	if (!m_bIgnoreData)
	{
		m_pCurrData = (BYTE *)wParam;
		RestartPaint();
	}
	return 0;
}

LRESULT CChildView::OnIgnore(WPARAM, LPARAM)
{
	m_bIgnoreData = false;
	Compute();
	return 0;
}

LRESULT CChildView::OnRestart(WPARAM wParam, LPARAM)
{
	m_bIgnoreData = false;
	return OnFinished(wParam, 0);
}

void CChildView::NormalizeColor(COLORREF clr)
{
	int nRed = GetRValue(clr);
	int nGreen = GetGValue(clr);
	int nBlue = GetBValue(clr);
	int nMax = max(nRed, max(nGreen, nBlue));
	if (nMax == 0)
		nRed = nGreen = nBlue = nMax = 255;
	m_nNormRed = 255 * nRed / nMax;
	m_nNormGreen = 255 * nGreen / nMax;
	m_nNormBlue = 255 * nBlue / nMax;
}

void CChildView::InitBitmap()
{
	CClientDC dc(this);
	if (m_bmpOff.GetSafeHandle())
		m_bmpOff.DeleteObject();
	m_bmpOff.CreateCompatibleBitmap(
		&dc, 
		m_nWidth, 
		m_nHeight);
}

void CChildView::Compute()
{
	CHodgeThread::s_pThrWnd->PostMessage(WM_COMPUTE);
}

COLORREF CChildView::GenerateColor(BYTE *pData, int x, int y)
{
	return Transform(pData[x + y * m_nWidth]);
}

COLORREF CChildView::Transform(int nVal)
{
	return RGB(
		nVal * m_nNormRed / m_nDepth,
		nVal * m_nNormGreen / m_nDepth,
		nVal * m_nNormBlue / m_nDepth);
}

void CChildView::StartThread()
{
	ResetEvent(CHodgeThread::s_hEvent);
	AfxBeginThread(RUNTIME_CLASS(CHodgeThread), THREAD_PRIORITY_IDLE);
	WaitForSingleObject(CHodgeThread::s_hEvent, INFINITE);
	CHodgeThread::s_pThrWnd->PostMessage(WM_SEND_HODGE_WND, (WPARAM)this);
}

void CChildView::StopThread()
{
	ResetEvent(CHodgeThread::s_hEvent);
	CHodgeThread::s_pThrWnd->SendMessage(WM_STOP_THREAD);
	WaitForSingleObject(CHodgeThread::s_hEvent, INFINITE);
}

void CChildView::Ignore()
{
	m_bIgnoreData = true;
	CHodgeThread::s_pThrWnd->PostMessage(WM_IGNORE);
}

int CChildView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	StartThread();

	return 0;
}

void CChildView::OnDestroy()
{
	CWnd::OnDestroy();
	
	StopThread();
}

void CChildView::OnTimer(UINT nIDEvent)
{
	if (nIDEvent == PAINT_TIMER)
		PostMessage(WM_PAINT_HODGE);
	else	
		CWnd::OnTimer(nIDEvent);
}

LRESULT CChildView::OnPaintHodge(WPARAM, LPARAM)
{
	KillTimer(PAINT_TIMER);
	StepPaint();
	return 0;
}

void CChildView::PostPaintHodge()
{
	SetTimer(PAINT_TIMER, 1, NULL);
}

void CChildView::RestartPaint()
{
	KillTimer(PAINT_TIMER);
	m_nCurrY = 0;
	PostPaintHodge();
}

void CChildView::StepPaint()
{
	PaintLine(m_nCurrY);
	m_nCurrY++;
	if (m_nCurrY < m_nHeight)
		PostPaintHodge();
	else
		Compute();
}

void CChildView::PaintLine(int y)
{
	CClientDC *pdc = new CClientDC(this);
	CDC memdc;
	memdc.CreateCompatibleDC(pdc);
	CBitmap *poldbmp = memdc.SelectObject(&m_bmpOff);
	int x;
	for (x = 0; x < m_nWidth; x++)
		memdc.SetPixel(x, y, GenerateColor(m_pCurrData, x, y));
	memdc.SelectObject(poldbmp);
	delete pdc;

	CRect r(0, y, m_nWidth, y + 1);
	InvalidateRect(r);
}

BOOL CChildView::OnEraseBkgnd(CDC* pDC)
{
	return TRUE;
}

void CChildView::OnSize(UINT nType, int cx, int cy)
{
	bool bSizeChanged = false;
	if (m_nWidth == 0 &&
		m_nHeight == 0)
	{
		if (cx != 0 ||
			cy != 0)
		{
			bSizeChanged = true;
		}
	}
	else if (m_nWidth != cx ||
		m_nHeight != cy)
	{
		bSizeChanged = true;
	}

	if (bSizeChanged)
	{
		m_nWidth = cx;
		m_nHeight = cy;

		g_Params.Lock();
		g_Params.SetWidth(m_nWidth);
		g_Params.SetHeight(m_nHeight);
		g_Params.Unlock();

		InitBitmap();
		Ignore();
	}

	CWnd::OnSize(nType, cx, cy);
}

/////////////////////////////////////////////////////////////////////////////
// CPropsDlg dialog

CPropsDlg::CPropsDlg(CWnd *pParent)
:	CDialog(CPropsDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CPropsDlg)
	m_nDepth = 0;
	m_nG = 0;
	m_nK1 = 0;
	m_nK2 = 0;
	//}}AFX_DATA_INIT
	m_clrBase = RGB(0, 0, 0);
}

void CPropsDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPropsDlg)
	DDX_Control(pDX, IDC_STATIC_COLOR, m_ctrlColor);
	DDX_Text(pDX, IDC_EDIT_DEPTH, m_nDepth);
	DDV_MinMaxInt(pDX, m_nDepth, 1, 255);
	DDX_Text(pDX, IDC_EDIT_G, m_nG);
	DDV_MinMaxInt(pDX, m_nG, 1, 16);
	DDX_Text(pDX, IDC_EDIT_K1, m_nK1);
	DDV_MinMaxInt(pDX, m_nK1, 1, 8);
	DDX_Text(pDX, IDC_EDIT_K2, m_nK2);
	DDV_MinMaxInt(pDX, m_nK2, 1, 8);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CPropsDlg, CDialog)
	//{{AFX_MSG_MAP(CPropsDlg)
	ON_WM_CTLCOLOR()
	ON_WM_LBUTTONDOWN()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPropsDlg message handlers

BOOL CPropsDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	
	m_Brush.CreateSolidBrush(m_clrBase);

	return TRUE;
}

HBRUSH CPropsDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	if (pWnd->GetSafeHwnd() == m_ctrlColor.GetSafeHwnd())
		return m_Brush;

	return CDialog::OnCtlColor(pDC, pWnd, nCtlColor);
}

void CPropsDlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	CRect r;
	m_ctrlColor.GetWindowRect(&r);
	ClientToScreen(&point);
	if (r.PtInRect(point))
	{
		CHOOSECOLOR cc;
		ZeroMemory(&cc, sizeof(cc));
		cc.lStructSize = sizeof(cc);
		cc.hwndOwner = this->GetSafeHwnd();
		cc.rgbResult = m_clrBase;
		COLORREF Cust[16];
		ZeroMemory(&Cust, sizeof(Cust));
		cc.lpCustColors = Cust;
		cc.Flags = CC_FULLOPEN | CC_RGBINIT | CC_ANYCOLOR;
		if (::ChooseColor(&cc) != FALSE)
		{
			m_clrBase = cc.rgbResult;
			m_Brush.DeleteObject();
			m_Brush.CreateSolidBrush(m_clrBase);
			m_ctrlColor.Invalidate();
		}
		return;
	}

	CDialog::OnLButtonDown(nFlags, point);
}
