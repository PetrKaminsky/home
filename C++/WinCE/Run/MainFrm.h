#if !defined(AFX_MAINFRM_H__9AA38B5A_FE27_4B1B_AEE2_BAEC72623756__INCLUDED_)
#define AFX_MAINFRM_H__9AA38B5A_FE27_4B1B_AEE2_BAEC72623756__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "ChildView.h"

/////////////////////////////////////////////////////////////////////////////

class CMainFrame : public CFrameWnd
{
public:
	virtual ~CMainFrame();

protected: 
	DECLARE_DYNAMIC(CMainFrame)

public:
	//{{AFX_VIRTUAL(CMainFrame)
	virtual BOOL OnCmdMsg(UINT nID, int nCode, void* pExtra, AFX_CMDHANDLERINFO* pHandlerInfo);
	//}}AFX_VIRTUAL

private:
	CCeCommandBar m_wndCommandBar;
	CRunForm m_wndView;

	void ActivateForm();

protected:
	//{{AFX_MSG(CMainFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSetFocus(CWnd *pOldWnd);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft eMbedded Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MAINFRM_H__9AA38B5A_FE27_4B1B_AEE2_BAEC72623756__INCLUDED_)
