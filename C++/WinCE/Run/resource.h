//{{NO_DEPENDENCIES}}
// Microsoft eMbedded Visual C++ generated include file.
// Used by Run.rc
//
#define IDD_FORM_RUN                    102
#define IDR_MAINFRAME                   128
#define IDI_ICON_FOLDER                 131
#define IDI_ICON_FILE                   132
#define IDD_DIALOG_SEL_FILE             134
#define IDC_COMBO_CMD                   1000
#define IDC_BUTTON_RUN                  1001
#define IDC_BUTTON_BROWSE               1002
#define IDC_LIST_FILES                  1017
#define IDC_BUTTON_ROOT                 1018
#define IDC_BUTTON_UP                   1019
#define IDC_EDIT_DIR                    1028

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        135
#define _APS_NEXT_COMMAND_VALUE         32772
#define _APS_NEXT_CONTROL_VALUE         1003
#define _APS_NEXT_SYMED_VALUE           104
#endif
#endif
