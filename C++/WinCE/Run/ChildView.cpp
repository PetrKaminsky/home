#include "stdafx.h"
#include "Run.h"
#include "ChildView.h"
#include "Reg.h"
#include "FileSelDlg.h"
#include "Globals.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CRunForm

CRunForm::CRunForm()
:	CFormView(CRunForm::IDD)
{
	//{{AFX_DATA_INIT(CRunForm)
	//}}AFX_DATA_INIT
}

void CRunForm::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CRunForm)
	DDX_Control(pDX, IDC_COMBO_CMD, m_ctrlCommands);
	//}}AFX_DATA_MAP
}

LPCTSTR CRunForm::s_szAppKeyName = _T("Software\\Kamen\\Run");

BEGIN_MESSAGE_MAP(CRunForm, CFormView)
	//{{AFX_MSG_MAP(CRunForm)
	ON_BN_CLICKED(IDC_BUTTON_RUN, OnButtonRun)
	ON_BN_CLICKED(IDC_BUTTON_BROWSE, OnButtonBrowse)
	ON_WM_SIZE()
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CRunForm message handlers

int CRunForm::Create(CWnd *pParent)
{
	return CFormView::Create(NULL, NULL, WS_CHILD | WS_VISIBLE,
		CRect(0, 0, 0, 0), pParent, IDD, NULL);
}

void CRunForm::BackupCmds()
{
	// backup command list to registry
	int i, nCnt = m_ctrlCommands.GetCount();
	if (nCnt > MAX_CMDS)
		nCnt = MAX_CMDS;
	TPropertyListIterator it = m_PropertyList.begin();
	for (i = 0; i < nCnt && it != m_PropertyList.end(); i++, it++)
		it->SetValue(GetLBText(&m_ctrlCommands, i));
}

void CRunForm::RestoreCmds()
{
	// restore command list from registry
	TPropertyListIterator it = m_PropertyList.begin();
	for (; it != m_PropertyList.end(); it++)
	{
		CString sCmd = it->GetValue();
		if (sCmd.IsEmpty() == FALSE)
			m_ctrlCommands.InsertString(m_ctrlCommands.GetCount(), sCmd);
		else
			break;
	}
}

void CRunForm::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();

	int i;
	for (i = 0; i < MAX_CMDS; i++)
	{
		CString sName;
		sName.Format(_T("Cmd%03i"), i);
		m_PropertyList.push_back(CRegPropertyString(CRunApp::s_pPropertySerializer, sName, _T("")));
	}

	RestoreCmds();
}

void CRunForm::OnButtonRun()
{
	CString sCmd = CGlobals::GetWindowText(&m_ctrlCommands);
	if (sCmd.IsEmpty())
		return;
	if (DoRun(sCmd))
	{
		// insert command to listbox
		int nIndex = m_ctrlCommands.FindStringExact(-1, sCmd);
		if (nIndex == CB_ERR)
		{
			// insert command to listbox only when not found
			m_ctrlCommands.InsertString(0, sCmd);
		}
		else
		{
			// otherwise move command to top
			if (nIndex > 0)
			{
				m_ctrlCommands.DeleteString(nIndex);
				m_ctrlCommands.InsertString(0, sCmd);
				m_ctrlCommands.SelectString(0, sCmd);
			}
		}

		BackupCmds();
	}
}

CString CRunForm::GetLBText(CComboBox *pCmb, int nInd)
{
	CString sResult;
	if (pCmb != NULL)
		pCmb->GetLBText(nInd, sResult);
	return sResult;
}

bool CRunForm::DoRun(LPCTSTR szCmdLine)
{
	CString sModule;
	CString sParams;
	ParseCmdLine(szCmdLine, sModule, sParams);
	SHELLEXECUTEINFO sei;
	ZeroMemory(&sei, sizeof(sei));
	sei.cbSize = sizeof(sei);
	sei.fMask = SEE_MASK_FLAG_NO_UI;
	sei.hwnd = GetSafeHwnd();
	sei.lpFile = sModule;
	if (!sParams.IsEmpty())
		sei.lpParameters = sParams;
	sei.nShow = SW_SHOW;
	if (::ShellExecuteEx(&sei) == FALSE) 
	{
		CGlobals::PrintLastError(_T("ShellExecuteEx"));
		return false;
	}
	return true;
}

void CRunForm::ParseCmdLine(LPCTSTR szCmdLine, CString &sModule, CString &sParams)
{
	CString sCmdLine(szCmdLine);
	sCmdLine.TrimLeft();
	sCmdLine.TrimLeft();
	sModule.Empty();
	sParams.Empty();
	int nState = 0;
	TCHAR c;
	int i, n = sCmdLine.GetLength();
	for (i = 0; i < n; i++)
	{
		c = sCmdLine.GetAt(i);
		switch (nState)
		{
			case 0:	// out of text
				if (c == _T('\"'))
					nState = 2;
				else
				{
					sModule += c;
					nState = 1;
				}
				break;

			case 1:	// in text
				if (c == _T(' '))
				{
					// separator found
					sParams = sCmdLine.Mid(i + 1);
					sParams.TrimLeft();
					return;
				}
				sModule += c;
				break;

			case 2:	// in apostrophed text
				if (c == _T('\"'))
				{
					// separator found
					sParams = sCmdLine.Mid(i + 1);
					sParams.TrimLeft();
					return;
				}
				sModule += c;
				break;
		}
	}
}

void CRunForm::OnButtonBrowse()
{
	CFileSelDlg dlg(this);
	if (dlg.DoModal() == IDOK)
	{
		CString sel(dlg.GetFilePath());
		if (sel.Find(_T(' ')) != -1)
			sel = _T('\"') + sel + _T('\"');
		int nLen = sel.GetLength();
		HANDLE hMem = ::LocalAlloc(LPTR, (nLen + 1) * sizeof(TCHAR));
		::CopyMemory((LPVOID)hMem, (LPVOID)(LPCTSTR)sel, nLen * sizeof(TCHAR));
		::OpenClipboard(GetSafeHwnd());
		::EmptyClipboard();
		::SetClipboardData(CF_UNICODETEXT, hMem);
		::CloseClipboard();
		m_ctrlCommands.Paste();
	}
}

void CRunForm::OnSize(UINT nType, int cx, int cy)
{
	CFormView::OnSize(nType, cx, cy);
	
	if (cx == 0 &&
		cy == 0)
	{
		return;
	}

	if (m_ctrlCommands.GetSafeHwnd() == NULL)
	{
		return;
	}

	CRect r;
	GetClientRect(&r);

	CGlobals::ResizeControl(&m_ctrlCommands, r.right - 2);
}

void CRunForm::OnDestroy()
{
	BackupCmds();

	CFormView::OnDestroy();
}
