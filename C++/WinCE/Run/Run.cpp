#include "stdafx.h"
#include "Run.h"
#include "MainFrm.h"
#include "Reg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CRunApp

BEGIN_MESSAGE_MAP(CRunApp, CWinApp)
	//{{AFX_MSG_MAP(CRunApp)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// The one and only CRunApp object

CRunApp theApp;
CRegPropertySerializer *CRunApp::s_pPropertySerializer = NULL;

/////////////////////////////////////////////////////////////////////////////
// CRunApp initialization

BOOL CRunApp::InitInstance()
{
	s_pPropertySerializer = new CRegPropertySerializer(_T("HKEY_LOCAL_MACHINE\\Software\\Kamen\\Run"));
	CMainFrame* pFrame = new CMainFrame;
	m_pMainWnd = pFrame;
	pFrame->LoadFrame(IDR_MAINFRAME, WS_OVERLAPPEDWINDOW);
	pFrame->ShowWindow(m_nCmdShow);
	pFrame->UpdateWindow();

	return TRUE;
}

int CRunApp::ExitInstance()
{
	delete s_pPropertySerializer;

	return CWinApp::ExitInstance();
}
