#if !defined(AFX_CHILDVIEW_H__BB8E0136_E5AD_4FFF_9445_A68D53B62AFB__INCLUDED_)
#define AFX_CHILDVIEW_H__BB8E0136_E5AD_4FFF_9445_A68D53B62AFB__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define MAX_CMDS 50

#include <deque>

typedef std::deque<CRegPropertyString> TPropertyList;
typedef TPropertyList::iterator TPropertyListIterator;

/////////////////////////////////////////////////////////////////////////////
// CRunForm form view

class CRunForm : public CFormView
{
public:
	CRunForm();

	int Create(CWnd *pParent);

	//{{AFX_DATA(CRunForm)
	enum { IDD = IDD_FORM_RUN };
	CComboBox	m_ctrlCommands;
	//}}AFX_DATA

	//{{AFX_VIRTUAL(CRunForm)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

	//{{AFX_MSG(CRunForm)
	afx_msg void OnButtonRun();
	afx_msg void OnButtonBrowse();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnDestroy();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	bool DoRun(LPCTSTR szCmdLine);
	void ParseCmdLine(LPCTSTR szCmdLine, CString &sModule, CString &sParams);
	void BackupCmds();
	void RestoreCmds();
	static CString GetLBText(CComboBox *pCmb, int nInd);

	static LPCTSTR s_szAppKeyName;
	TPropertyList m_PropertyList;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft eMbedded Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CHILDVIEW_H__BB8E0136_E5AD_4FFF_9445_A68D53B62AFB__INCLUDED_)
