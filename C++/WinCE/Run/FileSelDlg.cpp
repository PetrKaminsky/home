#include "stdafx.h"
#include "Run.h"
#include "FileSelDlg.h"
#include "Globals.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CFileSelDlg dialog

CFileSelDlg::CFileSelDlg(CWnd *pParent)
:	CDialog(CFileSelDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CFileSelDlg)
	//}}AFX_DATA_INIT
	m_sCurDir = _T('\\');
	m_sFileMask = _T('*');
}

void CFileSelDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CFileSelDlg)
	DDX_Control(pDX, IDC_LIST_FILES, m_ctrlFiles);
	DDX_Control(pDX, IDC_EDIT_DIR, m_ctrlDir);
	DDX_Control(pDX, IDC_BUTTON_UP, m_ctrlUp);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CFileSelDlg, CDialog)
	//{{AFX_MSG_MAP(CFileSelDlg)
	ON_NOTIFY(NM_CLICK, IDC_LIST_FILES, OnClickListFiles)
	ON_BN_CLICKED(IDC_BUTTON_ROOT, OnButtonRoot)
	ON_BN_CLICKED(IDC_BUTTON_UP, OnButtonUp)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CFileSelDlg message handlers

void CFileSelDlg::OnOK()
{
	m_sSelFileName.Empty();
	int nItem = m_ctrlFiles.GetNextItem(-1, LVIS_SELECTED);
	if (nItem != -1)
	{
		bool bFolder = m_ctrlFiles.GetItemData(nItem) == 0;
		if (!bFolder)
		{
			m_sSelFileName = m_ctrlFiles.GetItemText(nItem, 0);
		}
	}
	CDialog::OnOK();
}

void CFileSelDlg::OnClickListFiles(NMHDR *pNMHDR, LRESULT *pResult)
{
	NMLISTVIEW *pItem = (NMLISTVIEW *)pNMHDR;
	int nItem = pItem->iItem;
	if (nItem != -1)
	{
		bool bFolder = m_ctrlFiles.GetItemData(nItem) == 0;
		if (bFolder)
		{
			CString sDir(m_ctrlFiles.GetItemText(nItem, 0));
			ChangeSubdir(sDir);
		}
	}
	*pResult = 0;
}

void CFileSelDlg::OnButtonRoot()
{
	ChangeRoot();
}

void CFileSelDlg::OnButtonUp()
{
	ChangeUp();
}

BOOL CFileSelDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	PrepareImageList();

	ChangeDir(m_sCurDir);

	return TRUE;
}

void CFileSelDlg::FillList()
{
	WIN32_FIND_DATA wfd;
	CString sFileMask(m_sCurDir);
	if (sFileMask[sFileMask.GetLength() - 1] != _T('\\'))
		sFileMask += _T('\\');
	sFileMask += m_sFileMask;
	CWaitCursor wc;
	HANDLE hFind = FindFirstFile(
		sFileMask,
		&wfd);
	if (hFind != INVALID_HANDLE_VALUE)
	{
		CStringSet setDir;
		CStringSet setFile;
		do
		{
			if ((wfd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) != 0)
				setDir.Insert(wfd.cFileName);
			else
				setFile.Insert(wfd.cFileName);
			if (FindNextFile(hFind, &wfd) == FALSE)
			{
				DWORD dwError = GetLastError();
				if (dwError != ERROR_NO_MORE_FILES)
				{
					CGlobals::PrintLastError(_T("FindNextFile()"), dwError);
				}
				break;
			}
		}
		while (true);
		FindClose(hFind);
		LPCTSTR szDir = NULL;
		if (setDir.Begin())
			while ((szDir = setDir.Next()) != NULL)
				AddListItem(szDir, true);
		LPCTSTR szFile = NULL;
		if (setFile.Begin())
			while ((szFile = setFile.Next()) != NULL)
				AddListItem(szFile, false);
	}
	else
	{
		DWORD dwError = GetLastError();
		if (dwError != ERROR_NO_MORE_FILES)
		{
			CGlobals::PrintLastError(_T("FindFirstFile()"), dwError);
		}
	}
}

void CFileSelDlg::AddListItem(LPCTSTR szFileName, bool bFolder)
{
	int nIndex = m_ctrlFiles.GetItemCount();
	int nImageIndex = bFolder ? 0 : 1;
	m_ctrlFiles.InsertItem(
		nIndex,
		szFileName,
		nImageIndex);
	m_ctrlFiles.SetItemData(nIndex, nImageIndex);
}

void CFileSelDlg::EmptyList()
{
	m_ctrlFiles.DeleteAllItems();
}

void CFileSelDlg::ChangeDir(LPCTSTR szDir)
{
	SetCurDir(szDir);
	UpdateControls();
	EmptyList();
	FillList();
}

void CFileSelDlg::SetCurDir(LPCTSTR szDir)
{
	m_sCurDir = szDir;
	m_ctrlDir.SetWindowText(m_sCurDir);
}

void CFileSelDlg::UpdateControls()
{
	m_ctrlUp.EnableWindow(m_sCurDir != _T("\\") ? TRUE : FALSE);
}

CString CFileSelDlg::GetFilePath()
{
	CString sResult(m_sCurDir);
	if (!m_sSelFileName.IsEmpty())
	{
		if (sResult[sResult.GetLength() - 1] != _T('\\'))
			sResult += _T('\\');
		sResult += m_sSelFileName;
	}
	return sResult;
}

void CFileSelDlg::PrepareImageList()
{
	m_ImageList.Create(16, 16, TRUE, 2, 1);
	m_ImageList.Add(AfxGetApp()->LoadIcon(IDI_ICON_FOLDER));
	m_ImageList.Add(AfxGetApp()->LoadIcon(IDI_ICON_FILE));

	m_ctrlFiles.SetImageList(&m_ImageList, LVSIL_SMALL);
}

void CFileSelDlg::ChangeUp()
{
	CString sDir(m_sCurDir);
	int nIndex = sDir.ReverseFind(_T('\\'));
	if (nIndex != -1)
	{
		if (nIndex == 0)
			sDir = sDir.Left(1);
		else
			sDir = sDir.Left(nIndex);
	}
	ChangeDir(sDir);
}

void CFileSelDlg::ChangeRoot()
{
	ChangeDir(_T("\\"));
}

void CFileSelDlg::ChangeSubdir(LPCTSTR szDir)
{
	CString sDir(m_sCurDir);
	if (sDir[sDir.GetLength() - 1] != _T('\\'))
		sDir += _T('\\');
	sDir += szDir;
	ChangeDir(sDir);
}

/////////////////////////////////////////////////////////////////////////////

bool LessLPCTSTR::operator ()(LPCTSTR x, LPCTSTR y) const
{
	return _tcsicmp(x, y) < 0;
}

/////////////////////////////////////////////////////////////////////////////

CStringSet::~CStringSet()
{
	TStringSet::iterator it = begin();
	while (it != end())
	{
		free((void *)*it);
		it++;
	}
	clear();
}

void CStringSet::Insert(LPCTSTR szString)
{
	TStringSet::iterator it = find(szString);
	if (it != end())
		return;
	insert(_tcsdup(szString));
}

bool CStringSet::Begin()
{
	m_Iter = begin();
	if (m_Iter == end())
		return false;
	return true;
}

LPCTSTR CStringSet::Next()
{
	if (m_Iter == end())
		return NULL;
	LPCTSTR szResult = *m_Iter;
	m_Iter++;
	return szResult;
}

/////////////////////////////////////////////////////////////////////////////

std::_Lockit::_Lockit()
{
}

std::_Lockit::~_Lockit()
{
}
