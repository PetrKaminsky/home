#include "stdafx.h"
#include "SockCli.h"
#include "MainFrm.h"
#include "Socket.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define MAJOR_VER 2
#define MINOR_VER 0

/////////////////////////////////////////////////////////////////////////////
// CSockCliApp

BEGIN_MESSAGE_MAP(CSockCliApp, CWinApp)
	//{{AFX_MSG_MAP(CSockCliApp)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// The one and only CSockCliApp object

CSockCliApp theApp;
CRegPropertySerializer *CSockCliApp::s_pPropertySerializer = NULL;

/////////////////////////////////////////////////////////////////////////////
// CSockCliApp initialization

BOOL CSockCliApp::InitInstance()
{
	WSADATA wsaData;
	bool bWsaErr = false;
	if (::WSAStartup(MAKEWORD(MAJOR_VER, MINOR_VER), &wsaData) != 0)
		bWsaErr = true;
	else if (LOBYTE(wsaData.wVersion) != MAJOR_VER || HIBYTE(wsaData.wVersion) != MINOR_VER)
		bWsaErr = true;
	if (bWsaErr)
	{
		AfxMessageBox(IDP_SOCKETS_INIT_FAILED);
		return FALSE;
	}

	SYSTEMTIME st;
	::GetSystemTime(&st);
	FILETIME ft;
	::SystemTimeToFileTime(&st, &ft);
	srand(ft.dwLowDateTime);

	s_pPropertySerializer = new CRegPropertySerializer(_T("HKEY_LOCAL_MACHINE\\Software\\Kamen\\SockCli"));
	CMainFrame* pFrame = new CMainFrame;
	m_pMainWnd = pFrame;
	pFrame->LoadFrame(IDR_MAINFRAME, WS_OVERLAPPEDWINDOW);
	pFrame->ShowWindow(m_nCmdShow);
	pFrame->UpdateWindow();

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CSockCliApp message handlers

int CSockCliApp::ExitInstance()
{
	delete s_pPropertySerializer;
	CWinSockWorker::StopWorker();
	::WSACleanup();

	return CWinApp::ExitInstance();
}
