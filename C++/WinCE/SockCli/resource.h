//{{NO_DEPENDENCIES}}
// Microsoft eMbedded Visual C++ generated include file.
// Used by SockCli.rc
//
#define IDP_SOCKETS_INIT_FAILED         104
#define IDR_MAINFRAME                   128
#define IDS_MESSAGE_COLUMN0             129
#define IDD_FORM_CONNECT                130
#define IDS_MESSAGE_COLUMN1             130
#define IDS_ALREADY_CONNECTED           135
#define IDS_NOT_CONNECTED               136
#define IDS_DISCONNECTING               137
#define IDS_DISCONNECTED                138
#define IDS_CONNECTED                   139
#define IDS_RECEIVED_DATA               140
#define IDS_EMPTY_ADDRESS               141
#define IDS_EMPTY_PORT                  142
#define IDS_FORMAT_ERROR                143
#define IDS_FORMAT_BYTES                144
#define IDS_FORMAT_PEER                 145
#define IDC_EDIT_ADDR                   1000
#define IDC_EDIT_PORT                   1001
#define IDC_BUTTON_CONN                 1002
#define IDC_BUTTON_SEND                 1003
#define IDC_BUTTON_DISC                 1004
#define IDC_LIST_MESSAGE                1005
#define IDC_RADIO_IPV4                  1006
#define IDC_RADIO_IPV6                  1007

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        133
#define _APS_NEXT_COMMAND_VALUE         32774
#define _APS_NEXT_CONTROL_VALUE         1008
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
