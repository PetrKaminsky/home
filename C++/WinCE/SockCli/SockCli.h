#if !defined(AFX_SOCKCLI_H__CC0E3B60_590E_4ADF_8F53_4A369A620C4B__INCLUDED_)
#define AFX_SOCKCLI_H__CC0E3B60_590E_4ADF_8F53_4A369A620C4B__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols
#include "Property.h"

/////////////////////////////////////////////////////////////////////////////
// CSockCliApp:
//

class CSockCliApp : public CWinApp
{
public:
	static CRegPropertySerializer *s_pPropertySerializer;

	//{{AFX_VIRTUAL(CSockCliApp)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	//}}AFX_VIRTUAL

	//{{AFX_MSG(CSockCliApp)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////
//{{AFX_INSERT_LOCATION}}
// Microsoft eMbedded Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SOCKCLI_H__CC0E3B60_590E_4ADF_8F53_4A369A620C4B__INCLUDED_)
