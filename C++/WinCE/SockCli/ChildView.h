#if !defined(AFX_CHILDVIEW_H__9514EB82_022F_45D2_9AAB_A859A85FAEFD__INCLUDED_)
#define AFX_CHILDVIEW_H__9514EB82_022F_45D2_9AAB_A859A85FAEFD__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Socket.h"

/////////////////////////////////////////////////////////////////////////////
// CChldView form view

class CChldView : public CFormView
{
public:
	CChldView();
	CChldView(int nID);

	int Create(CWnd *pParent);

private:
	int m_nID;
};

/////////////////////////////////////////////////////////////////////////////
// CConnectForm form view

class CConnectForm : public CChldView
{
public:
	CConnectForm();

	//{{AFX_DATA(CConnectForm)
	enum { IDD = IDD_FORM_CONNECT };
	CEdit	m_ctrlPort;
	CListCtrl	m_ctrlMessage;
	CEdit	m_ctrlAddr;
	CButton	m_ctrlSend;
	CButton	m_ctrlDisc;
	CButton	m_ctrlConn;
	CString	m_sAddr;
	UINT	m_nPort;
	CButton m_ctrlIpv4;
	CButton m_ctrlIpv6;
	//}}AFX_DATA

	//{{AFX_VIRTUAL(CConnectForm)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

protected:
	//{{AFX_MSG(CConnectForm)
	afx_msg void OnButtonConn();
	afx_msg void OnButtonDisc();
	afx_msg void OnButtonSend();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnDestroy();
	//}}AFX_MSG
	afx_msg LRESULT OnSocketMessage(WPARAM, LPARAM);
	DECLARE_MESSAGE_MAP()

private:
	CRegPropertyInt m_Column0Width;
	CRegPropertyInt m_Column1Width;
	CRegPropertyString m_LastAddr;
	CRegPropertyInt m_LastPort;
	CRegPropertyBool m_LastIpv4;
	CRegPropertyBool m_LastIpv6;
	CWinSock m_Sock;
	bool m_bConnected;
	CWinSockBuffer m_Buff;

	void UpdateControls();
	void BackupProperties();
	void OnClose(int nErrCode);
	void OnConnect(int nErrCode);
	void OnReceive(int nErrCode);
	void OnSend(int nErrCode);
	void SetConnected(bool bConnected);
	void Log(LPCTSTR szMessage, LPCTSTR szMessage2 = NULL);
	void ReceiveBytes();
	void SendBytes();
	void CloseSocket();
	void AsyncSelect(long nEvents);
	int GetAddrFamily();
};

/////////////////////////////////////////////////////////////////////////////
//{{AFX_INSERT_LOCATION}}
// Microsoft eMbedded Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CHILDVIEW_H__9514EB82_022F_45D2_9AAB_A859A85FAEFD__INCLUDED_)
