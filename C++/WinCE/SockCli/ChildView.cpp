#include "stdafx.h"
#include "SockCli.h"
#include "ChildView.h"
#include "Globals.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CChldView

CChldView::CChldView()
:	CFormView(-1)
{
	m_nID = -1;
}

CChldView::CChldView(int nID)
:	CFormView(nID)
{
	m_nID = nID;
}

int CChldView::Create(CWnd *pParent)
{
	return CFormView::Create(NULL, NULL, WS_CHILD | WS_VISIBLE,
		CRect(0, 0, 0, 0), pParent, m_nID, NULL);
}

/////////////////////////////////////////////////////////////////////////////
// CConnectForm

#define WM_SOCKET (WM_USER + 1)

CConnectForm::CConnectForm()
:	CChldView(CConnectForm::IDD),
	m_Column0Width(CSockCliApp::s_pPropertySerializer, _T("Column0"), 100),
	m_Column1Width(CSockCliApp::s_pPropertySerializer, _T("Column1"), 100),
	m_LastAddr(CSockCliApp::s_pPropertySerializer, _T("Address"), _T("")),
	m_LastPort(CSockCliApp::s_pPropertySerializer, _T("Port"), 0),
	m_LastIpv4(CSockCliApp::s_pPropertySerializer, _T("Ipv4"), true),
	m_LastIpv6(CSockCliApp::s_pPropertySerializer, _T("Ipv6"), false)
{
	//{{AFX_DATA_INIT(CConnectForm)
	m_sAddr = _T("");
	m_nPort = 0;
	//}}AFX_DATA_INIT
	m_bConnected = false;
}

void CConnectForm::DoDataExchange(CDataExchange* pDX)
{
	CChldView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CConnectForm)
	DDX_Control(pDX, IDC_EDIT_PORT, m_ctrlPort);
	DDX_Control(pDX, IDC_LIST_MESSAGE, m_ctrlMessage);
	DDX_Control(pDX, IDC_EDIT_ADDR, m_ctrlAddr);
	DDX_Control(pDX, IDC_BUTTON_SEND, m_ctrlSend);
	DDX_Control(pDX, IDC_BUTTON_DISC, m_ctrlDisc);
	DDX_Control(pDX, IDC_BUTTON_CONN, m_ctrlConn);
	DDX_Text(pDX, IDC_EDIT_ADDR, m_sAddr);
	DDX_Text(pDX, IDC_EDIT_PORT, m_nPort);
	DDV_MinMaxUInt(pDX, m_nPort, 1, 65535);
	DDX_Control(pDX, IDC_RADIO_IPV4, m_ctrlIpv4);
	DDX_Control(pDX, IDC_RADIO_IPV6, m_ctrlIpv6);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CConnectForm, CChldView)
	//{{AFX_MSG_MAP(CConnectForm)
	ON_BN_CLICKED(IDC_BUTTON_CONN, OnButtonConn)
	ON_BN_CLICKED(IDC_BUTTON_DISC, OnButtonDisc)
	ON_BN_CLICKED(IDC_BUTTON_SEND, OnButtonSend)
	ON_WM_SIZE()
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
	ON_MESSAGE(WM_SOCKET, OnSocketMessage)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CConnectForm message handlers

void CConnectForm::OnButtonConn()
{
	if (m_bConnected)
		::AfxMessageBox(IDS_ALREADY_CONNECTED);
	else
	{
		if (UpdateData())
		{
			CString s;
			if (m_sAddr.IsEmpty())
				::AfxMessageBox(IDS_EMPTY_ADDRESS);
			else if (m_nPort == 0)
				::AfxMessageBox(IDS_EMPTY_PORT);
			else
			{
				int af = GetAddrFamily();
				if (m_Sock.Create(af))
				{
					AsyncSelect(FD_CONNECT | FD_CLOSE);
					if (m_Sock.Connect(m_sAddr, m_nPort, af))
					{
						CString s, sPeerAddr;
						short nPeerPort;
						if (m_Sock.GetPeerName(sPeerAddr, nPeerPort))
							s.Format(IDS_FORMAT_PEER, sPeerAddr, nPeerPort);
						Log(CString((LPCTSTR)IDS_CONNECTED), s);
						AsyncSelect(FD_CLOSE | FD_READ);
						SetConnected(true);
					}
					else
					{
						int nErr = CWinSock::GetLastError();
						if (nErr != WSAEWOULDBLOCK)
						{
							s.Format(IDS_FORMAT_ERROR, nErr);
							Log(_T("Connect()"), s);
							CloseSocket();
						}
					}
				}
				else
				{
					s.Format(IDS_FORMAT_ERROR, CWinSock::GetLastError());
					Log(_T("Create()"), s);
				}
			}
		}

		BackupProperties();
	}
}

void CConnectForm::OnButtonDisc()
{
	if (!m_bConnected)
		AfxMessageBox(IDS_NOT_CONNECTED);
	else
	{
		Log(CString((LPCTSTR)IDS_DISCONNECTING));
		CloseSocket();
	}
}

void CConnectForm::OnButtonSend()
{
	if (!m_bConnected)
		AfxMessageBox(IDS_NOT_CONNECTED);
	else
	{
		int i, nBuffLen = 32 + rand() % 32;
		LPBYTE pBuff = new BYTE[nBuffLen];
		for (i = 0; i < nBuffLen; i++)
			pBuff[i] = BYTE(rand() % 256);
		m_Buff.AppendData(pBuff, nBuffLen);
		delete []pBuff;
		AsyncSelect(FD_CLOSE | FD_READ | FD_WRITE);
	}
}

void CConnectForm::OnInitialUpdate()
{
	CChldView::OnInitialUpdate();

	m_ctrlMessage.InsertColumn(0, CString((LPCTSTR)IDS_MESSAGE_COLUMN0), LVCFMT_LEFT, m_Column0Width.GetValue());
	m_ctrlMessage.InsertColumn(1, CString((LPCTSTR)IDS_MESSAGE_COLUMN1), LVCFMT_LEFT, m_Column1Width.GetValue());
	m_ctrlMessage.SetExtendedStyle(m_ctrlMessage.GetExtendedStyle() | LVS_EX_FULLROWSELECT | LVS_EX_HEADERDRAGDROP);
	m_sAddr = m_LastAddr.GetValue();
	m_nPort = m_LastPort.GetValue();
	m_ctrlIpv4.SetCheck(m_LastIpv4.GetValue() ? BST_CHECKED : BST_UNCHECKED);
	m_ctrlIpv6.SetCheck(m_LastIpv6.GetValue() ? BST_CHECKED : BST_UNCHECKED);
	UpdateData(FALSE);

	UpdateControls();

	m_ctrlAddr.SetFocus();
}

void CConnectForm::UpdateControls()
{
	m_ctrlAddr.EnableWindow(m_bConnected ? FALSE : TRUE);
	m_ctrlPort.EnableWindow(m_bConnected ? FALSE : TRUE);
	m_ctrlConn.EnableWindow(m_bConnected ? FALSE : TRUE);
	m_ctrlIpv4.EnableWindow(m_bConnected ? FALSE : TRUE);
	m_ctrlIpv6.EnableWindow(m_bConnected ? FALSE : TRUE);
	m_ctrlSend.EnableWindow(m_bConnected ? TRUE : FALSE);
	m_ctrlDisc.EnableWindow(m_bConnected ? TRUE : FALSE);
}

void CConnectForm::OnSize(UINT nType, int cx, int cy)
{
	CChldView::OnSize(nType, cx, cy);

	if (cx == 0 &&
		cy == 0)
	{
		return;
	}

	if (m_ctrlAddr.GetSafeHwnd() == NULL ||
		m_ctrlMessage.GetSafeHwnd() == NULL)
	{
		return;
	}

	CRect r;
	GetClientRect(&r);

	CGlobals::ResizeControl(&m_ctrlAddr, r.right - 1);
	CGlobals::ResizeControl(&m_ctrlMessage, r.right - 1, r.bottom - 1);
}

void CConnectForm::BackupProperties()
{
	m_Column0Width.SetValue(m_ctrlMessage.GetColumnWidth(0));
	m_Column1Width.SetValue(m_ctrlMessage.GetColumnWidth(1));
	m_LastAddr.SetValue(m_sAddr);
	m_LastPort.SetValue(m_nPort);
	m_LastIpv4.SetValue(m_ctrlIpv4.GetCheck() == BST_CHECKED);
	m_LastIpv6.SetValue(m_ctrlIpv6.GetCheck() == BST_CHECKED);
}

void CConnectForm::OnDestroy()
{
	BackupProperties();
	CloseSocket();

	CChldView::OnDestroy();
}

LRESULT CConnectForm::OnSocketMessage(WPARAM wParam, LPARAM lParam)
{
	SOCKET s = (SOCKET)wParam;
	if (s == m_Sock.GetHandle())
	{
		int nErrCode = WSAGETSELECTERROR(lParam);
		int nEvent = WSAGETSELECTEVENT(lParam);
		switch (nEvent)
		{
		case FD_READ:
			OnReceive(nErrCode);
			break;

		case FD_WRITE:
			OnSend(nErrCode);
			break;

		case FD_CONNECT:
			OnConnect(nErrCode);
			break;

		case FD_CLOSE:
			OnClose(nErrCode);
			break;
		}
	}
	return (LRESULT)0;
}

void CConnectForm::OnClose(int nErrCode)
{
	if (nErrCode != 0)
	{
		CString s;
		s.Format(IDS_FORMAT_ERROR, nErrCode);
		Log(_T("OnClose()"), s);
	}
	ReceiveBytes();
	Log(CString((LPCTSTR)IDS_DISCONNECTED));
	CloseSocket();
}

void CConnectForm::OnConnect(int nErrCode)
{
	if (nErrCode != 0)
	{
		CString s;
		s.Format(IDS_FORMAT_ERROR, nErrCode);
		Log(_T("OnConnect()"), s);
	}
	else if (!m_bConnected)
	{
		CString s, sPeerAddr;
		short nPeerPort;
		if (m_Sock.GetPeerName(sPeerAddr, nPeerPort))
			s.Format(IDS_FORMAT_PEER, sPeerAddr, nPeerPort);
		Log(CString((LPCTSTR)IDS_CONNECTED), s);
		AsyncSelect(FD_CLOSE | FD_READ);
		SetConnected(true);
	}
}

void CConnectForm::OnReceive(int nErrCode)
{
	if (nErrCode != 0)
	{
		CString s;
		s.Format(IDS_FORMAT_ERROR, nErrCode);
		Log(_T("OnReceive()"), s);
	}
	else
		ReceiveBytes();
}

void CConnectForm::OnSend(int nErrCode)
{
	if (nErrCode != 0)
	{
		CString s;
		s.Format(IDS_FORMAT_ERROR, nErrCode);
		Log(_T("OnSend()"), s);
	}
	else
		SendBytes();
}

void CConnectForm::SetConnected(bool bConnected)
{
	m_bConnected = bConnected;
	UpdateControls();
}

void CConnectForm::Log(LPCTSTR szMessage, LPCTSTR szMessage2)
{
	int nItem = m_ctrlMessage.GetItemCount();
	m_ctrlMessage.InsertItem(nItem, szMessage);
	if (szMessage2 != NULL)
		m_ctrlMessage.SetItemText(nItem, 1, szMessage2);
}

void CConnectForm::ReceiveBytes()
{
	int nTotalBytes = m_Sock.GetAvailableReadCount();
	if (nTotalBytes > 0)
	{
		CString s;
		s.Format(IDS_FORMAT_BYTES, nTotalBytes);
		Log(CString((LPCTSTR)IDS_RECEIVED_DATA), s);
		BYTE *pBuff = new BYTE[nTotalBytes];
		int nRecvBytes = m_Sock.Receive(pBuff, nTotalBytes);
		if (nRecvBytes == SOCKET_ERROR)
		{
			int nErr = CWinSock::GetLastError();
			if (nErr != WSAEWOULDBLOCK)
			{
				s.Format(IDS_FORMAT_ERROR, nErr);
				Log(_T("Receive()"), s);
				CloseSocket();
			}
		}
		else
		{
			int nCurrBytes = 0, nChunkSize = 8, nLogBytes;
			while (nCurrBytes < nRecvBytes)
			{
				nLogBytes = nRecvBytes - nCurrBytes;
				if (nLogBytes > nChunkSize)
					nLogBytes = nChunkSize;
				Log(
					CGlobals::TransformToHexBytes(pBuff + nCurrBytes, nLogBytes),
					CGlobals::TransformToText(pBuff + nCurrBytes, nLogBytes));
				nCurrBytes += nLogBytes;
			}
		}
		delete []pBuff;
	}
}

void CConnectForm::SendBytes()
{
	if (!m_Buff.IsEmpty())
	{
		int nSentBytes = m_Sock.Send(m_Buff.GetData(), m_Buff.GetDataLen());
		if (nSentBytes > 0)
			m_Buff.RemoveHeadCount(nSentBytes);
		else if (nSentBytes == SOCKET_ERROR)
		{
			int nErr = CWinSock::GetLastError();
			if (nErr != WSAEWOULDBLOCK)
			{
				CString s;
				s.Format(IDS_FORMAT_ERROR, nErr);
				Log(_T("Send()"), s);
				CloseSocket();
				return;
			}
		}
	}
	if (m_Buff.IsEmpty())
		AsyncSelect(FD_CLOSE | FD_READ);
}

void CConnectForm::CloseSocket()
{
	if (m_bConnected)
	{
		if (!m_Sock.Shutdown())
		{
			CString s;
			s.Format(IDS_FORMAT_ERROR, CWinSock::GetLastError());
			Log(_T("Shutdown()"), s);
		}
	}
	m_Sock.Close();
	m_Buff.Clear();
	SetConnected(false);
}

void CConnectForm::AsyncSelect(long nEvents)
{
	m_Sock.AsyncSelect(GetSafeHwnd(), WM_SOCKET, nEvents);
}

int CConnectForm::GetAddrFamily()
{
	if (m_ctrlIpv6.GetCheck() == BST_CHECKED)
		return AF_INET6;
	return AF_INET;
}
