#include "stdafx.h"
#include "Reg.h"

/////////////////////////////////////////////////////////////////////////////

CRegKeyEnum::CRegKeyEnum(HKEY hParentKey)
{
	m_bOpened = FALSE;
	m_hParentKey = hParentKey;
	m_hKey = NULL;
	m_nIndex = 0;
}

CRegKeyEnum::~CRegKeyEnum()
{
	Close();
}

BOOL CRegKeyEnum::Init(LPCTSTR szSubkeyName)
{
	Close();
	if (::RegOpenKeyEx(
		m_hParentKey,
		szSubkeyName,
		0,
		0,
		&m_hKey) == ERROR_SUCCESS)
	{
		m_bOpened = TRUE;
		return TRUE;
	}
	return FALSE;
}

BOOL CRegKeyEnum::GetNext(CString &str)
{
	DWORD nSize = sizeof(m_szBuffer) / sizeof(TCHAR);
	ZeroMemory(m_szBuffer, sizeof(m_szBuffer));
	FILETIME ft;
	if (::RegEnumKeyEx(
		m_hKey,
		m_nIndex,
		m_szBuffer,
		&nSize,
		NULL,
		NULL,
		NULL,
		&ft) == ERROR_SUCCESS)
	{
		str = m_szBuffer;
		m_nIndex++;
		return TRUE;
	}
	return FALSE;
}

void CRegKeyEnum::Close()
{
	if (m_bOpened)
	{
		::RegCloseKey(m_hKey);
		m_hKey = NULL;
		m_bOpened = FALSE;
	}
	m_nIndex = 0;
}

/////////////////////////////////////////////////////////////////////////////

CRegValueEnum::CRegValueEnum(HKEY hParentKey)
{
	m_bOpened = FALSE;
	m_hParentKey = hParentKey;
	m_hKey = NULL;
	m_nIndex = 0;
}

CRegValueEnum::~CRegValueEnum()
{
	Close();
}

BOOL CRegValueEnum::Init(LPCTSTR szSubkeyName)
{
	Close();
	if (::RegOpenKeyEx(
		m_hParentKey,
		szSubkeyName,
		0,
		0,
		&m_hKey) == ERROR_SUCCESS)
	{
		m_bOpened = TRUE;
		return TRUE;
	}
	return FALSE;
}

BOOL CRegValueEnum::GetNext(CString &str, DWORD &dwType)
{
	DWORD nSize = sizeof(m_szBuffer) / sizeof(TCHAR);
	ZeroMemory(m_szBuffer, sizeof(m_szBuffer));
	if (::RegEnumValue( 
		m_hKey, 
		m_nIndex, 
		m_szBuffer, 
		&nSize, 
		NULL, 
		&dwType, 
		NULL, 
		NULL) == ERROR_SUCCESS)
	{
		str = m_szBuffer;
		m_nIndex++;
		return TRUE;
	}
	return FALSE;
}

void CRegValueEnum::Close()
{
	if (m_bOpened)
	{
		::RegCloseKey(m_hKey);
		m_hKey = NULL;
		m_bOpened = FALSE;
	}
	m_nIndex = 0;
}

/////////////////////////////////////////////////////////////////////////////

CRegKeyEx::CRegKeyEx(HKEY hParentKey)
{
	m_bOpened = FALSE;
	m_hParentKey = hParentKey;
	m_hKey = NULL;
}

CRegKeyEx::CRegKeyEx()
{
	m_bOpened = FALSE;
	m_hParentKey = NULL;
	m_hKey = NULL;
}

CRegKeyEx::~CRegKeyEx()
{
	CloseKey();
}

BOOL CRegKeyEx::DeleteKey(LPCTSTR szSubkeyName, HKEY hParentKey)
{
	if (::RegDeleteKey(hParentKey, szSubkeyName) == ERROR_SUCCESS)
		return TRUE;
	return FALSE;
}

BOOL CRegKeyEx::DeleteKey(LPCTSTR szSubkeyName)
{
	return DeleteKey(szSubkeyName, m_hKey);
}

BOOL CRegKeyEx::DeleteValue(LPCTSTR szValueName, HKEY hParentKey)
{
	if (::RegDeleteValue(hParentKey, szValueName) == ERROR_SUCCESS)
		return TRUE;
	return FALSE;
}

BOOL CRegKeyEx::DeleteValue(LPCTSTR szValueName)
{
	return DeleteValue(szValueName, m_hKey);
}

void CRegKeyEx::CloseKey()
{
	if (m_bOpened)
	{
		::RegCloseKey(m_hKey);
		m_hKey = NULL;
		m_bOpened = FALSE;
	}
}

BOOL CRegKeyEx::OpenKey(LPCTSTR szSubkeyName)
{
	CloseKey();
	if (::RegOpenKeyEx(
		m_hParentKey,
		szSubkeyName,
		0,
		KEY_QUERY_VALUE,
		&m_hKey) == ERROR_SUCCESS)
	{
		m_bOpened = TRUE;
		return TRUE;
	}
	return FALSE;
}

HKEY CRegKeyEx::GetParentKey(LPCTSTR szParentKey)
{
	if (_tcscmp(szParentKey, _T("HKEY_CLASSES_ROOT")) == 0)
		return HKEY_CLASSES_ROOT;
	else if (_tcscmp(szParentKey, _T("HKEY_CURRENT_USER")) == 0)
		return HKEY_CURRENT_USER;
	else if (_tcscmp(szParentKey, _T("HKEY_LOCAL_MACHINE")) == 0)
		return HKEY_LOCAL_MACHINE;
	else if (_tcscmp(szParentKey, _T("HKEY_USERS")) == 0)
		return HKEY_USERS;
	return NULL;
}

BOOL CRegKeyEx::OpenKey2(LPCTSTR szKeyName)
{
	CString sKeyName(szKeyName);
	if (!sKeyName.IsEmpty())
	{
		int nInd = sKeyName.Find(_T('\\'));
		if (nInd != -1)
		{
			CString sParent(sKeyName.Left(nInd)), sSubKey(sKeyName.Mid(nInd + 1));
			m_hParentKey = GetParentKey(sParent);
			return OpenKey(sSubKey);
		}
	}
	return FALSE;
}

BOOL CRegKeyEx::ReadDword(LPCTSTR szValueName, DWORD &dwValue)
{
	DWORD dwSize = sizeof(dwValue);
	if (m_bOpened)
		if (::RegQueryValueEx(
				m_hKey,
				szValueName,
				NULL,
				NULL,
				(LPBYTE)&dwValue,
				&dwSize) == ERROR_SUCCESS)
			return TRUE;
	return FALSE;
}

BOOL CRegKeyEx::ReadString(LPCTSTR szValueName, CString &sValue)
{
	BOOL bResult = FALSE;
	DWORD dwSize;
	if (m_bOpened)
		if (::RegQueryValueEx(
			m_hKey,
			szValueName,
			NULL,
			NULL,
			NULL,
			&dwSize) == ERROR_SUCCESS)
		{
			LPTSTR szBuffer = new TCHAR[dwSize];
			if (szBuffer)
			{
				if (::RegQueryValueEx(
					m_hKey,
					szValueName,
					NULL,
					NULL,
					(LPBYTE)szBuffer,
					&dwSize) == ERROR_SUCCESS)
				{
					sValue = szBuffer;
					bResult = TRUE;
				}
				delete []szBuffer;
			}
		}
	return bResult;
}

BOOL CRegKeyEx::ReadDouble(LPCTSTR szValueName, double &dValue)
{
	DWORD dwSize = sizeof(dValue);
	return ReadBinary(szValueName, (BYTE *)&dValue, &dwSize);
}

BOOL CRegKeyEx::ReadBinary(LPCTSTR szValueName, BYTE *pData, DWORD *pdwLen)
{
	if (m_bOpened)
		if (::RegQueryValueEx(
				m_hKey,
				szValueName,
				NULL,
				NULL,
				pData,
				pdwLen) == ERROR_SUCCESS)
			return TRUE;
	return FALSE;
}

BOOL CRegKeyEx::CreateKey(LPCTSTR szSubkeyName)
{
	CloseKey();
	if (::RegCreateKeyEx(
		m_hParentKey,
		szSubkeyName,
		0,
		NULL,
		REG_OPTION_NON_VOLATILE,
		KEY_WRITE,
		NULL,
		&m_hKey,
		NULL) == ERROR_SUCCESS)
	{
		m_bOpened = TRUE;
		return TRUE;
	}
	return FALSE;
}

BOOL CRegKeyEx::CreateKey2(LPCTSTR szKeyName)
{
	CString sKeyName(szKeyName);
	if (!sKeyName.IsEmpty())
	{
		int nInd = sKeyName.Find(_T('\\'));
		if (nInd != -1)
		{
			CString sParent(sKeyName.Left(nInd)), sSubKey(sKeyName.Mid(nInd + 1));
			m_hParentKey = GetParentKey(sParent);
			return CreateKey(sSubKey);
		}
	}
	return FALSE;
}

BOOL CRegKeyEx::WriteDword(LPCTSTR szValueName, DWORD dwValue)
{
	if (::RegSetValueEx(
			m_hKey,
			szValueName,
			0,
			REG_DWORD,
			(CONST BYTE *)&dwValue,
			sizeof(dwValue)) == ERROR_SUCCESS)
		return TRUE;
	return FALSE;
}

BOOL CRegKeyEx::WriteString(LPCTSTR szValueName, LPCTSTR szValue)
{
	if (::RegSetValueEx(
			m_hKey,
			szValueName,
			0,
			REG_SZ,
			(CONST BYTE *)szValue,
			sizeof(TCHAR) * (_tcslen(szValue) + 1)) == ERROR_SUCCESS)
		return TRUE;
	return FALSE;
}

BOOL CRegKeyEx::WriteDouble(LPCTSTR szValueName, double dValue)
{
	return WriteBinary(szValueName, (BYTE *)&dValue, sizeof(dValue));
}

BOOL CRegKeyEx::WriteBinary(LPCTSTR szValueName, BYTE *pData, DWORD dwLen)
{
	if (::RegSetValueEx(
			m_hKey,
			szValueName,
			0,
			REG_BINARY,
			pData,
			dwLen) == ERROR_SUCCESS)
		return TRUE;
	return FALSE;
}

//////////////////////////////////////////////////////////////////////

CRegPath::CRegPath(LPCTSTR szFullPath)
{
	ParseFullPath(szFullPath, m_sKeyName, m_sValueName);
}

CRegPath::CRegPath(LPCTSTR szKeyName, LPCTSTR szValueName)
{
	CString sFullPath(CreateFullPath(szKeyName, szValueName));
	ParseFullPath(sFullPath, m_sKeyName, m_sValueName);
}

CString CRegPath::CreateFullPath(LPCTSTR szKeyName, LPCTSTR szValueName)
{
	CString sResult;
	sResult += szKeyName;
	sResult += _T('\\');
	sResult += szValueName;
	return sResult;
}

void CRegPath::ParseFullPath(
	LPCTSTR szFullPath,
	CString &sKeyName,
	CString &sValueName)
{
	CString sFullPath(szFullPath);
	int nInd = sFullPath.ReverseFind(_T('\\'));
	if (nInd != -1)
	{
		sKeyName = sFullPath.Left(nInd);
		sValueName = sFullPath.Mid(nInd + 1);
	}
}
