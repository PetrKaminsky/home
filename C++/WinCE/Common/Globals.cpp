#include "stdafx.h"
#include "Globals.h"

#include <winsock.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

#pragma comment(lib, "ws2")

//////////////////////////////////////////////////////////////////////

CString CGlobals::GetWindowText(CWnd *pWnd)
{
	CString sResult;
	if (pWnd != NULL)
		pWnd->GetWindowText(sResult);
	return sResult;
}

void CGlobals::ResizeControl(CWnd *pCtrl, int nWidth, int nHeight)
{
	if (pCtrl != NULL)
	{
		CRect r;
		pCtrl->GetWindowRect(&r);
		pCtrl->GetParent()->ScreenToClient(&r);
		r.right = nWidth;
		if (nHeight != -1)
			r.bottom = nHeight;
		pCtrl->MoveWindow(&r);
	}
}

CString CGlobals::TransformToHexBytes(LPBYTE pBuff, int nBytes)
{
	int i;
	CString sTemp, sResult;
	for (i = 0; i < nBytes; i++)
	{
		if (i > 0)
			sResult += _T(' ');
		sTemp.Format(_T("%02X"), (BYTE)pBuff[i]);
		sResult += sTemp;
	}
	return sResult;
}

CString CGlobals::TransformToText(LPBYTE pBuff, int nBytes)
{
	int i;
	CString sResult;
	for (i = 0; i < nBytes; i++)
		sResult += (CGlobals::IsPrint(pBuff[i])) ? pBuff[i] : _T('.');
	return sResult;
}

void CGlobals::PrintLastError(
	LPCTSTR szMethod,
	DWORD dwError,
	LPCTSTR szMessage,
	LPCTSTR szModule)
{
	CString sMsg;
	sMsg.Format(
		_T("%s: %d"),
		szMethod,
		dwError);
	CString sErr = GetErrorText(dwError, szModule);
	if (!sErr.IsEmpty())
	{
		sMsg += _T(", ");
		sMsg += sErr;
	}
	if (szMessage != NULL && *szMessage != _T('\0'))
	{
		sMsg += _T(", ");
		sMsg += szMessage;
	}
	sMsg.Replace(_T("\r\n"), _T(" "));
	sMsg.TrimRight(_T(' '));

	::AfxMessageBox(sMsg);
}

CString CGlobals::GetErrorText(DWORD dwError, LPCTSTR szModule)
{
	CString sResult;
	LPVOID lpMsgBuf = NULL;
	HMODULE hMod = NULL;
	if (szModule != NULL)
	{
		hMod = ::LoadLibrary(szModule);
	}
	::FormatMessage( 
		FORMAT_MESSAGE_ALLOCATE_BUFFER | 
			FORMAT_MESSAGE_FROM_SYSTEM | 
			FORMAT_MESSAGE_IGNORE_INSERTS |
			FORMAT_MESSAGE_FROM_HMODULE,
		hMod,
		dwError,
		0,
		(LPTSTR)&lpMsgBuf,
		0,
		NULL);
	if (lpMsgBuf != NULL)
	{
		sResult = (LPCTSTR)lpMsgBuf;
		::LocalFree(lpMsgBuf);
	}
	if (hMod != NULL)
	{
		::FreeLibrary(hMod);
	}
	return sResult;
}

CString CGlobals::GuidToString(GUID &guid)
{
	CString sResult;
	BYTE *pGuid = (BYTE *)&guid;
	sResult.Format(_T("%08X-%04X-%04X-%02X%02X-%02X%02X%02X%02X%02X%02X"),
		*(unsigned int *)pGuid,
		*(unsigned short *)(pGuid + 4),
		*(unsigned short *)(pGuid + 6),
		pGuid[8], pGuid[9],
		pGuid[10], pGuid[11], pGuid[12], pGuid[13], pGuid[14], pGuid[15]);
	return sResult;
}

bool CGlobals::StringToGuid(LPCTSTR szGuid, GUID &guid)
{
	BYTE *pGuid = (BYTE *)&guid;
	int parsed = _stscanf(szGuid, _T("%08X-%04X-%04X-%02X%02X-%02X%02X%02X%02X%02X%02X"),
		(unsigned int *)pGuid,
		(unsigned short *)(pGuid + 4),
		(unsigned short *)(pGuid + 6),
		pGuid + 8, pGuid + 9,
		pGuid + 10, pGuid + 11, pGuid + 12, pGuid + 13, pGuid + 14, pGuid + 15);
	return parsed == 11;
}

CString CGlobals::ToString(int nValue)
{
	CString sResult;
	sResult.Format(_T("%d"), nValue);
	return sResult;
}

void CGlobals::SipOff()
{
	SIPINFO si;
	ZeroMemory(&si, sizeof(si));
	si.cbSize = sizeof(si);
	if (::SipGetInfo(&si) == TRUE)
		if (si.fdwFlags & SIPF_ON != 0)
		{
			si.fdwFlags &= ~SIPF_ON;
			::SipSetInfo(&si);
		}
}

CString CGlobals::FormatSize(DWORD nSize)
{
	CString sSize;
	if (nSize < (1 << 10))
	{
		sSize.Format(_T("%d B"), nSize);
	}
	else if (nSize < (1 << 20))
	{
		double nKB = (double)nSize / (1 << 10);
		sSize.Format(_T("%.1f KB"), nKB);
	}
	else if (nSize < (1 << 30))
	{
		double nMB = (double)nSize / (1 << 20);
		sSize.Format(_T("%.1f MB"), nMB);
	}
	else
	{
		double nGB = (double)nSize / (1 << 30);
		sSize.Format(_T("%.1f GB"), nGB);
	}
	return sSize;
}

int CGlobals::FromString(LPCTSTR szValue, int nDefault)
{
	int nRetVal = nDefault;
	if (szValue != NULL)
		_stscanf(szValue, _T("%d"), &nRetVal);
	return nRetVal;
}

bool CGlobals::IsPrint(BYTE c)
{
	if (c >= 0x20 && c <= 0x7e)
		return true;
	return false;
}


CString CGlobals::MacAddress(BYTE *pBuff, int nLen)
{
	CString sResult, s;
	int i;
	for (i = 0; i < nLen; i++)
	{
		if (i > 0)
			sResult += _T('-');
		s.Format(_T("%02X"), pBuff[i]);
		sResult += s;
	}
	return sResult;
}

CString CGlobals::IPAddress(DWORD dwAddr)
{
	CString sResult;
	in_addr dwAddress;
	dwAddress.S_un.S_addr = dwAddr;
	sResult.Format(_T("%hs"), ::inet_ntoa(dwAddress));
	return sResult;
}

/////////////////////////////////////////////////////////////////////////////

CIcon::CIcon()
{
	n_hIcon = NULL;
}

CIcon::~CIcon()
{
	if (n_hIcon != NULL)
		::DestroyIcon(n_hIcon);
}

BOOL CIcon::Load(int ID)
{
	n_hIcon = AfxGetApp()->LoadIcon(ID);
	return (n_hIcon != NULL) ? TRUE : FALSE;
}

BOOL CIcon::Load(int ID, int cxDesired, int cyDesired)
{
	n_hIcon = (HICON)::LoadImage(
		AfxGetApp()->m_hInstance,
		MAKEINTRESOURCE(ID),
		IMAGE_ICON,
		cxDesired,
		cyDesired,
		0);
	return (n_hIcon != NULL) ? TRUE : FALSE;
}
