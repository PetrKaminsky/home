#if !defined(AFX_PROPERTY_H__6E33FD9E_11C3_4550_A49C_9B44B47A8F19__INCLUDED_)
#define AFX_PROPERTY_H__6E33FD9E_11C3_4550_A49C_9B44B47A8F19__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//////////////////////////////////////////////////////////////////////
// CRegPropertySerializer class

class CRegPropertySerializer
{
public:
	CRegPropertySerializer(LPCTSTR szKeyName);

	int LoadInt(LPCTSTR szValueName, int nDefault);
	void SaveInt(LPCTSTR szValueName, int nValue);
	CString LoadString(LPCTSTR szValueName, LPCTSTR szDefault);
	void SaveString(LPCTSTR szValueName, LPCTSTR szValue);
	double LoadDouble(LPCTSTR szValueName, double dDefault);
	void SaveDouble(LPCTSTR szValueName, double dValue);

private:
	CString m_sRootKeyName;
};

/////////////////////////////////////////////////////////////////////////////
// CRegProperty class

class CRegProperty
{
public:
	CRegProperty(
		CRegPropertySerializer *pSerializer,
		LPCTSTR szValueName);

protected:
	CRegPropertySerializer *m_pSerializer;
	CString m_sValueName;
};

/////////////////////////////////////////////////////////////////////////////
// CPropertyInt class

class CPropertyInt
{
public:
	CPropertyInt(int nDefault);

	virtual int GetValue() = 0;
	virtual void SetValue(int nValue) = 0;

protected:
	int m_nDefault;
};

/////////////////////////////////////////////////////////////////////////////
// CRegPropertyInt class

class CRegPropertyInt
:	public CPropertyInt,
	public CRegProperty
{
public:
	CRegPropertyInt(
		CRegPropertySerializer *pSerializer,
		LPCTSTR szValueName,
		int nDefault);

	int GetValue();
	void SetValue(int nValue);
};

/////////////////////////////////////////////////////////////////////////////
// CPropertyString class

class CPropertyString
{
public:
	CPropertyString(LPCTSTR szDefault);

	virtual CString GetValue() = 0;
	virtual void SetValue(LPCTSTR szValue) = 0;

protected:
	LPCTSTR m_szDefault;
};

/////////////////////////////////////////////////////////////////////////////
// CRegPropertyString class

class CRegPropertyString
:	public CPropertyString,
	public CRegProperty
{
public:
	CRegPropertyString(
		CRegPropertySerializer *pSerializer,
		LPCTSTR szValueName,
		LPCTSTR szDefault);

	CString GetValue();
	void SetValue(LPCTSTR szValue);
};

/////////////////////////////////////////////////////////////////////////////
// CPropertyDouble class

class CPropertyDouble
{
public:
	CPropertyDouble(double dDefault);

	virtual double GetValue() = 0;
	virtual void SetValue(double dValue) = 0;

protected:
	double m_dDefault;
};

/////////////////////////////////////////////////////////////////////////////
// CRegPropertyDouble class

class CRegPropertyDouble
:	public CPropertyDouble,
	public CRegProperty
{
public:
	CRegPropertyDouble(
		CRegPropertySerializer *pSerializer,
		LPCTSTR szValueName,
		double dDefault);

	double GetValue();
	void SetValue(double dValue);
};

/////////////////////////////////////////////////////////////////////////////
// CRegPropertyBool class

class CRegPropertyBool
{
public:
	CRegPropertyBool(
		CRegPropertySerializer *pSerializer,
		LPCTSTR szValueName,
		bool bDefault);

	bool GetValue();
	void SetValue(bool bValue);

private:
	CRegPropertyInt m_IntField;

	static bool IntToBool(int nValue);
	static int BoolToInt(bool bValue);
};

/////////////////////////////////////////////////////////////////////////////
//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.
#endif // !defined(AFX_PROPERTY_H__6E33FD9E_11C3_4550_A49C_9B44B47A8F19__INCLUDED_)
