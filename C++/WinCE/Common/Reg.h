#ifndef _REG_H_
#define _REG_H_

/////////////////////////////////////////////////////////////////////////////

class CRegKeyEnum
{
public:
	CRegKeyEnum(HKEY hParentKey);
	virtual ~CRegKeyEnum();

	BOOL Init(LPCTSTR szSubkeyName);
	BOOL GetNext(CString &str);
	void Close();

protected:
	BOOL m_bOpened;
	HKEY m_hParentKey;
	HKEY m_hKey;
	DWORD m_nIndex;
	TCHAR m_szBuffer[256];
};

/////////////////////////////////////////////////////////////////////////////

class CRegValueEnum
{
public:
	CRegValueEnum(HKEY hParentKey);
	virtual ~CRegValueEnum();

	BOOL Init(LPCTSTR szSubkeyName);
	BOOL GetNext(CString &str, DWORD &dwType);
	void Close();

protected:
	BOOL m_bOpened;
	HKEY m_hParentKey;
	HKEY m_hKey;
	DWORD m_nIndex;
	TCHAR m_szBuffer[256];
};

/////////////////////////////////////////////////////////////////////////////

class CRegKeyEx
{
public:
	CRegKeyEx(HKEY hParentKey);
	CRegKeyEx();
	virtual ~CRegKeyEx();

	static BOOL DeleteKey(LPCTSTR szSubkeyName, HKEY hParentKey);
	BOOL DeleteKey(LPCTSTR szSubkeyName);
	static BOOL DeleteValue(LPCTSTR szValueName, HKEY hParentKey);
	BOOL DeleteValue(LPCTSTR szValueName);
	void CloseKey();
	BOOL OpenKey(LPCTSTR szSubkeyName);
	BOOL OpenKey2(LPCTSTR szKeyName);
	BOOL ReadDword(LPCTSTR szValueName, DWORD &dwValue);
	BOOL ReadString(LPCTSTR szValueName, CString &sValue);
	BOOL ReadDouble(LPCTSTR szValueName, double &dValue);
	BOOL ReadBinary(LPCTSTR szValueName, BYTE *pData, DWORD *pdwLen);
	BOOL CreateKey(LPCTSTR szSubkeyName);
	BOOL CreateKey2(LPCTSTR szKeyName);
	BOOL WriteDword(LPCTSTR szValueName, DWORD dwValue);
	BOOL WriteString(LPCTSTR szValueName, LPCTSTR szValue);
	BOOL WriteDouble(LPCTSTR szValueName, double dValue);
	BOOL WriteBinary(LPCTSTR szValueName, BYTE *pData, DWORD dwLen);

protected:
	BOOL m_bOpened;
	HKEY m_hParentKey;
	HKEY m_hKey;

	static HKEY GetParentKey(LPCTSTR szParentKey);
};

/////////////////////////////////////////////////////////////////////////////

class CRegPath  
{
public:
	CRegPath(LPCTSTR szFullPath);
	CRegPath(LPCTSTR szKeyName, LPCTSTR szValueName);

	LPCTSTR GetKeyName() { return m_sKeyName; }
	LPCTSTR GetValueName() { return m_sValueName; }

private:
	CString m_sKeyName;
	CString m_sValueName;

	static CString CreateFullPath(LPCTSTR szKeyName, LPCTSTR szValueName);
	static void ParseFullPath(
		LPCTSTR szFullPath,
		CString &sKeyName,
		CString &sValueName);
};

/////////////////////////////////////////////////////////////////////////////

#endif
