#if !defined(AFX_GLOBALS_H__0768DE1B_B142_4ABC_A46C_87CCF1183A3B__INCLUDED_)
#define AFX_GLOBALS_H__0768DE1B_B142_4ABC_A46C_87CCF1183A3B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////

class CGlobals  
{
private:
	CGlobals() { }

public:
	static CString GetWindowText(CWnd *pWnd);
	static void ResizeControl(CWnd *pCtrl, int nWidth, int nHeight = -1);
	static CString TransformToHexBytes(LPBYTE pBuff, int nBytes);
	static CString TransformToText(LPBYTE pBuff, int nBytes);
	static void PrintLastError(
		LPCTSTR szMethod,
		DWORD dwError = ::GetLastError(),
		LPCTSTR szMessage = NULL,
		LPCTSTR szModule = NULL);
	static CString GetErrorText(DWORD dwError, LPCTSTR szModule = NULL);
	static CString GuidToString(GUID &guid);
	static bool StringToGuid(LPCTSTR szGuid, GUID &guid);
	static CString ToString(int nValue);
	static void SipOff();
	static CString FormatSize(DWORD nSize);
	static int FromString(LPCTSTR szValue, int nDefault = 0);
	static bool IsPrint(BYTE c);
	static CString MacAddress(BYTE *pBuff, int nLen);
	static CString IPAddress(DWORD dwAddr);
};

/////////////////////////////////////////////////////////////////////////////

class CIcon
{
public:
	CIcon();
	~CIcon();

	BOOL Load(int ID);
	BOOL Load(int ID, int cxDesired, int cyDesired);
	operator HICON () { return n_hIcon; }

private:
	HICON n_hIcon;
};

/////////////////////////////////////////////////////////////////////////////
//{{AFX_INSERT_LOCATION}}
// Microsoft eMbedded Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GLOBALS_H__0768DE1B_B142_4ABC_A46C_87CCF1183A3B__INCLUDED_)
