#if !defined(AFX_SOCKET_H__86D6BB27_5209_4152_8390_C147D020AAF0__INCLUDED_)
#define AFX_SOCKET_H__86D6BB27_5209_4152_8390_C147D020AAF0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////

class CWinSock
{
public:
	CWinSock();
	virtual ~CWinSock();

	bool Close();
	bool Create(
		int nAddrFamily = AF_INET,
		int type = SOCK_STREAM,
		int proto = IPPROTO_TCP);
	static int GetLastError();
	bool Shutdown(int how = SD_BOTH);
	virtual bool Bind(const SOCKADDR *lpSockAddr, int nSockAddrLen);
	bool Bind(short nPort = 0, int nAddrFamily = AF_INET);
	bool GetPeerName(CString &sAddress, short &nPort);
	virtual bool GetPeerName(SOCKADDR *lpSockAddr, int *lpSockAddrLen);
	bool GetSockName(CString &sAddress, short &nPort);
	virtual bool GetSockName(SOCKADDR *lpSockAddr, int *lpSockAddrLen);
	virtual bool Connect(const SOCKADDR *lpSockAddr, int nSockAddrLen);
	bool Connect(
		LPCTSTR szAddress,
		short nPort,
		int nAddrFamily = AF_INET);
	CWinSock *Accept(CString &sAddress, short &nPort);
	virtual CWinSock *Accept(SOCKADDR *lpSockAddr, int *lpSockAddrLen);
	virtual int Receive(
		void *lpBuf,
		int nBufLen,
		int nFlags = 0);
	virtual int Send(
		const void *lpBuf,
		int nBufLen,
		int nFlags = 0);
	void AsyncSelect(
		HWND hWnd,
		UINT nMsgId,
		long nEvents = FD_READ | FD_WRITE | FD_ACCEPT | FD_CONNECT | FD_CLOSE);
	bool Listen(int nBacklog = SOMAXCONN);
	SOCKET GetHandle() { return m_hSock; }
	bool Ioctl(int cmd, int *argp);
	bool SetBlocking(bool bBlock = true);
	bool SetNonBlocking();
	int GetAvailableReadCount();
	static CString GetAddrString(LPSOCKADDR pAddr, DWORD dwAddrlen);
	static void PrintWinsockError(
		LPCTSTR szMethod,
		DWORD dwError = ::WSAGetLastError(),
		LPCTSTR szMessage = NULL);

protected:
	CWinSock(SOCKET s);

	SOCKET m_hSock;
	long m_nLastEvents;

	virtual CWinSock *CreateSock(SOCKET s);

private:
	typedef union SOCKADDR_GEN
	{
		SOCKADDR Address;
		SOCKADDR_IN AddressIn;
		SOCKADDR_IN6 AddressIn6;
	};

	static void GetAddrPort(
		SOCKADDR *lpSockAddr, 
		int nSockAddrLen,
		CString &sAddress,
		short &nPort);
	static bool GetSockAddr(
		LPCTSTR szAddress,
		short nPort,
		int nAddrFamily,
		int nFlags,
		SOCKADDR *lpSockAddr,
		int *lpSockAddrLen);
/*
	static CString ExtractOnlyAddress(LPCTSTR szAddress);
*/
};

/////////////////////////////////////////////////////////////////////////////

#include <map>

typedef std::map<WSAEVENT, SOCKET> TEventMap;
typedef TEventMap::iterator TEventMapIterator;
typedef struct
{
	WSAEVENT hEvent;
	HWND hWnd;
	UINT nMsgId;
} TSockData;
typedef std::map<SOCKET, TSockData> TSocketMap;
typedef TSocketMap::iterator TSocketMapIterator;
typedef TSocketMap::value_type TSocketMapValueType;

class CWinSockWorker
{
public:
	class CEventMap : public TEventMap
	{
	public:
		~CEventMap();
	};

	class CWinSockMutex
	{
	public:
		CWinSockMutex();
		~CWinSockMutex();

		void Lock();
		void Unlock();

	private:
		HANDLE m_hMutex;
	};

	static CWinSockWorker *GetInstance();
	static void StopWorker();
	static void EnqueRequest(
		SOCKET s,
		HWND hWnd,
		UINT nMsgId,
		long nEvents);

private:
	WSAEVENT m_hStopEvent;
	WSAEVENT m_hRingEvent;
	HANDLE m_hConfStopEvent;
	static CWinSockWorker *s_pInstance;
	CEventMap m_Events;
	CWinSockMutex m_EventMutex;
	TSocketMap m_Sockets;
	CWinSockMutex m_SocketMutex;

	CWinSockWorker();
	~CWinSockWorker();

	static DWORD ThreadProc(LPVOID lpParameter);
	void Stop();
	void Init();
	void Ring();
	bool StartThread();
	DWORD ThreadLoop();
	void Enque(
		SOCKET s,
		HWND hWnd,
		UINT nMsgId,
		long nEvents);
};

/////////////////////////////////////////////////////////////////////////////

class CWinSockBuffer
{
public:
	CWinSockBuffer();
	~CWinSockBuffer();

	bool IsEmpty();
	LPBYTE GetData();
	int GetDataLen();
	void AppendData(const LPBYTE pData, int nLen);
	void RemoveHeadCount(int nLen);
	void Clear();

private:
	BYTE *m_pData;
	int m_nTotalLen;
	int m_nDataLen;
};

/////////////////////////////////////////////////////////////////////////////
//{{AFX_INSERT_LOCATION}}
// Microsoft eMbedded Visual C++ will insert additional declarations immediately before the previous line.
#endif // !defined(AFX_SOCKET_H__86D6BB27_5209_4152_8390_C147D020AAF0__INCLUDED_)
