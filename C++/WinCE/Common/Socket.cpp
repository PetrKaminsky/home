#include "stdafx.h"
#include "Socket.h"
#include "Globals.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////

CWinSock::CWinSock()
{
	m_hSock = INVALID_SOCKET;
	m_nLastEvents = 0;
}

CWinSock::CWinSock(SOCKET s)
{
	m_hSock = s;
}

CWinSock::~CWinSock()
{
	Close();
}

bool CWinSock::Close()
{
	AsyncSelect(NULL, 0, 0);
	bool bResult = true;
	if (m_hSock != INVALID_SOCKET)
	{
		bResult = ::closesocket(m_hSock) != SOCKET_ERROR;
		m_hSock = INVALID_SOCKET;
	}
	return bResult;
}

bool CWinSock::Create(
	int nAddrFamily,
	int type,
	int proto)
{
	m_hSock = ::socket(nAddrFamily, type, proto);
	return m_hSock != INVALID_SOCKET;
}

int CWinSock::GetLastError()
{
	return WSAGetLastError();
}

bool CWinSock::Shutdown(int how)
{
	return ::shutdown(m_hSock, how) != SOCKET_ERROR;
}

bool CWinSock::Bind(const SOCKADDR *lpSockAddr, int nSockAddrLen)
{
	return ::bind(m_hSock, lpSockAddr, nSockAddrLen) != SOCKET_ERROR;
}

bool CWinSock::Bind(short nPort, int nAddrFamily)
{
	bool retVal = false;
	SOCKADDR_GEN sa;
	int saLen = sizeof(sa);
	ZeroMemory(&sa, saLen);
	if (GetSockAddr(
		NULL,
		nPort,
		nAddrFamily,
		AI_PASSIVE,
		(SOCKADDR *)&sa,
		&saLen))
	{
		retVal = Bind((SOCKADDR *)&sa, saLen);
	}
	return retVal;
}

bool CWinSock::GetPeerName(CString &sAddress, short &nPort)
{
	SOCKADDR_GEN sa;
	int saLen = sizeof(sa);
	ZeroMemory(&sa, saLen);
	bool bResult = GetPeerName((SOCKADDR *)&sa, &saLen);
	if (bResult)
	{
		GetAddrPort((SOCKADDR *)&sa, saLen, sAddress, nPort);
	}
	return bResult;
}

bool CWinSock::GetPeerName(SOCKADDR *lpSockAddr, int *lpSockAddrLen)
{
	return ::getpeername(m_hSock, lpSockAddr, lpSockAddrLen) != SOCKET_ERROR;
}

bool CWinSock::GetSockName(CString &sAddress, short &nPort)
{
	SOCKADDR_GEN sa;
	int saLen = sizeof(sa);
	ZeroMemory(&sa, saLen);
	bool bResult = GetSockName((SOCKADDR *)&sa, &saLen);
	if (bResult)
	{
		GetAddrPort((SOCKADDR *)&sa, saLen, sAddress, nPort);
	}
	return bResult;
}

bool CWinSock::GetSockName(SOCKADDR *lpSockAddr, int *lpSockAddrLen)
{
	return ::getsockname(m_hSock, lpSockAddr, lpSockAddrLen) != SOCKET_ERROR;
}

bool CWinSock::Connect(const SOCKADDR *lpSockAddr, int nSockAddrLen)
{
	return ::connect(m_hSock, lpSockAddr, nSockAddrLen) != SOCKET_ERROR;
}

bool CWinSock::Connect(
	LPCTSTR szAddress,
	short nPort,
	int nAddrFamily)
{
	bool retVal = false;
	SOCKADDR_GEN sa;
	int saLen = sizeof(sa);
	ZeroMemory(&sa, saLen);
	if (GetSockAddr(
		szAddress,
		nPort,
		nAddrFamily,
		0,
		(SOCKADDR *)&sa,
		&saLen))
	{
		retVal = Connect((SOCKADDR *)&sa, saLen);
	}
	return retVal;
}

CWinSock *CWinSock::Accept(CString &sAddress, short &nPort)
{
	SOCKADDR_GEN sa;
	int saLen = sizeof(sa);
	ZeroMemory(&sa, saLen);
	CWinSock *pSock = Accept((SOCKADDR *)&sa, &saLen);
	if (pSock != NULL)
	{
		GetAddrPort((SOCKADDR *)&sa, saLen, sAddress, nPort);
	}
	return pSock;
}

CWinSock *CWinSock::Accept(SOCKADDR *lpSockAddr, int *lpSockAddrLen)
{
	CWinSock *pSock = NULL;
	SOCKET s = ::accept(m_hSock, lpSockAddr, lpSockAddrLen);
	if (s != INVALID_SOCKET)
	{
		pSock = CreateSock(s);
	}
	return pSock;
}

int CWinSock::Receive(
	void *lpBuf,
	int nBufLen,
	int nFlags)
{
	return ::recv(m_hSock, (LPSTR)lpBuf, nBufLen, nFlags);
}

int CWinSock::Send(
	const void *lpBuf,
	int nBufLen,
	int nFlags)
{
	return ::send(m_hSock, (LPCSTR)lpBuf, nBufLen, nFlags);
}

void CWinSock::AsyncSelect(
	HWND hWnd,
	UINT nMsgId,
	long nEvents)
{
	if (m_nLastEvents != nEvents)
	{
		CWinSockWorker::EnqueRequest(
			m_hSock,
			hWnd,
			nMsgId,
			nEvents);
		m_nLastEvents = nEvents;
	}
}

bool CWinSock::Listen(int nBacklog)
{
	return ::listen(m_hSock, nBacklog) != SOCKET_ERROR;
}

CWinSock *CWinSock::CreateSock(SOCKET s)
{
	return new CWinSock(s);
}

bool CWinSock::Ioctl(int cmd, int *argp)
{
	return ::ioctlsocket(m_hSock, cmd, (u_long *)argp) != SOCKET_ERROR;
}

bool CWinSock::SetBlocking(bool bBlock)
{
	int arg = bBlock ? 0 : 1;
	return Ioctl(FIONBIO, &arg);
}

bool CWinSock::SetNonBlocking()
{
	return SetBlocking(false);
}

int CWinSock::GetAvailableReadCount()
{
	int nResult = 0;
	return Ioctl(FIONREAD, &nResult) ? nResult : -1;
}

void CWinSock::GetAddrPort(
	SOCKADDR *lpSockAddr, 
	int nSockAddrLen,
	CString &sAddress,
	short &nPort)
{
	nPort = ::ntohs(SS_PORT(lpSockAddr));
	sAddress = GetAddrString(lpSockAddr, nSockAddrLen);
}

CString CWinSock::GetAddrString(LPSOCKADDR pAddr, DWORD dwAddrlen)
{
	CString sResult;
/*
	int nAddrStringLen = 1;
	LPTSTR szAddrString = new TCHAR[nAddrStringLen + 1];
	DWORD dwErr = 0;
	if (::WSAAddressToString(
		pAddr,
		dwAddrlen,
		NULL,
		szAddrString,
		(LPDWORD)&nAddrStringLen) == SOCKET_ERROR)
	{
		dwErr = ::WSAGetLastError();
		if (dwErr == WSAEFAULT)
		{
			delete []szAddrString;
			szAddrString = new TCHAR[nAddrStringLen + 1];
			if (::WSAAddressToString(
				pAddr,
				dwAddrlen,
				NULL,
				szAddrString,
				(LPDWORD)&nAddrStringLen) != SOCKET_ERROR)
			{
				sResult = ExtractOnlyAddress(szAddrString);
				dwErr = 0;
			}
		}
	}
	if (dwErr != 0)
	{
		PrintWinsockError(_T("WSAAddressToString()"), dwErr);
	}
	if (szAddrString != NULL)
		delete []szAddrString;
*/
	char nameBuff[NI_MAXHOST + 1];
	if (::getnameinfo(
		pAddr,
		dwAddrlen,
		nameBuff,
		NI_MAXHOST,
		NULL,
		0,
		NI_NUMERICHOST) == 0)
	{
		sResult.Format(_T("%hs"), nameBuff);
	}
	else
	{
		PrintWinsockError(_T("getnameinfo()"));
	}
	return sResult;
}

bool CWinSock::GetSockAddr(
	LPCTSTR szAddress,
	short nPort,
	int nAddrFamily,
	int nFlags,
	SOCKADDR *lpSockAddr,
	int *lpSockAddrLen)
{
	bool retVal = false;
	char szNode[NI_MAXHOST + 1];
	if (szAddress != NULL)
		wcstombs(szNode, (LPCTSTR)szAddress, sizeof(szNode));
	char szServ[7];
	sprintf(szServ, "%hu", nPort);
	ADDRINFO hints, *res = NULL;
	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = nAddrFamily;
	hints.ai_flags = nFlags;
	if (::getaddrinfo(
		(szAddress != NULL) ? szNode : NULL,
		szServ,
		&hints,
		&res))
	{
		PrintWinsockError(_T("getaddrinfo()"));
	}
	else
	{
		if (res != NULL)
		{
			if (lpSockAddr != NULL &&
				lpSockAddrLen != NULL &&
				*lpSockAddrLen >= res->ai_addrlen)
			{
				CopyMemory(lpSockAddr, res->ai_addr, res->ai_addrlen);
				*lpSockAddrLen = res->ai_addrlen;
				retVal = true;
			}
		}
		::freeaddrinfo(res);
	}
	return retVal;
}

/*
CString CWinSock::ExtractOnlyAddress(LPCTSTR szAddress)
{
	CString retVal(szAddress);
	int nColonInd = retVal.ReverseFind(_T(':'));
	if (nColonInd != -1)
		retVal = retVal.Left(nColonInd);
	if (retVal[0] == _T('['))
		retVal.Delete(0);
	if (retVal[retVal.GetLength() - 1] == _T(']'))
		retVal.Delete(retVal.GetLength() - 1);
	return retVal;
}
*/

void CWinSock::PrintWinsockError(
	LPCTSTR szMethod,
	DWORD dwError,
	LPCTSTR szMessage)
{
	CGlobals::PrintLastError(
		szMethod,
		dwError,
		szMessage,
		_T("ws2.dll"));
}

/////////////////////////////////////////////////////////////////////////////

CWinSockWorker::CWinSockWorker()
{
	m_hStopEvent = NULL;
	m_hRingEvent = NULL;
	m_hConfStopEvent = NULL;
}

CWinSockWorker::~CWinSockWorker()
{
	if (m_hStopEvent != NULL)
	{
		::WSACloseEvent(m_hStopEvent);
		m_hStopEvent = NULL;
	}
	if (m_hRingEvent != NULL)
	{
		::WSACloseEvent(m_hRingEvent);
		m_hRingEvent = NULL;
	}
	if (m_hConfStopEvent != NULL)
	{
		::CloseHandle(m_hConfStopEvent);
		m_hConfStopEvent = NULL;
	}
}

void CWinSockWorker::Ring()
{
	::WSASetEvent(m_hRingEvent);
}

void CWinSockWorker::Stop()
{
	::WSASetEvent(m_hStopEvent);
	::WaitForSingleObject(m_hConfStopEvent, INFINITE);
}

CWinSockWorker *CWinSockWorker::s_pInstance = NULL;

bool CWinSockWorker::StartThread()
{
	HANDLE hThread = ::CreateThread(
		NULL,
		0,
		CWinSockWorker::ThreadProc,
		this,
		0,
		NULL);
	if (hThread != NULL)
	{
		::CloseHandle(hThread);
		return true;
	}
	return false;
}

DWORD CWinSockWorker::ThreadProc(LPVOID lpParameter)
{
	CWinSockWorker *pWorker = (CWinSockWorker *)lpParameter;
	return pWorker->ThreadLoop();
}

DWORD CWinSockWorker::ThreadLoop()
{
	while (true)
	{
		m_EventMutex.Lock();

		int nEventCount = m_Events.size();
		int nTotalCount = nEventCount + 2;
		HANDLE *pHandles = new HANDLE[nTotalCount];
		pHandles[0] = m_hStopEvent;
		pHandles[1] = m_hRingEvent;
		TEventMapIterator itEvt = m_Events.begin();
		int i = 0;
		for (; i < nEventCount && itEvt != m_Events.end(); i++, itEvt++)
			pHandles[i + 2] = itEvt->first;

		m_EventMutex.Unlock();

		DWORD dwResult = ::WaitForMultipleObjects(
			nTotalCount,
			pHandles,
			FALSE,
			INFINITE);
		switch (dwResult)
		{
		case WAIT_OBJECT_0:
			::SetEvent(m_hConfStopEvent);
			delete []pHandles;
			::ExitThread(0);
			break;

		case WAIT_OBJECT_0 + 1:
			::ResetEvent(m_hRingEvent);
			break;

		default:
			if (dwResult > (WAIT_OBJECT_0 + 1) &&
				dwResult <= (WAIT_OBJECT_0 + nTotalCount - 1))
			{
				HANDLE hEvent = pHandles[dwResult - WAIT_OBJECT_0];

				m_SocketMutex.Lock();
				m_EventMutex.Lock();

				itEvt = m_Events.find(hEvent);
				if (itEvt != m_Events.end())
				{
					SOCKET s = itEvt->second;
					TSocketMapIterator itSock = m_Sockets.find(s);
					if (itSock != m_Sockets.end())
					{
						WSANETWORKEVENTS ne;
						ZeroMemory(&ne, sizeof(ne));
						if (::WSAEnumNetworkEvents(s, hEvent, &ne) != SOCKET_ERROR)
						{
							HWND hWnd = itSock->second.hWnd;
							UINT nMsgId = itSock->second.nMsgId;
							if ((ne.lNetworkEvents & FD_READ) != 0)
								::PostMessage(hWnd, nMsgId, s, MAKELPARAM(FD_READ, ne.iErrorCode[FD_READ_BIT]));
							if ((ne.lNetworkEvents & FD_WRITE) != 0)
								::PostMessage(hWnd, nMsgId, s, MAKELPARAM(FD_WRITE, ne.iErrorCode[FD_WRITE_BIT]));
							if ((ne.lNetworkEvents & FD_ACCEPT) != 0)
								::PostMessage(hWnd, nMsgId, s, MAKELPARAM(FD_ACCEPT, ne.iErrorCode[FD_ACCEPT_BIT]));
							if ((ne.lNetworkEvents & FD_CONNECT) != 0)
								::PostMessage(hWnd, nMsgId, s, MAKELPARAM(FD_CONNECT, ne.iErrorCode[FD_CONNECT_BIT]));
							if ((ne.lNetworkEvents & FD_CLOSE) != 0)
								::PostMessage(hWnd, nMsgId, s, MAKELPARAM(FD_CLOSE, ne.iErrorCode[FD_CLOSE_BIT]));
						}
					}
				}

				m_EventMutex.Unlock();
				m_SocketMutex.Unlock();
			}
		}
		delete []pHandles;
	}
}

CWinSockWorker *CWinSockWorker::GetInstance()
{
	if (CWinSockWorker::s_pInstance == NULL)
	{
		CWinSockWorker::s_pInstance = new CWinSockWorker;
		CWinSockWorker::s_pInstance->Init();
		CWinSockWorker::s_pInstance->StartThread();
	}
	return CWinSockWorker::s_pInstance;
}

void CWinSockWorker::StopWorker()
{
	if (CWinSockWorker::s_pInstance != NULL)
		CWinSockWorker::GetInstance()->Stop();
	delete CWinSockWorker::s_pInstance;
	CWinSockWorker::s_pInstance = NULL;
}

void CWinSockWorker::Init()
{
	m_hStopEvent = ::WSACreateEvent();
	m_hRingEvent = ::WSACreateEvent();
	m_hConfStopEvent = ::CreateEvent(NULL, TRUE, FALSE, NULL);
}

void CWinSockWorker::EnqueRequest(
	SOCKET s,
	HWND hWnd,
	UINT nMsgId,
	long nEvents)
{
	CWinSockWorker::GetInstance()->Enque(
		s,
		hWnd,
		nMsgId,
		nEvents);
}

void CWinSockWorker::Enque(
	SOCKET s,
	HWND hWnd,
	UINT nMsgId,
	long nEvents)
{
	m_SocketMutex.Lock();
	m_EventMutex.Lock();

	TSocketMapIterator itSock = m_Sockets.find(s);
	if (itSock != m_Sockets.end())
	{
		if (nEvents == 0)
		{
			TEventMapIterator itEvt = m_Events.find(itSock->second.hEvent);
			if (itEvt != m_Events.end())
			{
				::WSACloseEvent(itEvt->first);
				m_Events.erase(itEvt);
			}
			m_Sockets.erase(itSock);

			Ring();
		}
		else
			::WSAEventSelect(s, itSock->second.hEvent, nEvents);
	}
	else
	{
		WSAEVENT hEvent = ::WSACreateEvent();
		TSockData sd =
		{
			hEvent,
			hWnd,
			nMsgId
		};
		::WSAEventSelect(s, hEvent, nEvents);
		m_Sockets[s] = sd;
		m_Events[hEvent] = s;

		Ring();
	}

	m_EventMutex.Unlock();
	m_SocketMutex.Unlock();
}

/////////////////////////////////////////////////////////////////////////////

CWinSockWorker::CEventMap::~CEventMap()
{
	TEventMapIterator it = begin();
	for (; it != end(); it++)
		::WSACloseEvent(it->first);
	clear();
}

/////////////////////////////////////////////////////////////////////////////

std::_Lockit::_Lockit()
{
}

std::_Lockit::~_Lockit()
{
}

/////////////////////////////////////////////////////////////////////////////

CWinSockWorker::CWinSockMutex::CWinSockMutex()
{
	m_hMutex = ::CreateMutex(NULL, FALSE, NULL);
}

CWinSockWorker::CWinSockMutex::~CWinSockMutex()
{
	::CloseHandle(m_hMutex);
}

void CWinSockWorker::CWinSockMutex::Lock()
{
	::WaitForSingleObject(m_hMutex, INFINITE);
}

void CWinSockWorker::CWinSockMutex::Unlock()
{
	::ReleaseMutex(m_hMutex);
}

/////////////////////////////////////////////////////////////////////////////

CWinSockBuffer::CWinSockBuffer()
{
	m_pData = NULL;
	m_nTotalLen = 0;
	m_nDataLen = 0;
}

CWinSockBuffer::~CWinSockBuffer()
{
	if (m_pData != NULL)
		delete []m_pData;
}

bool CWinSockBuffer::IsEmpty()
{
	return m_nDataLen == 0;
}

LPBYTE CWinSockBuffer::GetData()
{
	return m_pData;
}

int CWinSockBuffer::GetDataLen()
{
	return m_nDataLen;
}

void CWinSockBuffer::AppendData(const LPBYTE pData, int nLen)
{
	if (m_pData == NULL)
	{
		m_pData = new BYTE[nLen];
		CopyMemory(m_pData, pData, nLen);
		m_nTotalLen = m_nDataLen = nLen;
	}
	else if ((m_nDataLen + nLen) <= m_nTotalLen)
	{
		CopyMemory(m_pData + m_nDataLen, pData, nLen);
		m_nDataLen += nLen;
	}
	else
	{
		m_nTotalLen = m_nDataLen + nLen;
		LPBYTE pTemp = new BYTE[m_nTotalLen];
		CopyMemory(pTemp, m_pData, m_nDataLen);
		CopyMemory(pTemp + m_nDataLen, pData, nLen);
		delete []m_pData;
		m_pData = pTemp;
		m_nDataLen = m_nTotalLen;
	}
}

void CWinSockBuffer::RemoveHeadCount(int nLen)
{
	MoveMemory(m_pData, m_pData + nLen, m_nDataLen - nLen);
	m_nDataLen -= nLen;
}

void CWinSockBuffer::Clear()
{
	m_nDataLen = 0;
}
