#include "stdafx.h"
#include "Property.h"
#include "Reg.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// CRegPropertySerializer class.

CRegPropertySerializer::CRegPropertySerializer(LPCTSTR szKeyName)
:	m_sRootKeyName(szKeyName)
{
}

int CRegPropertySerializer::LoadInt(LPCTSTR szValueName, int nDefault)
{
	CRegPath path(m_sRootKeyName, szValueName);
	CRegKeyEx key;
	if (key.OpenKey2(path.GetKeyName()))
	{
		int nResult;
		if (key.ReadDword(path.GetValueName(), (DWORD &)nResult))
			return nResult;
	}
	return nDefault;
}

void CRegPropertySerializer::SaveInt(LPCTSTR szValueName, int nValue)
{
	CRegPath path(m_sRootKeyName, szValueName);
	CRegKeyEx key;
	if (key.CreateKey2(path.GetKeyName()))
		key.WriteDword(path.GetValueName(), nValue);
}

CString CRegPropertySerializer::LoadString(LPCTSTR szValueName, LPCTSTR szDefault)
{
	CRegPath path(m_sRootKeyName, szValueName);
	CRegKeyEx key;
	if (key.OpenKey2(path.GetKeyName()))
	{
		CString sResult;
		if (key.ReadString(path.GetValueName(), sResult))
			return sResult;
	}
	return szDefault;
}

void CRegPropertySerializer::SaveString(LPCTSTR szValueName, LPCTSTR szValue)
{
	CRegPath path(m_sRootKeyName, szValueName);
	CRegKeyEx key;
	if (key.CreateKey2(path.GetKeyName()))
		key.WriteString(path.GetValueName(), szValue);
}

double CRegPropertySerializer::LoadDouble(LPCTSTR szValueName, double dDefault)
{
	CRegPath path(m_sRootKeyName, szValueName);
	CRegKeyEx key;
	if (key.OpenKey2(path.GetKeyName()))
	{
		double dResult;
		if (key.ReadDouble(path.GetValueName(), dResult))
			return dResult;
	}
	return dDefault;
}

void CRegPropertySerializer::SaveDouble(LPCTSTR szValueName, double dValue)
{
	CRegPath path(m_sRootKeyName, szValueName);
	CRegKeyEx key;
	if (key.CreateKey2(path.GetKeyName()))
		key.WriteDouble(path.GetValueName(), dValue);
}

/////////////////////////////////////////////////////////////////////////////
// CRegProperty class

CRegProperty::CRegProperty(
	CRegPropertySerializer *pSerializer,
	LPCTSTR szValueName)
:	m_sValueName(szValueName)
{
	m_pSerializer = pSerializer;
}

//////////////////////////////////////////////////////////////////////
// CPropertyInt Class

CPropertyInt::CPropertyInt(int nDefault)
{
	m_nDefault = nDefault;
}

//////////////////////////////////////////////////////////////////////
// CRegPropertyInt Class

CRegPropertyInt::CRegPropertyInt(
	CRegPropertySerializer *pSerializer,
	LPCTSTR szValueName,
	int nDefault)
:	CPropertyInt(nDefault),
	CRegProperty(pSerializer, szValueName)
{
}

int CRegPropertyInt::GetValue()
{
	return m_pSerializer->LoadInt(m_sValueName, m_nDefault);
}

void CRegPropertyInt::SetValue(int nValue)
{
	m_pSerializer->SaveInt(m_sValueName, nValue);
}

//////////////////////////////////////////////////////////////////////
// CPropertyString Class

CPropertyString::CPropertyString(LPCTSTR szDefault)
{
	m_szDefault = szDefault;
}

//////////////////////////////////////////////////////////////////////
// CRegPropertyString Class

CRegPropertyString::CRegPropertyString(
	CRegPropertySerializer *pSerializer,
	LPCTSTR szValueName,
	LPCTSTR szDefault)
:	CPropertyString(szDefault),
	CRegProperty(pSerializer, szValueName)
{
}

CString CRegPropertyString::GetValue()
{
	return m_pSerializer->LoadString(m_sValueName, m_szDefault);
}

void CRegPropertyString::SetValue(LPCTSTR szValue)
{
	m_pSerializer->SaveString(m_sValueName, szValue);
}

//////////////////////////////////////////////////////////////////////
// CPropertyDouble Class

CPropertyDouble::CPropertyDouble(double dDefault)
{
	m_dDefault = dDefault;
}

//////////////////////////////////////////////////////////////////////
// CRegPropertyDouble Class

CRegPropertyDouble::CRegPropertyDouble(
	CRegPropertySerializer *pSerializer,
	LPCTSTR szValueName,
	double dDefault)
:	CPropertyDouble(dDefault),
	CRegProperty(pSerializer, szValueName)
{
}

double CRegPropertyDouble::GetValue()
{
	return m_pSerializer->LoadDouble(m_sValueName, m_dDefault);
}

void CRegPropertyDouble::SetValue(double dValue)
{
	m_pSerializer->SaveDouble(m_sValueName, dValue);
}

//////////////////////////////////////////////////////////////////////
// CRegPropertyBool Class

CRegPropertyBool::CRegPropertyBool(
	CRegPropertySerializer *pSerializer,
	LPCTSTR szValueName,
	bool bDefault)
:	m_IntField(
		pSerializer,
		szValueName,
		BoolToInt(bDefault))
{
}

bool CRegPropertyBool::GetValue()
{
	return IntToBool(m_IntField.GetValue());
}

void CRegPropertyBool::SetValue(bool bValue)
{
	m_IntField.SetValue(BoolToInt(bValue));
}

bool CRegPropertyBool::IntToBool(int nValue)
{
	return nValue != 0;
}

int CRegPropertyBool::BoolToInt(bool bValue)
{
	return bValue ? 1 : 0;
}
