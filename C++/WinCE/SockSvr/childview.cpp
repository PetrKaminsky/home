#include "stdafx.h"
#include "SockSvr.h"
#include "ChildView.h"
#include "Globals.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CChldView

CChldView::CChldView()
:	CFormView(-1)
{
	m_nID = -1;
}

CChldView::CChldView(int nID)
:	CFormView(nID)
{
	m_nID = nID;
}

int CChldView::Create(CWnd *pParent)
{
	return CFormView::Create(NULL, NULL, WS_CHILD | WS_VISIBLE,
		CRect(0, 0, 0, 0), pParent, m_nID, NULL);
}

/////////////////////////////////////////////////////////////////////////////
// CServerForm

#define WM_SOCKET (WM_USER + 1)

CServerForm::CServerForm()
:	CChldView(CServerForm::IDD),
	m_Column0Width(CSockSvrApp::s_pPropertySerializer, _T("Column0"), 100),
	m_Column1Width(CSockSvrApp::s_pPropertySerializer, _T("Column1"), 100),
	m_LastPort(CSockSvrApp::s_pPropertySerializer, _T("Port"), 0),
	m_LastIpv4(CSockSvrApp::s_pPropertySerializer, _T("Ipv4"), true),
	m_LastIpv6(CSockSvrApp::s_pPropertySerializer, _T("Ipv6"), false),
	m_LastEcho(CSockSvrApp::s_pPropertySerializer, _T("Echo"), true)
{
	//{{AFX_DATA_INIT(CServerForm)
	m_nPort = 0;
	//}}AFX_DATA_INIT
	m_bListening = false;
}

void CServerForm::DoDataExchange(CDataExchange* pDX)
{
	CChldView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CServerForm)
	DDX_Control(pDX, IDC_CHECK_ECHO, m_ctrlEcho);
	DDX_Control(pDX, IDC_EDIT_PORT, m_ctrlPort);
	DDX_Control(pDX, IDC_LIST_MESSAGE, m_ctrlMessage);
	DDX_Control(pDX, IDC_BUTTON_LISTEN, m_ctrlListen);
	DDX_Text(pDX, IDC_EDIT_PORT, m_nPort);
	DDV_MinMaxUInt(pDX, m_nPort, 0, 65535);
	DDX_Control(pDX, IDC_RADIO_IPV4, m_ctrlIpv4);
	DDX_Control(pDX, IDC_RADIO_IPV6, m_ctrlIpv6);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CServerForm, CChldView)
	//{{AFX_MSG_MAP(CServerForm)
	ON_WM_DESTROY()
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_BUTTON_LISTEN, OnButtonListen)
	//}}AFX_MSG_MAP
	ON_MESSAGE(WM_SOCKET, OnSocketMessage)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CServerForm message handlers

void CServerForm::OnInitialUpdate()
{
	CChldView::OnInitialUpdate();

	m_ctrlMessage.InsertColumn(0, CString((LPCTSTR)IDS_MESSAGE_COLUMN0), LVCFMT_LEFT, m_Column0Width.GetValue());
	m_ctrlMessage.InsertColumn(1, CString((LPCTSTR)IDS_MESSAGE_COLUMN1), LVCFMT_LEFT, m_Column1Width.GetValue());
	m_ctrlMessage.SetExtendedStyle(m_ctrlMessage.GetExtendedStyle() | LVS_EX_FULLROWSELECT | LVS_EX_HEADERDRAGDROP);
	m_nPort = m_LastPort.GetValue();
	m_ctrlIpv4.SetCheck(m_LastIpv4.GetValue() ? BST_CHECKED : BST_UNCHECKED);
	m_ctrlIpv6.SetCheck(m_LastIpv6.GetValue() ? BST_CHECKED : BST_UNCHECKED);
	m_ctrlEcho.SetCheck(m_LastEcho.GetValue() ? BST_CHECKED : BST_UNCHECKED);
	UpdateData(FALSE);

	UpdateControls();

	m_ctrlPort.SetFocus();
}

void CServerForm::OnDestroy()
{
	BackupProperties();

	CChldView::OnDestroy();
}

void CServerForm::OnSize(UINT nType, int cx, int cy)
{
	CChldView::OnSize(nType, cx, cy);

	if (cx == 0 &&
		cy == 0)
	{
		return;
	}

	if (m_ctrlMessage.GetSafeHwnd() == NULL)
	{
		return;
	}

	CRect r;
	GetClientRect(&r);

	CGlobals::ResizeControl(&m_ctrlMessage, r.right - 1, r.bottom - 1);
}

void CServerForm::UpdateControls()
{
	m_ctrlPort.EnableWindow(m_bListening ? FALSE : TRUE);
	m_ctrlListen.EnableWindow(m_bListening ? FALSE : TRUE);
	m_ctrlIpv4.EnableWindow(m_bListening ? FALSE : TRUE);
	m_ctrlIpv6.EnableWindow(m_bListening ? FALSE : TRUE);
}

void CServerForm::BackupProperties()
{
	m_Column0Width.SetValue(m_ctrlMessage.GetColumnWidth(0));
	m_Column1Width.SetValue(m_ctrlMessage.GetColumnWidth(1));
	m_LastPort.SetValue(m_nPort);
	m_LastIpv4.SetValue(m_ctrlIpv4.GetCheck() == BST_CHECKED);
	m_LastIpv6.SetValue(m_ctrlIpv6.GetCheck() == BST_CHECKED);
	m_LastEcho.SetValue(m_ctrlEcho.GetCheck() == BST_CHECKED);
}

LRESULT CServerForm::OnSocketMessage(WPARAM wParam, LPARAM lParam)
{
	SOCKET sock = (SOCKET)wParam;
	int nErrCode = WSAGETSELECTERROR(lParam);
	int nEvent = WSAGETSELECTEVENT(lParam);
	switch (nEvent)
	{
	case FD_ACCEPT:
		OnAccept(nErrCode);
		break;

	case FD_READ:
		OnReceive(sock, nErrCode);
		break;

	case FD_WRITE:
		OnSend(sock, nErrCode);
		break;

	case FD_CLOSE:
		OnClose(sock, nErrCode);
		break;
	}
	return (LRESULT)0;
}

void CServerForm::OnButtonListen()
{
	if (m_bListening)
		::AfxMessageBox(IDS_ALREADY_LISTENING);
	else
	{
		if (UpdateData())
		{
			CString s;
			int af = GetAddrFamily();
			if (m_SrvSock.Create(af))
			{
				if (m_SrvSock.Bind(m_nPort, af))
				{
					if (m_SrvSock.Listen())
					{
						m_bListening = true;
						AsyncSelect(&m_SrvSock, FD_ACCEPT);
						UpdateControls();
						CString sAddr;
						if (m_SrvSock.GetSockName(sAddr, (short &)m_nPort))
						{
							UpdateData(FALSE);
							Log(CString((LPCTSTR)IDS_LISTENING), CGlobals::ToString(m_nPort));
						}
						else
						{
							s.Format(IDS_FORMAT_ERROR, CWinSock::GetLastError());
							Log(_T("GetSockName()"), s);
						}
					}
					else
					{
						s.Format(IDS_FORMAT_ERROR, CWinSock::GetLastError());
						Log(_T("Listen()"), s);
						m_SrvSock.Close();
					}
				}
				else
				{
					s.Format(IDS_FORMAT_ERROR, CWinSock::GetLastError());
					Log(_T("Bind()"), s);
					m_SrvSock.Close();
				}
			}
			else
			{
				s.Format(IDS_FORMAT_ERROR, CWinSock::GetLastError());
				Log(_T("Create()"), s);
				m_SrvSock.Close();
			}
		}

		BackupProperties();
	}
}

void CServerForm::Log(LPCTSTR szMessage, LPCTSTR szMessage2)
{
	int nItem = m_ctrlMessage.GetItemCount();
	m_ctrlMessage.InsertItem(nItem, szMessage);
	if (szMessage2 != NULL)
		m_ctrlMessage.SetItemText(nItem, 1, szMessage2);
}

void CServerForm::AsyncSelect(CWinSock *pSock, long nEvents)
{
	if (pSock == NULL)
		return;
	pSock->AsyncSelect(GetSafeHwnd(), WM_SOCKET, nEvents);
}

void CServerForm::OnAccept(int nErrCode)
{
	CString s;
	if (nErrCode != 0)
	{
		s.Format(IDS_FORMAT_ERROR, nErrCode);
		Log(_T("OnAccept()"), s);
	}
	else
	{
		short nPort;
		CString sAddr;
		CWinSock *pSock = m_SrvSock.Accept(sAddr, nPort);
		if (pSock)
		{
			s.Format(IDS_FORMAT_CLIENT, sAddr, nPort);
			Log(CString((LPCTSTR)IDS_ACCEPTED), s);
			AsyncSelect(pSock, FD_CLOSE | FD_READ);
			SOCKET sock = pSock->GetHandle();
			if (m_ClientMap.find(sock) == m_ClientMap.end())
			{
				CCliData *pCli = new CCliData(pSock);
				m_ClientMap[sock] = pCli;
			}
		}
		else
		{
			s.Format(IDS_FORMAT_ERROR, CWinSock::GetLastError());
			Log(_T("Accept()"), s);
		}
	}
}

void CServerForm::OnClose(SOCKET sock, int nErrCode)
{
	if (nErrCode != 0)
	{
		CString s;
		s.Format(IDS_FORMAT_ERROR, nErrCode);
		Log(_T("OnClose()"), s);
	}
	Log(CString((LPCTSTR)IDS_DISCONNECTED));
	m_ClientMap.erase(sock);
}

void CServerForm::OnReceive(SOCKET sock, int nErrCode)
{
	CString s;
	if (nErrCode != 0)
	{
		s.Format(IDS_FORMAT_ERROR, nErrCode);
		Log(_T("OnReceive()"), s);
	}
	else
	{
		TCliSockMapIterator it = m_ClientMap.find(sock);
		if (it != m_ClientMap.end())
		{
			CCliData *pCli = it->second;
			CWinSock *pSock = pCli->GetSock();
			int nTotalBytes = pSock->GetAvailableReadCount();
			if (nTotalBytes > 0)
			{
				s.Format(IDS_FORMAT_BYTES, nTotalBytes);
				Log(CString((LPCTSTR)IDS_RECEIVED_DATA), s);
				BYTE *pBuff = new BYTE[nTotalBytes];
				int nRecvBytes = pSock->Receive(pBuff, nTotalBytes);
				if (nRecvBytes == SOCKET_ERROR)
				{
					int nErr = CWinSock::GetLastError();
					if (nErr != WSAEWOULDBLOCK)
					{
						s.Format(IDS_FORMAT_ERROR, nErr);
						Log(_T("Receive()"), s);
						m_ClientMap.erase(it);
					}
				}
				else
				{
					int nCurrBytes = 0, nChunkSize = 8, nLogBytes;
					while (nCurrBytes < nRecvBytes)
					{
						nLogBytes = nRecvBytes - nCurrBytes;
						if (nLogBytes > nChunkSize)
							nLogBytes = nChunkSize;
						Log(CGlobals::TransformToHexBytes(pBuff + nCurrBytes, nLogBytes),
							CGlobals::TransformToText(pBuff + nCurrBytes, nLogBytes));
						nCurrBytes += nLogBytes;
					}
					if (IsEcho())
						pCli->GetBuffer().AppendData(pBuff, nTotalBytes);
					if (!pCli->GetBuffer().IsEmpty())
						AsyncSelect(pSock, FD_CLOSE | FD_READ | FD_WRITE);
				}
				delete []pBuff;
			}
		}
	}
}

void CServerForm::OnSend(SOCKET sock, int nErrCode)
{
	CString s;
	if (nErrCode != 0)
	{
		s.Format(IDS_FORMAT_ERROR, nErrCode);
		Log(_T("OnSend()"), s);
	}
	else
	{
		TCliSockMapIterator it = m_ClientMap.find(sock);
		if (it != m_ClientMap.end())
		{
			CCliData *pCli = it->second;
			CWinSock *pSock = pCli->GetSock();
			if (!pCli->GetBuffer().IsEmpty())
			{
				int nSentBytes = pSock->Send(pCli->GetBuffer().GetData(), pCli->GetBuffer().GetDataLen());
				if (nSentBytes > 0)
					pCli->GetBuffer().RemoveHeadCount(nSentBytes);
				else if (nSentBytes == SOCKET_ERROR)
				{
					int nErr = CWinSock::GetLastError();
					if (nErr != WSAEWOULDBLOCK)
					{
						s.Format(IDS_FORMAT_ERROR, nErr);
						Log(_T("Send()"), s);
						m_ClientMap.erase(it);
						return;
					}
				}
			}
			if (pCli->GetBuffer().IsEmpty())
				AsyncSelect(pSock, FD_CLOSE | FD_READ);
		}
	}
}

int CServerForm::GetAddrFamily()
{
	if (m_ctrlIpv6.GetCheck() == BST_CHECKED)
		return AF_INET6;
	return AF_INET;
}

bool CServerForm::IsEcho()
{
	return m_ctrlEcho.GetCheck() == BST_CHECKED;
}

/////////////////////////////////////////////////////////////////////////////

CCliData::CCliData(CWinSock *pSock)
{
	m_pSock = pSock;
}

CCliData::~CCliData()
{
	if (m_pSock != NULL)
	{
		m_pSock->Shutdown();
		delete m_pSock;
		m_pSock = NULL;
	}
}

/////////////////////////////////////////////////////////////////////////////

CCliSockMap::~CCliSockMap()
{
	TCliSockMapIterator it = begin();
	for (; it != end(); it++)
		delete it->second;
	clear();
}
