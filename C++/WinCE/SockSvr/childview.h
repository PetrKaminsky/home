#if !defined(AFX_CHILDVIEW_H__926CE696_B622_4CF1_9336_E83E3068C382__INCLUDED_)
#define AFX_CHILDVIEW_H__926CE696_B622_4CF1_9336_E83E3068C382__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Socket.h"
#include <map>

/////////////////////////////////////////////////////////////////////////////
// CChldView form view

class CChldView : public CFormView
{
public:
	CChldView();
	CChldView(int nID);

	int Create(CWnd *pParent);

private:
	int m_nID;
};

/////////////////////////////////////////////////////////////////////////////

class CCliData
{
public:
	CCliData(CWinSock *pSock);
	~CCliData();

	CWinSock *GetSock() { return m_pSock; }
	CWinSockBuffer &GetBuffer() { return m_Buff; }

private:
	CWinSock *m_pSock;
	CWinSockBuffer m_Buff;
};

/////////////////////////////////////////////////////////////////////////////

typedef std::map<SOCKET, CCliData *> TCliSockMap;
typedef TCliSockMap::iterator TCliSockMapIterator;

class CCliSockMap : public TCliSockMap
{
public:
	~CCliSockMap();
};

/////////////////////////////////////////////////////////////////////////////
// CServerForm form view

class CServerForm : public CChldView
{
public:
	CServerForm();

	//{{AFX_DATA(CServerForm)
	enum { IDD = IDD_FORM_SERVER };
	CButton	m_ctrlEcho;
	CEdit	m_ctrlPort;
	CListCtrl	m_ctrlMessage;
	CButton	m_ctrlListen;
	UINT	m_nPort;
	CButton m_ctrlIpv4;
	CButton m_ctrlIpv6;
	//}}AFX_DATA

	//{{AFX_VIRTUAL(CServerForm)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

protected:
	//{{AFX_MSG(CServerForm)
	afx_msg void OnDestroy();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnButtonListen();
	//}}AFX_MSG
	afx_msg LRESULT OnSocketMessage(WPARAM, LPARAM);
	DECLARE_MESSAGE_MAP()

private:
	CRegPropertyInt m_Column0Width;
	CRegPropertyInt m_Column1Width;
	CRegPropertyInt m_LastPort;
	CRegPropertyBool m_LastIpv4;
	CRegPropertyBool m_LastIpv6;
	CRegPropertyBool m_LastEcho;
	CWinSock m_SrvSock;
	bool m_bListening;
	CCliSockMap m_ClientMap;

	void UpdateControls();
	void BackupProperties();
	void Log(LPCTSTR szMessage, LPCTSTR szMessage2 = NULL);
	void AsyncSelect(CWinSock *pSock, long nEvents);
	void OnAccept(int nErrCode);
	void OnClose(SOCKET sock, int nErrCode);
	void OnReceive(SOCKET sock, int nErrCode);
	void OnSend(SOCKET sock, int nErrCode);
	int GetAddrFamily();
	bool IsEcho();
};

/////////////////////////////////////////////////////////////////////////////
//{{AFX_INSERT_LOCATION}}
// Microsoft eMbedded Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CHILDVIEW_H__926CE696_B622_4CF1_9336_E83E3068C382__INCLUDED_)
