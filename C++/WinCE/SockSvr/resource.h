//{{NO_DEPENDENCIES}}
// Microsoft eMbedded Visual C++ generated include file.
// Used by SockSvr.rc
//
#define IDP_SOCKETS_INIT_FAILED         104
#define IDR_MAINFRAME                   128
#define IDS_MESSAGE_COLUMN0             129
#define IDD_FORM_SERVER                 130
#define IDS_MESSAGE_COLUMN1             130
#define IDS_ALREADY_LISTENING           131
#define IDS_FORMAT_ERROR                132
#define IDS_LISTENING                   133
#define IDS_FORMAT_CLIENT               134
#define IDS_ACCEPTED                    135
#define IDS_DISCONNECTED                136
#define IDS_RECEIVED_DATA               137
#define IDS_FORMAT_BYTES                138
#define IDC_RADIO_IPV4                  1000
#define IDC_EDIT_PORT                   1001
#define IDC_BUTTON_LISTEN               1002
#define IDC_RADIO_IPV6                  1003
#define IDC_CHECK_ECHO                  1004
#define IDC_LIST_MESSAGE                1005

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        131
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1005
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
