#if !defined(AFX_SOCKSVR_H__5D96C1B6_38B7_4BEB_9AEE_AA0BD02F97A8__INCLUDED_)
#define AFX_SOCKSVR_H__5D96C1B6_38B7_4BEB_9AEE_AA0BD02F97A8__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols
#include "Property.h"

/////////////////////////////////////////////////////////////////////////////
// CSockSvrApp:
//

class CSockSvrApp : public CWinApp
{
public:
	static CRegPropertySerializer *s_pPropertySerializer;

	//{{AFX_VIRTUAL(CSockSvrApp)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	//}}AFX_VIRTUAL

	//{{AFX_MSG(CSockSvrApp)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////
//{{AFX_INSERT_LOCATION}}
// Microsoft eMbedded Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SOCKSVR_H__5D96C1B6_38B7_4BEB_9AEE_AA0BD02F97A8__INCLUDED_)
