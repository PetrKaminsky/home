#include "stdafx.h"
#include "SockSvr.h"
#include "MainFrm.h"
#include "Socket.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define MAJOR_VER 2
#define MINOR_VER 0

/////////////////////////////////////////////////////////////////////////////
// CSockSvrApp

BEGIN_MESSAGE_MAP(CSockSvrApp, CWinApp)
	//{{AFX_MSG_MAP(CSockSvrApp)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// The one and only CSockSvrApp object

CSockSvrApp theApp;
CRegPropertySerializer *CSockSvrApp::s_pPropertySerializer = NULL;

/////////////////////////////////////////////////////////////////////////////
// CSockSvrApp initialization

BOOL CSockSvrApp::InitInstance()
{
	WSADATA wsaData;
	bool bWsaErr = false;
	if (::WSAStartup(MAKEWORD(MAJOR_VER, MINOR_VER), &wsaData) != 0)
		bWsaErr = true;
	else if (LOBYTE(wsaData.wVersion) != MAJOR_VER || HIBYTE(wsaData.wVersion) != MINOR_VER)
		bWsaErr = true;
	if (bWsaErr)
	{
		AfxMessageBox(IDP_SOCKETS_INIT_FAILED);
		return FALSE;
	}

	s_pPropertySerializer = new CRegPropertySerializer(_T("HKEY_LOCAL_MACHINE\\Software\\Kamen\\SockSvr"));
	CMainFrame* pFrame = new CMainFrame;
	m_pMainWnd = pFrame;
	pFrame->LoadFrame(IDR_MAINFRAME, WS_OVERLAPPEDWINDOW);
	pFrame->ShowWindow(m_nCmdShow);
	pFrame->UpdateWindow();

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CSockSvrApp message handlers

int CSockSvrApp::ExitInstance()
{
	delete s_pPropertySerializer;
	CWinSockWorker::StopWorker();
	::WSACleanup();

	return CWinApp::ExitInstance();
}
