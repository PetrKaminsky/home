#include "stdafx.h"
#include "ConnMgr.h"
#include "ChildView.h"
#include "MainFrm.h"
#include "Globals.h"
#include "Socket.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CChldView

CChldView::CChldView() :
	CFormView(-1)
{
	m_nID = -1;
}

CChldView::CChldView(int nID)
	: CFormView(nID)
{
	m_nID = nID;
}

int CChldView::Create(CWnd *pParent)
{
	return CFormView::Create(NULL, NULL, WS_CHILD | WS_VISIBLE,
		CRect(0, 0, 0, 0), pParent, m_nID, NULL);
}

/////////////////////////////////////////////////////////////////////////////
// CChldView1

CChldView1::CChldView1() :
	CChldView(CChldView1::IDD),
	m_Column0Width(CConnMgrApp::s_pPropertySerializer, _T("Networks\\Column0"), 300),
	m_Column1Width(CConnMgrApp::s_pPropertySerializer, _T("Networks\\Column1"), 150)
{
	//{{AFX_DATA_INIT(CChldView1)
	//}}AFX_DATA_INIT
}

void CChldView1::DoDataExchange(CDataExchange* pDX)
{
	CChldView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CChldView1)
	DDX_Control(pDX, IDC_LIST_CONN, m_ctrlConn);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CChldView1, CChldView)
	//{{AFX_MSG_MAP(CChldView1)
	ON_WM_SIZE()
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CChldView message handlers

void CChldView1::OnInitialUpdate()
{
	CChldView::OnInitialUpdate();

	m_ctrlConn.InsertColumn(0, CString((LPCTSTR)IDS_NETWORKS_COLUMN0), LVCFMT_LEFT, m_Column0Width.GetValue());
	m_ctrlConn.InsertColumn(1, CString((LPCTSTR)IDS_NETWORKS_COLUMN1), LVCFMT_LEFT, m_Column1Width.GetValue());
	m_ctrlConn.SetExtendedStyle(m_ctrlConn.GetExtendedStyle() | LVS_EX_FULLROWSELECT | LVS_EX_HEADERDRAGDROP);

	Fill();
}

void CChldView1::OnSize(UINT nType, int cx, int cy)
{
	CChldView::OnSize(nType, cx, cy);

	if (cx == 0 &&
		cy == 0)
	{
		return;
	}

	if (m_ctrlConn.GetSafeHwnd() == NULL)
	{
		return;
	}

	CRect r;
	GetClientRect(&r);

	CGlobals::ResizeControl(&m_ctrlConn, r.right - 1, r.bottom - 1);
}

void CChldView1::Fill()
{
	CONNMGR_DESTINATION_INFO cdi;
	int nIndex = 0;
	HRESULT hr;
	while ((hr = ::ConnMgrEnumDestinations(
		nIndex,
		&cdi)) == S_OK)
	{
		int nItem = m_ctrlConn.GetItemCount();
		m_ctrlConn.InsertItem(nItem, CGlobals::GuidToString(cdi.guid));
		m_ctrlConn.SetItemText(nItem, 1, cdi.szDescription);
		nIndex++;
	}
	if (hr != S_OK &&
		nIndex == 0)
	{
		CGlobals::PrintLastError(_T("ConnMgrEnumDestinations()"), hr);
	}
}

void CChldView1::OnDestroy() 
{
	BackupProperties();

	CChldView::OnDestroy();
}

void CChldView1::BackupProperties()
{
	m_Column0Width.SetValue(m_ctrlConn.GetColumnWidth(0));
	m_Column1Width.SetValue(m_ctrlConn.GetColumnWidth(1));
}

/////////////////////////////////////////////////////////////////////////////
// CChldView2

CChldView2::CChldView2() :
	CChldView(CChldView2::IDD),
	m_Column0Width(CConnMgrApp::s_pPropertySerializer, _T("MapUrl\\Column0"), 300),
	m_LastUrl(CConnMgrApp::s_pPropertySerializer, _T("MapUrl\\Url"), _T(""))
{
	//{{AFX_DATA_INIT(CChldView2)
	//}}AFX_DATA_INIT
}

void CChldView2::DoDataExchange(CDataExchange* pDX)
{
	CChldView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CChldView2)
	DDX_Control(pDX, IDC_EDIT_URL, m_ctrlURL);
	DDX_Control(pDX, IDC_LIST_GUID, m_ctrlGuid);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CChldView2, CChldView)
	//{{AFX_MSG_MAP(CChldView2)
	ON_BN_CLICKED(IDC_BUTTON_MAP_URL, OnButtonMapUrl)
	ON_WM_SIZE()
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CChldView2 message handlers

void CChldView2::OnButtonMapUrl()
{
	m_ctrlGuid.DeleteAllItems();
	CString sURL(CGlobals::GetWindowText(&m_ctrlURL));
	if (sURL.IsEmpty())
		return;
	GUID guid;
	DWORD dwIndex = 0;
	HRESULT hr;
	ZeroMemory(&guid, sizeof(guid));
	while ((hr = ::ConnMgrMapURL(
		sURL,
		&guid,
		&dwIndex)) == S_OK)
	{
		m_ctrlGuid.InsertItem(m_ctrlGuid.GetItemCount(), CGlobals::GuidToString(guid));
	}
	if (hr != S_OK &&
		dwIndex == 0)
	{
		CGlobals::PrintLastError(_T("ConnMgrMapURL()"), hr);
	}
	BackupProperties();
}

void CChldView2::OnInitialUpdate()
{
	CChldView::OnInitialUpdate();

	m_ctrlGuid.InsertColumn(0, CString((LPCTSTR)IDS_MAPURL_COLUMN0), LVCFMT_LEFT, m_Column0Width.GetValue());
	m_ctrlGuid.SetExtendedStyle(m_ctrlGuid.GetExtendedStyle() | LVS_EX_FULLROWSELECT | LVS_EX_HEADERDRAGDROP);
	m_ctrlURL.SetWindowText(m_LastUrl.GetValue());
}

void CChldView2::OnSize(UINT nType, int cx, int cy)
{
	CChldView::OnSize(nType, cx, cy);

	if (cx == 0 &&
		cy == 0)
	{
		return;
	}

	if (m_ctrlURL.GetSafeHwnd() == NULL ||
		m_ctrlGuid.GetSafeHwnd() == NULL)
	{
		return;
	}

	CRect r;
	GetClientRect(&r);

	CGlobals::ResizeControl(&m_ctrlURL, r.right - 1);
	CGlobals::ResizeControl(&m_ctrlGuid, r.right - 1, r.bottom - 1);
}

void CChldView2::OnDestroy()
{
	BackupProperties();

	CChldView::OnDestroy();
}

void CChldView2::BackupProperties()
{
	m_Column0Width.SetValue(m_ctrlGuid.GetColumnWidth(0));
	m_LastUrl.SetValue(CGlobals::GetWindowText(&m_ctrlURL));
}

/////////////////////////////////////////////////////////////////////////////
// CChldView3

CChldView3::CChldView3() :
	CChldView(CChldView3::IDD),
	m_Column0Width(CConnMgrApp::s_pPropertySerializer, _T("MapRas\\Column0"), 300),
	m_LastRas(CConnMgrApp::s_pPropertySerializer, _T("MapRas\\Ras"), _T(""))
{
	//{{AFX_DATA_INIT(CChldView3)
	//}}AFX_DATA_INIT
}

void CChldView3::DoDataExchange(CDataExchange* pDX)
{
	CChldView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CChldView3)
	DDX_Control(pDX, IDC_LIST_GUID, m_ctrlGuid);
	DDX_Control(pDX, IDC_EDIT_RAS, m_ctrlRas);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CChldView3, CChldView)
	//{{AFX_MSG_MAP(CChldView3)
	ON_BN_CLICKED(IDC_BUTTON_MAP_RAS, OnButtonMapRas)
	ON_WM_SIZE()
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CChldView3 message handlers

void CChldView3::OnButtonMapRas()
{
#if (_WIN32_WCE >= 500)
	m_ctrlGuid.DeleteAllItems();
	CString sRas(CGlobals::GetWindowText(&m_ctrlRas));
	if (sRas.IsEmpty())
		return;
	GUID guid;
	HRESULT hr;
	ZeroMemory(&guid, sizeof(guid));
	if ((hr = ::ConnMgrMapConRef(
		ConRefType_NAP,
		sRas,
		&guid)) == S_OK)
	{
		m_ctrlGuid.InsertItem(m_ctrlGuid.GetItemCount(), CGlobals::GuidToString(guid));
	}
	else
	{
		CGlobals::PrintLastError(_T("ConnMgrMapConRef()"), hr);
	}
#endif
	BackupProperties();
}

void CChldView3::OnInitialUpdate()
{
	CChldView::OnInitialUpdate();

	m_ctrlGuid.InsertColumn(0, CString((LPCTSTR)IDS_MAPRAS_COLUMN0), LVCFMT_LEFT, m_Column0Width.GetValue());
	m_ctrlGuid.SetExtendedStyle(m_ctrlGuid.GetExtendedStyle() | LVS_EX_FULLROWSELECT | LVS_EX_HEADERDRAGDROP);
	m_ctrlRas.SetWindowText(m_LastRas.GetValue());
}

void CChldView3::OnSize(UINT nType, int cx, int cy)
{
	CChldView::OnSize(nType, cx, cy);

	if (cx == 0 &&
		cy == 0)
	{
		return;
	}

	if (m_ctrlRas.GetSafeHwnd() == NULL ||
		m_ctrlGuid.GetSafeHwnd() == NULL)
	{
		return;
	}

	CRect r;
	GetClientRect(&r);

	CGlobals::ResizeControl(&m_ctrlRas, r.right - 1);
	CGlobals::ResizeControl(&m_ctrlGuid, r.right - 1, r.bottom - 1);
}

void CChldView3::OnDestroy()
{
	BackupProperties();

	CChldView::OnDestroy();
}

void CChldView3::BackupProperties()
{
	m_Column0Width.SetValue(m_ctrlGuid.GetColumnWidth(0));
	m_LastRas.SetValue(CGlobals::GetWindowText(&m_ctrlRas));
}

/////////////////////////////////////////////////////////////////////////////
// CChldView4

#define WM_CONN (WM_USER + 1)

CChldView4::CChldView4() :
	CChldView(CChldView4::IDD),
	m_Column0Width(CConnMgrApp::s_pPropertySerializer, _T("Conn\\Column0"), 1000),
	m_LastRas(CConnMgrApp::s_pPropertySerializer, _T("Conn\\Ras"), _T("")),
	m_LastGuid(CConnMgrApp::s_pPropertySerializer, _T("Conn\\Guid"), _T(""))
{
	//{{AFX_DATA_INIT(CChldView4)
	//}}AFX_DATA_INIT
	m_bConnected = false;
	m_hConnection = INVALID_HANDLE_VALUE;
}

void CChldView4::DoDataExchange(CDataExchange* pDX)
{
	CChldView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CChldView4)
	DDX_Control(pDX, IDC_EDIT_GUID, m_ctrlGuid);
	DDX_Control(pDX, IDC_LIST_MSG, m_ctrlMsg);
	DDX_Control(pDX, IDC_EDIT_RAS, m_ctrlRas);
	DDX_Control(pDX, IDC_BUTTON_DISC, m_ctrlDisc);
	DDX_Control(pDX, IDC_BUTTON_CONN, m_ctrlConn);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CChldView4, CChldView)
	//{{AFX_MSG_MAP(CChldView4)
	ON_EN_CHANGE(IDC_EDIT_RAS, OnChangeEditRas)
	ON_BN_CLICKED(IDC_BUTTON_CONN, OnButtonConn)
	ON_BN_CLICKED(IDC_BUTTON_DISC, OnButtonDisc)
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_EN_CHANGE(IDC_EDIT_GUID, OnChangeEditGuid)
	//}}AFX_MSG_MAP
	ON_MESSAGE(WM_CONN, OnConnMessage)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CChldView4 message handlers

void CChldView4::OnInitialUpdate()
{
	CChldView::OnInitialUpdate();

	m_ctrlMsg.InsertColumn(0, CString((LPCTSTR)IDS_CONNECT_COLUMN0), LVCFMT_LEFT, m_Column0Width.GetValue());
	m_ctrlMsg.SetExtendedStyle(m_ctrlMsg.GetExtendedStyle() | LVS_EX_FULLROWSELECT | LVS_EX_HEADERDRAGDROP);
	m_ctrlRas.SetWindowText(m_LastRas.GetValue());
	m_ctrlGuid.SetWindowText(m_LastGuid.GetValue());

	UpdateControls();
}

void CChldView4::OnChangeEditRas()
{
	UpdateControls();
}

void CChldView4::OnButtonConn()
{
	m_ctrlMsg.DeleteAllItems();
	GUID guid;
	HRESULT hr;
#if (_WIN32_WCE >= 500)
	CString sRas(CGlobals::GetWindowText(&m_ctrlRas));
	if (!sRas.IsEmpty())
	{
		if ((hr = ::ConnMgrMapConRef(
			ConRefType_NAP,
			sRas,
			&guid)) != S_OK)
		{
			CGlobals::PrintLastError(_T("ConnMgrMapConRef()"), hr);
			return;
		}
		m_ctrlGuid.SetWindowText(CGlobals::GuidToString(guid));
	}
#endif
	CString sGuid(CGlobals::GetWindowText(&m_ctrlGuid));
	if (sGuid.IsEmpty())
		return;
	ZeroMemory(&guid, sizeof(guid));
	if (!CGlobals::StringToGuid(sGuid, guid))
		return;
	CONNMGR_CONNECTIONINFO ci;
	ZeroMemory(&ci, sizeof(ci));
	ci.cbSize = sizeof(ci);
	ci.dwParams = CONNMGR_PARAM_GUIDDESTNET;
	ci.dwPriority = CONNMGR_PRIORITY_USERINTERACTIVE;
	ci.bExclusive = FALSE;
	ci.bDisabled = FALSE;
	CopyMemory(&ci.guidDestNet, &guid, sizeof(guid));
	ci.hWnd = GetSafeHwnd();
	ci.uMsg = WM_CONN;
	if ((hr = ::ConnMgrEstablishConnection(
		&ci,
		&m_hConnection)) != S_OK)
	{
		CGlobals::PrintLastError(_T("ConnMgrEstablishConnection()"), hr);
	}
	BackupProperties();
}

void CChldView4::OnButtonDisc()
{
	HRESULT hr;
	if ((hr = ::ConnMgrReleaseConnection(
		m_hConnection,
		FALSE)) == S_OK)
	{
		m_bConnected = false;
		UpdateControls();
	}
	else
	{
		CGlobals::PrintLastError(_T("ConnMgrReleaseConnection()"), hr);
	}
}

void CChldView4::UpdateControls()
{
	CString sRas(CGlobals::GetWindowText(&m_ctrlRas));
	CString sGuid(CGlobals::GetWindowText(&m_ctrlGuid));
	m_ctrlConn.EnableWindow(((sRas.IsEmpty() == FALSE || sGuid.IsEmpty() == FALSE) && !m_bConnected)
		? TRUE
		: FALSE);
	m_ctrlDisc.EnableWindow(m_bConnected ? TRUE : FALSE);
	m_ctrlRas.EnableWindow(m_bConnected ? FALSE : TRUE);
}

LRESULT CChldView4::OnConnMessage(WPARAM wParam, LPARAM lParam)
{
	int nItem = m_ctrlMsg.GetItemCount();
	m_ctrlMsg.InsertItem(nItem, ConnStateText(wParam));
	m_ctrlMsg.EnsureVisible(nItem, FALSE);

	switch (wParam)
	{
		case CONNMGR_STATUS_CONNECTED:
			m_bConnected = true;
			UpdateControls();
			break;
	}

	return 0;
}

CString CChldView4::ConnStateText(DWORD dwStatus)
{
	CString sResult;
	switch (dwStatus)
	{
		case CONNMGR_STATUS_UNKNOWN:
			sResult = CString((LPCTSTR)IDS_CONNMGR_STATUS_UNKNOWN);
			break;

		case CONNMGR_STATUS_CONNECTED:
			sResult = CString((LPCTSTR)IDS_CONNMGR_STATUS_CONNECTED);
			break;

		case CONNMGR_STATUS_DISCONNECTED:
			sResult = CString((LPCTSTR)IDS_CONNMGR_STATUS_DISCONNECTED);
			break;

		case CONNMGR_STATUS_CONNECTIONFAILED:
			sResult = CString((LPCTSTR)IDS_CONNMGR_STATUS_CONNECTIONFAILED);
			break;

		case CONNMGR_STATUS_CONNECTIONCANCELED:
			sResult = CString((LPCTSTR)IDS_CONNMGR_STATUS_CONNECTIONCANCELED);
			break;

		case CONNMGR_STATUS_CONNECTIONDISABLED:
			sResult = CString((LPCTSTR)IDS_CONNMGR_STATUS_CONNECTIONDISABLED);
			break;

		case CONNMGR_STATUS_NOPATHTODESTINATION:
			sResult = CString((LPCTSTR)IDS_CONNMGR_STATUS_NOPATHTODESTINATION);
			break;

		case CONNMGR_STATUS_WAITINGFORPATH:
			sResult = CString((LPCTSTR)IDS_CONNMGR_STATUS_WAITINGFORPATH);
			break;

		case CONNMGR_STATUS_WAITINGFORPHONE:
			sResult = CString((LPCTSTR)IDS_CONNMGR_STATUS_WAITINGFORPHONE);
			break;

		case CONNMGR_STATUS_WAITINGCONNECTION:
			sResult = CString((LPCTSTR)IDS_CONNMGR_STATUS_WAITINGCONNECTION);
			break;

		case CONNMGR_STATUS_WAITINGFORRESOURCE:
			sResult = CString((LPCTSTR)IDS_CONNMGR_STATUS_WAITINGFORRESOURCE);
			break;

		case CONNMGR_STATUS_WAITINGFORNETWORK:
			sResult = CString((LPCTSTR)IDS_CONNMGR_STATUS_WAITINGFORNETWORK);
			break;

		case CONNMGR_STATUS_WAITINGDISCONNECTION:
			sResult = CString((LPCTSTR)IDS_CONNMGR_STATUS_WAITINGDISCONNECTION);
			break;

		case CONNMGR_STATUS_WAITINGCONNECTIONABORT:
			sResult = CString((LPCTSTR)IDS_CONNMGR_STATUS_WAITINGCONNECTIONABORT);
			break;

#if (_WIN32_WCE >= 500)
		case CONNMGR_STATUS_SUSPENDED:
			sResult = CString((LPCTSTR)IDS_CONNMGR_STATUS_SUSPENDED);
			break;

		case CONNMGR_STATUS_PHONEOFF:
			sResult = CString((LPCTSTR)IDS_CONNMGR_STATUS_PHONEOFF);
			break;

		case CONNMGR_STATUS_EXCLUSIVECONFLICT:
			sResult = CString((LPCTSTR)IDS_CONNMGR_STATUS_EXCLUSIVECONFLICT);
			break;

		case CONNMGR_STATUS_NORESOURCES:
			sResult = CString((LPCTSTR)IDS_CONNMGR_STATUS_NORESOURCES);
			break;

		case CONNMGR_STATUS_CONNECTIONLINKFAILED:
			sResult = CString((LPCTSTR)IDS_CONNMGR_STATUS_CONNECTIONLINKFAILED);
			break;

		case CONNMGR_STATUS_AUTHENTICATIONFAILED:
			sResult = CString((LPCTSTR)IDS_CONNMGR_STATUS_AUTHENTICATIONFAILED);
			break;
#endif
	}
	return sResult;
}

void CChldView4::OnSize(UINT nType, int cx, int cy)
{
	CChldView::OnSize(nType, cx, cy);

	if (cx == 0 &&
		cy == 0)
	{
		return;
	}

	if (m_ctrlRas.GetSafeHwnd() == NULL ||
		m_ctrlGuid.GetSafeHwnd() == NULL ||
		m_ctrlMsg.GetSafeHwnd() == NULL)
	{
		return;
	}

	CRect r;
	GetClientRect(&r);

	CGlobals::ResizeControl(&m_ctrlRas, r.right - 1);
	CGlobals::ResizeControl(&m_ctrlGuid, r.right - 1);
	CGlobals::ResizeControl(&m_ctrlMsg, r.right - 1, r.bottom - 1);
}

void CChldView4::OnDestroy()
{
	BackupProperties();

	CChldView::OnDestroy();
}

void CChldView4::BackupProperties()
{
	m_Column0Width.SetValue(m_ctrlMsg.GetColumnWidth(0));
	m_LastRas.SetValue(CGlobals::GetWindowText(&m_ctrlRas));
	m_LastGuid.SetValue(CGlobals::GetWindowText(&m_ctrlGuid));
}

void CChldView4::OnChangeEditGuid()
{
	UpdateControls();
}

/////////////////////////////////////////////////////////////////////////////
// CChldView5

UINT CChldView5::s_nMsgId = WM_RASDIALEVENT;

CChldView5::CChldView5() :
	CChldView(CChldView5::IDD),
	m_Column0Width(CConnMgrApp::s_pPropertySerializer, _T("RasConn\\Column0"), 300),
	m_Column1Width(CConnMgrApp::s_pPropertySerializer, _T("RasConn\\Column1"), 200),
	m_LastRas(CConnMgrApp::s_pPropertySerializer, _T("RasConn\\Ras"), _T(""))
{
	//{{AFX_DATA_INIT(CChldView5)
	//}}AFX_DATA_INIT
	m_bConnected = false;
}

void CChldView5::DoDataExchange(CDataExchange* pDX)
{
	CChldView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CChldView5)
	DDX_Control(pDX, IDC_LIST_MSG, m_ctrlMsg);
	DDX_Control(pDX, IDC_EDIT_RAS, m_ctrlRas);
	DDX_Control(pDX, IDC_BUTTON_DISC, m_ctrlDisc);
	DDX_Control(pDX, IDC_BUTTON_CONN, m_ctrlConn);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CChldView5, CChldView)
	//{{AFX_MSG_MAP(CChldView5)
	ON_BN_CLICKED(IDC_BUTTON_CONN, OnButtonConn)
	ON_BN_CLICKED(IDC_BUTTON_DISC, OnButtonDisc)
	ON_EN_CHANGE(IDC_EDIT_RAS, OnChangeEditRas)
	ON_WM_SIZE()
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
	ON_REGISTERED_MESSAGE(s_nMsgId, OnRasDialEvent)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CChldView5 message handlers

void CChldView5::OnInitialUpdate()
{
	CChldView::OnInitialUpdate();

	m_ctrlMsg.InsertColumn(0, CString((LPCTSTR)IDS_RASCONN_COLUMN0), LVCFMT_LEFT, m_Column0Width.GetValue());
	m_ctrlMsg.InsertColumn(1, CString((LPCTSTR)IDS_RASCONN_COLUMN1), LVCFMT_LEFT, m_Column1Width.GetValue());
	m_ctrlMsg.SetExtendedStyle(m_ctrlMsg.GetExtendedStyle() | LVS_EX_FULLROWSELECT | LVS_EX_HEADERDRAGDROP);

	UpdateControls();
}

void CChldView5::OnButtonConn()
{
	CString sRas(CGlobals::GetWindowText(&m_ctrlRas));
	if (sRas.IsEmpty())
		return;
	DWORD dwErr;

	m_ctrlMsg.DeleteAllItems();
	RASENTRY RasEntry;
	DWORD dwSize = sizeof(RasEntry);
	ZeroMemory(&RasEntry, dwSize);
	RasEntry.dwSize = dwSize;
	dwErr = ::RasGetEntryProperties(
		NULL,
		(LPTSTR)(LPCTSTR)sRas,
		&RasEntry,
		&dwSize,
		NULL,
		NULL);
	if (dwErr != 0)
	{
		CGlobals::PrintLastError(_T("RasGetEntryProperties()"), dwErr, RasErrorText(dwErr));
		return;
	}

	RASDIALPARAMS rdp;
	ZeroMemory(&rdp, sizeof(rdp));
	rdp.dwSize = sizeof(rdp);
	_tcscpy(rdp.szEntryName, sRas);
	BOOL bPwd;
	dwErr = ::RasGetEntryDialParams(
		NULL,
		&rdp,
		&bPwd);
	if (dwErr != 0)
	{
		CGlobals::PrintLastError(_T("RasGetEntryDialParams()"), dwErr, RasErrorText(dwErr));
		return;
	}

	m_hConnection = NULL;
	dwErr = ::RasDial(
		NULL,
		NULL,
		&rdp,
		0xFFFFFFFF,
		GetSafeHwnd(),
		&m_hConnection);
	if (dwErr != 0)
	{
		CGlobals::PrintLastError(_T("RasDial()"), dwErr, RasErrorText(dwErr));
		if (m_hConnection != NULL)
			::RasHangUp(m_hConnection);
	}
	BackupProperties();
}

void CChldView5::OnButtonDisc()
{
	DWORD dwErr = ::RasHangUp(m_hConnection);
	if (dwErr != 0)
	{
		CGlobals::PrintLastError(_T("RasHangUp()"), dwErr, RasErrorText(dwErr));
	}
}

void CChldView5::OnChangeEditRas()
{
	UpdateControls();
}

void CChldView5::UpdateControls()
{
	CString sRas(CGlobals::GetWindowText(&m_ctrlRas));
	m_ctrlConn.EnableWindow((sRas.IsEmpty() == FALSE && !m_bConnected) ? TRUE : FALSE);
	m_ctrlDisc.EnableWindow(m_bConnected ? TRUE : FALSE);
	m_ctrlRas.EnableWindow(m_bConnected ? FALSE : TRUE);
}

LRESULT CChldView5::OnRasDialEvent(WPARAM wParam, LPARAM lParam)
{
	int nItem = m_ctrlMsg.GetItemCount();
	m_ctrlMsg.InsertItem(nItem, RasConnStateText((RASCONNSTATE)wParam));
	if (lParam != 0)
	{
		CString s;
		s.Format(_T("%d, %s"), lParam, RasErrorText(lParam));
		m_ctrlMsg.SetItemText(nItem, 1, s);
	}
	m_ctrlMsg.EnsureVisible(nItem, FALSE);

	switch ((RASCONNSTATE)wParam)
	{
		case RASCS_Connected:
			m_bConnected = true;
			UpdateControls();
			break;

		case RASCS_Disconnected:
			m_bConnected = false;
			UpdateControls();
			break;
	}

	return TRUE;
}

CString CChldView5::RasConnStateText(RASCONNSTATE state)
{
	CString sResult;
	switch (state)
	{
		case RASCS_OpenPort:
			sResult = CString((LPCTSTR)IDS_RASCS_OpenPort);
			break;

		case RASCS_PortOpened:
			sResult = CString((LPCTSTR)IDS_RASCS_PortOpened);
			break;

		case RASCS_ConnectDevice:
			sResult = CString((LPCTSTR)IDS_RASCS_ConnectDevice);
			break;

		case RASCS_DeviceConnected:
			sResult = CString((LPCTSTR)IDS_RASCS_DeviceConnected);
			break;

		case RASCS_AllDevicesConnected:
			sResult = CString((LPCTSTR)IDS_RASCS_AllDevicesConnected);
			break;

		case RASCS_Authenticate:
			sResult = CString((LPCTSTR)IDS_RASCS_Authenticate);
			break;

		case RASCS_AuthNotify:
			sResult = CString((LPCTSTR)IDS_RASCS_AuthNotify);
			break;

		case RASCS_AuthRetry:
			sResult = CString((LPCTSTR)IDS_RASCS_AuthRetry);
			break;

		case RASCS_AuthCallback:
			sResult = CString((LPCTSTR)IDS_RASCS_AuthCallback);
			break;

		case RASCS_AuthChangePassword:
			sResult = CString((LPCTSTR)IDS_RASCS_AuthChangePassword);
			break;

		case RASCS_AuthProject:
			sResult = CString((LPCTSTR)IDS_RASCS_AuthProject);
			break;

		case RASCS_AuthLinkSpeed:
			sResult = CString((LPCTSTR)IDS_RASCS_AuthLinkSpeed);
			break;

		case RASCS_AuthAck:
			sResult = CString((LPCTSTR)IDS_RASCS_AuthAck);
			break;

		case RASCS_ReAuthenticate:
			sResult = CString((LPCTSTR)IDS_RASCS_ReAuthenticate);
			break;

		case RASCS_Authenticated:
			sResult = CString((LPCTSTR)IDS_RASCS_Authenticated);
			break;

		case RASCS_PrepareForCallback:
			sResult = CString((LPCTSTR)IDS_RASCS_PrepareForCallback);
			break;

		case RASCS_WaitForModemReset:
			sResult = CString((LPCTSTR)IDS_RASCS_WaitForModemReset);
			break;

		case RASCS_WaitForCallback:
			sResult = CString((LPCTSTR)IDS_RASCS_WaitForCallback);
			break;

		case RASCS_Projected:
			sResult = CString((LPCTSTR)IDS_RASCS_Projected);
			break;

		case RASCS_Interactive:
			sResult = CString((LPCTSTR)IDS_RASCS_Interactive);
			break;

		case RASCS_RetryAuthentication:
			sResult = CString((LPCTSTR)IDS_RASCS_RetryAuthentication);
			break;

		case RASCS_CallbackSetByCaller:
			sResult = CString((LPCTSTR)IDS_RASCS_CallbackSetByCaller);
			break;

		case RASCS_PasswordExpired:
			sResult = CString((LPCTSTR)IDS_RASCS_PasswordExpired);
			break;

		case RASCS_Connected:
			sResult = CString((LPCTSTR)IDS_RASCS_Connected);
			break;

		case RASCS_Disconnected:
			sResult = CString((LPCTSTR)IDS_RASCS_Disconnected);
			break;
	}
	return sResult;
}

CString CChldView5::RasErrorText(DWORD dwError)
{
	CString sResult;
	switch (dwError)
	{
		case PENDING:
			sResult = CString((LPCTSTR)IDS_RAS_PENDING);
			break;

		case ERROR_INVALID_PORT_HANDLE:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_INVALID_PORT_HANDLE);
			break;

		case ERROR_PORT_ALREADY_OPEN:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_PORT_ALREADY_OPEN);
			break;

		case ERROR_BUFFER_TOO_SMALL:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_BUFFER_TOO_SMALL);
			break;

		case ERROR_WRONG_INFO_SPECIFIED:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_WRONG_INFO_SPECIFIED);
			break;

		case ERROR_CANNOT_SET_PORT_INFO:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_CANNOT_SET_PORT_INFO);
			break;

		case ERROR_PORT_NOT_CONNECTED:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_PORT_NOT_CONNECTED);
			break;

		case ERROR_EVENT_INVALID:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_EVENT_INVALID);
			break;

		case ERROR_DEVICE_DOES_NOT_EXIST:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_DEVICE_DOES_NOT_EXIST);
			break;

		case ERROR_DEVICETYPE_DOES_NOT_EXIST:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_DEVICETYPE_DOES_NOT_EXIST);
			break;

		case ERROR_BUFFER_INVALID:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_BUFFER_INVALID);
			break;

		case ERROR_ROUTE_NOT_AVAILABLE:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_ROUTE_NOT_AVAILABLE);
			break;

		case ERROR_ROUTE_NOT_ALLOCATED:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_ROUTE_NOT_ALLOCATED);
			break;

		case ERROR_INVALID_COMPRESSION_SPECIFIED:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_INVALID_COMPRESSION_SPECIFIED);
			break;

		case ERROR_OUT_OF_BUFFERS:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_OUT_OF_BUFFERS);
			break;

		case ERROR_PORT_NOT_FOUND:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_PORT_NOT_FOUND);
			break;

		case ERROR_ASYNC_REQUEST_PENDING:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_ASYNC_REQUEST_PENDING);
			break;

		case ERROR_ALREADY_DISCONNECTING:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_ALREADY_DISCONNECTING);
			break;

		case ERROR_PORT_NOT_OPEN:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_PORT_NOT_OPEN);
			break;

		case ERROR_PORT_DISCONNECTED:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_PORT_DISCONNECTED);
			break;

		case ERROR_NO_ENDPOINTS:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_NO_ENDPOINTS);
			break;

		case ERROR_CANNOT_OPEN_PHONEBOOK:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_CANNOT_OPEN_PHONEBOOK);
			break;

		case ERROR_CANNOT_LOAD_PHONEBOOK:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_CANNOT_LOAD_PHONEBOOK);
			break;

		case ERROR_CANNOT_FIND_PHONEBOOK_ENTRY:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_CANNOT_FIND_PHONEBOOK_ENTRY);
			break;

		case ERROR_CANNOT_WRITE_PHONEBOOK:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_CANNOT_WRITE_PHONEBOOK);
			break;

		case ERROR_CORRUPT_PHONEBOOK:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_CORRUPT_PHONEBOOK);
			break;

		case ERROR_CANNOT_LOAD_STRING:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_CANNOT_LOAD_STRING);
			break;

		case ERROR_KEY_NOT_FOUND:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_KEY_NOT_FOUND);
			break;

		case ERROR_DISCONNECTION:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_DISCONNECTION);
			break;

		case ERROR_REMOTE_DISCONNECTION:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_REMOTE_DISCONNECTION);
			break;

		case ERROR_HARDWARE_FAILURE:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_HARDWARE_FAILURE);
			break;

		case ERROR_USER_DISCONNECTION:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_USER_DISCONNECTION);
			break;

		case ERROR_INVALID_SIZE:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_INVALID_SIZE);
			break;

		case ERROR_PORT_NOT_AVAILABLE:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_PORT_NOT_AVAILABLE);
			break;

		case ERROR_CANNOT_PROJECT_CLIENT:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_CANNOT_PROJECT_CLIENT);
			break;

		case ERROR_UNKNOWN:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_UNKNOWN);
			break;

		case ERROR_WRONG_DEVICE_ATTACHED:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_WRONG_DEVICE_ATTACHED);
			break;

		case ERROR_BAD_STRING:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_BAD_STRING);
			break;

		case ERROR_REQUEST_TIMEOUT:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_REQUEST_TIMEOUT);
			break;

		case ERROR_CANNOT_GET_LANA:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_CANNOT_GET_LANA);
			break;

		case ERROR_NETBIOS_ERROR:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_NETBIOS_ERROR);
			break;

		case ERROR_SERVER_OUT_OF_RESOURCES:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_SERVER_OUT_OF_RESOURCES);
			break;

		case ERROR_NAME_EXISTS_ON_NET:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_NAME_EXISTS_ON_NET);
			break;

		case ERROR_SERVER_GENERAL_NET_FAILURE:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_SERVER_GENERAL_NET_FAILURE);
			break;

		case WARNING_MSG_ALIAS_NOT_ADDED:
			sResult = CString((LPCTSTR)IDS_RAS_WARNING_MSG_ALIAS_NOT_ADDED);
			break;

		case ERROR_AUTH_INTERNAL:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_AUTH_INTERNAL);
			break;

		case ERROR_RESTRICTED_LOGON_HOURS:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_RESTRICTED_LOGON_HOURS);
			break;

		case ERROR_ACCT_DISABLED:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_ACCT_DISABLED);
			break;

		case ERROR_PASSWD_EXPIRED:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_PASSWD_EXPIRED);
			break;

		case ERROR_NO_DIALIN_PERMISSION:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_NO_DIALIN_PERMISSION);
			break;

		case ERROR_SERVER_NOT_RESPONDING:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_SERVER_NOT_RESPONDING);
			break;

		case ERROR_FROM_DEVICE:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_FROM_DEVICE);
			break;

		case ERROR_UNRECOGNIZED_RESPONSE:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_UNRECOGNIZED_RESPONSE);
			break;

		case ERROR_MACRO_NOT_FOUND:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_MACRO_NOT_FOUND);
			break;

		case ERROR_MACRO_NOT_DEFINED:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_MACRO_NOT_DEFINED);
			break;

		case ERROR_MESSAGE_MACRO_NOT_FOUND:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_MESSAGE_MACRO_NOT_FOUND);
			break;

		case ERROR_DEFAULTOFF_MACRO_NOT_FOUND:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_DEFAULTOFF_MACRO_NOT_FOUND);
			break;

		case ERROR_FILE_COULD_NOT_BE_OPENED:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_FILE_COULD_NOT_BE_OPENED);
			break;

		case ERROR_DEVICENAME_TOO_LONG:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_DEVICENAME_TOO_LONG);
			break;

		case ERROR_DEVICENAME_NOT_FOUND:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_DEVICENAME_NOT_FOUND);
			break;

		case ERROR_NO_RESPONSES:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_NO_RESPONSES);
			break;

		case ERROR_NO_COMMAND_FOUND:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_NO_COMMAND_FOUND);
			break;

		case ERROR_WRONG_KEY_SPECIFIED:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_WRONG_KEY_SPECIFIED);
			break;

		case ERROR_UNKNOWN_DEVICE_TYPE:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_UNKNOWN_DEVICE_TYPE);
			break;

		case ERROR_ALLOCATING_MEMORY:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_ALLOCATING_MEMORY);
			break;

		case ERROR_PORT_NOT_CONFIGURED:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_PORT_NOT_CONFIGURED);
			break;

		case ERROR_DEVICE_NOT_READY:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_DEVICE_NOT_READY);
			break;

		case ERROR_READING_INI_FILE:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_READING_INI_FILE);
			break;

		case ERROR_NO_CONNECTION:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_NO_CONNECTION);
			break;

		case ERROR_BAD_USAGE_IN_INI_FILE:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_BAD_USAGE_IN_INI_FILE);
			break;

		case ERROR_READING_SECTIONNAME:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_READING_SECTIONNAME);
			break;

		case ERROR_READING_DEVICETYPE:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_READING_DEVICETYPE);
			break;

		case ERROR_READING_DEVICENAME:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_READING_DEVICENAME);
			break;

		case ERROR_READING_USAGE:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_READING_USAGE);
			break;

		case ERROR_READING_MAXCONNECTBPS:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_READING_MAXCONNECTBPS);
			break;

		case ERROR_READING_MAXCARRIERBPS:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_READING_MAXCARRIERBPS);
			break;

		case ERROR_LINE_BUSY:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_LINE_BUSY);
			break;

		case ERROR_VOICE_ANSWER:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_VOICE_ANSWER);
			break;

		case ERROR_NO_ANSWER:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_NO_ANSWER);
			break;

		case ERROR_NO_CARRIER:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_NO_CARRIER);
			break;

		case ERROR_NO_DIALTONE:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_NO_DIALTONE);
			break;

		case ERROR_IN_COMMAND:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_IN_COMMAND);
			break;

		case ERROR_WRITING_SECTIONNAME:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_WRITING_SECTIONNAME);
			break;

		case ERROR_WRITING_DEVICETYPE:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_WRITING_DEVICETYPE);
			break;

		case ERROR_WRITING_DEVICENAME:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_WRITING_DEVICENAME);
			break;

		case ERROR_WRITING_MAXCONNECTBPS:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_WRITING_MAXCONNECTBPS);
			break;

		case ERROR_WRITING_MAXCARRIERBPS:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_WRITING_MAXCARRIERBPS);
			break;

		case ERROR_WRITING_USAGE:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_WRITING_USAGE);
			break;

		case ERROR_WRITING_DEFAULTOFF:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_WRITING_DEFAULTOFF);
			break;

		case ERROR_READING_DEFAULTOFF:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_READING_DEFAULTOFF);
			break;

		case ERROR_EMPTY_INI_FILE:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_EMPTY_INI_FILE);
			break;

		case ERROR_AUTHENTICATION_FAILURE:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_AUTHENTICATION_FAILURE);
			break;

		case ERROR_PORT_OR_DEVICE:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_PORT_OR_DEVICE);
			break;

		case ERROR_NOT_BINARY_MACRO:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_NOT_BINARY_MACRO);
			break;

		case ERROR_DCB_NOT_FOUND:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_DCB_NOT_FOUND);
			break;

		case ERROR_STATE_MACHINES_NOT_STARTED:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_STATE_MACHINES_NOT_STARTED);
			break;

		case ERROR_STATE_MACHINES_ALREADY_STARTED:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_STATE_MACHINES_ALREADY_STARTED);
			break;

		case ERROR_PARTIAL_RESPONSE_LOOPING:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_PARTIAL_RESPONSE_LOOPING);
			break;

		case ERROR_UNKNOWN_RESPONSE_KEY:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_UNKNOWN_RESPONSE_KEY);
			break;

		case ERROR_RECV_BUF_FULL:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_RECV_BUF_FULL);
			break;

		case ERROR_CMD_TOO_LONG:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_CMD_TOO_LONG);
			break;

		case ERROR_UNSUPPORTED_BPS:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_UNSUPPORTED_BPS);
			break;

		case ERROR_UNEXPECTED_RESPONSE:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_UNEXPECTED_RESPONSE);
			break;

		case ERROR_INTERACTIVE_MODE:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_INTERACTIVE_MODE);
			break;

		case ERROR_BAD_CALLBACK_NUMBER:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_BAD_CALLBACK_NUMBER);
			break;

		case ERROR_INVALID_AUTH_STATE:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_INVALID_AUTH_STATE);
			break;

		case ERROR_WRITING_INITBPS:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_WRITING_INITBPS);
			break;

		case ERROR_X25_DIAGNOSTIC:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_X25_DIAGNOSTIC);
			break;

		case ERROR_ACCT_EXPIRED:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_ACCT_EXPIRED);
			break;

		case ERROR_CHANGING_PASSWORD:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_CHANGING_PASSWORD);
			break;

		case ERROR_OVERRUN:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_OVERRUN);
			break;

		case ERROR_RASMAN_CANNOT_INITIALIZE:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_RASMAN_CANNOT_INITIALIZE);
			break;

		case ERROR_BIPLEX_PORT_NOT_AVAILABLE:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_BIPLEX_PORT_NOT_AVAILABLE);
			break;

		case ERROR_NO_ACTIVE_ISDN_LINES:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_NO_ACTIVE_ISDN_LINES);
			break;

		case ERROR_NO_ISDN_CHANNELS_AVAILABLE:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_NO_ISDN_CHANNELS_AVAILABLE);
			break;

		case ERROR_TOO_MANY_LINE_ERRORS:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_TOO_MANY_LINE_ERRORS);
			break;

		case ERROR_IP_CONFIGURATION:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_IP_CONFIGURATION);
			break;

		case ERROR_NO_IP_ADDRESSES:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_NO_IP_ADDRESSES);
			break;

		case ERROR_PPP_TIMEOUT:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_PPP_TIMEOUT);
			break;

		case ERROR_PPP_REMOTE_TERMINATED:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_PPP_REMOTE_TERMINATED);
			break;

		case ERROR_PPP_NO_PROTOCOLS_CONFIGURED:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_PPP_NO_PROTOCOLS_CONFIGURED);
			break;

		case ERROR_PPP_NO_RESPONSE:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_PPP_NO_RESPONSE);
			break;

		case ERROR_PPP_INVALID_PACKET:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_PPP_INVALID_PACKET);
			break;

		case ERROR_PHONE_NUMBER_TOO_LONG:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_PHONE_NUMBER_TOO_LONG);
			break;

		case ERROR_IPXCP_NO_DIALOUT_CONFIGURED:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_IPXCP_NO_DIALOUT_CONFIGURED);
			break;

		case ERROR_IPXCP_NO_DIALIN_CONFIGURED:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_IPXCP_NO_DIALIN_CONFIGURED);
			break;

		case ERROR_IPXCP_DIALOUT_ALREADY_ACTIVE:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_IPXCP_DIALOUT_ALREADY_ACTIVE);
			break;

		case ERROR_ACCESSING_TCPCFGDLL:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_ACCESSING_TCPCFGDLL);
			break;

		case ERROR_NO_IP_RAS_ADAPTER:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_NO_IP_RAS_ADAPTER);
			break;

		case ERROR_SLIP_REQUIRES_IP:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_SLIP_REQUIRES_IP);
			break;

		case ERROR_PROJECTION_NOT_COMPLETE:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_PROJECTION_NOT_COMPLETE);
			break;

		case ERROR_PROTOCOL_NOT_CONFIGURED:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_PROTOCOL_NOT_CONFIGURED);
			break;

		case ERROR_PPP_NOT_CONVERGING:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_PPP_NOT_CONVERGING);
			break;

		case ERROR_PPP_CP_REJECTED:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_PPP_CP_REJECTED);
			break;

		case ERROR_PPP_LCP_TERMINATED:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_PPP_LCP_TERMINATED);
			break;

		case ERROR_PPP_REQUIRED_ADDRESS_REJECTED:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_PPP_REQUIRED_ADDRESS_REJECTED);
			break;

		case ERROR_PPP_NCP_TERMINATED:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_PPP_NCP_TERMINATED);
			break;

		case ERROR_PPP_LOOPBACK_DETECTED:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_PPP_LOOPBACK_DETECTED);
			break;

		case ERROR_PPP_NO_ADDRESS_ASSIGNED:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_PPP_NO_ADDRESS_ASSIGNED);
			break;

		case ERROR_CANNOT_USE_LOGON_CREDENTIALS:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_CANNOT_USE_LOGON_CREDENTIALS);
			break;

		case ERROR_TAPI_CONFIGURATION:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_TAPI_CONFIGURATION);
			break;

		case ERROR_NO_LOCAL_ENCRYPTION:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_NO_LOCAL_ENCRYPTION);
			break;

		case ERROR_NO_REMOTE_ENCRYPTION:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_NO_REMOTE_ENCRYPTION);
			break;

		case ERROR_REMOTE_REQUIRES_ENCRYPTION:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_REMOTE_REQUIRES_ENCRYPTION);
			break;

		case ERROR_IPXCP_NET_NUMBER_CONFLICT:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_IPXCP_NET_NUMBER_CONFLICT);
			break;

		case ERROR_INVALID_SMM:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_INVALID_SMM);
			break;

		case ERROR_SMM_UNINITIALIZED:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_SMM_UNINITIALIZED);
			break;

		case ERROR_NO_MAC_FOR_PORT:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_NO_MAC_FOR_PORT);
			break;

		case ERROR_SMM_TIMEOUT:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_SMM_TIMEOUT);
			break;

		case ERROR_BAD_PHONE_NUMBER:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_BAD_PHONE_NUMBER);
			break;

		case ERROR_WRONG_MODULE:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_WRONG_MODULE);
			break;

		case ERROR_PPP_MAC:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_PPP_MAC);
			break;

		case ERROR_PPP_LCP:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_PPP_LCP);
			break;

		case ERROR_PPP_AUTH:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_PPP_AUTH);
			break;

		case ERROR_PPP_NCP:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_PPP_NCP);
			break;

		case ERROR_POWER_OFF:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_POWER_OFF);
			break;

		case ERROR_POWER_OFF_CD:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_POWER_OFF_CD);
			break;

		case ERROR_DIAL_ALREADY_IN_PROGRESS:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_DIAL_ALREADY_IN_PROGRESS);
			break;

		case ERROR_RASAUTO_CANNOT_INITIALIZE:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_RASAUTO_CANNOT_INITIALIZE);
			break;

		case ERROR_UNABLE_TO_AUTHENTICATE_SERVER:
			sResult = CString((LPCTSTR)IDS_RAS_ERROR_UNABLE_TO_AUTHENTICATE_SERVER);
			break;
	}
	return sResult;
}

void CChldView5::OnSize(UINT nType, int cx, int cy)
{
	CChldView::OnSize(nType, cx, cy);

	if (cx == 0 &&
		cy == 0)
	{
		return;
	}

	if (m_ctrlRas.GetSafeHwnd() == NULL ||
		m_ctrlMsg.GetSafeHwnd() == NULL)
	{
		return;
	}

	CRect r;
	GetClientRect(&r);

	CGlobals::ResizeControl(&m_ctrlRas, r.right - 1);
	CGlobals::ResizeControl(&m_ctrlMsg, r.right - 1, r.bottom - 1);
}

void CChldView5::OnDestroy()
{
	BackupProperties();

	CChldView::OnDestroy();
}

void CChldView5::BackupProperties()
{
	m_Column0Width.SetValue(m_ctrlMsg.GetColumnWidth(0));
	m_Column1Width.SetValue(m_ctrlMsg.GetColumnWidth(1));
	m_LastRas.SetValue(CGlobals::GetWindowText(&m_ctrlRas));
}

/////////////////////////////////////////////////////////////////////////////
// CChldView6

CChldView6::CChldView6() :
	CChldView(CChldView6::IDD),
	m_LastAddress(CConnMgrApp::s_pPropertySerializer, _T("TcpConn\\Address"), _T("")),
	m_LastPort(CConnMgrApp::s_pPropertySerializer, _T("TcpConn\\Port"), _T("")),
	m_LastIpv4(CConnMgrApp::s_pPropertySerializer, _T("TcpConn\\Ipv4"), true),
	m_LastIpv6(CConnMgrApp::s_pPropertySerializer, _T("TcpConn\\Ipv6"), false),
	m_LastAsync(CConnMgrApp::s_pPropertySerializer, _T("TcpConn\\Async"), false),
	m_LastTimeout(CConnMgrApp::s_pPropertySerializer, _T("TcpConn\\Timeout"), _T(""))
{
	//{{AFX_DATA_INIT(CChldView6)
	//}}AFX_DATA_INIT
}

void CChldView6::DoDataExchange(CDataExchange* pDX)
{
	CChldView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CChldView6)
	DDX_Control(pDX, IDC_CHECK_ASYNC, m_ctrlAsync);
	DDX_Control(pDX, IDC_EDIT_TIMEOUT, m_ctrlTimeout);
	DDX_Control(pDX, IDC_EDIT_PORT, m_ctrlPort);
	DDX_Control(pDX, IDC_EDIT_ADDR, m_ctrlAddress);
	DDX_Control(pDX, IDC_RADIO_IPV4, m_ctrlIpv4);
	DDX_Control(pDX, IDC_RADIO_IPV6, m_ctrlIpv6);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CChldView6, CChldView)
	//{{AFX_MSG_MAP(CChldView6)
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_BUTTON_CONN, OnButtonConn)
	ON_BN_CLICKED(IDC_CHECK_ASYNC, OnCheckAsync)
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CChldView6 message handlers

void CChldView6::UpdateControls()
{
	if (IsAsyncMode())
		m_ctrlTimeout.EnableWindow(TRUE);
	else
	{
		m_ctrlTimeout.SetWindowText(_T(""));
		m_ctrlTimeout.EnableWindow(FALSE);
	}
}

void CChldView6::OnSize(UINT nType, int cx, int cy)
{
	CChldView::OnSize(nType, cx, cy);

	if (cx == 0 &&
		cy == 0)
	{
		return;
	}

	if (m_ctrlAddress.GetSafeHwnd() == NULL)
	{
		return;
	}

	CRect r;
	GetClientRect(&r);

	CGlobals::ResizeControl(&m_ctrlAddress, r.right - 1);
}

#ifndef SD_BOTH
#define SD_BOTH         0x02
#endif

void CChldView6::OnButtonConn()
{
	CString sAddress(CGlobals::GetWindowText(&m_ctrlAddress));
	CString sPort(CGlobals::GetWindowText(&m_ctrlPort));
	short nPort = CGlobals::FromString(sPort);
	int nTimeout = 0;
	bool bAsync = IsAsyncMode();
	if (bAsync)
	{
		CString sTimeout(CGlobals::GetWindowText(&m_ctrlTimeout));
		nTimeout = CGlobals::FromString(sTimeout);
	}
	if (sAddress.IsEmpty() ||
		nPort == 0 ||
		(bAsync && nTimeout == 0))
	{
		return;
	}

	CWaitCursor wait;
	int nFamily = GetCurrAddrFamily();
	SOCKET sock = ::socket(nFamily, SOCK_STREAM, IPPROTO_TCP);
	if (sock != INVALID_SOCKET)
	{
		if (bAsync)
		{
			u_long nNonblock = 1;
			if (::ioctlsocket(sock, FIONBIO, &nNonblock) == SOCKET_ERROR)
			{
				CGlobals::PrintLastError(_T("ioctlsocket(FIONBIO)"), ::WSAGetLastError());
				wait.Restore();
			}
		}
		char szNode[NI_MAXHOST + 1];
		wcstombs(szNode, (LPCTSTR)sAddress, sizeof(szNode));
		char szServ[7];
		sprintf(szServ, "%hu", nPort);
		ADDRINFO hints, *res = NULL;
		ZeroMemory(&hints, sizeof(hints));
		hints.ai_family = nFamily;
		if (::getaddrinfo(
			szNode,
			szServ,
			&hints,
			&res))
		{
			CGlobals::PrintLastError(_T("getaddrinfo()"), ::WSAGetLastError());
		}
		else
		{
			if (res != NULL)
			{
				if (::connect(sock, res->ai_addr, res->ai_addrlen) == SOCKET_ERROR)
				{
					int nErr = ::WSAGetLastError();
					if (nErr == WSAEWOULDBLOCK && bAsync)
					{
						fd_set fdsRd, fdsWr, fdsErr;
						FD_ZERO(&fdsRd);
						FD_ZERO(&fdsWr);
						FD_ZERO(&fdsErr);
						FD_SET(sock, &fdsRd);
						FD_SET(sock, &fdsWr);
						FD_SET(sock, &fdsErr);
						int nHandles;
						timeval tv = { nTimeout, 0 };
						nHandles = ::select(0, &fdsRd, &fdsWr, &fdsErr, &tv);
						if (nHandles == SOCKET_ERROR)
						{
							CGlobals::PrintLastError(_T("select()"), ::WSAGetLastError());
							wait.Restore();
						}
						else if (nHandles == 0)
						{
							AfxMessageBox(_T("Timeout expired"));
							wait.Restore();
						}
						else
						{
							bool bRd = FD_ISSET(sock, &fdsRd) != 0;
							bool bWr = FD_ISSET(sock, &fdsWr) != 0;
							bool bErr = FD_ISSET(sock, &fdsErr) != 0;
							if (bErr || bRd)
							{
								AfxMessageBox(_T("Failed"));
								wait.Restore();
							}
							if (bWr)
							{
								::shutdown(sock, SD_BOTH);
								AfxMessageBox(_T("O.K."), MB_OK | MB_ICONINFORMATION);
								wait.Restore();
							}
						}
					}
					else
					{
						CGlobals::PrintLastError(_T("connect()"), nErr);
						wait.Restore();
					}
				}
				else
				{
					::shutdown(sock, SD_BOTH);
					AfxMessageBox(_T("O.K."), MB_OK | MB_ICONINFORMATION);
					wait.Restore();
				}
			}
			::freeaddrinfo(res);
		}

		::closesocket(sock);
	}
	else
	{
		CGlobals::PrintLastError(_T("socket()"), ::WSAGetLastError());
		wait.Restore();
	}
	BackupProperties();
}

void CChldView6::OnCheckAsync()
{
	UpdateControls();
	if (IsAsyncMode())
		m_ctrlTimeout.SetFocus();
}

bool CChldView6::IsAsyncMode()
{
	return m_ctrlAsync.GetCheck() == BST_CHECKED ? true : false;
}

void CChldView6::OnInitialUpdate()
{
	CChldView::OnInitialUpdate();

	m_ctrlAddress.SetWindowText(m_LastAddress.GetValue());
	m_ctrlPort.SetWindowText(m_LastPort.GetValue());
	m_ctrlIpv4.SetCheck(m_LastIpv4.GetValue() ? BST_CHECKED : BST_UNCHECKED);
	m_ctrlIpv6.SetCheck(m_LastIpv6.GetValue() ? BST_CHECKED : BST_UNCHECKED);
	m_ctrlAsync.SetCheck(m_LastAsync.GetValue() ? BST_CHECKED : BST_UNCHECKED);
	m_ctrlTimeout.SetWindowText(m_LastTimeout.GetValue());

	UpdateControls();
}

void CChldView6::OnDestroy()
{
	BackupProperties();

	CChldView::OnDestroy();
}

void CChldView6::BackupProperties()
{
	m_LastAddress.SetValue(CGlobals::GetWindowText(&m_ctrlAddress));
	m_LastPort.SetValue(CGlobals::GetWindowText(&m_ctrlPort));
	m_LastAsync.SetValue(IsAsyncMode());
	m_LastTimeout.SetValue(CGlobals::GetWindowText(&m_ctrlTimeout));
	m_LastIpv4.SetValue(m_ctrlIpv4.GetCheck() == BST_CHECKED);
	m_LastIpv6.SetValue(m_ctrlIpv6.GetCheck() == BST_CHECKED);
}

int CChldView6::GetCurrAddrFamily()
{
	if (m_ctrlIpv6.GetCheck() == BST_CHECKED)
		return AF_INET6;
	return AF_INET;
}

/////////////////////////////////////////////////////////////////////////////
// CChldView7

CChldView7::CChldView7() :
	CChldView(CChldView7::IDD),
	m_AddrColumn0Width(CConnMgrApp::s_pPropertySerializer, _T("Dns\\Address\\Column0"), 400),
	m_LastHost(CConnMgrApp::s_pPropertySerializer, _T("Dns\\Host"), _T("")),
	m_LastIpv4(CConnMgrApp::s_pPropertySerializer, _T("Dns\\Ipv4"), true),
	m_LastIpv6(CConnMgrApp::s_pPropertySerializer, _T("Dns\\Ipv6"), false)
{
	//{{AFX_DATA_INIT(CChldView7)
	//}}AFX_DATA_INIT
}

void CChldView7::DoDataExchange(CDataExchange* pDX)
{
	CChldView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CChldView7)
	DDX_Control(pDX, IDC_LIST_ADDRESS, m_ctrlAddress);
	DDX_Control(pDX, IDC_EDIT_HOST, m_ctrlHost);
	DDX_Control(pDX, IDC_RADIO_IPV4, m_ctrlIpv4);
	DDX_Control(pDX, IDC_RADIO_IPV6, m_ctrlIpv6);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CChldView7, CChldView)
	//{{AFX_MSG_MAP(CChldView7)
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_BUTTON_QUERY, OnButtonQuery)
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CChldView7 message handlers

void CChldView7::OnInitialUpdate()
{
	CChldView::OnInitialUpdate();

	m_ctrlAddress.InsertColumn(0, CString((LPCTSTR)IDS_DNSADDR_COLUMN0), LVCFMT_LEFT, m_AddrColumn0Width.GetValue());
	m_ctrlAddress.SetExtendedStyle(m_ctrlAddress.GetExtendedStyle() | LVS_EX_FULLROWSELECT | LVS_EX_HEADERDRAGDROP);
	m_ctrlHost.SetWindowText(m_LastHost.GetValue());
	m_ctrlIpv4.SetCheck(m_LastIpv4.GetValue() ? BST_CHECKED : BST_UNCHECKED);
	m_ctrlIpv6.SetCheck(m_LastIpv6.GetValue() ? BST_CHECKED : BST_UNCHECKED);
}

void CChldView7::OnSize(UINT nType, int cx, int cy)
{
	CChldView::OnSize(nType, cx, cy);

	if (cx == 0 &&
		cy == 0)
	{
		return;
	}

	if (m_ctrlHost.GetSafeHwnd() == NULL ||
		m_ctrlAddress.GetSafeHwnd() == NULL)
	{
		return;
	}

	CRect r;
	GetClientRect(&r);

	CGlobals::ResizeControl(&m_ctrlHost, r.right - 1);
	CGlobals::ResizeControl(&m_ctrlAddress, r.right - 1, r.bottom - 1);
}

void CChldView7::OnButtonQuery()
{
	m_ctrlAddress.DeleteAllItems();
	CWaitCursor c;
	FillValues();

	BackupProperties();
}

void CChldView7::FillValues()
{
	CString sAddress(CGlobals::GetWindowText(&m_ctrlHost));
	char szName[256];
	if (sAddress.IsEmpty())
	{
		if (::gethostname(szName, sizeof(szName)))
		{
			CGlobals::PrintLastError(_T("gethostname()"), ::WSAGetLastError());
			return;
		}
	}
	else
		wcstombs(szName, (LPCTSTR)sAddress, sizeof(szName));
	addrinfo hints, *res = NULL;
	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = GetCurrAddrFamily();
	if (::getaddrinfo(
		szName,
		NULL,
		&hints,
		&res))
	{
		CGlobals::PrintLastError(_T("getaddrinfo()"), ::WSAGetLastError());
		return;
	}
	else
	{
		while (res != NULL)
		{
			m_ctrlAddress.InsertItem(m_ctrlAddress.GetItemCount(), CWinSock::GetAddrString(res->ai_addr, res->ai_addrlen));
			res = res->ai_next;
		}
		::freeaddrinfo(res);
	}
}

void CChldView7::OnDestroy()
{
	BackupProperties();

	CChldView::OnDestroy();
}

void CChldView7::BackupProperties()
{
	m_AddrColumn0Width.SetValue(m_ctrlAddress.GetColumnWidth(0));
	m_LastHost.SetValue(CGlobals::GetWindowText(&m_ctrlHost));
	m_LastIpv4.SetValue(m_ctrlIpv4.GetCheck() == BST_CHECKED);
	m_LastIpv6.SetValue(m_ctrlIpv6.GetCheck() == BST_CHECKED);
}

int CChldView7::GetCurrAddrFamily()
{
	if (m_ctrlIpv6.GetCheck() == BST_CHECKED)
		return AF_INET6;
	return AF_INET;
}

/////////////////////////////////////////////////////////////////////////////
// CChldView8

CChldView8::CChldView8() :
	CChldView(CChldView8::IDD),
	m_Column0Width(CConnMgrApp::s_pPropertySerializer, _T("Ping\\Column0"), 100),
	m_Column1Width(CConnMgrApp::s_pPropertySerializer, _T("Ping\\Column1"), 75),
	m_Column2Width(CConnMgrApp::s_pPropertySerializer, _T("Ping\\Column2"), 45),
	m_LastHost(CConnMgrApp::s_pPropertySerializer, _T("Ping\\Host"), _T(""))
{
	//{{AFX_DATA_INIT(CChldView8)
	//}}AFX_DATA_INIT
	m_bWorking = false;
	m_Socket = INVALID_SOCKET;
	m_hIcmp = INVALID_HANDLE_VALUE;
	m_bIcmpPing = false;
}

void CChldView8::DoDataExchange(CDataExchange* pDX)
{
	CChldView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CChldView8)
	DDX_Control(pDX, IDC_LIST_MSG, m_ctrlMsg);
	DDX_Control(pDX, IDC_EDIT_HOST, m_ctrlHost);
	DDX_Control(pDX, IDC_BUTTON_STOP, m_ctrlStop);
	DDX_Control(pDX, IDC_BUTTON_START, m_ctrlStart);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CChldView8, CChldView)
	//{{AFX_MSG_MAP(CChldView8)
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_BUTTON_START, OnButtonStart)
	ON_BN_CLICKED(IDC_BUTTON_STOP, OnButtonStop)
	ON_WM_DESTROY()
	ON_WM_TIMER()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CChldView8 message handlers

void CChldView8::OnInitialUpdate()
{
	CChldView::OnInitialUpdate();

	m_ctrlMsg.InsertColumn(0, CString((LPCTSTR)IDS_PING_COLUMN0), LVCFMT_LEFT, m_Column0Width.GetValue());
	m_ctrlMsg.InsertColumn(1, CString((LPCTSTR)IDS_PING_COLUMN1), LVCFMT_RIGHT, m_Column1Width.GetValue());
	m_ctrlMsg.InsertColumn(2, CString((LPCTSTR)IDS_PING_COLUMN2), LVCFMT_RIGHT, m_Column2Width.GetValue());
	m_ctrlMsg.SetExtendedStyle(m_ctrlMsg.GetExtendedStyle() | LVS_EX_FULLROWSELECT | LVS_EX_HEADERDRAGDROP);
	m_ctrlHost.SetWindowText(m_LastHost.GetValue());

	OSVERSIONINFO osvi;
	osvi.dwOSVersionInfoSize = sizeof(osvi);
	::GetVersionEx(&osvi);
	if (osvi.dwMajorVersion <= 4)
		m_bIcmpPing = true;

	UpdateControls();
}

void CChldView8::OnSize(UINT nType, int cx, int cy)
{
	CChldView::OnSize(nType, cx, cy);

	if (cx == 0 &&
		cy == 0)
	{
		return;
	}

	if (m_ctrlHost.GetSafeHwnd() == NULL ||
		m_ctrlMsg.GetSafeHwnd() == NULL)
	{
		return;
	}

	CRect r;
	GetClientRect(&r);

	CGlobals::ResizeControl(&m_ctrlHost, r.right - 1);
	CGlobals::ResizeControl(&m_ctrlMsg, r.right - 1, r.bottom - 1);
}

void CChldView8::OnButtonStart()
{
	CString sHost(CGlobals::GetWindowText(&m_ctrlHost));
	if (sHost.IsEmpty())
		return;

	if (m_bIcmpPing)
	{
		m_hIcmp = ::IcmpCreateFile();
		if (m_hIcmp == INVALID_HANDLE_VALUE)
		{
			CGlobals::PrintLastError(_T("IcmpCreateFile"), ::WSAGetLastError());
			return;
		}
	}
	else
	{
		// Create a Raw socket
		m_Socket = ::socket(AF_INET, SOCK_RAW, IPPROTO_ICMP);
		if (m_Socket == INVALID_SOCKET)
		{
			CGlobals::PrintLastError(_T("socket"), ::WSAGetLastError());
			return;
		}
	}

	char szHost[256];
	wcstombs(szHost, (LPCTSTR)sHost, sizeof(szHost));

	// Lookup host
	u_long nAddr = ::inet_addr(szHost);
	if (nAddr == INADDR_NONE)
	{
		LPHOSTENT lpHost = ::gethostbyname(szHost);
		if (lpHost == NULL)
		{
			CGlobals::PrintLastError(_T("gethostbyname"), ::WSAGetLastError());
			return;
		}
		nAddr = *((u_long *)(lpHost->h_addr));
	}

	// Setup destination socket address
	ZeroMemory(&m_saDest, sizeof(m_saDest));
	m_saDest.sin_addr.s_addr = nAddr;
	m_saDest.sin_family = AF_INET;

	m_bWorking = true;
	UpdateControls();
	m_ctrlMsg.DeleteAllItems();

	m_nId = (u_short)::GetCurrentProcessId();
	m_nSeq = 1;

	Echo();

	BackupProperties();
}

void CChldView8::OnButtonStop()
{
	Close();

	m_bWorking = false;
	UpdateControls();
}

void CChldView8::OnDestroy()
{
	BackupProperties();

	CChldView::OnDestroy();

	Close();
}

void CChldView8::BackupProperties()
{
	m_Column0Width.SetValue(m_ctrlMsg.GetColumnWidth(0));
	m_Column1Width.SetValue(m_ctrlMsg.GetColumnWidth(1));
	m_Column2Width.SetValue(m_ctrlMsg.GetColumnWidth(2));
	m_LastHost.SetValue(CGlobals::GetWindowText(&m_ctrlHost));
}

void CChldView8::Close()
{
	KillTimer(PING_TIMER);

	if (m_bIcmpPing)
	{
		if (m_hIcmp != INVALID_HANDLE_VALUE)
		{
			if (::IcmpCloseHandle(m_hIcmp) == FALSE)
				CGlobals::PrintLastError(_T("IcmpCloseHandle"), ::WSAGetLastError());
			m_hIcmp = INVALID_HANDLE_VALUE;
		}
	}
	else
	{
		if (m_Socket != INVALID_SOCKET)
		{
			if (::closesocket(m_Socket) == SOCKET_ERROR)
				CGlobals::PrintLastError(_T("closesocket"), ::WSAGetLastError());
			m_Socket = INVALID_SOCKET;
		}
	}
}

void CChldView8::UpdateControls()
{
	m_ctrlHost.EnableWindow(m_bWorking ? FALSE : TRUE);
	m_ctrlStart.EnableWindow(m_bWorking ? FALSE : TRUE);
	m_ctrlStop.EnableWindow(m_bWorking ? TRUE : FALSE);
}

void CChldView8::OnTimer(UINT nIDEvent)
{
	if (nIDEvent == PING_TIMER)
	{
		KillTimer(PING_TIMER);

		Echo();
	}

	CChldView::OnTimer(nIDEvent);
}

void CChldView8::ScheduleNext()
{
	SetTimer(PING_TIMER, PING_TIMER_ELAPSE, NULL);
	m_nSeq++;
}

void CChldView8::Echo()
{
	if (m_bIcmpPing)
	{
		ICMP_REQUEST_DATA req;
		req.ID = m_nId;
		req.Seq = m_nSeq;
		ICMP_REPLY_DATA rep;
		ZeroMemory(&rep, sizeof(rep));
		if (::IcmpSendEcho(
			m_hIcmp,
			m_saDest.sin_addr.s_addr,
			(LPVOID)&req,
			sizeof(req),
			NULL,
			(LPVOID)&rep,
			sizeof(rep),
			WAIT_REPLY_ELAPSE) > 0)
		{
			if (rep.reply.Options.Tos == ICMP_ECHOREPLY &&
				((ICMP_REQUEST_DATA *)rep.reply.Data)->ID == req.ID &&
				((ICMP_REQUEST_DATA *)rep.reply.Data)->Seq == req.Seq)
			{
				in_addr a;
				a.s_addr = rep.reply.Address;
				AppendMessage(
					::inet_ntoa(a),
					rep.reply.RoundTripTime,
					rep.reply.Options.Ttl);
			}
		}
		else
			AppendMessage(_T("Timeout."));
	}
	else
	{
		SendRequest();
		DWORD dwStart = ::GetTickCount(), dwRecv;
		SOCKADDR_IN saSrc;
		u_char nTtl;
		while (true)
		{
			long nWaitMsec = dwStart + WAIT_REPLY_ELAPSE - ::GetTickCount();
			if (nWaitMsec < 0)
				nWaitMsec = 0;
			if (!WaitForReply(nWaitMsec))
			{
				AppendMessage(_T("Timeout."));
				break;
			}

			if (RecvReply(&saSrc, &nTtl, &dwRecv))
			{
				AppendMessage(
					::inet_ntoa(saSrc.sin_addr),
					GetTickCount() - dwRecv,
					nTtl);
				break;
			}
		}
	}

	ScheduleNext();
}

void CChldView8::SendRequest()
{
	ECHOREQUEST echoReq;

	// Fill in echo request
	echoReq.icmpHdr.Type = ICMP_ECHOREQ;
	echoReq.icmpHdr.Code = 0;
	echoReq.icmpHdr.Checksum = 0;
	echoReq.icmpHdr.ID = m_nId;
	echoReq.icmpHdr.Seq = m_nSeq;

	// Save tick count when sent
	echoReq.dwTime = GetTickCount();

	// Put data in packet and compute checksum
	echoReq.icmpHdr.Checksum = in_cksum((u_short *)&echoReq, sizeof(ECHOREQUEST));

	// Send the echo request  								  
	if (::sendto(
			m_Socket,						/* socket */
			(LPSTR)&echoReq,			/* buffer */
			sizeof(echoReq),
			0,							/* flags */
			(LPSOCKADDR)&m_saDest, /* destination */
			sizeof(m_saDest))   /* address length */
		== SOCKET_ERROR)
	{
		CGlobals::PrintLastError(_T("sendto"), ::WSAGetLastError());
	}
}

//
// Mike Muuss' in_cksum() function
// and his comments from the original
// ping program
//
// * Author -
// *	Mike Muuss
// *	U. S. Army Ballistic Research Laboratory
// *	December, 1983

/*
 *			I N _ C K S U M
 *
 * Checksum routine for Internet Protocol family headers (C Version)
 *
 */
u_short CChldView8::in_cksum(u_short *addr, int len)
{
	register int nleft = len;
	register u_short *w = addr;
	register u_short answer;
	register int sum = 0;

	/*
	 *  Our algorithm is simple, using a 32 bit accumulator (sum),
	 *  we add sequential 16 bit words to it, and at the end, fold
	 *  back all the carry bits from the top 16 bits into the lower
	 *  16 bits.
	 */
	while( nleft > 1 )  {
		sum += *w++;
		nleft -= 2;
	}

	/* mop up an odd byte, if necessary */
	if( nleft == 1 ) {
		u_short	u = 0;

		*(u_char *)(&u) = *(u_char *)w ;
		sum += u;
	}

	/*
	 * add back carry outs from top 16 bits to low 16 bits
	 */
	sum = (sum >> 16) + (sum & 0xffff);	/* add hi 16 to low 16 */
	sum += (sum >> 16);			/* add carry */
	answer = ~sum;				/* truncate to 16 bits */
	return (answer);
}

bool CChldView8::WaitForReply(long nWaitMsec)
{
	struct timeval Timeout;
	fd_set readfds;

	FD_ZERO(&readfds);
	FD_SET(m_Socket, &readfds);
	Timeout.tv_sec = nWaitMsec / 1000;
    Timeout.tv_usec = (nWaitMsec % 1000) * 1000;

	switch (::select(1, &readfds, NULL, NULL, &Timeout))
	{
		case SOCKET_ERROR:
			CGlobals::PrintLastError(_T("select"), ::WSAGetLastError());
			return false;

		case 0:
			return false;
	}

	return true;
}

void CChldView8::AppendMessage(LPCTSTR szMessage)
{
	int nItem = m_ctrlMsg.GetItemCount();
	m_ctrlMsg.InsertItem(nItem, szMessage);
	m_ctrlMsg.EnsureVisible(nItem, FALSE);
}

void CChldView8::AppendMessage(char *szAddr, DWORD dwElapse, int nTTL)
{
	int nItem = m_ctrlMsg.GetItemCount();
	CString s;
	s.Format(_T("%hs"), szAddr);
	m_ctrlMsg.InsertItem(nItem, s);
	m_ctrlMsg.SetItemText(nItem, 1, CGlobals::ToString(dwElapse));
	m_ctrlMsg.SetItemText(nItem, 2, CGlobals::ToString(nTTL));
	m_ctrlMsg.EnsureVisible(nItem, FALSE);
}

bool CChldView8::RecvReply(SOCKADDR_IN *psaFrom, u_char *pTTL, DWORD *pdwTime)
{
	ECHOREPLY echoReply;
	int nAddrLen = sizeof(SOCKADDR_IN);

	// Receive the echo reply
	if (::recvfrom(
			m_Socket,					// socket
			(LPSTR)&echoReply,	// buffer
			sizeof(echoReply),	// size of buffer
			0,					// flags
			(LPSOCKADDR)psaFrom,	// From address
			&nAddrLen)			// pointer to address len
		== SOCKET_ERROR)
	{
		CGlobals::PrintLastError(_T("recvfrom"), ::WSAGetLastError());
	}

	if (echoReply.echoRequest.icmpHdr.Type != ICMP_ECHOREPLY ||
		echoReply.echoRequest.icmpHdr.ID != m_nId ||
		echoReply.echoRequest.icmpHdr.Seq != m_nSeq)
	{
		return false;
	}

	// return time sent and IP TTL
	*pTTL = echoReply.ipHdr.TTL;
	*pdwTime = echoReply.echoRequest.dwTime;
	return true;
}

/////////////////////////////////////////////////////////////////////////////
// CChldView9

CChldView9::CChldView9() :
	CChldView(CChldView9::IDD),
	m_Column0Width(CConnMgrApp::s_pPropertySerializer, _T("Adapters\\Column0"), 100),
	m_Column1Width(CConnMgrApp::s_pPropertySerializer, _T("Adapters\\Column1"), 150),
	m_Column2Width(CConnMgrApp::s_pPropertySerializer, _T("Adapters\\Column2"), 50),
	m_Column3Width(CConnMgrApp::s_pPropertySerializer, _T("Adapters\\Column3"), 100),
	m_Column4Width(CConnMgrApp::s_pPropertySerializer, _T("Adapters\\Column4"), 100),
	m_Column5Width(CConnMgrApp::s_pPropertySerializer, _T("Adapters\\Column5"), 100),
	m_Column6Width(CConnMgrApp::s_pPropertySerializer, _T("Adapters\\Column6"), 100)
{
	//{{AFX_DATA_INIT(CChldView9)
	//}}AFX_DATA_INIT
}

void CChldView9::DoDataExchange(CDataExchange* pDX)
{
	CChldView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CChldView9)
	DDX_Control(pDX, IDC_LIST_ADAPTERS, m_ctrlAdapters);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CChldView9, CChldView)
	//{{AFX_MSG_MAP(CChldView9)
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_BUTTON_REFRESH, OnButtonRefresh)
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CChldView9 message handlers

void CChldView9::OnInitialUpdate()
{
	CChldView::OnInitialUpdate();

	m_ctrlAdapters.InsertColumn(0, CString((LPCTSTR)IDS_ADAPTERS_COLUMN0), LVCFMT_LEFT, m_Column0Width.GetValue());
	m_ctrlAdapters.InsertColumn(1, CString((LPCTSTR)IDS_ADAPTERS_COLUMN1), LVCFMT_LEFT, m_Column1Width.GetValue());
	m_ctrlAdapters.InsertColumn(2, CString((LPCTSTR)IDS_ADAPTERS_COLUMN2), LVCFMT_LEFT, m_Column2Width.GetValue());
	m_ctrlAdapters.InsertColumn(3, CString((LPCTSTR)IDS_ADAPTERS_COLUMN3), LVCFMT_LEFT, m_Column3Width.GetValue());
	m_ctrlAdapters.InsertColumn(4, CString((LPCTSTR)IDS_ADAPTERS_COLUMN4), LVCFMT_LEFT, m_Column4Width.GetValue());
	m_ctrlAdapters.InsertColumn(5, CString((LPCTSTR)IDS_ADAPTERS_COLUMN5), LVCFMT_LEFT, m_Column5Width.GetValue());
	m_ctrlAdapters.InsertColumn(6, CString((LPCTSTR)IDS_ADAPTERS_COLUMN6), LVCFMT_LEFT, m_Column6Width.GetValue());
	m_ctrlAdapters.SetExtendedStyle(m_ctrlAdapters.GetExtendedStyle() | LVS_EX_FULLROWSELECT | LVS_EX_HEADERDRAGDROP);

	Fill();
}

void CChldView9::OnSize(UINT nType, int cx, int cy)
{
	CChldView::OnSize(nType, cx, cy);

	if (cx == 0 &&
		cy == 0)
	{
		return;
	}

	if (m_ctrlAdapters.GetSafeHwnd() == NULL)
	{
		return;
	}

	CRect r;
	GetClientRect(&r);

	CGlobals::ResizeControl(&m_ctrlAdapters, r.right - 1, r.bottom - 1);
}

void CChldView9::OnButtonRefresh()
{
	Fill();

	BackupProperties();
}

void CChldView9::Fill()
{
	m_ctrlAdapters.DeleteAllItems();
	IP_ADAPTER_INFO *pBuffer = NULL;
	ULONG nSize = 0;
	DWORD dwErr = ::GetAdaptersInfo(pBuffer, &nSize);
	if (dwErr == ERROR_BUFFER_OVERFLOW)
	{
		IP_ADAPTER_INFO *pAdapters = pBuffer = (IP_ADAPTER_INFO *)new BYTE[nSize];
		dwErr = ::GetAdaptersInfo(pBuffer, &nSize);
		if (dwErr == NO_ERROR)
		{
#ifdef _DEBUG
			::OutputDebugString(_T("GetAdaptersInfo()\n"));
#endif
			while (pAdapters != NULL)
			{
				int nItem = m_ctrlAdapters.GetItemCount();
				CString s;
				s.Format(_T("%hs"), pAdapters->AdapterName);
				m_ctrlAdapters.InsertItem(nItem, s);
				m_ctrlAdapters.SetItemText(
					nItem,
					1,
					CGlobals::MacAddress(pAdapters->Address, pAdapters->AddressLength));
				m_ctrlAdapters.SetItemText(
					nItem,
					2,
					AdapterTypeText(pAdapters->Type));
				s.Format(_T("%hs"), pAdapters->CurrentIpAddress->IpAddress.String);
				m_ctrlAdapters.SetItemText(
					nItem,
					3,
					s);
				s.Format(_T("%hs"), pAdapters->CurrentIpAddress->IpMask.String);
				m_ctrlAdapters.SetItemText(
					nItem,
					4,
					s);
				s.Format(_T("%hs"), pAdapters->GatewayList.IpAddress.String);
				m_ctrlAdapters.SetItemText(
					nItem,
					5,
					s);
				s.Format(_T("%hs"), pAdapters->DhcpServer.IpAddress.String);
				m_ctrlAdapters.SetItemText(
					nItem,
					6,
					s);

				pAdapters = pAdapters->Next;
			}
		}
		else
		{
			CGlobals::PrintLastError(_T("GetAdaptersInfo"), dwErr);
		}
		delete []pBuffer;
	}
}

void CChldView9::OnDestroy()
{
	BackupProperties();

	CChldView::OnDestroy();
}

void CChldView9::BackupProperties()
{
	m_Column0Width.SetValue(m_ctrlAdapters.GetColumnWidth(0));
	m_Column1Width.SetValue(m_ctrlAdapters.GetColumnWidth(1));
	m_Column2Width.SetValue(m_ctrlAdapters.GetColumnWidth(2));
	m_Column3Width.SetValue(m_ctrlAdapters.GetColumnWidth(3));
	m_Column4Width.SetValue(m_ctrlAdapters.GetColumnWidth(4));
	m_Column5Width.SetValue(m_ctrlAdapters.GetColumnWidth(5));
	m_Column6Width.SetValue(m_ctrlAdapters.GetColumnWidth(6));
}

CString CChldView9::AdapterTypeText(UINT nType)
{
	CString sResult;
	switch (nType)
	{
		case MIB_IF_TYPE_OTHER:
			sResult = CString((LPCTSTR)IDS_ADAPTER_TYPE_OTHER);
			break;

		case MIB_IF_TYPE_ETHERNET:
			sResult = CString((LPCTSTR)IDS_ADAPTER_TYPE_ETHERNET);
			break;

		case MIB_IF_TYPE_TOKENRING:
			sResult = CString((LPCTSTR)IDS_ADAPTER_TYPE_TOKENRING);
			break;

		case MIB_IF_TYPE_FDDI:
			sResult = CString((LPCTSTR)IDS_ADAPTER_TYPE_FDDI);
			break;

		case MIB_IF_TYPE_PPP:
			sResult = CString((LPCTSTR)IDS_ADAPTER_TYPE_PPP);
			break;

		case MIB_IF_TYPE_LOOPBACK:
			sResult = CString((LPCTSTR)IDS_ADAPTER_TYPE_LOOPBACK);
			break;

		case MIB_IF_TYPE_SLIP:
			sResult = CString((LPCTSTR)IDS_ADAPTER_TYPE_SLIP);
			break;

		default:
			sResult.Format(IDS_UNKNOWN, nType);
	}
	return sResult;
}

/////////////////////////////////////////////////////////////////////////////
// CChldView10

CChldView10::CChldView10() :
	CChldView(CChldView10::IDD),
	m_Column0Width(CConnMgrApp::s_pPropertySerializer, _T("IpAddress\\Column0"), 100),
	m_Column1Width(CConnMgrApp::s_pPropertySerializer, _T("IpAddress\\Column1"), 100)
{
	//{{AFX_DATA_INIT(CChldView10)
	//}}AFX_DATA_INIT
}

void CChldView10::DoDataExchange(CDataExchange* pDX)
{
	CChldView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CChldView10)
	DDX_Control(pDX, IDC_LIST_ADDR, m_ctrlAddr);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CChldView10, CChldView)
	//{{AFX_MSG_MAP(CChldView10)
	ON_BN_CLICKED(IDC_BUTTON_REFRESH, OnButtonRefresh)
	ON_WM_SIZE()
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CChldView10 message handlers

void CChldView10::OnButtonRefresh()
{
	Fill();

	BackupProperties();
}

void CChldView10::OnInitialUpdate()
{
	CChldView::OnInitialUpdate();

	m_ctrlAddr.InsertColumn(0, CString((LPCTSTR)IDS_IPADDRESS_COLUMN0), LVCFMT_LEFT, m_Column0Width.GetValue());
	m_ctrlAddr.InsertColumn(1, CString((LPCTSTR)IDS_IPADDRESS_COLUMN1), LVCFMT_LEFT, m_Column1Width.GetValue());
	m_ctrlAddr.SetExtendedStyle(m_ctrlAddr.GetExtendedStyle() | LVS_EX_FULLROWSELECT | LVS_EX_HEADERDRAGDROP);

	Fill();
}

void CChldView10::OnSize(UINT nType, int cx, int cy)
{
	CChldView::OnSize(nType, cx, cy);

	if (cx == 0 &&
		cy == 0)
	{
		return;
	}

	if (m_ctrlAddr.GetSafeHwnd() == NULL)
	{
		return;
	}

	CRect r;
	GetClientRect(&r);

	CGlobals::ResizeControl(&m_ctrlAddr, r.right - 1, r.bottom - 1);
}

void CChldView10::Fill()
{
	m_ctrlAddr.DeleteAllItems();
	MIB_IPADDRTABLE *pTable = NULL;
	ULONG nSize = 0;
	DWORD dwErr = ::GetIpAddrTable(pTable, &nSize, FALSE);
	if (dwErr == ERROR_INSUFFICIENT_BUFFER)
	{
		pTable = (MIB_IPADDRTABLE *)new BYTE[nSize];
		dwErr = ::GetIpAddrTable(pTable, &nSize, FALSE);
		if (dwErr == NO_ERROR)
		{
#ifdef _DEBUG
			::OutputDebugString(_T("GetIpAddrTable()\n"));
#endif
			for (DWORD i = 0; i < pTable->dwNumEntries; i++)
			{
				int nItem = m_ctrlAddr.GetItemCount();
				m_ctrlAddr.InsertItem(nItem, CGlobals::IPAddress(pTable->table[i].dwAddr));
				m_ctrlAddr.SetItemText(
					nItem,
					1,
					CGlobals::IPAddress(pTable->table[i].dwMask));
			}
		}
		else
		{
			CGlobals::PrintLastError(_T("GetIpAddrTable"), dwErr);
		}
		delete []pTable;
	}
}

void CChldView10::OnDestroy()
{
	BackupProperties();

	CChldView::OnDestroy();
}

void CChldView10::BackupProperties()
{
	m_Column0Width.SetValue(m_ctrlAddr.GetColumnWidth(0));
	m_Column1Width.SetValue(m_ctrlAddr.GetColumnWidth(1));
}

/////////////////////////////////////////////////////////////////////////////
// CChldView11

CChldView11::CChldView11() :
	CChldView(CChldView11::IDD),
	m_Column0Width(CConnMgrApp::s_pPropertySerializer, _T("Route\\Column0"), 100),
	m_Column1Width(CConnMgrApp::s_pPropertySerializer, _T("Route\\Column1"), 100),
	m_Column2Width(CConnMgrApp::s_pPropertySerializer, _T("Route\\Column2"), 100),
	m_Column3Width(CConnMgrApp::s_pPropertySerializer, _T("Route\\Column3"), 70),
	m_Column4Width(CConnMgrApp::s_pPropertySerializer, _T("Route\\Column4"), 50),
	m_Column5Width(CConnMgrApp::s_pPropertySerializer, _T("Route\\Column5"), 50),
	m_Column6Width(CConnMgrApp::s_pPropertySerializer, _T("Route\\Column6"), 50)
{
	//{{AFX_DATA_INIT(CChldView11)
	//}}AFX_DATA_INIT
}

void CChldView11::DoDataExchange(CDataExchange* pDX)
{
	CChldView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CChldView11)
	DDX_Control(pDX, IDC_LIST_ROUTE, m_ctrlRoute);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CChldView11, CChldView)
	//{{AFX_MSG_MAP(CChldView11)
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_BUTTON_REFRESH, OnButtonRefresh)
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CChldView11 message handlers

void CChldView11::OnInitialUpdate()
{
	CChldView::OnInitialUpdate();

	m_ctrlRoute.InsertColumn(0, CString((LPCTSTR)IDS_ROUTE_COLUMN0), LVCFMT_LEFT, m_Column0Width.GetValue());
	m_ctrlRoute.InsertColumn(1, CString((LPCTSTR)IDS_ROUTE_COLUMN1), LVCFMT_LEFT, m_Column1Width.GetValue());
	m_ctrlRoute.InsertColumn(2, CString((LPCTSTR)IDS_ROUTE_COLUMN2), LVCFMT_LEFT, m_Column2Width.GetValue());
	m_ctrlRoute.InsertColumn(3, CString((LPCTSTR)IDS_ROUTE_COLUMN3), LVCFMT_RIGHT, m_Column3Width.GetValue());
	m_ctrlRoute.InsertColumn(4, CString((LPCTSTR)IDS_ROUTE_COLUMN4), LVCFMT_RIGHT, m_Column4Width.GetValue());
	m_ctrlRoute.InsertColumn(5, CString((LPCTSTR)IDS_ROUTE_COLUMN5), LVCFMT_RIGHT, m_Column5Width.GetValue());
	m_ctrlRoute.InsertColumn(6, CString((LPCTSTR)IDS_ROUTE_COLUMN6), LVCFMT_RIGHT, m_Column6Width.GetValue());
	m_ctrlRoute.SetExtendedStyle(m_ctrlRoute.GetExtendedStyle() | LVS_EX_FULLROWSELECT | LVS_EX_HEADERDRAGDROP);

	Fill();
}

void CChldView11::OnSize(UINT nType, int cx, int cy)
{
	CChldView::OnSize(nType, cx, cy);

	if (cx == 0 &&
		cy == 0)
	{
		return;
	}

	if (m_ctrlRoute.GetSafeHwnd() == NULL)
	{
		return;
	}

	CRect r;
	GetClientRect(&r);

	CGlobals::ResizeControl(&m_ctrlRoute, r.right - 1, r.bottom - 1);
}

void CChldView11::OnButtonRefresh()
{
	Fill();

	BackupProperties();
}

void CChldView11::OnDestroy()
{
	BackupProperties();

	CChldView::OnDestroy();
}

void CChldView11::Fill()
{
	m_ctrlRoute.DeleteAllItems();
	MIB_IPFORWARDTABLE *pTable = NULL;
	ULONG nSize = 0;
	DWORD dwErr = ::GetIpForwardTable(pTable, &nSize, FALSE);
	if (dwErr == ERROR_INSUFFICIENT_BUFFER)
	{
		pTable = (MIB_IPFORWARDTABLE *)new BYTE[nSize];
		dwErr = ::GetIpForwardTable(pTable, &nSize, FALSE);
		if (dwErr == NO_ERROR)
		{
#ifdef _DEBUG
			::OutputDebugString(_T("GetIpForwardTable()\n"));
#endif
			for (DWORD i = 0; i < pTable->dwNumEntries; i++)
			{
				int nItem = m_ctrlRoute.GetItemCount();
				m_ctrlRoute.InsertItem(nItem, CGlobals::IPAddress(pTable->table[i].dwForwardDest));
				m_ctrlRoute.SetItemText(
					nItem,
					1,
					CGlobals::IPAddress(pTable->table[i].dwForwardMask));
				m_ctrlRoute.SetItemText(
					nItem,
					2,
					CGlobals::IPAddress(pTable->table[i].dwForwardNextHop));
				m_ctrlRoute.SetItemText(
					nItem,
					3,
					CGlobals::ToString(pTable->table[i].dwForwardIfIndex));
				m_ctrlRoute.SetItemText(
					nItem,
					4,
					RouteTypeText(pTable->table[i].dwForwardType));
				m_ctrlRoute.SetItemText(
					nItem,
					5,
					CChldView12::ProtocolText(pTable->table[i].dwForwardProto));
				m_ctrlRoute.SetItemText(
					nItem,
					6,
					CGlobals::ToString(pTable->table[i].dwForwardMetric1));
			}
		}
		else
		{
			CGlobals::PrintLastError(_T("GetIpForwardTable"), dwErr);
		}
		delete []pTable;
	}
}

void CChldView11::BackupProperties()
{
	m_Column0Width.SetValue(m_ctrlRoute.GetColumnWidth(0));
	m_Column1Width.SetValue(m_ctrlRoute.GetColumnWidth(1));
	m_Column2Width.SetValue(m_ctrlRoute.GetColumnWidth(2));
	m_Column3Width.SetValue(m_ctrlRoute.GetColumnWidth(3));
	m_Column4Width.SetValue(m_ctrlRoute.GetColumnWidth(4));
	m_Column5Width.SetValue(m_ctrlRoute.GetColumnWidth(5));
	m_Column6Width.SetValue(m_ctrlRoute.GetColumnWidth(6));
}

CString CChldView11::RouteTypeText(DWORD dwType)
{
	CString sResult;
	switch (dwType)
	{
		case MIB_IPROUTE_TYPE_OTHER:
			sResult = CString((LPCTSTR)IDS_IPROUTE_TYPE_OTHER);
			break;

		case MIB_IPROUTE_TYPE_INVALID:
			sResult = CString((LPCTSTR)IDS_IPROUTE_TYPE_INVALID);
			break;

		case MIB_IPROUTE_TYPE_DIRECT:
			sResult = CString((LPCTSTR)IDS_IPROUTE_TYPE_DIRECT);
			break;

		case MIB_IPROUTE_TYPE_INDIRECT:
			sResult = CString((LPCTSTR)IDS_IPROUTE_TYPE_INDIRECT);
			break;

		default:
			sResult.Format(IDS_UNKNOWN, dwType);
	}
	return sResult;
}

/////////////////////////////////////////////////////////////////////////////
// CChldView12

CChldView12::CChldView12() :
	CChldView(CChldView12::IDD),
	m_Column0Width(CConnMgrApp::s_pPropertySerializer, _T("Proto\\Column0"), 70),
	m_Column1Width(CConnMgrApp::s_pPropertySerializer, _T("Proto\\Column1"), 100),
	m_Column2Width(CConnMgrApp::s_pPropertySerializer, _T("Proto\\Column2"), 100),
	m_Column3Width(CConnMgrApp::s_pPropertySerializer, _T("Proto\\Column3"), 350)
{
	//{{AFX_DATA_INIT(CChldView12)
	//}}AFX_DATA_INIT
}

void CChldView12::DoDataExchange(CDataExchange* pDX)
{
	CChldView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CChldView12)
	DDX_Control(pDX, IDC_LIST_PROTO, m_ctrlProto);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CChldView12, CChldView)
	//{{AFX_MSG_MAP(CChldView12)
	ON_WM_DESTROY()
	ON_WM_SIZE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CChldView12 message handlers

void CChldView12::OnInitialUpdate()
{
	CChldView::OnInitialUpdate();

	m_ctrlProto.InsertColumn(0, CString((LPCTSTR)IDS_PROTO_COLUMN0), LVCFMT_LEFT, m_Column0Width.GetValue());
	m_ctrlProto.InsertColumn(1, CString((LPCTSTR)IDS_PROTO_COLUMN1), LVCFMT_LEFT, m_Column1Width.GetValue());
	m_ctrlProto.InsertColumn(2, CString((LPCTSTR)IDS_PROTO_COLUMN2), LVCFMT_LEFT, m_Column2Width.GetValue());
	m_ctrlProto.InsertColumn(3, CString((LPCTSTR)IDS_PROTO_COLUMN3), LVCFMT_LEFT, m_Column3Width.GetValue());
	m_ctrlProto.SetExtendedStyle(m_ctrlProto.GetExtendedStyle() | LVS_EX_FULLROWSELECT | LVS_EX_HEADERDRAGDROP);

	Fill();
}

void CChldView12::OnDestroy()
{
	BackupProperties();

	CChldView::OnDestroy();
}

void CChldView12::OnSize(UINT nType, int cx, int cy)
{
	CChldView::OnSize(nType, cx, cy);

	if (cx == 0 &&
		cy == 0)
	{
		return;
	}

	if (m_ctrlProto.GetSafeHwnd() == NULL)
	{
		return;
	}

	CRect r;
	GetClientRect(&r);

	CGlobals::ResizeControl(&m_ctrlProto, r.right - 1, r.bottom - 1);
}

void CChldView12::Fill()
{
	LPWSAPROTOCOL_INFO pBuff = NULL;
	DWORD dwBuffLen = 0;
	::WSAEnumProtocols(NULL, pBuff, &dwBuffLen);
	DWORD dwErr = ::WSAGetLastError();
	if (dwErr == WSAENOBUFS)
	{
		pBuff = (LPWSAPROTOCOL_INFO)new BYTE[dwBuffLen];
		if (::WSAEnumProtocols(NULL, pBuff, &dwBuffLen) != SOCKET_ERROR)
		{
#ifdef _DEBUG
			::OutputDebugString(_T("WSAEnumProtocols()\n"));
#endif
			LPWSAPROTOCOL_INFO pInfo = pBuff;
			int i = dwBuffLen / sizeof(WSAPROTOCOL_INFO);
			while (i > 0)
			{
				int nItem = m_ctrlProto.GetItemCount();
				m_ctrlProto.InsertItem(nItem, AddressFamilyText(pInfo->iAddressFamily));
				m_ctrlProto.SetItemText(nItem, 1, SockTypeText(pInfo->iSocketType));
				m_ctrlProto.SetItemText(nItem, 2, ProtocolText(pInfo->iProtocol));
				m_ctrlProto.SetItemText(nItem, 3, pInfo->szProtocol);
#ifdef _DEBUG
				CString s;
				s.Format(_T("family: %d, type: %d, proto: %d, name: %s, min. addr.: %d, max. addr.: %d\n"),
					pInfo->iAddressFamily,
					pInfo->iSocketType,
					pInfo->iProtocol,
					pInfo->szProtocol,
					pInfo->iMinSockAddr, 
					pInfo->iMaxSockAddr);
				::OutputDebugString(s);
#endif
				pInfo++;
				i--;
			}
		}
		else
		{
			CGlobals::PrintLastError(_T("WSAEnumProtocols"), ::WSAGetLastError());
		}
		delete []pBuff;
	}
	else
	{
		CGlobals::PrintLastError(_T("WSAEnumProtocols"), dwErr);
	}
}

void CChldView12::BackupProperties()
{
	m_Column0Width.SetValue(m_ctrlProto.GetColumnWidth(0));
	m_Column1Width.SetValue(m_ctrlProto.GetColumnWidth(1));
	m_Column2Width.SetValue(m_ctrlProto.GetColumnWidth(2));
	m_Column3Width.SetValue(m_ctrlProto.GetColumnWidth(3));
}

CString CChldView12::ProtocolText(int nProto)
{
	CString sResult;
	switch (nProto)
	{
		case IPPROTO_IP:
			sResult = CString((LPCTSTR)IDS_IPPROTO_IP);
			break;

		case IPPROTO_ICMP:
			sResult = CString((LPCTSTR)IDS_IPPROTO_ICMP);
			break;

		case IPPROTO_IGMP:
			sResult = CString((LPCTSTR)IDS_IPPROTO_IGMP);
			break;

		case IPPROTO_GGP:
			sResult = CString((LPCTSTR)IDS_IPPROTO_GGP);
			break;

		case IPPROTO_TCP:
			sResult = CString((LPCTSTR)IDS_IPPROTO_TCP);
			break;

		case IPPROTO_PUP:
			sResult = CString((LPCTSTR)IDS_IPPROTO_PUP);
			break;

		case IPPROTO_UDP:
			sResult = CString((LPCTSTR)IDS_IPPROTO_UDP);
			break;

		case IPPROTO_IDP:
			sResult = CString((LPCTSTR)IDS_IPPROTO_IDP);
			break;

		case IPPROTO_IPV6:
			sResult = CString((LPCTSTR)IDS_IPPROTO_IPV6);
			break;

		case IPPROTO_ND:
			sResult = CString((LPCTSTR)IDS_IPPROTO_ND);
			break;

		case IPPROTO_ICLFXBM:
			sResult = CString((LPCTSTR)IDS_IPPROTO_ICLFXBM);
			break;

		case IPPROTO_RAW:
			sResult = CString((LPCTSTR)IDS_IPPROTO_RAW);
			break;

		default:
			sResult.Format(IDS_UNKNOWN, nProto);
	}
	return sResult;
}

CString CChldView12::SockTypeText(int nSockType)
{
	CString sResult;
	switch (nSockType)
	{
		case SOCK_STREAM:
			sResult = CString((LPCTSTR)IDS_SOCK_STREAM);
			break;

		case SOCK_DGRAM:
			sResult = CString((LPCTSTR)IDS_SOCK_DGRAM);
			break;

		case SOCK_RAW:
			sResult = CString((LPCTSTR)IDS_SOCK_RAW);
			break;

		case SOCK_RDM:
			sResult = CString((LPCTSTR)IDS_SOCK_RDM);
			break;

		case SOCK_SEQPACKET:
			sResult = CString((LPCTSTR)IDS_SOCK_SEQPACKET);
			break;

		default:
			sResult.Format(IDS_UNKNOWN, nSockType);
	}
	return sResult;
}

CString CChldView12::AddressFamilyText(int nAddressFamily)
{
	CString sResult;
	switch (nAddressFamily)
	{
		case AF_UNSPEC:
			sResult = CString((LPCTSTR)IDS_AF_UNSPEC);
			break;

		case AF_UNIX:
			sResult = CString((LPCTSTR)IDS_AF_UNIX);
			break;

		case AF_INET:
			sResult = CString((LPCTSTR)IDS_AF_INET);
			break;

		case AF_IMPLINK:
			sResult = CString((LPCTSTR)IDS_AF_IMPLINK);
			break;

		case AF_PUP:
			sResult = CString((LPCTSTR)IDS_AF_PUP);
			break;

		case AF_CHAOS:
			sResult = CString((LPCTSTR)IDS_AF_CHAOS);
			break;

		case AF_NS:
			sResult = CString((LPCTSTR)IDS_AF_NS);
			break;

		case AF_ISO:
			sResult = CString((LPCTSTR)IDS_AF_ISO);
			break;

		case AF_ECMA:
			sResult = CString((LPCTSTR)IDS_AF_ECMA);
			break;

		case AF_DATAKIT:
			sResult = CString((LPCTSTR)IDS_AF_DATAKIT);
			break;

		case AF_CCITT:
			sResult = CString((LPCTSTR)IDS_AF_CCITT);
			break;

		case AF_SNA:
			sResult = CString((LPCTSTR)IDS_AF_SNA);
			break;

		case AF_DECnet:
			sResult = CString((LPCTSTR)IDS_AF_DECnet);
			break;

		case AF_DLI:
			sResult = CString((LPCTSTR)IDS_AF_DLI);
			break;

		case AF_LAT:
			sResult = CString((LPCTSTR)IDS_AF_LAT);
			break;

		case AF_HYLINK:
			sResult = CString((LPCTSTR)IDS_AF_HYLINK);
			break;

		case AF_APPLETALK:
			sResult = CString((LPCTSTR)IDS_AF_APPLETALK);
			break;

		case AF_NETBIOS:
			sResult = CString((LPCTSTR)IDS_AF_NETBIOS);
			break;

		case AF_VOICEVIEW:
			sResult = CString((LPCTSTR)IDS_AF_VOICEVIEW);
			break;

		case AF_FIREFOX:
			sResult = CString((LPCTSTR)IDS_AF_FIREFOX);
			break;

		case AF_UNKNOWN1:
			sResult = CString((LPCTSTR)IDS_AF_UNKNOWN1);
			break;

		case AF_BAN:
			sResult = CString((LPCTSTR)IDS_AF_BAN);
			break;

		case AF_IRDA:
			sResult = CString((LPCTSTR)IDS_AF_IRDA);
			break;

		case AF_INET6:
			sResult = CString((LPCTSTR)IDS_AF_INET6);
			break;

		case AF_CLUSTER:
			sResult = CString((LPCTSTR)IDS_AF_CLUSTER);
			break;

		case AF_12844:
			sResult = CString((LPCTSTR)IDS_AF_12844);
			break;

		case AF_ATM:
			sResult = CString((LPCTSTR)IDS_AF_ATM);
			break;

		case AF_NETDES:
			sResult = CString((LPCTSTR)IDS_AF_NETDES);
			break;

		case AF_TCNPROCESS:
			sResult = CString((LPCTSTR)IDS_AF_TCNPROCESS);
			break;

		case AF_TCNMESSAGE:
			sResult = CString((LPCTSTR)IDS_AF_TCNMESSAGE);
			break;

		case AF_ICLFXBM:
			sResult = CString((LPCTSTR)IDS_AF_ICLFXBM);
			break;

		default:
			sResult.Format(IDS_UNKNOWN, nAddressFamily);
	}
	return sResult;
}

/////////////////////////////////////////////////////////////////////////////
// CChldView13

CChldView13::CChldView13() :
	CChldView(CChldView13::IDD),
	m_Column0Width(CConnMgrApp::s_pPropertySerializer, _T("Interface\\Column0"), 50),
	m_Column1Width(CConnMgrApp::s_pPropertySerializer, _T("Interface\\Column1"), 50),
	m_Column2Width(CConnMgrApp::s_pPropertySerializer, _T("Interface\\Column2"), 100),
	m_Column3Width(CConnMgrApp::s_pPropertySerializer, _T("Interface\\Column3"), 70)
{
	//{{AFX_DATA_INIT(CChldView13)
	//}}AFX_DATA_INIT
}

void CChldView13::DoDataExchange(CDataExchange* pDX)
{
	CChldView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CChldView13)
	DDX_Control(pDX, IDC_LIST_ITF, m_ctrlItf);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CChldView13, CChldView)
	//{{AFX_MSG_MAP(CChldView13)
	ON_WM_DESTROY()
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_BUTTON_REFRESH, OnButtonRefresh)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CChldView13 message handlers

void CChldView13::OnInitialUpdate()
{
	CChldView::OnInitialUpdate();

	m_ctrlItf.InsertColumn(0, CString((LPCTSTR)IDS_ITF_COLUMN0), LVCFMT_LEFT, m_Column0Width.GetValue());
	m_ctrlItf.InsertColumn(1, CString((LPCTSTR)IDS_ITF_COLUMN1), LVCFMT_LEFT, m_Column1Width.GetValue());
	m_ctrlItf.InsertColumn(2, CString((LPCTSTR)IDS_ITF_COLUMN2), LVCFMT_LEFT, m_Column2Width.GetValue());
	m_ctrlItf.InsertColumn(3, CString((LPCTSTR)IDS_ITF_COLUMN3), LVCFMT_LEFT, m_Column3Width.GetValue());
	m_ctrlItf.SetExtendedStyle(m_ctrlItf.GetExtendedStyle() | LVS_EX_FULLROWSELECT | LVS_EX_HEADERDRAGDROP);

	Fill();
}

void CChldView13::OnDestroy()
{
	BackupProperties();

	CChldView::OnDestroy();
}

void CChldView13::OnSize(UINT nType, int cx, int cy)
{
	CChldView::OnSize(nType, cx, cy);

	if (cx == 0 &&
		cy == 0)
	{
		return;
	}

	if (m_ctrlItf.GetSafeHwnd() == NULL)
	{
		return;
	}

	CRect r;
	GetClientRect(&r);

	CGlobals::ResizeControl(&m_ctrlItf, r.right - 1, r.bottom - 1);
}

void CChldView13::Fill()
{
	m_ctrlItf.DeleteAllItems();
	MIB_IFTABLE *pTable = NULL;
	ULONG nSize = 0;
	DWORD dwErr = ::GetIfTable(pTable, &nSize, FALSE);
	if (dwErr == ERROR_INSUFFICIENT_BUFFER)
	{
		pTable = (MIB_IFTABLE *)new BYTE[nSize];
		dwErr = ::GetIfTable(pTable, &nSize, FALSE);
		if (dwErr == NO_ERROR)
		{
#ifdef _DEBUG
			::OutputDebugString(_T("GetIfTable()\n"));
#endif
			for (DWORD i = 0; i < pTable->dwNumEntries; i++)
			{
				int nItem = m_ctrlItf.GetItemCount();
				m_ctrlItf.InsertItem(nItem, CGlobals::ToString(pTable->table[i].dwIndex));
				m_ctrlItf.SetItemText(
					nItem,
					1,
					CChldView9::AdapterTypeText(pTable->table[i].dwType));
				m_ctrlItf.SetItemText(
					nItem,
					2,
					CGlobals::MacAddress(pTable->table[i].bPhysAddr, pTable->table[i].dwPhysAddrLen));
				m_ctrlItf.SetItemText(
					nItem,
					3,
					OperStatusText(pTable->table[i].dwOperStatus));
			}
		}
		else
		{
			CGlobals::PrintLastError(_T("GetIfTable"), dwErr);
		}
		delete []pTable;
	}
}

void CChldView13::OnButtonRefresh()
{
	Fill();

	BackupProperties();
}

void CChldView13::BackupProperties()
{
	m_Column0Width.SetValue(m_ctrlItf.GetColumnWidth(0));
	m_Column1Width.SetValue(m_ctrlItf.GetColumnWidth(1));
	m_Column2Width.SetValue(m_ctrlItf.GetColumnWidth(2));
	m_Column3Width.SetValue(m_ctrlItf.GetColumnWidth(3));
}

CString CChldView13::OperStatusText(DWORD dwStatus)
{
	CString sResult;
	switch (dwStatus)
	{
		case MIB_IF_OPER_STATUS_NON_OPERATIONAL:
			sResult = CString((LPCTSTR)IDS_IF_OPER_STATUS_NON_OPERATIONAL);
			break;

		case MIB_IF_OPER_STATUS_UNREACHABLE:
			sResult = CString((LPCTSTR)IDS_IF_OPER_STATUS_UNREACHABLE);
			break;

		case MIB_IF_OPER_STATUS_DISCONNECTED:
			sResult = CString((LPCTSTR)IDS_IF_OPER_STATUS_DISCONNECTED);
			break;

		case MIB_IF_OPER_STATUS_CONNECTING:
			sResult = CString((LPCTSTR)IDS_IF_OPER_STATUS_CONNECTING);
			break;

		case MIB_IF_OPER_STATUS_CONNECTED:
			sResult = CString((LPCTSTR)IDS_IF_OPER_STATUS_CONNECTED);
			break;

		case MIB_IF_OPER_STATUS_OPERATIONAL:
			sResult = CString((LPCTSTR)IDS_IF_OPER_STATUS_OPERATIONAL);
			break;
	}
	return sResult;
}

/////////////////////////////////////////////////////////////////////////////
// CChldView14

CChldView14::CChldView14() :
	CChldView(CChldView14::IDD),
	m_Column0Width(CConnMgrApp::s_pPropertySerializer, _T("Arp\\Column0"), 50),
	m_Column1Width(CConnMgrApp::s_pPropertySerializer, _T("Arp\\Column1"), 100),
	m_Column2Width(CConnMgrApp::s_pPropertySerializer, _T("Arp\\Column2"), 70),
	m_Column3Width(CConnMgrApp::s_pPropertySerializer, _T("Arp\\Column3"), 70)
{
	//{{AFX_DATA_INIT(CChldView14)
	//}}AFX_DATA_INIT
}

void CChldView14::DoDataExchange(CDataExchange* pDX)
{
	CChldView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CChldView14)
	DDX_Control(pDX, IDC_LIST_ARP, m_ctrlArp);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CChldView14, CChldView)
	//{{AFX_MSG_MAP(CChldView14)
	ON_WM_DESTROY()
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_BUTTON_REFRESH, OnButtonRefresh)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CChldView14 message handlers

void CChldView14::OnInitialUpdate()
{
	CChldView::OnInitialUpdate();

	m_ctrlArp.InsertColumn(0, CString((LPCTSTR)IDS_ARP_COLUMN0), LVCFMT_LEFT, m_Column0Width.GetValue());
	m_ctrlArp.InsertColumn(1, CString((LPCTSTR)IDS_ARP_COLUMN1), LVCFMT_LEFT, m_Column1Width.GetValue());
	m_ctrlArp.InsertColumn(2, CString((LPCTSTR)IDS_ARP_COLUMN2), LVCFMT_LEFT, m_Column2Width.GetValue());
	m_ctrlArp.InsertColumn(3, CString((LPCTSTR)IDS_ARP_COLUMN3), LVCFMT_LEFT, m_Column3Width.GetValue());
	m_ctrlArp.SetExtendedStyle(m_ctrlArp.GetExtendedStyle() | LVS_EX_FULLROWSELECT | LVS_EX_HEADERDRAGDROP);

	Fill();
}

void CChldView14::OnDestroy()
{
	BackupProperties();

	CChldView::OnDestroy();
}

void CChldView14::OnSize(UINT nType, int cx, int cy)
{
	CChldView::OnSize(nType, cx, cy);

	if (cx == 0 &&
		cy == 0)
	{
		return;
	}

	if (m_ctrlArp.GetSafeHwnd() == NULL)
	{
		return;
	}

	CRect r;
	GetClientRect(&r);

	CGlobals::ResizeControl(&m_ctrlArp, r.right - 1, r.bottom - 1);
}

void CChldView14::OnButtonRefresh()
{
	Fill();

	BackupProperties();
}

void CChldView14::Fill()
{
	m_ctrlArp.DeleteAllItems();
	MIB_IPNETTABLE *pTable = NULL;
	ULONG nSize = 0;
	DWORD dwErr = ::GetIpNetTable(pTable, &nSize, FALSE);
	if (dwErr == ERROR_INSUFFICIENT_BUFFER)
	{
		pTable = (MIB_IPNETTABLE *)new BYTE[nSize];
		dwErr = ::GetIpNetTable(pTable, &nSize, FALSE);
		if (dwErr == NO_ERROR)
		{
#ifdef _DEBUG
			::OutputDebugString(_T("GetIpNetTable()\n"));
#endif
			for (DWORD i = 0; i < pTable->dwNumEntries; i++)
			{
				int nItem = m_ctrlArp.GetItemCount();
				m_ctrlArp.InsertItem(nItem, CGlobals::ToString(pTable->table[i].dwIndex));
				m_ctrlArp.SetItemText(
					nItem,
					1,
					CGlobals::MacAddress(pTable->table[i].bPhysAddr, pTable->table[i].dwPhysAddrLen));
				m_ctrlArp.SetItemText(
					nItem,
					2,
					CGlobals::IPAddress(pTable->table[i].dwAddr));
				m_ctrlArp.SetItemText(
					nItem,
					3,
					EntryTypeText(pTable->table[i].dwType));
			}
		}
		else
		{
			CGlobals::PrintLastError(_T("GetIpNetTable"), dwErr);
		}
		delete []pTable;
	}
}

void CChldView14::BackupProperties()
{
	m_Column0Width.SetValue(m_ctrlArp.GetColumnWidth(0));
	m_Column1Width.SetValue(m_ctrlArp.GetColumnWidth(1));
	m_Column2Width.SetValue(m_ctrlArp.GetColumnWidth(2));
	m_Column3Width.SetValue(m_ctrlArp.GetColumnWidth(3));
}

CString CChldView14::EntryTypeText(DWORD dwType)
{
	CString sResult;
	switch (dwType)
	{
		case 1:
			sResult = CString((LPCTSTR)IDS_ARP_ENTRY_TYPE_OTHER);
			break;

		case 2:
			sResult = CString((LPCTSTR)IDS_ARP_ENTRY_TYPE_INVALID);
			break;

		case 3:
			sResult = CString((LPCTSTR)IDS_ARP_ENTRY_TYPE_DYNAMIC);
			break;

		case 4:
			sResult = CString((LPCTSTR)IDS_ARP_ENTRY_TYPE_STATIC);
			break;
	}
	return sResult;
}
