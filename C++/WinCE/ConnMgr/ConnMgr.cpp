#include "stdafx.h"
#include "ConnMgr.h"
#include "MainFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define MAJOR_VER 2
#define MINOR_VER 0

/////////////////////////////////////////////////////////////////////////////
// CConnMgrApp

BEGIN_MESSAGE_MAP(CConnMgrApp, CWinApp)
	//{{AFX_MSG_MAP(CConnMgrApp)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// The one and only CConnMgrApp object

CConnMgrApp theApp;
CRegPropertySerializer *CConnMgrApp::s_pPropertySerializer = NULL;

/////////////////////////////////////////////////////////////////////////////
// CConnMgrApp initialization

BOOL CConnMgrApp::InitInstance()
{
	WSADATA wsaData;
	bool bWsaErr = false;
	if (::WSAStartup(MAKEWORD(MAJOR_VER, MINOR_VER), &wsaData) != 0)
		bWsaErr = true;
	else if (LOBYTE(wsaData.wVersion) != MAJOR_VER || HIBYTE(wsaData.wVersion) != MINOR_VER)
		bWsaErr = true;
	if (bWsaErr)
	{
		AfxMessageBox(IDP_SOCKETS_INIT_FAILED);
		return FALSE;
	}

	s_pPropertySerializer = new CRegPropertySerializer(_T("HKEY_LOCAL_MACHINE\\Software\\Kamen\\ConnMgr"));
	CMainFrame* pFrame = new CMainFrame;
	m_pMainWnd = pFrame;
	pFrame->LoadFrame(IDR_MAINFRAME, WS_OVERLAPPEDWINDOW);
	pFrame->ShowWindow(m_nCmdShow);
	pFrame->UpdateWindow();

	return TRUE;
}

int CConnMgrApp::ExitInstance()
{
	delete s_pPropertySerializer;
	::WSACleanup();

	return CWinApp::ExitInstance();
}
