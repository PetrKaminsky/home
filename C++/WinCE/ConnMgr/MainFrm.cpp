#include "stdafx.h"
#include "ConnMgr.h"
#include "MainFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMainFrame

IMPLEMENT_DYNAMIC(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	//{{AFX_MSG_MAP(CMainFrame)
	ON_WM_CREATE()
	ON_WM_SETFOCUS()
	ON_COMMAND(ID_FORM1, OnForm1)
	ON_COMMAND(ID_FORM2, OnForm2)
	ON_WM_SIZE()
	ON_COMMAND(ID_FORM3, OnForm3)
	ON_COMMAND(ID_FORM4, OnForm4)
	ON_COMMAND(ID_FORM5, OnForm5)
	ON_UPDATE_COMMAND_UI(ID_FORM1, OnUpdateForm1)
	ON_UPDATE_COMMAND_UI(ID_FORM2, OnUpdateForm2)
	ON_UPDATE_COMMAND_UI(ID_FORM3, OnUpdateForm3)
	ON_UPDATE_COMMAND_UI(ID_FORM4, OnUpdateForm4)
	ON_UPDATE_COMMAND_UI(ID_FORM5, OnUpdateForm5)
	ON_COMMAND(ID_FORM6, OnForm6)
	ON_UPDATE_COMMAND_UI(ID_FORM6, OnUpdateForm6)
	ON_COMMAND(ID_FORM7, OnForm7)
	ON_UPDATE_COMMAND_UI(ID_FORM7, OnUpdateForm7)
	ON_COMMAND(ID_FORM8, OnForm8)
	ON_UPDATE_COMMAND_UI(ID_FORM8, OnUpdateForm8)
	ON_COMMAND(ID_FORM9, OnForm9)
	ON_UPDATE_COMMAND_UI(ID_FORM9, OnUpdateForm9)
	ON_COMMAND(ID_FORM10, OnForm10)
	ON_UPDATE_COMMAND_UI(ID_FORM10, OnUpdateForm10)
	ON_COMMAND(ID_FORM11, OnForm11)
	ON_UPDATE_COMMAND_UI(ID_FORM11, OnUpdateForm11)
	ON_COMMAND(ID_FORM12, OnForm12)
	ON_UPDATE_COMMAND_UI(ID_FORM12, OnUpdateForm12)
	ON_COMMAND(ID_FORM13, OnForm13)
	ON_UPDATE_COMMAND_UI(ID_FORM13, OnUpdateForm13)
	ON_COMMAND(ID_FORM14, OnForm14)
	ON_UPDATE_COMMAND_UI(ID_FORM14, OnUpdateForm14)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMainFrame construction/destruction

CMainFrame::CMainFrame()
{
	m_pView1 = NULL;
	m_pView2 = NULL;
	m_pView3 = NULL;
	m_pView4 = NULL;
	m_pView5 = NULL;
	m_pView6 = NULL;
	m_pView7 = NULL;
	m_pView8 = NULL;
	m_pView9 = NULL;
	m_pView10 = NULL;
	m_pView11 = NULL;
	m_pView12 = NULL;
	m_pView13 = NULL;
	m_pView14 = NULL;

	m_pActiveView = NULL;
}

CMainFrame::~CMainFrame()
{
	DestroyView(m_pView1);
	DestroyView(m_pView2);
	DestroyView(m_pView3);
	DestroyView(m_pView4);
	DestroyView(m_pView5);
	DestroyView(m_pView6);
	DestroyView(m_pView7);
	DestroyView(m_pView8);
	DestroyView(m_pView9);
	DestroyView(m_pView10);
	DestroyView(m_pView11);
	DestroyView(m_pView12);
	DestroyView(m_pView13);
	DestroyView(m_pView14);
}

void CMainFrame::DestroyView(CChldView *&pView)
{
	if (pView != NULL)
	{
		pView->DestroyWindow();
		delete pView;
		pView = NULL;
	}
}

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	// create a view to occupy the client area of the frame
	
	m_pView1 = new CChldView1;
	m_pView1->Create(this);
	m_pView2 = new CChldView2;
	m_pView2->Create(this);
	m_pView3 = new CChldView3;
	m_pView3->Create(this);
	m_pView4 = new CChldView4;
	m_pView4->Create(this);
	m_pView5 = new CChldView5;
	m_pView5->Create(this);
	m_pView6 = new CChldView6;
	m_pView6->Create(this);
	m_pView7 = new CChldView7;
	m_pView7->Create(this);
	m_pView8 = new CChldView8;
	m_pView8->Create(this);
	m_pView9 = new CChldView9;
	m_pView9->Create(this);
	m_pView10 = new CChldView10;
	m_pView10->Create(this);
	m_pView11 = new CChldView11;
	m_pView11->Create(this);
	m_pView12 = new CChldView12;
	m_pView12->Create(this);
	m_pView13 = new CChldView13;
	m_pView13->Create(this);
	m_pView14 = new CChldView14;
	m_pView14->Create(this);

	if (!m_wndCommandBar.Create(this) ||
		!m_wndCommandBar.InsertMenuBar(IDR_MAINFRAME))
	{
		return -1;      // fail to create
	}

	m_wndCommandBar.SetBarStyle(m_wndCommandBar.GetBarStyle() |
		CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_FIXED);

	ActivateForm(m_pView1);

	return 0;
}

/////////////////////////////////////////////////////////////////////////////
// CMainFrame message handlers

void CMainFrame::OnSetFocus(CWnd* pOldWnd)
{
	m_pActiveView->SetFocus();
}

BOOL CMainFrame::OnCmdMsg(UINT nID, int nCode, void* pExtra, AFX_CMDHANDLERINFO* pHandlerInfo)
{
	if (m_pActiveView->OnCmdMsg(nID, nCode, pExtra, pHandlerInfo))
		return TRUE;

	return CFrameWnd::OnCmdMsg(nID, nCode, pExtra, pHandlerInfo);
}

void CMainFrame::OnForm1()
{
	ActivateForm(m_pView1);
}

void CMainFrame::OnForm2()
{
	ActivateForm(m_pView2);
}

void CMainFrame::OnForm3()
{
	ActivateForm(m_pView3);
}

void CMainFrame::OnForm4()
{
	ActivateForm(m_pView4);
}

void CMainFrame::OnForm5()
{
	ActivateForm(m_pView5);
}

void CMainFrame::OnSize(UINT nType, int cx, int cy) 
{
	CFrameWnd::OnSize(nType, cx, cy);
	
// @@@@ PocketPC
	CRect r;
	GetClientRect(&r);
	if (m_pActiveView != NULL)
		m_pActiveView->MoveWindow(&r);
// @@@@ PocketPC
// @@@@ CE.NET
// @@@@ CE.NET
}

void CMainFrame::ActivateForm(CFormView *pForm)
{
	SetActiveView(pForm);
// @@@@ PocketPC
	CRect r;
	GetClientRect(&r);
// @@@@ PocketPC
// @@@@ CE.NET
//	CRect rc;
//	GetClientRect(&rc);
//	CRect rcBar;
//	m_wndCommandBar.GetWindowRect(rcBar);
//	CRect r(rc.left, rcBar.Height(), rc.right, rc.bottom);
// @@@@ CE.NET
	pForm->MoveWindow(&r);
	if (m_pActiveView != NULL && m_pActiveView != pForm)
	{
		CRect r0;
		r0.SetRectEmpty();
		m_pActiveView->MoveWindow(&r0);
	}
	m_pActiveView = pForm;
}

void CMainFrame::OnUpdateForm1(CCmdUI* pCmdUI)
{
	OnUpdateForm(pCmdUI, m_pView1);
}

void CMainFrame::OnUpdateForm2(CCmdUI* pCmdUI)
{
	OnUpdateForm(pCmdUI, m_pView2);
}

void CMainFrame::OnUpdateForm3(CCmdUI* pCmdUI)
{
	OnUpdateForm(pCmdUI, m_pView3);
}

void CMainFrame::OnUpdateForm4(CCmdUI* pCmdUI)
{
	OnUpdateForm(pCmdUI, m_pView4);
}

void CMainFrame::OnUpdateForm5(CCmdUI* pCmdUI)
{
	OnUpdateForm(pCmdUI, m_pView5);
}

void CMainFrame::OnForm6()
{
	ActivateForm(m_pView6);
}

void CMainFrame::OnUpdateForm6(CCmdUI* pCmdUI)
{
	OnUpdateForm(pCmdUI, m_pView6);
}

void CMainFrame::OnForm7()
{
	ActivateForm(m_pView7);
}

void CMainFrame::OnUpdateForm7(CCmdUI* pCmdUI)
{
	OnUpdateForm(pCmdUI, m_pView7);
}

void CMainFrame::OnForm8()
{
	ActivateForm(m_pView8);
}

void CMainFrame::OnUpdateForm8(CCmdUI* pCmdUI)
{
	OnUpdateForm(pCmdUI, m_pView8);
}

void CMainFrame::OnForm9()
{
	ActivateForm(m_pView9);
}

void CMainFrame::OnUpdateForm9(CCmdUI* pCmdUI)
{
	OnUpdateForm(pCmdUI, m_pView9);
}

void CMainFrame::OnForm10()
{
	ActivateForm(m_pView10);
}

void CMainFrame::OnUpdateForm10(CCmdUI* pCmdUI)
{
	OnUpdateForm(pCmdUI, m_pView10);
}

void CMainFrame::OnForm11()
{
	ActivateForm(m_pView11);
}

void CMainFrame::OnUpdateForm11(CCmdUI* pCmdUI)
{
	OnUpdateForm(pCmdUI, m_pView11);
}

void CMainFrame::OnForm12()
{
	ActivateForm(m_pView12);
}

void CMainFrame::OnUpdateForm12(CCmdUI* pCmdUI)
{
	OnUpdateForm(pCmdUI, m_pView12);
}

void CMainFrame::OnUpdateForm(CCmdUI *pCmdUI, CView *pView)
{
	pCmdUI->Enable(TRUE);
	pCmdUI->SetCheck(m_pActiveView == pView ? 1 : 0);
}

void CMainFrame::OnForm13()
{
	ActivateForm(m_pView13);
}

void CMainFrame::OnUpdateForm13(CCmdUI* pCmdUI)
{
	OnUpdateForm(pCmdUI, m_pView13);
}

void CMainFrame::OnForm14()
{
	ActivateForm(m_pView14);
}

void CMainFrame::OnUpdateForm14(CCmdUI* pCmdUI)
{
	OnUpdateForm(pCmdUI, m_pView14);
}
