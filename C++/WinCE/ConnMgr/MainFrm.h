#if !defined(AFX_MAINFRM_H__C724EC2C_8AA3_4867_9911_C80756E0F47A__INCLUDED_)
#define AFX_MAINFRM_H__C724EC2C_8AA3_4867_9911_C80756E0F47A__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "ChildView.h"

/////////////////////////////////////////////////////////////////////////////
// CChldView2 form view

class CMainFrame : public CFrameWnd
{
public:
	CMainFrame();

protected: 
	DECLARE_DYNAMIC(CMainFrame)

	virtual ~CMainFrame();

public:
	//{{AFX_VIRTUAL(CMainFrame)
	virtual BOOL OnCmdMsg(UINT nID, int nCode, void* pExtra, AFX_CMDHANDLERINFO* pHandlerInfo);
	//}}AFX_VIRTUAL

protected:  // control bar embedded members
	CCeCommandBar	m_wndCommandBar;
	CChldView *m_pView1;
	CChldView *m_pView2;
	CChldView *m_pView3;
	CChldView *m_pView4;
	CChldView *m_pView5;
	CChldView *m_pView6;
	CChldView *m_pView7;
	CChldView *m_pView8;
	CChldView *m_pView9;
	CChldView *m_pView10;
	CChldView *m_pView11;
	CChldView *m_pView12;
	CChldView *m_pView13;
	CChldView *m_pView14;
	CView *m_pActiveView;

	void ActivateForm(CFormView *pForm);

protected:
	//{{AFX_MSG(CMainFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSetFocus(CWnd *pOldWnd);
	afx_msg void OnForm1();
	afx_msg void OnForm2();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnForm3();
	afx_msg void OnForm4();
	afx_msg void OnForm5();
	afx_msg void OnUpdateForm1(CCmdUI* pCmdUI);
	afx_msg void OnUpdateForm2(CCmdUI* pCmdUI);
	afx_msg void OnUpdateForm3(CCmdUI* pCmdUI);
	afx_msg void OnUpdateForm4(CCmdUI* pCmdUI);
	afx_msg void OnUpdateForm5(CCmdUI* pCmdUI);
	afx_msg void OnForm6();
	afx_msg void OnUpdateForm6(CCmdUI* pCmdUI);
	afx_msg void OnForm7();
	afx_msg void OnUpdateForm7(CCmdUI* pCmdUI);
	afx_msg void OnForm8();
	afx_msg void OnUpdateForm8(CCmdUI* pCmdUI);
	afx_msg void OnForm9();
	afx_msg void OnUpdateForm9(CCmdUI* pCmdUI);
	afx_msg void OnForm10();
	afx_msg void OnUpdateForm10(CCmdUI* pCmdUI);
	afx_msg void OnForm11();
	afx_msg void OnUpdateForm11(CCmdUI* pCmdUI);
	afx_msg void OnForm12();
	afx_msg void OnUpdateForm12(CCmdUI* pCmdUI);
	afx_msg void OnForm13();
	afx_msg void OnUpdateForm13(CCmdUI* pCmdUI);
	afx_msg void OnForm14();
	afx_msg void OnUpdateForm14(CCmdUI* pCmdUI);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	void DestroyView(CChldView *&pView);
	void OnUpdateForm(CCmdUI *pCmdUI, CView *pView);
};

/////////////////////////////////////////////////////////////////////////////
//{{AFX_INSERT_LOCATION}}
// Microsoft eMbedded Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MAINFRM_H__C724EC2C_8AA3_4867_9911_C80756E0F47A__INCLUDED_)
