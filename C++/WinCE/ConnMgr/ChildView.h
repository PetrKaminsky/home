#if !defined(AFX_CHILDVIEW_H__AE575F94_A381_4334_B893_0ECDEB319D1A__INCLUDED_)
#define AFX_CHILDVIEW_H__AE575F94_A381_4334_B893_0ECDEB319D1A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CChldView form view

class CChldView : public CFormView
{
public:
	CChldView();
	CChldView(int nID);

	int Create(CWnd *pParent);

private:
	int m_nID;
};

/////////////////////////////////////////////////////////////////////////////
// CChldView form view

class CChldView1 : public CChldView
{
public:
	CChldView1();

	//{{AFX_DATA(CChldView1)
	enum { IDD = IDD_DIALOG_FORM1 };
	CListCtrl	m_ctrlConn;
	//}}AFX_DATA

	//{{AFX_VIRTUAL(CChldView1)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

protected:
	//{{AFX_MSG(CChldView1)
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnDestroy();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	CRegPropertyInt m_Column0Width;
	CRegPropertyInt m_Column1Width;

	void Fill();
	void BackupProperties();
};

/////////////////////////////////////////////////////////////////////////////
// CChldView2 form view

class CChldView2 : public CChldView
{
public:
	CChldView2();

	//{{AFX_DATA(CChldView2)
	enum { IDD = IDD_DIALOG_FORM2 };
	CEdit	m_ctrlURL;
	CListCtrl	m_ctrlGuid;
	//}}AFX_DATA

	//{{AFX_VIRTUAL(CChldView2)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

protected:
	//{{AFX_MSG(CChldView2)
	afx_msg void OnButtonMapUrl();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnDestroy();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	CRegPropertyInt m_Column0Width;
	CRegPropertyString m_LastUrl;

	void BackupProperties();
};

/////////////////////////////////////////////////////////////////////////////
// CChldView3 form view

class CChldView3 : public CChldView
{
public:
	CChldView3();

	//{{AFX_DATA(CChldView3)
	enum { IDD = IDD_DIALOG_FORM3 };
	CListCtrl	m_ctrlGuid;
	CEdit	m_ctrlRas;
	//}}AFX_DATA

	//{{AFX_VIRTUAL(CChldView3)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

protected:
	//{{AFX_MSG(CChldView3)
	afx_msg void OnButtonMapRas();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnDestroy();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	CRegPropertyInt m_Column0Width;
	CRegPropertyString m_LastRas;

	void BackupProperties();
};

/////////////////////////////////////////////////////////////////////////////
// CChldView4 form view

class CChldView4 : public CChldView
{
public:
	CChldView4();

	//{{AFX_DATA(CChldView4)
	enum { IDD = IDD_DIALOG_FORM4 };
	CEdit	m_ctrlGuid;
	CListCtrl	m_ctrlMsg;
	CEdit	m_ctrlRas;
	CButton	m_ctrlDisc;
	CButton	m_ctrlConn;
	//}}AFX_DATA

	//{{AFX_VIRTUAL(CChldView4)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

protected:
	//{{AFX_MSG(CChldView4)
	afx_msg void OnChangeEditRas();
	afx_msg void OnButtonConn();
	afx_msg void OnButtonDisc();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnDestroy();
	afx_msg void OnChangeEditGuid();
	//}}AFX_MSG
	afx_msg LRESULT OnConnMessage(WPARAM wParam, LPARAM lParam);
	DECLARE_MESSAGE_MAP()

private:
	bool m_bConnected;
	HANDLE m_hConnection;
	CRegPropertyInt m_Column0Width;
	CRegPropertyString m_LastRas;
	CRegPropertyString m_LastGuid;

	void UpdateControls();
	static CString ConnStateText(DWORD dwStatus);
	void BackupProperties();
};

/////////////////////////////////////////////////////////////////////////////
// CChldView5 form view

class CChldView5 : public CChldView
{
public:
	CChldView5();

	//{{AFX_DATA(CChldView5)
	enum { IDD = IDD_DIALOG_FORM5 };
	CListCtrl	m_ctrlMsg;
	CEdit	m_ctrlRas;
	CButton	m_ctrlDisc;
	CButton	m_ctrlConn;
	//}}AFX_DATA

	//{{AFX_VIRTUAL(CChldView5)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

protected:
	//{{AFX_MSG(CChldView5)
	afx_msg void OnButtonConn();
	afx_msg void OnButtonDisc();
	afx_msg void OnChangeEditRas();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnDestroy();
	//}}AFX_MSG
	afx_msg LRESULT OnRasDialEvent(WPARAM wParam, LPARAM lParam);
	DECLARE_MESSAGE_MAP()

private:
	bool m_bConnected;
	HRASCONN m_hConnection;
	static UINT s_nMsgId;
	CRegPropertyInt m_Column0Width;
	CRegPropertyInt m_Column1Width;
	CRegPropertyString m_LastRas;

	void UpdateControls();
	static CString RasConnStateText(RASCONNSTATE state);
	static CString RasErrorText(DWORD dwError);
	void BackupProperties();
};

/////////////////////////////////////////////////////////////////////////////
// CChldView6 form view

class CChldView6 : public CChldView
{
public:
	CChldView6();

	//{{AFX_DATA(CChldView6)
	enum { IDD = IDD_DIALOG_FORM6 };
	CButton	m_ctrlAsync;
	CEdit	m_ctrlTimeout;
	CEdit	m_ctrlPort;
	CEdit	m_ctrlAddress;
	CButton m_ctrlIpv4;
	CButton m_ctrlIpv6;
	//}}AFX_DATA

	//{{AFX_VIRTUAL(CChldView6)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

protected:
	//{{AFX_MSG(CChldView6)
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnButtonConn();
	afx_msg void OnCheckAsync();
	afx_msg void OnDestroy();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	CRegPropertyString m_LastAddress;
	CRegPropertyString m_LastPort;
	CRegPropertyBool m_LastIpv4;
	CRegPropertyBool m_LastIpv6;
	CRegPropertyBool m_LastAsync;
	CRegPropertyString m_LastTimeout;

	void UpdateControls();
	bool IsAsyncMode();
	void BackupProperties();
	int GetCurrAddrFamily();
};

/////////////////////////////////////////////////////////////////////////////
// CChldView7 form view

class CChldView7 : public CChldView
{
public:
	CChldView7();

	//{{AFX_DATA(CChldView7)
	enum { IDD = IDD_DIALOG_FORM7 };
	CListCtrl	m_ctrlAddress;
	CEdit	m_ctrlHost;
	CButton m_ctrlIpv4;
	CButton m_ctrlIpv6;
	//}}AFX_DATA

	//{{AFX_VIRTUAL(CChldView7)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

protected:
	//{{AFX_MSG(CChldView7)
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnButtonQuery();
	afx_msg void OnDestroy();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	CRegPropertyInt m_AddrColumn0Width;
	CRegPropertyString m_LastHost;
	CRegPropertyBool m_LastIpv4;
	CRegPropertyBool m_LastIpv6;

	void FillValues();
	void BackupProperties();
	int GetCurrAddrFamily();
};

/////////////////////////////////////////////////////////////////////////////
// CChldView8 form view

#define PING_TIMER 1
#define PING_TIMER_ELAPSE 1000
#define WAIT_REPLY_ELAPSE 1000

struct ICMP_REQUEST_DATA
{
	u_short ID;
	u_short Seq;
};

struct ICMP_REPLY_DATA
{
	ICMP_ECHO_REPLY reply;
	BYTE dummy[8];
};

class CChldView8 : public CChldView
{
public:
	CChldView8();

	//{{AFX_DATA(CChldView8)
	enum { IDD = IDD_DIALOG_FORM8 };
	CListCtrl	m_ctrlMsg;
	CEdit	m_ctrlHost;
	CButton	m_ctrlStop;
	CButton	m_ctrlStart;
	//}}AFX_DATA

	//{{AFX_VIRTUAL(CChldView8)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

protected:
	//{{AFX_MSG(CChldView8)
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnButtonStart();
	afx_msg void OnButtonStop();
	afx_msg void OnDestroy();
	afx_msg void OnTimer(UINT nIDEvent);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	SOCKET m_Socket;
	bool m_bWorking;
	SOCKADDR_IN m_saDest;
	u_short m_nId;
	u_short m_nSeq;
	CRegPropertyInt m_Column0Width;
	CRegPropertyInt m_Column1Width;
	CRegPropertyInt m_Column2Width;
	CRegPropertyString m_LastHost;
	HANDLE m_hIcmp;
	bool m_bIcmpPing;

	void Close();
	void UpdateControls();
	void ScheduleNext();
	void Echo();
	void SendRequest();
	bool WaitForReply(long nWaitMsec);
	void AppendMessage(LPCTSTR szMessage);
	void AppendMessage(char *szAddr, DWORD dwElapse, int nTTL);
	bool RecvReply(SOCKADDR_IN *psaFrom, u_char *pTTL, DWORD *pdwTime);
	static u_short in_cksum(u_short *addr, int len);
	void BackupProperties();

/////////////////////////////////////////////////////////////////////////////
// ICMP defines

#pragma pack(1)

#define ICMP_ECHOREPLY	0
#define ICMP_ECHOREQ	8

// IP Header -- RFC 791
typedef struct tagIPHDR
{
	u_char  VIHL;			// Version and IHL
	u_char	TOS;			// Type Of Service
	short	TotLen;			// Total Length
	short	ID;				// Identification
	short	FlagOff;		// Flags and Fragment Offset
	u_char	TTL;			// Time To Live
	u_char	Protocol;		// Protocol
	u_short	Checksum;		// Checksum
	struct	in_addr iaSrc;	// Internet Address - Source
	struct	in_addr iaDst;	// Internet Address - Destination
} IPHDR, *PIPHDR;

// ICMP Header - RFC 792
typedef struct tagICMPHDR
{
	u_char	Type;			// Type
	u_char	Code;			// Code
	u_short	Checksum;		// Checksum
	u_short	ID;				// Identification
	u_short	Seq;			// Sequence
} ICMPHDR, *PICMPHDR;

// ICMP Echo Request
typedef struct tagECHOREQUEST
{
	ICMPHDR icmpHdr;
	DWORD	dwTime;
} ECHOREQUEST, *PECHOREQUEST;

// ICMP Echo Reply
typedef struct tagECHOREPLY
{
	IPHDR	ipHdr;
	ECHOREQUEST	echoRequest;
} ECHOREPLY, *PECHOREPLY;

#pragma pack()
};

/////////////////////////////////////////////////////////////////////////////
// CChldView9 form view

class CChldView9 : public CChldView
{
public:
	CChldView9();

	static CString AdapterTypeText(UINT nType);

	//{{AFX_DATA(CChldView9)
	enum { IDD = IDD_DIALOG_FORM9 };
	CListCtrl	m_ctrlAdapters;
	//}}AFX_DATA

	//{{AFX_VIRTUAL(CChldView9)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

protected:
	//{{AFX_MSG(CChldView9)
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnButtonRefresh();
	afx_msg void OnDestroy();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	CRegPropertyInt m_Column0Width;
	CRegPropertyInt m_Column1Width;
	CRegPropertyInt m_Column2Width;
	CRegPropertyInt m_Column3Width;
	CRegPropertyInt m_Column4Width;
	CRegPropertyInt m_Column5Width;
	CRegPropertyInt m_Column6Width;

	static CString MacAddress(BYTE *pBuff, int nLen);
	void Fill();
	void BackupProperties();
};

/////////////////////////////////////////////////////////////////////////////
// CChldView10 form view

class CChldView10 : public CChldView
{
public:
	CChldView10();

	//{{AFX_DATA(CChldView10)
	enum { IDD = IDD_DIALOG_FORM10 };
	CListCtrl	m_ctrlAddr;
	//}}AFX_DATA

	//{{AFX_VIRTUAL(CChldView10)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

protected:
	//{{AFX_MSG(CChldView10)
	afx_msg void OnButtonRefresh();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnDestroy();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	CRegPropertyInt m_Column0Width;
	CRegPropertyInt m_Column1Width;

	void Fill();
	void BackupProperties();
};

/////////////////////////////////////////////////////////////////////////////
// CChldView11 form view

class CChldView11 : public CChldView
{
public:
	CChldView11();

	//{{AFX_DATA(CChldView11)
	enum { IDD = IDD_DIALOG_FORM11 };
	CListCtrl	m_ctrlRoute;
	//}}AFX_DATA

	//{{AFX_VIRTUAL(CChldView11)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

protected:
	//{{AFX_MSG(CChldView11)
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnButtonRefresh();
	afx_msg void OnDestroy();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	CRegPropertyInt m_Column0Width;
	CRegPropertyInt m_Column1Width;
	CRegPropertyInt m_Column2Width;
	CRegPropertyInt m_Column3Width;
	CRegPropertyInt m_Column4Width;
	CRegPropertyInt m_Column5Width;
	CRegPropertyInt m_Column6Width;

	void Fill();
	void BackupProperties();
	static CString RouteTypeText(DWORD dwType);
};

/////////////////////////////////////////////////////////////////////////////
// CChldView12 form view

class CChldView12 : public CChldView
{
public:
	CChldView12();

	static CString ProtocolText(int nProto);
	static CString SockTypeText(int nSockType);
	static CString AddressFamilyText(int nAddressFamily);

	//{{AFX_DATA(CChldView12)
	enum { IDD = IDD_DIALOG_FORM12 };
	CListCtrl	m_ctrlProto;
	//}}AFX_DATA

	//{{AFX_VIRTUAL(CChldView12)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

protected:
	//{{AFX_MSG(CChldView12)
	afx_msg void OnDestroy();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	CRegPropertyInt m_Column0Width;
	CRegPropertyInt m_Column1Width;
	CRegPropertyInt m_Column2Width;
	CRegPropertyInt m_Column3Width;

	void Fill();
	void BackupProperties();
};

/////////////////////////////////////////////////////////////////////////////
// CChldView13 form view

class CChldView13 : public CChldView
{
public:
	CChldView13();

	//{{AFX_DATA(CChldView13)
	enum { IDD = IDD_DIALOG_FORM13 };
	CListCtrl	m_ctrlItf;
	//}}AFX_DATA

protected:
	//{{AFX_VIRTUAL(CChldView13)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

	//{{AFX_MSG(CChldView13)
	afx_msg void OnDestroy();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnButtonRefresh();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	CRegPropertyInt m_Column0Width;
	CRegPropertyInt m_Column1Width;
	CRegPropertyInt m_Column2Width;
	CRegPropertyInt m_Column3Width;

	void Fill();
	void BackupProperties();
	static CString OperStatusText(DWORD dwStatus);
};

/////////////////////////////////////////////////////////////////////////////
// CChldView14 form view

class CChldView14 : public CChldView
{
public:
	CChldView14();

	//{{AFX_DATA(CChldView14)
	enum { IDD = IDD_DIALOG_FORM14 };
	CListCtrl	m_ctrlArp;
	//}}AFX_DATA

protected:
	//{{AFX_VIRTUAL(CChldView14)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

	//{{AFX_MSG(CChldView14)
	afx_msg void OnDestroy();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnButtonRefresh();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	CRegPropertyInt m_Column0Width;
	CRegPropertyInt m_Column1Width;
	CRegPropertyInt m_Column2Width;
	CRegPropertyInt m_Column3Width;

	void Fill();
	void BackupProperties();
	static CString EntryTypeText(DWORD dwType);
};

/////////////////////////////////////////////////////////////////////////////
//{{AFX_INSERT_LOCATION}}
// Microsoft eMbedded Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CHILDVIEW_H__AE575F94_A381_4334_B893_0ECDEB319D1A__INCLUDED_)
