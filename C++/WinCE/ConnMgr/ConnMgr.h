#if !defined(AFX_CONNMGR_H__AF4C60AA_3DEA_4668_A3BC_0BC955F9620F__INCLUDED_)
#define AFX_CONNMGR_H__AF4C60AA_3DEA_4668_A3BC_0BC955F9620F__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols
#include "Property.h"

/////////////////////////////////////////////////////////////////////////////
// CConnMgrApp:
//

class CConnMgrApp : public CWinApp
{
public:
	static CRegPropertySerializer *s_pPropertySerializer;

	//{{AFX_VIRTUAL(CConnMgrApp)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	//}}AFX_VIRTUAL

	//{{AFX_MSG(CConnMgrApp)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft eMbedded Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CONNMGR_H__AF4C60AA_3DEA_4668_A3BC_0BC955F9620F__INCLUDED_)
