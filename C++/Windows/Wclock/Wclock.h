/*****************************************************************************/
#if !defined(AFX_WCLOCK_H__69244D35_160F_11D8_A834_0060083EA000__INCLUDED_)
#define AFX_WCLOCK_H__69244D35_160F_11D8_A834_0060083EA000__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols
/*****************************************************************************/
class CWclockApp : public CWinApp
{
public:

	//{{AFX_VIRTUAL(CWclockApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

	//{{AFX_MSG(CWclockApp)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
/*****************************************************************************/
//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_WCLOCK_H__69244D35_160F_11D8_A834_0060083EA000__INCLUDED_)
/*****************************************************************************/
