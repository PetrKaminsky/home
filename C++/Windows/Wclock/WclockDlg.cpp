/*****************************************************************************/
#include "stdafx.h"
#include "Wclock.h"
#include "WclockDlg.h"
/*****************************************************************************/
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif
/*****************************************************************************/
#define WCLOCK_TIMER 1
#define WCLOCK_TIMER_PERIOD 500
/*****************************************************************************/
CWclockDlg::CWclockDlg(CWnd *pParent /*=NULL*/)
:	CDialog(CWclockDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CWclockDlg)
	//}}AFX_DATA_INIT
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_dSqrt3Div2 = sqrt(3) / 2;
	m_dSmallA = 0.12;
	m_dSmallB = 0.055;
	m_dBigA = 0.2;
	m_dBigB = 0.07;
	m_nHour = 0;
	m_nMinute = 0;
	m_dHourA = 0.6;
	m_dHourB = 0.09;
	m_dMinuteA = 0.9;
	m_dMinuteB = 0.08;
	m_bTopmost = FALSE;
	m_bCaption = TRUE;
	m_bTransparent = FALSE;
	m_clrBg = RGB(0, 0, 0);
	m_clrFg = RGB(255, 255, 0);
	m_dPi = acos(-1);
	m_nFrameDiv = 100;
}
/*****************************************************************************/
void CWclockDlg::DoDataExchange(CDataExchange *pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CWclockDlg)
	//}}AFX_DATA_MAP
}
/*****************************************************************************/
BEGIN_MESSAGE_MAP(CWclockDlg, CDialog)
	//{{AFX_MSG_MAP(CWclockDlg)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_CLOSE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_TIMECHANGE()
	ON_WM_TIMER()
	ON_WM_CONTEXTMENU()
	ON_WM_LBUTTONDOWN()
	ON_COMMAND(ID_TRANSPARENT, OnMenuTransparent)
	ON_COMMAND(ID_TOPMOST, OnMenuTopMost)
	ON_COMMAND(ID_CAPTION, OnMenuCaption)
	ON_COMMAND(ID_RESTORE, OnMenuRestore)
	ON_COMMAND(ID_MOVE, OnMenuMove)
	ON_COMMAND(ID_SIZE, OnMenuSize)
	ON_COMMAND(ID_MINIMIZE, OnMenuMinimize)
	ON_COMMAND(ID_MAXIMIZE, OnMenuMaximize)
	ON_COMMAND(ID_CLOSE, OnMenuClose)
	ON_WM_SETCURSOR()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()
/*****************************************************************************/
BOOL CWclockDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	SetIcon(m_hIcon, TRUE);
	SetIcon(m_hIcon, FALSE);
	
	SetTimer(WCLOCK_TIMER, WCLOCK_TIMER_PERIOD, NULL);

	RefreshTime();
	m_brBg.CreateSolidBrush(m_clrBg);
	m_brFg.CreateSolidBrush(m_clrFg);

	return TRUE;
}
/*****************************************************************************/
void CWclockDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM)dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect r;
		GetClientRect(&r);
		int x = (r.Width() - cxIcon + 1) / 2;
		int y = (r.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CPaintDC dc(this);
		CRect r;
		GetClientRect(&r);
		CPoint pt = r.CenterPoint();
		int nRadius = r.Height();
		if (nRadius > r.Width())
			nRadius = r.Width();
		nRadius = (int)(0.92 * nRadius / 2);
		CPen pen;
		pen.CreatePen(PS_NULL, 0, (COLORREF)0);
		CPen *pOldPen = dc.SelectObject(&pen);
		dc.SetPolyFillMode(WINDING);
		CBrush *pOldBr = dc.GetCurrentBrush();
		DrawPoly01(dc, pt, nRadius, TRUE);
		DrawPoly02(dc, pt, nRadius, TRUE);
		DrawPoly03(dc, pt, nRadius, TRUE);
		DrawPoly04(dc, pt, nRadius, TRUE);
		DrawPoly05(dc, pt, nRadius, TRUE);
		DrawPoly06(dc, pt, nRadius, TRUE);
		DrawPoly07(dc, pt, nRadius, TRUE);
		DrawPoly08(dc, pt, nRadius, TRUE);
		DrawPoly09(dc, pt, nRadius, TRUE);
		DrawPoly10(dc, pt, nRadius, TRUE);
		DrawPoly11(dc, pt, nRadius, TRUE);
		DrawPoly12(dc, pt, nRadius, TRUE);
		DrawPolyHourPointer(dc, pt, nRadius, TRUE);
		DrawPolyMinutePointer(dc, pt, nRadius, TRUE);
		dc.SelectObject(pOldPen);
		dc.SelectObject(pOldBr);
	}
}
/*****************************************************************************/
HCURSOR CWclockDlg::OnQueryDragIcon()
{
	return (HCURSOR)m_hIcon;
}
/*****************************************************************************/
void CWclockDlg::OnOK()
{
}
/*****************************************************************************/
void CWclockDlg::OnCancel()
{
	PostMessage(WM_SYSCOMMAND, SC_MINIMIZE);
}
/*****************************************************************************/
void CWclockDlg::OnClose()
{
	DestroyWindow();
}
/*****************************************************************************/
void CWclockDlg::OnSize(UINT nType, int cx, int cy)
{
	CDialog::OnSize(nType, cx, cy);
	SetRegion();
	Invalidate();
}
/*****************************************************************************/
void CWclockDlg::CalcPoly(
	double dRadius,
	double dLength,
	double dWidth,
	double dCos,
	double dSin,
	CPoint &pt,
	POINT poly[])
{
	dWidth /= 2;
	if ((int)dWidth < 1)
		dWidth = 1;
	if ((int)dLength < 1)
		dLength = 1;
	double dX = dRadius * dCos;
	double dY = dRadius * dSin;
	double dWX = dWidth * dSin;
	double dWY = dWidth * dCos;
	double dLX = dLength * dCos;
	double dLY = dLength * dSin;
	poly[1].x = (LONG)(pt.x - dWX + dX);
	poly[1].y = (LONG)(pt.y + dWY + dY);
	poly[2].x = (LONG)(pt.x + dWX + dX);
	poly[2].y = (LONG)(pt.y - dWY + dY);
	poly[0].x = (LONG)(pt.x - dWX + dX - dLX);
	poly[0].y = (LONG)(pt.y + dWY + dY - dLY);
	poly[3].x = (LONG)(pt.x + dWX + dX - dLX);
	poly[3].y = (LONG)(pt.y - dWY + dY - dLY);
}
/*****************************************************************************/
void CWclockDlg::CalcPoly01(CPoint &pt, int nRadius, POINT poly[], BOOL bSmall)
{
	double dRealRadius = nRadius;
	double dRealLength = m_dSmallA * nRadius;
	double dRealWidth = m_dSmallB * nRadius;
	if (bSmall)
		Recalc(dRealRadius, dRealLength, dRealWidth, nRadius);
	CalcPoly(
		dRealRadius,
		dRealLength,
		dRealWidth,
		0.5,
		-m_dSqrt3Div2,
		pt,
		poly);
}
/*****************************************************************************/
void CWclockDlg::CalcPoly02(CPoint &pt, int nRadius, POINT poly[], BOOL bSmall)
{
	double dRealRadius = nRadius;
	double dRealLength = m_dSmallA * nRadius;
	double dRealWidth = m_dSmallB * nRadius;
	if (bSmall)
		Recalc(dRealRadius, dRealLength, dRealWidth, nRadius);
	CalcPoly(
		dRealRadius,
		dRealLength,
		dRealWidth,
		m_dSqrt3Div2,
		-0.5,
		pt,
		poly);
}
/*****************************************************************************/
void CWclockDlg::CalcPoly03(CPoint &pt, int nRadius, POINT poly[], BOOL bSmall)
{
	double dRealRadius = nRadius;
	double dRealLength = m_dBigA * nRadius;
	double dRealWidth = m_dBigB * nRadius;
	if (bSmall)
		Recalc(dRealRadius, dRealLength, dRealWidth, nRadius);
	CalcPoly(
		dRealRadius,
		dRealLength,
		dRealWidth,
		1,
		0,
		pt,
		poly);
}
/*****************************************************************************/
void CWclockDlg::CalcPoly04(CPoint &pt, int nRadius, POINT poly[], BOOL bSmall)
{
	double dRealRadius = nRadius;
	double dRealLength = m_dSmallA * nRadius;
	double dRealWidth = m_dSmallB * nRadius;
	if (bSmall)
		Recalc(dRealRadius, dRealLength, dRealWidth, nRadius);
	CalcPoly(
		dRealRadius,
		dRealLength,
		dRealWidth,
		m_dSqrt3Div2,
		0.5,
		pt,
		poly);
}
/*****************************************************************************/
void CWclockDlg::CalcPoly05(CPoint &pt, int nRadius, POINT poly[], BOOL bSmall)
{
	double dRealRadius = nRadius;
	double dRealLength = m_dSmallA * nRadius;
	double dRealWidth = m_dSmallB * nRadius;
	if (bSmall)
		Recalc(dRealRadius, dRealLength, dRealWidth, nRadius);
	CalcPoly(
		dRealRadius,
		dRealLength,
		dRealWidth,
		0.5,
		m_dSqrt3Div2,
		pt,
		poly);
}
/*****************************************************************************/
void CWclockDlg::CalcPoly06(CPoint &pt, int nRadius, POINT poly[], BOOL bSmall)
{
	double dRealRadius = nRadius;
	double dRealLength = m_dBigA * nRadius;
	double dRealWidth = m_dBigB * nRadius;
	if (bSmall)
		Recalc(dRealRadius, dRealLength, dRealWidth, nRadius);
	CalcPoly(
		dRealRadius,
		dRealLength,
		dRealWidth,
		0,
		1,
		pt,
		poly);
}
/*****************************************************************************/
void CWclockDlg::CalcPoly07(CPoint &pt, int nRadius, POINT poly[], BOOL bSmall)
{
	double dRealRadius = nRadius;
	double dRealLength = m_dSmallA * nRadius;
	double dRealWidth = m_dSmallB * nRadius;
	if (bSmall)
		Recalc(dRealRadius, dRealLength, dRealWidth, nRadius);
	CalcPoly(
		dRealRadius,
		dRealLength,
		dRealWidth,
		-0.5,
		m_dSqrt3Div2,
		pt,
		poly);
}
/*****************************************************************************/
void CWclockDlg::CalcPoly08(CPoint &pt, int nRadius, POINT poly[], BOOL bSmall)
{
	double dRealRadius = nRadius;
	double dRealLength = m_dSmallA * nRadius;
	double dRealWidth = m_dSmallB * nRadius;
	if (bSmall)
		Recalc(dRealRadius, dRealLength, dRealWidth, nRadius);
	CalcPoly(
		dRealRadius,
		dRealLength,
		dRealWidth,
		-m_dSqrt3Div2,
		0.5,
		pt,
		poly);
}
/*****************************************************************************/
void CWclockDlg::CalcPoly09(CPoint &pt, int nRadius, POINT poly[], BOOL bSmall)
{
	double dRealRadius = nRadius;
	double dRealLength = m_dBigA * nRadius;
	double dRealWidth = m_dBigB * nRadius;
	if (bSmall)
		Recalc(dRealRadius, dRealLength, dRealWidth, nRadius);
	CalcPoly(
		dRealRadius,
		dRealLength,
		dRealWidth,
		-1,
		0,
		pt,
		poly);
}
/*****************************************************************************/
void CWclockDlg::CalcPoly10(CPoint &pt, int nRadius, POINT poly[], BOOL bSmall)
{
	double dRealRadius = nRadius;
	double dRealLength = m_dSmallA * nRadius;
	double dRealWidth = m_dSmallB * nRadius;
	if (bSmall)
		Recalc(dRealRadius, dRealLength, dRealWidth, nRadius);
	CalcPoly(
		dRealRadius,
		dRealLength,
		dRealWidth,
		-m_dSqrt3Div2,
		-0.5,
		pt,
		poly);
}
/*****************************************************************************/
void CWclockDlg::CalcPoly11(CPoint &pt, int nRadius, POINT poly[], BOOL bSmall)
{
	double dRealRadius = nRadius;
	double dRealLength = m_dSmallA * nRadius;
	double dRealWidth = m_dSmallB * nRadius;
	if (bSmall)
		Recalc(dRealRadius, dRealLength, dRealWidth, nRadius);
	CalcPoly(
		dRealRadius,
		dRealLength,
		dRealWidth,
		-0.5,
		-m_dSqrt3Div2,
		pt,
		poly);
}
/*****************************************************************************/
void CWclockDlg::CalcPoly12(CPoint &pt, int nRadius, POINT poly[], BOOL bSmall)
{
	double dRealRadius = nRadius;
	double dRealLength = m_dBigA * nRadius;
	double dRealWidth = m_dBigB * nRadius;
	if (bSmall)
		Recalc(dRealRadius, dRealLength, dRealWidth, nRadius);
	CalcPoly(
		dRealRadius,
		dRealLength,
		dRealWidth,
		0,
		-1,
		pt,
		poly);
}
/*****************************************************************************/
void CWclockDlg::CalcPolyHourPointer(CPoint &pt, int nRadius, POINT poly[], BOOL bSmall)
{
	double dAngle = (m_nHour % 12) * 30 + (double)m_nMinute / 2 - 90;
	dAngle *= m_dPi / 180;
	double dRealRadius = m_dHourA * nRadius;
	double dRealLength = (m_dHourA * 1.1) * (double)nRadius;
	double dRealWidth = m_dHourB * nRadius;
	if (bSmall)
		Recalc(dRealRadius, dRealLength, dRealWidth, nRadius);
	CalcPoly(
		dRealRadius,
		dRealLength,
		dRealWidth,
		cos(dAngle),
		sin(dAngle),
		pt,
		poly);
}
/*****************************************************************************/
void CWclockDlg::CalcPolyMinutePointer(CPoint &pt, int nRadius, POINT poly[], BOOL bSmall)
{
	double dAngle = m_nMinute * 6 - 90;
	dAngle *= m_dPi / 180;
	double dRealRadius = m_dMinuteA * nRadius;
	double dRealLength = (m_dMinuteA * 1.1) * (double)nRadius;
	double dRealWidth = m_dMinuteB * nRadius;
	if (bSmall)
		Recalc(dRealRadius, dRealLength, dRealWidth, nRadius);
	CalcPoly(
		dRealRadius,
		dRealLength,
		dRealWidth,
		cos(dAngle),
		sin(dAngle),
		pt,
		poly);
}
/*****************************************************************************/
void CWclockDlg::OnDestroy()
{
	CDialog::OnDestroy();
	KillTimer(WCLOCK_TIMER);
}
/*****************************************************************************/
void CWclockDlg::OnTimeChange()
{
	CDialog::OnTimeChange();
	RefreshTime();
	SetRegion();
	Invalidate();
}
/*****************************************************************************/
void CWclockDlg::OnTimer(UINT nIDEvent)
{
	if (nIDEvent == WCLOCK_TIMER)
	{
		CTime t(CTime::GetCurrentTime());
		if (m_nHour != t.GetHour() || m_nMinute != t.GetMinute())
		{
			m_nHour = t.GetHour();
			m_nMinute = t.GetMinute();
			SetRegion();
			Invalidate();
		}
	}
	else
		CDialog::OnTimer(nIDEvent);
}
/*****************************************************************************/
void CWclockDlg::RefreshTime()
{
	CTime t(CTime::GetCurrentTime());
	m_nHour = t.GetHour();
	m_nMinute = t.GetMinute();
}
/*****************************************************************************/
void CWclockDlg::OnContextMenu(CWnd *pWnd, CPoint point)
{
	CRect r;
	if (point.x == -1 &&
		point.y == -1)
	{
		GetWindowRect(&r);
		point.x = r.left;
		point.y = r.top;
	}
	else
	{
		GetClientRect(&r);
		ClientToScreen(&r);
		if (!r.PtInRect(point))
		{
			CDialog::OnContextMenu(pWnd, point);
			return;
		}
	}
	CMenu Menu;
	Menu.LoadMenu(IDR_MENU);
	CMenu* pCtxMenu = Menu.GetSubMenu(0);
	if (m_bTransparent)
		pCtxMenu->CheckMenuItem(ID_TRANSPARENT, MF_CHECKED | MF_BYCOMMAND);
	if (m_bTopmost)
		pCtxMenu->CheckMenuItem(ID_TOPMOST, MF_CHECKED | MF_BYCOMMAND);
	if (m_bCaption)
		pCtxMenu->CheckMenuItem(ID_CAPTION, MF_CHECKED | MF_BYCOMMAND);
	WINDOWPLACEMENT wp;
	GetWindowPlacement(&wp);
	if (wp.showCmd == SW_SHOWMAXIMIZED)
	{
		pCtxMenu->EnableMenuItem(ID_MOVE, MF_GRAYED | MF_BYCOMMAND);
		pCtxMenu->EnableMenuItem(ID_SIZE, MF_GRAYED | MF_BYCOMMAND);
		pCtxMenu->EnableMenuItem(ID_MAXIMIZE, MF_GRAYED | MF_BYCOMMAND);
	}
	else if (wp.showCmd == SW_SHOWNORMAL)
		pCtxMenu->EnableMenuItem(ID_RESTORE, MF_GRAYED | MF_BYCOMMAND);
	pCtxMenu->TrackPopupMenu(
		TPM_LEFTALIGN | TPM_LEFTBUTTON,
		point.x,
		point.y,
		this);
}
/*****************************************************************************/
void CWclockDlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	if (m_bCaption)
		CDialog::OnLButtonDown(nFlags, point);
	else
		PostMessage(WM_NCLBUTTONDOWN, HTCAPTION, MAKELONG(point.x, point.y));
}
/*****************************************************************************/
void CWclockDlg::OnMenuTransparent()
{
	m_bTransparent = !m_bTransparent;
	SetRegion();
	Invalidate();
}
/*****************************************************************************/
void CWclockDlg::OnMenuTopMost()
{
	m_bTopmost = !m_bTopmost;
	SetWindowPos(
		m_bTopmost ? &wndTopMost : &wndNoTopMost,
		0,
		0,
		0,
		0,
		SWP_NOMOVE | SWP_NOSIZE);
}
/*****************************************************************************/
void CWclockDlg::OnMenuCaption()
{
	m_bCaption = !m_bCaption;
	if (m_bCaption)
		ModifyStyle(0, WS_CAPTION, SWP_FRAMECHANGED);
	else
		ModifyStyle(WS_CAPTION, 0, SWP_FRAMECHANGED);
}
/*****************************************************************************/
void CWclockDlg::OnMenuRestore()
{
	PostMessage(WM_SYSCOMMAND, SC_RESTORE);
}
/*****************************************************************************/
void CWclockDlg::OnMenuMove()
{
	PostMessage(WM_SYSCOMMAND, SC_MOVE);
}
/*****************************************************************************/
void CWclockDlg::OnMenuSize()
{
	PostMessage(WM_SYSCOMMAND, SC_SIZE);
}
/*****************************************************************************/
void CWclockDlg::OnMenuMinimize()
{
	PostMessage(WM_SYSCOMMAND, SC_MINIMIZE);
}
/*****************************************************************************/
void CWclockDlg::OnMenuMaximize()
{
	PostMessage(WM_SYSCOMMAND, SC_MAXIMIZE);
}
/*****************************************************************************/
void CWclockDlg::OnMenuClose()
{
	DestroyWindow();
}
/*****************************************************************************/
void CWclockDlg::SetRegion()
{
	if (!m_bTransparent)
	{
		SetWindowRgn(NULL, TRUE);
		return;
	}
	CRect r, r1;
	GetClientRect(&r);
	GetWindowRect(&r1);
	r1.OffsetRect(-r1.left, -r1.top);
	int nBorderWidth = (r1.Width() - r.Width()) / 2;
	int nLeft = r.left + nBorderWidth;
	int nTop = r1.Height() - r.Height() - nBorderWidth;
	CPoint pt = r.CenterPoint();
	pt.Offset(nLeft, nTop);
	int nRadius = r.Height();
	if (nRadius > r.Width())
		nRadius = r.Width();
	nRadius = (int)(0.92 * nRadius / 2);
	CClientDC cdc(this);
	CDC dc;
	dc.CreateCompatibleDC(&cdc);
	dc.SetPolyFillMode(WINDING);
	dc.BeginPath();
	if (m_bCaption)
	{
		POINT poly[] =
		{
			{ r1.left, r1.top },
			{ r1.left, nTop },
			{ r1.right, nTop },
			{ r1.right, r1.top }
		};
		dc.Polygon(poly, 4);
	}
	DrawPoly01(dc, pt, nRadius, FALSE);
	DrawPoly02(dc, pt, nRadius, FALSE);
	DrawPoly03(dc, pt, nRadius, FALSE);
	DrawPoly04(dc, pt, nRadius, FALSE);
	DrawPoly05(dc, pt, nRadius, FALSE);
	DrawPoly06(dc, pt, nRadius, FALSE);
	DrawPoly07(dc, pt, nRadius, FALSE);
	DrawPoly08(dc, pt, nRadius, FALSE);
	DrawPoly09(dc, pt, nRadius, FALSE);
	DrawPoly10(dc, pt, nRadius, FALSE);
	DrawPoly11(dc, pt, nRadius, FALSE);
	DrawPoly12(dc, pt, nRadius, FALSE);
	DrawPolyHourPointer(dc, pt, nRadius, FALSE);
	DrawPolyMinutePointer(dc, pt, nRadius, FALSE);
	dc.EndPath();
	CRgn rgn;
	rgn.CreateFromPath(&dc);
	SetWindowRgn(rgn, TRUE);
}
/*****************************************************************************/
BOOL CWclockDlg::OnSetCursor(CWnd *pWnd, UINT nHitTest, UINT message)
{
	switch (nHitTest)
	{
	case HTCAPTION:
	case HTCLIENT:
	case HTMAXBUTTON:
	case HTMINBUTTON:
	case HTSYSMENU:
	case HTCLOSE:
		SetCursor(
			LoadCursor(
				AfxGetInstanceHandle(),
				MAKEINTRESOURCE(IDC_CURSOR)));
		return TRUE;
	}
	return CDialog::OnSetCursor(pWnd, nHitTest, message);
}
/*****************************************************************************/
void CWclockDlg::DrawPoly01(CDC &dc, CPoint &pt, int nRadius, BOOL bPaint)
{
	POINT poly[4];
	if (bPaint)
		dc.SelectObject(&m_brBg);
	CalcPoly01(pt, nRadius, poly, FALSE);
	dc.Polygon(poly, 4);
	if (bPaint)
	{
		dc.SelectObject(&m_brFg);
		CalcPoly01(pt, nRadius, poly, TRUE);
		dc.Polygon(poly, 4);
	}
}
/*****************************************************************************/
void CWclockDlg::DrawPoly02(CDC &dc, CPoint &pt, int nRadius, BOOL bPaint)
{
	POINT poly[4];
	if (bPaint)
		dc.SelectObject(&m_brBg);
	CalcPoly02(pt, nRadius, poly, FALSE);
	dc.Polygon(poly, 4);
	if (bPaint)
	{
		dc.SelectObject(&m_brFg);
		CalcPoly02(pt, nRadius, poly, TRUE);
		dc.Polygon(poly, 4);
	}
}
/*****************************************************************************/
void CWclockDlg::DrawPoly03(CDC &dc, CPoint &pt, int nRadius, BOOL bPaint)
{
	POINT poly[4];
	if (bPaint)
		dc.SelectObject(&m_brBg);
	CalcPoly03(pt, nRadius, poly, FALSE);
	dc.Polygon(poly, 4);
	if (bPaint)
	{
		dc.SelectObject(&m_brFg);
		CalcPoly03(pt, nRadius, poly, TRUE);
		dc.Polygon(poly, 4);
	}
}
/*****************************************************************************/
void CWclockDlg::DrawPoly04(CDC &dc, CPoint &pt, int nRadius, BOOL bPaint)
{
	POINT poly[4];
	if (bPaint)
		dc.SelectObject(&m_brBg);
	CalcPoly04(pt, nRadius, poly, FALSE);
	dc.Polygon(poly, 4);
	if (bPaint)
	{
		dc.SelectObject(&m_brFg);
		CalcPoly04(pt, nRadius, poly, TRUE);
		dc.Polygon(poly, 4);
	}
}
/*****************************************************************************/
void CWclockDlg::DrawPoly05(CDC &dc, CPoint &pt, int nRadius, BOOL bPaint)
{
	POINT poly[4];
	if (bPaint)
		dc.SelectObject(&m_brBg);
	CalcPoly05(pt, nRadius, poly, FALSE);
	dc.Polygon(poly, 4);
	if (bPaint)
	{
		dc.SelectObject(&m_brFg);
		CalcPoly05(pt, nRadius, poly, TRUE);
		dc.Polygon(poly, 4);
	}
}
/*****************************************************************************/
void CWclockDlg::DrawPoly06(CDC &dc, CPoint &pt, int nRadius, BOOL bPaint)
{
	POINT poly[4];
	if (bPaint)
		dc.SelectObject(&m_brBg);
	CalcPoly06(pt, nRadius, poly, FALSE);
	dc.Polygon(poly, 4);
	if (bPaint)
	{
		dc.SelectObject(&m_brFg);
		CalcPoly06(pt, nRadius, poly, TRUE);
		dc.Polygon(poly, 4);
	}
}
/*****************************************************************************/
void CWclockDlg::DrawPoly07(CDC &dc, CPoint &pt, int nRadius, BOOL bPaint)
{
	POINT poly[4];
	if (bPaint)
		dc.SelectObject(&m_brBg);
	CalcPoly07(pt, nRadius, poly, FALSE);
	dc.Polygon(poly, 4);
	if (bPaint)
	{
		dc.SelectObject(&m_brFg);
		CalcPoly07(pt, nRadius, poly, TRUE);
		dc.Polygon(poly, 4);
	}
}
/*****************************************************************************/
void CWclockDlg::DrawPoly08(CDC &dc, CPoint &pt, int nRadius, BOOL bPaint)
{
	POINT poly[4];
	if (bPaint)
		dc.SelectObject(&m_brBg);
	CalcPoly08(pt, nRadius, poly, FALSE);
	dc.Polygon(poly, 4);
	if (bPaint)
	{
		dc.SelectObject(&m_brFg);
		CalcPoly08(pt, nRadius, poly, TRUE);
		dc.Polygon(poly, 4);
	}
}
/*****************************************************************************/
void CWclockDlg::DrawPoly09(CDC &dc, CPoint &pt, int nRadius, BOOL bPaint)
{
	POINT poly[4];
	if (bPaint)
		dc.SelectObject(&m_brBg);
	CalcPoly09(pt, nRadius, poly, FALSE);
	dc.Polygon(poly, 4);
	if (bPaint)
	{
		dc.SelectObject(&m_brFg);
		CalcPoly09(pt, nRadius, poly, TRUE);
		dc.Polygon(poly, 4);
	}
}
/*****************************************************************************/
void CWclockDlg::DrawPoly10(CDC &dc, CPoint &pt, int nRadius, BOOL bPaint)
{
	POINT poly[4];
	if (bPaint)
		dc.SelectObject(&m_brBg);
	CalcPoly10(pt, nRadius, poly, FALSE);
	dc.Polygon(poly, 4);
	if (bPaint)
	{
		dc.SelectObject(&m_brFg);
		CalcPoly10(pt, nRadius, poly, TRUE);
		dc.Polygon(poly, 4);
	}
}
/*****************************************************************************/
void CWclockDlg::DrawPoly11(CDC &dc, CPoint &pt, int nRadius, BOOL bPaint)
{
	POINT poly[4];
	if (bPaint)
		dc.SelectObject(&m_brBg);
	CalcPoly11(pt, nRadius, poly, FALSE);
	dc.Polygon(poly, 4);
	if (bPaint)
	{
		dc.SelectObject(&m_brFg);
		CalcPoly11(pt, nRadius, poly, TRUE);
		dc.Polygon(poly, 4);
	}
}
/*****************************************************************************/
void CWclockDlg::DrawPoly12(CDC &dc, CPoint &pt, int nRadius, BOOL bPaint)
{
	POINT poly[4];
	if (bPaint)
		dc.SelectObject(&m_brBg);
	CalcPoly12(pt, nRadius, poly, FALSE);
	dc.Polygon(poly, 4);
	if (bPaint)
	{
		dc.SelectObject(&m_brFg);
		CalcPoly12(pt, nRadius, poly, TRUE);
		dc.Polygon(poly, 4);
	}
}
/*****************************************************************************/
void CWclockDlg::DrawPolyHourPointer(CDC &dc, CPoint &pt, int nRadius, BOOL bPaint)
{
	POINT poly[4];
	if (bPaint)
		dc.SelectObject(&m_brBg);
	CalcPolyHourPointer(pt, nRadius, poly, FALSE);
	dc.Polygon(poly, 4);
	if (bPaint)
	{
		dc.SelectObject(&m_brFg);
		CalcPolyHourPointer(pt, nRadius, poly, TRUE);
		dc.Polygon(poly, 4);
	}
}
/*****************************************************************************/
void CWclockDlg::DrawPolyMinutePointer(CDC &dc, CPoint &pt, int nRadius, BOOL bPaint)
{
	POINT poly[4];
	if (bPaint)
		dc.SelectObject(&m_brBg);
	CalcPolyMinutePointer(pt, nRadius, poly, FALSE);
	dc.Polygon(poly, 4);
	if (bPaint)
	{
		dc.SelectObject(&m_brFg);
		CalcPolyMinutePointer(pt, nRadius, poly, TRUE);
		dc.Polygon(poly, 4);
	}
}
/*****************************************************************************/
void CWclockDlg::Recalc(
	double &dRealRadius,
	double &dRealLength,
	double &dRealWidth,
	int nRadius)
{
	int nFrame = 3 + nRadius / m_nFrameDiv;
	dRealRadius -= nFrame;
	dRealLength -= (2 * nFrame);
	dRealWidth -= (2 * nFrame);
}
/*****************************************************************************/
