/*****************************************************************************/
#include "stdafx.h"
#include "Wclock.h"
#include "WclockDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif
/*****************************************************************************/
BEGIN_MESSAGE_MAP(CWclockApp, CWinApp)
	//{{AFX_MSG_MAP(CWclockApp)
	//}}AFX_MSG
END_MESSAGE_MAP()
/*****************************************************************************/
CWclockApp theApp;
/*****************************************************************************/
BOOL CWclockApp::InitInstance()
{
#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif

	CWclockDlg dlg;
	m_pMainWnd = &dlg;
	dlg.DoModal();
	return FALSE;
}
/*****************************************************************************/
