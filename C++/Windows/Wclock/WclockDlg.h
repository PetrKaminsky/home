/*****************************************************************************/
#if !defined(AFX_WCLOCKDLG_H__69244D37_160F_11D8_A834_0060083EA000__INCLUDED_)
#define AFX_WCLOCKDLG_H__69244D37_160F_11D8_A834_0060083EA000__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
/*****************************************************************************/
class CWclockDlg : public CDialog
{
public:
	CWclockDlg(CWnd* pParent = NULL);	// standard constructor

	//{{AFX_DATA(CWclockDlg)
	enum { IDD = IDD_WCLOCK_DIALOG };
	//}}AFX_DATA

	//{{AFX_VIRTUAL(CWclockDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

protected:
	HICON m_hIcon;
	double m_dSqrt3Div2;
	double m_dSmallA;
	double m_dSmallB;
	double m_dBigA;
	double m_dBigB;
	int m_nHour;
	int m_nMinute;
	double m_dHourA;
	double m_dHourB;
	double m_dMinuteA;
	double m_dMinuteB;
	BOOL m_bTopmost;
	BOOL m_bCaption;
	BOOL m_bTransparent;
	COLORREF m_clrBg;
	COLORREF m_clrFg;
	double m_dPi;
	int m_nFrameDiv;
	CBrush m_brBg;
	CBrush m_brFg;

	void CalcPoly01(CPoint &pt, int nRadius, POINT poly[], BOOL bSmall);
	void CalcPoly02(CPoint &pt, int nRadius, POINT poly[], BOOL bSmall);
	void CalcPoly03(CPoint &pt, int nRadius, POINT poly[], BOOL bSmall);
	void CalcPoly04(CPoint &pt, int nRadius, POINT poly[], BOOL bSmall);
	void CalcPoly05(CPoint &pt, int nRadius, POINT poly[], BOOL bSmall);
	void CalcPoly06(CPoint &pt, int nRadius, POINT poly[], BOOL bSmall);
	void CalcPoly07(CPoint &pt, int nRadius, POINT poly[], BOOL bSmall);
	void CalcPoly08(CPoint &pt, int nRadius, POINT poly[], BOOL bSmall);
	void CalcPoly09(CPoint &pt, int nRadius, POINT poly[], BOOL bSmall);
	void CalcPoly10(CPoint &pt, int nRadius, POINT poly[], BOOL bSmall);
	void CalcPoly11(CPoint &pt, int nRadius, POINT poly[], BOOL bSmall);
	void CalcPoly12(CPoint &pt, int nRadius, POINT poly[], BOOL bSmall);
	void CalcPolyHourPointer(CPoint &pt, int nRadius, POINT poly[], BOOL bSmall);
	void CalcPolyMinutePointer(CPoint &pt, int nRadius, POINT poly[], BOOL bSmall);
	void CalcPoly(
		double dRadius,
		double dLength,
		double dWidth,
		double dCos,
		double dSin,
		CPoint &pt,
		POINT poly[]);
	void RefreshTime();
	void SetRegion();
	void DrawPoly01(CDC &dc, CPoint &pt, int nRadius, BOOL bPaint);
	void DrawPoly02(CDC &dc, CPoint &pt, int nRadius, BOOL bPaint);
	void DrawPoly03(CDC &dc, CPoint &pt, int nRadius, BOOL bPaint);
	void DrawPoly04(CDC &dc, CPoint &pt, int nRadius, BOOL bPaint);
	void DrawPoly05(CDC &dc, CPoint &pt, int nRadius, BOOL bPaint);
	void DrawPoly06(CDC &dc, CPoint &pt, int nRadius, BOOL bPaint);
	void DrawPoly07(CDC &dc, CPoint &pt, int nRadius, BOOL bPaint);
	void DrawPoly08(CDC &dc, CPoint &pt, int nRadius, BOOL bPaint);
	void DrawPoly09(CDC &dc, CPoint &pt, int nRadius, BOOL bPaint);
	void DrawPoly10(CDC &dc, CPoint &pt, int nRadius, BOOL bPaint);
	void DrawPoly11(CDC &dc, CPoint &pt, int nRadius, BOOL bPaint);
	void DrawPoly12(CDC &dc, CPoint &pt, int nRadius, BOOL bPaint);
	void DrawPolyHourPointer(CDC &dc, CPoint &pt, int nRadius, BOOL bPaint);
	void DrawPolyMinutePointer(CDC &dc, CPoint &pt, int nRadius, BOOL bPaint);
	void Recalc(
		double &dRealRadius,
		double &dRealLength,
		double &dRealWidth,
		int nRadius);

	//{{AFX_MSG(CWclockDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	virtual void OnOK();
	virtual void OnCancel();
	afx_msg void OnClose();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnDestroy();
	afx_msg void OnTimeChange();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnMenuTransparent();
	afx_msg void OnMenuTopMost();
	afx_msg void OnMenuCaption();
	afx_msg void OnMenuRestore();
	afx_msg void OnMenuMove();
	afx_msg void OnMenuSize();
	afx_msg void OnMenuMinimize();
	afx_msg void OnMenuMaximize();
	afx_msg void OnMenuClose();
	afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
/*****************************************************************************/
//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_WCLOCKDLG_H__69244D37_160F_11D8_A834_0060083EA000__INCLUDED_)
/*****************************************************************************/
