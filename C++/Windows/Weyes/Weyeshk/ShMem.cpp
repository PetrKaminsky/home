/*****************************************************************************/
#include "stdafx.h"
#include "ShMem.h"
/*****************************************************************************/
using namespace std;
/*****************************************************************************/
CSharedMemory::CSharedMemory()
{
	m_pMemory = NULL;
	m_hMapping = NULL;
	m_hFile = INVALID_HANDLE_VALUE;
}
/*****************************************************************************/
CSharedMemory::~CSharedMemory()
{
	Close();
}
/*****************************************************************************/
void CSharedMemory::Close()
{
	if (m_pMemory != NULL)
		UnmapViewOfFile(m_pMemory);
	if (m_hMapping != NULL)
		CloseHandle(m_hMapping);
	if (m_hFile != INVALID_HANDLE_VALUE)
		CloseHandle(m_hFile);
}
/*****************************************************************************/
void CSharedMemory::GenerateFileName(std::string &sFileName)
{
	DWORD dwPathLength = GetTempPath(0, NULL);
	LPTSTR pszTempPath = new char[dwPathLength];
	GetTempPath(dwPathLength, pszTempPath);
	char szTempFile[MAX_PATH];
	UINT uiUnique = GetTempFileName(
		pszTempPath,
		"EYE",
		0,
		szTempFile);
	sFileName = szTempFile;
	delete []pszTempPath;
}
/*****************************************************************************/
BYTE *CSharedMemory::Create(LPCTSTR szName, DWORD dwSize)
{
	string sFileName;
	GenerateFileName(sFileName);
	m_hFile = CreateFile(
		sFileName.c_str(),
		GENERIC_READ | GENERIC_WRITE,
		0,
		NULL,
		CREATE_ALWAYS,
		FILE_ATTRIBUTE_TEMPORARY | FILE_FLAG_DELETE_ON_CLOSE,
		NULL);
	if (m_hFile != INVALID_HANDLE_VALUE)
	{
		SetFilePointer(
			m_hFile,
			dwSize,
			NULL,
			FILE_BEGIN);
		SetEndOfFile(m_hFile);
		m_hMapping = CreateFileMapping(
			m_hFile,
			NULL,
			PAGE_READWRITE,
			0,
			0,
			szName);
		if (m_hMapping != NULL)
		{
			m_pMemory = MapViewOfFile(
				m_hMapping,
				FILE_MAP_ALL_ACCESS,
				0,
				0,
				0);
		}
	}
	return (BYTE *)m_pMemory;
}
/*****************************************************************************/
BYTE *CSharedMemory::Open(LPCTSTR szName)
{
	m_hMapping = OpenFileMapping(
		FILE_MAP_ALL_ACCESS,
		FALSE,
		szName);
	if (m_hMapping != NULL)
	{
		m_pMemory = MapViewOfFile(
			m_hMapping,
			FILE_MAP_ALL_ACCESS,
			0,
			0,
			0);
	}
	return (BYTE *)m_pMemory;
}
/*****************************************************************************/
