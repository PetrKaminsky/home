/*****************************************************************************/
#include "stdafx.h"
#include "Weyeshk.h"
#include "ShMem.h"
/*****************************************************************************/
#define WM_HK_MOUSEMOVE (WM_USER + 3)
/*****************************************************************************/
HHOOK g_hkMouseHook;
LPCTSTR g_szSharedMemoryName = "WEYES_SHARED_MEMORY_6E729120_07A3_11D8_A823_0060083EA000";
/*****************************************************************************/
DLL_API LRESULT CALLBACK MouseHookProc(
	int nCode,
	WPARAM wParam,
	LPARAM lParam)
{
	if (nCode >= 0)
	{
		LPMOUSEHOOKSTRUCT msh = (MOUSEHOOKSTRUCT *)lParam;
		switch (wParam)
		{
			case WM_MOUSEMOVE:
			case WM_NCMOUSEMOVE:
				{
					CSharedMemory sm;
					HWND *pWnd = (HWND *)sm.Open(g_szSharedMemoryName);
					if (pWnd)
						if (*pWnd)
							PostMessage(
								*pWnd,
								WM_HK_MOUSEMOVE,
								msh->pt.x,
								msh->pt.y);
				}
				break;

		}
	}	
	return CallNextHookEx(
		g_hkMouseHook,
		nCode,
		wParam,
		lParam);
}
/*****************************************************************************/
DLL_API void InstallMouseHook(void)
{
	HMODULE hModule = GetModuleHandle("Weyeshk.dll");
	g_hkMouseHook = SetWindowsHookEx(
		WH_MOUSE,
		MouseHookProc,
		hModule,
		0);
}
/*****************************************************************************/
DLL_API void UninstallMouseHook(void)
{
	UnhookWindowsHookEx(g_hkMouseHook);
}
/*****************************************************************************/
