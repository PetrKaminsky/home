/*****************************************************************************/
#if !defined(AFX_SHMEM_H__AB827535_1AA5_11D8_A839_0060083EA000__INCLUDED_)
#define AFX_SHMEM_H__AB827535_1AA5_11D8_A839_0060083EA000__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
/*****************************************************************************/
class CSharedMemory
{
public:
	CSharedMemory();
	~CSharedMemory();

	void Close();
	BYTE *Create(LPCTSTR szName, DWORD dwSize);
	BYTE *Open(LPCTSTR szName);

private:
	LPVOID m_pMemory;
	HANDLE m_hMapping;
	HANDLE m_hFile;

	void GenerateFileName(std::string &sFileName);
};
/*****************************************************************************/
#endif // !defined(AFX_SHMEM_H__AB827535_1AA5_11D8_A839_0060083EA000__INCLUDED_)
/*****************************************************************************/
