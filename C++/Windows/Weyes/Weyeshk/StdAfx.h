/*****************************************************************************/
#if !defined(AFX_STDAFX_H__7B9B67E7_0B92_11D8_A82A_0060083EA000__INCLUDED_)
#define AFX_STDAFX_H__7B9B67E7_0B92_11D8_A82A_0060083EA000__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
/*****************************************************************************/
#define WIN32_LEAN_AND_MEAN		// Exclude rarely-used stuff from Windows headers

#include <windows.h>
#include <string>
/*****************************************************************************/
//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__7B9B67E7_0B92_11D8_A82A_0060083EA000__INCLUDED_)
/*****************************************************************************/
