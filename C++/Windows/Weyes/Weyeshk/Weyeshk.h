/*****************************************************************************/
#ifdef WEYESHK_EXPORTS
#define DLL_API __declspec(dllexport)
#else
#define DLL_API __declspec(dllimport)
#endif
/*****************************************************************************/
DLL_API void InstallMouseHook(void);
DLL_API void UninstallMouseHook(void);
DLL_API LRESULT CALLBACK MouseHookProc(
	int nCode,
	WPARAM wParam,
	LPARAM lParam);
/*****************************************************************************/
