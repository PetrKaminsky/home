/*****************************************************************************/
#include "stdafx.h"
#include "WndSet.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif
/*****************************************************************************/
using namespace std;
/*****************************************************************************/
void CWndSet::AddWnd(CWnd *pWnd)
{
	m_Set.insert(pWnd);
}
/*****************************************************************************/
void CWndSet::RemoveWnd(CWnd *pWnd)
{
	m_Set.erase(pWnd);
}
/*****************************************************************************/
BOOL CWndSet::IsEmpty()
{
	return (BOOL)m_Set.empty();
}
/*****************************************************************************/
void CWndSet::PostMessage(UINT message, WPARAM wParam, LPARAM lParam)
{
	TWndSet::iterator it = m_Set.begin();
	while (it != m_Set.end())
	{
		(*it)->PostMessage(message, wParam, lParam);
		it++;
	}
}
/*****************************************************************************/
