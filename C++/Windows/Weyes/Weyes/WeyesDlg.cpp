/*****************************************************************************/
#include "stdafx.h"
#include "Weyes.h"
#include "WeyesDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif
/*****************************************************************************/
CWeyesDlg::CWeyesDlg(CWnd *pParent /*=NULL*/)
	: CDialog(CWeyesDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CWeyesDlg)
	//}}AFX_DATA_INIT
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_bTopMost = FALSE;
	m_bCaption = TRUE;
	m_bInitialized = FALSE;
	m_bTransparent = TRUE;
}
/*****************************************************************************/
void CWeyesDlg::DoDataExchange(CDataExchange *pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CWeyesDlg)
	//}}AFX_DATA_MAP
}
/*****************************************************************************/
BEGIN_MESSAGE_MAP(CWeyesDlg, CDialog)
	//{{AFX_MSG_MAP(CWeyesDlg)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_CLOSE()
	ON_WM_SIZE()
	ON_WM_MOVE()
	ON_WM_CONTEXTMENU()
	ON_WM_LBUTTONDOWN()
	ON_COMMAND(ID_TRANSPARENT, OnMenuTransparent)
	ON_COMMAND(ID_TOPMOST, OnMenuTopMost)
	ON_COMMAND(ID_CAPTION, OnMenuCaption)
	ON_COMMAND(ID_RESTORE, OnMenuRestore)
	ON_COMMAND(ID_MOVE, OnMenuMove)
	ON_COMMAND(ID_SIZE, OnMenuSize)
	ON_COMMAND(ID_MINIMIZE, OnMenuMinimize)
	ON_COMMAND(ID_MAXIMIZE, OnMenuMaximize)
	ON_COMMAND(ID_CLOSE, OnMenuClose)
	ON_WM_ERASEBKGND()
	ON_WM_SETCURSOR()
	//}}AFX_MSG_MAP
	ON_MESSAGE(WM_HK_MOUSEMOVE, OnHookMouseMove)
END_MESSAGE_MAP()
/*****************************************************************************/
BOOL CWeyesDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	m_LeftEye.Init();
	m_RightEye.Init();

	ReconfigureEyes(TRUE);
	SetRegion();

	m_bInitialized = TRUE;

	return TRUE;
}
/*****************************************************************************/
void CWeyesDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM)dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		if (GetUpdateRect(NULL))
		{
			CPaintDC dc(this);
			CDC dcMem;
			dcMem.CreateCompatibleDC(&dc);
			CRect r = dc.m_ps.rcPaint;
			CBitmap *pOldBmp = dcMem.SelectObject(&m_bmpOff);
			dc.BitBlt(r.left,
				r.top,
				r.right - r.left,
				r.bottom - r.top,
				&dcMem,
				r.left,
				r.top,
				SRCCOPY);
			dcMem.SelectObject(pOldBmp);
		}
	}
}
/*****************************************************************************/
HCURSOR CWeyesDlg::OnQueryDragIcon()
{
	return (HCURSOR)m_hIcon;
}
/*****************************************************************************/
void CWeyesDlg::PostNcDestroy() 
{
	CDialog::PostNcDestroy();
	delete this;
	((CWeyesApp *)AfxGetApp())->RemoveWnd(this);
}
/*****************************************************************************/
void CWeyesDlg::OnClose() 
{
	DestroyWindow();
}
/*****************************************************************************/
void CWeyesDlg::OnCancel() 
{
	PostMessage(WM_SYSCOMMAND, SC_MINIMIZE);
}
/*****************************************************************************/
void CWeyesDlg::OnOK() 
{
}
/*****************************************************************************/
LRESULT CWeyesDlg::OnHookMouseMove(WPARAM wParam, LPARAM lParam)
{
	CPoint pt(wParam, lParam);
	CRect r;
	CClientDC dc(this);
	CDC dcMem;
	dcMem.CreateCompatibleDC(&dc);
	CBitmap *pOldBmp = dcMem.SelectObject(&m_bmpOff);
	m_LeftEye.SetPointer(pt);
	m_LeftEye.DrawRect(dcMem, r);
	InvalidateRect(&r, FALSE);
	m_RightEye.SetPointer(pt);
	m_RightEye.DrawRect(dcMem, r);
	InvalidateRect(&r, FALSE);
	dcMem.SelectObject(pOldBmp);
	return 0;
}
/*****************************************************************************/
void CWeyesDlg::OnSize(UINT nType, int cx, int cy)
{
	CDialog::OnSize(nType, cx, cy);
	if (m_bInitialized)
	{
		ReconfigureEyes(TRUE);
		SetRegion();
		Invalidate();
	}
}
/*****************************************************************************/
void CWeyesDlg::OnMove(int x, int y)
{
	CDialog::OnMove(x, y);
	if (m_bInitialized)
	{
		ReconfigureEyes(FALSE);
		Invalidate(FALSE);
	}
}
/*****************************************************************************/
void CWeyesDlg::SetRegion()
{
	if (!m_bTransparent)
	{
		SetWindowRgn(NULL, TRUE);
		return;
	}
	CRect r, r1;
	GetClientRect(&r);
	GetWindowRect(&r1);
	int nBorderWidth = (r1.Width() - r.Width()) / 2;
	int nLeft = r.left + nBorderWidth;
	int nTop = r1.Height() - r.Height() - nBorderWidth;
	CRgn rgn;
	if (m_bCaption)
		rgn.CreateRectRgn(0, 0, r1.Width(), nTop);
	CRect r2(nLeft, nTop, nLeft + r.Width() / 2, nTop + r.Height());
	Resize(r2, 0.97);
	CRgn rgn1;
	rgn1.CreateEllipticRgn(r2.left, r2.top, r2.right, r2.bottom);
	CRgn rgn2;
	if (m_bCaption)
	{
		rgn2.CreateRectRgn(0, 0, 0, 0);
		rgn2.CombineRgn(&rgn, &rgn1, RGN_OR);
	}
	else
		rgn2.CreateEllipticRgn(r2.left, r2.top, r2.right, r2.bottom);
	CRect r3(nLeft + r.Width() / 2, nTop, nLeft + r.Width(), nTop + r.Height());
	Resize(r3, 0.97);
	CRgn rgn3;
	rgn3.CreateEllipticRgn(r3.left, r3.top, r3.right, r3.bottom);
	CRgn rgn4;
	rgn4.CreateRectRgn(0, 0, 0, 0);
	rgn4.CombineRgn(&rgn2, &rgn3, RGN_OR);
	SetWindowRgn(rgn4, TRUE);
}
/*****************************************************************************/
void CWeyesDlg::OnContextMenu(CWnd *pWnd, CPoint point)
{
	CRect r;
	if (point.x == -1 && point.y == -1)
	{
		GetWindowRect(&r);
		point.x = r.left;
		point.y = r.top;
	}
	else
	{
		GetClientRect(&r);
		ClientToScreen(&r);
		if (!r.PtInRect(point))
		{
			CDialog::OnContextMenu(pWnd, point);
			return;
		}
	}
	CMenu Menu;
	Menu.LoadMenu(IDR_MENU);
	CMenu* pCtxMenu = Menu.GetSubMenu(0);
	if (m_bTransparent)
		pCtxMenu->CheckMenuItem(ID_TRANSPARENT, MF_CHECKED | MF_BYCOMMAND);
	if (m_bTopMost)
		pCtxMenu->CheckMenuItem(ID_TOPMOST, MF_CHECKED | MF_BYCOMMAND);
	if (m_bCaption)
		pCtxMenu->CheckMenuItem(ID_CAPTION, MF_CHECKED | MF_BYCOMMAND);
	WINDOWPLACEMENT wp;
	GetWindowPlacement(&wp);
	if (wp.showCmd == SW_SHOWMAXIMIZED)
	{
		pCtxMenu->EnableMenuItem(ID_MOVE, MF_GRAYED | MF_BYCOMMAND);
		pCtxMenu->EnableMenuItem(ID_SIZE, MF_GRAYED | MF_BYCOMMAND);
		pCtxMenu->EnableMenuItem(ID_MAXIMIZE, MF_GRAYED | MF_BYCOMMAND);
	}
	else if (wp.showCmd == SW_SHOWNORMAL)
		pCtxMenu->EnableMenuItem(ID_RESTORE, MF_GRAYED | MF_BYCOMMAND);
	pCtxMenu->TrackPopupMenu(
		TPM_LEFTALIGN | TPM_LEFTBUTTON,
		point.x,
		point.y,
		this);
}
/*****************************************************************************/
void CWeyesDlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	if (m_bCaption)
		CDialog::OnLButtonDown(nFlags, point);
	else
		PostMessage(WM_NCLBUTTONDOWN, HTCAPTION, MAKELONG(point.x, point.y));
}
/*****************************************************************************/
void CWeyesDlg::OnMenuTransparent()
{
	m_bTransparent = !m_bTransparent;
	SetRegion();
}
/*****************************************************************************/
void CWeyesDlg::OnMenuTopMost()
{
	m_bTopMost = !m_bTopMost;
	SetWindowPos(
		m_bTopMost ? &wndTopMost : &wndNoTopMost,
		0,
		0,
		0,
		0,
		SWP_NOMOVE | SWP_NOSIZE);
}
/*****************************************************************************/
void CWeyesDlg::OnMenuCaption()
{
	m_bCaption = !m_bCaption;
	if (m_bCaption)
		ModifyStyle(0, WS_CAPTION, SWP_FRAMECHANGED);
	else
		ModifyStyle(WS_CAPTION, 0, SWP_FRAMECHANGED);
}
/*****************************************************************************/
void CWeyesDlg::OnMenuRestore()
{
	PostMessage(WM_SYSCOMMAND, SC_RESTORE);
}
/*****************************************************************************/
void CWeyesDlg::OnMenuMove()
{
	PostMessage(WM_SYSCOMMAND, SC_MOVE);
}
/*****************************************************************************/
void CWeyesDlg::OnMenuSize()
{
	PostMessage(WM_SYSCOMMAND, SC_SIZE);
}
/*****************************************************************************/
void CWeyesDlg::OnMenuMinimize()
{
	PostMessage(WM_SYSCOMMAND, SC_MINIMIZE);
}
/*****************************************************************************/
void CWeyesDlg::OnMenuMaximize()
{
	PostMessage(WM_SYSCOMMAND, SC_MAXIMIZE);
}
/*****************************************************************************/
void CWeyesDlg::OnMenuClose()
{
	DestroyWindow();
}
/*****************************************************************************/
BOOL CWeyesDlg::OnEraseBkgnd(CDC *pDC)
{
	return TRUE;
}
/*****************************************************************************/
void CWeyesDlg::ReconfigureEyes(BOOL bBitmap)
{
	CClientDC dc(this);
	CRect r;
	GetClientRect(&r);
	BOOL bErase = bBitmap;
	if (bBitmap)
	{
		if (m_bmpOff.GetSafeHandle())
			m_bmpOff.DeleteObject();
		m_bmpOff.CreateCompatibleBitmap(
			&dc, 
			r.Width(), 
			r.Height());
	}
	CDC dcMem;
	dcMem.CreateCompatibleDC(&dc);
	CBitmap *pOldBmp = dcMem.SelectObject(&m_bmpOff);
	if (bErase)
	{
		CBrush br;
		br.CreateSysColorBrush(COLOR_BTNFACE);
		dcMem.FillRect(&r, &br);
	}
	CRect rLeft(0, 0, r.Width() / 2, r.Height());
	CPoint ptLeft = rLeft.CenterPoint();
	ClientToScreen(&ptLeft);
	m_LeftEye.SetCoords(rLeft, ptLeft);
	m_LeftEye.DrawFull(dcMem, bErase);
	CRect rRight(r.Width() / 2, 0, r.Width(), r.Height());
	CPoint ptRight = rRight.CenterPoint();
	ClientToScreen(&ptRight);
	m_RightEye.SetCoords(rRight, ptRight);
	m_RightEye.DrawFull(dcMem, bErase);
	dcMem.SelectObject(pOldBmp);
}
/*****************************************************************************/
BOOL CWeyesDlg::OnSetCursor(CWnd *pWnd, UINT nHitTest, UINT message)
{
	switch (nHitTest)
	{
	case HTCAPTION:
	case HTCLIENT:
	case HTMAXBUTTON:
	case HTMINBUTTON:
	case HTSYSMENU:
	case HTCLOSE:
		SetCursor(
			LoadCursor(
				AfxGetInstanceHandle(),
				MAKEINTRESOURCE(IDC_CURSOR)));
		return TRUE;
	}
	return CDialog::OnSetCursor(pWnd, nHitTest, message);
}
/*****************************************************************************/
