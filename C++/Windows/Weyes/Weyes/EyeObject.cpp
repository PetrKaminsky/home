/*****************************************************************************/
#include "stdafx.h"
#include "EyeObject.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif
/*****************************************************************************/
#define BLACK_COLOR RGB(0, 0, 0)
#define WHITE_COLOR RGB(255, 255, 255)
/*****************************************************************************/
void Resize(CRect &r, double dKoef)
{
	CSize s(r.Size());
	if (dKoef < 1)
	{
		dKoef = 1 - dKoef;
		s.cx = (int)(dKoef / 2 * s.cx);
		s.cy = (int)(dKoef / 2 * s.cy);
		r.DeflateRect(s);
	}
	else if (dKoef > 1)
	{
		dKoef -= 1;
		s.cx = (int)(dKoef / 2 * s.cx);
		s.cy = (int)(dKoef / 2 * s.cy);
		r.InflateRect(s);
	}
}
/*****************************************************************************/
/*****************************************************************************/
/*****************************************************************************/
/*****************************************************************************/
CEyeObject::CEyeObject()
:	m_Rect(0, 0, 0, 0),
	m_Center(0, 0),
	m_Pointer(0, 0),
	m_LastRect(0, 0, 0, 0)
{
}
/*****************************************************************************/
void CEyeObject::SetCoords(CRect &r, CPoint &pt)
{
	m_Rect = r;
	m_Center = pt;
}
/*****************************************************************************/
void CEyeObject::SetPointer(CPoint &pt)
{
	m_Pointer = pt;
}
/*****************************************************************************/
void CEyeObject::CalcEllipseRect(CRect &r)
{
	int nA = (int)(0.4 * m_Rect.Width() / 2);
	int nB = (int)(0.4 * m_Rect.Height() / 2);
	int nX = m_Pointer.x - m_Center.x;
	int nY = m_Pointer.y - m_Center.y;
	double nQ = sqrt((double)nX * nX / nA / nA + (double)nY * nY / nB / nB);
	if (nQ > 1)
	{
		nX = (int)(nX / nQ);
		nY = (int)(nY / nQ);
	}
	CPoint pt(nX, nY);
	r = m_Rect;
	Resize(r, 0.2);
	r += pt;
}
/*****************************************************************************/
void CEyeObject::DrawFull(CDC &dc, BOOL bErase)
{
	CRect r;
	Draw(dc, TRUE, bErase, r);
}
/*****************************************************************************/
void CEyeObject::DrawRect(CDC &dc, CRect &r)
{
	Draw(dc, FALSE, FALSE, r);
}
/*****************************************************************************/
void CEyeObject::Init()
{
	m_brWhite.CreateSolidBrush(WHITE_COLOR);
	m_brBlack.CreateSolidBrush(BLACK_COLOR);
	m_pen.CreatePen(PS_NULL, 0, BLACK_COLOR);
}
/*****************************************************************************/
void CEyeObject::Draw(CDC &dc, BOOL bFull, BOOL bErase, CRect &r)
{
	CRect r1;
	CBrush *pOldBrush = dc.SelectObject(&m_brBlack);
	CPen *pOldPen = dc.SelectObject(&m_pen);
	if (bFull)
	{
		r1 = m_Rect;
		if (bErase)
		{
			CRect r3(r1);
			Resize(r3, 0.97);
			dc.Ellipse(&r3);
		}
		Resize(r1, 0.75);
	}
	else
		r1 = m_LastRect;
	dc.SelectObject(&m_brWhite);
	dc.Ellipse(&r1);
	dc.SelectObject(&m_brBlack);
	CRect r2;
	CalcEllipseRect(r2);
	dc.Ellipse(&r2);
	if (!bFull)
	{
		if (m_LastRect.IsRectEmpty())
			r = r2;
		else
			r.UnionRect(&r2, &m_LastRect);
	}
	m_LastRect = r2;
	dc.SelectObject(pOldBrush);
}
/*****************************************************************************/
