/*****************************************************************************/
#if !defined(AFX_EYEEVENT_H__CED67FD6_0D3E_11D8_A82C_0060083EA000__INCLUDED_)
#define AFX_EYEEVENT_H__CED67FD6_0D3E_11D8_A82C_0060083EA000__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
/*****************************************************************************/
class CEyeEvent  
{
public:
	CEyeEvent();
	~CEyeEvent();

	BOOL Create(LPCTSTR szName);
	BOOL Open(LPCTSTR szName);
	void Signal();
	void WaitFor();
	operator HANDLE();

private:
	HANDLE m_hEvent;
};
/*****************************************************************************/
#endif // !defined(AFX_EYEEVENT_H__CED67FD6_0D3E_11D8_A82C_0060083EA000__INCLUDED_)
/*****************************************************************************/
