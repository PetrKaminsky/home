/*****************************************************************************/
#if !defined(AFX_EYEOBJECT_H__11E3C803_09FF_11D8_A828_0060083EA000__INCLUDED_)
#define AFX_EYEOBJECT_H__11E3C803_09FF_11D8_A828_0060083EA000__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
/*****************************************************************************/
void Resize(CRect &r, double dKoef);
/*****************************************************************************/
class CEyeObject  
{
public:
	CEyeObject();

	void SetCoords(CRect &r, CPoint &pt);
	void SetPointer(CPoint &pt);
	void Draw(CDC &dc, BOOL bFull, BOOL bErase, CRect &r);
	void DrawFull(CDC &dc, BOOL bErase);
	void DrawRect(CDC &dc, CRect &r);
	void Init();

private:
	CRect m_Rect;
	CPoint m_Center;
	CPoint m_Pointer;
	CRect m_LastRect;
	CBrush m_brWhite;
	CBrush m_brBlack;
	CPen m_pen;

	void CalcEllipseRect(CRect &r);
};
/*****************************************************************************/
#endif // !defined(AFX_EYEOBJECT_H__11E3C803_09FF_11D8_A828_0060083EA000__INCLUDED_)
/*****************************************************************************/
