//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by Weyes.rc
//
#define IDD_WEYES_DIALOG                102
#define IDC_CURSOR                      104
#define IDR_MAINFRAME                   128
#define IDR_MENU                        129
#define ID_TRANSPARENT                  32771
#define ID_TOPMOST                      32772
#define ID_CAPTION                      32773
#define ID_RESTORE                      32774
#define ID_MINIMIZE                     32775
#define ID_MOVE                         32776
#define ID_SIZE                         32777
#define ID_MAXIMIZE                     32778
#define ID_CLOSE                        32779

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32780
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
