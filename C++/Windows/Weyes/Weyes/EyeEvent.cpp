/*****************************************************************************/
#include "stdafx.h"
#include "EyeEvent.h"
/*****************************************************************************/
CEyeEvent::CEyeEvent()
{
	m_hEvent = NULL;
}
/*****************************************************************************/
CEyeEvent::~CEyeEvent()
{
	if (m_hEvent != NULL)
		CloseHandle(m_hEvent);
}
/*****************************************************************************/
BOOL CEyeEvent::Create(LPCTSTR szName)
{
	m_hEvent = CreateEvent(
		NULL,
		FALSE,
		FALSE,
		szName);
	return (m_hEvent != NULL);
}
/*****************************************************************************/
BOOL CEyeEvent::Open(LPCTSTR szName)
{
	m_hEvent = OpenEvent(
		EVENT_ALL_ACCESS,
		FALSE,
		szName);
	return (m_hEvent != NULL);
}
/*****************************************************************************/
void CEyeEvent::Signal()
{
	SetEvent(m_hEvent);
}
/*****************************************************************************/
void CEyeEvent::WaitFor()
{
	WaitForSingleObject(m_hEvent, INFINITE);
}
/*****************************************************************************/
CEyeEvent::operator HANDLE()
{
	return m_hEvent;
}
/*****************************************************************************/
