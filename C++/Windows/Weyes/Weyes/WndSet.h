/*****************************************************************************/
#if !defined(AFX_WNDSET_H__2D6090A4_07DD_11D8_A824_0060083EA000__INCLUDED_)
#define AFX_WNDSET_H__2D6090A4_07DD_11D8_A824_0060083EA000__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#pragma warning(disable: 4786)
/*****************************************************************************/
#include <set>
/*****************************************************************************/
typedef std::set<CWnd *> TWndSet;
/*****************************************************************************/
class CWndSet  
{
public:
	void AddWnd(CWnd *pWnd);
	void RemoveWnd(CWnd *pWnd);
	BOOL IsEmpty();
	void PostMessage(UINT message, WPARAM wParam, LPARAM lParam);

private:
	TWndSet m_Set;
};
/*****************************************************************************/
#endif // !defined(AFX_WNDSET_H__2D6090A4_07DD_11D8_A824_0060083EA000__INCLUDED_)
/*****************************************************************************/
