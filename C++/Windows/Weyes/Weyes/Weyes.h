/*****************************************************************************/
#if !defined(AFX_WEYES_H__7B9B67F0_0B92_11D8_A82A_0060083EA000__INCLUDED_)
#define AFX_WEYES_H__7B9B67F0_0B92_11D8_A82A_0060083EA000__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif
/*****************************************************************************/
#include "resource.h"
#include "EyeEvent.h"
#include "WndSet.h"
#include "ShMem.h"
/*****************************************************************************/
#define WM_HK_MOUSEMOVE (WM_USER + 3)
/*****************************************************************************/
class CWeyesApp : public CWinApp
{
public:
	//{{AFX_VIRTUAL(CWeyesApp)
	public:
	virtual BOOL InitInstance();
	virtual int Run();
	virtual int ExitInstance();
	//}}AFX_VIRTUAL

	void NewWnd();
	void RemoveWnd(CWnd *pWnd);
	void OnEvent();

private:
	CEyeEvent m_Event;
	CWndSet m_WndSet;
	CSharedMemory m_SharedMemory;

	//{{AFX_MSG(CWeyesApp)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
/*****************************************************************************/
//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_WEYES_H__7B9B67F0_0B92_11D8_A82A_0060083EA000__INCLUDED_)
/*****************************************************************************/
