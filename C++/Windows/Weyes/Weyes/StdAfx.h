/*****************************************************************************/
#if !defined(AFX_STDAFX_H__7B9B67F4_0B92_11D8_A82A_0060083EA000__INCLUDED_)
#define AFX_STDAFX_H__7B9B67F4_0B92_11D8_A82A_0060083EA000__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
/*****************************************************************************/
#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT
#include <math.h>
/*****************************************************************************/
//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__7B9B67F4_0B92_11D8_A82A_0060083EA000__INCLUDED_)
/*****************************************************************************/
