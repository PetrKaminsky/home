/*****************************************************************************/
#if !defined(AFX_WEYESDLG_H__7B9B67F2_0B92_11D8_A82A_0060083EA000__INCLUDED_)
#define AFX_WEYESDLG_H__7B9B67F2_0B92_11D8_A82A_0060083EA000__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
/*****************************************************************************/
#include "EyeObject.h"
/*****************************************************************************/
class CWeyesDlg : public CDialog
{
public:
	CWeyesDlg(CWnd* pParent = NULL);	// standard constructor

	//{{AFX_DATA(CWeyesDlg)
	enum { IDD = IDD_WEYES_DIALOG };
	//}}AFX_DATA

	//{{AFX_VIRTUAL(CWeyesDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	virtual void PostNcDestroy();
	//}}AFX_VIRTUAL

protected:
	HICON m_hIcon;
	CEyeObject m_LeftEye;
	CEyeObject m_RightEye;
	BOOL m_bTransparent;
	BOOL m_bTopMost;
	BOOL m_bCaption;
	CBitmap m_bmpOff;
	BOOL m_bInitialized;

	void SetRegion();
	void ReconfigureEyes(BOOL bBitmap);

	//{{AFX_MSG(CWeyesDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnClose();
	virtual void OnCancel();
	virtual void OnOK();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnMove(int x, int y);
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnMenuTransparent();
	afx_msg void OnMenuTopMost();
	afx_msg void OnMenuCaption();
	afx_msg void OnMenuRestore();
	afx_msg void OnMenuMove();
	afx_msg void OnMenuSize();
	afx_msg void OnMenuMinimize();
	afx_msg void OnMenuMaximize();
	afx_msg void OnMenuClose();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
	//}}AFX_MSG
	afx_msg LRESULT OnHookMouseMove(WPARAM, LPARAM);
	DECLARE_MESSAGE_MAP()
};
/*****************************************************************************/
//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_WEYESDLG_H__7B9B67F2_0B92_11D8_A82A_0060083EA000__INCLUDED_)
/*****************************************************************************/
