/*****************************************************************************/
#if !defined(AFX_MYWND_H__91228575_0854_11D8_A825_0060083EA000__INCLUDED_)
#define AFX_MYWND_H__91228575_0854_11D8_A825_0060083EA000__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
/*****************************************************************************/
#include "WndSet.h"
/*****************************************************************************/
class CEyeWnd : public CWnd
{
public:
	CEyeWnd(CWndSet &WndSet);

	//{{AFX_VIRTUAL(CEyeWnd)
	protected:
	virtual void PostNcDestroy();
	//}}AFX_VIRTUAL

public:
	virtual ~CEyeWnd();

private:
	CWndSet &m_WndSet;

protected:
	//{{AFX_MSG(CEyeWnd)
	//}}AFX_MSG
	afx_msg LRESULT OnHookMouseMove(WPARAM, LPARAM);
	DECLARE_MESSAGE_MAP()
};
/*****************************************************************************/
//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MYWND_H__91228575_0854_11D8_A825_0060083EA000__INCLUDED_)
/*****************************************************************************/
