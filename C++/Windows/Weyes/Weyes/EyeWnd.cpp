/*****************************************************************************/
#include "stdafx.h"
#include "Weyes.h"
#include "EyeWnd.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif
/*****************************************************************************/
CEyeWnd::CEyeWnd(CWndSet &WndSet)
:	m_WndSet(WndSet)
{
}
/*****************************************************************************/
CEyeWnd::~CEyeWnd()
{
}
/*****************************************************************************/
BEGIN_MESSAGE_MAP(CEyeWnd, CWnd)
	//{{AFX_MSG_MAP(CEyeWnd)
	//}}AFX_MSG_MAP
	ON_MESSAGE(WM_HK_MOUSEMOVE, OnHookMouseMove)
END_MESSAGE_MAP()
/*****************************************************************************/
void CEyeWnd::PostNcDestroy() 
{
	CWnd::PostNcDestroy();
	delete this;
}
/*****************************************************************************/
LRESULT CEyeWnd::OnHookMouseMove(WPARAM wParam, LPARAM lParam)
{
	m_WndSet.PostMessage(
		WM_HK_MOUSEMOVE,
		wParam,
		lParam);
	return 0;
}
/*****************************************************************************/
