/*****************************************************************************/
#include "stdafx.h"
#include "Weyes.h"
#include "WeyesDlg.h"
#include "EyeWnd.h"
#include "..\Weyeshk\Weyeshk.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif
/*****************************************************************************/
LPCTSTR g_szEventName = "WEYES_EVENT_6E729120_07A3_11D8_A823_0060083EA000";
LPCTSTR g_szSharedMemoryName = "WEYES_SHARED_MEMORY_6E729120_07A3_11D8_A823_0060083EA000";
/*****************************************************************************/
BEGIN_MESSAGE_MAP(CWeyesApp, CWinApp)
	//{{AFX_MSG_MAP(CWeyesApp)
	//}}AFX_MSG
END_MESSAGE_MAP()
/*****************************************************************************/
CWeyesApp theApp;
/*****************************************************************************/
BOOL CWeyesApp::InitInstance()
{
	if (m_Event.Open(g_szEventName))
	{
		m_Event.Signal();
		return FALSE;
	}

	m_Event.Create(g_szEventName);

#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif

	m_pMainWnd = new CEyeWnd(m_WndSet);
	m_pMainWnd->CreateEx(
		0,
		AfxRegisterWndClass(NULL),
		"Weyes",
		WS_POPUP,
		CRect(0, 0, 1, 1),
		NULL,
		0);

	HWND *pWnd = (HWND *)m_SharedMemory.Create(g_szSharedMemoryName, sizeof(HWND));
	*pWnd = m_pMainWnd->GetSafeHwnd();

	InstallMouseHook();

	NewWnd();

	return TRUE;
}
/*****************************************************************************/
int CWeyesApp::Run()
{
	HANDLE hEvent = (HANDLE)m_Event;
	while (TRUE)
	{
		while (PeekMessage(&m_msgCur, NULL, 0, 0, PM_REMOVE))
        {
			if (m_msgCur.message == WM_QUIT)
				return ExitInstance();

			if (!PreTranslateMessage(&m_msgCur))
			{
				TranslateMessage(&m_msgCur); 
				DispatchMessage(&m_msgCur); 
			}
		}

		if (MsgWaitForMultipleObjects(
				1,
				&hEvent,
				FALSE,
				INFINITE,
				QS_ALLINPUT) == WAIT_OBJECT_0)
			OnEvent();
	}
}
/*****************************************************************************/
int CWeyesApp::ExitInstance()
{
	UninstallMouseHook();

	return CWinApp::ExitInstance();
}
/*****************************************************************************/
void CWeyesApp::OnEvent()
{
	NewWnd();
}
/*****************************************************************************/
void CWeyesApp::NewWnd()
{
	CWeyesDlg *pDlg = new CWeyesDlg;
	pDlg->Create(CWeyesDlg::IDD, m_pMainWnd);
	m_WndSet.AddWnd(pDlg);
}
/*****************************************************************************/
void CWeyesApp::RemoveWnd(CWnd *pWnd)
{
	m_WndSet.RemoveWnd(pWnd);
	if (m_WndSet.IsEmpty())
		m_pMainWnd->DestroyWindow();
}
/*****************************************************************************/
