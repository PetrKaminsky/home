// TilePuzzle.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "TilePuzzle.h"
#include "PuzzleWnd.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTilePuzzleApp

BEGIN_MESSAGE_MAP(CTilePuzzleApp, CWinApp)
	//{{AFX_MSG_MAP(CTilePuzzleApp)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTilePuzzleApp construction

CTilePuzzleApp::CTilePuzzleApp()
{
	m_pWnd = NULL;
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CTilePuzzleApp object

CTilePuzzleApp g_App;

/////////////////////////////////////////////////////////////////////////////
// CTilePuzzleApp initialization

BOOL CTilePuzzleApp::InitInstance()
{
#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif

	// Change the registry key under which our settings are stored.
	SetRegistryKey(_T("TilePuzzle"));

	m_pWnd = new CPuzzleWnd;
	m_pMainWnd = m_pWnd;
	m_pWnd->Create();
	m_pWnd->CenterWindow();
	m_pWnd->ShowWindow(SW_SHOW);

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CTilePuzzleApp message handlers

int CTilePuzzleApp::ExitInstance()
{
	if (m_pWnd != NULL)
	{
		delete m_pWnd;
		m_pWnd = NULL;
	}

	return CWinApp::ExitInstance();
}
