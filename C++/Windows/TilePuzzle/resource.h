//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by TilePuzzle.rc
//
#define IDD_ABOUTBOX                    100
#define IDR_MAINFRAME                   128
#define IDR_TILEPUTYPE                  129
#define ID_KEY_UP                       32771
#define ID_KEY_DOWN                     32772
#define ID_KEY_LEFT                     32773
#define ID_KEY_RIGHT                    32774
#define ID_DESK_3_3                     32775
#define ID_DESK_4_4                     32776
#define ID_DESK_5_5                     32777
#define ID_DESK_6_6                     32778
#define ID_DESK_7_7                     32779
#define ID_SHUFFLE                      32780
#define ID_CANCEL                       32781
#define ID_DESK_8_8                     32782
#define ID_AUTO_MODE                    32783
#define ID_DESK_9_9                     32784

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_3D_CONTROLS                     1
#define _APS_NEXT_RESOURCE_VALUE        135
#define _APS_NEXT_COMMAND_VALUE         32785
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
