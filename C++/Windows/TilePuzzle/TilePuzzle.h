// TilePuzzle.h : main header file for the TILEPUZZLE application
//

#if !defined(AFX_TILEPUZZLE_H__96A9DF85_7177_4914_AD12_C00C4E120C64__INCLUDED_)
#define AFX_TILEPUZZLE_H__96A9DF85_7177_4914_AD12_C00C4E120C64__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols
#include "PuzzleWnd.h"

template<class T>
void Swap(T &x, T &y)
{
	T tmp = x;
	x = y;
	y = tmp;
}

/////////////////////////////////////////////////////////////////////////////

class CTilePuzzleApp : public CWinApp
{
public:
	CTilePuzzleApp();

	//{{AFX_VIRTUAL(CTilePuzzleApp)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	//}}AFX_VIRTUAL

// Implementation

public:
	//{{AFX_MSG(CTilePuzzleApp)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	CPuzzleWnd *m_pWnd;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TILEPUZZLE_H__96A9DF85_7177_4914_AD12_C00C4E120C64__INCLUDED_)
