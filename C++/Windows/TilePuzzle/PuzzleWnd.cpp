// PuzzleWnd.cpp : implementation file
//

#include "stdafx.h"
#include "TilePuzzle.h"
#include "PuzzleWnd.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPuzzleWnd

CPuzzleWnd::CPuzzleWnd()
{
	m_hAccelTable = NULL;
	m_nSize = 300;
	m_nBorder = 5;
	m_nDistance = 2;
	m_bSolving = false;
	m_bSmoothTimer = false;
	m_bAutoMode = false;
	m_clrTileBgNotSolved = RGB(255, 96, 0);
	m_clrTileBgSolved = RGB(255, 192, 0);
	m_clrTileTxNotSolved = RGB(255, 255, 255);
	m_clrTileTxSolved = RGB(0, 0, 0);
	m_clrDeskBg = RGB(0, 128, 192);
}

BEGIN_MESSAGE_MAP(CPuzzleWnd, CWnd)
	//{{AFX_MSG_MAP(CPuzzleWnd)
	ON_WM_PAINT()
	ON_WM_CREATE()
	ON_COMMAND(ID_KEY_UP, OnKeyUp)
	ON_COMMAND(ID_KEY_DOWN, OnKeyDown)
	ON_COMMAND(ID_KEY_LEFT, OnKeyLeft)
	ON_COMMAND(ID_KEY_RIGHT, OnKeyRight)
	ON_COMMAND(ID_SHUFFLE, OnShuffle)
	ON_COMMAND(ID_DESK_3_3, OnDesk3x3)
	ON_COMMAND(ID_DESK_4_4, OnDesk4x4)
	ON_COMMAND(ID_DESK_5_5, OnDesk5x5)
	ON_COMMAND(ID_DESK_6_6, OnDesk6x6)
	ON_COMMAND(ID_DESK_7_7, OnDesk7x7)
	ON_COMMAND(ID_DESK_8_8, OnDesk8x8)
	ON_WM_CONTEXTMENU()
	ON_COMMAND(ID_APP_EXIT, OnAppExit)
	ON_WM_ERASEBKGND()
	ON_WM_TIMER()
	ON_COMMAND(ID_AUTO_MODE, OnAutoMode)
	ON_WM_LBUTTONDOWN()
	ON_COMMAND(ID_DESK_9_9, OnDesk9x9)
	//}}AFX_MSG_MAP
	ON_MESSAGE(WM_TEST_SOLVED, OnTestSolved)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPuzzleWnd message handlers

void CPuzzleWnd::OnPaint() 
{
	CPaintDC dc(this);
	CDC dcMem;
	dcMem.CreateCompatibleDC(&dc);
	CRect r = dc.m_ps.rcPaint;
	CBitmap *pOldBmp = dcMem.SelectObject(&m_bmpOff);
	dc.BitBlt(r.left,
		r.top,
		r.right - r.left,
		r.bottom - r.top,
		&dcMem,
		r.left,
		r.top,
		SRCCOPY);
	dcMem.SelectObject(pOldBmp);
}

int CPuzzleWnd::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (!LoadAccelTable())
		return -1;

	// init game
	m_Desk.Init(4);

	Redraw();

	return 0;
}

void CPuzzleWnd::DrawTile(
	CDC &dc,
	CRect &r,
	int nTile,
	bool bTileSolved)
{
	COLORREF clrBk(bTileSolved ? m_clrTileBgSolved : m_clrTileBgNotSolved);
	dc.FillSolidRect(r, clrBk);
	DrawRect(dc, r);
	dc.MoveTo(r.left, r.top);
	dc.LineTo(r.left, r.bottom);
	dc.LineTo(r.right, r.bottom);
	dc.LineTo(r.right, r.top);
	dc.LineTo(r.left, r.top);

	CString sTile;
	sTile.Format(_T("%d"), nTile);
	int nPointSize = 4 * (m_nSize - 2 * m_nBorder) / m_Desk.GetSize();
	CFont f;
	f.CreatePointFont(
		nPointSize,
		_T("Arial"),
		&dc);
	COLORREF clrTx(bTileSolved ? m_clrTileTxSolved : m_clrTileTxNotSolved);
	dc.SetTextColor(clrTx);
	CFont *pOldFont = dc.SelectObject(&f);
	dc.DrawText(
		sTile,
		r,
		DT_SINGLELINE | DT_VCENTER | DT_CENTER);
	dc.SelectObject(pOldFont);
}

void CPuzzleWnd::OnKeyUp()
{
	if (!m_bSolving)
		return;
	if (m_bAutoMode)
		return;
	ShiftUp();
}

void CPuzzleWnd::OnKeyDown()
{
	if (!m_bSolving)
		return;
	if (m_bAutoMode)
		return;
	ShiftDown();
}

void CPuzzleWnd::OnKeyLeft()
{
	if (!m_bSolving)
		return;
	if (m_bAutoMode)
		return;
	ShiftLeft();
}

void CPuzzleWnd::OnKeyRight()
{
	if (!m_bSolving)
		return;
	if (m_bAutoMode)
		return;
	ShiftRight();
}

BOOL CPuzzleWnd::LoadAccelTable()
{
	m_hAccelTable = ::LoadAccelerators(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDR_MAINFRAME));

	return m_hAccelTable != NULL;
}

BOOL CPuzzleWnd::PreTranslateMessage(MSG *pMsg)
{
	if (pMsg->message >= WM_KEYFIRST && pMsg->message <= WM_KEYLAST)
		return m_hAccelTable != NULL && ::TranslateAccelerator(m_hWnd, m_hAccelTable, pMsg);

	return CWnd::PreTranslateMessage(pMsg);
}

void CPuzzleWnd::OnShuffle()
{
	if (!GiveUp())
		return;
	m_Desk.Shuffle();
	Redraw();
	m_bSolving = true;
}

void CPuzzleWnd::OnDesk3x3()
{
	OnDesk(3);
}

void CPuzzleWnd::OnDesk4x4()
{
	OnDesk(4);
}

void CPuzzleWnd::OnDesk5x5()
{
	OnDesk(5);
}

void CPuzzleWnd::OnDesk6x6()
{
	OnDesk(6);
}

void CPuzzleWnd::OnDesk7x7()
{
	OnDesk(7);
}

void CPuzzleWnd::OnDesk8x8()
{
	OnDesk(8);
}

void CPuzzleWnd::OnDesk9x9()
{
	OnDesk(9);
}

void CPuzzleWnd::OnContextMenu(CWnd *pWnd, CPoint point)
{
	CMenu menu;
	if (menu.LoadMenu(IDR_MAINFRAME))
	{
		CMenu *pPopup = menu.GetSubMenu(0);
		InitMenu(pPopup);
		pPopup->TrackPopupMenu(
			TPM_LEFTALIGN | TPM_RIGHTBUTTON,
			point.x,
			point.y,
			this);
	}
}

void CPuzzleWnd::OnAppExit()
{
	if (!GiveUp())
		return;
	DestroyWindow();
}

void CPuzzleWnd::InitMenu(CMenu *pMenu)
{
	switch (m_Desk.GetSize())
	{
		case 3:
			pMenu->CheckMenuItem(ID_DESK_3_3, MF_BYCOMMAND | MF_CHECKED);
			break;

		case 4:
			pMenu->CheckMenuItem(ID_DESK_4_4, MF_BYCOMMAND | MF_CHECKED);
			break;

		case 5:
			pMenu->CheckMenuItem(ID_DESK_5_5, MF_BYCOMMAND | MF_CHECKED);
			break;

		case 6:
			pMenu->CheckMenuItem(ID_DESK_6_6, MF_BYCOMMAND | MF_CHECKED);
			break;

		case 7:
			pMenu->CheckMenuItem(ID_DESK_7_7, MF_BYCOMMAND | MF_CHECKED);
			break;

		case 8:
			pMenu->CheckMenuItem(ID_DESK_8_8, MF_BYCOMMAND | MF_CHECKED);
			break;

		case 9:
			pMenu->CheckMenuItem(ID_DESK_9_9, MF_BYCOMMAND | MF_CHECKED);
			break;
	}
	if (m_bAutoMode)
	{
		pMenu->CheckMenuItem(ID_AUTO_MODE, MF_BYCOMMAND | MF_CHECKED);
		pMenu->EnableMenuItem(ID_KEY_UP, MF_BYCOMMAND | MF_GRAYED);
		pMenu->EnableMenuItem(ID_KEY_DOWN, MF_BYCOMMAND | MF_GRAYED);
		pMenu->EnableMenuItem(ID_KEY_LEFT, MF_BYCOMMAND | MF_GRAYED);
		pMenu->EnableMenuItem(ID_KEY_RIGHT, MF_BYCOMMAND | MF_GRAYED);
		pMenu->EnableMenuItem(ID_DESK_3_3, MF_BYCOMMAND | MF_GRAYED);
		pMenu->EnableMenuItem(ID_DESK_4_4, MF_BYCOMMAND | MF_GRAYED);
		pMenu->EnableMenuItem(ID_DESK_5_5, MF_BYCOMMAND | MF_GRAYED);
		pMenu->EnableMenuItem(ID_DESK_6_6, MF_BYCOMMAND | MF_GRAYED);
		pMenu->EnableMenuItem(ID_DESK_7_7, MF_BYCOMMAND | MF_GRAYED);
		pMenu->EnableMenuItem(ID_DESK_8_8, MF_BYCOMMAND | MF_GRAYED);
		pMenu->EnableMenuItem(ID_DESK_9_9, MF_BYCOMMAND | MF_GRAYED);
		pMenu->EnableMenuItem(ID_SHUFFLE, MF_BYCOMMAND | MF_GRAYED);
	}
	if (!m_bSolving)
	{
		pMenu->EnableMenuItem(ID_AUTO_MODE, MF_BYCOMMAND | MF_GRAYED);
		pMenu->EnableMenuItem(ID_KEY_UP, MF_BYCOMMAND | MF_GRAYED);
		pMenu->EnableMenuItem(ID_KEY_DOWN, MF_BYCOMMAND | MF_GRAYED);
		pMenu->EnableMenuItem(ID_KEY_LEFT, MF_BYCOMMAND | MF_GRAYED);
		pMenu->EnableMenuItem(ID_KEY_RIGHT, MF_BYCOMMAND | MF_GRAYED);
	}
}

int CPuzzleWnd::Create()
{
	return CreateEx(
		0,
		AfxRegisterWndClass(
			CS_HREDRAW | CS_VREDRAW,
			::LoadCursor(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDR_MAINFRAME))),
		_T("TilePuzzle"),
		WS_POPUP,
		CRect(0, 0, m_nSize, m_nSize),
		NULL,
		0);
}

void CPuzzleWnd::RedrawOff()
{
	CClientDC dc(this);
	CRect r;
	GetClientRect(&r);
	if (m_bmpOff.GetSafeHandle() == NULL)
		m_bmpOff.CreateCompatibleBitmap(
			&dc, 
			r.Width(), 
			r.Height());
	CDC dcMem;
	dcMem.CreateCompatibleDC(&dc);
	CBitmap *pOldBmp = dcMem.SelectObject(&m_bmpOff);
	CBrush br;
	br.CreateSolidBrush(m_clrDeskBg);
	dcMem.FillRect(&r, &br);
	r.DeflateRect(0, 0, 1, 1);
	DrawRect(dcMem, r);

	int nX1 = m_nBorder;
	int nY1 = m_nBorder;
	int nX2 = m_nSize - m_nBorder;
	int nY2 = m_nSize - m_nBorder;
	int nWidth = nX2 - nX1;
	int nHeight = nY2 - nY1;
	int nSub = m_nDistance;

	bool bTileSolved;
	int i, j, nTile;
	int nSize = m_Desk.GetSize();
	int nTileWidth = nWidth / nSize;
	int nTileHeight = nHeight / nSize;
	m_ClickItemMap.Reset();
	for (j = 0; j < nSize; j++)
		for (i = 0; i < nSize; i++)
		{
			CRect r(
				CPoint(nX1 + (i * nTileWidth) + nSub,nY1 + (j * nTileHeight) + nSub), 
				CSize(nTileWidth - 2 * nSub, nTileHeight - 2 * nSub));
			m_ClickItemMap.AddItem(CClickItem(r, i, j));
			m_Desk.GetTile(i, j, nTile, bTileSolved);
			if (nTile != CDesk::s_nEmpty)
			{
				DrawTile(
					dcMem,
					r,
					nTile,
					bTileSolved);
			}
		}

	dcMem.SelectObject(pOldBmp);
}

BOOL CPuzzleWnd::OnEraseBkgnd(CDC *pDC)
{
	return TRUE;
}

void CPuzzleWnd::PostTestSolved()
{
	PostMessage(WM_TEST_SOLVED);
}

LRESULT CPuzzleWnd::OnTestSolved(WPARAM wParam, LPARAM lParam)
{
	if (m_Desk.IsSolved())
	{
		m_bSolving = false;
		if (!m_bAutoMode)
			if (AfxMessageBox(_T("Congratulations! New game?"), MB_YESNO | MB_ICONINFORMATION | MB_DEFBUTTON1) == IDYES)
				OnShuffle();
	}
	return 0;
}

bool CPuzzleWnd::GiveUp()
{
	if (m_bSolving)
		if (AfxMessageBox(_T("Give up?"), MB_YESNO | MB_ICONINFORMATION | MB_DEFBUTTON2) == IDNO)
			return false;
	return true;
}

void CPuzzleWnd::OnTimer(UINT nIDEvent)
{
	if (nIDEvent == SMOOTH_TIMER)
		OnSmoothMoveTimer();
	else if (nIDEvent == AUTO_TIMER)
		OnAutoTimer();

	CWnd::OnTimer(nIDEvent);
}

void CPuzzleWnd::OnSmoothMoveTimer()
{
	CSmoothMove SmoothMove;
	if (GetSmoothMove(SmoothMove))
	{
		CClientDC dc(this);
		CDC dcMem;
		dcMem.CreateCompatibleDC(&dc);
		CBitmap *pOldBmp = dcMem.SelectObject(&m_bmpOff);
		CBrush br;
		br.CreateSolidBrush(m_clrDeskBg);
		dcMem.FillRect(SmoothMove.GetRedrawRect(), &br);
		DrawTile(
			dcMem,
			SmoothMove.GetTileRect(),
			SmoothMove.GetTile(),
			SmoothMove.GetTileSolved());

		dcMem.SelectObject(pOldBmp);
		InvalidateRect(SmoothMove.GetRedrawRect());
	}
	else
	{
		KillTimer(SMOOTH_TIMER);
		m_bSmoothTimer = false;
	}
}

void CPuzzleWnd::GenerSmoothMoves(const CMove &Move)
{
	CRect rFrom, rTo, rRedraw;
	CalcRects(
		Move,
		rFrom,
		rTo,
		rRedraw);
	int nPos, nStep;
	int nTile = Move.GetTile();
	bool bTileSolved = Move.IsTileSolved();
	int nShiftBy = 4;
	if (Move.GetFromX() == Move.GetToX())
	{
		if (Move.GetFromY() < Move.GetToY())
		{
			// down moves
			nPos = rFrom.top;
			nStep = (rTo.top - rFrom.top) >> nShiftBy;
			do
			{
				nPos += nStep;
				if (nPos > rTo.top)
					nPos = rTo.top;
				CRect rTile(
					CPoint(rFrom.left, nPos),
					CSize(rFrom.Width(), rFrom.Height()));
				m_SmoothMoveQueue.AddTail(CSmoothMove(
					rTile,
					rRedraw,
					nTile,
					bTileSolved));
			} while (nPos < rTo.top);
		}
		else
		{
			// up moves
			nPos = rFrom.top;
			nStep = (rFrom.top - rTo.top) >> nShiftBy;
			do
			{
				nPos -= nStep;
				if (nPos < rTo.top)
					nPos = rTo.top;
				CRect rTile(
					CPoint(rFrom.left, nPos),
					CSize(rFrom.Width(), rFrom.Height()));
				m_SmoothMoveQueue.AddTail(CSmoothMove(
					rTile,
					rRedraw,
					nTile,
					bTileSolved));
			} while (nPos > rTo.top);
		}
	}
	else
	{
		if (Move.GetFromX() < Move.GetToX())
		{
			// right moves
			nPos = rFrom.left;
			nStep = (rTo.left - rFrom.left) >> nShiftBy;
			do
			{
				nPos += nStep;
				if (nPos > rTo.left)
					nPos = rTo.left;
				CRect rTile(
					CPoint(nPos, rFrom.top),
					CSize(rFrom.Width(), rFrom.Height()));
				m_SmoothMoveQueue.AddTail(CSmoothMove(
					rTile,
					rRedraw,
					nTile,
					bTileSolved));
			} while (nPos < rTo.left);
		}
		else
		{
			// left moves
			nPos = rFrom.left;
			nStep = (rFrom.left - rTo.left) >> nShiftBy;
			do
			{
				nPos -= nStep;
				if (nPos < rTo.left)
					nPos = rTo.left;
				CRect rTile(
					CPoint(nPos, rFrom.top),
					CSize(rFrom.Width(), rFrom.Height()));
				m_SmoothMoveQueue.AddTail(CSmoothMove(
					rTile,
					rRedraw,
					nTile,
					bTileSolved));
			} while (nPos > rTo.left);
		}
	}

	if (!m_bSmoothTimer)
	{
		SetTimer(SMOOTH_TIMER, SMOOTH_TIMER_PERIOD, NULL);
		m_bSmoothTimer = true;
	}
}

void CPuzzleWnd::CalcRects(
	const CMove &Move,
	CRect &rFrom,
	CRect &rTo,
	CRect &rRedraw)
{
	int nX1 = m_nBorder;
	int nY1 = m_nBorder;
	int nX2 = m_nSize - m_nBorder;
	int nY2 = m_nSize - m_nBorder;
	int nWidth = nX2 - nX1;
	int nHeight = nY2 - nY1;
	int nSub = m_nDistance;
	int nSize = m_Desk.GetSize();
	int nTileWidth = nWidth / nSize;
	int nTileHeight = nHeight / nSize;

	rFrom = CRect(
		CPoint(nX1 + (Move.GetFromX() * nTileWidth) + nSub, nY1 + (Move.GetFromY() * nTileHeight) + nSub),
		CSize(nTileWidth - 2 * nSub, nTileHeight - 2 * nSub));
	rTo = CRect(
		CPoint(nX1 + (Move.GetToX() * nTileWidth) + nSub, nY1 + (Move.GetToY() * nTileHeight) + nSub),
		CSize(nTileWidth - 2 * nSub, nTileHeight - 2 * nSub));
	rRedraw.UnionRect(rFrom, rTo);
	rRedraw.InflateRect(0, 0, 1, 1);
}

bool CPuzzleWnd::GetSmoothMove(CSmoothMove &SmoothMove)
{
	if (m_SmoothMoveQueue.IsEmpty())
		return false;

	SmoothMove = m_SmoothMoveQueue.RemoveHead();

	return true;
}

void CPuzzleWnd::ShiftUp()
{
	m_Desk.ShiftUp();
	ProcessDeskMoves();
}

void CPuzzleWnd::ShiftDown()
{
	m_Desk.ShiftDown();
	ProcessDeskMoves();
}

void CPuzzleWnd::ShiftLeft()
{
	m_Desk.ShiftLeft();
	ProcessDeskMoves();
}

void CPuzzleWnd::ShiftRight()
{
	m_Desk.ShiftRight();
	ProcessDeskMoves();
}

void CPuzzleWnd::ProcessDeskMoves()
{
	CMove Move;
	while (m_Desk.GetMove(Move))
		GenerSmoothMoves(Move);
	PostTestSolved();
}

void CPuzzleWnd::Redraw()
{
	RedrawOff();
	Invalidate();
}

void CPuzzleWnd::OnAutoMode()
{
	if (!m_bSolving)
		return;
	if (!m_bAutoMode)
	{
		m_bAutoMode = true;
		AutoSolverStart();
	}
	else
	{
		m_bAutoMode = false;
		AutoSolverStop();
	}
}

void CPuzzleWnd::OnAutoTimer()
{
	if (!m_bAutoMode)
	{
		AutoSolverStop();
		return;
	}
	int nMove;
	if (m_AutoSolver.GetMove(nMove))
	{
		switch (nMove)
		{
			case CAutoSolver::AUTO_UP:
				ShiftUp();
				break;

			case CAutoSolver::AUTO_DOWN:
				ShiftDown();
				break;

			case CAutoSolver::AUTO_LEFT:
				ShiftLeft();
				break;

			case CAutoSolver::AUTO_RIGHT:
				ShiftRight();
				break;
		}
	}
	else
	{
		m_AutoSolver.SolveStep();
		if (m_AutoSolver.IsMove())
			return;
		m_bAutoMode = false;
		AutoSolverStop();
	}
}

void CPuzzleWnd::AutoSolverStart()
{
	m_AutoSolver.InitFromDesk(m_Desk);
	m_AutoSolver.SolveStep();
	if (m_AutoSolver.IsMove())
		SetTimer(AUTO_TIMER, AUTO_TIMER_PERIOD, NULL);
	else
	{
		m_bAutoMode = false;
		AutoSolverStop();
	}
}

void CPuzzleWnd::AutoSolverStop()
{
	KillTimer(AUTO_TIMER);
	m_AutoSolver.Reset();
}

void CPuzzleWnd::OnLButtonDown(UINT nFlags, CPoint point)
{
	if (!m_bAutoMode && m_bSolving)
	{
		int nX, nY;
		if (m_ClickItemMap.FindTile(
			point,
			nX,
			nY))
		{
			m_Desk.ClickTile(nX, nY);
			ProcessDeskMoves();
		}
	}
	CWnd::OnLButtonDown(nFlags, point);
}

void CPuzzleWnd::DrawRect(CDC &dc, CRect &r)
{
	dc.MoveTo(r.left, r.top);
	dc.LineTo(r.left, r.bottom);
	dc.LineTo(r.right, r.bottom);
	dc.LineTo(r.right, r.top);
	dc.LineTo(r.left, r.top);
}

void CPuzzleWnd::OnDesk(int nSize)
{
	if (m_Desk.GetSize() == nSize)
		return;
	if (!GiveUp())
		return;
	m_Desk.Init(nSize);
	if (m_bSolving)
		m_Desk.Shuffle();
	Redraw();
}

/////////////////////////////////////////////////////////////////////////////

CSmoothMove::CSmoothMove()
{
	m_nTile = CDesk::s_nEmpty;
	m_bTileSolved = false;
}

CSmoothMove::CSmoothMove(
	const CRect &rTileRect,
	const CRect &rRedrawRect,
	int nTile,
	bool bTileSolved)
:	m_rTileRect(rTileRect),
	m_rRedrawRect(rRedrawRect)
{
	m_nTile = nTile;
	m_bTileSolved = bTileSolved;
}

/////////////////////////////////////////////////////////////////////////////

CClickItem::CClickItem()
{
	m_nX = CDesk::s_nEmpty;
	m_nY = CDesk::s_nEmpty;
}

CClickItem::CClickItem(
	const CRect &rClickRect,
	int nX,
	int nY)
:	m_rClickRect(rClickRect)
{
	m_nX = nX;
	m_nY = nY;
}

/////////////////////////////////////////////////////////////////////////////

void CClickItemMap::AddItem(CClickItem &Item)
{
	m_List.AddTail(Item);
}

bool CClickItemMap::FindTile(
	const CPoint &ptClick,
	int &nX,
	int &nY)
{
	POSITION pos = m_List.GetHeadPosition();
	while (pos != NULL)
	{
		CClickItem &Item = m_List.GetNext(pos);
		if (Item.GetClickRect().PtInRect(ptClick))
		{
			nX = Item.GetX();
			nY = Item.GetY();
			return true;
		}
	}
	return false;
}

void CClickItemMap::Reset()
{
	m_List.RemoveAll();
}
