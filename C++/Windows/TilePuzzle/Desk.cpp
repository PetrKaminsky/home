// Desk.cpp: implementation of the CDesk class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "TilePuzzle.h"
#include "Desk.h"

//////////////////////////////////////////////////////////////////////

int CDesk::s_nEmpty = -1;

CDesk::CDesk()
{
	m_nSize = -1;
	m_pDesk = NULL;
	m_nEmptyX = -1;
	m_nEmptyY = -1;
}

CDesk::~CDesk()
{
	CleanUp();
}

void CDesk::Init(int nSize)
{
	CleanUp();

	m_nSize = nSize;
	int nTiles = m_nSize * m_nSize;
	m_pDesk = new int[nTiles];
	int i = 0, n = nTiles - 1;
	while (i < n)
	{
		m_pDesk[i] = i + 1;
		i++;
	};
	m_pDesk[i] = s_nEmpty;
	m_nEmptyX = m_nSize - 1;
	m_nEmptyY = m_nSize - 1;
}

void CDesk::CleanUp()
{
	if (m_pDesk != NULL)
		delete []m_pDesk;
	m_nSize = -1;
}

void CDesk::GetTile(
	int nX,
	int nY,
	int &nTile,
	bool &bPosOK)
{
	int nPos = GetTilePos(nX, nY);
	nTile = m_pDesk[nPos];
	bPosOK = IsTileSolved(nTile, nPos);
}

void CDesk::ShiftUp()
{
	int nX = m_nEmptyX, nY = m_nEmptyY;
	if (nY >= (m_nSize - 1))
		return;
	int nFromX = nX;
	int nFromY = nY + 1;
	int nToX = nX;
	int nToY = nY;
	int nFromPos = GetTilePos(nFromX, nFromY);
	int nToPos = GetTilePos(nToX, nToY);
	m_MoveQueue.AddTail(CMove(
		m_pDesk[nFromPos],
		IsTileSolved(m_pDesk[nFromPos], nToPos),
		nFromX,
		nFromY,
		nToX,
		nToY));
	Swap(m_pDesk[nFromPos], m_pDesk[nToPos]);
	m_nEmptyX = nFromX;
	m_nEmptyY = nFromY;
}

void CDesk::ShiftDown()
{
	int nX = m_nEmptyX, nY = m_nEmptyY;
	if (nY < 1)
		return;
	int nFromX = nX;
	int nFromY = nY - 1;
	int nToX = nX;
	int nToY = nY;
	int nFromPos = GetTilePos(nFromX, nFromY);
	int nToPos = GetTilePos(nToX, nToY);
	m_MoveQueue.AddTail(CMove(
		m_pDesk[nFromPos],
		IsTileSolved(m_pDesk[nFromPos], nToPos),
		nFromX,
		nFromY,
		nToX,
		nToY));
	Swap(m_pDesk[nFromPos], m_pDesk[nToPos]);
	m_nEmptyX = nFromX;
	m_nEmptyY = nFromY;
}

void CDesk::ShiftLeft()
{
	int nX = m_nEmptyX, nY = m_nEmptyY;
	if (nX >= (m_nSize - 1))
		return;
	int nFromX = nX + 1;
	int nFromY = nY;
	int nToX = nX;
	int nToY = nY;
	int nFromPos = GetTilePos(nFromX, nFromY);
	int nToPos = GetTilePos(nToX, nToY);
	m_MoveQueue.AddTail(CMove(
		m_pDesk[nFromPos],
		IsTileSolved(m_pDesk[nFromPos], nToPos),
		nFromX,
		nFromY,
		nToX,
		nToY));
	Swap(m_pDesk[nFromPos], m_pDesk[nToPos]);
	m_nEmptyX = nFromX;
	m_nEmptyY = nFromY;
}

void CDesk::ShiftRight()
{
	int nX = m_nEmptyX, nY = m_nEmptyY;
	if (nX < 1)
		return;
	int nFromX = nX - 1;
	int nFromY = nY;
	int nToX = nX;
	int nToY = nY;
	int nFromPos = GetTilePos(nFromX, nFromY);
	int nToPos = GetTilePos(nToX, nToY);
	m_MoveQueue.AddTail(CMove(
		m_pDesk[nFromPos],
		IsTileSolved(m_pDesk[nFromPos], nToPos),
		nFromX,
		nFromY,
		nToX,
		nToY));
	Swap(m_pDesk[nFromPos], m_pDesk[nToPos]);
	m_nEmptyX = nFromX;
	m_nEmptyY = nFromY;
}

void CDesk::Shuffle()
{
	int i, nMoves = m_nSize * m_nSize * 200;
	srand(time(NULL));
	for (i = 0; i < nMoves; i++)
		switch (4 * rand() / RAND_MAX)
		{
		case 0:
			ShiftUp();
			break;
		case 1:
			ShiftDown();
			break;
		case 2:
			ShiftLeft();
			break;
		case 3:
			ShiftRight();
			break;
		}
	m_MoveQueue.RemoveAll();
}

bool CDesk::IsSolved()
{
	int i, n = m_nSize * m_nSize - 1;
	for (i = 0; i < n; i++)
		if (m_pDesk[i] != (i + 1))
			return false;

	return true;
}

bool CDesk::GetMove(CMove &Move)
{
	if (m_MoveQueue.IsEmpty())
		return false;

	Move = m_MoveQueue.RemoveHead();

	return true;
}

int CDesk::GetTilePos(int nX, int nY)
{
	return nX + nY * m_nSize;
}

bool CDesk::IsTileSolved(int nTile, int nPos)
{
	return (nTile != s_nEmpty) && (nTile == (nPos + 1));
}

void CDesk::ClickTile(int nX, int nY)
{
	int nDestX = m_nEmptyX;
	int nDestY = m_nEmptyY;
	if ((nDestX == nX) ||
		(nDestY == nY))
	{
		if ((nDestX == nX) &&
			(nDestY == nY))
			return;
		while (nDestX != nX)
		{
			if (nX > nDestX)
			{
				ShiftLeft();
				nX--;
			}
			else
			{
				ShiftRight();
				nX++;
			}
		}
		while (nDestY != nY)
		{
			if (nY > nDestY)
			{
				ShiftUp();
				nY--;
			}
			else
			{
				ShiftDown();
				nY++;
			}
		}
	}
}

//////////////////////////////////////////////////////////////////////

CMove::CMove()
{
	m_nTile = CDesk::s_nEmpty;
	m_bTileSolved = false;
	m_nFromX = CDesk::s_nEmpty;
	m_nFromY = CDesk::s_nEmpty;
	m_nToX = CDesk::s_nEmpty;
	m_nToY = CDesk::s_nEmpty;
}

CMove::CMove(
	int nTile,
	bool bTileSolved,
	int nFromX,
	int nFromY,
	int nToX,
	int nToY)
{
	m_nTile = nTile;
	m_bTileSolved = bTileSolved;
	m_nFromX = nFromX;
	m_nFromY = nFromY;
	m_nToX = nToX;
	m_nToY = nToY;
}
