#if !defined(AFX_PUZZLEWND_H__2332AE49_3309_4E3B_A9B7_4C445627695B__INCLUDED_)
#define AFX_PUZZLEWND_H__2332AE49_3309_4E3B_A9B7_4C445627695B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// PuzzleWnd.h : header file
//

#include "Desk.h"
#include "AutoSolver.h"

#define WM_TEST_SOLVED (WM_USER + 1)
#define SMOOTH_TIMER 1
#define SMOOTH_TIMER_PERIOD 10
#define AUTO_TIMER 2
#define AUTO_TIMER_PERIOD 300

/////////////////////////////////////////////////////////////////////////////

class CSmoothMove
{
private:
	CRect m_rTileRect;
	CRect m_rRedrawRect;
	int m_nTile;
	bool m_bTileSolved;

public:
	CSmoothMove();
	CSmoothMove(
		const CRect &rTileRect,
		const CRect &rRedrawRect,
		int nTile,
		bool bTileSolved);

	CRect GetTileRect() const
		{ return m_rTileRect; }
	CRect GetRedrawRect() const 
		{ return m_rRedrawRect; }
	int GetTile() const
		{ return m_nTile; }
	bool GetTileSolved() const
		{ return m_bTileSolved; }
};

/////////////////////////////////////////////////////////////////////////////

class CClickItem
{
private:
	CRect m_rClickRect;
	int m_nX;
	int m_nY;

public:
	CClickItem();
	CClickItem(
		const CRect &rClickRect,
		int nX,
		int nY);

	CRect GetClickRect() const
		{ return m_rClickRect; }
	int GetX() const
		{ return m_nX; }
	int GetY() const
		{ return m_nY; }
};

/////////////////////////////////////////////////////////////////////////////

class CClickItemMap
{
public:
	void AddItem(CClickItem &Item);
	bool FindTile(
		const CPoint &ptClick,
		int &nX,
		int &nY);
	void Reset();

private:
	CList<CClickItem, CClickItem&> m_List;
};

/////////////////////////////////////////////////////////////////////////////

class CPuzzleWnd : public CWnd
{
public:
	CPuzzleWnd();

	int Create();

	//{{AFX_VIRTUAL(CPuzzleWnd)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	//}}AFX_VIRTUAL

protected:
	//{{AFX_MSG(CPuzzleWnd)
	afx_msg void OnPaint();
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnKeyUp();
	afx_msg void OnKeyDown();
	afx_msg void OnKeyLeft();
	afx_msg void OnKeyRight();
	afx_msg void OnShuffle();
	afx_msg void OnDesk3x3();
	afx_msg void OnDesk4x4();
	afx_msg void OnDesk5x5();
	afx_msg void OnDesk6x6();
	afx_msg void OnDesk7x7();
	afx_msg void OnDesk8x8();
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
	afx_msg void OnAppExit();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnAutoMode();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnDesk9x9();
	//}}AFX_MSG
	afx_msg LRESULT OnTestSolved(WPARAM wParam, LPARAM lParam);
	DECLARE_MESSAGE_MAP()

private:
	CDesk m_Desk;
	HACCEL m_hAccelTable;
	int m_nSize;
	int m_nBorder;
	int m_nDistance;
	CBitmap m_bmpOff;
	bool m_bSolving;
	bool m_bSmoothTimer;
	CList<CSmoothMove, CSmoothMove&> m_SmoothMoveQueue;
	bool m_bAutoMode;
	CAutoSolver m_AutoSolver;
	COLORREF m_clrTileBgNotSolved;
	COLORREF m_clrTileBgSolved;
	COLORREF m_clrTileTxNotSolved;
	COLORREF m_clrTileTxSolved;
	COLORREF m_clrDeskBg;
	CClickItemMap m_ClickItemMap;

	void DrawTile(
		CDC &dc,
		CRect &r,
		int nTile,
		bool bTileSolved);
	BOOL LoadAccelTable();
	void InitMenu(CMenu *pMenu);
	void RedrawOff();
	void PostTestSolved();
	bool GiveUp();
	void OnSmoothMoveTimer();
	void GenerSmoothMoves(const CMove &Move);
	void CalcRects(
		const CMove &Move,
		CRect &rFrom,
		CRect &rTo,
		CRect &rRedraw);
	bool GetSmoothMove(CSmoothMove &SmoothMove);
	void ShiftUp();
	void ShiftDown();
	void ShiftLeft();
	void ShiftRight();
	void ProcessDeskMoves();
	void Redraw();
	void OnAutoTimer();
	void AutoSolverStart();
	void AutoSolverStop();
	void DrawRect(CDC &dc, CRect &r);
	void OnDesk(int nSize);
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PUZZLEWND_H__2332AE49_3309_4E3B_A9B7_4C445627695B__INCLUDED_)
