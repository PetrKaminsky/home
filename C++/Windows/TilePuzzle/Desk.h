// Desk.h: interface for the CDesk class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DESK_H__5383B461_7AFA_45D0_A418_600BBE070465__INCLUDED_)
#define AFX_DESK_H__5383B461_7AFA_45D0_A418_600BBE070465__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//////////////////////////////////////////////////////////////////////

class CMove
{
private:
	int m_nTile;
	bool m_bTileSolved;
	int m_nFromX;
	int m_nFromY;
	int m_nToX;
	int m_nToY;

public:
	CMove();
	CMove(
		int nTile,
		bool bTileSolved,
		int nFromX,
		int nFromY,
		int nToX,
		int nToY);

	int GetTile() const
		{ return m_nTile; }
	bool IsTileSolved() const
		{ return m_bTileSolved; }
	int GetFromX() const
		{ return m_nFromX; }
	int GetFromY() const
		{ return m_nFromY; }
	int GetToX() const
		{ return m_nToX; }
	int GetToY() const
		{ return m_nToY; }
};

//////////////////////////////////////////////////////////////////////

class CDesk
{
public:
	CDesk();
	~CDesk();

	static int s_nEmpty;
	void Init(int nSize);
	void GetTile(
		int nX,
		int nY,
		int &nTile,
		bool &bPosOK);
	void ShiftUp();
	void ShiftDown();
	void ShiftLeft();
	void ShiftRight();
	void Shuffle();
	int GetSize() const
		{ return m_nSize; }
	int *GetDesk() const
		{ return m_pDesk; }
	bool IsSolved();
	bool GetMove(CMove &Move);
	void ClickTile(int nX, int nY);

private:
	int m_nSize;
	int *m_pDesk;
	CList<CMove, CMove&> m_MoveQueue;
	int m_nEmptyX;
	int m_nEmptyY;

	void CleanUp();
	int GetTilePos(int nX, int nY);
	bool IsTileSolved(int nTile, int nPos);
};

#endif // !defined(AFX_DESK_H__5383B461_7AFA_45D0_A418_600BBE070465__INCLUDED_)
