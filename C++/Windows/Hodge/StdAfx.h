//*******************************************************************************************
#if !defined(AFX_STDAFX_H__882BA509_FDB4_11D4_AB74_0060083D8E3C__INCLUDED_)
#define AFX_STDAFX_H__882BA509_FDB4_11D4_AB74_0060083D8E3C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers

#include <afxwin.h>
#include <afxmt.h>
#include <afxdlgs.h>
//*******************************************************************************************
//{{AFX_INSERT_LOCATION}}

#endif // !defined(AFX_STDAFX_H__882BA509_FDB4_11D4_AB74_0060083D8E3C__INCLUDED_)
