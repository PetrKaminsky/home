//*******************************************************************************************
#if !defined(AFX_THREAD_H__DCD86E85_C0BF_11D5_A72C_000000000000__INCLUDED_)
#define AFX_THREAD_H__DCD86E85_C0BF_11D5_A72C_000000000000__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
//*******************************************************************************************
class CMemoryMappedFile
{
public:
	CMemoryMappedFile();
	~CMemoryMappedFile();

	BYTE *Reinitialize(DWORD dwSize);

private:
	CString m_sFileName;
	LPVOID m_pMemory;
	HANDLE m_hMapping;
	HANDLE m_hFile;

	void GenerateName();
	void Close();
	BYTE *Open(DWORD dwSize);
};
//*******************************************************************************************
class CThreadWnd : public CWnd
{
public:
	CThreadWnd();

	//{{AFX_VIRTUAL(CThreadWnd)
	//}}AFX_VIRTUAL

private:
	void AllocateMemory();
	void Generate1stGeneration();
	void CalculateGeneration();
	int CalculateCell(BYTE *pData, int x, int y);
	int GetRealX(int x);
	int GetRealY(int y);
	bool IsHealth(int nState);
	bool IsInfected(int nState);
	bool IsIll(int nState);
	int CalculateHealth(BYTE *pData, int x, int y);
	int CalculateInfected(BYTE *pData, int x, int y);

	CWnd *m_pHodgeWnd;
	int m_nMaxX;
	int m_nMaxY;
	int m_nMaxColor;
	int m_nConstK1;
	int m_nConstK2;
	int m_nConstG;
	CMemoryMappedFile m_Data1;
	CMemoryMappedFile m_Data2;
	BYTE *m_pData1;
	BYTE *m_pData2;
	BYTE *m_pCurrData;

protected:
	//{{AFX_MSG(CThreadWnd)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	//}}AFX_MSG
	afx_msg LRESULT OnSendHodgeWnd(WPARAM wParam, LPARAM);
	afx_msg LRESULT OnStopThread(WPARAM wParam, LPARAM);
	afx_msg LRESULT OnCompute(WPARAM, LPARAM);
	afx_msg LRESULT OnRestart(WPARAM, LPARAM);
	afx_msg LRESULT OnIgnore(WPARAM, LPARAM);

	DECLARE_MESSAGE_MAP()
};
//*******************************************************************************************
class CHodgeThread : public CWinThread
{
	DECLARE_DYNCREATE(CHodgeThread)
protected:
	CHodgeThread();           // protected constructor used by dynamic creation

public:
	//{{AFX_VIRTUAL(CHodgeThread)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	//}}AFX_VIRTUAL

	static CWnd *s_pThrWnd;
	static HANDLE s_hEvent;

protected:
	//{{AFX_MSG(CHodgeThread)
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};
//*******************************************************************************************

/////////////////////////////////////////////////////////////////////////////
//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_THREAD_H__DCD86E85_C0BF_11D5_A72C_000000000000__INCLUDED_)
