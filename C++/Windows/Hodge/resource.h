//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by Hodge.rc
//
#define IDR_MAINFRAME                   128
#define IDR_MENU                        129
#define IDC_CURSOR                      130
#define IDD_PROPS                       131
#define IDC_EDIT_WIDTH                  1000
#define IDC_EDIT_HEIGHT                 1001
#define IDC_EDIT_DEPTH                  1002
#define IDC_EDIT_K1                     1003
#define IDC_EDIT_K2                     1004
#define IDC_EDIT_G                      1005
#define IDC_STATIC_COLOUR               1010
#define ID_QUIT                         32772
#define ID_PROPS                        32773
#define ID_RESTART                      32775

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        133
#define _APS_NEXT_COMMAND_VALUE         32776
#define _APS_NEXT_CONTROL_VALUE         1011
#define _APS_NEXT_SYMED_VALUE           104
#endif
#endif
