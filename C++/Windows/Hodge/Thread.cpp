//*******************************************************************************************
#include "stdafx.h"
#include "hodge.h"
#include "Thread.h"
//*******************************************************************************************
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif
//*******************************************************************************************
IMPLEMENT_DYNCREATE(CHodgeThread, CWinThread)
//*******************************************************************************************
CWnd *CHodgeThread::s_pThrWnd = NULL;
HANDLE CHodgeThread::s_hEvent = NULL;
//*******************************************************************************************
CHodgeThread::CHodgeThread()
{
}
//*******************************************************************************************
BOOL CHodgeThread::InitInstance()
{
	s_pThrWnd = new CThreadWnd;
	s_pThrWnd->CreateEx(0,
			AfxRegisterWndClass(NULL),
			"Thread",
			WS_POPUP,
			CRect(0, 0, 1, 1),
			NULL,
			0);
	m_pMainWnd = s_pThrWnd;

	return TRUE;
}
//*******************************************************************************************
int CHodgeThread::ExitInstance()
{
	s_pThrWnd->DestroyWindow();
	delete s_pThrWnd;
	SetEvent(s_hEvent);
	m_pMainWnd = NULL;

	return CWinThread::ExitInstance();
}
//*******************************************************************************************
BEGIN_MESSAGE_MAP(CHodgeThread, CWinThread)
	//{{AFX_MSG_MAP(CHodgeThread)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()
//*******************************************************************************************
//*******************************************************************************************
//*******************************************************************************************
//*******************************************************************************************
CThreadWnd::CThreadWnd()
{
	m_pHodgeWnd = NULL;
	m_nMaxX = 0;
	m_nMaxY = 0;
	m_nMaxColor = 0;
	m_nConstK1 = 0;
	m_nConstK2 = 0;
	m_nConstG = 0;
	m_pData1 = NULL;
	m_pData2 = NULL;
	m_pCurrData = NULL;
}
//*******************************************************************************************
BEGIN_MESSAGE_MAP(CThreadWnd, CWnd)
	//{{AFX_MSG_MAP(CThreadWnd)
	ON_WM_CREATE()
	//}}AFX_MSG_MAP
	ON_MESSAGE(WM_SEND_HODGE_WND, OnSendHodgeWnd)
	ON_MESSAGE(WM_STOP_THREAD, OnStopThread)
	ON_MESSAGE(WM_COMPUTE, OnCompute)
	ON_MESSAGE(WM_RESTART, OnRestart)
	ON_MESSAGE(WM_IGNORE, OnIgnore)
END_MESSAGE_MAP()
//*******************************************************************************************
int CThreadWnd::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	SetEvent(CHodgeThread::s_hEvent);
	return 0;
}
//*******************************************************************************************
LRESULT CThreadWnd::OnSendHodgeWnd(WPARAM wParam, LPARAM)
{
	m_pHodgeWnd = (CWnd *)wParam;
	return 0;
}
//*******************************************************************************************
LRESULT CThreadWnd::OnStopThread(WPARAM, LPARAM)
{
	PostQuitMessage(0);
	return 0;
}
//*******************************************************************************************
LRESULT CThreadWnd::OnCompute(WPARAM, LPARAM)
{
	g_Params.Lock();
	int nMaxX = g_Params.GetMaxX();
	int nMaxY = g_Params.GetMaxY();
	m_nMaxColor = g_Params.GetMaxColor();
	m_nConstK1 = g_Params.GetConstK1();
	m_nConstK2 = g_Params.GetConstK2();
	m_nConstG = g_Params.GetConstG();
	g_Params.Unlock();
	if (m_nMaxX != nMaxX ||
		m_nMaxY != nMaxY)
	{
		m_nMaxX = nMaxX;
		m_nMaxY = nMaxY;
		AllocateMemory();
		Generate1stGeneration();
	}
	else
		CalculateGeneration();
	m_pHodgeWnd->PostMessage(WM_FINISHED, (WPARAM)m_pCurrData);
	return 0;
}
//*******************************************************************************************
LRESULT CThreadWnd::OnRestart(WPARAM, LPARAM)
{
	Generate1stGeneration();
	m_pHodgeWnd->PostMessage(WM_RESTART, (WPARAM)m_pCurrData);
	return 0;
}
//*******************************************************************************************
LRESULT CThreadWnd::OnIgnore(WPARAM, LPARAM)
{
	m_pHodgeWnd->PostMessage(WM_IGNORE);
	return 0;
}
//*******************************************************************************************
void CThreadWnd::AllocateMemory()
{
	m_pData1 = m_Data1.Reinitialize(m_nMaxX * m_nMaxY);
	m_pData2 = m_Data2.Reinitialize(m_nMaxX * m_nMaxY);
}
//*******************************************************************************************
void CThreadWnd::Generate1stGeneration()
{
	int i, j;
	for (j = 0; j < m_nMaxY; j++)
		for (i = 0; i < m_nMaxX; i++)
			m_pData1[i + j * m_nMaxX] = (rand() * m_nMaxColor) / RAND_MAX;
	m_pCurrData = m_pData1;
}
//*******************************************************************************************
void CThreadWnd::CalculateGeneration()
{
	BYTE *pData = m_pCurrData;
	BYTE *pDstData = pData == m_pData1 ? m_pData2 : m_pData1;
	BYTE *pSrcData = pData == m_pData1 ? m_pData1 : m_pData2;
	int i, j;
	for (j = 0; j < m_nMaxY; j++)
		for (i = 0; i < m_nMaxX; i++)
			pDstData[i + j * m_nMaxX] = CalculateCell(pSrcData, i, j);

	m_pCurrData = pDstData;
}
//*******************************************************************************************
int CThreadWnd::CalculateCell(BYTE *pData, int x, int y)
{
	int nCurrState = pData[x + y * m_nMaxX], nNewState;
	if (IsHealth(nCurrState))
		nNewState = CalculateHealth(pData, x, y);
	else if (IsInfected(nCurrState))
		nNewState = CalculateInfected(pData, x, y);
	else
		nNewState = 0;
	return nNewState;
}
//*******************************************************************************************
int CThreadWnd::GetRealX(int x)
{
	if (x < 0)
	{
		do
		{
			x += m_nMaxX;
		}
		while (x < 0);
	}
	else if (x >= m_nMaxX)
	{
		do
		{
			x -= m_nMaxX;
		}
		while (x >= m_nMaxX);
	}
	return x;
}
//*******************************************************************************************
int CThreadWnd::GetRealY(int y)
{
	if (y < 0)
	{
		do
		{
			y += m_nMaxY;
		}
		while (y < 0);
	}
	else if (y >= m_nMaxY)
	{
		do
		{
			y -= m_nMaxY;
		}
		while (y >= m_nMaxY);
	}
	return y;
}
//*******************************************************************************************
bool CThreadWnd::IsHealth(int nState)
{
	return (nState == 0);
}
//*******************************************************************************************
bool CThreadWnd::IsInfected(int nState)
{
	return (nState > 0 && nState < m_nMaxColor);
}
//*******************************************************************************************
bool CThreadWnd::IsIll(int nState)
{
	return (nState == m_nMaxColor);
}
//*******************************************************************************************
int CThreadWnd::CalculateHealth(BYTE *pData, int x, int y)
{
	int i, j, nState, nInfected = 0, nIll = 0;
	int nMinX = x - 1, nMaxX = x + 1;
	int nMinY = y - 1, nMaxY = y + 1;
	for (j = nMinY; j <= nMaxY; j++)
		for (i = nMinX; i <= nMaxX; i++)
		{
			if (j == y && i == x)
				continue;
			nState = pData[GetRealX(i) + GetRealY(j) * m_nMaxX];
			if (IsInfected(nState))
				nInfected++;
			else if (IsIll(nState))
				nIll++;
		}
	nState = (int)((double)nInfected / m_nConstK1 + (double)nIll / m_nConstK2);
	if (nState > m_nMaxColor)
		nState = m_nMaxColor;
	return nState;
}
//*******************************************************************************************
int CThreadWnd::CalculateInfected(BYTE *pData, int x, int y)
{
	int i, j, nState, nInfected = 0, nSumState = 0;
	int nMinX = x - 1, nMaxX = x + 1;
	int nMinY = y - 1, nMaxY = y + 1;
	for (j = nMinY; j <= nMaxY; j++)
		for (i = nMinX; i <= nMaxX; i++)
		{
			nState = pData[GetRealX(i) + GetRealY(j) * m_nMaxX];
			if (IsInfected(nState))
				nInfected++;
			nSumState += nState;
		}
	nState = nSumState / nInfected + m_nConstG;
	if (nState > m_nMaxColor)
		nState = m_nMaxColor;
	return nState;
}
//*******************************************************************************************
//*******************************************************************************************
//*******************************************************************************************
//*******************************************************************************************
CMemoryMappedFile::CMemoryMappedFile()
{
	m_pMemory = NULL;
	m_hMapping = NULL;
	m_hFile = INVALID_HANDLE_VALUE;
}
//*******************************************************************************************
CMemoryMappedFile::~CMemoryMappedFile()
{
	Close();
}
//*******************************************************************************************
BYTE *CMemoryMappedFile::Reinitialize(DWORD dwSize)
{
	if (m_sFileName.IsEmpty())
		GenerateName();
	Close();
	return Open(dwSize);
}
//*******************************************************************************************
void CMemoryMappedFile::GenerateName()
{
	DWORD dwLength = GetTempPath(0, NULL);
	char *pTempPath = new char[dwLength];
	GetTempPath(dwLength, pTempPath);
	char *pTempFile = new char[dwLength + MAX_PATH];
	GetTempFileName(
		pTempPath,
		"HDG",
		0,
		pTempFile);
	m_sFileName = pTempFile;
	delete []pTempFile;
	delete []pTempPath;
}
//*******************************************************************************************
void CMemoryMappedFile::Close()
{
	if (m_pMemory)
	{
		UnmapViewOfFile(m_pMemory);
		m_pMemory = NULL;
	}
	if (m_hMapping)
	{
		CloseHandle(m_hMapping);
		m_hMapping = NULL;
	}
	if (m_hFile != INVALID_HANDLE_VALUE)
	{
		CloseHandle(m_hFile);
		m_hFile = INVALID_HANDLE_VALUE;
	}
}
//*******************************************************************************************
BYTE *CMemoryMappedFile::Open(DWORD dwSize)
{
	m_hFile = CreateFile(
		m_sFileName,
		GENERIC_READ | GENERIC_WRITE,
		0,
		NULL,
		CREATE_ALWAYS,
		FILE_ATTRIBUTE_TEMPORARY | FILE_FLAG_DELETE_ON_CLOSE,
		NULL);
	if (m_hFile != INVALID_HANDLE_VALUE)
	{
		SetFilePointer(
			m_hFile,
			dwSize,
			NULL,
			FILE_BEGIN);
		SetEndOfFile(m_hFile);
		m_hMapping = CreateFileMapping(
			m_hFile,
			NULL,
			PAGE_READWRITE,
			0,
			0,
			NULL);
		if (m_hMapping)
			m_pMemory = MapViewOfFile(
				m_hMapping,
				FILE_MAP_WRITE,
				0,
				0,
				0);
	}
	return (BYTE *)m_pMemory;
}
//*******************************************************************************************
