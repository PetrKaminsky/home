//*******************************************************************************************
#include "stdafx.h"
#include "Hodge.h"
#include "HodgeWnd.h"
#include "PropsDlg.h"
#include "Thread.h"
//*******************************************************************************************
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif
//*******************************************************************************************
CHodgeWnd::CHodgeWnd(
	int nMaxX,
	int nMaxY,
	int nMaxColor,
	COLORREF clrBase)
{
	m_nMaxX = nMaxX;
	m_nMaxY = nMaxY;
	m_nMaxColor = nMaxColor;
	m_bVisible = false;
	m_bIgnoreData = false;
	m_nNormRed = 0;
	m_nNormGreen = 0;
	m_nNormBlue = 0;
	NormalizeColour(clrBase);
}
//*******************************************************************************************
BEGIN_MESSAGE_MAP(CHodgeWnd, CWnd)
	//{{AFX_MSG_MAP(CHodgeWnd)
	ON_WM_CONTEXTMENU()
	ON_COMMAND(ID_QUIT, OnMenuQuit)
	ON_COMMAND(ID_PROPS, OnMenuProps)
	ON_COMMAND(ID_RESTART, OnMenuRestart)
	ON_WM_ERASEBKGND()
	ON_WM_PAINT()
	ON_WM_CREATE()
	ON_WM_LBUTTONDBLCLK()
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
	ON_MESSAGE(WM_FINISHED, OnFinished)
	ON_MESSAGE(WM_IGNORE, OnIgnore)
	ON_MESSAGE(WM_RESTART, OnRestart)
END_MESSAGE_MAP()
//*******************************************************************************************
void CHodgeWnd::OnContextMenu(CWnd *pWnd, CPoint point)
{
	CMenu Menu;
	Menu.LoadMenu(IDR_MENU);
	CMenu* pCtxMenu = Menu.GetSubMenu(0);
	if (point.x == -1 && point.y == -1)
	{
		CRect r;
		GetWindowRect(&r);
		point.x = r.left;
		point.y = r.top;
	}
	pCtxMenu->TrackPopupMenu(TPM_LEFTALIGN | TPM_LEFTBUTTON, point.x, point.y, this);
}
//*******************************************************************************************
void CHodgeWnd::OnMenuQuit()
{
	DestroyWindow();
}
//*******************************************************************************************
void CHodgeWnd::OnMenuProps()
{
	CPropsDlg dlg(this);
	g_Params.Lock();
	int nMaxX = dlg.m_nWidth = m_nMaxX;
	int nMaxY = dlg.m_nHeight = m_nMaxY;
	dlg.m_nDepth = g_Params.GetMaxColor();
	dlg.m_nK1 = g_Params.GetConstK1();
	dlg.m_nK2 = g_Params.GetConstK2();
	dlg.m_nG = g_Params.GetConstG();
	dlg.m_clrBase = g_Params.GetColour();
	g_Params.Unlock();
	if (dlg.DoModal() == IDOK)
	{
		bool bSizeChanged = false;
		nMaxX = dlg.m_nWidth;
		nMaxY = dlg.m_nHeight;
		if (m_nMaxX != nMaxX ||
			m_nMaxY != nMaxY)
			bSizeChanged = true;
		g_Params.Lock();
		if (bSizeChanged)
		{
			g_Params.SetMaxX(m_nMaxX = nMaxX);
			g_Params.SetMaxY(m_nMaxY = nMaxY);
		}
		g_Params.SetMaxColor(m_nMaxColor = dlg.m_nDepth);
		g_Params.SetConstK1(dlg.m_nK1);
		g_Params.SetConstK2(dlg.m_nK2);
		g_Params.SetConstG(dlg.m_nG);
		g_Params.SetColour(dlg.m_clrBase);
		g_Params.Save();
		g_Params.Unlock();
		NormalizeColour(dlg.m_clrBase);

		if (bSizeChanged)
		{
			m_bVisible = false;
			ShowWindow(SW_HIDE);
			MoveWindow(0, 0, nMaxX, nMaxY, FALSE);
			InitBitmap();
			CenterWindow();
		}
		Ignore();
	}
}
//*******************************************************************************************
BOOL CHodgeWnd::OnEraseBkgnd(CDC *pDC)
{
	return TRUE;
}
//*******************************************************************************************
void CHodgeWnd::OnPaint()
{
	if (GetUpdateRect(NULL))
	{
		CPaintDC dc(this);
		CDC memdc;
		memdc.CreateCompatibleDC(&dc);
		CRect r = dc.m_ps.rcPaint;
		CBitmap *poldbmp = memdc.SelectObject(&m_bmpOff);
		dc.BitBlt(r.left,
			r.top,
			r.right - r.left,
			r.bottom - r.top,
			&memdc,
			r.left,
			r.top,
			SRCCOPY);
		memdc.SelectObject(poldbmp);
	}
}
//*******************************************************************************************
int CHodgeWnd::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	StartThread();
	InitBitmap();
	Compute();
	return 0;
}
//*******************************************************************************************
void CHodgeWnd::OnLButtonDblClk(UINT nFlags, CPoint point)
{
	OnMenuQuit();
}
//*******************************************************************************************
void CHodgeWnd::InitBitmap()
{
	CClientDC dc(this);
	if (m_bmpOff.GetSafeHandle())
		m_bmpOff.DeleteObject();
	m_bmpOff.CreateCompatibleBitmap(&dc, 
		m_nMaxX, 
		m_nMaxY);
}
//*******************************************************************************************
void CHodgeWnd::Compute()
{
	CHodgeThread::s_pThrWnd->PostMessage(WM_COMPUTE);
}
//*******************************************************************************************
void CHodgeWnd::OnDestroy()
{
	StopThread();
	CWnd::OnDestroy();
}
//*******************************************************************************************
COLORREF CHodgeWnd::GenerateColor(BYTE *pData, int x, int y)
{
	return Transform(pData[x + y * m_nMaxX]);
}
//*******************************************************************************************
COLORREF CHodgeWnd::Transform(int nVal)
{
	return RGB(
		nVal * m_nNormRed / m_nMaxColor,
		nVal * m_nNormGreen / m_nMaxColor,
		nVal * m_nNormBlue / m_nMaxColor);
}
//*******************************************************************************************
void CHodgeWnd::OnMenuRestart()
{
	m_bIgnoreData = true;
	CHodgeThread::s_pThrWnd->PostMessage(WM_RESTART);
}
//*******************************************************************************************
void CHodgeWnd::StartThread()
{
	ResetEvent(CHodgeThread::s_hEvent);
	AfxBeginThread(RUNTIME_CLASS(CHodgeThread), THREAD_PRIORITY_IDLE);
	WaitForSingleObject(CHodgeThread::s_hEvent, INFINITE);
	CHodgeThread::s_pThrWnd->PostMessage(WM_SEND_HODGE_WND, (WPARAM)this);
}
//*******************************************************************************************
void CHodgeWnd::StopThread()
{
	ResetEvent(CHodgeThread::s_hEvent);
	CHodgeThread::s_pThrWnd->SendMessage(WM_STOP_THREAD);
	WaitForSingleObject(CHodgeThread::s_hEvent, INFINITE);
}
//*******************************************************************************************
LRESULT CHodgeWnd::OnFinished(WPARAM wParam, LPARAM)
{
	if (!m_bIgnoreData)
	{
		BYTE *pData = (BYTE *)wParam;
		CClientDC *pdc = new CClientDC(this);
		CDC memdc;
		memdc.CreateCompatibleDC(pdc);
		CBitmap *poldbmp = memdc.SelectObject(&m_bmpOff);
		int i, j;
		for (j = 0; j < m_nMaxY; j++)
			for (i = 0; i < m_nMaxX; i++)
				memdc.FillSolidRect(i, j, 1, 1, GenerateColor(pData, i, j));
		memdc.SelectObject(poldbmp);
		delete pdc;
		if (m_bVisible)
			InvalidateRect(NULL);
		else
		{
			m_bVisible = true;
			ShowWindow(SW_SHOW);
		}
		Compute();
	}
	return 0;
}
//*******************************************************************************************
void CHodgeWnd::Ignore()
{
	m_bIgnoreData = true;
	CHodgeThread::s_pThrWnd->PostMessage(WM_IGNORE);
}
//*******************************************************************************************
LRESULT CHodgeWnd::OnIgnore(WPARAM, LPARAM)
{
	m_bIgnoreData = false;
	Compute();
	return 0;
}
//*******************************************************************************************
void CHodgeWnd::NormalizeColour(COLORREF clr)
{
	int nRed = GetRValue(clr);
	int nGreen = GetGValue(clr);
	int nBlue = GetBValue(clr);
	int nMax = max(nRed, max(nGreen, nBlue));
	if (nMax == 0)
		nRed = nGreen = nBlue = nMax = 255;
	m_nNormRed = 255 * nRed / nMax;
	m_nNormGreen = 255 * nGreen / nMax;
	m_nNormBlue = 255 * nBlue / nMax;
}
//*******************************************************************************************
LRESULT CHodgeWnd::OnRestart(WPARAM wParam, LPARAM)
{
	m_bIgnoreData = false;
	return OnFinished(wParam, 0);
}
//*******************************************************************************************
