//*******************************************************************************************
#if !defined(AFX_PROPSDLG_H__ED38D035_FF83_11D4_A716_D9B7BC4D1864__INCLUDED_)
#define AFX_PROPSDLG_H__ED38D035_FF83_11D4_A716_D9B7BC4D1864__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
//*******************************************************************************************
class CPropsDlg : public CDialog
{
public:
	CPropsDlg(CWnd *pParent);

	//{{AFX_DATA(CPropsDlg)
	enum { IDD = IDD_PROPS };
	CStatic	m_ctrlColour;
	int		m_nHeight;
	int		m_nWidth;
	int		m_nDepth;
	int		m_nG;
	int		m_nK1;
	int		m_nK2;
	//}}AFX_DATA
	COLORREF m_clrBase;

	//{{AFX_VIRTUAL(CPropsDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

protected:
	//{{AFX_MSG(CPropsDlg)
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	virtual BOOL OnInitDialog();
	afx_msg void OnStaticColour();
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()

private:
	CBrush m_Brush;
};
//*******************************************************************************************
//{{AFX_INSERT_LOCATION}}

#endif // !defined(AFX_PROPSDLG_H__ED38D035_FF83_11D4_A716_D9B7BC4D1864__INCLUDED_)
