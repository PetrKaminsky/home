//*******************************************************************************************
#include "stdafx.h"
#include "Hodge.h"
#include "Thread.h"
//*******************************************************************************************
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif
//*******************************************************************************************
#define DEFAULT_MAXX 320
#define DEFAULT_MAXY 240
#define DEFAULT_MAXCOLOR 31
#define DEFAULT_CONST_K1 3
#define DEFAULT_CONST_K2 3
#define DEFAULT_CONST_G 6
#define DEFAULT_COLOUR RGB(255, 0, 0)
//*******************************************************************************************
BEGIN_MESSAGE_MAP(CHodgeApp, CWinApp)
	//{{AFX_MSG_MAP(CHodgeApp)
	//}}AFX_MSG
END_MESSAGE_MAP()
//*******************************************************************************************
CHodgeApp::CHodgeApp()
{
	m_pWnd = NULL;
}
//*******************************************************************************************
CHodgeApp g_App;
CParameters g_Params;
//*******************************************************************************************
BOOL CHodgeApp::InitInstance()
{
	SetPriorityClass(GetCurrentProcess(), IDLE_PRIORITY_CLASS);

#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif

	srand((UINT)time(NULL));
	CHodgeThread::s_hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);

	SetRegistryKey("Kamen");

	g_Params.Lock();
	g_Params.Load();
	int nMaxX = g_Params.GetMaxX();
	int nMaxY = g_Params.GetMaxY();
	m_pWnd = new CHodgeWnd(
		nMaxX,
		nMaxY,
		g_Params.GetMaxColor(),
		g_Params.GetColour());
	g_Params.Unlock();
	if (m_pWnd)
	{
		m_pWnd->CreateEx(0,
			AfxRegisterWndClass(CS_DBLCLKS, AfxGetApp()->LoadCursor(IDC_CURSOR)),
			"Hodge",
			WS_POPUP,
			CRect(0, 0, nMaxX, nMaxY),
			NULL,
			0);
		m_pMainWnd = m_pWnd;
		m_pWnd->CenterWindow();
	}

	return TRUE;
}
//*******************************************************************************************
int CHodgeApp::ExitInstance()
{
	if (m_pWnd)
		delete m_pWnd;
	m_pMainWnd = NULL;
	CloseHandle(CHodgeThread::s_hEvent);
	CHodgeThread::s_pThrWnd = NULL;;

	return CWinApp::ExitInstance();
}
//*******************************************************************************************
//*******************************************************************************************
//*******************************************************************************************
//*******************************************************************************************
LPCTSTR CParameters::s_szParamKeyName = "Parameters";
LPCTSTR CParameters::s_szWidthValueName = "Width";
LPCTSTR CParameters::s_szHeightValueName = "Height";
LPCTSTR CParameters::s_szDepthValueName = "Depth";
LPCTSTR CParameters::s_szK1ValueName = "K1";
LPCTSTR CParameters::s_szK2ValueName = "K2";
LPCTSTR CParameters::s_szGValueName = "G";
LPCTSTR CParameters::s_szColourValueName = "Colour";
//*******************************************************************************************
CParameters::CParameters()
{
	m_nMaxX = 0;
	m_nMaxY = 0;
	m_nMaxColor = 0;
	m_nConstK1 = 0;
	m_nConstK2 = 0;
	m_nConstG = 0;
	m_clrBase = RGB(0, 0, 0);
}
//*******************************************************************************************
int CParameters::GetMaxX()
{
	return m_nMaxX;
}
//*******************************************************************************************
int CParameters::GetMaxY()
{
	return m_nMaxY;
}
//*******************************************************************************************
int CParameters::GetMaxColor()
{
	return m_nMaxColor;
}
//*******************************************************************************************
int CParameters::GetConstK1()
{
	return m_nConstK1;
}
//*******************************************************************************************
int CParameters::GetConstK2()
{
	return m_nConstK2;
}
//*******************************************************************************************
int CParameters::GetConstG()
{
	return m_nConstG;
}
//*******************************************************************************************
COLORREF CParameters::GetColour()
{
	return m_clrBase;
}
//*******************************************************************************************
void CParameters::Lock()
{
	m_CritSect.Lock();
}
//*******************************************************************************************
void CParameters::Unlock()
{
	m_CritSect.Unlock();
}
//*******************************************************************************************
void CParameters::SetMaxX(int nMaxX)
{
	m_nMaxX = nMaxX;
}
//*******************************************************************************************
void CParameters::SetMaxY(int nMaxY)
{
	m_nMaxY = nMaxY;
}
//*******************************************************************************************
void CParameters::SetMaxColor(int nMaxColor)
{
	m_nMaxColor = nMaxColor;
}
//*******************************************************************************************
void CParameters::SetConstK1(int nConstK1)
{
	m_nConstK1 = nConstK1;
}
//*******************************************************************************************
void CParameters::SetConstK2(int nConstK2)
{
	m_nConstK2 = nConstK2;
}
//*******************************************************************************************
void CParameters::SetConstG(int nConstG)
{
	m_nConstG = nConstG;
}
//*******************************************************************************************
void CParameters::SetColour(COLORREF clrBase)
{
	m_clrBase = clrBase;
}
//*******************************************************************************************
void CParameters::Load()
{
	CWinApp *pApp = AfxGetApp();
	m_nMaxX = pApp->GetProfileInt(s_szParamKeyName, s_szWidthValueName, DEFAULT_MAXX);
	m_nMaxY = pApp->GetProfileInt(s_szParamKeyName, s_szHeightValueName, DEFAULT_MAXY);
	m_nMaxColor = pApp->GetProfileInt(s_szParamKeyName, s_szDepthValueName, DEFAULT_MAXCOLOR);
	m_nConstK1 = pApp->GetProfileInt(s_szParamKeyName, s_szK1ValueName, DEFAULT_CONST_K1);
	m_nConstK2 = pApp->GetProfileInt(s_szParamKeyName, s_szK2ValueName, DEFAULT_CONST_K2);
	m_nConstG = pApp->GetProfileInt(s_szParamKeyName, s_szGValueName, DEFAULT_CONST_G);
	m_clrBase = (COLORREF)pApp->GetProfileInt(s_szParamKeyName, s_szColourValueName, (int)DEFAULT_COLOUR);
}
//*******************************************************************************************
void CParameters::Save()
{
	CWinApp *pApp = AfxGetApp();
	pApp->WriteProfileInt(s_szParamKeyName, s_szWidthValueName, m_nMaxX);
	pApp->WriteProfileInt(s_szParamKeyName, s_szHeightValueName, m_nMaxY);
	pApp->WriteProfileInt(s_szParamKeyName, s_szDepthValueName, m_nMaxColor);
	pApp->WriteProfileInt(s_szParamKeyName, s_szK1ValueName, m_nConstK1);
	pApp->WriteProfileInt(s_szParamKeyName, s_szK2ValueName, m_nConstK2);
	pApp->WriteProfileInt(s_szParamKeyName, s_szGValueName, m_nConstG);
	pApp->WriteProfileInt(s_szParamKeyName, s_szColourValueName, (int)m_clrBase);
}
//*******************************************************************************************
