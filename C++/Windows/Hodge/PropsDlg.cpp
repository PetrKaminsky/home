//*******************************************************************************************
#include "stdafx.h"
#include "hodge.h"
#include "PropsDlg.h"
//*******************************************************************************************
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif
//*******************************************************************************************
CPropsDlg::CPropsDlg(CWnd *pParent)
	: CDialog(CPropsDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CPropsDlg)
	m_nHeight = 0;
	m_nWidth = 0;
	m_nDepth = 0;
	m_nG = 0;
	m_nK1 = 0;
	m_nK2 = 0;
	//}}AFX_DATA_INIT
	m_clrBase = RGB(0, 0, 0);
}
//*******************************************************************************************
void CPropsDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPropsDlg)
	DDX_Control(pDX, IDC_STATIC_COLOUR, m_ctrlColour);
	DDX_Text(pDX, IDC_EDIT_HEIGHT, m_nHeight);
	DDV_MinMaxInt(pDX, m_nHeight, 30, 1200);
	DDX_Text(pDX, IDC_EDIT_WIDTH, m_nWidth);
	DDV_MinMaxInt(pDX, m_nWidth, 40, 1600);
	DDX_Text(pDX, IDC_EDIT_DEPTH, m_nDepth);
	DDV_MinMaxInt(pDX, m_nDepth, 1, 255);
	DDX_Text(pDX, IDC_EDIT_G, m_nG);
	DDV_MinMaxInt(pDX, m_nG, 1, 16);
	DDX_Text(pDX, IDC_EDIT_K1, m_nK1);
	DDV_MinMaxInt(pDX, m_nK1, 1, 8);
	DDX_Text(pDX, IDC_EDIT_K2, m_nK2);
	DDV_MinMaxInt(pDX, m_nK2, 1, 8);
	//}}AFX_DATA_MAP
}
//*******************************************************************************************
BEGIN_MESSAGE_MAP(CPropsDlg, CDialog)
	//{{AFX_MSG_MAP(CPropsDlg)
	ON_WM_CTLCOLOR()
	ON_BN_CLICKED(IDC_STATIC_COLOUR, OnStaticColour)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()
//*******************************************************************************************
HBRUSH CPropsDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);

	if (pWnd->GetSafeHwnd() == m_ctrlColour.GetSafeHwnd())
		return m_Brush;

	return hbr;
}
//*******************************************************************************************
BOOL CPropsDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_Brush.CreateSolidBrush(m_clrBase);

	return TRUE;
}
//*******************************************************************************************
void CPropsDlg::OnStaticColour()
{
	CColorDialog cd(
		m_clrBase,
		CC_FULLOPEN | CC_RGBINIT | CC_ANYCOLOR,
		this);
	if (cd.DoModal() == IDOK)
	{
		m_clrBase = cd.GetColor();
		m_Brush.DeleteObject();
		m_Brush.CreateSolidBrush(m_clrBase);
		m_ctrlColour.Invalidate();
	}
}
//*******************************************************************************************
