//*******************************************************************************************
#if !defined(AFX_HODGEWND_H__882BA511_FDB4_11D4_AB74_0060083D8E3C__INCLUDED_)
#define AFX_HODGEWND_H__882BA511_FDB4_11D4_AB74_0060083D8E3C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
//*******************************************************************************************
class CHodgeWnd : public CWnd
{
public:
	CHodgeWnd(
		int nMaxX,
		int nMaxY,
		int nMaxColor,
		COLORREF clrBase);

	//{{AFX_VIRTUAL(CHodgeWnd)
	//}}AFX_VIRTUAL

protected:
	//{{AFX_MSG(CHodgeWnd)
	afx_msg void OnContextMenu(CWnd *pWnd, CPoint point);
	afx_msg void OnMenuQuit();
	afx_msg void OnMenuProps();
	afx_msg void OnMenuRestart();
	afx_msg BOOL OnEraseBkgnd(CDC *pDC);
	afx_msg void OnPaint();
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnDestroy();
	//}}AFX_MSG
	afx_msg LRESULT OnFinished(WPARAM wParam, LPARAM);
	afx_msg LRESULT OnIgnore(WPARAM, LPARAM);
	afx_msg LRESULT OnRestart(WPARAM wParam, LPARAM);

	DECLARE_MESSAGE_MAP()

private:
	void InitBitmap();
	void Compute();
	COLORREF GenerateColor(BYTE *pData, int x, int y);
	COLORREF Transform(int nVal);
	void StartThread();
	void StopThread();
	void Ignore();
	void NormalizeColour(COLORREF clr);

	CBitmap m_bmpOff;
	int m_nMaxX;
	int m_nMaxY;
	int m_nMaxColor;
	bool m_bVisible;
	bool m_bIgnoreData;
	COLORREF m_clrBase;
	BYTE m_nNormRed;
	BYTE m_nNormGreen;
	BYTE m_nNormBlue;
};
//*******************************************************************************************
//{{AFX_INSERT_LOCATION}}

#endif // !defined(AFX_HODGEWND_H__882BA511_FDB4_11D4_AB74_0060083D8E3C__INCLUDED_)
