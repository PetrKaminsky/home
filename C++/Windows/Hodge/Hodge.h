//*******************************************************************************************
#if !defined(AFX_HODGE_H__882BA505_FDB4_11D4_AB74_0060083D8E3C__INCLUDED_)
#define AFX_HODGE_H__882BA505_FDB4_11D4_AB74_0060083D8E3C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif
//*******************************************************************************************
#include "resource.h"		// main symbols
#include "HodgeWnd.h"
//*******************************************************************************************
#define WM_SEND_HODGE_WND (WM_USER + 1)
#define WM_STOP_THREAD (WM_USER + 2)
#define WM_COMPUTE (WM_USER + 3)
#define WM_FINISHED (WM_USER + 4)
#define WM_RESTART (WM_USER + 5)
#define WM_IGNORE (WM_USER + 6)
//*******************************************************************************************
class CHodgeApp : public CWinApp
{
public:
	CHodgeApp();

	//{{AFX_VIRTUAL(CHodgeApp)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	//}}AFX_VIRTUAL

	CHodgeWnd *m_pWnd;

	//{{AFX_MSG(CHodgeApp)
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};
//*******************************************************************************************
class CParameters
{
public:
	CParameters();

	int GetMaxX();
	int GetMaxY();
	int GetMaxColor();
	int GetConstK1();
	int GetConstK2();
	int GetConstG();
	COLORREF GetColour();
	void Lock();
	void Unlock();
	void SetMaxX(int nMaxX);
	void SetMaxY(int nMaxY);
	void SetMaxColor(int nMaxColor);
	void SetConstK1(int nConstK1);
	void SetConstK2(int nConstK2);
	void SetConstG(int nConstG);
	void SetColour(COLORREF clrBase);
	void Load();
	void Save();

private:
	int m_nMaxX;
	int m_nMaxY;
	int m_nMaxColor;
	int m_nConstK1;
	int m_nConstK2;
	int m_nConstG;
	COLORREF m_clrBase;
	CCriticalSection m_CritSect;
	static LPCTSTR s_szParamKeyName;
	static LPCTSTR s_szWidthValueName;
	static LPCTSTR s_szHeightValueName;
	static LPCTSTR s_szDepthValueName;
	static LPCTSTR s_szK1ValueName;
	static LPCTSTR s_szK2ValueName;
	static LPCTSTR s_szGValueName;
	static LPCTSTR s_szColourValueName;
};
//*******************************************************************************************
extern CParameters g_Params;
//*******************************************************************************************
//{{AFX_INSERT_LOCATION}}

#endif // !defined(AFX_HODGE_H__882BA505_FDB4_11D4_AB74_0060083D8E3C__INCLUDED_)
