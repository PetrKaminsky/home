package com.example.petr.spinnertest;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

/**
 * Created by petr on 4/10/17.
 */

public class DepotAdapter extends BaseAdapter {
    @Override
    public int getCount() {
        return DepotDataSource.getInstance().size();
    }

    @Override
    public Object getItem(int position) {
        return DepotDataSource.getInstance().get(position);
    }

    @Override
    public long getItemId(int position) {
        final Depot depot = (Depot) getItem(position);
        return depot.getDepId();
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        if (view == null) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.depot_spinner_layout, parent, false);
        }
        final TextView textViewDepCode = (TextView) view.findViewById(R.id.textViewDepCode);
        final TextView textViewDepName = (TextView) view.findViewById(R.id.textViewDepName);
        final Depot depot = (Depot) getItem(position);
        textViewDepCode.setText(depot.getDepCode());
        textViewDepName.setText(depot.getDepName());
        return view;
    }
}
