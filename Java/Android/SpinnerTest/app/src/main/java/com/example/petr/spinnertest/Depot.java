package com.example.petr.spinnertest;

/**
 * Created by petr on 4/10/17.
 */

public class Depot {
    private int depId;
    private String depCode;
    private String depName;

    public Depot(int id, String code, String name) {
        this.depId = id;
        this.depCode = code;
        this.depName = name;
    }

    public int getDepId() {
        return depId;
    }

    public void setDepId(int id) {
        depId = id;
    }

    public String getDepCode() {
        return depCode;
    }

    public void setDepCode(String code) {
        depCode = code;
    }

    public String getDepName() {
        return depName;
    }

    public void setDepName(String name) {
        depName = name;
    }
}
