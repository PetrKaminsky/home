package com.example.petr.spinnertest;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;

public class MainActivity extends Activity {
    private Spinner spinnerDepot = null;
    private DepotAdapter adapter = null;
    private View.OnClickListener onButtonRun =
            new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d("main", "buttonRun.onClick()");

                    int position = spinnerDepot.getSelectedItemPosition();
                    if (position != Spinner.INVALID_POSITION) {
                        Depot depot = DepotDataSource.getInstance().get(position);

                        Log.d("main", "DepId: " + depot.getDepId());
                    }
                }
            };
    private AdapterView.OnItemSelectedListener onSelectedDepot =
            new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    Log.d("main", "onItemSelected(), position: " + position);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                    Log.d("main", "onNothingSelected()");
                }
            };
    private View.OnClickListener onButtonFill =
            new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d("main", "buttonFill.onClick()");

                    DepotDataSource.getInstance().fill();
                    adapter.notifyDataSetChanged();
                }
            };
    private View.OnClickListener onButtonEmpty =
            new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d("main", "buttonEmpty.onClick()");

                    DepotDataSource.getInstance().clear();
                    adapter.notifyDataSetChanged();
                }
            };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        spinnerDepot = (Spinner) findViewById(R.id.spinnerDepot);
        spinnerDepot.setOnItemSelectedListener(onSelectedDepot);
        adapter = new DepotAdapter();
        spinnerDepot.setAdapter(adapter);
        findViewById(R.id.buttonRun).setOnClickListener(onButtonRun);
        findViewById(R.id.buttonFill).setOnClickListener(onButtonFill);
        findViewById(R.id.buttonEmpty).setOnClickListener(onButtonEmpty);
    }
}
