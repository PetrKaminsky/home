package com.example.petr.spinnertest;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by petr on 4/10/17.
 */

public class DepotDataSource extends ArrayList<Depot> {
    private static DepotDataSource instance = new DepotDataSource();

    private DepotDataSource() {
    }

    public static DepotDataSource getInstance() {
        return instance;
    }

    @SuppressWarnings("SpellCheckingInspection")
    public void fill() {
        clear();
        addAll(Arrays.asList(
                new Depot(10, "EJP", "Ejpovice"),
                new Depot(20, "MOD", "Modletice"),
                new Depot(30, "OVA", "Ostrava"),
                new Depot(40, "ZVO", "Zvolen"),
                new Depot(50, "PRE", "Presov")
        ));
    }
}
