package com.example.petrkaminsky.sygicembedtest;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.sygic.aura.embedded.IApiCallback;
import com.sygic.aura.embedded.SygicFragmentSupportV4;
import com.sygic.sdk.api.events.ApiEvents;

/**
 * Created by petr.kaminsky on 8.1.2018.
 */

public class SygicNaviFragment extends SygicFragmentSupportV4 {

    private boolean navigationStarted = false;

    public boolean isNavigationStarted() {
        return navigationStarted;
    }

    @Override
    public void onResume() {
        super.onResume();

        startNavi();
        setCallback(new IApiCallback() {
            @Override
            public void onEvent(int i, String s) {
                Log.d("fragment", "IApiCallback.onEvent(): " + i + ", " + s);

                switch (i) {
                    case ApiEvents.EVENT_APP_STARTED:
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                navigationStarted = true;
                            }
                        });
                        break;

                    case ApiEvents.EVENT_APP_EXIT:
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                getActivity().finish();
                            }
                        });
                        break;
                }
            }

            @Override
            public void onServiceConnected() {
                Log.d("fragment", "IApiCallback.onServiceConnected()");
            }

            @Override
            public void onServiceDisconnected() {
                Log.d("fragment", "IApiCallback.onServiceDisconnected()");
            }
        });
    }
}
