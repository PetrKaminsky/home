package com.example.petrkaminsky.sygicembedtest;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.sygic.aura.utils.PermissionsUtils;
import com.sygic.sdk.api.ApiLocation;
import com.sygic.sdk.api.ApiMaps;
import com.sygic.sdk.api.ApiNavigation;
import com.sygic.sdk.api.exception.GeneralException;
import com.sygic.sdk.api.exception.NavigationException;
import com.sygic.sdk.api.model.Position;
import com.sygic.sdk.api.model.WayPoint;

import static android.content.pm.PackageManager.PERMISSION_GRANTED;

public class MainActivity extends AppCompatActivity {

    private SygicNaviFragment fragment = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        final EditText editTextAddress = (EditText) findViewById(R.id.editTextAddress);
        findViewById(R.id.buttonNavigate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String address = editTextAddress.getText().toString();
                if (!address.equals("")) {
                    navigateTo(address);
                }
            }
        });
        findViewById(R.id.buttonShowOnMap).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String address = editTextAddress.getText().toString();
                if (!address.equals("")) {
                    showOnMap(address);
                }
            }
        });

        if (PermissionsUtils.requestStartupPermissions(this) == PERMISSION_GRANTED) {
            initUI();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        for (int result : grantResults) {
            if (result != PERMISSION_GRANTED) {
                return;
            }
        }

        initUI();
    }

    private void initUI() {
        fragment = new SygicNaviFragment();

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragmentHolder, fragment)
                .commit();
    }

    private void navigateTo(String address) {
        if (fragment != null &&
                fragment.isNavigationStarted()) {
            try {
                boolean postal = false;
                boolean fuzzyMatch = true;
                Position pos = ApiLocation.locationFromAddress(address, postal, fuzzyMatch, 0);
                int flags = 0;
                boolean searchAddress = false;
                WayPoint wp = new WayPoint();
                wp.SetLocation(pos.getX(), pos.getY());
                ApiNavigation.startNavigation(wp, flags, searchAddress, 0);
            } catch (GeneralException ex) {
                Log.e("activity", "navigateTo(): " + address, ex);
            }
        }
    }

    private void showOnMap(String address) {
        if (fragment != null &&
                fragment.isNavigationStarted()) {
            try {
                boolean postal = false;
                boolean fuzzyMatch = true;
                Position pos = ApiLocation.locationFromAddress(address, postal, fuzzyMatch, 0);
                ApiMaps.showCoordinatesOnMap(pos, 0, 0);
            }catch (GeneralException ex) {
                Log.e("activity", "showOnMap(): " + address, ex);
            }
        }
    }
}
