package com.example.petrkaminsky.androidlibrary;

import android.util.Log;

/**
 * Created by petr.kaminsky on 31.1.2018.
 */

public abstract class AarCallback implements AarInterface {
    protected AarCallback() {
        Log.d("trace", "AarCallback.AarCallback()");
    }

    public abstract void onResult();

    @Override
    public void method() {
        Log.d("trace", "AarCallback.method()");

        onResult();
    }
}
