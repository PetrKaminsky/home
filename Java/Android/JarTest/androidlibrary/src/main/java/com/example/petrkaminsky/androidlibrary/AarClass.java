package com.example.petrkaminsky.androidlibrary;

import android.util.Log;

/**
 * Created by petr.kaminsky on 31.1.2018.
 */

public class AarClass {
    public AarClass() {
        Log.d("trace", "AarClass.AarClass()");
    }

    public static void method1() {
        Log.d("trace", "AarClass.method1()");

        new AarClass().method3();
    }

    public boolean method2(AarCallback c) {
        Log.d("trace", "AarClass.method2()");

        c.onResult();

        return false;
    }

    private int method3() {
        Log.d("trace", "AarClass.method3()");

        return 0;
    }
}
