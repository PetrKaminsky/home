package com.example.petrkaminsky.jartest;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.example.petrkaminsky.androidlibrary.AarCallback;
import com.example.petrkaminsky.androidlibrary.AarClass;
import com.example.petrkaminsky.javalibrary.JarCallback;
import com.example.petrkaminsky.javalibrary.JarClass;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        AarClass aarClass = new AarClass();
        AarClass.method1();
        aarClass.method2(new AarCallback() {
            @Override
            public void onResult() {
                Log.d("trace", "MainActivity.AarCallback.onResult()");
            }
        });

        JarClass jarClass = new JarClass();
        JarClass.method1();
        jarClass.method2(new JarCallback() {
            @Override
            public void onResult() {
                Log.d("trace", "MainActivity.JarCallback.onResult()");
            }
        });
    }
}
