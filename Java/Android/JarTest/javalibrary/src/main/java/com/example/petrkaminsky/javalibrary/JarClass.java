package com.example.petrkaminsky.javalibrary;

public class JarClass {
    public JarClass() {
    }

    public static void method1() {
        new JarClass().method3();
    }

    public boolean method2(JarCallback c) {
        c.onResult();

        return false;
    }

    private int method3() {
        return 0;
    }
}
