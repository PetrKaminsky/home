package com.example.petrkaminsky.javalibrary;

/**
 * Created by petr.kaminsky on 30.1.2018.
 */

abstract class JarAbstractClass {
    abstract void method();
}
