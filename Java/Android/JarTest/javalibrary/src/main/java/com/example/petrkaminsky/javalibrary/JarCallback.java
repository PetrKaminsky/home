package com.example.petrkaminsky.javalibrary;

/**
 * Created by petr.kaminsky on 30.1.2018.
 */

public abstract class JarCallback extends JarAbstractClass {
    protected JarCallback() {
    }

    public abstract void onResult();

    void method() {
        onResult();
    }
}
