package com.example.petrkaminsky.cameraapitest;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Surface;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;

import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

import static android.hardware.Camera.CameraInfo.CAMERA_FACING_BACK;

public class MainActivity extends Activity {

    private CameraPreview preview = null;
    private Camera camera = null;
    private int numberOfCameras = 0;
    private String imageFileName = null;
    private ParcelFileDescriptor imageFileDescriptor = null;
    private byte[] pictureBytes = null;
    private PreviewState previewState = PreviewState.RUNNING;
    private Button buttonCaptureImage = null;
    private Button buttonSaveImage = null;
    private Button buttonOtherImage = null;
    private boolean takingPicture = false;
    // The first back facing camera
    private int defaultCameraId = -1;
    private View.OnClickListener onButtonSaveImageClick =
            new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    retrieveImageFile();

                    if (pictureBytes != null) {
                        if (imageFileDescriptor != null) {
                            saveImageFile();
                        } else if (imageFileName != null && !imageFileName.isEmpty()) {
                            if (getWriteStoragePermission().isPermissionGranted()) {
                                saveImageFile();
                            } else {
                                // will handle when permission result available
                                return;
                            }
                        }
                    }

                    setResult(RESULT_OK);
                    finish();
                }
            };
    private View.OnClickListener onButtonOtherImageClick =
            new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    pictureBytes = null;
                    camera.startPreview();

                    previewState = PreviewState.RUNNING;
                    updateButtons();
                }
            };
    private Camera.PictureCallback pictureCallback =
            new Camera.PictureCallback() {
                @Override
                public void onPictureTaken(byte[] data, Camera camera) {
                    takingPicture = false;
                    camera.stopPreview();
                    pictureBytes = data;

                    previewState = PreviewState.PICTURE;
                    updateButtons();
                }
            };
    private Camera.ShutterCallback shutterCallback =
            new Camera.ShutterCallback() {
                @Override
                public void onShutter() {
                }
            };
    private View.OnClickListener onButtonCaptureImageClick =
            new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!takingPicture &&
                            camera != null) {
                        takingPicture = true;
                        camera.takePicture(shutterCallback, null, pictureCallback);
                    }
                }
            };
    private PermissionObject.Listener cameraPermissionListener =
            new PermissionObject.Listener() {
                @Override
                public void onPermissionGranted() {
                    Log.d("permission", "camera/onPermissionGranted()");

                    invalidateOptionsMenu();
                }

                @Override
                public void onPermissionDenied() {
                    Log.d("permission", "camera/onPermissionDenied()");

                    numberOfCameras = 0;
                    defaultCameraId = -1;
                    invalidateOptionsMenu();
                }
            };
    private PermissionObject.Listener writeStoragePermissionListener =
            new PermissionObject.Listener() {
                @Override
                public void onPermissionGranted() {
                    Log.d("permission", "writeStorage/onPermissionGranted()");

                    saveImageFile();

                    setResult(RESULT_OK);
                    finish();
                }

                @Override
                public void onPermissionDenied() {
                    Log.d("permission", "writeStorage/onPermissionDenied()");

                    setResult(RESULT_CANCELED);
                    finish();
                }
            };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.d("life", "onCreate()");

        setContentView(R.layout.activity_main);

        preview = new CameraPreview(this);
        final FrameLayout framePreview = (FrameLayout) findViewById(R.id.framePreview);
        framePreview.addView(preview);
        buttonCaptureImage = (Button) findViewById(R.id.buttonCaptureImage);
        buttonCaptureImage.setOnClickListener(onButtonCaptureImageClick);
        buttonSaveImage = (Button) findViewById(R.id.buttonSaveImage);
        buttonSaveImage.setOnClickListener(onButtonSaveImageClick);
        buttonOtherImage = (Button) findViewById(R.id.buttonOtherImage);
        buttonOtherImage.setOnClickListener(onButtonOtherImageClick);

        getCameraPermission().setActivity(this);
        getCameraPermission().setListener(cameraPermissionListener);
        getWriteStoragePermission().setActivity(this);
        getWriteStoragePermission().setListener(writeStoragePermissionListener);
    }

    @Override
    protected void onResume() {
        super.onResume();

        Log.d("life", "onResume()");

        if (getCameraPermission().isPermissionGranted()) {
            initializeCameraPreview();
        }

        if (camera != null) {
            preview.switchCamera(camera);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        Log.d("life", "onPause()");

        // Because the Camera object is a shared resource, it's very
        // important to release it when the activity is paused.
        if (camera != null) {
            preview.setPreviewCamera(null);
            camera.release();
            camera = null;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!getCameraPermission().isPermissionGranted()) {
            return super.onCreateOptionsMenu(menu);
        }

        getMenuInflater().inflate(R.menu.camera_menu, menu);
        createFlashModeMenu(menu);
        createFocusModeMenu(menu);

        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (!getCameraPermission().isPermissionGranted()) {
            return super.onPrepareOptionsMenu(menu);
        }

        // check for availability of multiple cameras
        menu.findItem(R.id.switch_camera).setVisible(numberOfCameras > 1);
        prepareFlashModeMenu(menu);
        prepareFocusModeMenu(menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (!getCameraPermission().isPermissionGranted()) {
            return super.onOptionsItemSelected(item);
        }

        if (handleFlashModeMenu(item) || handleFocusModeMenu(item)) {
            return true;
        }

        // Handle item selection
        switch (item.getItemId()) {
            case R.id.switch_camera:
                // Release this camera -> cameraCurrentlyLocked
                if (camera != null) {
                    camera.stopPreview();
                    preview.setPreviewCamera(null);
                    camera.release();
                    camera = null;
                }

                // Acquire the next camera and request Preview to reconfigure
                // parameters.
                int newCameraId = (getCameraCurrentlyLocked() + 1) % numberOfCameras;
                camera = Camera.open(newCameraId);
                setCameraCurrentlyLocked(newCameraId);
                invalidateOptionsMenu();
                setCameraOrientation();
                preview.switchCamera(camera);
                previewState = PreviewState.RUNNING;
                updateButtons();
                return true;

            case android.R.id.home:
                setResult(RESULT_CANCELED);
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        if (!getCameraPermission().processRequestPermissionsResult(
                requestCode, permissions, grantResults)) {
            getWriteStoragePermission().processRequestPermissionsResult(
                    requestCode, permissions, grantResults);
        }
    }

    private PermissionObject getCameraPermission() {
        return MainActivityData.getInstance().getCameraPermission();
    }

    private PermissionObject getWriteStoragePermission() {
        return MainActivityData.getInstance().getWriteStoragePermission();
    }

    private void saveImageFile() {
        Bitmap bitmapOrig = BitmapFactory.decodeByteArray(pictureBytes, 0, pictureBytes.length);
        int width = bitmapOrig.getWidth();
        int height = bitmapOrig.getHeight();
        Matrix matrix = new Matrix();
        matrix.postRotate(getPictureOrientationDegrees());
        Bitmap bitmapRotated = Bitmap.createBitmap(bitmapOrig, 0, 0, width, height, matrix, true);
        try {
            OutputStream fs = null;
            if (imageFileDescriptor != null) {
                FileDescriptor fileDescriptor = imageFileDescriptor.getFileDescriptor();
                fs = new FileOutputStream(fileDescriptor);
            } else {
                fs = new FileOutputStream(imageFileName);
            }
            bitmapRotated.compress(Bitmap.CompressFormat.JPEG, 100, fs);
            fs.flush();
            fs.close();
            if (imageFileDescriptor != null) {
                imageFileDescriptor.close();
            }
        } catch (IOException e) {
            Log.e("MainActivity", "saveImageFile/FileOutputStream", e);
        }
        bitmapOrig.recycle();
        bitmapRotated.recycle();
    }

    private void initializeCameraPreview() {
        inspectCameras();

        if (getCameraCurrentlyLocked() == -1) {
            setCameraCurrentlyLocked(defaultCameraId);
        }
        camera = Camera.open(getCameraCurrentlyLocked());
        invalidateOptionsMenu();
        setCameraOrientation();
        preview.setPreviewCamera(camera);
        previewState = PreviewState.RUNNING;
        updateButtons();
    }

    private boolean handleFlashModeMenu(MenuItem item) {
        if (item.getGroupId() == R.id.flash_mode_group) {
            if (!item.isChecked()) {
                item.setChecked(true);
                String flashMode = item.getTitle().toString();
                Camera.Parameters parameters = camera.getParameters();
                parameters.setFlashMode(flashMode);
                camera.setParameters(parameters);
            }

            return true;
        }

        return false;
    }

    private boolean handleFocusModeMenu(MenuItem item) {
        if (item.getGroupId() == R.id.focus_mode_group) {
            if (!item.isChecked()) {
                item.setChecked(true);
                String focusMode = item.getTitle().toString();
                Camera.Parameters parameters = camera.getParameters();
                parameters.setFocusMode(focusMode);
                camera.setParameters(parameters);
            }

            return true;
        }

        return false;
    }

    public int getCameraCurrentlyLocked() {
        return MainActivityData.getInstance().getCameraCurrentlyLocked();
    }

    public void setCameraCurrentlyLocked(int cameraId) {
        MainActivityData.getInstance().setCameraCurrentlyLocked(cameraId);
    }

    private void prepareFlashModeMenu(Menu menu) {
        if (camera != null) {
            MenuItem flashMenu = menu.findItem(R.id.flash_mode);
            List<String> supportedFlashModes = camera.getParameters().getSupportedFlashModes();
            if (supportedFlashModes != null) {
                String flashMode = camera.getParameters().getFlashMode();

                int size = flashMenu.getSubMenu().size();
                for (int i = 0; i < size; i++) {
                    MenuItem subMenuItem = flashMenu.getSubMenu().getItem(i);
                    String subMenuItemTitle = subMenuItem.getTitle().toString();
                    if (flashMode.equals(subMenuItemTitle)) {
                        subMenuItem.setChecked(true);
                        break;
                    }
                }
            }
        }
    }

    private void prepareFocusModeMenu(Menu menu) {
        if (camera != null) {
            MenuItem focusMenu = menu.findItem(R.id.focus_mode);
            List<String> supportedFocusModes = camera.getParameters().getSupportedFocusModes();
            if (supportedFocusModes != null) {
                String focusMode = camera.getParameters().getFocusMode();

                int size = focusMenu.getSubMenu().size();
                for (int i = 0; i < size; i++) {
                    MenuItem subMenuItem = focusMenu.getSubMenu().getItem(i);
                    String subMenuItemTitle = subMenuItem.getTitle().toString();
                    if (focusMode.equals(subMenuItemTitle)) {
                        subMenuItem.setChecked(true);
                        break;
                    }
                }
            }
        }
    }

    private void createFlashModeMenu(Menu menu) {
        if (camera != null) {
            MenuItem flashMenu = menu.findItem(R.id.flash_mode);
            List<String> supportedFlashModes = camera.getParameters().getSupportedFlashModes();
            if (supportedFlashModes != null) {
                for (String mode : supportedFlashModes) {
                    flashMenu.getSubMenu().add(R.id.flash_mode_group, Menu.NONE, Menu.NONE, mode);
                }
                flashMenu.getSubMenu().setGroupCheckable(R.id.flash_mode_group, true, true);
            } else {
                flashMenu.setVisible(false);
            }
        }
    }

    private void createFocusModeMenu(Menu menu) {
        if (camera != null) {
            MenuItem focusMenu = menu.findItem(R.id.focus_mode);
            List<String> supportedFocusModes = camera.getParameters().getSupportedFocusModes();
            if (supportedFocusModes != null) {
                for (String mode : supportedFocusModes) {
                    focusMenu.getSubMenu().add(R.id.focus_mode_group, Menu.NONE, Menu.NONE, mode);
                }
                focusMenu.getSubMenu().setGroupCheckable(R.id.focus_mode_group, true, true);
            } else {
                focusMenu.setVisible(false);
            }
        }
    }

    private void setCameraOrientation() {
        camera.setDisplayOrientation(getCameraOrientationDegrees());
    }

    private void inspectCameras() {
        if (numberOfCameras == 0 && defaultCameraId == -1) {
            // Find the total number of cameras available
            numberOfCameras = Camera.getNumberOfCameras();

            // Find the ID of the default camera
            Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
            for (int i = 0; i < numberOfCameras; i++) {
                Camera.getCameraInfo(i, cameraInfo);
                if (cameraInfo.facing == CAMERA_FACING_BACK) {
                    defaultCameraId = i;

                    Log.d("camera", "Default camera Id: " + defaultCameraId);
                }
            }
        }
    }

    private void retrieveImageFile() {
        Intent intent = getIntent();
        if (intent != null && intent.hasExtra(MediaStore.EXTRA_OUTPUT)) {
            Uri uri = intent.getParcelableExtra(MediaStore.EXTRA_OUTPUT);
            if (uri.getScheme().equals("file")) {
                imageFileName = uri.getEncodedPath();
            } else {
                try {
                    imageFileDescriptor = getContentResolver().openFileDescriptor(uri, "rwt");
                } catch (FileNotFoundException e) {
                    Log.e("MainActivity", "retrieveImageFileDescriptor/ParcelFileDescriptor", e);
                }
            }
        }
    }

    private int getCameraCompensationDegrees() {
        int degrees = 0;
        switch (getWindowManager().getDefaultDisplay().getRotation()) {
            case Surface.ROTATION_0:
                degrees = 0;
                break;

            case Surface.ROTATION_90:
                degrees = 90;
                break;

            case Surface.ROTATION_180:
                degrees = 180;
                break;

            case Surface.ROTATION_270:
                degrees = 270;
                break;
        }

        return degrees;
    }

    private int getCameraOrientationDegrees() {
        int result;
        int degrees = getCameraCompensationDegrees();
        Camera.CameraInfo info = new Camera.CameraInfo();
        Camera.getCameraInfo(getCameraCurrentlyLocked(), info);
        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            result = (info.orientation + degrees) % 360;
            result = (360 - result) % 360;  // compensate the mirror
        } else {  // back-facing
            result = (info.orientation - degrees + 360) % 360;
        }

        return result;
    }

    private int getPictureOrientationDegrees() {
        int result;
        int degrees = getCameraCompensationDegrees();
        Camera.CameraInfo info = new Camera.CameraInfo();
        Camera.getCameraInfo(getCameraCurrentlyLocked(), info);
        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            result = (degrees + 360 - 90) % 360;
        } else {
            result = (360 + 90 - degrees) % 360;
        }

        return result;
    }

    private void updateButtons() {
        setButtonVisibility(buttonCaptureImage, previewState == PreviewState.RUNNING);
        setButtonVisibility(buttonSaveImage, previewState == PreviewState.PICTURE);
        setButtonVisibility(buttonOtherImage, previewState == PreviewState.PICTURE);
    }

    private void setButtonVisibility(Button button, boolean visible) {
        button.setVisibility(visible ? View.VISIBLE : View.GONE);
    }

    private enum PreviewState {
        RUNNING,
        PICTURE
    }

    private static class MainActivityData {
        private static MainActivityData instance = new MainActivityData();
        private int cameraCurrentlyLocked = -1;
        private PermissionObject cameraPermission =
                new PermissionObject(Manifest.permission.CAMERA);
        private PermissionObject writeStoragePermission =
                new PermissionObject(Manifest.permission.WRITE_EXTERNAL_STORAGE);

        private MainActivityData() {
        }

        public static MainActivityData getInstance() {
            return instance;
        }

        public PermissionObject getCameraPermission() {
            return cameraPermission;
        }

        public PermissionObject getWriteStoragePermission() {
            return writeStoragePermission;
        }

        public int getCameraCurrentlyLocked() {
            return cameraCurrentlyLocked;
        }

        public void setCameraCurrentlyLocked(int cameraId) {
            cameraCurrentlyLocked = cameraId;
        }
    }
}
