package com.example.petrkaminsky.cameraapitest;

import android.annotation.TargetApi;
import android.app.Activity;
import android.os.Build;
import android.util.Log;

import static android.content.pm.PackageManager.PERMISSION_GRANTED;

/**
 * Created by petr on 5/7/17.
 */

public class PermissionObject {

    private static int REQUEST_COUNTER = 1;
    private int requestCode = -1;
    private Listener listener = null;
    private Activity activity = null;
    private String permission = null;
    private boolean requestedPermission = false;

    public PermissionObject(String permission) {
        this.permission = permission;
        this.requestCode = REQUEST_COUNTER;
        REQUEST_COUNTER++;
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    public boolean isPermissionGranted() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            boolean retVal = checkPermission();
            if (!retVal) {
                requestPermission();
            }

            return retVal;
        }

        return true;
    }

    @TargetApi(23)
    private boolean checkPermission() {
        Log.d("permission", "checkPermission(): " + permission);

        if (activity != null) {
            return activity.checkSelfPermission(permission) == PERMISSION_GRANTED;
        }

        return false;
    }

    @TargetApi(23)
    private void requestPermission() {
        Log.d("permission", "requestPermission(): " + permission);

        if (!requestedPermission) {
            if (activity != null) {
                requestedPermission = true;

                activity.requestPermissions(new String[]{permission}, requestCode);
            }
        }
    }

    public boolean processRequestPermissionsResult(int requestCode,
                                                   String permissions[], int[] grantResults) {
        Log.d("permission", "processRequestPermissionsResult(): " + permission + ", " + requestCode);

        boolean retVal = false;
        if (requestCode == this.requestCode) {
            if (permissions.length > 0
                    && grantResults.length > 0) {
                if (permissions[0].equals(permission)) {
                    retVal = true;
                    requestedPermission = false;
                    if (grantResults[0] == PERMISSION_GRANTED) {
                        if (listener != null) {
                            listener.onPermissionGranted();
                        }
                    } else {
                        if (listener != null) {
                            listener.onPermissionDenied();
                        }
                    }
                }
            }
        }

        return retVal;
    }

    public interface Listener {
        void onPermissionGranted();

        void onPermissionDenied();
    }
}
