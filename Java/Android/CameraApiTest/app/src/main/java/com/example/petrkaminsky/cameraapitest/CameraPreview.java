package com.example.petrkaminsky.cameraapitest;

import android.content.Context;
import android.hardware.Camera;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.io.IOException;

/**
 * Created by petr on 5/8/17.
 */

public class CameraPreview extends SurfaceView implements SurfaceHolder.Callback {

    private Camera previewCamera = null;
    private boolean surfaceAvailable = false;

    public CameraPreview(Context context) {
        super(context);

        getHolder().addCallback(this);
    }

    public Camera getPreviewCamera() {
        return previewCamera;
    }

    public void setPreviewCamera(Camera previewCamera) {
        this.previewCamera = previewCamera;
    }

    public void switchCamera(Camera camera) {
        setPreviewCamera(camera);
        if (surfaceAvailable) {
            try {
                camera.setPreviewDisplay(getHolder());
                camera.startPreview();
            } catch (IOException e) {
                Log.e("CameraPreview", "switchCamera/setPreviewDisplay", e);
            }
        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        surfaceAvailable = true;
        try {
            Camera camera = getPreviewCamera();
            if (camera != null) {
                camera.setPreviewDisplay(holder);
                camera.startPreview();
            }
        } catch (IOException e) {
            Log.e("CameraPreview", "surfaceCreated/setPreviewDisplay", e);
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        surfaceAvailable = false;
        getHolder().removeCallback(this);
        Camera camera = getPreviewCamera();
        if (camera != null) {
            camera.stopPreview();
        }
    }
}
