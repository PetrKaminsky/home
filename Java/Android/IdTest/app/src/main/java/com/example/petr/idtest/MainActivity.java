package com.example.petr.idtest;

import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.widget.TextView;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        final TextView textViewBuildSerial = (TextView) findViewById(R.id.textViewBuildSerial);
        textViewBuildSerial.setText(getBuildSerial());
        final TextView textViewAndroidId = (TextView) findViewById(R.id.textViewAndroidId);
        textViewAndroidId.setText(getAndroidId());
    }

    private String getBuildSerial() {
        return Build.SERIAL;
    }

    private String getAndroidId() {
        return Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
    }
}
