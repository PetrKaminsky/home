package com.example.petrkaminsky.grayscale;

/**
 * Created by petr.kaminsky on 24.10.2017.
 */

public enum Orientation {
    Horizontal,
    Vertical
}
