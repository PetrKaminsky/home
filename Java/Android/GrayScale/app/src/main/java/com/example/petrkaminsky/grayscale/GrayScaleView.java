package com.example.petrkaminsky.grayscale;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by petr on 10/29/17.
 */

public class GrayScaleView extends View {

    private Paint paint = new Paint();
    private boolean inEditMode = false;

    public GrayScaleView(Context context) {
        super(context);

        initialize();
    }

    public GrayScaleView(Context context, AttributeSet attrs) {
        super(context, attrs);

        initialize();
    }

    public GrayScaleView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        initialize();
    }

    private static int getNormalizedColorValue(int value, int maxValue) {
        return 255 * value / maxValue;
    }

    private void initialize() {
        inEditMode = isInEditMode();
        paint.setDither(true);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if (inEditMode) {
            return;
        }

        canvas.drawColor(Color.BLACK);

        int width = getWidth();
        int height = getHeight();
        switch (Properties.getInstance().getOrientation()) {
            case Horizontal:
                for (int i = 0; i < width; i++) {
                    int normalizedValue = getNormalizedColorValue(i, width);
                    int color = Color.rgb(
                            normalizedValue,
                            normalizedValue,
                            normalizedValue);
                    paint.setColor(color);
                    canvas.drawRect(i, 0, i + 1, height, paint);
                }
                break;

            case Vertical:
                for (int i = 0; i < height; i++) {
                    int normalizedValue = getNormalizedColorValue(i, height);
                    int color = Color.rgb(
                            normalizedValue,
                            normalizedValue,
                            normalizedValue);
                    paint.setColor(color);
                    canvas.drawRect(0, i, width, i + 1, paint);
                }
                break;
        }
    }
}
