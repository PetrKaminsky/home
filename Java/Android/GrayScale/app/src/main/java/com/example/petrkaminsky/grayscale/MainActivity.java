package com.example.petrkaminsky.grayscale;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class MainActivity extends Activity {

    private GrayScaleView grayScaleView = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.i("life", "MainActivity.onCreate()");

        setContentView(R.layout.activity_main);
        grayScaleView = (GrayScaleView) findViewById(R.id.grayScaleView);
        grayScaleView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Properties.getInstance().toggleOrientation();
                grayScaleView.invalidate();

                return true;
            }
        });
    }
}
