package com.example.petrkaminsky.grayscale;

/**
 * Created by petr.kaminsky on 24.10.2017.
 */

public class Properties {
    private static final Properties instance = new Properties();
    private Orientation orientation = Orientation.Horizontal;

    private Properties() {
    }

    public static Properties getInstance() {
        return instance;
    }

    public Orientation getOrientation() {
        return orientation;
    }

    public void toggleOrientation() {
        switch (orientation) {
            case Horizontal:
                orientation = Orientation.Vertical;
                break;

            case Vertical:
                orientation = Orientation.Horizontal;
                break;
        }
    }
}
