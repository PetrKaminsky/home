package com.example.petrkaminsky.messageboxtest;

import android.app.Activity;
import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by petr.kaminsky on 15.9.2017.
 */

public class MessageDialog extends DialogFragment {

    private Button buttonOk = null;
    private Button buttonYes = null;
    private Button buttonNo = null;
    private Button buttonCancel = null;

    private static boolean stringIsNullOrEmpty(String s) {
        return s == null || s.length() == 0;
    }

    public static void showMessageDialog(
            Activity activity,
            MessageStyle messageStyle,
            String title,
            String message,
            ButtonClick onOkClick,
            ButtonClick onYesClick,
            ButtonClick onNoClick,
            ButtonClick onCancelClick) {
        MessageDialog messageDialog = new MessageDialog();
        messageDialog.setMessageStyle(messageStyle);
        messageDialog.setTitle(title);
        messageDialog.setMessage(message);
        messageDialog.setOnOkClick(onOkClick);
        messageDialog.setOnYesClick(onYesClick);
        messageDialog.setOnNoClick(onNoClick);
        messageDialog.setOnCancelClick(onCancelClick);
        messageDialog.show(activity.getFragmentManager(), "");
    }

    public static void showMessageOk(
            Activity activity,
            String title,
            String message,
            ButtonClick onOkClick) {
        showMessageDialog(
                activity,
                MessageStyle.OK,
                title,
                message,
                onOkClick,
                null,
                null,
                null);
    }

    public static void showMessageOk(
            Activity activity,
            String title,
            String message) {
        showMessageOk(
                activity,
                title,
                message,
                null);
    }

    public static void showMessageOkCancel(
            Activity activity,
            String title,
            String message,
            ButtonClick onOkClick,
            ButtonClick onCancelClick) {
        showMessageDialog(
                activity,
                MessageStyle.OK_CANCEL,
                title,
                message,
                onOkClick,
                null,
                null,
                onCancelClick);
    }

    public static void showMessageYesNo(
            Activity activity,
            String title,
            String message,
            ButtonClick onYesClick,
            ButtonClick onNoClick) {
        showMessageDialog(
                activity,
                MessageStyle.YES_NO,
                title,
                message,
                null,
                onYesClick,
                onNoClick,
                null);
    }

    public static void showMessageYesNoCancel(
            Activity activity,
            String title,
            String message,
            ButtonClick onYesClick,
            ButtonClick onNoClick,
            ButtonClick onCancelClick) {
        showMessageDialog(
                activity,
                MessageStyle.YES_NO_CANCEL,
                title,
                message,
                null,
                onYesClick,
                onNoClick,
                onCancelClick);
    }

    public ButtonClick getOnOkClick() {
        return MessageDialogData.getInstance().getOnOkClick();
    }

    public void setOnOkClick(ButtonClick onOkClick) {
        MessageDialogData.getInstance().setOnOkClick(onOkClick);
    }

    public ButtonClick getOnYesClick() {
        return MessageDialogData.getInstance().getOnYesClick();
    }

    public void setOnYesClick(ButtonClick onYesClick) {
        MessageDialogData.getInstance().setOnYesClick(onYesClick);
    }

    public ButtonClick getOnNoClick() {
        return MessageDialogData.getInstance().getOnNoClick();
    }

    public void setOnNoClick(ButtonClick onNoClick) {
        MessageDialogData.getInstance().setOnNoClick(onNoClick);
    }

    public ButtonClick getOnCancelClick() {
        return MessageDialogData.getInstance().getOnCancelClick();
    }

    public void setOnCancelClick(ButtonClick onCancelClick) {
        MessageDialogData.getInstance().setOnCancelClick(onCancelClick);
    }

    public String getTitle() {
        return MessageDialogData.getInstance().getTitle();
    }

    public void setTitle(String title) {
        MessageDialogData.getInstance().setTitle(title);
    }

    public String getMessage() {
        return MessageDialogData.getInstance().getMessage();
    }

    public void setMessage(String message) {
        MessageDialogData.getInstance().setMessage(message);
    }

    public MessageStyle getMessageStyle() {
        return MessageDialogData.getInstance().getMessageStyle();
    }

    public void setMessageStyle(MessageStyle messageStyle) {
        MessageDialogData.getInstance().setMessageStyle(messageStyle);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        getDialog().requestWindowFeature(STYLE_NO_TITLE);

        View view = inflater.inflate(R.layout.message, container, false);
        buttonOk = (Button) view.findViewById(R.id.buttonOk);
        buttonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ButtonClick onOkClick = getOnOkClick();
                if (onOkClick != null) {
                    onOkClick.onClick();
                }

                dismiss();
            }
        });
        buttonYes = (Button) view.findViewById(R.id.buttonYes);
        buttonYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ButtonClick onYesClick = getOnYesClick();
                if (onYesClick != null) {
                    onYesClick.onClick();
                }

                dismiss();
            }
        });
        buttonNo = (Button) view.findViewById(R.id.buttonNo);
        buttonNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ButtonClick onNoClick = getOnNoClick();
                if (onNoClick != null) {
                    onNoClick.onClick();
                }

                dismiss();
            }
        });
        buttonCancel = (Button) view.findViewById(R.id.buttonCancel);
        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ButtonClick onCancelClick = getOnCancelClick();
                if (onCancelClick != null) {
                    onCancelClick.onClick();
                }

                dismiss();
            }
        });
        String title = getTitle();
        if (!stringIsNullOrEmpty(title)) {
            final TextView textViewTitle = (TextView) view.findViewById(R.id.textViewTitle);
            textViewTitle.setText(title);
        }
        String message = getMessage();
        if (!stringIsNullOrEmpty(message)) {
            final TextView textViewMessage = (TextView) view.findViewById(R.id.textViewMessage);
            textViewMessage.setText(message);
        }

        updateButtons();

        return view;
    }

    private void updateButtons() {
        switch (getMessageStyle()) {
            case OK:
                buttonOk.setVisibility(View.VISIBLE);
                buttonYes.setVisibility(View.GONE);
                buttonNo.setVisibility(View.GONE);
                buttonCancel.setVisibility(View.GONE);
                break;

            case OK_CANCEL:
                buttonOk.setVisibility(View.VISIBLE);
                buttonYes.setVisibility(View.GONE);
                buttonNo.setVisibility(View.GONE);
                buttonCancel.setVisibility(View.VISIBLE);
                break;

            case YES_NO:
                buttonOk.setVisibility(View.GONE);
                buttonYes.setVisibility(View.VISIBLE);
                buttonNo.setVisibility(View.VISIBLE);
                buttonCancel.setVisibility(View.GONE);
                break;

            case YES_NO_CANCEL:
                buttonOk.setVisibility(View.GONE);
                buttonYes.setVisibility(View.VISIBLE);
                buttonNo.setVisibility(View.VISIBLE);
                buttonCancel.setVisibility(View.VISIBLE);
                break;
        }
    }

    public enum MessageStyle {
        OK,
        OK_CANCEL,
        YES_NO,
        YES_NO_CANCEL
    }

    public interface ButtonClick {
        void onClick();
    }

    private static class MessageDialogData {
        private static MessageDialogData instance = new MessageDialogData();
        private MessageStyle messageStyle;
        private String title = null;
        private String message = null;
        private ButtonClick onOkClick = null;
        private ButtonClick onYesClick = null;
        private ButtonClick onNoClick = null;
        private ButtonClick onCancelClick = null;

        private MessageDialogData() {
        }

        public static MessageDialogData getInstance() {
            return instance;
        }

        public MessageStyle getMessageStyle() {
            return messageStyle;
        }

        public void setMessageStyle(MessageStyle messageStyle) {
            this.messageStyle = messageStyle;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public ButtonClick getOnOkClick() {
            return onOkClick;
        }

        public void setOnOkClick(ButtonClick onOkClick) {
            this.onOkClick = onOkClick;
        }

        public ButtonClick getOnYesClick() {
            return onYesClick;
        }

        public void setOnYesClick(ButtonClick onYesClick) {
            this.onYesClick = onYesClick;
        }

        public ButtonClick getOnNoClick() {
            return onNoClick;
        }

        public void setOnNoClick(ButtonClick onNoClick) {
            this.onNoClick = onNoClick;
        }

        public ButtonClick getOnCancelClick() {
            return onCancelClick;
        }

        public void setOnCancelClick(ButtonClick onCancelClick) {
            this.onCancelClick = onCancelClick;
        }
    }
}
