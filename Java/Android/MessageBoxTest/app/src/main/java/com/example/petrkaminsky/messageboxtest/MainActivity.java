package com.example.petrkaminsky.messageboxtest;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    private MessageDialog.ButtonClick onOkClick = new MessageDialog.ButtonClick() {
        @Override
        public void onClick() {
            Log.d("message", "OK clicked");
        }
    };
    private MessageDialog.ButtonClick onCancelClick = new MessageDialog.ButtonClick() {
        @Override
        public void onClick() {
            Log.d("message", "Cancel clicked");
        }
    };
    private MessageDialog.ButtonClick onYesClick = new MessageDialog.ButtonClick() {
        @Override
        public void onClick() {
            Log.d("message", "Yes clicked");
        }
    };
    private MessageDialog.ButtonClick onNoClick = new MessageDialog.ButtonClick() {
        @Override
        public void onClick() {
            Log.d("message", "No clicked");
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        findViewById(R.id.buttonMessageOk).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MessageDialog.showMessageOk(
                        getStaticActivity(),
                        getString(R.string.txt_title_1),
                        getString(R.string.txt_message_1),
                        onOkClick);
            }
        });
        findViewById(R.id.buttonMessageOkCancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MessageDialog.showMessageOkCancel(
                        getStaticActivity(),
                        getString(R.string.txt_title_2),
                        getString(R.string.txt_message_2),
                        onOkClick,
                        onCancelClick);
            }
        });
        findViewById(R.id.buttonMessageYesNo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MessageDialog.showMessageYesNo(
                        getStaticActivity(),
                        getString(R.string.txt_title_3),
                        getString(R.string.txt_message_3),
                        onYesClick,
                        onNoClick);
            }
        });
        findViewById(R.id.buttonMessageYesNoCancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MessageDialog.showMessageYesNoCancel(
                        getStaticActivity(),
                        getString(R.string.txt_title_4),
                        getString(R.string.txt_message_4),
                        onYesClick,
                        onNoClick,
                        onCancelClick);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        setStaticActivity(this);
    }

    @Override
    protected void onStop() {
        super.onStop();

        setStaticActivity(null);
    }

    private Activity getStaticActivity() {
        return MainActivityData.getInstance().getActivity();
    }

    private void setStaticActivity(Activity activity) {
        MainActivityData.getInstance().setActivity(activity);
    }

    private static class MainActivityData {
        private static MainActivityData instance = new MainActivityData();
        private Activity activity = null;

        private MainActivityData() {
        }

        public static MainActivityData getInstance() {
            return instance;
        }

        public Activity getActivity() {
            return activity;
        }

        public void setActivity(Activity activity) {
            this.activity = activity;
        }
    }
}
