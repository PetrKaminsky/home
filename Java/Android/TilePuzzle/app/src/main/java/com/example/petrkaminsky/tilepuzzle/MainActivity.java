package com.example.petrkaminsky.tilepuzzle;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private TileBox tileBox = null;
    private CheckBox checkBoxAutoSolve = null;
    private CheckBox checkBoxAnimate = null;
    private CheckBox checkBoxDistance = null;
    private DimensionDataSource dataSource = null;
    private AdapterView.OnItemSelectedListener onDimensionChanged = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            int dimension = dataSource.get(i);
            Desk.getInstance().setDimension(dimension);
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {
        }
    };
    private View.OnClickListener onShuffle = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Desk.getInstance().shuffle();
        }
    };
    private View.OnClickListener onAutoSolve = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            boolean autoSolve = checkBoxAutoSolve.isChecked();
            Desk.getInstance().setAutoSolver(autoSolve);
        }
    };
    private View.OnClickListener onAnimate = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            boolean animate = checkBoxAnimate.isChecked();
            Properties.getInstance().setAnimate(animate);
        }
    };
    private View.OnClickListener onDistance = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            boolean distance = checkBoxDistance.isChecked();
            Properties.getInstance().setDistance(distance);
            tileBox.refreshTiles();
        }
    };
    private Desk.SolveListener onSolve = new Desk.SolveListener() {
        @Override
        public void OnSolve() {
            checkBoxAutoSolve.setChecked(false);
        }
    };

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        tileBox = (TileBox) findViewById(R.id.tileBox);
        final Spinner spinnerDimension = (Spinner) findViewById(R.id.spinnerDimension);
        dataSource = new DimensionDataSource();
        DimensionAdapter adapter = new DimensionAdapter(dataSource);
        spinnerDimension.setAdapter(adapter);
        spinnerDimension.setSelection(dataSource.indexOf(Desk.getInstance().getDimension()));
        spinnerDimension.setOnItemSelectedListener(onDimensionChanged);
        final Button buttonShuffle = (Button) findViewById(R.id.buttonShuffle);
        buttonShuffle.setOnClickListener(onShuffle);
        checkBoxAutoSolve = (CheckBox) findViewById(R.id.checkBoxAutoSolve);
        checkBoxAutoSolve.setOnClickListener(onAutoSolve);
        checkBoxAnimate = (CheckBox) findViewById(R.id.checkBoxAnimate);
        checkBoxAnimate.setChecked(Properties.getInstance().isAnimate());
        checkBoxAnimate.setOnClickListener(onAnimate);
        checkBoxDistance = (CheckBox) findViewById(R.id.checkBoxDistance);
        checkBoxDistance.setChecked(Properties.getInstance().isDistance());
        checkBoxDistance.setOnClickListener(onDistance);
        Desk.getInstance().setSolveListener(onSolve);
    }

    private static class DimensionDataSource extends ArrayList<Integer> {

        public DimensionDataSource() {
            for (int dimension = 2; dimension <= 12; dimension++) {
                add(dimension);
            }
        }
    }

    private static class DimensionAdapter extends BaseAdapter {

        private DimensionDataSource dataSource = null;

        public DimensionAdapter(DimensionDataSource ds) {
            dataSource = ds;
        }

        @Override
        public int getCount() {
            return dataSource.size();
        }

        @Override
        public Object getItem(int i) {
            return dataSource.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            if (view == null) {
                view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.spinner_dimension, viewGroup, false);
            }
            TextView textView = view.findViewById(R.id.textViewDimension);
            textView.setText(Utils.toString(dataSource.get(i)));

            return view;
        }
    }
}
