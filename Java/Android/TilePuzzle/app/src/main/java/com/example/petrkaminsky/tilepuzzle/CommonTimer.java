package com.example.petrkaminsky.tilepuzzle;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by petr.kaminsky on 3.7.2018.
 */

public class CommonTimer {

    private long timerDelay = 0;
    private Timer timer = new Timer();
    private boolean active = false;
    private TimerControl timerControl = null;

    public CommonTimer(long delay, TimerControl control) {
        timerDelay = delay;
        timerControl = control;
    }

    public void start() {
        if (active) {
            return;
        }
        if (timerControl != null) {
            if (timerControl.shouldStop()) {
                active = false;
                return;
            }
            Runnable runnable = timerControl.getRunStep();
            if (runnable != null) {
                runnable.run();
            }
            schedule();
            active = true;
        } else {
            active = false;
        }
    }

    private void schedule() {
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                Utils.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        active = false;
                        start();
                    }
                });
            }
        }, timerDelay);
    }

    public interface TimerControl {
        boolean shouldStop();

        Runnable getRunStep();
    }
}
