package com.example.petrkaminsky.tilepuzzle;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;

/**
 * Created by petr.kaminsky on 4.7.2018.
 */

public class AutoSolver {

    private DeskData deskData = new DeskData();
    private Queue<AutoSolverMove> moveDeque = new LinkedList<>();
    private StateChangeListener stateChangeListener = null;

    public void setStateChangeListener(StateChangeListener listener) {
        stateChangeListener = listener;
    }

    public void initialize(DeskData data) {
        moveDeque.clear();
        deskData.initFrom(data);
    }

    public void solveStep() {
        if (deskData.isSolved()) {
            signalSolved();
            return;
        }
        int i, n = deskData.getDimension() - 1;
        for (i = 0; i < n; i++) {
            if (!isRowSolved(i)) {
                solveRow(i);
                return;
            }
            if (!isColumnSolved(i)) {
                solveColumn(i);
                return;
            }
        }
    }

    private void signalSolved() {
        if (stateChangeListener != null) {
            stateChangeListener.OnSolve();
        }
    }

    public Queue<MoveBatch> getSolutionMoves() {
        Queue<MoveBatch> batches = new LinkedList<>();
        optimizeMoves();
        MoveBatch batch = new MoveBatch();
        AutoSolverMoveType lastType = AutoSolverMoveType.UNKNOWN;
        while (!moveDeque.isEmpty()) {
            AutoSolverMove move = moveDeque.poll();
            if (batch.isEmpty()) {
                lastType = move.getMoveType();
                batch.add(move);
            } else {
                if (lastType == move.getMoveType()) {
                    batch.add(move);
                } else {
                    batches.add(batch);
                    batch = new MoveBatch();
                    lastType = move.getMoveType();
                    batch.add(move);
                }
            }
        }
        if (!batch.isEmpty()) {
            batches.add(batch);
        }
        return batches;
    }

    private void optimizeMoves() {
        boolean changed;
        AutoSolverMove previousMove = null;
        do {
            changed = false;
            Iterator<AutoSolverMove> iterator = moveDeque.iterator();
            while (iterator.hasNext()) {
                AutoSolverMove move = iterator.next();
                if (previousMove != null &&
                        move != null) {
                    if (AutoSolverMove.isOpposite(previousMove, move)) {
                        moveDeque.remove(previousMove);
                        moveDeque.remove(move);
                        changed = true;
                        break;
                    }
                }
                previousMove = move;
            }
        } while (changed);
    }

    private boolean isRowSolved(int y) {
        for (int x = y; x < deskData.getDimension(); x++) {
            if (!deskData.isSolved(x, y)) {
                return false;
            }
        }
        return true;
    }

    private boolean isColumnSolved(int x) {
        for (int y = x; y < deskData.getDimension(); y++) {
            if (!deskData.isSolved(x, y)) {
                return false;
            }
        }
        return true;
    }

    private void solveRow(int y) {
        for (int x = y; x < deskData.getDimension(); x++) {
            if (!deskData.isSolved(x, y)) {
                solveTileRowMode(x, y);
                return;
            }
        }
    }

    private void solveColumn(int x) {
        for (int y = x; y < deskData.getDimension(); y++) {
            if (!deskData.isSolved(x, y)) {
                solveTileColumnMode(x, y);
                return;
            }
        }
    }

    private void solveTileRowMode(int x, int y) {
        int tile = deskData.getIndex(x, y) + 1;
        Location startLocation = deskData.findTile(tile);
        int startX = startLocation.getX();
        int startY = startLocation.getY();
        if (x == (deskData.getDimension() - 1)) {
            // last tile
            if (deskData.getValue(x, y) == Desk.EMPTY_VALUE &&
                    deskData.getValue(x, y + 1) == tile) {
                // up
                moveTileUp();
            } else {
                generateTileMoveRowMode(
                        startX,
                        startY,
                        x,
                        y + 1);
                Location emptyLocation = deskData.findEmptyTile();
                generateSpaceMoveRowMode(
                        emptyLocation.getX(),
                        emptyLocation.getY(),
                        x,
                        y + 2);
                moveTileDown();
                moveTileDown();
                moveTileRight();
                moveTileUp();
                moveTileLeft();
                moveTileUp();
                moveTileRight();
                moveTileDown();
                moveTileDown();
                moveTileLeft();
                moveTileUp();
            }
        } else {
            generateTileMoveRowMode(
                    startX,
                    startY,
                    x,
                    y);
        }
    }

    private void solveTileColumnMode(int x, int y) {
        int tile = deskData.getIndex(x, y) + 1;
        Location startLocation = deskData.findTile(tile);
        int startX = startLocation.getX();
        int startY = startLocation.getY();
        if (y == (deskData.getDimension() - 1)) {
            // last tile
            if (deskData.getValue(x, y) == Desk.EMPTY_VALUE &&
                    deskData.getValue(x + 1, y) == tile) {
                // left
                moveTileLeft();
            } else {
                generateTileMoveColumnMode(
                        startX,
                        startY,
                        x + 1,
                        y);
                Location emptyLocation = deskData.findEmptyTile();
                generateSpaceMoveColumnMode(
                        emptyLocation.getX(),
                        emptyLocation.getY(),
                        x + 2,
                        y);
                moveTileRight();
                moveTileRight();
                moveTileDown();
                moveTileLeft();
                moveTileUp();
                moveTileLeft();
                moveTileDown();
                moveTileRight();
                moveTileRight();
                moveTileUp();
                moveTileLeft();
            }
        } else {
            generateTileMoveColumnMode(
                    startX,
                    startY,
                    x,
                    y);
        }
    }

    private void generateTileMoveRowMode(
            int x1,
            int y1,
            int x2,
            int y2) {
        generateTileMoveHorizontalRowMode(
                x1,
                x2,
                y1);
        generateTileMoveVerticalRowMode(
                x2,
                y1,
                y2);
    }

    private void generateTileMoveColumnMode(
            int x1,
            int y1,
            int x2,
            int y2) {
        generateTileMoveVerticalColumnMode(
                x1,
                y1,
                y2);
        generateTileMoveHorizontalColumnMode(
                x1,
                x2,
                y2);
    }

    private void generateTileMoveHorizontalRowMode(
            int x1,
            int x2,
            int y) {
        while (x2 != x1) {
            int x = x1;
            if (x < x2) {
                x++;
            } else {
                x--;
            }
            generateTileMoveByOneRowMode(
                    x1,
                    y,
                    x,
                    y);
            x1 = x;
        }
    }

    private void generateTileMoveVerticalRowMode(
            int x,
            int y1,
            int y2) {
        while (y2 != y1) {
            int y = y1;
            if (y < y2) {
                y++;
            } else {
                y--;
            }
            generateTileMoveByOneRowMode(
                    x,
                    y1,
                    x,
                    y);
            y1 = y;
        }
    }

    private void generateTileMoveVerticalColumnMode(
            int x,
            int y1,
            int y2) {
        while (y2 != y1) {
            int y = y1;
            if (y < y2) {
                y++;
            } else {
                y--;
            }
            generateTileMoveByOneColumnMode(
                    x,
                    y1,
                    x,
                    y);
            y1 = y;
        }
    }

    private void generateTileMoveHorizontalColumnMode(
            int x1,
            int x2,
            int y) {
        while (x2 != x1) {
            int x = x1;
            if (x < x2) {
                x++;
            } else {
                x--;
            }
            generateTileMoveByOneColumnMode(
                    x1,
                    y,
                    x,
                    y);
            x1 = x;
        }
    }

    private void generateTileMoveByOneRowMode(
            int x1,
            int y1,
            int x2,
            int y2) {
        if (y1 == y2 &&
                x1 == x2) {
            return;
        }
        generateSpaceMoveRowMode(
                x1,
                y1,
                x2,
                y2);
        if (y1 == y2) {
            if (x1 < x2) {
                // right
                moveTileRight();
            } else if (x1 > x2) {
                // left
                moveTileLeft();
            }
        } else {
            if (y1 < y2) {
                // down
                moveTileDown();
            } else if (y1 > y2) {
                // up
                moveTileUp();
            }
        }
    }

    private void generateTileMoveByOneColumnMode(
            int x1,
            int y1,
            int x2,
            int y2) {
        if (y1 == y2 &&
                x1 == x2) {
            return;
        }
        generateSpaceMoveColumnMode(
                x1,
                y1,
                x2,
                y2);
        if (y1 == y2) {
            if (x1 < x2) {
                // right
                moveTileRight();
            } else if (x1 > x2) {
                // left
                moveTileLeft();
            }
        } else {
            if (y1 < y2) {
                // down
                moveTileDown();
            } else if (y1 > y2) {
                // up
                moveTileUp();
            }
        }
    }

    private void generateSpaceMoveRowMode(
            int x1,
            int y1,
            int x2,
            int y2) {
        Location emptyLocation = deskData.findEmptyTile();
        int x = emptyLocation.getX();
        int y = emptyLocation.getY();
        if (y < y2) {
            // empty down
            while (y != y2) {
                if (((y + 1) == y1) && (x == x1)) {
                    break;
                }
                // empty down, i.e. tile up
                moveTileUp();
                y++;
            }
            if (y == y2) {
                while (x != x2) {
                    if (x < x2) {
                        // empty right
                        if ((y == y1) && ((x + 1) == x1)) {
                            break;
                        }
                        // empty right, i.e. tile left
                        moveTileLeft();
                        x++;
                    } else {
                        // empty left
                        if ((y == y1) && ((x - 1) == x1)) {
                            break;
                        }
                        // empty left, i.e. tile right
                        moveTileRight();
                        x--;
                    }
                }
            }
        } else {
            while (x != x2) {
                if (x < x2) {
                    // empty right
                    if ((y == y1) && ((x + 1) == x1)) {
                        break;
                    }
                    // empty right, i.e. tile left
                    moveTileLeft();
                    x++;
                } else {
                    // empty left
                    if ((y == y1) && ((x - 1) == x1)) {
                        break;
                    }
                    // empty left, i.e. tile right
                    moveTileRight();
                    x--;
                }
            }
            if (x == x2) {
                // empty up
                while (y != y2) {
                    if (((y - 1) == y1) && (x == x1)) {
                        break;
                    }
                    // empty up, i.e. tile down
                    moveTileDown();
                    y--;
                }
            }
        }
        moveSpaceAround(
                x1,
                y1,
                x,
                y,
                x2,
                y2);
    }

    private void generateSpaceMoveColumnMode(
            int x1,
            int y1,
            int x2,
            int y2) {
        Location emptyLocation = deskData.findEmptyTile();
        int x = emptyLocation.getX();
        int y = emptyLocation.getY();
        if (x < x2) {
            // empty right
            while (x != x2) {
                if ((y == y1) && ((x + 1) == x1)) {
                    break;
                }
                // empty right, i.e. tile left
                moveTileLeft();
                x++;
            }
            if (x == x2) {
                while (y != y2) {
                    if (y < y2) {
                        // empty down
                        if (((y + 1) == y1) && (x == x1)) {
                            break;
                        }
                        // empty down, i.e. tile up
                        moveTileUp();
                        y++;
                    } else {
                        // empty up
                        if (((y - 1) == y1) && (x == x1)) {
                            break;
                        }
                        // empty up, i.e. tile down
                        moveTileDown();
                        y--;
                    }
                }
            }
        } else {
            while (y != y2) {
                if (y < y2) {
                    // empty down
                    if (((y + 1) == y1) && (x == x1)) {
                        break;
                    }
                    // empty down, i.e. tile up
                    moveTileUp();
                    y++;
                } else {
                    // empty up
                    if (((y - 1) == y1) && (x == x1)) {
                        break;
                    }
                    // empty up, i.e. tile down
                    moveTileDown();
                    y--;
                }
            }
            if (y == y2) {
                // empty left
                while (x != x2) {
                    if ((y == y1) && ((x - 1) == x1)) {
                        break;
                    }
                    // empty left, i.e. tile right
                    moveTileRight();
                    x--;
                }
            }
        }
        moveSpaceAround(
                x1,
                y1,
                x,
                y,
                x2,
                y2);
    }

    private void moveSpaceAround(
            int tileX,
            int tileY,
            int x1,
            int y1,
            int x2,
            int y2) {
        if (y1 < tileY) {
            // empty is up
            if (y2 < tileY) {
                // should be up, do nothing
            } else if (y2 > tileY) {
                // should be down
                moveSpaceAroundUpDown(tileX, tileY);
            } else if (x2 < tileX) {
                // should be left
                moveSpaceAroundUpLeft(tileX, tileY);
            } else if (x2 > tileX) {
                // should be right
                moveSpaceAroundUpRight(tileX, tileY);
            }
        } else if (y1 > tileY) {
            // empty is down
            if (y2 < tileY) {
                // should be up
                moveSpaceAroundDownUp(tileX, tileY);
            } else if (y2 > tileY) {
                // should be down, do nothing
            } else if (x2 < tileX) {
                // should be left
                moveSpaceAroundDownLeft(tileX, tileY);
            } else if (x2 > tileX) {
                // should be right
                moveSpaceAroundDownRight(tileX, tileY);
            }
        } else if (x1 < tileX) {
            // empty is left
            if (y2 < tileY) {
                // should be up
                moveSpaceAroundLeftUp(tileX, tileY);
            } else if (y2 > tileY) {
                // should be down
                moveSpaceAroundLeftDown(tileX, tileY);
            } else if (x2 < tileX) {
                // should be left, do nothing
            } else if (x2 > tileX) {
                // should be right
                moveSpaceAroundLeftRight(tileX, tileY);
            }
        } else if (x1 > tileX) {
            // empty is right
            if (y2 < tileY) {
                // should be up
                moveSpaceAroundRightUp(tileX, tileY);
            } else if (y2 > tileY) {
                // should be down
                moveSpaceAroundRightDown(tileX, tileY);
            } else if (x2 < tileX) {
                // should be left
                moveSpaceAroundRightLeft(tileX, tileY);
            } else if (x2 > tileX) {
                // should be right, do nothing
            }
        }
    }

    private void moveSpaceAroundUpDown(int tileX, int tileY) {
        if (tileX == (deskData.getDimension() - 1)) {
            moveTileRight();
            moveTileUp();
            moveTileUp();
            moveTileLeft();
        } else {
            moveTileLeft();
            moveTileUp();
            moveTileUp();
            moveTileRight();
        }
    }

    private void moveSpaceAroundUpLeft(int tileX, int tileY) {
        if (tileX == (deskData.getDimension() - 1) ||
                tileY == (deskData.getDimension() - 1)) {
            moveTileRight();
            moveTileUp();
        } else {
            moveTileLeft();
            moveTileUp();
            moveTileUp();
            moveTileRight();
            moveTileRight();
            moveTileDown();
        }
    }

    private void moveSpaceAroundUpRight(int tileX, int tileY) {
        moveTileLeft();
        moveTileUp();
    }

    private void moveSpaceAroundDownUp(int tileX, int tileY) {
        if (tileX == (deskData.getDimension() - 1)) {
            moveTileRight();
            moveTileDown();
            moveTileDown();
            moveTileLeft();
        } else {
            moveTileLeft();
            moveTileDown();
            moveTileDown();
            moveTileRight();
        }
    }

    private void moveSpaceAroundDownLeft(int tileX, int tileY) {
        moveTileRight();
        moveTileDown();
    }

    private void moveSpaceAroundDownRight(int tileX, int tileY) {
        moveTileLeft();
        moveTileDown();
    }

    private void moveSpaceAroundLeftUp(int tileX, int tileY) {
        if (tileX == (deskData.getDimension() - 1) ||
                tileY == (deskData.getDimension() - 1)) {
            moveTileDown();
            moveTileLeft();
        } else {
            moveTileUp();
            moveTileLeft();
            moveTileLeft();
            moveTileDown();
            moveTileDown();
            moveTileRight();
        }
    }

    private void moveSpaceAroundLeftDown(int tileX, int tileY) {
        moveTileUp();
        moveTileLeft();
    }

    private void moveSpaceAroundLeftRight(int tileX, int tileY) {
        if (tileY == (deskData.getDimension() - 1)) {
            moveTileDown();
            moveTileLeft();
            moveTileLeft();
            moveTileUp();
        } else {
            moveTileUp();
            moveTileLeft();
            moveTileLeft();
            moveTileDown();
        }
    }

    private void moveSpaceAroundRightUp(int tileX, int tileY) {
        moveTileDown();
        moveTileRight();
    }

    private void moveSpaceAroundRightDown(int tileX, int tileY) {
        moveTileUp();
        moveTileRight();
    }

    private void moveSpaceAroundRightLeft(int tileX, int tileY) {
        if (tileY == (deskData.getDimension() - 1)) {
            moveTileDown();
            moveTileRight();
            moveTileRight();
            moveTileUp();
        } else {
            moveTileUp();
            moveTileRight();
            moveTileRight();
            moveTileDown();
        }
    }

    private void moveTileUp() {
        Location emptyLocation = deskData.findEmptyTile();
        int emptyX = emptyLocation.getX();
        int emptyY = emptyLocation.getY();
        int x = emptyX;
        int y = emptyY + 1;
        if (y >= deskData.getDimension()) {
            // empty at bottom, do nothing
            return;
        }
        moveTileUp(emptyX,
                emptyY,
                x,
                y);
    }

    private void moveTileDown() {
        Location emptyLocation = deskData.findEmptyTile();
        int emptyX = emptyLocation.getX();
        int emptyY = emptyLocation.getY();
        int x = emptyX;
        int y = emptyY - 1;
        if (y < 0) {
            // empty at top, do nothing
            return;
        }
        moveTileDown(
                emptyX,
                emptyY,
                x,
                y);
    }

    private void moveTileLeft() {
        Location emptyLocation = deskData.findEmptyTile();
        int emptyX = emptyLocation.getX();
        int emptyY = emptyLocation.getY();
        int x = emptyX + 1;
        int y = emptyY;
        if (x >= deskData.getDimension()) {
            // empty at right, do nothing
            return;
        }
        moveTileLeft(
                emptyX,
                emptyY,
                x,
                y);
    }

    private void moveTileRight() {
        Location emptyLocation = deskData.findEmptyTile();
        int emptyX = emptyLocation.getX();
        int emptyY = emptyLocation.getY();
        int x = emptyX - 1;
        int y = emptyY;
        if (x < 0) {
            // empty at left, do nothing
            return;
        }
        moveTileRight(
                emptyX,
                emptyY,
                x,
                y);
    }

    private void moveTileUp(
            int x1,
            int y1,
            int x2,
            int y2) {
        deskData.exchangeValues(
                x1,
                y1,
                x2,
                y2);
        moveDeque.add(
                new AutoSolverMove(
                        AutoSolverMoveType.UP,
                        x1,
                        y1,
                        x2,
                        y2));
    }

    private void moveTileDown(
            int x1,
            int y1,
            int x2,
            int y2) {
        deskData.exchangeValues(
                x1,
                y1,
                x2,
                y2);
        moveDeque.add(
                new AutoSolverMove(
                        AutoSolverMoveType.DOWN,
                        x1,
                        y1,
                        x2,
                        y2));
    }

    private void moveTileLeft(
            int x1,
            int y1,
            int x2,
            int y2) {
        deskData.exchangeValues(
                x1,
                y1,
                x2,
                y2);
        moveDeque.add(
                new AutoSolverMove(
                        AutoSolverMoveType.LEFT,
                        x1,
                        y1,
                        x2,
                        y2));
    }

    private void moveTileRight(
            int x1,
            int y1,
            int x2,
            int y2) {
        deskData.exchangeValues(
                x1,
                y1,
                x2,
                y2);
        moveDeque.add(
                new AutoSolverMove(
                        AutoSolverMoveType.RIGHT,
                        x1,
                        y1,
                        x2,
                        y2));
    }

    private enum AutoSolverMoveType {
        UNKNOWN,
        UP,
        DOWN,
        LEFT,
        RIGHT
    }

    public interface StateChangeListener {
        void OnSolve();
    }

    private static class AutoSolverMove extends Move {

        private AutoSolverMoveType moveType;

        public AutoSolverMove(AutoSolverMoveType type, Location loc1, Location loc2) {
            super(loc1, loc2);
            moveType = type;
        }

        public AutoSolverMove(
                AutoSolverMoveType type,
                int x1,
                int y1,
                int x2,
                int y2) {
            super(x1, y1, x2, y2);
            moveType = type;
        }

        public static boolean isOpposite(AutoSolverMove move1, AutoSolverMove move2) {
            return
                    (move1.getMoveType() == AutoSolverMoveType.UP && move2.getMoveType() == AutoSolverMoveType.DOWN) ||
                            (move1.getMoveType() == AutoSolverMoveType.DOWN && move2.getMoveType() == AutoSolverMoveType.UP) ||
                            (move1.getMoveType() == AutoSolverMoveType.LEFT && move2.getMoveType() == AutoSolverMoveType.RIGHT) ||
                            (move1.getMoveType() == AutoSolverMoveType.RIGHT && move2.getMoveType() == AutoSolverMoveType.LEFT);
        }

        public AutoSolverMoveType getMoveType() {
            return moveType;
        }
    }
}
