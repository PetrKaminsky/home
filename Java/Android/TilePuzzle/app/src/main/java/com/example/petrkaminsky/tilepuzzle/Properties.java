package com.example.petrkaminsky.tilepuzzle;

import android.content.SharedPreferences;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by petr.kaminsky on 3.7.2018.
 */

public class Properties {

    private static final Properties instance = new Properties();
    private static final int DEFAULT_DIMENSION_VALUE = 4;
    private static final String DEFAULT_TILES_VALUE = "";
    private static final boolean DEFAULT_ANIMATE = true;
    private static final boolean DEFAULT_DISTANCE = false;
    private static final String TILES_SEPARATOR = ",";
    private SharedPreferences preferences = null;
    private boolean animate = DEFAULT_ANIMATE;
    private boolean distance = DEFAULT_DISTANCE;

    private Properties() {
        preferences = Utils.getApplicationContext().getSharedPreferences(
                Utils.getPackageName(),
                MODE_PRIVATE);
        load();
    }

    public static Properties getInstance() {
        return instance;
    }

    public boolean isDistance() {
        return distance;
    }

    public void setDistance(boolean on) {
        distance = on;
        preferences.edit()
                .putBoolean(Utils.getString(R.string.preference_distance), distance)
                .apply();
    }

    private void load() {
        animate = preferences.getBoolean(Utils.getString(R.string.preference_animate), DEFAULT_ANIMATE);
        distance = preferences.getBoolean(Utils.getString(R.string.preference_distance), DEFAULT_DISTANCE);
    }

    public int getDimension() {
        return preferences.getInt(Utils.getString(R.string.preference_dimension), DEFAULT_DIMENSION_VALUE);
    }

    public void setDimension(int dimension) {
        preferences.edit()
                .putInt(Utils.getString(R.string.preference_dimension), dimension)
                .apply();
    }

    public String getTilesText() {
        return preferences.getString(Utils.getString(R.string.preference_tiles), DEFAULT_TILES_VALUE);
    }

    public void setTilesText(String tiles) {
        preferences.edit()
                .putString(Utils.getString(R.string.preference_tiles), tiles)
                .apply();
    }

    public boolean isAnimate() {
        return animate;
    }

    public void setAnimate(boolean on) {
        animate = on;
        preferences.edit()
                .putBoolean(Utils.getString(R.string.preference_animate), animate)
                .apply();
    }

    public int[] getTiles() {
        int[] retVal = null;
        String tilesText = getTilesText();
        if (!tilesText.equals(DEFAULT_TILES_VALUE)) {
            String[] tilesArray = tilesText.split(TILES_SEPARATOR);
            int length = tilesArray.length;
            retVal = new int[length];
            for (int i = 0; i < length; i++) {
                retVal[i] = Integer.parseInt(tilesArray[i]);
            }
        }
        return retVal;
    }

    public void setTiles(int[] tiles) {
        StringBuilder builder = new StringBuilder();
        boolean first = true;
        if (tiles != null) {
            int length = tiles.length;
            for (int i = 0; i < length; i++) {
                if (first) {
                    first = false;
                } else {
                    builder.append(TILES_SEPARATOR);
                }
                builder.append(Utils.toString(tiles[i]));
            }
        }
        setTilesText(builder.toString());
    }
}
