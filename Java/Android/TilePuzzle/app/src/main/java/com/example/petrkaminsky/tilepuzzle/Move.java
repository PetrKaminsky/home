package com.example.petrkaminsky.tilepuzzle;

/**
 * Created by petr.kaminsky on 12.6.2018.
 */

public class Move {

    private Location location1 = null;
    private Location location2 = null;

    public Move(Location loc1, Location loc2) {
        location1 = loc1;
        location2 = loc2;
    }

    public Move(
            int x1,
            int y1,
            int x2,
            int y2) {
        location1 = new Location(x1, y1);
        location2 = new Location(x2, y2);
    }

    public Location getLocation1() {
        return location1;
    }

    public Location getLocation2() {
        return location2;
    }
}
