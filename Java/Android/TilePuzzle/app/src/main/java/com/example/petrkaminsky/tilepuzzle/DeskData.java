package com.example.petrkaminsky.tilepuzzle;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * Created by petr.kaminsky on 4.7.2018.
 */

public class DeskData {

    private int dimension = 0;
    private int[] deskValues = null;

    public int getDimension() {
        return dimension;
    }

    public void initFrom(DeskData data) {
        dimension = data.dimension;
        deskValues = Arrays.copyOf(data.deskValues, data.deskValues.length);
    }

    public void initialize(int d) {
        if (dimension != d) {
            deskValues = null;
            dimension = d;
        }
        if (deskValues == null) {
            deskValues = new int[getValueCount()];
        }
    }

    public void initializeValues() {
        int count = getValueCount() - 1;
        for (int i = 0; i < count; i++) {
            setValue(i, i + 1);
        }
        setValue(count, Desk.EMPTY_VALUE);
    }

    private int getValueCount() {
        return dimension * dimension;
    }

    public int getIndex(int x, int y) {
        return x + y * dimension;
    }

    public int getValue(int x, int y) {
        return getValue(getIndex(x, y));
    }

    public int getValue(int index) {
        return deskValues[index];
    }

    public void setValue(int index, int value) {
        deskValues[index] = value;
    }

    private void exchangeValues(int index1, int index2) {
        int temp = getValue(index1);
        setValue(index1, getValue(index2));
        setValue(index2, temp);
    }

    public void exchangeValues(
            int x1,
            int y1,
            int x2,
            int y2) {
        exchangeValues(getIndex(x1, y1), getIndex(x2, y2));
    }

    public void exchangeValues(Location location1, Location location2) {
        exchangeValues(
                location1.getX(),
                location1.getY(),
                location2.getX(),
                location2.getY());
    }

    public boolean isSolved(int x, int y) {
        int index = getIndex(x, y);
        return index == getValue(index) - 1;
    }

    public boolean isSolved() {
        int count = getValueCount() - 1;
        for (int i = 0; i < count; i++) {
            if (i != getValue(i) - 1) {
                return false;
            }
        }
        return true;
    }

    private int getEmptyTileIndex() {
        int count = getValueCount();
        for (int i = 0; i < count; i++) {
            if (getValue(i) == Desk.EMPTY_VALUE) {
                return i;
            }
        }
        return -1;
    }

    public Location findEmptyTile() {
        final int emptyIndex = getEmptyTileIndex();
        return new Location(emptyIndex % dimension, emptyIndex / dimension);
    }

    public Location findTile(int value) {
        for (int row = 0; row < dimension; row++) {
            for (int column = 0; column < dimension; column++) {
                if (getValue(column, row) == value) {
                    return new Location(column, row);
                }
            }
        }
        return null;
    }

    public int[] getValues() {
        return deskValues;
    }

    public void setValues(int[] values) {
        deskValues = values;
    }

    public void shuffle() {
        initializeValues();
        int count = getValueCount() - 1;
        List<Integer> srcIndices = new ArrayList<>(count);
        for (int i = 0; i < count; i++) {
            srcIndices.add(i);
        }
        List<Integer> dstIndices = new ArrayList<>(count);
        dstIndices.addAll(srcIndices);
        shuffle(dstIndices);
        int exchangeCount = 0;
        for (int i = 0; i < count; i++) {
            int srcIndex = srcIndices.get(i);
            int dstIndex = dstIndices.get(i);
            if (srcIndex != dstIndex) {
                exchangeValues(srcIndex, dstIndex);
                exchangeCount++;
            }
        }
        // odd exchange count
        if (exchangeCount % 2 == 1) {
            exchangeValues(0, count - 1);
        }
    }

    private void shuffle(List<Integer> indices) {
        int n = indices.size();
        Random r = new Random();
        for (int i = n - 1; i > 0; i--) {
            Collections.swap(indices, i, r.nextInt(i));
        }
    }

    public int getTileDistance(int x, int y) {
        int retVal = -1;
        int value = getValue(x, y);
        if (value != Desk.EMPTY_VALUE) {
            int finalX = (value - 1) % dimension;
            int finalY = (value - 1) / dimension;
            retVal = Math.abs(x - finalX) + Math.abs(y - finalY);
        }
        return retVal;
    }
}
