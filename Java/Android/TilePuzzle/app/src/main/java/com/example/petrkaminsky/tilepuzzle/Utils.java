package com.example.petrkaminsky.tilepuzzle;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;

/**
 * Created by petr.kaminsky on 2.7.2018.
 */

public class Utils {
    public static void runOnUiThread(Runnable runnable) {
        new Handler(Looper.getMainLooper()).post(runnable);
    }

    public static String getString(int resId) {
        return getApplicationContext().getString(resId);
    }

    public static Context getApplicationContext() {
        return TilePuzzle.getContext();
    }

    public static String getPackageName() {
        return getApplicationContext().getPackageName();
    }

    public static String toString(int value) {
        return String.valueOf(value);
    }
}
