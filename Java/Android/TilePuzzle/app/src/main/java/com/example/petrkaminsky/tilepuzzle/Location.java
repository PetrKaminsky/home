package com.example.petrkaminsky.tilepuzzle;

/**
 * Created by petr.kaminsky on 6.6.2018.
 */

public class Location {
    private int x = 0;
    private int y = 0;

    public Location() {
    }

    public Location(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
