package com.example.petrkaminsky.tilepuzzle;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

/**
 * Created by petr.kaminsky on 6.6.2018.
 */

public class Desk {
    public static final int EMPTY_VALUE = -1;
    private static final long MOVE_TIMER_DELAY = 250;
    private static final Desk instance = new Desk();
    private int dimension = 0;
    private DimensionChangeListener dimensionChangeListener = null;
    private ValueChangeListener valueChangeListener = null;
    private DeskData deskData = new DeskData();
    private Queue<MoveBatch> moveQueue = new LinkedList<>();
    private CommonTimer moveTimer = null;
    private boolean autoSolverActive = false;
    private AutoSolver autoSolver = new AutoSolver();
    private SolveListener solveListener = null;
    private CommonTimer.TimerControl moveTimerControl = new CommonTimer.TimerControl() {
        @Override
        public boolean shouldStop() {
            return moveQueue.isEmpty();
        }

        @Override
        public Runnable getRunStep() {
            return new Runnable() {
                @Override
                public void run() {
                    executeMoves();

                    if (autoSolverActive) {
                        if (!isSolved()) {
                            if (moveQueue.isEmpty()) {
                                generateAutoSolverMoves();
                            }
                        } else {
                            autoSolverActive = false;
                            signalSolved();
                        }
                    }
                }
            };
        }
    };
    private AutoSolver.StateChangeListener autoSolverListener = new AutoSolver.StateChangeListener() {
        @Override
        public void OnSolve() {
            signalSolved();
        }
    };

    private Desk() {
        moveTimer = new CommonTimer(MOVE_TIMER_DELAY, moveTimerControl);
        autoSolver.setStateChangeListener(autoSolverListener);
        restoreDesk();
    }

    public static Desk getInstance() {
        return instance;
    }

    public void setSolveListener(SolveListener listener) {
        solveListener = listener;
    }

    public void setAutoSolver(boolean active) {
        autoSolverActive = active;
        if (active) {
            if (!isSolved()) {
                generateAutoSolverMoves();
                startMoveTimer();
            } else {
                signalSolved();
            }
        } else {
            moveQueue.clear();
        }
    }

    private void signalSolved() {
        if (solveListener != null) {
            solveListener.OnSolve();
        }
    }

    private void generateAutoSolverMoves() {
        autoSolver.initialize(deskData);
        autoSolver.solveStep();
        moveQueue.addAll(autoSolver.getSolutionMoves());
    }

    public void setValueChangeListener(ValueChangeListener listener) {
        valueChangeListener = listener;
    }

    public void setDimensionChangeListener(DimensionChangeListener listener) {
        dimensionChangeListener = listener;
    }

    public int getDimension() {
        return dimension;
    }

    public void setDimension(int d) {
        if (d != dimension) {
            dimension = d;
            deskData.initialize(d);
            deskData.initializeValues();
            if (dimensionChangeListener != null) {
                dimensionChangeListener.OnChange();
            }
            backupDesk();
        }
    }

    public void shuffle() {
        deskData.shuffle();
        if (valueChangeListener != null) {
            valueChangeListener.OnChange();
        }
        backupDesk();
    }

    public int getValue(int x, int y) {
        return deskData.getValue(x, y);
    }

    public void clickTile(int x, int y) {
        if (moveQueue.isEmpty() &&
                !isSolved() &&
                allowClickMove(x, y)) {
            Location emptyLocation = deskData.findEmptyTile();
            generateMoves(x, y, emptyLocation.getX(), emptyLocation.getY());
        }
    }

    private void generateMoves(
            int x,
            int y,
            int emptyX,
            int emptyY) {
        if (x == emptyX) {
            generateVerticalMoves(x, y, emptyY);
        } else if (y == emptyY) {
            generateHorizontalMoves(x, emptyX, y);
        }
    }

    private void generateHorizontalMoves(
            int x,
            int emptyX,
            int y) {
        boolean negative = x < emptyX;
        MoveBatch batch = new MoveBatch();
        while (x != emptyX) {
            int newEmptyX = negative
                    ? emptyX - 1
                    : emptyX + 1;
            batch.add(new Move(newEmptyX, y, emptyX, y));
            emptyX = newEmptyX;
        }
        queueMoves(batch);
    }

    private void generateVerticalMoves(
            int x,
            int y,
            int emptyY) {
        boolean negative = y < emptyY;
        MoveBatch batch = new MoveBatch();
        while (y != emptyY) {
            int newEmptyY = negative
                    ? emptyY - 1
                    : emptyY + 1;
            batch.add(new Move(x, newEmptyY, x, emptyY));
            emptyY = newEmptyY;
        }
        queueMoves(batch);
    }

    private void queueMoves(MoveBatch moves) {
        moveQueue.add(moves);
        // directly move
        executeMoves();
    }

    private void executeMoves() {
        MoveBatch moves = moveQueue.poll();
        while (!moves.isEmpty()) {
            Move move = moves.poll();
            deskData.exchangeValues(move.getLocation1(), move.getLocation2());
        }
        if (valueChangeListener != null) {
            valueChangeListener.OnChange();
        }
        backupDesk();
    }

    private boolean allowClickMove(int x, int y) {
        Location emptyLocation = deskData.findEmptyTile();
        int emptyX = emptyLocation.getX();
        int emptyY = emptyLocation.getY();
        return (x == emptyX && y != emptyY) ||
                (x != emptyX && y == emptyY);
    }

    public boolean isSolved(int x, int y) {
        return deskData.isSolved(x, y);
    }

    public boolean isSolved() {
        return deskData.isSolved();
    }

    public int getDistance(int x, int y) {
        return deskData.getTileDistance(x, y);
    }

    public void backupDesk() {
        Properties.getInstance().setDimension(dimension);
        Properties.getInstance().setTiles(deskData.getValues());
    }

    public void restoreDesk() {
        dimension = Properties.getInstance().getDimension();
        deskData.initialize(dimension);
        int[] tiles = Properties.getInstance().getTiles();
        if (tiles != null) {
            deskData.setValues(Arrays.copyOf(tiles, tiles.length));
        } else {
            deskData.initializeValues();
        }
    }

    private void startMoveTimer() {
        moveTimer.start();
    }

    public interface DimensionChangeListener {
        void OnChange();
    }

    public interface ValueChangeListener {
        void OnChange();
    }

    public interface SolveListener {
        void OnSolve();
    }
}
