package com.example.petrkaminsky.tilepuzzle;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.transition.ChangeBounds;
import android.support.transition.TransitionManager;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by petr.kaminsky on 5.6.2018.
 */

public class TileBox extends ViewGroup {

    private static final int ANIMATION_DURATION = 50;
    private int dimension = 0;
    private float padding = 0;
    private List<Tile> tileList = new ArrayList<>();
    private Desk.ValueChangeListener deskValueChange = new Desk.ValueChangeListener() {
        @Override
        public void OnChange() {
            resetTiles();
            invalidate();
            requestLayout();
        }
    };
    private Desk.DimensionChangeListener deskDimensionChange = new Desk.DimensionChangeListener() {
        @Override
        public void OnChange() {
            setDimension(Desk.getInstance().getDimension());
        }
    };

    public TileBox(Context context) {
        super(context);

        initialize(context);
    }

    public TileBox(Context context, AttributeSet attrs) {
        super(context, attrs);

        obtainAttributes(context, attrs, 0);
        initialize(context);
    }

    public TileBox(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        obtainAttributes(context, attrs, defStyleAttr);
        initialize(context);
    }

    public void setDimension(int d) {
        dimension = d;
        resetTiles();
        invalidate();
        requestLayout();
    }

    public void refreshTiles() {
        for (Tile tile : tileList) {
            tile.invalidate();
        }
    }

    private void obtainAttributes(Context context, AttributeSet attrs, int defStyleAttr) {
        TypedArray typedArray = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.TileBox,
                defStyleAttr,
                0);
        try {
            dimension = typedArray.getInteger(R.styleable.TileBox_dimension, 0);
            padding = typedArray.getDimension(R.styleable.TileBox_android_padding, 0);
        } finally {
            typedArray.recycle();
        }
    }

    private void initialize(Context context) {
        if (!isInEditMode()) {
            dimension = Desk.getInstance().getDimension();
            resetTiles();
            Desk.getInstance().setDimensionChangeListener(deskDimensionChange);
            Desk.getInstance().setValueChangeListener(deskValueChange);
        }
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        final int count = getChildCount();

        if (count > 0) {
            final int childLeft = getPaddingLeft();
            final int childTop = getPaddingTop();
            final int childRight = getMeasuredWidth() - getPaddingRight();
            final int childBottom = getMeasuredHeight() - getPaddingBottom();
            final int childWidth = childRight - childLeft;
            final int childHeight = childBottom - childTop;
            final int boxSize = Math.min(childWidth, childHeight);
            final float tileSize = (boxSize - (dimension - 1) * padding) / dimension;
            for (int y = 0; y < dimension; y++) {
                for (int x = 0; x < dimension; x++) {
                    final View view = getChildAt(getIndex(x, y));
                    final int tileLeft = (int) (childLeft + x * (tileSize + padding));
                    final int tileTop = (int) (childTop + y * (tileSize + padding));
                    final int tileRight = (int) (tileLeft + tileSize);
                    final int tileBottom = (int) (tileTop + tileSize);
                    view.layout(
                            tileLeft,
                            tileTop,
                            tileRight,
                            tileBottom);
                }
            }
        }
    }

    private int getIndex(int x, int y) {
        return x + y * dimension;
    }

    private int getTileCount() {
        return dimension * dimension;
    }

    private void regenerateTiles() {
        tileList.clear();
        final int count = getTileCount();
        for (int i = 0; i < count; i++) {
            Tile tile = new Tile(getContext());
            tileList.add(tile);
            if (i < count - 1) {
                tile.setValue(i + 1);
                tile.setEmpty(false);
            } else {
                tile.setEmpty(true);
            }
        }
        refreshTiles();
    }

    private void resetTiles() {
        if (!isInEditMode()) {
            if (tileList.size() != getTileCount()) {
                regenerateTiles();
            }
            if (Properties.getInstance().isAnimate()) {
                final ChangeBounds transition = new ChangeBounds();
                transition.setDuration(ANIMATION_DURATION);
                TransitionManager.beginDelayedTransition(this, transition);
            }
            removeAllViews();
            for (int y = 0; y < dimension; y++) {
                for (int x = 0; x < dimension; x++) {
                    final int value = Desk.getInstance().getValue(x, y);
                    final Tile tile = tileList.get(value == Desk.EMPTY_VALUE
                            ? getTileCount() - 1
                            : value - 1);
                    tile.setTileLocation(x, y);
                    tile.updateState();
                    addView(tile);
                }
            }
            refreshTiles();
        }
    }
}
