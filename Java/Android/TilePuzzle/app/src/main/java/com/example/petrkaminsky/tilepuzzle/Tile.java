package com.example.petrkaminsky.tilepuzzle;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by petr.kaminsky on 5.6.2018.
 */

public class Tile extends View {

    private boolean empty = false;
    private int value = Desk.EMPTY_VALUE;
    private int solvedColor = Color.WHITE;
    private int notSolvedColor = Color.LTGRAY;
    private int distanceColor = Color.MAGENTA;
    private boolean solved = false;
    private int tileX = 0;
    private int tileY = 0;
    private int distance = 0;
    private Paint paintRect = null;
    private Paint paintFrame = null;
    private Path pathFrame = null;
    private Paint paintText = null;
    private Paint paintDistance = null;
    private OnClickListener onClick = new OnClickListener() {
        @Override
        public void onClick(View view) {
            if (!isInEditMode()) {
                if (!empty) {
                    Desk.getInstance().clickTile(tileX, tileY);
                }
            }
        }
    };

    public Tile(Context context) {
        super(context);

        initialize(context);
    }

    public Tile(Context context, AttributeSet attrs) {
        super(context, attrs);

        obtainAttributes(context, attrs, 0);
        initialize(context);
    }

    public Tile(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        obtainAttributes(context, attrs, defStyleAttr);
        initialize(context);
    }

    public void setTileLocation(int x, int y) {
        tileX = x;
        tileY = y;
    }

    public void setValue(int v) {
        value = v;
    }

    public void setEmpty(boolean e) {
        empty = e;
    }

    public void updateState() {
        solved = Desk.getInstance().isSolved(tileX, tileY);
        distance = Desk.getInstance().getDistance(tileX, tileY);
    }

    private void obtainAttributes(Context context, AttributeSet attrs, int defStyleAttr) {
        TypedArray typedArray = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.Tile,
                defStyleAttr,
                0);
        try {
            value = typedArray.getInteger(R.styleable.Tile_value, 0);
            empty = typedArray.getBoolean(R.styleable.Tile_empty, false);
            solvedColor = typedArray.getColor(R.styleable.Tile_solvedColor, Color.WHITE);
            notSolvedColor = typedArray.getColor(R.styleable.Tile_notSolvedColor, Color.LTGRAY);
            solved = typedArray.getBoolean(R.styleable.Tile_solved, false);
        } finally {
            typedArray.recycle();
        }
    }

    private void initialize(Context context) {
        paintRect = new Paint();
        paintRect.setStyle(Paint.Style.FILL);

        paintFrame = new Paint();
        paintFrame.setColor(Color.BLACK);
        paintFrame.setAntiAlias(true);
        paintFrame.setStyle(Paint.Style.STROKE);
        paintFrame.setStrokeJoin(Paint.Join.ROUND);
        paintFrame.setStrokeCap(Paint.Cap.ROUND);

        pathFrame = new Path();

        paintText = new Paint();
        paintText.setColor(Color.BLACK);
        paintText.setAntiAlias(true);
        paintText.setTypeface(Typeface.DEFAULT_BOLD);

        paintDistance = new Paint();
        paintDistance.setColor(distanceColor);
        paintDistance.setAntiAlias(true);

        setOnClickListener(onClick);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if (!empty) {
            float size = Math.min(getWidth(), getHeight());
            float strokeWidth = size / 15f;
            float rectRadius = strokeWidth / 2f;
            double dTextSize = size / 3.1f * getResources().getDisplayMetrics().density + 0.5f;
            float leftRect = rectRadius;
            float topRect = rectRadius;
            float rightRect = size - rectRadius;
            float bottomRect = size - rectRadius;

            paintRect.setColor(solved
                    ? solvedColor
                    : notSolvedColor);
            canvas.drawRect(leftRect, topRect, rightRect, bottomRect, paintRect);

            paintFrame.setStrokeWidth(strokeWidth);
            pathFrame.reset();
            pathFrame.moveTo(leftRect, topRect);
            pathFrame.lineTo(rightRect, topRect);
            pathFrame.lineTo(rightRect, bottomRect);
            pathFrame.lineTo(leftRect, bottomRect);
            pathFrame.lineTo(leftRect, topRect);
            canvas.drawPath(pathFrame, paintFrame);

            String sValue = Integer.toString(value);
            paintText.setTextSize((int) dTextSize);
            drawTextCenter(canvas, paintText, size, size, sValue);

            if (Properties.getInstance().isDistance()) {
                String sDistance = Integer.toString(distance);
                paintDistance.setTextSize((int) (dTextSize / 2f));
                drawTextTopRight(canvas, paintDistance, size, size, sDistance);
            }
        }
    }

    private void drawTextCenter(Canvas canvas, Paint paint, float width, float height, String text) {
        paint.setTextAlign(Paint.Align.LEFT);
        Rect r = new Rect();
        paint.getTextBounds(text, 0, text.length(), r);
        float x = width / 2f - r.width() / 2f - r.left;
        float y = height / 2f + r.height() / 2f - r.bottom;
        canvas.drawText(text, x, y, paint);
    }

    private void drawTextTopRight(Canvas canvas, Paint paint, float width, float height, String text) {
        paint.setTextAlign(Paint.Align.LEFT);
        Rect r = new Rect();
        paint.getTextBounds(text, 0, text.length(), r);
        float x = width - r.width() - width / 9f;
        float y = height / 9f + r.height();
        canvas.drawText(text, x, y, paint);
    }
}
