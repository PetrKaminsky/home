package com.example.petr.customviewtest;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.d("life", "MainActivity.onCreate()");

        setContentView(R.layout.activity_main);
    }
}
