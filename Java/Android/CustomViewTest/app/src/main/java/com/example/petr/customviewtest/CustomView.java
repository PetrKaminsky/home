package com.example.petr.customviewtest;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by petr on 10/29/17.
 */

public class CustomView extends View {

    private Paint paint = new Paint();
    private boolean inEditMode = false;

    public CustomView(Context context) {
        super(context);

        initialize();
    }

    public CustomView(Context context, AttributeSet attrs) {
        super(context, attrs);

        initialize();
    }

    public CustomView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        initialize();
    }

    private void initialize() {
        inEditMode = isInEditMode();
        paint.setDither(true);
        paint.setColor(Color.WHITE);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if (inEditMode) {
            return;
        }

        canvas.drawColor(Color.BLACK);

        int top = 0;
        int bottom = getHeight() - 1;
        int left = 0;
        int right = getWidth() - 1;
        canvas.drawLine(left, top, right, bottom, paint);
        canvas.drawLine(right, top, left, bottom, paint);
        canvas.drawLine(left, top, left, bottom, paint);
        canvas.drawLine(right, top, right, bottom, paint);
        canvas.drawLine(left, top, right, top, paint);
        canvas.drawLine(left, bottom, right, bottom, paint);
    }
}
