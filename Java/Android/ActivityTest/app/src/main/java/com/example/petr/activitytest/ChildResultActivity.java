package com.example.petr.activitytest;

import android.os.Bundle;
import android.view.View;

public class ChildResultActivity extends CommonActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_child_result);

        findViewById(R.id.buttonOK).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeActivity(RESULT_OK);
            }
        });
        findViewById(R.id.buttonCancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeActivity(RESULT_CANCELED);
            }
        });
    }
}
