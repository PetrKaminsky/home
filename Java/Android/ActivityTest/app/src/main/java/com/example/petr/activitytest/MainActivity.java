package com.example.petr.activitytest;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class MainActivity extends CommonActivity {

    private static final int CHILD_RESULT_REQUEST_CODE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        findViewById(R.id.buttonChild).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, ChildActivity.class));
            }
        });
        findViewById(R.id.buttonChildResult).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(MainActivity.this, ChildResultActivity.class), CHILD_RESULT_REQUEST_CODE);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CHILD_RESULT_REQUEST_CODE) {
            Log.d("trace", "MainActivity.onActivityResult(): " + resultCode + ", " + this);

            return;
        }

        super.onActivityResult(requestCode, resultCode, data);
    }
}
