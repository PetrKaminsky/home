package com.example.petr.activitytest;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import static android.R.id.home;

/**
 * Created by petr on 11/20/17.
 */

public class CommonActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.i("life", getClass().getSimpleName() + ".onCreate(): " + this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        Log.i("life", getClass().getSimpleName() + ".onDestroy(): " + this);
    }

    @Override
    protected void onStart() {
        super.onStart();

        Log.i("life", getClass().getSimpleName() + ".onStart(): " + this);
    }

    @Override
    protected void onStop() {
        super.onStop();

        Log.i("life", getClass().getSimpleName() + ".onStop(): " + this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        Log.i("life", getClass().getSimpleName() + ".onResume(): " + this);
    }

    @Override
    protected void onPause() {
        super.onPause();

        Log.i("life", getClass().getSimpleName() + ".onPause(): " + this);
    }

    @Override
    public void onBackPressed() {
        Log.d("trace", getClass().getSimpleName() + ".onBackPressed(): " + this);

        closeActivity();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.d("trace", getClass().getSimpleName() + ".onOptionsItemSelected(): " + this);

        switch (item.getItemId()) {
            case home:
                closeActivity();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    protected void closeActivity(int resultCode) {
        setResult(resultCode);
        finish();
    }

    protected void closeActivity() {
        closeActivity(RESULT_CANCELED);
    }
}
