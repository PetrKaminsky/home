package com.example.petr.activitytest;

import android.app.Application;
import android.util.Log;

/**
 * Created by petr on 11/20/17.
 */

public class CommonApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        Log.i("life", "CommonApplication.onCreate(): " + this);
    }
}
