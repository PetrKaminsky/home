package com.example.petrkaminsky.permissiontest;

import java.util.ArrayList;

/**
 * Created by petr.kaminsky on 30.5.2017.
 */

public class PermissionObject {
    public static final int INVALID_REQUEST_CODE = -1;
    private ArrayList<PermissionListener> permissionListeners = new ArrayList<>();
    private int requestCode = INVALID_REQUEST_CODE;

    public int getRequestCode() {
        return requestCode;
    }

    public void setRequestCode(int requestCode) {
        this.requestCode = requestCode;
    }

    public void addPermissionListener(PermissionListener listener) {
        if (listener != null) {
            permissionListeners.add(listener);
        }
    }

    public void clearPermissionListeners() {
        permissionListeners.clear();
    }

    public void invokeListenersGranted() {
        for (PermissionListener listener : permissionListeners) {
            if (listener != null) {
                listener.onGranted();
            }
        }
    }

    public void invokeListenersDenied() {
        for (PermissionListener listener : permissionListeners) {
            if (listener != null) {
                listener.onDenied();
            }
        }
    }
}
