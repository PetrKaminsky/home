package com.example.petrkaminsky.permissiontest;

/**
 * Created by petr.kaminsky on 30.5.2017.
 */

public interface PermissionListener {
    void onGranted();

    void onDenied();
}
