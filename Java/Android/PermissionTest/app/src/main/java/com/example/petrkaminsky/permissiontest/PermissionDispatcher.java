package com.example.petrkaminsky.permissiontest;

import android.annotation.TargetApi;
import android.app.Activity;
import android.os.Build;
import android.util.Log;

import java.util.Dictionary;
import java.util.Hashtable;

import static android.content.pm.PackageManager.PERMISSION_GRANTED;

/**
 * Created by petr.kaminsky on 30.5.2017.
 */

public class PermissionDispatcher {
    private static int REQUEST_COUNTER = 1;
    private static PermissionDispatcher instance = new PermissionDispatcher();
    private Dictionary<String, PermissionObject> permissionObjects = new Hashtable<>();

    private PermissionDispatcher() {
    }

    public static PermissionDispatcher getInstance() {
        return instance;
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        Log.d("permission", "onRequestPermissionsResult(), requestCode: " + requestCode);

        if (permissions != null && permissions.length > 0) {
            String permission = permissions[0];

            Log.d("permission", "onRequestPermissionsResult(), permission: " + permission);

            PermissionObject permissionObject = permissionObjects.get(permission);
            if (permissionObject != null) {
                if (permissionObject.getRequestCode() == requestCode) {
                    int grantResult = grantResults[0];

                    Log.d("permission", "onRequestPermissionsResult(), grantResult: " + grantResult);

                    if (grantResult == PERMISSION_GRANTED) {
                        permissionObject.invokeListenersGranted();
                    } else {
                        permissionObject.invokeListenersDenied();
                    }
                    permissionObject.clearPermissionListeners();
                    permissionObject.setRequestCode(PermissionObject.INVALID_REQUEST_CODE);
                }
            }
        }
    }

    public void checkPermission(Activity activity, String permission, PermissionListener listener) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            invokeListenersGranted(listener);

            return;
        }

        if (activity == null) {
            invokeListenersDenied(listener);

            return;
        }

        if (checkPermission(activity, permission)) {
            invokeListenersGranted(listener);

            return;
        }

        PermissionObject permissionObject = permissionObjects.get(permission);
        if (permissionObject == null) {
            permissionObject = new PermissionObject();
            permissionObjects.put(permission, permissionObject);
        }

        permissionObject.addPermissionListener(listener);

        if (permissionObject.getRequestCode() == PermissionObject.INVALID_REQUEST_CODE) {
            int requestCode = REQUEST_COUNTER;
            REQUEST_COUNTER++;
            permissionObject.setRequestCode(requestCode);
            requestPermission(activity, permission, requestCode);
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void requestPermission(Activity activity, String permission, int requestCode) {
        Log.d("permission", "requestPermission(): " + permission);

        activity.requestPermissions(new String[]{permission}, requestCode);
    }

    @TargetApi(Build.VERSION_CODES.M)
    private boolean checkPermission(Activity activity, String permission) {
        Log.d("permission", "checkPermission(): " + permission);

        return activity.checkSelfPermission(permission) == PERMISSION_GRANTED;
    }

    private void invokeListenersGranted(PermissionListener listener) {
        if (listener != null) {
            listener.onGranted();
        }
    }

    private void invokeListenersDenied(PermissionListener listener) {
        if (listener != null) {
            listener.onDenied();
        }
    }
}
