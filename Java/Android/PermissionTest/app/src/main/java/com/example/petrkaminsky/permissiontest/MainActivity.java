package com.example.petrkaminsky.permissiontest;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;

public class MainActivity extends Activity {

    private View.OnClickListener buttonCameraPermissionClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            PermissionDispatcher.getInstance().checkPermission(MainActivity.this,
                    Manifest.permission.CAMERA,
                    new PermissionListener() {
                        @Override
                        public void onGranted() {
                            Log.d("permission", Manifest.permission.CAMERA + " granted");
                        }

                        @Override
                        public void onDenied() {
                            Log.d("permission", Manifest.permission.CAMERA + " denied");
                        }
                    });
        }
    };
    private View.OnClickListener buttonInternetPermissionClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            PermissionDispatcher.getInstance().checkPermission(MainActivity.this,
                    Manifest.permission.INTERNET,
                    new PermissionListener() {
                        @Override
                        public void onGranted() {
                            Log.d("permission", Manifest.permission.INTERNET + " granted");
                        }

                        @Override
                        public void onDenied() {
                            Log.d("permission", Manifest.permission.INTERNET + " denied");
                        }
                    });
        }
    };
    private View.OnClickListener buttonReadExternalPermissionClick = new View.OnClickListener() {
        @Override
        @TargetApi(16)
        public void onClick(View v) {
            PermissionDispatcher.getInstance().checkPermission(MainActivity.this,
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    new PermissionListener() {
                        @Override
                        public void onGranted() {
                            Log.d("permission", Manifest.permission.READ_EXTERNAL_STORAGE + " granted");
                        }

                        @Override
                        public void onDenied() {
                            Log.d("permission", Manifest.permission.READ_EXTERNAL_STORAGE + " denied");
                        }
                    });
        }
    };
    private View.OnClickListener buttonWriteExternalPermissionClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            PermissionDispatcher.getInstance().checkPermission(MainActivity.this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    new PermissionListener() {
                        @Override
                        public void onGranted() {
                            Log.d("permission", Manifest.permission.WRITE_EXTERNAL_STORAGE + " granted");
                        }

                        @Override
                        public void onDenied() {
                            Log.d("permission", Manifest.permission.WRITE_EXTERNAL_STORAGE + " denied");
                        }
                    });
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        findViewById(R.id.buttonCameraPermission)
                .setOnClickListener(buttonCameraPermissionClick);
        findViewById(R.id.buttonInternetPermission)
                .setOnClickListener(buttonInternetPermissionClick);
        findViewById(R.id.buttonReadExternalPermission)
                .setOnClickListener(buttonReadExternalPermissionClick);
        findViewById(R.id.buttonWriteExternalPermission)
                .setOnClickListener(buttonWriteExternalPermissionClick);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        PermissionDispatcher.getInstance().onRequestPermissionsResult(requestCode, permissions,
                grantResults);
    }
}
