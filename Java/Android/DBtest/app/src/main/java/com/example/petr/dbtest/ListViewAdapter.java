package com.example.petr.dbtest;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Locale;

/**
 * Created by petr on 2/23/17.
 */

public class ListViewAdapter extends BaseAdapter {

    private ListViewData data = null;
    private SortState.StateListener firstNameStateListener = new SortState.StateListener() {
        @Override
        public void onChanged(Sort state) {
            getData().setFirstNameSort(state);

            notifyDataSetChanged();
        }
    };
    private SortState.StateListener lastNameStateListener = new SortState.StateListener() {
        @Override
        public void onChanged(Sort state) {
            getData().setLastNameSort(state);

            notifyDataSetChanged();
        }
    };
    private SortState.StateListener ageStateListener = new SortState.StateListener() {
        @Override
        public void onChanged(Sort state) {
            getData().setAgeSort(state);

            notifyDataSetChanged();
        }
    };

    public ListViewAdapter(ListViewData d) {
        super();

        data = d;
    }

    public static void setAgeText(TextView textView, ListViewItem item) {
        Integer age = item.getAge();
        textView.setText(age == null ? "" : age.toString());
    }

    public static void setItemAge(String ageText, ListViewItem item) {
        if (!ageText.isEmpty()) {
            try {
                Integer age = Integer.parseInt(ageText);
                item.setAge(age);
            } catch (Exception exc) {
                Log.e("format", "ListViewAdapter.setItemAge()", exc);
            }
        }
    }

    @Override
    public int getCount() {
        return getData().getCount();
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        if (view == null) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_view_item, parent, false);
        }
        final TextView textViewNumber = (TextView) view.findViewById(R.id.textViewNumber);
        final TextView textViewFirstName = (TextView) view.findViewById(R.id.textViewFirstName);
        final TextView textViewLastName = (TextView) view.findViewById(R.id.textViewLastName);
        final TextView textViewAge = (TextView) view.findViewById(R.id.textViewAge);
        final TextView textViewCreated = (TextView) view.findViewById(R.id.textViewCreated);
        ListViewItem item = (ListViewItem) getItem(position);
        textViewNumber.setText(Integer.toString(position));
        textViewFirstName.setText(item.getFirstName());
        textViewLastName.setText(item.getLastName());
        setAgeText(textViewAge, item);
        textViewCreated.setText(new SimpleDateFormat("dd.MM.yyyy HH:mm:ss", Locale.US)
                .format(item.getDateCreated()));

        return view;
    }

    @Override
    public long getItemId(int position) {
        return getData().getItemId(position);
    }

    @Override
    public Object getItem(int position) {
        return getData().getItemByPosition(position);
    }

    private ListViewData getData() {
        return data;
    }

    public void delete(int position) {
        getData().delete(position);

        reloadData();
    }

    public SortState.StateListener getFirstNameStateListener() {
        return firstNameStateListener;
    }

    public SortState.StateListener getLastNameStateListener() {
        return lastNameStateListener;
    }

    public SortState.StateListener getAgeStateListener() {
        return ageStateListener;
    }

    public void reloadData() {
        getData().reload();

        notifyDataSetChanged();
    }

    public void addRandomItem() {
        ListViewItem item = new ListViewItem();
        RandomItem randomItem = new RandomItem();
        item.setFirstName(randomItem.getFirstName());
        item.setLastName(randomItem.getLastName());
        item.setAge(randomItem.getAge());
        getData().add(item);

        reloadData();
    }

    public void addNRandomItems() {
        getData().beginTransaction();

        for (int i = 0; i < 10000; i++) {
            ListViewItem item = new ListViewItem();
            RandomItem randomItem = new RandomItem();
            item.setFirstName(randomItem.getFirstName());
            item.setLastName(randomItem.getLastName());
            item.setAge(randomItem.getAge());
            getData().add(item);
        }

        getData().commitTransaction();
        getData().endTransaction();

        reloadData();
    }

    public void deleteAll() {
        getData().deleteAll();

        reloadData();
    }
}
