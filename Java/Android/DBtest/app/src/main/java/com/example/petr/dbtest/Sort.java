package com.example.petr.dbtest;

/**
 * Created by petr on 2/26/17.
 */

public enum Sort {
    UNSPECIFIED,
    ASCENDING,
    DESCENDING
}
