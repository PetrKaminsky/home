package com.example.petr.dbtest;

/**
 * Created by petr on 2/25/17.
 */

public class SortState {

    private Sort sortState = Sort.UNSPECIFIED;
    private StateListener stateListener = null;

    public void setStateListener(StateListener listener) {
        stateListener = listener;
    }

    public void switchState() {
        switch (sortState) {
            case UNSPECIFIED:
                sortState = Sort.ASCENDING;
                break;

            case ASCENDING:
                sortState = Sort.DESCENDING;
                break;

            case DESCENDING:
                sortState = Sort.UNSPECIFIED;
                break;
        }

        informStateListener(sortState);
    }

    private void informStateListener(Sort state) {
        if (stateListener != null) {
            stateListener.onChanged(state);
        }
    }

    public void refresh() {
        informStateListener(sortState);
    }

    public interface StateListener {
        void onChanged(Sort state);
    }
}
