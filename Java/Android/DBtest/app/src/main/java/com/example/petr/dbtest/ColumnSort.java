package com.example.petr.dbtest;

/**
 * Created by petr on 2/26/17.
 */

public class ColumnSort {

    private static final String ASCENDING_TEXT = "ASC";
    private static final String DESCENDING_TEXT = "DESC";
    private String columnName = null;
    private Sort state = Sort.UNSPECIFIED;

    public ColumnSort(String columnName) {
        this.columnName = columnName;
    }

    private static String formatColumnSort(String columnName, String sortText) {
        if (columnName != null && !columnName.isEmpty()) {
            return columnName + " " + sortText;
        }

        return "";
    }

    public void setState(Sort state) {
        this.state = state;
    }

    @Override
    public String toString() {
        switch (state) {
            case ASCENDING:
                return formatColumnSort(columnName, ASCENDING_TEXT);

            case DESCENDING:
                return formatColumnSort(columnName, DESCENDING_TEXT);
        }

        return "";
    }
}
