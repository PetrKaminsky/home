package com.example.petr.dbtest;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;

public class MainActivity extends Activity {

    private ListViewAdapter adapter = null;
    private SortFragment fragmentSortFirstName = null;
    private SortFragment fragmentSortLastName = null;
    private SortFragment fragmentSortAge = null;
    private Switch switchVisibleHeaderPane = null;
    private LinearLayout layoutHeaderPane = null;
    private TextView textViewCount = null;
    private View.OnClickListener onSwitchVisibleHeaderPaneClickListener =
            new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    MainActivityData.getInstance()
                            .setHeaderPaneVisible(switchVisibleHeaderPane.isChecked());
                    refreshHeaderPane();
                }
            };
    private SortFragment.SwitchListener firstNameSwitchListener =
            new SortFragment.SwitchListener() {
                @Override
                public void OnSwitched() {
                    getFirstNameSortState().switchState();
                }
            };
    private SortFragment.SwitchListener lastNameSwitchListener =
            new SortFragment.SwitchListener() {
                @Override
                public void OnSwitched() {
                    getLastNameSortState().switchState();
                }
            };
    private SortFragment.SwitchListener ageSwitchListener =
            new SortFragment.SwitchListener() {
                @Override
                public void OnSwitched() {
                    getAgeSortState().switchState();
                }
            };
    private SortState.StateListener firstNameStateListener =
            new SortState.StateListener() {
                @Override
                public void onChanged(Sort state) {
                    adapter.getFirstNameStateListener().onChanged(state);
                    fragmentSortFirstName.setIndicatorState(state);
                }
            };
    private SortState.StateListener lastNameStateListener =
            new SortState.StateListener() {
                @Override
                public void onChanged(Sort state) {
                    adapter.getLastNameStateListener().onChanged(state);
                    fragmentSortLastName.setIndicatorState(state);
                }
            };
    private SortState.StateListener ageStateListener =
            new SortState.StateListener() {
                @Override
                public void onChanged(Sort state) {
                    adapter.getAgeStateListener().onChanged(state);
                    fragmentSortAge.setIndicatorState(state);
                }
            };
    private AdapterView.OnItemClickListener onListViewItemClickListener =
            new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent intent = new Intent(MainActivity.this, EditActivity.class);
                    intent.putExtra(EditActivity.ID_KEY_NAME, id);
                    startActivityForResult(intent, EditActivity.EDIT_REQUEST_CODE);
                }
            };
    private AdapterView.OnItemLongClickListener onListViewItemLongClickListener =
            new AdapterView.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                    adapter.delete(position);
                    updateCount();

                    return true;
                }
            };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        ListViewData data = ListViewData.getInstance(this.getApplicationContext());
        adapter = new ListViewAdapter(data);
        getFirstNameSortState().setStateListener(firstNameStateListener);
        getLastNameSortState().setStateListener(lastNameStateListener);
        getAgeSortState().setStateListener(ageStateListener);
        final ListView listViewItems = (ListView) findViewById(R.id.listViewItems);
        fragmentSortFirstName = (SortFragment) getFragmentManager()
                .findFragmentById(R.id.fragmentSortFirstName);
        fragmentSortFirstName.setButtonText(getString(R.string.text_first_name));
        fragmentSortFirstName.setSwitchListener(firstNameSwitchListener);
        fragmentSortLastName = (SortFragment) getFragmentManager()
                .findFragmentById(R.id.fragmentSortLastName);
        fragmentSortLastName.setButtonText(getString(R.string.text_last_name));
        fragmentSortLastName.setSwitchListener(lastNameSwitchListener);
        fragmentSortAge = (SortFragment) getFragmentManager()
                .findFragmentById(R.id.fragmentSortAge);
        fragmentSortAge.setButtonText(getString(R.string.text_age));
        fragmentSortAge.setSwitchListener(ageSwitchListener);
        listViewItems.setOnItemClickListener(onListViewItemClickListener);
        listViewItems.setOnItemLongClickListener(onListViewItemLongClickListener);
        listViewItems.setAdapter(adapter);
        switchVisibleHeaderPane = (Switch) findViewById(R.id.switchVisibleHeaderPane);
        switchVisibleHeaderPane.setOnClickListener(onSwitchVisibleHeaderPaneClickListener);
        switchVisibleHeaderPane.setChecked(MainActivityData.getInstance().isHeaderPaneVisible());
        layoutHeaderPane = (LinearLayout) findViewById(R.id.layoutHeaderPane);
        textViewCount = (TextView) findViewById(R.id.textViewCount);

        getFirstNameSortState().refresh();
        getLastNameSortState().refresh();
        getAgeSortState().refresh();
        refreshHeaderPane();
        updateCount();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == EditActivity.EDIT_REQUEST_CODE &&
                resultCode == RESULT_OK) {
            adapter.reloadData();
            updateCount();

            return;
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        return true;
    }

    @Override
    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add:
                Intent intent = new Intent(MainActivity.this, EditActivity.class);
                startActivityForResult(intent, EditActivity.EDIT_REQUEST_CODE);

                return true;

            case R.id.action_add_random:
                adapter.addRandomItem();
                updateCount();

                return true;

            case R.id.action_add_n_random:
                adapter.addNRandomItems();
                updateCount();

                return true;

            case R.id.action_delete_all:
                adapter.deleteAll();
                updateCount();

                return true;
        }

        return super.onMenuItemSelected(featureId, item);
    }

    private void refreshHeaderPane() {
        layoutHeaderPane.setVisibility(
                MainActivityData.getInstance().isHeaderPaneVisible()
                        ? View.VISIBLE
                        : View.GONE);
    }

    private void updateCount() {
        textViewCount.setText("(" + adapter.getCount() + ")");
    }

    private SortState getFirstNameSortState() {
        return MainActivityData.getInstance().getFirstNameSortState();
    }

    private SortState getLastNameSortState() {
        return MainActivityData.getInstance().getLastNameSortState();
    }

    private SortState getAgeSortState() {
        return MainActivityData.getInstance().getAgeSortState();
    }

    private static class MainActivityData {
        private static final MainActivityData instance = new MainActivityData();
        private SortState firstNameSortState = new SortState();
        private SortState lastNameSortState = new SortState();
        private SortState ageSortState = new SortState();
        private boolean headerPaneVisible = false;

        private MainActivityData() {
        }

        public static MainActivityData getInstance() {
            return instance;
        }

        public SortState getFirstNameSortState() {
            return firstNameSortState;
        }

        public SortState getLastNameSortState() {
            return lastNameSortState;
        }

        public SortState getAgeSortState() {
            return ageSortState;
        }

        public boolean isHeaderPaneVisible() {
            return headerPaneVisible;
        }

        public void setHeaderPaneVisible(boolean visible) {
            headerPaneVisible = visible;
        }
    }
}
