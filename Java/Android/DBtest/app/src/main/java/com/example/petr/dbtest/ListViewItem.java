package com.example.petr.dbtest;

import java.util.Date;

/**
 * Created by petr on 2/23/17.
 */

public class ListViewItem {

    public static final long INVALID_ID = -1;
    private long id = INVALID_ID;
    private String firstName = null;
    private String lastName = null;
    private Integer age = null;
    private Date dateCreated = new Date();

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }
}
