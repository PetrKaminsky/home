package com.example.petr.dbtest;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

/**
 * A simple {@link Fragment} subclass.
 */
public class SortFragment extends Fragment {

    private Button buttonSortSwitch = null;
    private SwitchListener switchListener = null;
    private View.OnClickListener onButtonSortSwitchClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            informSwitchListener();
        }
    };

    public SortFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sort, container, false);
        buttonSortSwitch = (Button) view.findViewById(R.id.buttonSortSwitch);
        buttonSortSwitch.setOnClickListener(onButtonSortSwitchClickListener);
        return view;
    }

    private void informSwitchListener() {
        if (switchListener != null) {
            switchListener.OnSwitched();
        }
    }

    public void setButtonText(String text) {
        buttonSortSwitch.setText(text);
    }

    public void setSwitchListener(SwitchListener listener) {
        switchListener = listener;
    }

    public void setIndicatorState(Sort state) {
        int id = 0;
        switch (state) {
            case ASCENDING:
                id = R.mipmap.ic_asc;
                break;

            case DESCENDING:
                id = R.mipmap.ic_desc;
                break;
        }

        buttonSortSwitch.setCompoundDrawablesWithIntrinsicBounds(0, 0, id, 0);
    }

    public interface SwitchListener {
        void OnSwitched();
    }
}
