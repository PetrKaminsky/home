package com.example.petr.dbtest;

import java.util.ArrayList;

/**
 * Created by petr on 2/26/17.
 */

public class ColumnSortBuilder {

    private ArrayList<ColumnSort> listSort = new ArrayList<>();

    public ColumnSortBuilder add(ColumnSort sort) {
        listSort.add(sort);

        return this;
    }

    @Override
    public String toString() {
        String result = "";
        for (ColumnSort sort : listSort) {
            String sortText = sort.toString();
            if (!sortText.isEmpty()) {
                if (!result.isEmpty()) {
                    result += ", ";
                }
                result += sortText;
            }
        }
        if (!result.isEmpty()) {
            result = " ORDER BY " + result;
        }

        return result;
    }
}
