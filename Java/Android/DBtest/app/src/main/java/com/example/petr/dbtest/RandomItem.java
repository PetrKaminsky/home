package com.example.petr.dbtest;

import java.util.Date;
import java.util.Random;

/**
 * Created by petr.kaminsky on 15.5.2017.
 */

public class RandomItem {

    private static final char[] lowerCase = new char[]{'a', 'b', 'c', 'd', 'e', 'f', 'g', 'i', 'j',
            'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};
    private static final char[] upperCase = new char[]{'A', 'B', 'C', 'D', 'E', 'F', 'G', 'I', 'J',
            'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};
    private static final int minFirstNameLength = 3;
    private static final int maxFirstNameLength = 10;
    private static final int minLastNameLength = 5;
    private static final int maxLastNameLength = 15;
    private static final int minAge = 10;
    private static final int maxAge = 99;
    private Random random = new Random();

    private int getRandomInt(int fromValue, int toValue) {
        return fromValue + (int) (random.nextDouble() * (toValue - fromValue));
    }

    public String getFirstName() {
        int length = getRandomInt(minFirstNameLength, maxFirstNameLength);
        int charIndex = getRandomInt(0, upperCase.length);
        StringBuilder result = new StringBuilder(length + 1);
        result.append(upperCase[charIndex]);
        while (length > 0) {
            result.append(lowerCase[charIndex]);
            length--;
        }

        return result.toString();
    }

    public String getLastName() {
        int length = getRandomInt(minLastNameLength, maxLastNameLength);
        int charIndex = getRandomInt(0, upperCase.length);
        StringBuilder result = new StringBuilder(length + 1);
        result.append(upperCase[charIndex]);
        while (length > 0) {
            result.append(lowerCase[charIndex]);
            length--;
        }

        return result.toString();
    }

    public int getAge() {
        return getRandomInt(minAge, maxAge);
    }
}
