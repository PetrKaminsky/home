package com.example.petrkaminsky.mapviewtest;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.Date;

public class MapsActivity extends FragmentActivity {

    private static final int PERMISSION_REQUEST = 1;
    private LocationManager locationManager = null;
    private String locationProvider = null;
    private GoogleMap map = null;
    private Location lastLocation = null;
    private boolean firstLocation = true;
    private LocationListener locationListener =
            new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    Date date = new Date(location.getTime());
                    Log.d("location", "onLocationChanged()" +
                            ", latitude: " + location.getLatitude() +
                            ", longitude: " + location.getLongitude() +
                            ", time: " + date);

                    if (map != null) {
                        if (firstLocation) {
                            map.setMinZoomPreference(10.0f);
                            map.setMaxZoomPreference(20.0f);
                        }

                        // Add a marker in my location and move the camera
                        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
                        map.clear();
                        map.addMarker(new MarkerOptions().position(latLng).title(date.toString()));
                        map.moveCamera(CameraUpdateFactory.newLatLng(latLng));

                        if (firstLocation) {
                            map.moveCamera(CameraUpdateFactory.zoomTo(18.0f));
                        }

                        firstLocation = false;
                    }
                }

                @Override
                public void onStatusChanged(String provider, int status, Bundle extras) {
                    Log.d("location", "onStatusChanged(), provider: " + provider + ", status: " + status);
                }

                @Override
                public void onProviderEnabled(String provider) {
                    Log.d("location", "onProviderEnabled(), provider: " + provider);
                }

                @Override
                public void onProviderDisabled(String provider) {
                    Log.d("location", "onProviderDisabled(), provider: " + provider);
                }
            };
    private OnMapReadyCallback mapReadyCallback =
            new OnMapReadyCallback() {
                /**
                 * Manipulates the map once available.
                 * This mapReadyCallback is triggered when the map is ready to be used.
                 * This is where we can add markers or lines, add listeners or move the camera.
                 * If Google Play services is not installed on the device, the user will be prompted to install
                 * it inside the SupportMapFragment. This method will only be triggered once the user has
                 * installed Google Play services and returned to the app.
                 */
                @Override
                public void onMapReady(GoogleMap googleMap) {
                    map = googleMap;

                    if (lastLocation != null) {
                        locationListener.onLocationChanged(lastLocation);
                    }
                }
            };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(mapReadyCallback);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (isLocationAllowed()) {
                setupLocation();
            } else {
                requestPermission();
            }
        } else {
            setupLocation();
        }
    }

    private void setupLocation() {
        initLocationManager();

        lastLocation = getLastKnownLocation();

        if (lastLocation != null) {
            locationListener.onLocationChanged(lastLocation);
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void requestPermission() {
        requestPermissions(new String[]{
                        Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                PERMISSION_REQUEST);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    setupLocation();
                }
                return;
        }

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onResume() {
        super.onResume();

        startLocationRequests();
    }

    @Override
    protected void onPause() {
        super.onPause();

        stopLocationRequests();
    }

    private boolean isLocationAllowed() {
        return ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED;
    }

    private void initLocationManager() {
        if (isLocationAllowed()) {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        }

        if (locationManager != null) {
            Criteria criteria = new Criteria();
            criteria.setAccuracy(Criteria.ACCURACY_FINE);
            locationProvider = locationManager.getBestProvider(criteria, true);
        }
    }

    private void startLocationRequests() {
        try {
            if (locationManager != null) {
                locationManager.requestLocationUpdates(locationProvider, 0, 0, locationListener);
            }
        } catch (SecurityException e) {
            Log.e("error", getClass().getSimpleName() + ".startLocationRequests()", e);
        }
    }

    private Location getLastKnownLocation() {
        try {
            if (locationManager != null) {
                return locationManager.getLastKnownLocation(locationProvider);
            }
        } catch (SecurityException e) {
            Log.e("error", getClass().getSimpleName() + ".getLastKnownLocation()", e);
        }

        return null;
    }

    private void stopLocationRequests() {
        try {
            if (locationManager != null) {
                locationManager.removeUpdates(locationListener);
            }
        } catch (Exception e) {
            Log.e("error", getClass().getSimpleName() + ".stopLocationRequests()", e);
        }
    }
}
