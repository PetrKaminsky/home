package com.example.petrkaminsky.layouttest;

import android.os.Bundle;

public class Child2Activity extends CommonActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        setContentView(R.layout.activity_child2);
    }
}
