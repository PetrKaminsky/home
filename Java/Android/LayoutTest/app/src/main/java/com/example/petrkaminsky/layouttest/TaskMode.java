package com.example.petrkaminsky.layouttest;

/**
 * Created by petr.kaminsky on 26.2.2018.
 */

public class TaskMode {
    public static final int Unknown = 0;
    public static final int Cargo = 1;
    public static final int Parcel = 2;
}
