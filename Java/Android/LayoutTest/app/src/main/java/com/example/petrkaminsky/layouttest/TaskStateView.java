package com.example.petrkaminsky.layouttest;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

public class TaskStateView extends LinearLayout {

    private Button buttonCompleteTask = null;
    private Button buttonCancelTask = null;
    private Button buttonTaskCompleted = null;
    private Button buttonTaskCanceled = null;
    private Runnable completeRunnable = null;
    private Runnable cancelRunnable = null;
    private int taskState = TaskState.Unknown;
    private String taskCompletedText = null;
    private String taskCanceledText = null;

    public TaskStateView(Context context) {
        super(context);
        init(null, 0);
    }

    public TaskStateView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs, 0);
    }

    public TaskStateView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs, defStyle);
    }

    public void setCompleteRunnable(Runnable runnable) {
        completeRunnable = runnable;
    }

    public void setCancelRunnable(Runnable runnable) {
        cancelRunnable = runnable;
    }

    public void setTaskState(int state) {
        taskState = state;
        updateButtons();
    }

    private void init(AttributeSet attrs, int defStyle) {
        // Load attributes
        final TypedArray a = getContext().obtainStyledAttributes(
                attrs, R.styleable.TaskStateView, defStyle, 0);
        if (a.hasValue(R.styleable.TaskStateView_taskCompletedText)) {
            taskCompletedText = a.getString(
                    R.styleable.TaskStateView_taskCompletedText);
        }
        if (a.hasValue(R.styleable.TaskStateView_taskCanceledText)) {
            taskCanceledText = a.getString(
                    R.styleable.TaskStateView_taskCanceledText);
        }
        if (a.hasValue(R.styleable.TaskStateView_taskState)) {
            taskState = a.getInt(
                    R.styleable.TaskStateView_taskCanceledText,
                    TaskState.Unknown);
        }
        a.recycle();

        LayoutInflater.from(getContext()).inflate(R.layout.task_state_view, this);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        buttonCompleteTask = (Button) findViewById(R.id.buttonCompleteTask);
        buttonCompleteTask.setText(taskCompletedText);
        buttonCompleteTask.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                setTaskState(TaskState.Completed);

                if (completeRunnable != null) {
                    completeRunnable.run();
                }
            }
        });
        buttonCancelTask = (Button) findViewById(R.id.buttonCancelTask);
        buttonCancelTask.setText(taskCanceledText);
        buttonCancelTask.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                setTaskState(TaskState.Canceled);

                if (cancelRunnable != null) {
                    cancelRunnable.run();
                }
            }
        });
        buttonTaskCompleted = (Button) findViewById(R.id.buttonTaskCompleted);
        buttonTaskCompleted.setText(taskCompletedText);
        buttonTaskCanceled = (Button) findViewById(R.id.buttonTaskCanceled);
        buttonTaskCanceled.setText(taskCanceledText);
        setTaskState(taskState);
    }

    private void updateButtons() {
        switch (taskState) {
            case TaskState.Unknown:
                buttonCompleteTask.setVisibility(VISIBLE);
                buttonCancelTask.setVisibility(VISIBLE);
                buttonTaskCompleted.setVisibility(GONE);
                buttonTaskCanceled.setVisibility(GONE);
                break;

            case TaskState.Completed:
                buttonCompleteTask.setVisibility(GONE);
                buttonCancelTask.setVisibility(GONE);
                buttonTaskCompleted.setVisibility(VISIBLE);
                buttonTaskCanceled.setVisibility(GONE);
                break;

            case TaskState.Canceled:
                buttonCompleteTask.setVisibility(GONE);
                buttonCancelTask.setVisibility(GONE);
                buttonTaskCompleted.setVisibility(GONE);
                buttonTaskCanceled.setVisibility(VISIBLE);
                break;
        }
    }
}
