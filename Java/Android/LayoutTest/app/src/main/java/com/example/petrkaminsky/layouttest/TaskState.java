package com.example.petrkaminsky.layouttest;

public class TaskState {
    public static final int Unknown = 0;
    public static final int Completed = 1;
    public static final int Canceled = 2;
}
