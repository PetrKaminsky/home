package com.example.petrkaminsky.layouttest;

/**
 * Created by petr.kaminsky on 26.2.2018.
 */

public class TaskModeWrapper {
    private static final TaskModeWrapper ourInstance = new TaskModeWrapper();
    private int currentMode = TaskMode.Unknown;

    private TaskModeWrapper() {
    }

    public static TaskModeWrapper getInstance() {
        return ourInstance;
    }

    public int getCurrentMode() {
        return currentMode;
    }

    public void setCurrentMode(int mode) {
        currentMode = mode;
    }

    public void toggleMode() {
        switch (currentMode) {
            case TaskMode.Cargo:
                setCurrentMode(TaskMode.Parcel);
                break;

            case TaskMode.Parcel:
                setCurrentMode(TaskMode.Cargo);
                break;
        }
    }

    public void invalidate() {
        setCurrentMode(TaskMode.Unknown);
    }
}
