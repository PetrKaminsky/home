package com.example.petrkaminsky.layouttest;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends CommonActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        setContentView(R.layout.activity_main);

        findViewById(R.id.buttonChild1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Child1Activity.class);
                startActivityForResult(intent, RequestCode.Child1);
            }
        });
        findViewById(R.id.buttonChild2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Child2Activity.class);
                startActivityForResult(intent, RequestCode.Child2);
            }
        });
        findViewById(R.id.buttonChild3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Child3Activity.class);
                startActivityForResult(intent, RequestCode.Child3);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case RequestCode.Child1:
                break;

            case RequestCode.Child2:
                break;

            case RequestCode.Child3:
                break;
        }
    }
}
