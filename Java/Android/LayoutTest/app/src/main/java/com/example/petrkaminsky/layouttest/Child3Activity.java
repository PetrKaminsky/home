package com.example.petrkaminsky.layouttest;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.MenuItem;
import android.view.View;

import java.util.Arrays;

import static android.R.id.home;

public class Child3Activity extends CommonActivity {

    private OperationState operationState = Child3ActivityData.getInstance().getOperationState();
    private BottomToolbar bottomToolbar = null;
    private TaskStateView taskStateView = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        setContentView(R.layout.activity_child3);
        bottomToolbar = (BottomToolbar) findViewById(R.id.bottomToolbar);
        taskStateView = (TaskStateView) findViewById(R.id.taskStateView);
        taskStateView.setCompleteRunnable(new Runnable() {
            @Override
            public void run() {
                operationState.change2();
            }
        });
        taskStateView.setCancelRunnable(new Runnable() {
            @Override
            public void run() {
                operationState.change3();
            }
        });

        operationState.setChangeListener(new StateMachine.StateChangeAdapter<State, Event>() {
            @Override
            public void onPostChangeState(State newState) {
                super.onPostChangeState(newState);

                switch (newState) {
                    case STATE_1:
                    case STATE_2:
                    case STATE_3:
                        updateStateView();
                        initializeFragments();
                        break;
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        initializeState();
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        updateStateView();
        initializeFragments();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case home:
                cleanUpAndFinish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        cleanUpAndFinish();
    }

    private void cleanUpAndFinish() {
        cleanUp();
        finish();
    }

    private void cleanUp() {
        operationState.cleanUp();
    }

    private void initializeState() {
        switch (operationState.getState()) {
            case INITIAL:
                operationState.change1();
                break;
        }
    }

    private void initializeFragments() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragmentHolder);

        switch (operationState.getState()) {
            case STATE_1:
                Fragment1 fragment1 = Utils.reuseFragment(fragment, Fragment1.class);
                fragment = fragment1;
                bottomToolbar.setTaskModeViewEnabled(true);
                bottomToolbar.setButtonScanVisible(false);
                break;

            case STATE_2:
                Fragment2 fragment2 = Utils.reuseFragment(fragment, Fragment2.class);
                fragment = fragment2;
                bottomToolbar.setTaskModeViewEnabled(false);
                bottomToolbar.setButtonScanVisible(true);
                break;

            case STATE_3:
                Fragment3 fragment3 = Utils.reuseFragment(fragment, Fragment3.class);
                fragment = fragment3;
                bottomToolbar.setTaskModeViewEnabled(false);
                bottomToolbar.setButtonScanVisible(true);
                break;
        }

        if (fragment != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragmentHolder, fragment)
                    .commit();
        }
    }

    private void updateStateView() {
        switch (operationState.getState()) {
            case INITIAL:
                taskStateView.setVisibility(View.GONE);
                taskStateView.setTaskState(TaskState.Unknown);
                break;

            case STATE_1:
                taskStateView.setVisibility(View.VISIBLE);
                taskStateView.setTaskState(TaskState.Unknown);
                break;

            case STATE_2:
                taskStateView.setVisibility(View.VISIBLE);
                taskStateView.setTaskState(TaskState.Completed);
                break;

            case STATE_3:
                taskStateView.setVisibility(View.VISIBLE);
                taskStateView.setTaskState(TaskState.Canceled);
                break;
        }
    }

    private enum State {
        INITIAL,
        STATE_1,
        STATE_2,
        STATE_3
    }

    private enum Event {
        CHANGE_1,
        CHANGE_2,
        CHANGE_3,
        CLEAN_UP
    }

    private static class OperationState extends StateMachine<State, Event> {
        public OperationState() {
            super(State.INITIAL);
        }

        @Override
        protected void initStateChart() {
            statechart = Arrays.asList(
                    new Edge<>(State.INITIAL, Event.CHANGE_1, State.STATE_1),
                    new Edge<>(State.STATE_1, Event.CHANGE_2, State.STATE_2),
                    new Edge<>(State.STATE_1, Event.CHANGE_3, State.STATE_3),
                    new Edge<>(State.STATE_2, Event.CLEAN_UP, State.INITIAL),
                    new Edge<>(State.STATE_3, Event.CLEAN_UP, State.INITIAL)
            );
        }

        public void change1() {
            delayProcessEvent(Event.CHANGE_1);
        }

        public void change2() {
            delayProcessEvent(Event.CHANGE_2);
        }

        public void change3() {
            delayProcessEvent(Event.CHANGE_3);
        }

        public void cleanUp() {
            delayProcessEvent(Event.CLEAN_UP);
        }

        private void delayProcessEvent(final Event e) {
            Utils.delayExecution(new Runnable() {
                @Override
                public void run() {
                    processEvent(e);
                }
            });
        }
    }

    private static class Child3ActivityData {
        private static Child3ActivityData instance = new Child3ActivityData();
        private OperationState operationState = new OperationState();

        private Child3ActivityData() {
        }

        public static Child3ActivityData getInstance() {
            return instance;
        }

        public OperationState getOperationState() {
            return operationState;
        }
    }
}
