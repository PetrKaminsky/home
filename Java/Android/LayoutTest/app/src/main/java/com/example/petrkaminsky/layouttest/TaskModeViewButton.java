package com.example.petrkaminsky.layouttest;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.widget.AppCompatButton;
import android.util.AttributeSet;

public class TaskModeViewButton extends AppCompatButton {

    private boolean longLabel = false;
    private String shortLabelText;
    private String longLabelText;

    public TaskModeViewButton(Context context) {
        super(context);

        init(null, 0);
    }

    public TaskModeViewButton(Context context, AttributeSet attrs) {
        super(context, attrs);

        init(attrs, 0);
    }

    public TaskModeViewButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        init(attrs, defStyle);
    }

    public boolean isLongLabel() {
        return longLabel;
    }

    public void setLongLabel(boolean b) {
        longLabel = b;
        updateText();
    }

    public String getShortLabelText() {
        return shortLabelText;
    }

    public void setShortLabelText(String s) {
        shortLabelText = s;
    }

    public String getLongLabelText() {
        return longLabelText;
    }

    public void setLongLabelText(String s) {
        longLabelText = s;
    }

    private void init(AttributeSet attrs, int defStyle) {
        final TypedArray a = getContext().obtainStyledAttributes(
                attrs, R.styleable.TaskModeViewButton, defStyle, 0);

        if (a.hasValue(R.styleable.TaskModeViewButton_shortLabelText)) {
            shortLabelText = a.getString(
                    R.styleable.TaskModeViewButton_shortLabelText);
        }
        if (a.hasValue(R.styleable.TaskModeViewButton_longLabelText)) {
            longLabelText = a.getString(
                    R.styleable.TaskModeViewButton_longLabelText);
        }

        a.recycle();
    }


    private void updateText() {
        setText(isLongLabel()
                ? getLongLabelText()
                : getShortLabelText());
    }
}
