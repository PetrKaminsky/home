package com.example.petrkaminsky.layouttest;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class LoginActivity extends CommonActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);

        final TaskModeView taskModeViewParcel = (TaskModeView) findViewById(R.id.taskModeViewParcel);
        taskModeViewParcel.setModeClickRunnable(new Runnable() {
            @Override
            public void run() {
                TaskModeWrapper.getInstance().setCurrentMode(TaskMode.Parcel);
                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                startActivityForResult(intent, RequestCode.Main);
            }
        });
        final TaskModeView taskModeViewCargo = (TaskModeView) findViewById(R.id.taskModeViewCargo);
        taskModeViewCargo.setModeClickRunnable(new Runnable() {
            @Override
            public void run() {
                TaskModeWrapper.getInstance().setCurrentMode(TaskMode.Cargo);
                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                startActivityForResult(intent, RequestCode.Main);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RequestCode.Main) {
            TaskModeWrapper.getInstance().invalidate();
        }
    }
}
