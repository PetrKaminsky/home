package com.example.petrkaminsky.layouttest;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageButton;
import android.widget.LinearLayout;

public class BottomToolbar extends LinearLayout {

    private TaskModeView taskModeView = null;
    private ImageButton buttonScan = null;
    private boolean taskModeViewVisible = true;
    private boolean taskModeViewEnabled = true;
    private boolean buttonScanVisible = true;
    private boolean visible = true;

    public BottomToolbar(Context context) {
        super(context);

        init(null, 0);
    }

    public BottomToolbar(Context context, AttributeSet attrs) {
        super(context, attrs);

        init(attrs, 0);
    }

    public BottomToolbar(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        init(attrs, defStyle);
    }

    public void setVisible(boolean b) {
        visible = b;
        setVisibility(b
                ? VISIBLE
                : GONE);
    }

    public void setTaskModeViewVisible(boolean b) {
        taskModeViewVisible = b;
        taskModeView.setVisibility(b
                ? VISIBLE
                : INVISIBLE);
    }

    public void setTaskModeViewEnabled(boolean b) {
        taskModeViewEnabled = b;
        taskModeView.setEnabled(b);
    }

    public void setButtonScanVisible(boolean b) {
        buttonScanVisible = b;
        buttonScan.setVisibility(b
                ? VISIBLE
                : INVISIBLE);
    }

    private void init(AttributeSet attrs, int defStyle) {
        final TypedArray a = getContext().obtainStyledAttributes(
                attrs, R.styleable.BottomToolbar, defStyle, 0);

        if (a.hasValue(R.styleable.BottomToolbar_taskModeViewVisible)) {
            taskModeViewVisible = a.getBoolean(
                    R.styleable.BottomToolbar_taskModeViewVisible,
                    true);
        }
        if (a.hasValue(R.styleable.BottomToolbar_taskModeViewEnabled)) {
            taskModeViewEnabled = a.getBoolean(
                    R.styleable.BottomToolbar_taskModeViewEnabled,
                    true);
        }
        if (a.hasValue(R.styleable.BottomToolbar_buttonScanVisible)) {
            buttonScanVisible = a.getBoolean(
                    R.styleable.BottomToolbar_buttonScanVisible,
                    true);
        }
        if (a.hasValue(R.styleable.BottomToolbar_visible)) {
            visible = a.getBoolean(
                    R.styleable.BottomToolbar_visible,
                    true);
        }

        a.recycle();

        LayoutInflater.from(getContext()).inflate(R.layout.bottom_toolbar, this);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        taskModeView = (TaskModeView) findViewById(R.id.taskModeView);
        taskModeView.setCurrentMode(TaskModeWrapper.getInstance().getCurrentMode());
        taskModeView.setModeClickRunnable(new Runnable() {
                                              @Override
                                              public void run() {
                                                  TaskModeWrapper.getInstance().toggleMode();
                                                  taskModeView.setCurrentMode(TaskModeWrapper.getInstance().getCurrentMode());
                                              }
                                          }
        );
        buttonScan = (ImageButton) findViewById(R.id.buttonScan);
        setTaskModeViewVisible(taskModeViewVisible);
        setTaskModeViewEnabled(taskModeViewEnabled);
        setButtonScanVisible(buttonScanVisible);
        setVisible(visible);
    }
}
