package com.example.petrkaminsky.layouttest;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class Child1Activity extends CommonActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        setContentView(R.layout.activity_child1);
    }
}
