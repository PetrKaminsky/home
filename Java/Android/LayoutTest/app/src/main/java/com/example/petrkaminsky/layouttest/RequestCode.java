package com.example.petrkaminsky.layouttest;

/**
 * Created by petr.kaminsky on 26.2.2018.
 */

public class RequestCode {
    public static final int Main = 1;
    public static final int Child1 = 2;
    public static final int Child2 = 3;
    public static final int Child3 = 4;
}
