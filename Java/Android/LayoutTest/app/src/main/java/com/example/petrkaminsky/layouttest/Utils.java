package com.example.petrkaminsky.layouttest;

import android.os.Handler;
import android.support.v4.app.Fragment;

public class Utils {

    public static void delayExecution(Runnable r, long delay) {
        new Handler().postDelayed(r, delay);
    }

    public static void delayExecution(Runnable r) {
        delayExecution(r, 100);
    }

    public static <T extends Fragment> T reuseFragment(Fragment fragment, Class<T> cls) {
        if (fragment == null || fragment.getClass() != cls) {
            try {
                return cls.newInstance();
            } catch (IllegalAccessException ex) {
                return null;
            } catch (InstantiationException ex) {
                return null;
            }
        }

        return (T) fragment;
    }
}
