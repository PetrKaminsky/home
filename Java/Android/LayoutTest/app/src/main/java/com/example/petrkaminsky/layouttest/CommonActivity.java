package com.example.petrkaminsky.layouttest;

import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import static android.R.id.home;

/**
 * Created by petr.kaminsky on 26.2.2018.
 */

public class CommonActivity extends AppCompatActivity {

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case home:
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
