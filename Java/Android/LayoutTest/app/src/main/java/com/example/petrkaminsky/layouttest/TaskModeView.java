package com.example.petrkaminsky.layouttest;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

public class TaskModeView extends LinearLayout {

    private TaskModeViewButton buttonModeParcel = null;
    private TaskModeViewButton buttonModeCargo = null;
    private Button buttonModeUnknown = null;
    private Runnable modeClickRunnable = null;
    private int currentMode = TaskMode.Unknown;
    private boolean longLabel = false;

    public TaskModeView(Context context) {
        super(context);

        init(null, 0);
    }

    public TaskModeView(Context context, AttributeSet attrs) {
        super(context, attrs);

        init(attrs, 0);
    }

    public TaskModeView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        init(attrs, defStyle);
    }

    public void setModeClickRunnable(Runnable runnable) {
        modeClickRunnable = runnable;
    }

    public void setCurrentMode(int mode) {
        currentMode = mode;
        updateButtonsVisibility();
    }

    public void setLongLabel(boolean b) {
        longLabel = b;
        buttonModeParcel.setLongLabel(b);
        buttonModeCargo.setLongLabel(b);
    }

    private void init(AttributeSet attrs, int defStyle) {
        final TypedArray a = getContext().obtainStyledAttributes(
                attrs, R.styleable.TaskModeView, defStyle, 0);

        if (a.hasValue(R.styleable.TaskModeView_longLabel)) {
            longLabel = a.getBoolean(
                    R.styleable.TaskModeView_longLabel,
                    false);
        }
        if (a.hasValue(R.styleable.TaskModeView_taskMode)) {
            currentMode = a.getInt(
                    R.styleable.TaskModeView_taskMode,
                    TaskMode.Unknown);
        }

        a.recycle();

        LayoutInflater.from(getContext()).inflate(R.layout.task_mode_view, this);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        buttonModeParcel = (TaskModeViewButton) findViewById(R.id.buttonModeParcel);
        buttonModeParcel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (modeClickRunnable != null) {
                    modeClickRunnable.run();
                }
            }
        });
        buttonModeCargo = (TaskModeViewButton) findViewById(R.id.buttonModeCargo);
        buttonModeCargo.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (modeClickRunnable != null) {
                    modeClickRunnable.run();
                }
            }
        });
        buttonModeUnknown = (Button) findViewById(R.id.buttonModeUnknown);
        setLongLabel(longLabel);
        setCurrentMode(currentMode);
    }

    @Override
    public void setEnabled(boolean b) {
        super.setEnabled(b);

        buttonModeParcel.setEnabled(b);
        buttonModeCargo.setEnabled(b);
    }

    private void updateButtonsVisibility() {
        switch (currentMode) {
            case TaskMode.Cargo:
                buttonModeParcel.setVisibility(GONE);
                buttonModeCargo.setVisibility(VISIBLE);
                buttonModeUnknown.setVisibility(GONE);
                break;

            case TaskMode.Parcel:
                buttonModeParcel.setVisibility(VISIBLE);
                buttonModeCargo.setVisibility(GONE);
                buttonModeUnknown.setVisibility(GONE);
                break;

            case TaskMode.Unknown:
                buttonModeParcel.setVisibility(GONE);
                buttonModeCargo.setVisibility(GONE);
                buttonModeUnknown.setVisibility(VISIBLE);
                break;
        }
    }
}
