package com.example.petr.motiontest;

import android.app.Activity;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.OrientationEventListener;
import android.view.SurfaceView;

import static android.hardware.SensorManager.SENSOR_DELAY_NORMAL;

public class MainActivity extends Activity {

    private OrientationEventListener orientationListener = null;
    private SurfaceView surfaceView = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        surfaceView = (SurfaceView) findViewById(R.id.surfaceView);

        orientationListener = new OrientationEventListener(
                this, SENSOR_DELAY_NORMAL) {
            @Override
            public void onOrientationChanged(int orientation) {
                if (orientation == ORIENTATION_UNKNOWN) {
                    return;
                }

                redrawContents(orientation);
            }
        };
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (orientationListener.canDetectOrientation()) {
            orientationListener.enable();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        orientationListener.disable();
    }

    private void redrawContents(int orientation) {
        orientation = getFilter().getSmoothValue(orientation);

        Canvas canvas = surfaceView.getHolder().lockCanvas();
        if (canvas != null) {
            canvas.drawColor(Color.BLACK);

            int width = canvas.getWidth();
            int height = canvas.getHeight();
            int lineLength = 2 + (int) Math.sqrt(width * width + height * height);
            Paint paint = new Paint();
            paint.setColor(Color.WHITE);
            paint.setAntiAlias(true);

            canvas.save();

            canvas.translate(width / 2, height / 2);
            canvas.rotate(-orientation);
            float left = -lineLength / 2;
            float top = 0;
            float right = lineLength / 2;
            float bottom = lineLength / 2;

            canvas.drawRect(
                    left,
                    top,
                    right,
                    bottom,
                    paint);

            canvas.restore();

            surfaceView.getHolder().unlockCanvasAndPost(canvas);
        }
    }

    private SmoothFilter getFilter() {
        return MainActivityData.getInstance().getFilter();
    }

    private static class MainActivityData {
        private static final MainActivityData instance = new MainActivityData();
        private SmoothFilter filter = new SmoothFilter();

        private MainActivityData() {
        }

        public static MainActivityData getInstance() {
            return instance;
        }

        public SmoothFilter getFilter() {
            return filter;
        }
    }
}
