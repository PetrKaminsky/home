package com.example.petr.motiontest;

/**
 * Created by petr.kaminsky on 9.5.2017.
 */

public class SmoothFilter {

    private static final int MODULUS = 360;
    private static final int MAX_HISTORY_LENGTH = 5;
    private final int[] history = new int[MAX_HISTORY_LENGTH];
    private int historyLength = 0;

    public int getSmoothValue(int value) {
        int retVal = value % MODULUS;
        if (isHistoryEmpty()) {
            appendToHistory(retVal);
        } else {
            appendToHistory(retVal);
            normalizeHistory();
            retVal = calculateValue();
        }

        return retVal % MODULUS;
    }

    private boolean isHistoryEmpty() {
        return historyLength == 0;
    }

    private boolean isHistoryFull() {
        return historyLength == MAX_HISTORY_LENGTH;
    }

    private void shiftHistory() {
        System.arraycopy(history, 1, history, 0, historyLength - 1);
        historyLength--;
    }

    private void appendToHistory(int value) {
        if (isHistoryFull()) {
            shiftHistory();
        }
        history[historyLength] = normalizeValue(value);
        historyLength++;
    }

    private int normalizeValue(int value) {
        if (!isHistoryEmpty()) {
            value %= MODULUS;
            int lastValue = getLastHistoryValue();
            if (value > lastValue) {
                if (Math.abs(value - lastValue) > Math.abs(MODULUS + lastValue - value)) {
                    value -= MODULUS;
                }
            } else {
                if (Math.abs(lastValue - value) > Math.abs(MODULUS + value - lastValue)) {
                    value += MODULUS;
                }
            }
        }

        return value;
    }

    private int getLastHistoryValue() {
        return history[historyLength - 1];
    }

    private void normalizeHistory() {
        if (isHistoryAboveModulus()) {
            modifyHistory(-MODULUS);
        } else if (isHistoryBelowZero()) {
            modifyHistory(MODULUS);
        }
    }

    private boolean isHistoryAboveModulus() {
        if (!isHistoryEmpty()) {
            for (int i = 0; i < historyLength; i++) {
                if (history[i] < MODULUS) {
                    return false;
                }
            }

            return true;
        }

        return false;
    }

    private boolean isHistoryBelowZero() {
        if (!isHistoryEmpty()) {
            for (int i = 0; i < historyLength; i++) {
                if (history[i] >= 0) {
                    return false;
                }
            }

            return true;
        }

        return false;
    }

    private void modifyHistory(int valueAdd) {
        for (int i = 0; i < historyLength; i++) {
            history[i] += valueAdd;
        }
    }

    private int calculateValue() {
        int retVal = 0;
        if (!isHistoryEmpty()) {
            for (int i = 0; i < historyLength; i++) {
                retVal += history[i];
            }
            retVal /= historyLength;
        }

        return retVal;
    }
}
