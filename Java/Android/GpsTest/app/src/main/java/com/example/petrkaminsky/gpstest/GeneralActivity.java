package com.example.petrkaminsky.gpstest;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public abstract class GeneralActivity extends AppCompatActivity {

    @Override
    protected void onResume() {
        super.onResume();

        Log.d("life", "onResume(): " + this);

        GeneralActivityRegistry.getInstance().resumeActivity(this);
    }

    @Override
    protected void onPause() {
        super.onPause();

        Log.d("life", "onPause(): " + this);

        GeneralActivityRegistry.getInstance().pauseActivity(this);
    }

    @Override
    public void onRequestPermissionsResult(
            int requestCode,
            @NonNull String[] permissions,
            @NonNull int[] grantResults) {
        PermissionRequester.getInstance().onRequestPermissionsResult(
                requestCode,
                permissions,
                grantResults);

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    public static void executeActive(GeneralActivity activity, ActiveExecution execution) {
        GeneralActivityRegistry.getInstance().execute(activity, execution);
    }

    public GeneralActivity getCurrentActivity(GeneralActivity activity) {
        return GeneralActivityRegistry.getInstance().getCurrentActivity(activity);
    }

    public static class GeneralActivityRegistry {

        public static GeneralActivityRegistry getInstance() {
            return instance;
        }

        private static GeneralActivityRegistry instance = new GeneralActivityRegistry();

        private HashMap<Class, GeneralActivity> activities = new HashMap<>();
        private HashMap<Class, List<ActiveExecution>> callbacks = new HashMap<>();

        public void resumeActivity(GeneralActivity activity) {
            Class clazz = activity.getClass();
            activities.put(clazz, activity);
            dequeExecutions(clazz);
        }

        public void pauseActivity(GeneralActivity activity) {
            Class clazz = activity.getClass();
            activities.put(clazz, null);
        }

        public void execute(GeneralActivity activity, ActiveExecution execution) {
            Class clazz = activity.getClass();
            if (activities.containsKey(clazz)) {
                GeneralActivity activeActivity = activities.get(clazz);
                if (activeActivity != null) {
                    if (execution != null) {
                        execution.execute();
                    }
                    dequeExecutions(clazz);
                } else {
                    enqueExecution(clazz, execution);
                }
            }
        }

        public GeneralActivity getCurrentActivity(GeneralActivity activity) {
            Class clazz = activity.getClass();
            return activities.get(clazz);
        }

        private void enqueExecution(Class clazz, ActiveExecution execution) {
            List<ActiveExecution> executions = callbacks.get(clazz);
            if (executions != null) {
                executions.add(execution);
            } else {
                executions = new ArrayList<>();
                executions.add(execution);
                callbacks.put(clazz, executions);
            }
        }

        private void dequeExecutions(Class clazz) {
            List<ActiveExecution> executions = callbacks.get(clazz);
            if (executions != null) {
                for (ActiveExecution execution : executions) {
                    if (execution != null) {
                        execution.execute();
                    }
                }
                executions.clear();
            }
            callbacks.put(clazz, null);
        }
    }

    public interface ActiveExecution {
        void execute();
    }
}
