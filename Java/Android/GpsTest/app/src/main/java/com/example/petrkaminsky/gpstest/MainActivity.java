package com.example.petrkaminsky.gpstest;

import android.Manifest;
import android.location.Location;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.Date;

public class MainActivity extends GeneralActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        findViewById(R.id.buttonStartRequest).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PermissionRequester.getInstance().request(
                        MainActivity.this,
                        new String[]{
                                Manifest.permission.ACCESS_FINE_LOCATION
                        },
                        new PermissionRequester.PermissionExecution() {
                            @Override
                            public void execute() {
                                initLocationManager();
                                startGpsRequest();
                            }
                        });
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        updateButton();
    }

    private void initLocationManager() {
        LocationWrapper.getInstance().initLocationManager(this);
    }

    private void startGpsRequest() {
        if (!isProviderEnabled()) {
            Utils.openLocationSettings(this);
        }

        if (isRequestInProgress()) {
            return;
        }

        setRequestInProgress(true);

        updateScreen(null);
        startLocationRequests();
    }

    private void updateScreen(Location location) {
        updateButton();
        setEditTextLongitude(location != null ? Utils.formatDouble(location.getLongitude()) : "");
        setEditTextLatitude(location != null ? Utils.formatDouble(location.getLatitude()) : "");
        setEditTextUtcDateTime(location != null ? Utils.formatDate(new Date(location.getTime())) : "");
    }

    private void startLocationRequests() {
        LocationWrapper.getInstance().startLocationRequests(new LocationWrapper.LocationChangedListener() {
            @Override
            public void onLocationChanged(final Location location) {
                stopLocationRequests();

                setRequestInProgress(false);

                GeneralActivity.executeActive(MainActivity.this, new ActiveExecution() {
                    @Override
                    public void execute() {
                        ((MainActivity) getCurrentActivity(MainActivity.this)).updateScreen(location);
                    }
                });
            }
        });
    }

    private void stopLocationRequests() {
        LocationWrapper.getInstance().stopLocationRequests();
    }

    private boolean isProviderEnabled() {
        return LocationWrapper.getInstance().isProviderEnabled();
    }

    private void updateButton() {
        Button buttonStartRequest = (Button) findViewById(R.id.buttonStartRequest);
        buttonStartRequest.setEnabled(!isRequestInProgress());
    }

    private void setEditTextLongitude(String text) {
        setEditText(R.id.editTextLongitude, text);
    }

    private void setEditTextLatitude(String text) {
        setEditText(R.id.editTextLatitude, text);
    }

    private void setEditTextUtcDateTime(String text) {
        setEditText(R.id.editTextUtcDateTime, text);
    }

    private void setEditText(int id, String text) {
        EditText editText = (EditText) findViewById(id);
        editText.setText(text);
    }

    private boolean isRequestInProgress() {
        return MainActivityData.getInstance().isRequestInProgress();
    }

    private void setRequestInProgress(boolean requestInProgress) {
        MainActivityData.getInstance().setRequestInProgress(requestInProgress);
    }

    public static class MainActivityData {

        public static MainActivityData getInstance() {
            return instance;
        }

        private static final MainActivityData instance = new MainActivityData();

        public boolean isRequestInProgress() {
            return requestInProgress;
        }

        public void setRequestInProgress(boolean requestInProgress) {
            this.requestInProgress = requestInProgress;
        }

        private boolean requestInProgress = false;
    }
}
