package com.example.petrkaminsky.gpstest;

import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Bundle;
import android.util.Log;

import java.util.Date;

public class LocationWrapper {

    public static LocationWrapper getInstance() {
        return instance;
    }

    private static LocationWrapper instance = new LocationWrapper();

    private LocationManager locationManager = null;
    private String locationProvider = null;
    private LocationListener locationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            Log.d("location", "onLocationChanged()" +
                    ", longitude: " + Utils.formatDouble(location.getLongitude()) +
                    ", latitude: " + Utils.formatDouble(location.getLatitude()) +
                    ", time: " + new Date(location.getTime()));

            if (locationChangedListener != null) {
                locationChangedListener.onLocationChanged(location);
            }
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            Log.d("location", "onStatusChanged()" +
                    ", provider: " + provider +
                    ", status: " + statusToString(status));
        }

        @Override
        public void onProviderEnabled(String provider) {
            Log.d("location", "onProviderEnabled(), provider: " + provider);
        }

        @Override
        public void onProviderDisabled(String provider) {
            Log.d("location", "onProviderDisabled(), provider: " + provider);
        }

        private String statusToString(int status) {
            switch (status) {
                case LocationProvider.OUT_OF_SERVICE:
                    return "OUT_OF_SERVICE";

                case LocationProvider.TEMPORARILY_UNAVAILABLE:
                    return "TEMPORARILY_UNAVAILABLE";

                case LocationProvider.AVAILABLE:
                    return "AVAILABLE";
            }

            return "";
        }
    };
    private LocationChangedListener locationChangedListener = null;

    public void initLocationManager(GeneralActivity activity) {
        if (locationManager != null) {
            return;
        }

        try {
            locationManager = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);

            if (locationManager != null) {
                Log.d("trace", "initLocationManager(), locationManager != null");

                for (String provider : locationManager.getAllProviders()) {
                    Log.d("trace", "Provider: " + provider);
                }

                Criteria criteria = new Criteria();
                criteria.setAccuracy(Criteria.ACCURACY_FINE);
                locationProvider = locationManager.getBestProvider(criteria, false);

                Log.d("trace", "initLocationManager(), best provider: " + locationProvider);
            }
        } catch (Exception e) {
            Log.d("exception", "initLocationManager(): " + e);
        }
    }

    public boolean isProviderEnabled() {
        try {
            if (locationManager != null) {
                return locationManager.isProviderEnabled(locationProvider);
            }
        } catch (Exception e) {
            Log.d("exception", "isProviderEnabled(): " + e);
        }

        return false;
    }

    public void startLocationRequests(LocationChangedListener listener) {
        locationChangedListener = listener;

        try {
            if (locationManager != null) {
                Log.d("trace", "LocationManager.requestLocationUpdates()");

                locationManager.requestLocationUpdates(
                        locationProvider,
                        0,
                        0,
                        locationListener);
            }
        } catch (SecurityException e) {
            Log.d("exception", "startLocationRequests(): " + e);
        }
    }

    public void stopLocationRequests() {
        try {
            if (locationManager != null) {
                Log.d("trace", "LocationManager.removeUpdates()");

                locationManager.removeUpdates(locationListener);
            }
        } catch (Exception e) {
            Log.d("exception", "stopLocationRequests(): " + e);
        }
    }

    public interface LocationChangedListener {
        void onLocationChanged(Location location);
    }
}
