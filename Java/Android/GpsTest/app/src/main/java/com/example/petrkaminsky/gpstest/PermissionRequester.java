package com.example.petrkaminsky.gpstest;

import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import java.util.HashMap;

public class PermissionRequester {

    private static int PERMISSION_REQUEST = 1;

    public static PermissionRequester getInstance() {
        return instance;
    }

    private static PermissionRequester instance = new PermissionRequester();

    private HashMap<Integer, PermissionExecution> requestExecutions = new HashMap<>();

    public void request(
            GeneralActivity activity,
            String[] permissions,
            PermissionExecution execution) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M ||
                hasPermissions(activity, permissions)) {
            if (execution != null) {
                execution.execute();
            }
        } else {
            int requestCode = PERMISSION_REQUEST;
            PERMISSION_REQUEST++;
            requestExecutions.put(requestCode, execution);
            ActivityCompat.requestPermissions(
                    activity,
                    permissions,
                    requestCode);
        }
    }

    public void onRequestPermissionsResult(
            int requestCode,
            String[] permissions,
            int[] grantResults) {
        for (int grantResult : grantResults) {
            if (grantResult != PackageManager.PERMISSION_GRANTED) {
                return;
            }
        }
        PermissionExecution execution = requestExecutions.get(requestCode);
        if (execution != null) {
            execution.execute();
        }
    }

    private boolean hasPermissions(GeneralActivity activity, String[] permissions) {
        if (permissions != null) {
            for (String permission : permissions) {
                if (!hasPermission(activity, permission)) {
                    return false;
                }
            }
        }

        return true;
    }

    private boolean hasPermission(GeneralActivity activity, String permission) {
        return ContextCompat.checkSelfPermission(
                activity,
                permission)
                == PackageManager.PERMISSION_GRANTED;
    }

    public interface PermissionExecution {
        void execute();
    }
}
