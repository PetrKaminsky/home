package com.example.petrkaminsky.gpstest;

import android.content.Intent;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS;

public class Utils {

    public static String formatDouble(double d) {
        return String.format(Locale.ENGLISH, "%f", d);
    }

    public static String formatDate(Date date) {
        return new SimpleDateFormat("dd.MM.yyyy HH:mm:ss", Locale.ENGLISH).format(date);
    }

    public static void openLocationSettings(GeneralActivity activity) {
        Intent intent = new Intent(ACTION_LOCATION_SOURCE_SETTINGS);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        activity.getApplicationContext().startActivity(intent);
    }
}
