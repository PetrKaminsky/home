package com.example.petrkaminsky.sygictest;

import android.os.Handler;
import android.os.Looper;

/**
 * Created by petr.kaminsky on 30.1.2018.
 */

public class Utils {
    public static void runOnUiThread(Runnable runnable) {
        new Handler(Looper.getMainLooper()).post(runnable);
    }
}
