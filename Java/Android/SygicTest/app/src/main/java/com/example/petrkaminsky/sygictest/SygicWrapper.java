package com.example.petrkaminsky.sygictest;

import android.content.Context;
import android.os.RemoteException;
import android.util.Log;

import com.sygic.sdk.remoteapi.Api;
import com.sygic.sdk.remoteapi.ApiCallback;
import com.sygic.sdk.remoteapi.ApiLocation;
import com.sygic.sdk.remoteapi.ApiMaps;
import com.sygic.sdk.remoteapi.ApiNavigation;
import com.sygic.sdk.remoteapi.callback.OnSearchListener;
import com.sygic.sdk.remoteapi.events.ApiEvents;
import com.sygic.sdk.remoteapi.exception.GeneralException;
import com.sygic.sdk.remoteapi.model.GpsPosition;
import com.sygic.sdk.remoteapi.model.Position;
import com.sygic.sdk.remoteapi.model.WayPoint;

import java.util.ArrayList;

/**
 * Created by petr.kaminsky on 30.1.2018.
 */

public class SygicWrapper implements ApiCallback {
    private static SygicWrapper ourInstance = null;
    private boolean serviceConnected = false;
    private Api remoteApi = null;
    private Runnable serviceConnectedRunnable = null;

    private SygicWrapper(Context context) {
        remoteApi = Api.init(context, "com.sygic.truck", "com.sygic.truck.SygicService", this);
    }

    public static SygicWrapper getInstance(Context context) {
        if (ourInstance == null) {
            ourInstance = new SygicWrapper(context);
        }
        return ourInstance;
    }

    public boolean isServiceConnected() {
        synchronized (this) {
            return serviceConnected;
        }
    }

    public void setServiceConnected(boolean value) {
        synchronized (this) {
            if (serviceConnected == value) {
                return;
            }
            serviceConnected = value;
        }
        if (value && serviceConnectedRunnable != null) {
            Utils.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (isSygicRunning()) {
                        serviceConnectedRunnable.run();
                        serviceConnectedRunnable = null;
                    }
                }
            });
        }
    }

    @Override
    public void onEvent(int eventCode, String data) {
        Log.d("trace", "MainActivity.onEvent(), eventCode: " + eventCode + ", data: " + data);

        switch (eventCode) {
            case ApiEvents.EVENT_APP_STARTED:
                Utils.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (serviceConnectedRunnable != null) {
                            serviceConnectedRunnable.run();
                            serviceConnectedRunnable = null;
                        }
                    }
                });
                break;
        }
    }

    @Override
    public void onServiceConnected() {
        Log.d("trace", "MainActivity.onServiceConnected()");

        registerCallback();
        if (!isSygicRunning()) {
            show(true);
        }
        setServiceConnected(true);
    }

    @Override
    public void onServiceDisconnected() {
        Log.d("trace", "MainActivity.onServiceConnected()");

        setServiceConnected(false);
        unregisterCallback();
    }

    private void connect() {
        if (remoteApi != null) {
            remoteApi.connect();
        }
    }

    private void disconnect() {
        if (remoteApi != null) {
            remoteApi.disconnect();
        }
    }

    private void registerCallback() {
        if (remoteApi != null) {
            try {
                remoteApi.registerCallback();
            } catch (RemoteException ex) {
                Log.e("error", "SygicWrapper.registerCallback()", ex);
            }
        }
    }

    private void unregisterCallback() {
        if (remoteApi != null) {
            try {
                remoteApi.unregisterCallback();
            } catch (RemoteException ex) {
                Log.e("error", "SygicWrapper.unregisterCallback()", ex);
            }
        }
    }

    private void runConnected(Runnable runnable) {
        if (!isServiceConnected()) {
            serviceConnectedRunnable = runnable;
            connect();
        } else if (!isSygicRunning()) {
            serviceConnectedRunnable = runnable;
            show(true);
        } else {
            runnable.run();
        }
    }

    public void destroy() {
        unregisterCallback();
        disconnect();
        remoteApi = null;
    }

    public void show() {
        runConnected(new Runnable() {
            @Override
            public void run() {
                show(true);
            }
        });
    }

    public void displayAddress(final String address) {
        runConnected(new Runnable() {
            @Override
            public void run() {
                if (showAddressOnMap(address)) {
                    show(true);
                }

            }
        });
    }

    public void runNavigation(final String address) {
        runConnected(new Runnable() {
            @Override
            public void run() {
                if (navigateToAddress(address)) {
                    show(true);
                }
            }
        });
    }

    public void cancelNavigation() {
        runConnected(new Runnable() {
            @Override
            public void run() {
                if (stopNavigation()) {
                    show(true);
                }
            }
        });
    }

    public void showMyLocation() {
        runConnected(new Runnable() {
            @Override
            public void run() {
                if (showMyLocationOnMap()) {
                    show(true);
                }
            }
        });
    }

    public void search(final String address) {
        runConnected(new Runnable() {
            @Override
            public void run() {
                searchAddress(address);
            }
        });
    }

    private void show(boolean background) {
        if (remoteApi != null) {
            try {
                remoteApi.show(background);
            } catch (RemoteException ex) {
                Log.e("error", "SygicWrapper.show()", ex);
            }
        }
    }

    private boolean navigateToAddress(String address) {
        try {
            Position pos = ApiLocation.locationFromAddress(address, false, false, 0);
            WayPoint waypoint = new WayPoint();
            waypoint.SetLocation(pos.getX(), pos.getY());
            ApiNavigation.startNavigation(waypoint, 0, false, 0);
            //ApiNavigation.navigateToAddress(address, false, 0, 0);

            return true;
        } catch (GeneralException ex) {
            Log.e("error", "SygicWrapper.navigateToAddress()", ex);
        }

        return false;
    }

    private boolean stopNavigation() {
        try {
            ApiNavigation.stopNavigation(0);

            return true;
        } catch (GeneralException ex) {
            Log.e("error", "SygicWrapper.stopNavigation()", ex);
        }

        return false;
    }

    private boolean showAddressOnMap(String address) {
        try {
            Position pos = ApiLocation.locationFromAddress(address, false, false, 0);
            ApiMaps.showCoordinatesOnMap(pos, 1000, 0);

            return true;
        } catch (GeneralException ex) {
            Log.e("error", "SygicWrapper.showAddressOnMap()", ex);
        }

        return false;
    }

    private boolean showMyLocationOnMap() {
        try {
            GpsPosition gpsPosition = ApiNavigation.getActualGpsPosition(true, 0);
            if (gpsPosition != null) {
                Position pos = new Position();
                pos.setPosition(gpsPosition.getLongitude(), gpsPosition.getLatitude());
                ApiMaps.showCoordinatesOnMap(pos, 1000, 0);

                return true;
            }
        } catch (GeneralException ex) {
            Log.e("error", "SygicWrapper.showMyLocationOnMap()", ex);
        }

        return false;
    }

    private boolean isSygicRunning() {
        boolean retVal = false;
        try {
            retVal = Api.isApplicationRunning(100);
        } catch (GeneralException ex) {
            Log.e("error", "SygicWrapper.isSygicRunning()", ex);
        }
        return retVal;
    }

    public void searchAddress(String address) {
        try {
            ApiLocation.searchLocation(address, new OnSearchListener() {
                @Override
                public void onResult(String input, ArrayList<WayPoint> results, int resultCode) {
                    for (WayPoint wayPoint : results) {
                        Log.i("data", "waypoint: " + wayPoint.getStrAddress() +
                                ", longitude: " + ((double) wayPoint.getLocation().getX() / 100000) +
                                ", latitude: " + ((double) wayPoint.getLocation().getY() / 100000));
                    }
                }
            }, 0);
        } catch (GeneralException ex) {
            Log.e("error", "SygicWrapper.searchAddress()", ex);
        }
    }
}
