package com.example.petrkaminsky.sygictest;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    private EditText editTextAddress = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        editTextAddress = (EditText) findViewById(R.id.editTextAddress);
        findViewById(R.id.buttonShow).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SygicWrapper.getInstance(getApplicationContext()).show();
            }
        });
        findViewById(R.id.buttonShowLocation).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SygicWrapper.getInstance(getApplicationContext()).showMyLocation();
            }
        });
        findViewById(R.id.buttonNavigate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String address = editTextAddress.getText().toString();
                if (!address.isEmpty()) {
                    SygicWrapper.getInstance(getApplicationContext()).runNavigation(address);
                }
            }
        });
        findViewById(R.id.buttonStopNavigation).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SygicWrapper.getInstance(getApplicationContext()).cancelNavigation();
            }
        });
        findViewById(R.id.buttonShowOnMap).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String address = editTextAddress.getText().toString();
                if (!address.isEmpty()) {
                    SygicWrapper.getInstance(getApplicationContext()).displayAddress(address);
                }
            }
        });
        findViewById(R.id.buttonSearch).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String address = editTextAddress.getText().toString();
                if (!address.isEmpty()) {
                    SygicWrapper.getInstance(getApplicationContext()).search(address);
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        SygicWrapper.getInstance(getApplicationContext()).destroy();
    }
}
