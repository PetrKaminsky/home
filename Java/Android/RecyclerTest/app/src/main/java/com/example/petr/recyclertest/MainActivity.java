package com.example.petr.recyclertest;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.ImageView;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class MainActivity extends Activity {

    private static final int CAMERA_REQUEST_CODE = 1;
    private static final String IMAGE_PREFIX = "JPEG_";
    private static final int MAX_IMAGE_WIDTH = 240;
    private static final int MAX_IMAGE_HEIGHT = 240;
    private RecyclerAdapter adapter = null;
    private LinearLayoutManager layoutManager = null;

    public static File getFilesDir(Activity activity) {
        return activity.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        MainActivityData.getInstance().setActivity(this);

        setContentView(R.layout.activity_main);
        RecyclerView recyclerViewItems = (RecyclerView) findViewById(R.id.recyclerViewItems);
        layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        recyclerViewItems.setLayoutManager(layoutManager);
        adapter = new RecyclerAdapter();
        adapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void OnItemClick(View v, int position) {
                Log.d("main", "OnItemClick(): " + position);

                viewImage(position);
            }
        });
        recyclerViewItems.setAdapter(adapter);
        ItemTouchHelper.Callback callback =
                new RecyclerItemTouchHelperCallback(adapter);
        ItemTouchHelper touchHelper = new ItemTouchHelper(callback);
        touchHelper.attachToRecyclerView(recyclerViewItems);
        findViewById(R.id.buttonAppend).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startAppendImage();
            }
        });
        findViewById(R.id.buttonClear).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int itemCount = RecyclerDataSource.getInstance().size();
                RecyclerDataSource.getInstance().eraseAll();
                adapter.notifyItemRangeRemoved(0, itemCount);
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        MainActivityData.getInstance().setActivity(null);
    }

    private void viewImage(int position) {
        String imagePath = RecyclerDataSource.getInstance().get(position);

        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        File imageFile = new File(imagePath);
        Uri uri = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            uri = FileProvider.getUriForFile(this,
                    "com.example.petr.fileprovider",
                    imageFile);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        } else {
            uri = Uri.fromFile(imageFile);
        }
        String mime = getMimeType(imagePath);
        intent.setDataAndType(uri, mime);

        startActivity(intent);
    }

    private String getMimeType(String imagePath) {
        String retVal = null;
        int i = imagePath.lastIndexOf('.');
        if (i != -1) {
            String extension = imagePath.substring(i + 1);
            retVal = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
        }

        return retVal;
    }

    private void startAppendImage() {
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (cameraIntent.resolveActivity(getPackageManager()) != null) {
            File photoFile = null;

            try {
                photoFile = createImageFile();
            } catch (IOException e) {
                Log.e("main", "buttonAppend.onClick()", e);
            }

            if (photoFile != null) {
                MainActivityData.getInstance().setImagePath(photoFile.getAbsolutePath());
                Uri photoUri = null;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    photoUri = FileProvider.getUriForFile(this,
                            "com.example.petr.fileprovider",
                            photoFile);
                    cameraIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION
                            | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                } else {
                    photoUri = Uri.fromFile(photoFile);
                }
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);

                startActivityForResult(cameraIntent, CAMERA_REQUEST_CODE);
            } else {
                MainActivityData.getInstance().setImagePath(null);
            }
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(new Date());
        String imageFileName = IMAGE_PREFIX + timeStamp + "_";
        File storageDir = getFilesDir(this);
        return File.createTempFile(
                imageFileName, // filename
                ".jpg", // extension
                storageDir);  // folder
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAMERA_REQUEST_CODE &&
                MainActivityData.getInstance().getImagePath() != null) {
            if (resultCode == RESULT_OK) {
                finishAppendImage();
            } else {
                new File(MainActivityData.getInstance().getImagePath()).delete();
            }

            MainActivityData.getInstance().setImagePath(null);

            return;
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    private void finishAppendImage() {
        RecyclerDataSource.getInstance().appendItem(MainActivityData.getInstance().getImagePath());
        int newPosition = RecyclerDataSource.getInstance().size() - 1;
        adapter.notifyItemInserted(newPosition);
        layoutManager.scrollToPosition(newPosition);
    }

    interface OnItemClickListener {
        void OnItemClick(View v, int position);
    }

    interface ItemTouchHelperAdapter {
        boolean onItemMove(int fromPosition, int toPosition);

        void onItemSwiped(int position);
    }

    private static class RecyclerViewHolder extends RecyclerView.ViewHolder {

        public ImageView imageViewSnap = null;

        public RecyclerViewHolder(View view,
                                  final OnItemClickListener onItemClickListener) {
            super(view);

            imageViewSnap = (ImageView) view.findViewById(R.id.imageViewSnap);

            if (onItemClickListener != null) {
                view.setClickable(true);
                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onItemClickListener.OnItemClick(v, getAdapterPosition());
                    }
                });
            }
        }
    }

    private static class RecyclerAdapter extends RecyclerView.Adapter<RecyclerViewHolder>
            implements ItemTouchHelperAdapter {

        private OnItemClickListener onItemClickListener = null;

        private static Bitmap getScaledImage(
                String imagePath, int targetWidth, int targetHeight) {
            // Get the dimensions of the bitmap
            BitmapFactory.Options bmOptions = new BitmapFactory.Options();
            bmOptions.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(imagePath, bmOptions);
            int photoWidth = bmOptions.outWidth;
            int photoHeight = bmOptions.outHeight;

            // Determine how much to scale down the image
            int scaleFactor = Math.min(photoWidth / targetWidth, photoHeight / targetHeight);

            // Decode the image file into a Bitmap sized to fill the View
            bmOptions.inJustDecodeBounds = false;
            bmOptions.inSampleSize = scaleFactor;

            return BitmapFactory.decodeFile(imagePath, bmOptions);
        }

        public void setOnItemClickListener(
                OnItemClickListener onItemClickListener) {
            this.onItemClickListener = onItemClickListener;
        }

        @Override
        public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.recycler_item, parent, false);
            return new RecyclerViewHolder(view, onItemClickListener);
        }

        @Override
        public void onBindViewHolder(RecyclerViewHolder holder, int position) {
            String imagePath = RecyclerDataSource.getInstance().get(position);
            holder.imageViewSnap.setImageBitmap(
                    getScaledImage(imagePath, MAX_IMAGE_WIDTH, MAX_IMAGE_HEIGHT));
        }

        @Override
        public int getItemCount() {
            return RecyclerDataSource.getInstance().size();
        }

        @Override
        public void onItemSwiped(int position) {
            RecyclerDataSource.getInstance().eraseItem(position);

            notifyItemRemoved(position);
        }

        @Override
        public boolean onItemMove(int fromPosition, int toPosition) {
//            if (fromPosition < toPosition) {
//                for (int i = fromPosition; i < toPosition; i++) {
//                    Collections.swap(mItems, i, i + 1);
//                }
//            } else {
//                for (int i = fromPosition; i > toPosition; i--) {
//                    Collections.swap(mItems, i, i - 1);
//                }
//            }
//
//            notifyItemMoved(fromPosition, toPosition);
//
//            return true;

            return false;
        }
    }

    private static class RecyclerDataSource extends ArrayList<String> {

        private static final RecyclerDataSource instance = new RecyclerDataSource();

        private RecyclerDataSource() {
            loadItems();
        }

        public static RecyclerDataSource getInstance() {
            return instance;
        }

        private void loadItems() {
            File storageDir = MainActivity.getFilesDir(
                    MainActivityData.getInstance().getActivity());
            if (storageDir != null) {
                String[] files = storageDir.list(new FilenameFilter() {
                    @Override
                    public boolean accept(File dir, String name) {
                        return name.startsWith(MainActivity.IMAGE_PREFIX);
                    }
                });
                for (String file : files) {
                    appendItem(new File(storageDir, file).getAbsolutePath());
                }
            }
        }

        public void appendItem(String imagePath) {
            add(imagePath);
        }

        public void eraseAll() {
            for (String imagePath : this) {
                new File(imagePath).delete();
            }

            clear();
        }

        public void eraseItem(int position) {
            String imagePath = get(position);
            new File(imagePath).delete();

            remove(position);
        }
    }

    public static class MainActivityData {

        private static final MainActivityData instance = new MainActivityData();
        private Activity activity = null;
        private String imagePath = null;

        private MainActivityData() {
        }

        public static MainActivityData getInstance() {
            return instance;
        }

        public String getImagePath() {
            return imagePath;
        }

        public void setImagePath(String imagePath) {
            this.imagePath = imagePath;
        }

        public Activity getActivity() {
            return activity;
        }

        public void setActivity(Activity activity) {
            this.activity = activity;
        }
    }

    public class RecyclerItemTouchHelperCallback extends ItemTouchHelper.Callback {

        private final ItemTouchHelperAdapter adapter;

        public RecyclerItemTouchHelperCallback(ItemTouchHelperAdapter a) {
            adapter = a;
        }

        @Override
        public boolean isLongPressDragEnabled() {
//            return true;
            return false;
        }

        @Override
        public boolean isItemViewSwipeEnabled() {
            return true;
        }

        @Override
        public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
            int dragFlags = 0;
            int swipeFlags = ItemTouchHelper.UP;
            return makeMovementFlags(dragFlags, swipeFlags);
        }

        @Override
        public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder source,
                              RecyclerView.ViewHolder target) {
            return adapter.onItemMove(source.getAdapterPosition(), target.getAdapterPosition());
        }

        @Override
        public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
            adapter.onItemSwiped(viewHolder.getAdapterPosition());
        }
    }
}
