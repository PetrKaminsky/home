package com.example.petr.sqlciphertest;

import android.content.Context;

import net.sqlcipher.Cursor;
import net.sqlcipher.database.SQLiteDatabase;
import net.sqlcipher.database.SQLiteOpenHelper;
import net.sqlcipher.database.SQLiteStatement;

import java.util.Date;

public class ListViewData {

    private static final String TABLE_COLUMN_ID = "id";
    private static final String TABLE_COLUMN_FIRST_NAME = "first_name";
    private static final String TABLE_COLUMN_LAST_NAME = "last_name";
    private static final String TABLE_COLUMN_AGE = "age";
    private static final String TABLE_COLUMN_CREATED = "created";
    private static final String DB_NAME = "test_db";
    private static final int DB_VERSION = 1;
    private static final String TABLE_NAME = "test_table";
    private static ListViewData instance = null;
    private SQLiteDatabase database;
    private Cursor cursor;

    private ListViewData(Context context) {
        SQLiteOpenHelper helper = new SQLiteOpenHelper(context, DB_NAME, null, DB_VERSION) {
            @Override
            public void onCreate(SQLiteDatabase db) {
                db.execSQL(getCreateTableText());
                db.execSQL(getCreateIndex1Text());
                db.execSQL(getCreateIndex2Text());
                db.execSQL(getCreateIndex3Text());
            }

            private String getCreateTableText() {
                String commandText = "CREATE TABLE " + TABLE_NAME + " (" +
                        TABLE_COLUMN_ID + " integer primary key autoincrement not null, " +
                        TABLE_COLUMN_FIRST_NAME + " text not null, " +
                        TABLE_COLUMN_LAST_NAME + " text not null, " +
                        TABLE_COLUMN_AGE + " integer null, " +
                        TABLE_COLUMN_CREATED + " integer not null);";

                return commandText;
            }

            private String getCreateIndex1Text() {
                String commandText = "CREATE INDEX IX_" + TABLE_NAME + "_" + TABLE_COLUMN_FIRST_NAME +
                        " ON " + TABLE_NAME + " (" +
                        TABLE_COLUMN_FIRST_NAME + ");";

                return commandText;
            }

            private String getCreateIndex2Text() {
                String commandText = "CREATE INDEX IX_" + TABLE_NAME + "_" + TABLE_COLUMN_LAST_NAME +
                        " ON " + TABLE_NAME + " (" +
                        TABLE_COLUMN_LAST_NAME + ");";

                return commandText;
            }

            private String getCreateIndex3Text() {
                String commandText = "CREATE INDEX IX_" + TABLE_NAME + "_" + TABLE_COLUMN_AGE +
                        " ON " + TABLE_NAME + " (" +
                        TABLE_COLUMN_AGE + ");";

                return commandText;
            }

            @Override
            public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            }
        };
        SQLiteDatabase.loadLibs(context);
        database = helper.getWritableDatabase("ZAQ1234rfv!@#$");

        reload();
    }

    public static ListViewData getInstance(Context context) {
        if (instance == null) {
            instance = new ListViewData(context);
        }

        return instance;
    }

    private static ListViewItem getItem(Cursor c) {
        int id = c.getInt(c.getColumnIndex(TABLE_COLUMN_ID));
        String firstName = c.getString(c.getColumnIndex(TABLE_COLUMN_FIRST_NAME));
        String lastName = c.getString(c.getColumnIndex(TABLE_COLUMN_LAST_NAME));
        Integer age = null;
        if (!c.isNull(c.getColumnIndex(TABLE_COLUMN_AGE))) {
            age = c.getInt(c.getColumnIndex(TABLE_COLUMN_AGE));
        }
        Date dateCreated = new Date(c.getLong(c.getColumnIndex(TABLE_COLUMN_CREATED)));
        ListViewItem item = new ListViewItem();
        item.setId(id);
        item.setFirstName(firstName);
        item.setLastName(lastName);
        if (age != null)
            item.setAge(age);
        item.setDateCreated(dateCreated);

        return item;
    }

    private static String getSelectText(long itemId) {
        String commandText = "SELECT * FROM " + TABLE_NAME + " WHERE " +
                TABLE_COLUMN_ID + " = " + itemId + ";";

        return commandText;
    }

    private String getSelectText() {
        String commandText = "SELECT * FROM " + TABLE_NAME + " ORDER BY " +
                TABLE_COLUMN_LAST_NAME + ", " +
                TABLE_COLUMN_FIRST_NAME + ";";

        return commandText;
    }

    public void add(ListViewItem item) {
        SQLiteStatement statement = null;

        try {
            statement = getInsertStatement();
            statement.bindString(1, item.getFirstName());
            statement.bindString(2, item.getLastName());
            Integer age = item.getAge();
            if (age == null) {
                statement.bindNull(3);
            } else {
                statement.bindLong(3, age.longValue());
            }
            statement.bindLong(4, item.getDateCreated().getTime());
            statement.executeInsert();
        } finally {
            if (statement != null) {
                statement.close();
            }
        }
    }

    private SQLiteStatement getInsertStatement() {
        String commandText = "INSERT INTO " + TABLE_NAME + " (" +
                TABLE_COLUMN_FIRST_NAME + ", " +
                TABLE_COLUMN_LAST_NAME + ", " +
                TABLE_COLUMN_AGE + ", " +
                TABLE_COLUMN_CREATED +
                ") VALUES (?, ?, ?, ?);";

        return database.compileStatement(commandText);
    }

    public void update(ListViewItem item) {
        SQLiteStatement statement = null;

        try {
            statement = getUpdateStatement();
            statement.bindString(1, item.getFirstName());
            statement.bindString(2, item.getLastName());
            Integer age = item.getAge();
            if (age == null) {
                statement.bindNull(3);
            } else {
                statement.bindLong(3, age.longValue());
            }
            statement.bindLong(4, item.getId());
            statement.executeUpdateDelete();
        } finally {
            if (statement != null) {
                statement.close();
            }
        }
    }

    private SQLiteStatement getUpdateStatement() {
        String commandText = "UPDATE " + TABLE_NAME + " SET " +
                TABLE_COLUMN_FIRST_NAME + " = ?, " +
                TABLE_COLUMN_LAST_NAME + " = ?, " +
                TABLE_COLUMN_AGE + " = ? " +
                "WHERE " + TABLE_COLUMN_ID + " = ?;";

        return database.compileStatement(commandText);
    }

    public void delete(int position) {
        long id = getItemId(position);
        SQLiteStatement statement = null;

        try {
            statement = getDeleteStatement();
            statement.bindLong(1, id);
            statement.executeUpdateDelete();
        } finally {
            if (statement != null) {
                statement.close();
            }
        }
    }

    public void deleteAll() {
        SQLiteStatement statement = null;

        try {
            statement = getDeleteAllStatement();
            statement.executeUpdateDelete();
        } finally {
            if (statement != null) {
                statement.close();
            }
        }
    }

    private SQLiteStatement getDeleteStatement() {
        String commandText = "DELETE FROM " + TABLE_NAME + " WHERE " +
                TABLE_COLUMN_ID + " = ?;";
        return database.compileStatement(commandText);
    }

    private SQLiteStatement getDeleteAllStatement() {
        String commandText = "DELETE FROM " + TABLE_NAME + ";";
        return database.compileStatement(commandText);
    }

    public int getCount() {
        return cursor.getCount();
    }

    public ListViewItem getItemByPosition(int position) {
        cursor.moveToPosition(position);

        return getItem(cursor);
    }

    public long getItemId(int position) {
        cursor.moveToPosition(position);

        return cursor.getInt(cursor.getColumnIndex(TABLE_COLUMN_ID));
    }

    private void closeCursor() {
        if (cursor != null) {
            if (!cursor.isClosed()) {
                cursor.close();
            }
            cursor = null;
        }
    }

    public void reload() {
        closeCursor();
        cursor = database.rawQuery(getSelectText(), null);
    }

    public ListViewItem getItemById(long itemId) {
        Cursor c = database.rawQuery(getSelectText(itemId), null);
        c.moveToFirst();

        return getItem(c);
    }

    public void beginTransaction() {
        database.beginTransaction();
    }

    public void commitTransaction() {
        database.setTransactionSuccessful();
    }

    public void endTransaction() {
        database.endTransaction();
    }
}
