package com.example.petr.sqlciphertest;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

public class MainActivity extends Activity {

    private ListViewAdapter adapter = null;
    private TextView textViewCount = null;
    private AdapterView.OnItemClickListener onListViewItemClickListener =
            new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent intent = new Intent(MainActivity.this, EditActivity.class);
                    intent.putExtra(EditActivity.ID_KEY_NAME, id);
                    startActivityForResult(intent, EditActivity.EDIT_REQUEST_CODE);
                }
            };
    private AdapterView.OnItemLongClickListener onListViewItemLongClickListener =
            new AdapterView.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                    adapter.delete(position);
                    updateCount();

                    return true;
                }
            };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        ListViewData data = ListViewData.getInstance(getApplicationContext());
        adapter = new ListViewAdapter(data);
        final ListView listViewItems = (ListView) findViewById(R.id.listViewItems);
        listViewItems.setOnItemClickListener(onListViewItemClickListener);
        listViewItems.setOnItemLongClickListener(onListViewItemLongClickListener);
        listViewItems.setAdapter(adapter);
        textViewCount = (TextView) findViewById(R.id.textViewCount);

        updateCount();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == EditActivity.EDIT_REQUEST_CODE &&
                resultCode == RESULT_OK) {
            adapter.reloadData();
            updateCount();

            return;
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add:
                Intent intent = new Intent(MainActivity.this, EditActivity.class);
                startActivityForResult(intent, EditActivity.EDIT_REQUEST_CODE);

                return true;

            case R.id.action_add_random:
                adapter.addRandomItem();
                updateCount();

                return true;

            case R.id.action_add_n_random:
                adapter.addNRandomItems();
                updateCount();

                return true;

            case R.id.action_delete_all:
                adapter.deleteAll();
                updateCount();

                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void updateCount() {
        textViewCount.setText("(" + adapter.getCount() + ")");
    }
}
