package com.example.petr.sqlciphertest;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class EditActivity extends Activity {

    public static final String ID_KEY_NAME = "Id";
    public static final int EDIT_REQUEST_CODE = 123;
    private EditText textFirstName = null;
    private EditText textLastName = null;
    private EditText textAge = null;
    private ListViewData data = null;
    private View.OnClickListener onButtonSaveClickListener =
            new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String firstName = textFirstName.getText().toString();
                    String lastName = textLastName.getText().toString();
                    String ageText = textAge.getText().toString();
                    if (!firstName.isEmpty() && !lastName.isEmpty()) {
                        ListViewItem item = new ListViewItem();
                        item.setFirstName(firstName);
                        item.setLastName(lastName);
                        ListViewAdapter.setItemAge(ageText, item);
                        long itemId = getItemId();
                        if (itemId != ListViewItem.INVALID_ID) {
                            item.setId(itemId);
                            getData().update(item);
                        } else {
                            getData().add(item);
                        }

                        setResult(RESULT_OK);
                        finish();
                    }
                }
            };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_edit);
        textFirstName = (EditText) findViewById(R.id.textFirstName);
        textLastName = (EditText) findViewById(R.id.textLastName);
        textAge = (EditText) findViewById(R.id.textAge);
        findViewById(R.id.buttonSave).setOnClickListener(onButtonSaveClickListener);
        data = ListViewData.getInstance(getApplicationContext());

        long itemId = getItemId();
        if (itemId != ListViewItem.INVALID_ID) {
            ListViewItem item = getData().getItemById(itemId);
            textFirstName.setText(item.getFirstName());
            textLastName.setText(item.getLastName());
            ListViewAdapter.setAgeText(textAge, item);
        }
    }

    private long getItemId() {
        long itemId = ListViewItem.INVALID_ID;
        Intent intent = getIntent();
        if (intent.hasExtra(ID_KEY_NAME)) {
            itemId = intent.getLongExtra(ID_KEY_NAME, ListViewItem.INVALID_ID);
        }
        return itemId;
    }

    private ListViewData getData() {
        return data;
    }
}
