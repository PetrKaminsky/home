package com.example.petr.reftest;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.lang.ref.WeakReference;

public class MainActivity extends Activity {

    private Button buttonRun = null;
    private View.OnClickListener onButtonRun =
            new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AsyncCall call = new AsyncCall();
                    call.setListener(new AsyncCall.AsyncCallListener() {
                        @Override
                        public void onWorkerBegin() {
                            Log.i("async", "onWorkerBegin()");
                        }

                        @Override
                        public void onWorkerEnd() {
                            Log.i("async", "onWorkerEnd()");

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Log.i("async", "onWorkerEnd()/runOnUiThread()");

                                    showMessage();
                                }
                            });
                        }

                        @Override
                        public void onPostExecute() {
                            Log.i("async", "onPostExecute()");
                        }
                    });
                    call.execute();
                }
            };

    private void showMessage() {
        MessageDialog dialog = new MessageDialog();
        dialog.setMode(MessageDialog.Mode.OK);
        dialog.setTitle(getString(R.string.txt_title));
        dialog.setMessage(getString(R.string.txt_message));
        dialog.show(MainActivityData.getInstance().getActivity());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.i("life", "onCreate()");

        setContentView(R.layout.activity_main);
        buttonRun = (Button) findViewById(R.id.buttonRun);
        buttonRun.setOnClickListener(onButtonRun);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        Log.i("life", "onDestroy()");
    }

    @Override
    protected void onResume() {
        super.onResume();

        Log.i("life", "onResume()");
    }

    @Override
    protected void onPause() {
        super.onPause();

        Log.i("life", "onPause()");
    }

    @Override
    protected void onStart() {
        super.onStart();

        Log.i("life", "onStart()");

        MainActivityData.getInstance().setActivity(this);
    }

    @Override
    protected void onStop() {
        super.onStop();

        Log.i("life", "onStop()");

        MainActivityData.getInstance().setActivity(null);
    }

    private static class MainActivityData {
        private static final MainActivityData instance = new MainActivityData();
        private WeakReference<Activity> activity = null;

        private MainActivityData() {
        }

        public static MainActivityData getInstance() {
            return instance;
        }

        public Activity getActivity() {
            return activity.get();
        }

        public synchronized void setActivity(Activity a) {
            activity = new WeakReference<>(a);
        }
    }

    private static class AsyncCall extends AsyncTask<Void, Void, Void> {

        private AsyncCallListener listener = null;

        public void setListener(AsyncCallListener l) {
            listener = l;
        }

        @Override
        protected Void doInBackground(Void... params) {
            broadcastWorkerBegin();

            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
            }

            broadcastWorkerEnd();

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            broadcastPostExecute();
        }

        private void broadcastWorkerBegin() {
            if (listener != null) {
                listener.onWorkerBegin();
            }
        }

        private void broadcastWorkerEnd() {
            if (listener != null) {
                listener.onWorkerEnd();
            }
        }

        private void broadcastPostExecute() {
            if (listener != null) {
                listener.onPostExecute();
            }
        }

        private interface AsyncCallListener {
            void onWorkerBegin();

            void onWorkerEnd();

            void onPostExecute();
        }
    }
}
