package com.example.petr.reftest;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by petr on 2/16/17.
 */

public class MessageDialog extends DialogFragment {

    private MessageDialogClickListener clickListener;
    private Mode mode = Mode.OK;
    private Button buttonOk = null;
    private Button buttonCancel = null;
    private Button buttonYes = null;
    private Button buttonNo = null;
    private View.OnClickListener onOkClickListener =
            new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (MessageDialog.this.clickListener != null) {
                        MessageDialog.this.clickListener.OnOk(v);
                    }

                    dismiss();
                }
            };
    private View.OnClickListener onCancelClickListener =
            new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (MessageDialog.this.clickListener != null) {
                        MessageDialog.this.clickListener.OnCancel(v);
                    }

                    dismiss();
                }
            };
    private View.OnClickListener onYesClickListener =
            new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (MessageDialog.this.clickListener != null) {
                        MessageDialog.this.clickListener.OnYes(v);
                    }

                    dismiss();
                }
            };

    private View.OnClickListener onNoClickListener =
            new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (MessageDialog.this.clickListener != null) {
                        MessageDialog.this.clickListener.OnNo(v);
                    }

                    dismiss();
                }
            };

    public void setMode(Mode m) {
        mode = m;
    }

    public void setTitle(String title) {
        MessageDialogData.getInstance().setTitle(title);
    }

    public void setMessage(String message) {
        MessageDialogData.getInstance().setMessage(message);
    }

    public void setClickListener(MessageDialogClickListener l) {
        clickListener = l;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);

        String title = MessageDialogData.getInstance().getTitle();
        if (title == null || title.isEmpty()) {
            setStyle(STYLE_NO_TITLE, 0);
        } else {
            dialog.setTitle(title);
        }

        return dialog;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.message_dialog, container);
        final TextView textMessage = (TextView) view.findViewById(R.id.textMessage);
        textMessage.setText(MessageDialogData.getInstance().getMessage());
        buttonOk = (Button) view.findViewById(R.id.buttonOk);
        buttonOk.setOnClickListener(onOkClickListener);
        buttonCancel = (Button) view.findViewById(R.id.buttonCancel);
        buttonCancel.setOnClickListener(onCancelClickListener);
        buttonYes = (Button) view.findViewById(R.id.buttonYes);
        buttonYes.setOnClickListener(onYesClickListener);
        buttonNo = (Button) view.findViewById(R.id.buttonNo);
        buttonNo.setOnClickListener(onNoClickListener);

        updateButtonOk();
        updateButtonCancel();
        updateButtonYes();
        updateButtonNo();

        return view;
    }

    public void show(Activity activity) {
        if (activity != null) {
            FragmentManager manager = activity.getFragmentManager();
            if (manager != null) {
                show(manager, "");
            }
        }
    }

    private void updateButtonOk() {
        int visibility = View.GONE;
        switch (mode) {
            case OK:
            case OK_CANCEL:
                visibility = View.VISIBLE;
                break;
        }
        buttonOk.setVisibility(visibility);
    }

    private void updateButtonCancel() {
        int visibility = View.GONE;
        switch (mode) {
            case OK_CANCEL:
            case YES_NO_CANCEL:
                visibility = View.VISIBLE;
                break;
        }
        buttonCancel.setVisibility(visibility);
    }

    private void updateButtonYes() {
        int visibility = View.GONE;
        switch (mode) {
            case YES_NO:
            case YES_NO_CANCEL:
                visibility = View.VISIBLE;
                break;
        }
        buttonYes.setVisibility(visibility);
    }

    private void updateButtonNo() {
        int visibility = View.GONE;
        switch (mode) {
            case YES_NO:
            case YES_NO_CANCEL:
                visibility = View.VISIBLE;
                break;
        }
        buttonNo.setVisibility(visibility);
    }

    public enum Mode {
        OK,
        OK_CANCEL,
        YES_NO,
        YES_NO_CANCEL
    }

    public interface MessageDialogClickListener {
        void OnOk(View view);

        void OnCancel(View view);

        void OnYes(View view);

        void OnNo(View view);
    }

    public abstract static class MessageDialogClickAdapter implements MessageDialogClickListener {
        public void OnOk(View view) {
        }

        public void OnCancel(View view) {
        }

        public void OnYes(View view) {
        }

        public void OnNo(View view) {
        }
    }

    private static class MessageDialogData {
        private static final MessageDialogData instance = new MessageDialogData();
        private String title = null;
        private String message = null;

        private MessageDialogData() {
        }

        public static MessageDialogData getInstance() {
            return instance;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String t) {
            title = t;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String m) {
            message = m;
        }
    }
}
