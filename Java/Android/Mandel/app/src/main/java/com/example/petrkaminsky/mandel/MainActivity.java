package com.example.petrkaminsky.mandel;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class MainActivity extends Activity {

    private static final int PARAMETERS_REQUEST_CODE = 1;

    private MandelView.OnClickPositionListener onClickMandel = new MandelView.OnClickPositionListener() {
        @Override
        public void onClick(View view, int x, int y) {
            Log.d("trace", "onClick(), x: " + x + ", y: " + y);

            // zoom-in
        }
    };
    private View.OnLongClickListener onLongClickMandel = new View.OnLongClickListener() {
        @Override
        public boolean onLongClick(View view) {
            Intent intent = new Intent(MainActivity.this, ParametersActivity.class);
            startActivityForResult(intent, PARAMETERS_REQUEST_CODE);
            return true;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        final MandelView viewMandel = findViewById(R.id.viewMandel);
        viewMandel.setOnClickPositionListener(onClickMandel);
        viewMandel.setOnLongClickListener(onLongClickMandel);
    }

    @Override
    public void onBackPressed() {
        // zoom-out
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PARAMETERS_REQUEST_CODE &&
                resultCode == RESULT_OK) {
            // restart calculation
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
