package com.example.petrkaminsky.mandel;

import android.app.Application;
import android.content.Context;

/**
 * Created by petr.kaminsky on 11.7.2018.
 */

public class Mandel extends Application {

    public static Context getContext() {
        return context;
    }

    private static Context context = null;

    @Override
    public void onCreate() {
        super.onCreate();

        context = this;
    }
}
