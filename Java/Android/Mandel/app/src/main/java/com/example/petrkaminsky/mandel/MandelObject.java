package com.example.petrkaminsky.mandel;

import static java.lang.Math.sqrt;

/**
 * Created by petr.kaminsky on 12.7.2018.
 */

public class MandelObject {

    private static final MandelObject ourInstance = new MandelObject();
    private double realMin = -1;
    private double imaginaryMin = -1;
    private double realSize = 2;
    private double imaginarySize = 2;
    private int maxValue = 8;

    private MandelObject() {
    }

    public static MandelObject getInstance() {
        return ourInstance;
    }

    public void initialize(int width, int height) {
        realMin = Parameters.getInstance().getRealMin();
        imaginaryMin = Parameters.getInstance().getImaginaryMin();
        realSize = Parameters.getInstance().getRealSize();
        double imaginaryRealRatio = (double) height / width;
        imaginarySize = realSize * imaginaryRealRatio;
        maxValue = Parameters.getInstance().getMaxValue();
    }

    public int getMandelValue(double realRelative, double imaginaryRelative) {
        return calcMandelValue(
                realMin + realRelative * realSize,
                imaginaryMin + imaginaryRelative * imaginarySize);
    }

    private int calcMandelValue(double real, double imaginary) {
        int retVal = 0;
        double realCurr = real;
        double imaginaryCurr = imaginary;
        while (true) {
            if (retVal >= maxValue) {
                break;
            }
            double realSqr = realCurr * realCurr;
            double imaginarySqr = imaginaryCurr * imaginaryCurr;
            if (sqrt(realSqr + imaginarySqr) >= 2) {
                break;
            }
            double realNew = realSqr - imaginarySqr + real;
            double imaginaryNew = 2 * realCurr * imaginaryCurr + imaginary;
            realCurr = realNew;
            imaginaryCurr = imaginaryNew;
            retVal++;
        }
        return retVal;
    }
}
