package com.example.petrkaminsky.mandel;

import android.app.Activity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.EditText;

public class ParametersActivity extends Activity {

    private EditText editTextMaxValue = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_parameters);
        editTextMaxValue = findViewById(R.id.editTextMaxValue);
    }

    @Override
    protected void onResume() {
        super.onResume();

        Parameters.getInstance().load();
        editTextMaxValue.setText(Utils.toString(Parameters.getInstance().getMaxValue()));
    }

    @Override
    protected void onPause() {
        super.onPause();

        int maxValue = Integer.valueOf(editTextMaxValue.getText().toString());
        Parameters.getInstance().setMaxValue(maxValue);
        Parameters.getInstance().save();
    }

    @Override
    public void onBackPressed() {
        closeActivity();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                closeActivity();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void closeActivity() {
        setResult(RESULT_OK);
        finish();
    }
}
