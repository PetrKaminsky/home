package com.example.petrkaminsky.mandel;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import static android.os.Process.THREAD_PRIORITY_BACKGROUND;

/**
 * Created by petr.kaminsky on 11.7.2018.
 */

public class MandelView extends View {

    private Paint paint = new Paint();
    private Bitmap offBitmap = null;
    private boolean inEditMode = false;
    private OnClickPositionListener onClickPositionListener = null;
    private OnTouchListener onTouchListener = new OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (motionEvent.getActionMasked() == MotionEvent.ACTION_UP) {
                if (onClickPositionListener != null) {
                    onClickPositionListener.onClick(
                            view,
                            (int) motionEvent.getX(),
                            (int) motionEvent.getY());
                }
            }
            return false;
        }
    };

    public MandelView(Context context) {
        super(context);

        initialize();
    }

    public MandelView(Context context, AttributeSet attrs) {
        super(context, attrs);

        initialize();
    }

    public MandelView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        initialize();
    }

    private static int getNormalizedColorValue(int value, int maxValue) {
        return 255 * value / maxValue;
    }

    public void setOnClickPositionListener(OnClickPositionListener listener) {
        onClickPositionListener = listener;
    }

    private void initialize() {
        inEditMode = isInEditMode();
        paint.setDither(true);
        setOnTouchListener(onTouchListener);
        restartCalculation();
    }

    private void restartCalculation() {
    }

    private void stepCalculation() {
    }

    private void onPostCalculation() {
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if (inEditMode) {
            return;
        }

        canvas.drawColor(Color.BLACK);
        int width = getWidth();
        int height = getHeight();
        MandelData data = MandelData.getInstance();

        if (!data.isInitialized() ||
                width != data.getWidth() ||
                height != data.getHeight()) {
            data.initialize(
                    width,
                    height);
            MandelObject.getInstance().initialize(width, height);
            if (offBitmap != null) {
                offBitmap.recycle();
                offBitmap = null;
            }
        }
        if (offBitmap == null) {
            offBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        }

        Canvas offCanvas = new Canvas(offBitmap);
        int maxValue = Parameters.getInstance().getMaxValue();

        for (int j = 0; j < height; j++) {
            for (int i = 0; i < width; i++) {
                byte value = data.getValue(i, j);
                int normalizedValue = getNormalizedColorValue(value, maxValue);
                int color = Color.rgb(
                        normalizedValue,
                        normalizedValue,
                        normalizedValue);
                paint.setColor(color);
                offCanvas.drawPoint(i, j, paint);
            }
        }

        canvas.drawBitmap(offBitmap, 0, 0, paint);
    }

    private class CalculateTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            Thread.currentThread().setPriority(THREAD_PRIORITY_BACKGROUND);

            try {
            } catch (Exception e) {
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            Utils.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    MandelView.this.onPostCalculation();
                }
            });
        }
    }

    public interface OnClickPositionListener {
        void onClick(View view, int x, int y);
    }
}
