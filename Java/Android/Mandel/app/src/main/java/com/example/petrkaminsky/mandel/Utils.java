package com.example.petrkaminsky.mandel;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;

/**
 * Created by petr.kaminsky on 11.7.2018.
 */

public class Utils {

    public static String getString(int resId) {
        return getApplicationContext().getString(resId);
    }

    public static Context getApplicationContext() {
        return Mandel.getContext();
    }

    public static String getPackageName() {
        return getApplicationContext().getPackageName();
    }

    public static String toString(int n) {
        return String.valueOf(n);
    }

    public static void runOnUiThread(Runnable r) {
        new Handler(Looper.getMainLooper()).post(r);
    }
}
