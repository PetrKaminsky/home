package com.example.petrkaminsky.mandel;

import android.util.Log;

/**
 * Created by petr.kaminsky on 11.7.2018.
 */

public class MandelData {

    private static final MandelData ourInstance = new MandelData();
    private int width = 0;
    private int height = 0;
    private byte[] data = null;

    public static MandelData getInstance() {
        return ourInstance;
    }

    private MandelData() {
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public boolean isInitialized() {
        return width != 0 && height != 0;
    }

    public void initialize(int w, int h) {
        Log.d("trace", "MandelData.initialize()");

        width = w;
        height = h;

        Log.d("trace", "width: " + width);
        Log.d("trace", "height: " + height);

        allocateMemory();
    }

    private void allocateMemory() {
        Log.d("trace", "MandelData.allocateMemory()");

        int size = getSize();
        data = new byte[size];
    }

    private int getSize() {
        return width * height;
    }

    private int getIndex(int x, int y) {
        return x + width * y;
    }

    public byte getValue(int x, int y) {
        return data[getIndex(x, y)];
    }

    private void setValue(byte[] data, int x, int y, byte b) {
        data[getIndex(x, y)] = b;
    }
}
