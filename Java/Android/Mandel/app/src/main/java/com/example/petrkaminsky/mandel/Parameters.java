package com.example.petrkaminsky.mandel;

import android.content.SharedPreferences;
import android.util.Log;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by petr.kaminsky on 11.7.2018.
 */

public class Parameters {

    private static final Parameters instance = new Parameters();
    private static final double DEFAULT_REAL_MIN = -2.5;
    private static final double DEFAULT_IMAGINARY_MIN = -1.95;
    private static final double DEFAULT_REAL_SIZE = 3.5;
    private static final int DEFAULT_MAX_VALUE = 64;
    private SharedPreferences preferences = null;
    private double realMin = DEFAULT_REAL_MIN;
    private double imaginaryMin = DEFAULT_IMAGINARY_MIN;
    private double realSize = DEFAULT_REAL_SIZE;
    private int maxValue = DEFAULT_MAX_VALUE;

    private Parameters() {
        preferences = Utils.getApplicationContext().getSharedPreferences(
                Utils.getPackageName(),
                MODE_PRIVATE);
        load();
    }

    public static Parameters getInstance() {
        return instance;
    }

    public double getRealMin() {
        return realMin;
    }

    public void setRealMin(double v) {
        realMin = v;
    }

    public double getImaginaryMin() {
        return imaginaryMin;
    }

    public void setImaginaryMin(double v) {
        imaginaryMin = v;
    }

    public double getRealSize() {
        return realSize;
    }

    public void setRealSize(double v) {
        realSize = v;
    }

    public int getMaxValue() {
        return maxValue;
    }

    public void setMaxValue(int i) {
        maxValue = i;
    }

    public void load() {
        Log.d("trace", "Parameters.load()");

        setRealMin(preferences.getFloat(Utils.getString(R.string.parameter_real_min), (float) DEFAULT_REAL_MIN));
        setImaginaryMin(preferences.getFloat(Utils.getString(R.string.parameter_imaginary_min), (float) DEFAULT_IMAGINARY_MIN));
        setRealSize(preferences.getFloat(Utils.getString(R.string.parameter_real_size), (float) DEFAULT_REAL_SIZE));
        setMaxValue(preferences.getInt(Utils.getString(R.string.parameter_max_value), DEFAULT_MAX_VALUE));
    }

    public void save() {
        Log.d("trace", "Parameters.save()");

        preferences.edit()
                .putFloat(Utils.getString(R.string.parameter_real_min), (float) getRealMin())
                .putFloat(Utils.getString(R.string.parameter_imaginary_min), (float) getImaginaryMin())
                .putFloat(Utils.getString(R.string.parameter_real_size), (float) getRealSize())
                .putInt(Utils.getString(R.string.parameter_max_value), getMaxValue())
                .apply();
    }
}
