package com.example.petrkaminsky.gesturelistener;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;

public class MainActivity extends Activity {

    private GestureDetector detector = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        detector = new GestureDetector(this, new GestureListener());
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        detector.onTouchEvent(event);
        return super.onTouchEvent(event);
    }

    private class GestureListener implements GestureDetector.OnGestureListener {

        @Override
        public boolean onDown(MotionEvent event) {
            Log.d("trace", "GestureListener.onDown(): " + event.toString());
            return true;
        }

        @Override
        public void onShowPress(MotionEvent event) {
            Log.d("trace", "GestureListener.onShowPress(): " + event.toString());
        }

        @Override
        public boolean onSingleTapUp(MotionEvent event) {
            Log.d("trace", "GestureListener.onSingleTapUp(): " + event.toString());
            return true;
        }

        @Override
        public boolean onScroll(
                MotionEvent event1,
                MotionEvent event2,
                float velocityX,
                float velocityY) {
            Log.d("trace", "GestureListener.onScroll(): " + event1.toString() +
                    ", " + event2.toString() +
                    ", " + velocityX +
                    ", " + velocityY);
            return true;
        }

        @Override
        public void onLongPress(MotionEvent event) {
            Log.d("trace", "GestureListener.onLongPress(): " + event.toString());
        }

        @Override
        public boolean onFling(
                MotionEvent event1,
                MotionEvent event2,
                float velocityX,
                float velocityY) {
            Log.d("trace", "GestureListener.onFling(): " + event1.toString() +
                    ", " + event2.toString() +
                    ", " + velocityX +
                    ", " + velocityY);
            return true;
        }
    }
}
