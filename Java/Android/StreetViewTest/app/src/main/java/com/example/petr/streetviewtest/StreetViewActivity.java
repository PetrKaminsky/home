package com.example.petr.streetviewtest;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.google.android.gms.maps.OnStreetViewPanoramaReadyCallback;
import com.google.android.gms.maps.StreetViewPanorama;
import com.google.android.gms.maps.SupportStreetViewPanoramaFragment;
import com.google.android.gms.maps.model.LatLng;

public class StreetViewActivity extends AppCompatActivity {

    private EditText editTextLatitude = null;
    private EditText editTextLongitude = null;
    private StreetViewPanorama panorama = null;
    private OnStreetViewPanoramaReadyCallback panoramaReadyCallback = new OnStreetViewPanoramaReadyCallback() {
        @Override
        public void onStreetViewPanoramaReady(StreetViewPanorama streetViewPanorama) {
            panorama = streetViewPanorama;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_street_view);
        SupportStreetViewPanoramaFragment fragment = (SupportStreetViewPanoramaFragment) getSupportFragmentManager()
                .findFragmentById(R.id.streetView);
        fragment.getStreetViewPanoramaAsync(panoramaReadyCallback);
        editTextLatitude = (EditText) findViewById(R.id.editTextLatitude);
        editTextLongitude = (EditText) findViewById(R.id.editTextLongitude);
        findViewById(R.id.buttonStreetView).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                streetView(
                        editTextLatitude.getEditableText().toString(),
                        editTextLongitude.getEditableText().toString());
            }
        });
    }

    private void streetView(String txtLatitude, String txtLongitude) {
        streetView(
                Double.valueOf(txtLatitude),
                Double.valueOf(txtLongitude));
    }

    private void streetView(double latitude, double longitude) {
        Log.d("trace", "streetView() - latitude: " + latitude + ", longitude: " + longitude);

        if (panorama != null) {
            LatLng latLng = new LatLng(latitude, longitude);
            panorama.setPosition(latLng);
        }
    }
}
