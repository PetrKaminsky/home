package com.example.petrkaminsky.hodgepodge;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by petr on 10/29/17.
 */

public class HodgePodgeView extends View {

    private Paint paint = new Paint();
    private boolean inEditMode = false;
    private Bitmap offBitmap = null;

    public HodgePodgeView(Context context) {
        super(context);

        initialize();
    }

    public HodgePodgeView(Context context, AttributeSet attrs) {
        super(context, attrs);

        initialize();
    }

    public HodgePodgeView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        initialize();
    }

    private static int getNormalizedColorValue(int value, int maxValue) {
        return 255 * value / maxValue;
    }

    private void initialize() {
        inEditMode = isInEditMode();
        paint.setDither(true);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if (inEditMode) {
            return;
        }

        canvas.drawColor(Color.BLACK);

        GlobalData data = GlobalData.getInstance();

        int width = getWidth();
        int height = getHeight();

        if (!data.isInitialized() ||
                width != data.getWidth() ||
                height != data.getHeight()) {
            data.initialize(
                    width,
                    height);
            if (offBitmap != null) {
                offBitmap.recycle();
                offBitmap = null;
            }
        }
        if (offBitmap == null) {
            offBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        }

        Canvas offCanvas = new Canvas(offBitmap);
        int maxValue = Parameters.getInstance().getMaxValue();

        for (int j = 0; j < height; j++) {
            for (int i = 0; i < width; i++) {
                byte value = data.getValue(i, j);
                int normalizedValue = getNormalizedColorValue(value, maxValue);
                int color = Color.rgb(
                        normalizedValue,
                        normalizedValue,
                        normalizedValue);
                paint.setColor(color);
                offCanvas.drawPoint(i, j, paint);
            }
        }

        canvas.drawBitmap(offBitmap, 0, 0, paint);
    }
}
