package com.example.petrkaminsky.hodgepodge;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;

import java.util.Random;

import static android.os.Process.THREAD_PRIORITY_BACKGROUND;

public class MainActivity extends Activity {

    private static final int parametersRequestCode = 0;
    private boolean restartSignaled = false;
    private HodgePodgeView hodgePodgeView = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.i("life", "MainActivity.onCreate()");

        setContentView(R.layout.activity_main);
        hodgePodgeView = findViewById(R.id.hodgePodgeView);
        hodgePodgeView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Log.d("trace", "surfaceView.onLongClick()");

                showParametersActivity();

                return true;
            }
        });

        redrawContents();

        scheduleCalculation();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == parametersRequestCode) {
            restartSignaled = true;
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    private void showParametersActivity() {
        startActivityForResult(
                new Intent(this, ParametersActivity.class),
                parametersRequestCode);
    }

    private void redrawContents() {
        hodgePodgeView.invalidate();
    }

    private void onCalculateFinish() {
        Log.d("trace", "MainActivity.onCalculateFinish()");

        if (restartSignaled) {
            restartSignaled = false;
            GlobalData.getInstance().restartGeneration();
        } else {
            GlobalData.getInstance().switchCurrentData();
        }

        redrawContents();

        scheduleCalculation();
    }

    private void scheduleCalculation() {
        Log.d("trace", "MainActivity.scheduleCalculation()");

        Utils.delayExecution(new Runnable() {
            @Override
            public void run() {
                new CalculateTask().execute();
            }
        }, 200);
    }


    private class CalculateTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            Thread.currentThread().setPriority(THREAD_PRIORITY_BACKGROUND);

            Log.d("trace", "CalculateTask.doInBackground()");

            try {
                GlobalData.getInstance().calculateNextGeneration();
            } catch (Exception e) {
                Log.e("error", "CalculateTask.doInBackground()", e);
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            Log.d("trace", "CalculateTask.onPostExecute()");

            MainActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    MainActivity.this.onCalculateFinish();
                }
            });
        }
    }
}
