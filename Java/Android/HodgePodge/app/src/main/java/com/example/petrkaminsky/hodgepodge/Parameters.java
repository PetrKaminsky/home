package com.example.petrkaminsky.hodgepodge;

import android.content.SharedPreferences;
import android.util.Log;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by petr on 10/14/17.
 */

public class Parameters {

    private static final Parameters instance = new Parameters();
    private static final int DEFAULT_MAXVALUE = 31;
    private static final int DEFAULT_CONST_K1 = 3;
    private static final int DEFAULT_CONST_K2 = 3;
    private static final int DEFAULT_CONST_G = 6;
    private int maxValue = DEFAULT_MAXVALUE;
    private int constK1 = DEFAULT_CONST_K1;
    private int constK2 = DEFAULT_CONST_K2;
    private int constG = DEFAULT_CONST_G;
    private SharedPreferences preferences = null;

    private Parameters() {
        preferences = Utils.getApplicationContext().getSharedPreferences(
                Utils.getPackageName(),
                MODE_PRIVATE);
        load();
    }

    public static Parameters getInstance() {
        return instance;
    }

    public int getMaxValue() {
        return maxValue;
    }

    public void setMaxValue(int value) {
        maxValue = value;
    }

    public int getConstK1() {
        return constK1;
    }

    public void setConstK1(int k1) {
        constK1 = k1;
    }

    public int getConstK2() {
        return constK2;
    }

    public void setConstK2(int k2) {
        constK2 = k2;
    }

    public int getConstG() {
        return constG;
    }

    public void setConstG(int g) {
        constG = g;
    }

    public void load() {
        Log.d("trace", "Parameters.load()");

        setMaxValue(preferences.getInt(Utils.getString(R.string.preference_max_value), DEFAULT_MAXVALUE));
        setConstK1(preferences.getInt(Utils.getString(R.string.preference_const_k1), DEFAULT_CONST_K1));
        setConstK2(preferences.getInt(Utils.getString(R.string.preference_const_k2), DEFAULT_CONST_K2));
        setConstG(preferences.getInt(Utils.getString(R.string.preference_const_g), DEFAULT_CONST_G));
    }

    public void save() {
        Log.d("trace", "Parameters.save()");

        preferences.edit()
                .putInt(Utils.getString(R.string.preference_max_value), getMaxValue())
                .putInt(Utils.getString(R.string.preference_const_k1), getConstK1())
                .putInt(Utils.getString(R.string.preference_const_k2), getConstK2())
                .putInt(Utils.getString(R.string.preference_const_g), getConstG())
                .apply();
    }
}
