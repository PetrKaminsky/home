package com.example.petrkaminsky.hodgepodge;

import android.app.Application;
import android.content.Context;
import android.util.Log;

/**
 * Created by petr.kaminsky on 24.10.2017.
 */

public class HodgePodge extends Application {

    public static Context getContext() {
        return context;
    }

    private static Context context = null;

    @Override
    public void onCreate() {
        super.onCreate();

        Log.i("life", "HodgePodge.onCreate()");

        context = this;
    }
}
