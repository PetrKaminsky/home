package com.example.petrkaminsky.hodgepodge;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.EditText;

import java.util.Locale;

import static android.R.id.home;

public class ParametersActivity extends Activity {

    private EditText editTextConstK1 = null;
    private EditText editTextConstK2 = null;
    private EditText editTextConstG = null;
    private EditText editTextMaxValue = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.i("life", "ParametersActivity.onCreate()");

        setContentView(R.layout.activity_parameters);
        editTextConstK1 = findViewById(R.id.editTextConstK1);
        editTextConstK2 = findViewById(R.id.editTextConstK2);
        editTextConstG = findViewById(R.id.editTextConstG);
        editTextMaxValue = findViewById(R.id.editTextMaxValue);
    }

    @Override
    protected void onResume() {
        super.onResume();

        Log.i("life", "ParametersActivity.onResume()");

        Parameters parameters = Parameters.getInstance();
        editTextConstK1.setText(formatValue(parameters.getConstK1()));
        editTextConstK2.setText(formatValue(parameters.getConstK2()));
        editTextConstG.setText(formatValue(parameters.getConstG()));
        editTextMaxValue.setText(formatValue(parameters.getMaxValue()));
    }

    @Override
    public void onBackPressed() {
        saveAndClose();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case home:
                saveAndClose();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void saveAndClose() {
        try {
            Parameters parameters = Parameters.getInstance();
            parameters.setConstK1(parseValue(editTextConstK1.getText().toString()));
            parameters.setConstK2(parseValue(editTextConstK2.getText().toString()));
            parameters.setConstG(parseValue(editTextConstG.getText().toString()));
            parameters.setMaxValue(parseValue(editTextMaxValue.getText().toString()));

            parameters.save();

            finish();
        } catch (Exception ex) {
            Log.e("format", "ParametersActivity.saveAndClose()", ex);
        }
    }

    private String formatValue(int value) {
        return String.format(Locale.getDefault(), "%d", value);
    }

    private int parseValue(String textValue) {
        return Integer.valueOf(textValue);
    }
}
