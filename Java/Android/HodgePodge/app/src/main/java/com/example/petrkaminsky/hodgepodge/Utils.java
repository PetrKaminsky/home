package com.example.petrkaminsky.hodgepodge;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;

/**
 * Created by petr.kaminsky on 10.7.2018.
 */

public class Utils {
    public static String getString(int resId) {
        return getApplicationContext().getString(resId);
    }

    public static void delayExecution(Runnable r, long d) {
        new Handler(Looper.getMainLooper()).postDelayed(r, d);
    }

    public static void delayExecution(Runnable r) {
        delayExecution(r, 50);
    }

    public static Context getApplicationContext() {
        return HodgePodge.getContext();
    }

    public static String getPackageName() {
        return getApplicationContext().getPackageName();
    }
}
