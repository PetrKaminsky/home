package com.example.petrkaminsky.hodgepodge;

import android.util.Log;

import java.util.Random;

/**
 * Created by petr on 10/29/17.
 */

public class GlobalData {

    private static final GlobalData instance = new GlobalData();
    private int width = 0;
    private int height = 0;
    private byte[] data1 = null;
    private byte[] data2 = null;
    private byte[] currentData = null;
    private int maxValue = 0;
    private int constK1 = 0;
    private int constK2 = 0;
    private int constG = 0;

    private GlobalData() {
    }

    public static GlobalData getInstance() {
        return instance;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public boolean isInitialized() {
        return width != 0 && height != 0;
    }

    public void initialize(int w, int h) {
        Log.d("trace", "GlobalData.initialize()");

        width = w;
        height = h;

        Log.d("trace", "width: " + width);
        Log.d("trace", "height: " + height);

        allocateMemory();

        restartGeneration();
    }

    public void restartGeneration() {
        Log.d("trace", "GlobalData.restartGeneration()");

        currentData = data1;

        reloadParameters();
        randomizeFirstGeneration();
    }

    private void allocateMemory() {
        Log.d("trace", "GlobalData.allocateMemory()");

        int size = width * height;
        data1 = new byte[size];
        data2 = new byte[size];
    }

    private void randomizeFirstGeneration() {
        Log.d("trace", "GlobalData.randomizeFirstGeneration()");

        int bound = maxValue + 1;
        Random random = new Random();
        for (int i = 0; i < currentData.length; i++) {
            currentData[i] = (byte) random.nextInt(bound);
        }
    }

    private void reloadParameters() {
        Log.d("trace", "GlobalData.reloadParameters()");

        maxValue = Parameters.getInstance().getMaxValue();
        constK1 = Parameters.getInstance().getConstK1();
        constK2 = Parameters.getInstance().getConstK2();
        constG = Parameters.getInstance().getConstG();
    }

    public void calculateNextGeneration() {
        if (!isInitialized()) {
            return;
        }

        Log.d("trace", "GlobalData.calculateNextGeneration()");

        byte[] sourceData = currentData;
        byte[] destinationData = currentData == data1 ? data2 : data1;
        for (int j = 0; j < height; j++) {
            for (int i = 0; i < width; i++) {
                setValue(
                        destinationData,
                        i,
                        j,
                        (byte) calculateNewValue(
                                sourceData,
                                i,
                                j));
            }
        }
    }

    private int calculateNewValue(byte[] data, int x, int y) {
        int currState = getValue(data, x, y), newState = 0;
        if (isHealthy(currState)) {
            newState = calculateNewValueOfHealthy(data, x, y);
        } else if (isInfected(currState)) {
            newState = calculateNewValueOfInfected(data, x, y);
        }

        return newState;
    }

    private boolean isHealthy(int state) {
        return state == 0;
    }

    private boolean isInfected(int state) {
        return (state > 0 && state < maxValue);
    }

    private boolean isIll(int state) {
        return state == maxValue;
    }

    private int calculateNewValueOfHealthy(byte[] data, int x, int y) {
        int state, infectedCount = 0, illCount = 0;
        int minX = x - 1, maxX = x + 1;
        int minY = y - 1, maxY = y + 1;
        for (int j = minY; j <= maxY; j++) {
            for (int i = minX; i <= maxX; i++) {
                if (j == y && i == x) {
                    continue;
                }
                state = getValue(data, getRealX(i), getRealY(j));
                if (isInfected(state)) {
                    infectedCount++;
                } else if (isIll(state)) {
                    illCount++;
                }
            }
        }
        state = (int) ((double) infectedCount / constK1 + (double) illCount / constK2);
        if (state > maxValue) {
            state = maxValue;
        }

        return state;
    }

    private int calculateNewValueOfInfected(byte[] data, int x, int y) {
        int state, infectedCount = 0, sumState = 0;
        int minX = x - 1, maxX = x + 1;
        int minY = y - 1, maxY = y + 1;
        for (int j = minY; j <= maxY; j++) {
            for (int i = minX; i <= maxX; i++) {
                state = getValue(data, getRealX(i), getRealY(j));
                if (isInfected(state)) {
                    infectedCount++;
                }
                sumState += state;
            }
        }
        state = sumState / infectedCount + constG;
        if (state > maxValue) {
            state = maxValue;
        }

        return state;
    }

    public void switchCurrentData() {
        currentData = currentData == data1 ? data2 : data1;
    }

    private int getIndex(int x, int y) {
        return x + width * y;
    }

    private int getRealX(int x) {
        if (x < 0) {
            do {
                x += width;
            }
            while (x < 0);
        } else if (x >= width) {
            do {
                x -= width;
            }
            while (x >= width);
        }

        return x;
    }

    private int getRealY(int y) {
        if (y < 0) {
            do {
                y += height;
            }
            while (y < 0);
        } else if (y >= height) {
            do {
                y -= height;
            }
            while (y >= height);
        }

        return y;
    }

    public byte getValue(int x, int y) {
        return currentData[getIndex(x, y)];
    }

    private byte getValue(byte[] data, int x, int y) {
        return data[getIndex(x, y)];
    }

    private void setValue(byte[] data, int x, int y, byte b) {
        data[getIndex(x, y)] = b;
    }
}
