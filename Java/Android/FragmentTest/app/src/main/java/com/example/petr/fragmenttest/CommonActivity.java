package com.example.petr.fragmenttest;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;

/**
 * Created by petr on 11/15/17.
 */

public class CommonActivity extends Activity {

    private static final Object referencesLock = new Object();
    private static final Object activeExecutionsLock = new Object();
    private static InstanceReference<CommonActivity> references = new InstanceReference<>();
    private static ActiveExecutionQueue activeExecutions = new ActiveExecutionQueue();
    private boolean active = false;

    public boolean isActive() {
        boolean retVal = false;
        CommonActivity activity = getCurrentActivity();
        if (activity != null) {
            retVal = activity.active;
        }

        return retVal;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.i("life", getClass().getSimpleName() + ".onCreate(): " + this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        Log.i("life", getClass().getSimpleName() + ".onDestroy(): " + this);
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        Log.i("life", getClass().getSimpleName() + ".onPostCreate(): " + this);
    }

    @Override
    protected void onStart() {
        super.onStart();

        Log.i("life", getClass().getSimpleName() + ".onStart(): " + this);

        setCurrentActivity(this);
    }

    @Override
    protected void onStop() {
        super.onStop();

        Log.i("life", getClass().getSimpleName() + ".onStop(): " + this);

        setCurrentActivity(null);
    }

    @Override
    protected void onResume() {
        super.onResume();

        Log.i("life", getClass().getSimpleName() + ".onResume(): " + this);
    }

    @Override
    protected void onPause() {
        super.onPause();

        Log.i("life", getClass().getSimpleName() + ".onPause(): " + this);

        active = false;
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();

        active = true;
        executeQueued();
    }

    private void executeQueued() {
        ActiveExecutionQueue.Item item = null;
        while (true) {
            synchronized (activeExecutionsLock) {
                item = activeExecutions.dequeue();
            }
            if (item != null) {
                Log.d("trace", getClass().getSimpleName() + ".activeExecution() - dequeue");

                if (item.getItemClass().equals(getClass())) {
                    Log.d("trace", getClass().getSimpleName() + ".activeExecution() - resumed execution");

                    item.getItemRunnable().run();
                }
            } else {
                break;
            }
        }
    }

    public void activeExecution(Runnable r) {
        if (r != null) {
            if (isActive()) {
                executeQueued();

                Log.d("trace", getClass().getSimpleName() + ".activeExecution() - direct execution");

                r.run();
            } else {
                Log.d("trace", getClass().getSimpleName() + ".activeExecution() - enqueue");

                ActiveExecutionQueue.Item item = new ActiveExecutionQueue.Item(getClass(), r);
                synchronized (activeExecutionsLock) {
                    activeExecutions.enqueue(item);
                }
            }
        }
    }

    protected CommonActivity getCurrentActivity() {
        synchronized (referencesLock) {
            return references.get(getClass());
        }
    }

    protected void setCurrentActivity(CommonActivity activity) {
        synchronized (referencesLock) {
            references.put(getClass(), activity);
        }
    }

    public CommonActivity getStaticActivity() {
        return getCurrentActivity();
    }

    public <T extends CommonActivity> T getStaticActivity(Class<T> cls) {
        return (T)getCurrentActivity();
    }
}
