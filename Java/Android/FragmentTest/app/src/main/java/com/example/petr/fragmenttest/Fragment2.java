package com.example.petr.fragmenttest;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Arrays;

public class Fragment2 extends CommonFragment {

    private ChangeState previousStateListener = null;
    private ChangeState nextStateListener = null;
    private OperationState operationState = Fragment2Data.getInstance().getOperationState();

    public void setNextStateListener(ChangeState listener) {
        this.nextStateListener = listener;
    }

    public void setPreviousStateListener(ChangeState listener) {
        this.previousStateListener = listener;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_2, container, false);
        view.findViewById(R.id.buttonPrevious).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (previousStateListener != null) {
                    previousStateListener.OnChange();
                }
            }
        });
        view.findViewById(R.id.buttonNext).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                operationState.next();
            }
        });
        operationState.setChangeListener(new StateMachine.StateChangeAdapter<State, Event>() {
            @Override
            public void onPostChangeState(State newState) {
                super.onPostChangeState(newState);

                Log.d("state", "Fragment2.onPostChangeState(): " + newState);

                switch (operationState.getState()) {
                    case PROGRESS_1:
                        nextProgress1();
                        break;

                    case PROGRESS_2:
                        nextProgress2();
                        break;

                    case NEXT:
                        nextFragment();
                        break;
                }
            }

            @Override
            public void onChangeState(State oldState, State newState) {
                super.onChangeState(oldState, newState);

                Log.d("state", "Fragment2.onChangeState(): " + oldState + " --> " + newState);
            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        initializeState();
    }

    private void initializeState() {
        switch (operationState.getState()) {
            case INITIAL:
                operationState.initialize();
                break;
        }
    }

    private void nextProgress1() {
        ProgressDialog.showMessage(
                getActivity(),
                R.string.txt_fragment2_progress1,
                new Runnable() {
                    @Override
                    public void run() {
                        Utils.sleep(3400);
                    }
                },
                new Runnable() {
                    @Override
                    public void run() {
                        activeExecution(new Runnable() {
                            @Override
                            public void run() {
                                operationState.next();
                            }
                        });
                    }
                },
                new Runnable() {
                    @Override
                    public void run() {
                        activeExecution(new Runnable() {
                            @Override
                            public void run() {
                                operationState.initialize();
                            }
                        });
                    }
                });
    }

    private void nextProgress2() {
        ProgressDialog.showMessage(
                getActivity(),
                R.string.txt_fragment2_progress2,
                new Runnable() {
                    @Override
                    public void run() {
                        Utils.sleep(2600);
                    }
                },
                new Runnable() {
                    @Override
                    public void run() {
                        activeExecution(new Runnable() {
                            @Override
                            public void run() {
                                operationState.next();
                            }
                        });
                    }
                },
                new Runnable() {
                    @Override
                    public void run() {
                        activeExecution(new Runnable() {
                            @Override
                            public void run() {
                                operationState.initialize();
                            }
                        });
                    }
                });
    }

    private void nextFragment() {
        if (nextStateListener != null) {
            nextStateListener.OnChange();
        }
        operationState.initialize();
    }

    private enum State {
        INITIAL,
        WORK,
        PROGRESS_1,
        PROGRESS_2,
        NEXT
    }

    private enum Event {
        INITIALIZE,
        NEXT
    }

    private static class OperationState extends StateMachine<State, Event> {

        public OperationState() {
            super(State.INITIAL);
        }

        @Override
        protected void initStateChart() {
            statechart = Arrays.asList(
                    new Edge<>(State.INITIAL, Event.INITIALIZE, State.WORK),
                    new Edge<>(State.WORK, Event.NEXT, State.PROGRESS_1),
                    new Edge<>(State.PROGRESS_1, Event.NEXT, State.PROGRESS_2),
                    new Edge<>(State.PROGRESS_1, Event.INITIALIZE, State.WORK),
                    new Edge<>(State.PROGRESS_2, Event.NEXT, State.NEXT),
                    new Edge<>(State.PROGRESS_2, Event.INITIALIZE, State.WORK),
                    new Edge<>(State.NEXT, Event.INITIALIZE, State.WORK)
            );
        }

        public void initialize() {
            DelayProcessEvent(Event.INITIALIZE);
        }

        public void next() {
            DelayProcessEvent(Event.NEXT);
        }

        private void DelayProcessEvent(final Event e) {
            Utils.delayExecution(new Runnable() {
                @Override
                public void run() {
                    processEvent(e);
                }
            });
        }
    }

    private static class Fragment2Data {
        private static Fragment2Data instance = new Fragment2Data();
        private OperationState operationState = new OperationState();

        private Fragment2Data() {
        }

        public static Fragment2Data getInstance() {
            return instance;
        }

        public OperationState getOperationState() {
            return operationState;
        }
    }
}
