package com.example.petr.fragmenttest;

import android.app.Activity;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by petr on 2/16/17.
 */

public class MessageDialog extends DialogFragment {

    private Button buttonOk = null;
    private Button buttonCancel = null;
    private Button buttonYes = null;
    private Button buttonNo = null;
    private View.OnClickListener onOkClickListener =
            new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    MessageDialogClickListener clickListener = getClickListener();
                    if (clickListener != null) {
                        clickListener.OnOk(v);
                    }

                    dismiss();
                }
            };
    private View.OnClickListener onCancelClickListener =
            new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    MessageDialogClickListener clickListener = getClickListener();
                    if (clickListener != null) {
                        clickListener.OnCancel(v);
                    }

                    dismiss();
                }
            };
    private View.OnClickListener onYesClickListener =
            new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    MessageDialogClickListener clickListener = getClickListener();
                    if (clickListener != null) {
                        clickListener.OnYes(v);
                    }

                    dismiss();
                }
            };

    private View.OnClickListener onNoClickListener =
            new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    MessageDialogClickListener clickListener = getClickListener();
                    if (clickListener != null) {
                        clickListener.OnNo(v);
                    }

                    dismiss();
                }
            };

    public static void showMessage(
            Activity activity,
            DialogStyle style,
            String title,
            String message,
            MessageDialogClickListener clickListener) {
        if (Utils.isActivityValid(activity)) {
            MessageDialog dialog = new MessageDialog();
            dialog.setDialogStyle(style);
            dialog.setTitle(title);
            dialog.setMessage(message);
            dialog.setClickListener(clickListener);
            dialog.show(activity);
        }
    }

    public static void showMessageOk(
            Activity activity,
            String title,
            String message) {
        showMessageOk(
                activity,
                title,
                message,
                null);
    }

    public static void showMessageOk(
            Activity activity,
            int titleId,
            int messageId) {
        showMessageOk(
                activity,
                Utils.getString(titleId),
                Utils.getString(messageId));
    }

    public static void showMessageOk(
            Activity activity,
            String message) {
        showMessageOk(
                activity,
                Utils.getString(R.string.lbl_message),
                message);
    }

    public static void showMessageOk(
            Activity activity,
            int messageId) {
        showMessageOk(
                activity,
                R.string.lbl_message,
                messageId);
    }

    public static void showMessageOk(
            Activity activity,
            String title,
            String message,
            MessageDialogClickListener clickListener) {
        showMessage(
                activity,
                DialogStyle.OK,
                title,
                message,
                clickListener);
    }

    public static void showMessageOk(
            Activity activity,
            int titleId,
            int messageId,
            MessageDialogClickListener clickListener) {
        showMessageOk(
                activity,
                Utils.getString(titleId),
                Utils.getString(messageId),
                clickListener);
    }

    public static void showMessageOk(
            Activity activity,
            String message,
            MessageDialogClickListener clickListener) {
        showMessageOk(
                activity,
                Utils.getString(R.string.lbl_message),
                message,
                clickListener);
    }

    public static void showMessageOk(
            Activity activity,
            int messageId,
            MessageDialogClickListener clickListener) {
        showMessageOk(
                activity,
                Utils.getString(messageId),
                clickListener);
    }

    public static void showMessageOkCancel(
            Activity activity,
            String title,
            String message,
            MessageDialogClickListener clickListener) {
        showMessage(
                activity,
                DialogStyle.OK_CANCEL,
                title,
                message,
                clickListener);
    }

    public static void showMessageOkCancel(
            Activity activity,
            int titleId,
            int messageId,
            MessageDialogClickListener clickListener) {
        showMessageOkCancel(
                activity,
                Utils.getString(titleId),
                Utils.getString(messageId),
                clickListener);
    }

    public static void showMessageOkCancel(
            Activity activity,
            String message,
            MessageDialogClickListener clickListener) {
        showMessageOkCancel(
                activity,
                Utils.getString(R.string.lbl_message),
                message,
                clickListener);
    }

    public static void showMessageOkCancel(
            Activity activity,
            int messageId,
            MessageDialogClickListener clickListener) {
        showMessageOkCancel(
                activity,
                R.string.lbl_message,
                messageId,
                clickListener);
    }

    public static void showMessageYesNo(
            Activity activity,
            String title,
            String message,
            MessageDialogClickListener clickListener) {
        showMessage(
                activity,
                DialogStyle.YES_NO,
                title,
                message,
                clickListener);
    }

    public static void showMessageYesNo(
            Activity activity,
            int titleId,
            int messageId,
            MessageDialogClickListener clickListener) {
        showMessageYesNo(
                activity,
                Utils.getString(titleId),
                Utils.getString(messageId),
                clickListener);
    }

    public static void showMessageYesNo(
            Activity activity,
            String message,
            MessageDialogClickListener clickListener) {
        showMessageYesNo(
                activity,
                Utils.getString(R.string.lbl_message),
                message,
                clickListener);
    }

    public static void showMessageYesNo(
            Activity activity,
            int messageId,
            MessageDialogClickListener clickListener) {
        showMessageYesNo(
                activity,
                R.string.lbl_message,
                messageId,
                clickListener);
    }

    public static void showMessageYesNoCancel(
            Activity activity,
            String title,
            String message,
            MessageDialogClickListener clickListener) {
        showMessage(
                activity,
                DialogStyle.YES_NO_CANCEL,
                title,
                message,
                clickListener);
    }

    public static void showMessageYesNoCancel(
            Activity activity,
            int titleId,
            int messageId,
            MessageDialogClickListener clickListener) {
        showMessageYesNoCancel(
                activity,
                Utils.getString(titleId),
                Utils.getString(messageId),
                clickListener);
    }

    public static void showMessageYesNoCancel(
            Activity activity,
            String message,
            MessageDialogClickListener clickListener) {
        showMessageYesNoCancel(
                activity,
                Utils.getString(R.string.lbl_message),
                message,
                clickListener);
    }

    public static void showMessageYesNoCancel(
            Activity activity,
            int messageId,
            MessageDialogClickListener clickListener) {
        showMessageYesNoCancel(
                activity,
                R.string.lbl_message,
                messageId,
                clickListener);
    }

    private DialogStyle getDialogStyle() {
        return MessageDialogData.getInstance().getDialogStyle();
    }

    private void setDialogStyle(DialogStyle s) {
        MessageDialogData.getInstance().setDialogStyle(s);
    }

    private String getTitle() {
        return MessageDialogData.getInstance().getTitle();
    }

    private void setTitle(String title) {
        MessageDialogData.getInstance().setTitle(title);
    }

    private String getMessage() {
        return MessageDialogData.getInstance().getMessage();
    }

    private void setMessage(String message) {
        MessageDialogData.getInstance().setMessage(message);
    }

    private MessageDialogClickListener getClickListener() {
        return MessageDialogData.getInstance().getClickListener();
    }

    private void setClickListener(MessageDialogClickListener l) {
        MessageDialogData.getInstance().setClickListener(l);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().requestWindowFeature(STYLE_NO_TITLE);

        final View view = inflater.inflate(R.layout.message_dialog, container);
        final TextView textTitle = (TextView) view.findViewById(R.id.textTitle);
        textTitle.setText(getTitle());
        final TextView textMessage = (TextView) view.findViewById(R.id.textMessage);
        textMessage.setText(getMessage());
        buttonOk = (Button) view.findViewById(R.id.buttonOk);
        buttonOk.setOnClickListener(onOkClickListener);
        buttonCancel = (Button) view.findViewById(R.id.buttonCancel);
        buttonCancel.setOnClickListener(onCancelClickListener);
        buttonYes = (Button) view.findViewById(R.id.buttonYes);
        buttonYes.setOnClickListener(onYesClickListener);
        buttonNo = (Button) view.findViewById(R.id.buttonNo);
        buttonNo.setOnClickListener(onNoClickListener);

        updateButtons();

        return view;
    }

    public void show(Activity activity) {
        if (activity != null &&
                activity.getFragmentManager() != null) {
            Utils.playQuestion();

            show(activity.getFragmentManager(), "");
        }
    }

    private void updateButtons() {
        switch (getDialogStyle()) {
            case OK:
                buttonOk.setVisibility(View.VISIBLE);
                buttonCancel.setVisibility(View.GONE);
                buttonYes.setVisibility(View.GONE);
                buttonNo.setVisibility(View.GONE);
                break;

            case OK_CANCEL:
                buttonOk.setVisibility(View.VISIBLE);
                buttonCancel.setVisibility(View.VISIBLE);
                buttonYes.setVisibility(View.GONE);
                buttonNo.setVisibility(View.GONE);
                break;

            case YES_NO:
                buttonOk.setVisibility(View.GONE);
                buttonCancel.setVisibility(View.GONE);
                buttonYes.setVisibility(View.VISIBLE);
                buttonNo.setVisibility(View.VISIBLE);
                break;

            case YES_NO_CANCEL:
                buttonOk.setVisibility(View.GONE);
                buttonCancel.setVisibility(View.VISIBLE);
                buttonYes.setVisibility(View.VISIBLE);
                buttonNo.setVisibility(View.VISIBLE);
                break;
        }
    }

    private enum DialogStyle {
        OK,
        OK_CANCEL,
        YES_NO,
        YES_NO_CANCEL
    }

    public interface MessageDialogClickListener {
        void OnOk(View view);

        void OnCancel(View view);

        void OnYes(View view);

        void OnNo(View view);
    }

    public abstract static class MessageDialogClickAdapter implements MessageDialogClickListener {
        public void OnOk(View view) {
        }

        public void OnCancel(View view) {
        }

        public void OnYes(View view) {
        }

        public void OnNo(View view) {
        }
    }

    private static class MessageDialogData {
        private static final MessageDialogData instance = new MessageDialogData();
        private String title = null;
        private String message = null;
        private DialogStyle dialogStyle = DialogStyle.OK;
        private MessageDialogClickListener clickListener = null;

        private MessageDialogData() {
        }

        public static MessageDialogData getInstance() {
            return instance;
        }

        public MessageDialogClickListener getClickListener() {
            return clickListener;
        }

        public void setClickListener(MessageDialogClickListener clickListener) {
            this.clickListener = clickListener;
        }

        public DialogStyle getDialogStyle() {
            return dialogStyle;
        }

        public void setDialogStyle(DialogStyle s) {
            dialogStyle = s;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String t) {
            title = t;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String m) {
            message = m;
        }
    }
}
