package com.example.petr.fragmenttest;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;

/**
 * Created by petr on 11/15/17.
 */

public class CommonApplication extends Application implements Application.ActivityLifecycleCallbacks {

    private static Application application = null;

    public static Application getApplication() {
        return application;
    }

    public static Context getContext() {
        return application;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        Log.i("life", getClass().getSimpleName() + ".onCreate(): " + this);

        application = this;

        registerActivityLifecycleCallbacks(this);
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
        Log.i("life", getClass().getSimpleName() + ".onActivityCreated(): " + activity.getClass().getSimpleName() + ", " + activity);
    }

    @Override
    public void onActivityStarted(Activity activity) {
        Log.i("life", getClass().getSimpleName() + ".onActivityStarted(): " + activity.getClass().getSimpleName() + ", " + activity);
    }

    @Override
    public void onActivityResumed(Activity activity) {
        Log.i("life", getClass().getSimpleName() + ".onActivityResumed(): " + activity.getClass().getSimpleName() + ", " + activity);
    }

    @Override
    public void onActivityPaused(Activity activity) {
        Log.i("life", getClass().getSimpleName() + ".onActivityPaused(): " + activity.getClass().getSimpleName() + ", " + activity);
    }

    @Override
    public void onActivityStopped(Activity activity) {
        Log.i("life", getClass().getSimpleName() + ".onActivityStopped(): " + activity.getClass().getSimpleName() + ", " + activity);
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
        Log.i("life", getClass().getSimpleName() + ".onActivitySaveInstanceState(): " + activity.getClass().getSimpleName() + ", " + activity);
    }

    @Override
    public void onActivityDestroyed(Activity activity) {
        Log.i("life", getClass().getSimpleName() + ".onActivityDestroyed(): " + activity.getClass().getSimpleName() + ", " + activity);
    }
}
