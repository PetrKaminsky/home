package com.example.petr.fragmenttest;

import android.app.Activity;
import android.app.Fragment;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

/**
 * Created by petr on 11/13/17.
 */

public class Utils {
    public static void delayExecution(Runnable r, long delay) {
        new Handler().postDelayed(r, delay);
    }

    public static void delayExecution(Runnable r) {
        delayExecution(r, 0);
    }

    public static <T extends Fragment> T reuseFragment(Fragment fragment, Class<T> cls) {
        if (fragment == null || fragment.getClass() != cls) {
            try {
                return cls.newInstance();
            } catch (IllegalAccessException ex) {
                Log.e("error", "Utils.reuseFragment()", ex);
                return null;
            } catch (InstantiationException ex) {
                Log.e("error", "Utils.reuseFragment()", ex);
                return null;
            }
        }
        return (T) fragment;
    }

    public static String getString(int resId) {
        return CommonApplication.getContext().getString(resId);
    }

    public static void runOnUiThread(Runnable r) {
        new Handler(Looper.getMainLooper()).post(r);
    }

    public static void sleep(int milliseconds) {
        try {
            Thread.sleep(milliseconds);
        } catch (InterruptedException ex) {
        }
    }

    public static void playSound(int resId) {
        MediaPlayer player = MediaPlayer.create(CommonApplication.getContext(), resId);
        if (player != null) {
            player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    mp.release();
                }
            });
            player.start();
        }
    }

    public static void playQuestion() {
        playSound(R.raw.question);
    }

    public static boolean isActivityValid(Activity activity) {
        boolean retVal = false;
        if (activity != null) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1) {
                retVal = true;
            } else {
                retVal = !activity.isDestroyed();
            }
        }

        return retVal;
    }
}
