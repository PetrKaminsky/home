package com.example.petr.fragmenttest;

import java.util.List;

/**
 * Created by petr.kaminsky on 24.3.2017.
 */

public abstract class StateMachine<S extends Enum, E extends Enum> {

    protected List<Edge<S, E>> statechart = null;
    private S state;
    private StateChangeListener<S, E> changeListener = null;

    public StateMachine(S s) {
        state = s;

        initStateChart();
    }

    protected abstract void initStateChart();

    public void setChangeListener(StateChangeListener<S, E> listener) {
        changeListener = listener;
    }

    public S getState() {
        return state;
    }

    protected void processEvent(E event) {
        for (Edge<S, E> edge : statechart) {
            if (edge.getFromState() == state &&
                    edge.getEvent() == event) {
                if (setState(edge.getToState())) {
                    edgeAction(
                            edge.getFromState(),
                            edge.getEvent(),
                            edge.getToState());
                }

                return;
            }
        }
    }

    private boolean setState(S newState) {
        if (canChangeState(state, newState)) {
            S oldState = state;
            preChangeState(oldState, newState);

            state = newState;

            changeState(oldState, newState);
            postChangeState(newState);

            return true;
        }

        return false;
    }

    private boolean canChangeState(S oldState, S newState) {
        if (changeListener != null) {
            return changeListener.canChangeState(oldState, newState);
        }

        return false;
    }

    private void edgeAction(S oldState, E event, S newState) {
        if (changeListener != null) {
            changeListener.onEdgeAction(oldState, event, newState);
        }
    }

    private void preChangeState(S oldState, S newState) {
        if (changeListener != null) {
            changeListener.onPreChangeState(oldState, newState);
        }
    }

    private void changeState(S oldState, S newState) {
        if (changeListener != null) {
            changeListener.onChangeState(oldState, newState);
        }
    }

    private void postChangeState(S newState) {
        if (changeListener != null) {
            changeListener.onPostChangeState(newState);
        }
    }

    public interface StateChangeListener<S, E> {
        boolean canChangeState(S oldState, S newState);

        void onPreChangeState(S oldState, S newState);

        void onChangeState(S oldState, S newState);

        void onEdgeAction(S oldState, E event, S newState);

        void onPostChangeState(S newState);
    }

    public static abstract class StateChangeAdapter<S, E> implements StateChangeListener<S, E> {
        @Override
        public boolean canChangeState(S oldState, S newState) {
            return true;
        }

        @Override
        public void onPreChangeState(S oldState, S newState) {
        }

        @Override
        public void onChangeState(S oldState, S newState) {
        }

        @Override
        public void onEdgeAction(S oldState, E event, S newState) {
        }

        @Override
        public void onPostChangeState(S newState) {
        }
    }

    public static class Edge<S, E> {

        private S fromState;
        private S toState;
        private E event;

        public Edge(S f, E e, S t) {
            fromState = f;
            toState = t;
            event = e;
        }

        public S getFromState() {
            return fromState;
        }

        public S getToState() {
            return toState;
        }

        public E getEvent() {
            return event;
        }
    }
}
