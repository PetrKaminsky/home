package com.example.petr.fragmenttest;

import java.util.LinkedList;

/**
 * Created by petr on 12/7/17.
 */

public class ActiveExecutionQueue {
    private LinkedList<Item> itemQueue = new LinkedList<>();

    public void enqueue(Item item) {
        itemQueue.add(item);
    }

    public Item dequeue() {
        Item retVal = null;
        if (itemQueue.size() > 0) {
            retVal = itemQueue.remove();
        }

        return retVal;
    }

    public static class Item {
        private Class aClass;
        private Runnable runnable;

        public Item(Class c, Runnable r) {
            aClass = c;
            runnable = r;
        }

        public Class getItemClass() {
            return aClass;
        }

        public Runnable getItemRunnable() {
            return runnable;
        }
    }
}
