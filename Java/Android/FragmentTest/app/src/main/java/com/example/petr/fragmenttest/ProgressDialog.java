package com.example.petr.fragmenttest;

import android.app.Activity;
import android.app.DialogFragment;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class ProgressDialog extends DialogFragment {

    public static void showMessage(
            Activity activity,
            int messageId,
            Runnable action) {
        showMessage(
                activity,
                Utils.getString(messageId),
                action,
                null,
                null);
    }

    public static void showMessage(
            Activity activity,
            String message,
            Runnable action) {
        showMessage(
                activity,
                message,
                action,
                null,
                null);
    }

    public static void showMessage(
            Activity activity,
            int messageId,
            Runnable action,
            Runnable afterAction) {
        showMessage(
                activity,
                Utils.getString(messageId),
                action,
                afterAction,
                null);
    }

    public static void showMessage(
            Activity activity,
            String message,
            Runnable action,
            Runnable afterAction) {
        showMessage(
                activity,
                message,
                action,
                afterAction,
                null);
    }

    public static void showMessage(
            Activity activity,
            int messageId,
            Runnable action,
            Runnable afterAction,
            Runnable breakAction) {
        showMessage(
                activity,
                Utils.getString(messageId),
                action,
                afterAction,
                breakAction);
    }

    public static void showMessage(
            Activity activity,
            String message,
            final Runnable action,
            final Runnable afterAction,
            final Runnable breakAction) {
        final ProgressDialog dialog = new ProgressDialog();
        dialog.setMessage(message);
        dialog.setFinished(false);
        dialog.show(activity.getFragmentManager(), "");
        ProgressTask task = new ProgressTask();
        task.setAction(new Runnable() {
            @Override
            public void run() {
                try {
                    if (action != null) {
                        action.run();
                    }
                } catch (Exception ex) {
                    Log.e("error", "ProgressDialog.action.run()", ex);
                }
            }
        });
        task.setAfterAction(new Runnable() {
            @Override
            public void run() {
                try {
                    if (afterAction != null) {
                        afterAction.run();
                    }
                } catch (Exception ex) {
                    Log.e("error", "ProgressDialog.afterAction.run()", ex);
                } finally {
                    dialog.setFinished(true);
                    ProgressDialog d = dialog.getProgressDialog();
                    if (d != null) {
                        d.setProgressDialog(null);
                        d.setProgressTask(null);
                        d.dismiss();
                    }
                }
            }
        });
        task.setBreakAction(new Runnable() {
            @Override
            public void run() {
                try {
                    if (breakAction != null) {
                        breakAction.run();
                    }
                } catch (Exception ex) {
                    Log.e("error", "ProgressDialog.breakAction.run()", ex);
                } finally {
                    dialog.setFinished(true);
                    ProgressDialog d = dialog.getProgressDialog();
                    if (d != null) {
                        d.setProgressDialog(null);
                        d.setProgressTask(null);
                        d.dismiss();
                    }
                }
            }
        });
        dialog.setProgressTask(task);
        task.execute();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().requestWindowFeature(STYLE_NO_TITLE);

        View view = inflater.inflate(R.layout.progress_dialog, container, false);
        final TextView textView = (TextView) view.findViewById(R.id.textView);
        textView.setText(getMessage());
        view.findViewById(R.id.buttonBreak).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProgressTask t = getProgressTask();
                if (t != null) {
                    t.cancel(true);
                }
            }
        });

        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.i("life", getClass().getSimpleName() + ".onCreate(): " + this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        Log.i("life", getClass().getSimpleName() + ".onDestroy(): " + this);
    }

    @Override
    public void onStart() {
        super.onStart();

        Log.i("life", getClass().getSimpleName() + ".onStart(): " + this);

        if (!isFinished()) {
            setProgressDialog(this);
        } else {
            dismiss();
        }
    }

    @Override
    public void onStop() {
        super.onStop();

        Log.i("life", getClass().getSimpleName() + ".onStop(): " + this);

        setProgressDialog(null);
    }

    @Override
    public void onResume() {
        super.onResume();

        Log.i("life", getClass().getSimpleName() + ".onResume(): " + this);
    }

    @Override
    public void onPause() {
        super.onPause();

        Log.i("life", getClass().getSimpleName() + ".onPause(): " + this);
    }

    private String getMessage() {
        return ProgressDialogData.getInstance().getMessage();
    }

    private void setMessage(String m) {
        ProgressDialogData.getInstance().setMessage(m);
    }

    private boolean isFinished() {
        return ProgressDialogData.getInstance().isFinished();
    }

    private void setFinished(boolean f) {
        ProgressDialogData.getInstance().setFinished(f);
    }

    private ProgressDialog getProgressDialog() {
        return ProgressDialogData.getInstance().getProgressDialog();
    }

    private void setProgressDialog(ProgressDialog d) {
        ProgressDialogData.getInstance().setProgressDialog(d);
    }

    private ProgressTask getProgressTask() {
        return ProgressDialogData.getInstance().getProgressTask();
    }

    private void setProgressTask(ProgressTask t) {
        ProgressDialogData.getInstance().setProgressTask(t);
    }

    private static class ProgressTask extends AsyncTask<Void, Void, Void> {
        private Runnable action = null;
        private Runnable afterAction = null;
        private Runnable breakAction = null;

        public void setAction(Runnable r) {
            action = r;
        }

        public void setAfterAction(Runnable r) {
            afterAction = r;
        }

        public void setBreakAction(Runnable r) {
            breakAction = r;
        }

        @Override
        protected Void doInBackground(Void... params) {
            Log.d("trace", "ProgressTask.doInBackground()");

            if (action != null) {
                action.run();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            Log.d("trace", "ProgressTask.onPostExecute()");

            if (afterAction != null) {
                afterAction.run();
            }
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();

            Log.d("trace", "ProgressTask.onCancelled()");

            if (breakAction != null) {
                breakAction.run();
            }
        }
    }

    private static class ProgressDialogData {
        private static ProgressDialogData instance = new ProgressDialogData();
        private String message;
        private boolean finished;
        private ProgressDialog dialog;
        private ProgressTask task;

        private ProgressDialogData() {
        }

        public static ProgressDialogData getInstance() {
            return instance;
        }

        public ProgressTask getProgressTask() {
            return task;
        }

        public void setProgressTask(ProgressTask t) {
            task = t;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String m) {
            message = m;
        }

        public boolean isFinished() {
            return finished;
        }

        public void setFinished(boolean f) {
            finished = f;
        }

        public ProgressDialog getProgressDialog() {
            return dialog;
        }

        public void setProgressDialog(ProgressDialog d) {
            dialog = d;
        }
    }
}
