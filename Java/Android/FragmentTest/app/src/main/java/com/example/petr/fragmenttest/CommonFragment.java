package com.example.petr.fragmenttest;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;

/**
 * Created by petr on 11/13/17.
 */

public class CommonFragment extends Fragment {

    private static final Object referencesLock = new Object();
    private static final Object activeExecutionsLock = new Object();
    private static InstanceReference<CommonFragment> references = new InstanceReference<>();
    private static ActiveExecutionQueue activeExecutions = new ActiveExecutionQueue();
    private boolean active = false;

    public boolean isActive() {
        boolean retVal = false;
        CommonFragment fragment = getCurrentFragment();
        if (fragment != null) {
            retVal = fragment.active;
        }

        return retVal;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.i("life", getClass().getSimpleName() + ".onCreate(): " + this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        Log.i("life", getClass().getSimpleName() + ".onDestroy(): " + this);
    }

    @Override
    public void onActivityCreated(@Nullable final Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        onPostCreate(savedInstanceState);
    }

    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        Log.i("life", getClass().getSimpleName() + ".onPostCreate(): " + this);
    }

    @Override
    public void onStart() {
        super.onStart();

        Log.i("life", getClass().getSimpleName() + ".onStart(): " + this);

        setCurrentFragment(this);
    }

    @Override
    public void onStop() {
        super.onStop();

        Log.i("life", getClass().getSimpleName() + ".onStop(): " + this);

        setCurrentFragment(null);
    }

    @Override
    public void onResume() {
        super.onResume();

        Log.i("life", getClass().getSimpleName() + ".onResume(): " + this);

        active = true;
        executeQueued();
    }

    @Override
    public void onPause() {
        super.onPause();

        Log.i("life", getClass().getSimpleName() + ".onPause(): " + this);

        active = false;
    }

    private void executeQueued() {
        ActiveExecutionQueue.Item item = null;
        while (true) {
            synchronized (activeExecutionsLock) {
                item = activeExecutions.dequeue();
            }
            if (item != null) {
                Log.d("trace", getClass().getSimpleName() + ".activeExecution() - dequeue");

                if (item.getItemClass().equals(getClass())) {
                    Log.d("trace", getClass().getSimpleName() + ".activeExecution() - resumed execution");

                    item.getItemRunnable().run();
                }
            } else {
                break;
            }
        }
    }

    public void activeExecution(Runnable r) {
        if (r != null) {
            if (isActive()) {
                executeQueued();

                Log.d("trace", getClass().getSimpleName() + ".activeExecution() - direct execution");

                r.run();
            } else {
                Log.d("trace", getClass().getSimpleName() + ".activeExecution() - enqueue");

                ActiveExecutionQueue.Item item = new ActiveExecutionQueue.Item(getClass(), r);
                synchronized (activeExecutionsLock) {
                    activeExecutions.enqueue(item);
                }
            }
        }
    }

    protected CommonFragment getCurrentFragment() {
        synchronized (referencesLock) {
            return references.get(getClass());
        }
    }

    protected synchronized void setCurrentFragment(CommonFragment fragment) {
        synchronized (referencesLock) {
            references.put(getClass(), fragment);
        }
    }

    public CommonFragment getStaticFragment() {
        return getCurrentFragment();
    }

    public CommonActivity getStaticActivity() {
        CommonActivity retVal = null;
        CommonFragment fragment = getCurrentFragment();
        if (fragment != null) {
            Activity activity = fragment.getActivity();
            if (activity != null &&
                    activity instanceof CommonActivity) {
                retVal = ((CommonActivity)activity).getStaticActivity();
            }
        }

        return retVal;
    }

    public <T extends CommonFragment> T getStaticFragment(Class<T> cls) {
        return (T)getCurrentFragment();
    }
}
