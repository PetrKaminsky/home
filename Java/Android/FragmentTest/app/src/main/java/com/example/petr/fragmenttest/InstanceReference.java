package com.example.petr.fragmenttest;

import java.util.HashMap;

/**
 * Created by petr on 12/7/17.
 */

public class InstanceReference<T> {

    private HashMap<Class, T> references = new HashMap<>();

    public T get(Class c) {
        T retVal = null;
        if (references.containsKey(c)) {
            retVal = references.get(c);
        }

        return retVal;
    }

    public void put(Class c, T instance) {
        if (instance != null) {
            references.put(c, instance);
        } else {
            references.remove(c);
        }
    }
}
