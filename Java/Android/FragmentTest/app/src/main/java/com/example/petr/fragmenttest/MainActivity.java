package com.example.petr.fragmenttest;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.app.Fragment;
import android.util.Log;
import android.view.View;

import java.util.Arrays;

public class MainActivity extends CommonActivity {

    private OperationState operationState = MainActivityData.getInstance().getOperationState();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        operationState.setChangeListener(new StateMachine.StateChangeAdapter<State, Event>() {
            @Override
            public void onPostChangeState(State newState) {
                super.onPostChangeState(newState);

                Log.d("state", "MainActivity.onPostChangeState(): " + newState);

                initializeFragments();
            }

            @Override
            public void onChangeState(State oldState, State newState) {
                super.onChangeState(oldState, newState);

                Log.d("state", "MainActivity.onChangeState(): " + oldState + " --> " + newState);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        initializeState();
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        activeExecution(new Runnable() {
            @Override
            public void run() {
                initializeFragments();
            }
        });
    }

    private void initializeState() {
        switch (operationState.getState()) {
            case INITIAL:
                operationState.change1();
                break;
        }
    }

    private void initializeFragments() {
        Fragment fragment = getFragmentManager().findFragmentById(R.id.fragmentHolder);

        switch (operationState.getState()) {
            case STATE_1:
                Fragment1 fragment1 = Utils.reuseFragment(fragment, Fragment1.class);
                if (fragment1 != null) {
                    fragment1.setPreviousStateListener(new ChangeState() {
                        @Override
                        public void OnChange() {
                            operationState.change3();
                        }
                    });
                    fragment1.setNextStateListener(new ChangeState() {
                        @Override
                        public void OnChange() {
                            nextProgress1();
                        }
                    });
                }
                fragment = fragment1;
                break;

            case STATE_2:
                Fragment2 fragment2 = Utils.reuseFragment(fragment, Fragment2.class);
                if (fragment2 != null) {
                    fragment2.setPreviousStateListener(new ChangeState() {
                        @Override
                        public void OnChange() {
                            operationState.change1();
                        }
                    });
                    fragment2.setNextStateListener(new ChangeState() {
                        @Override
                        public void OnChange() {
                            operationState.change3();
                        }
                    });
                }
                fragment = fragment2;
                break;

            case STATE_3:
                Fragment3 fragment3 = Utils.reuseFragment(fragment, Fragment3.class);
                if (fragment3 != null) {
                    fragment3.setPreviousStateListener(new ChangeState() {
                        @Override
                        public void OnChange() {
                            operationState.change2();
                        }
                    });
                    fragment3.setNextStateListener(new ChangeState() {
                        @Override
                        public void OnChange() {
                            MessageDialog.showMessageYesNo(
                                    MainActivity.this,
                                    R.string.txt_continue,
                                    new MessageDialog.MessageDialogClickAdapter() {
                                        @Override
                                        public void OnYes(View view) {
                                            activeExecution(new Runnable() {
                                                @Override
                                                public void run() {
                                                    operationState.change1();
                                                }
                                            });
                                        }
                                    }
                            );
                        }
                    });
                }
                fragment = fragment3;
                break;
        }

        if (fragment != null) {
            getFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragmentHolder, fragment)
                    .commit();
        }
    }

    private void nextProgress1() {
        ProgressDialog.showMessage(
                this,
                R.string.txt_activity_progress1,
                new Runnable() {
                    @Override
                    public void run() {
                        Utils.sleep(5000);
                    }
                },
                new Runnable() {
                    @Override
                    public void run() {
                        activeExecution(new Runnable() {
                            @Override
                            public void run() {
                                operationState.change2();
                            }
                        });
                    }
                });
    }

    private enum State {
        INITIAL,
        STATE_1,
        STATE_2,
        STATE_3
    }

    private enum Event {
        CHANGE_1,
        CHANGE_2,
        CHANGE_3
    }

    private static class OperationState extends StateMachine<State, Event> {
        public OperationState() {
            super(State.INITIAL);
        }

        @Override
        protected void initStateChart() {
            statechart = Arrays.asList(
                    new Edge<>(State.INITIAL, Event.CHANGE_1, State.STATE_1),
                    new Edge<>(State.STATE_1, Event.CHANGE_2, State.STATE_2),
                    new Edge<>(State.STATE_1, Event.CHANGE_3, State.STATE_3),
                    new Edge<>(State.STATE_2, Event.CHANGE_3, State.STATE_3),
                    new Edge<>(State.STATE_2, Event.CHANGE_1, State.STATE_1),
                    new Edge<>(State.STATE_3, Event.CHANGE_1, State.STATE_1),
                    new Edge<>(State.STATE_3, Event.CHANGE_2, State.STATE_2)
            );
        }

        public void change1() {
            delayProcessEvent(Event.CHANGE_1);
        }

        public void change2() {
            delayProcessEvent(Event.CHANGE_2);
        }

        public void change3() {
            delayProcessEvent(Event.CHANGE_3);
        }

        private void delayProcessEvent(final Event e) {
            Utils.delayExecution(new Runnable() {
                @Override
                public void run() {
                    processEvent(e);
                }
            });
        }
    }

    private static class MainActivityData {
        private static MainActivityData instance = new MainActivityData();
        private OperationState operationState = new OperationState();

        private MainActivityData() {
        }

        public static MainActivityData getInstance() {
            return instance;
        }

        public OperationState getOperationState() {
            return operationState;
        }
    }
}
