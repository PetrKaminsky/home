package com.example.petr.fragmenttest;

/**
 * Created by petr on 11/13/17.
 */

public interface ChangeState {
    void OnChange();
}
