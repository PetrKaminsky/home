package com.example.petr.fragmenttest;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class Fragment1 extends CommonFragment {

    private ChangeState previousStateListener = null;
    private ChangeState nextStateListener = null;

    public void setNextStateListener(ChangeState listener) {
        this.nextStateListener = listener;
    }

    public void setPreviousStateListener(ChangeState listener) {
        this.previousStateListener = listener;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_1, container, false);
        view.findViewById(R.id.buttonPrevious).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MessageDialog.showMessageYesNo(
                        getActivity(),
                        R.string.txt_continue,
                        new MessageDialog.MessageDialogClickAdapter() {
                            @Override
                            public void OnYes(View view) {
                                activeExecution(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (previousStateListener != null) {
                                            previousStateListener.OnChange();
                                        }
                                    }
                                });
                            }
                        }
                );
            }
        });
        view.findViewById(R.id.buttonNext).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (nextStateListener != null) {
                    nextStateListener.OnChange();
                }
            }
        });

        return view;
    }
}
