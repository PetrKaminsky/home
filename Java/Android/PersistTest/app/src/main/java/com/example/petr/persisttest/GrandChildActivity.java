package com.example.petr.persisttest;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

public class GrandChildActivity extends PersistActivity {

    private static final String bundleTextName = "bundleText";
    private static String staticText = null;
    private String instanceText = null;
    private String bundleText = null;
    private EditText editTextStatic = null;
    private EditText editTextSingleton = null;
    private EditText editTextInstance = null;
    private EditText editTextBundle = null;
    private EditText editTextDefault = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        if (!getPersistApplication().getGlobalData().isProperlyInitialized()) {
//            Log.i("life", "GrandChildActivity/Recovery");
//
//            getPersistApplication().getGlobalData().setRecoveryInProgress();
//
//            finish();
//
//            return;
//        }

        setContentView(R.layout.grand_child);
        editTextStatic = (EditText) findViewById(R.id.editTextStatic);
        editTextSingleton = (EditText) findViewById(R.id.editTextSingleton);
        editTextInstance = (EditText) findViewById(R.id.editTextInstance);
        editTextBundle = (EditText) findViewById(R.id.editTextBundle);
        editTextDefault = (EditText) findViewById(R.id.editTextDefault);
        findViewById(R.id.buttonException).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("action", "GrandChildActivity/buttonException.onClick()");

                PersistApplication.throwException();
            }
        });
        findViewById(R.id.buttonRestart).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Log.i("action", "GrandChildActivity/buttonRestart.onClick()");
//
//                getPersistApplication().getGlobalData().setRestartInProgress();
//
//                finish();
            }
        });

//        if (savedInstanceState != null) {
//            Log.i("life", "GrandChildActivity.onCreate(), savedInstanceState != null");
//
//            bundleText = savedInstanceState.getString(bundleTextName);
//        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        if (savedInstanceState != null) {
            Log.i("life", "GrandChildActivity.onRestoreInstanceState(), savedInstanceState != null");

            bundleText = savedInstanceState.getString(bundleTextName);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putString(bundleTextName, bundleText);
    }

    @Override
    protected void onResume() {
        super.onResume();

        editTextStatic.setText(staticText);
        editTextSingleton.setText(getSingletonText());
        editTextInstance.setText(instanceText);
        editTextBundle.setText(bundleText);

        Log.d("text", "editTextStatic: " + editTextStatic.getText().toString());
        Log.d("text", "editTextSingleton: " + editTextSingleton.getText().toString());
        Log.d("text", "editTextBundle: " + editTextBundle.getText().toString());
        Log.d("text", "editTextInstance: " + editTextInstance.getText().toString());
        Log.d("text", "editTextDefault: " + editTextDefault.getText().toString());
    }

    @Override
    protected void onPause() {
        super.onPause();

        staticText = editTextStatic.getText().toString();
        setSingletonText(editTextSingleton.getText().toString());
        instanceText = editTextInstance.getText().toString();
        bundleText = editTextBundle.getText().toString();
    }

    public String getSingletonText() {
        return GrandChildActivityData.getInstance().getSingletonText();
    }

    public void setSingletonText(String singletonText) {
        GrandChildActivityData.getInstance().setSingletonText(singletonText);
    }

    private PersistApplication getPersistApplication() {
        return (PersistApplication) getApplication();
    }

    private static class GrandChildActivityData {
        private static GrandChildActivityData instance = new GrandChildActivityData();
        private String singletonText = null;

        private GrandChildActivityData() {
        }

        public static GrandChildActivityData getInstance() {
            return instance;
        }

        public String getSingletonText() {
            return singletonText;
        }

        public void setSingletonText(String text) {
            this.singletonText = text;
        }
    }
}
