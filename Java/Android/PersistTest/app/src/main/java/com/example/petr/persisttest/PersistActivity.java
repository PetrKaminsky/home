package com.example.petr.persisttest;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

/**
 * Created by petr on 11/22/17.
 */

public class PersistActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.i("life", getClass().getSimpleName() + ".onCreate(): " + this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        Log.i("life", getClass().getSimpleName() + ".onDestroy(): " + this);
    }

    @Override
    protected void onStart() {
        super.onStart();

        Log.i("life", getClass().getSimpleName() + ".onStart(): " + this);
    }

    @Override
    protected void onStop() {
        super.onStop();

        Log.i("life", getClass().getSimpleName() + ".onStop(): " + this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        Log.i("life", getClass().getSimpleName() + ".onResume(): " + this);
    }

    @Override
    protected void onPause() {
        super.onPause();

        Log.i("life", getClass().getSimpleName() + ".onPause(): " + this);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        Log.i("life", getClass().getSimpleName() + ".onRestoreInstanceState(): " + this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        Log.i("life", getClass().getSimpleName() + ".onSaveInstanceState(): " + this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.i("life", getClass().getSimpleName() + ".onOptionsItemSelected(): " + this);

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();

                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Log.i("life", getClass().getSimpleName() + ".onBackPressed(): " + this);

        finish();
    }
}
