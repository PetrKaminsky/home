package com.example.petr.persisttest;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends PersistActivity {

    private static final String bundleTextName = "bundleText";
    private static String staticText = null;
    private String instanceText = null;
    private String bundleText = null;
    private EditText editTextStatic = null;
    private EditText editTextSingleton = null;
    private EditText editTextInstance = null;
    private EditText editTextBundle = null;
    private EditText editTextDefault = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        if (getPersistApplication().getGlobalData().isRecoveryInProgress()) {
//            Log.i("life", "MainActivity/Recovery");
//
//            getPersistApplication().getGlobalData().unsetRecoveryInProgress();
//
//            finish();
//
//            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
//            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
//            startActivity(intent);
//
//            return;
//        }

        setContentView(R.layout.main);
        editTextStatic = (EditText) findViewById(R.id.editTextStatic);
        editTextSingleton = (EditText) findViewById(R.id.editTextSingleton);
        editTextInstance = (EditText) findViewById(R.id.editTextInstance);
        editTextBundle = (EditText) findViewById(R.id.editTextBundle);
        editTextDefault = (EditText) findViewById(R.id.editTextDefault);
        findViewById(R.id.buttonException).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("action", "MainActivity/buttonException.onClick()");

                PersistApplication.throwException();
            }
        });
        findViewById(R.id.buttonRestart).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Log.i("action", "MainActivity/buttonRestart.onClick()");
//
//                finish();
//
//                startActivity(new Intent(getApplicationContext(), MainActivity.class));
            }
        });
        findViewById(R.id.buttonChild).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("action", "MainActivity/buttonChild.onClick()");

                startActivity(new Intent(MainActivity.this, ChildActivity.class));
            }
        });

//        if (savedInstanceState != null) {
//            Log.i("life", "MainActivity.onCreate(), savedInstanceState != null");
//
//            bundleText = savedInstanceState.getString(bundleTextName);
//        }

//        getPersistApplication().getGlobalData().setProperlyInitialized();
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        if (savedInstanceState != null) {
            Log.i("life", "MainActivity.onRestoreInstanceState(), savedInstanceState != null");

            bundleText = savedInstanceState.getString(bundleTextName);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putString(bundleTextName, bundleText);
    }

    @Override
    protected void onStart() {
        super.onStart();

//        if (getPersistApplication().getGlobalData().isRestartInProgress()) {
//            Log.i("life", "MainActivity/Restart");
//
//            getPersistApplication().getGlobalData().unsetRestartInProgress();
//
//            finish();
//
//            startActivity(new Intent(getApplicationContext(), MainActivity.class));
//        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        editTextStatic.setText(staticText);
        editTextSingleton.setText(getSingletonText());
        editTextInstance.setText(instanceText);
        editTextBundle.setText(bundleText);

        Log.d("text", "editTextStatic: " + editTextStatic.getText().toString());
        Log.d("text", "editTextSingleton: " + editTextSingleton.getText().toString());
        Log.d("text", "editTextBundle: " + editTextBundle.getText().toString());
        Log.d("text", "editTextInstance: " + editTextInstance.getText().toString());
        Log.d("text", "editTextDefault: " + editTextDefault.getText().toString());
    }

    @Override
    protected void onPause() {
        super.onPause();

        staticText = editTextStatic.getText().toString();
        setSingletonText(editTextSingleton.getText().toString());
        instanceText = editTextInstance.getText().toString();
        bundleText = editTextBundle.getText().toString();
    }

    public String getSingletonText() {
        return MainActivityData.getInstance().getSingletonText();
    }

    public void setSingletonText(String singletonText) {
        MainActivityData.getInstance().setSingletonText(singletonText);
    }

    private PersistApplication getPersistApplication() {
        return (PersistApplication) getApplication();
    }

    private static class MainActivityData {
        private static MainActivityData instance = new MainActivityData();
        private String singletonText = null;

        private MainActivityData() {
        }

        public static MainActivityData getInstance() {
            return instance;
        }

        public String getSingletonText() {
            return singletonText;
        }

        public void setSingletonText(String text) {
            this.singletonText = text;
        }
    }
}
