package com.example.petr.persisttest;

import android.app.Application;
import android.util.Log;

/**
 * Created by petr on 9/23/17.
 */

public class PersistApplication extends Application {
    private PersistApplicationData globalData = new PersistApplicationData();

    public static void throwException() {
        int a = 1;
        int b = 0;
        int c = a / b;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        Log.i("life", "PersistApplication.onCreate()");
    }

    public PersistApplicationData getGlobalData() {
        return globalData;
    }

    public static class PersistApplicationData {
        private boolean properlyInitialized = false;
        private boolean recoveryInProgress = false;
        private boolean restartInProgress = false;

        public boolean isRestartInProgress() {
            return restartInProgress;
        }

        public void setRestartInProgress() {
            Log.d("flag", "PersistApplication.setRestartInProgress()");

            this.restartInProgress = true;
        }

        public void unsetRestartInProgress() {
            Log.d("flag", "PersistApplication.unsetRestartInProgress()");

            this.restartInProgress = false;
        }

        public boolean isRecoveryInProgress() {
            return recoveryInProgress;
        }

        public void setRecoveryInProgress() {
            Log.d("flag", "PersistApplication.setRecoveryInProgress()");

            this.recoveryInProgress = true;
        }

        public void unsetRecoveryInProgress() {
            Log.d("flag", "PersistApplication.unsetRecoveryInProgress()");

            this.recoveryInProgress = false;
        }

        public boolean isProperlyInitialized() {
            return properlyInitialized;
        }

        public void setProperlyInitialized() {
            Log.d("flag", "PersistApplication.setProperlyInitialized()");

            this.properlyInitialized = true;
        }
    }
}
