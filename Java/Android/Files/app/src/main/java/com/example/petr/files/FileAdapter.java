package com.example.petr.files;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by petr on 3/19/17.
 */

public class FileAdapter extends BaseAdapter {

    private DirectoryChangeListener directoryChangeListener = null;
    private FileDataSource dataSource = null;

    public FileAdapter(FileDataSource source) {
        dataSource = source;
    }

    private static void setVisibility(View view, boolean show) {
        view.setVisibility(show
                ? View.VISIBLE
                : View.GONE);
    }

    public void setDirectoryChangeListener(DirectoryChangeListener listener) {
        directoryChangeListener = listener;
    }

    @Override
    public int getCount() {
        return dataSource.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.file_item, parent, false);
        }

        FileItem fileItem = dataSource.get(position);
        final ImageView imageViewItemFolder = (ImageView) convertView
                .findViewById(R.id.imageViewItemFolder);
        setVisibility(imageViewItemFolder, fileItem.isDirectory());
        final ImageView imageViewItemFile = (ImageView) convertView
                .findViewById(R.id.imageViewItemFile);
        setVisibility(imageViewItemFile, !fileItem.isDirectory());
        final TextView textViewItemName = (TextView) convertView
                .findViewById(R.id.textViewItemName);
        textViewItemName.setText(fileItem.getFileName());

        return convertView;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Object getItem(int position) {
        return dataSource.get(position);
    }

    public void changeDirUp() {
        changeDir(FileObject.getParentDirectory(dataSource.getCurrentPath()));
    }

    public void changeDir(String directory) {
        if (directory != null && !directory.isEmpty()) {
            if (FileObject.pathExists(directory)) {
                dataSource.setCurrentPath(directory);

                notifyDataSetChanged();

                if (directoryChangeListener != null) {
                    directoryChangeListener.onChangeDirectory(dataSource.getCurrentPath());
                }
            }
        }
    }

    public void changeSubDirectory(String subDirectory) {
        changeDir(FileObject.addPath(
                dataSource.getCurrentPath(), subDirectory));
    }

    public void openFile(String fileName, Activity activity) {
        FileObject.openFile(FileObject.addPath(
                dataSource.getCurrentPath(), fileName), activity);
    }

    public boolean deleteFile(String fileName) {
        boolean retVal = FileObject.deleteFile(FileObject.addPath(
                dataSource.getCurrentPath(), fileName));
        if (retVal) {
            dataSource.refresh();
            notifyDataSetChanged();
        }

        return retVal;
    }

    public void refresh() {
        dataSource.refresh();
        notifyDataSetChanged();
    }

    public int getFilePosition(String fileName) {
        int size = dataSource.size();
        for (int i = 0; i < size; i++) {
            FileItem item = dataSource.get(i);
            if (!item.isDirectory() &&
                    item.getFileName().equals(fileName)) {
                Log.d("find", "found at index: " + i);

                return i;
            }
        }

        return -1;
    }

    public interface DirectoryChangeListener {
        void onChangeDirectory(String directory);
    }
}
