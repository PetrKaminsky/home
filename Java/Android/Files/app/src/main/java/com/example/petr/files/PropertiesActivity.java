package com.example.petr.files;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.CheckBox;
import android.widget.TextView;

import java.io.File;
import java.util.Locale;

public class PropertiesActivity extends Activity {

    public static final String PARAM_EXTRA_NAME = "FileName";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getActionBar().setDisplayHomeAsUpEnabled(true);

        setContentView(R.layout.activity_properties);
        final TextView textViewName = (TextView) findViewById(R.id.textViewName);
        final TextView textViewParent = (TextView) findViewById(R.id.textViewParent);
        final CheckBox checkBoxIsDirectory = (CheckBox) findViewById(R.id.checkBoxIsDirectory);
        final CheckBox checkBoxIsFile = (CheckBox) findViewById(R.id.checkBoxIsFile);
        final TextView textViewLength = (TextView) findViewById(R.id.textViewLength);
        final CheckBox checkBoxCanRead = (CheckBox) findViewById(R.id.checkBoxCanRead);
        final CheckBox checkBoxCanWrite = (CheckBox) findViewById(R.id.checkBoxCanWrite);
        final CheckBox checkBoxCanExecute = (CheckBox) findViewById(R.id.checkBoxCanExecute);

        Intent intent = getIntent();
        if (intent != null) {
            String fileName = intent.getStringExtra(PARAM_EXTRA_NAME);
            if (fileName != null && !fileName.isEmpty()) {
                File file = new File(fileName);
                if (file.exists()) {
                    textViewName.setText(file.getName());
                    textViewParent.setText(file.getParent());
                    checkBoxIsDirectory.setChecked(file.isDirectory());
                    checkBoxIsFile.setChecked(file.isFile());
                    textViewLength.setText(String.format(Locale.US, "%d", file.length()));
                    checkBoxCanRead.setChecked(file.canRead());
                    checkBoxCanWrite.setChecked(file.canWrite());
                    checkBoxCanExecute.setChecked(file.canExecute());
                }
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
