package com.example.petr.files;

import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

/**
 * Created by petr.kaminsky on 27.3.2017.
 */

public class FindAdapter extends BaseAdapter {

    private FindListener listener = null;
    private FindDataSource dataSource = null;
    private AsyncFind asyncFind = null;

    public void setDataSource(FindDataSource data) {
        dataSource = data;
    }

    public void setFindListener(FindListener l) {
        listener = l;
    }

    @Override
    public int getCount() {
        if (dataSource != null) {
            return dataSource.size();
        }

        return 0;
    }

    @Override
    public Object getItem(int position) {
        if (dataSource != null) {
            return dataSource.get(position);
        }

        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (dataSource != null) {
            if (convertView == null) {
                convertView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.find_item, parent, false);
            }

            String fileName = dataSource.get(position);
            final TextView textViewFindFile = (TextView) convertView
                    .findViewById(R.id.textViewFindFile);
            textViewFindFile.setText(fileName);

            return convertView;
        }

        return null;
    }

    public void startFindFiles(String mask) {
        if (dataSource != null) {
            dataSource.empty();

            notifyDataSetChanged();

            if (asyncFind == null) {
                asyncFind = new AsyncFind();
                asyncFind.execute(mask);
            }
        }
    }

    private void internalFinish() {
        notifyDataSetChanged();

        if (listener != null) {
            listener.onFinish();
        }

        asyncFind = null;
    }

    public interface FindListener {
        void onFinish();
    }

    private class AsyncFind extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... params) {
            dataSource.setMask(params[0]);

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            internalFinish();
        }
    }
}
