package com.example.petr.files;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

public class SelectFolderActivity extends Activity
        implements FileListFragment.FileDataSourceProvider {

    public static final String RESULT_EXTRA_NAME = "SelectedFolder";
    private FileListFragment fileListFragment = null;
    private Button buttonConfirm = null;
    private View.OnClickListener onButtonConfirm =
            new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String folderPath = SelectFolderActivityData.getInstance()
                            .getDataSource().getCurrentPath();
                    Intent intent = new Intent();
                    intent.putExtra(RESULT_EXTRA_NAME, folderPath);
                    setResult(RESULT_OK, intent);
                    finish();
                }
            };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_select_folder);
        fileListFragment = (FileListFragment) getFragmentManager().findFragmentById(
                R.id.fragmentFileList);
        buttonConfirm = (Button) findViewById(R.id.buttonConfirm);
        buttonConfirm.setOnClickListener(onButtonConfirm);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return fileListFragment.onCreateOptionsMenu(getMenuInflater(), menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (fileListFragment.onOptionsItemSelected(item)) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public FileDataSource getFileDataSource() {
        return SelectFolderActivityData.getInstance().getDataSource();
    }

    private static class SelectFolderActivityData {
        private static final SelectFolderActivityData instance = new SelectFolderActivityData();
        private FileDataSource dataSource = FileDataSource.createInstance(
                FileDataSource.FileListMode.FOLDERS);

        private SelectFolderActivityData() {
        }

        public static SelectFolderActivityData getInstance() {
            return instance;
        }

        public FileDataSource getDataSource() {
            return dataSource;
        }
    }
}
