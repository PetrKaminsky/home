package com.example.petr.files;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;

public class FileListFragment extends Fragment {

    private TextView textViewCurrentDirectory = null;
    private ListView listViewItems = null;
    private EditText editTextPath = null;
    private MenuItem actionEnterMenuItem = null;
    private FileAdapter fileAdapter = null;
    private OnFileItemClickListener onFileItemClickListener = null;
    private OnFileItemLongClickListener onFileItemLongClickListener = null;
    private PopupMenuListener popupMenuListener = null;
    private AdapterView.OnItemClickListener onItemClick =
            new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    FileItem fileItem = (FileItem) fileAdapter.getItem(position);

                    if (onFileItemClickListener != null) {
                        if (onFileItemClickListener.onFileItemClick(fileItem)) {
                            return;
                        }
                    }

                    String fileName = fileItem.getFileName();
                    if (fileItem.isDirectory()) {
                        changeSubDirectory(fileName);
                    } else {
                        openFileItem(fileName);
                    }
                }
            };
    private FileAdapter.DirectoryChangeListener onDirectoryChange =
            new FileAdapter.DirectoryChangeListener() {
                @Override
                public void onChangeDirectory(String directory) {
                    refreshPath();
                }
            };
    private AdapterView.OnItemLongClickListener onItemLongClick =
            new AdapterView.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                    FileItem fileItem = (FileItem) fileAdapter.getItem(position);

                    if (onFileItemLongClickListener != null) {
                        if (onFileItemLongClickListener.onFileItemLongClick(fileItem)) {
                            return true;
                        }
                    }

                    showPopupMenu(view, fileItem);

                    return true;
                }
            };
    private View.OnClickListener onButtonGoPathClick =
            new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String directory = editTextPath.getText().toString();
                    if (!directory.isEmpty()) {
                        actionEnterMenuItem.collapseActionView();

                        changeDirectory(directory);
                    }
                }
            };

    public void setPopupMenuListener(PopupMenuListener popupMenuListener) {
        this.popupMenuListener = popupMenuListener;
    }

    public void setOnFileItemLongClickListener(OnFileItemLongClickListener listener) {
        onFileItemLongClickListener = listener;
    }

    public void setOnFileItemClickListener(OnFileItemClickListener listener) {
        onFileItemClickListener = listener;
    }

    private void showPopupMenu(View view, final FileItem fileItem) {
        final String fileName = fileItem.getFileName();
        boolean isDirectory = fileItem.isDirectory();
        PopupMenu popupMenu = new PopupMenu(getActivity(), view);
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if (popupMenuListener != null) {
                    if (popupMenuListener.onMenuItemClick(item, fileItem)) {
                        return true;
                    }
                }

                int id = item.getItemId();
                switch (id) {
                    case R.id.action_file_open:
                        openFileItem(fileName);
                        return true;

                    case R.id.action_file_delete:
                        deleteFileItem(fileName);
                        return true;

                    case R.id.action_directory_change:
                        changeSubDirectory(fileName);
                        return true;

                    case R.id.action_properties:
                        showProperties(fileName);
                        return true;
                }

                return false;
            }
        });

        Menu menu = popupMenu.getMenu();
        MenuInflater inflater = popupMenu.getMenuInflater();

        if (popupMenuListener != null) {
            if (popupMenuListener.includeDefaultItems()) {
                popupMenu.getMenuInflater().inflate(R.menu.popup_menu_fragment, menu);

                menu.findItem(R.id.action_file_open).setVisible(!isDirectory);
                menu.findItem(R.id.action_file_delete).setVisible(!isDirectory);
                menu.findItem(R.id.action_directory_change).setVisible(isDirectory);
            }

            popupMenuListener.inflateCustomMenu(inflater, menu, fileItem);
        }

        popupMenu.show();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        fileAdapter = new FileAdapter(getDataSource());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.file_list_fragment, container, false);
        textViewCurrentDirectory = (TextView) view.findViewById(R.id.textViewCurrentDirectory);
        listViewItems = (ListView) view.findViewById(R.id.listViewItems);
        listViewItems.setAdapter(fileAdapter);
        listViewItems.setOnItemClickListener(onItemClick);
        listViewItems.setOnItemLongClickListener(onItemLongClick);
        fileAdapter.setDirectoryChangeListener(onDirectoryChange);

        return view;
    }

    public boolean onCreateOptionsMenu(MenuInflater inflater, Menu menu) {
        inflater.inflate(R.menu.menu_fragment, menu);

        actionEnterMenuItem = menu.findItem(R.id.action_enter);
        View actionView = actionEnterMenuItem.getActionView();
        editTextPath = (EditText) actionView.findViewById(R.id.editTextPath);
        actionView.findViewById(R.id.buttonGoPath).setOnClickListener(onButtonGoPathClick);

        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_root:
                changeDirectoryRoot();
                return true;

            case R.id.action_up:
                changeDirectoryUp();
                return true;

            case R.id.action_sec_storage:
                changeDirectorySecondaryStorage();
                return true;

            case R.id.action_env_root:
                changeDirectoryEnvironmentRoot();
                return true;

            case R.id.action_env_data:
                changeDirectoryEnvironmentData();
                return true;

            case R.id.action_env_download_cache:
                changeDirectoryDownloadCache();
                return true;

            case R.id.action_ext_downloads:
                changeDirectoryExternalDownloads();
                return true;

            case R.id.action_ext_storage:
                changeDirectoryExternalStorage();
                return true;

            case R.id.action_ext_music:
                changeDirectoryExternalMusic();
                return true;

            case R.id.action_refresh:
                refresh();
                return true;

            case R.id.action_ext_dcim:
                changeDirectoryExternalDcim();
                return true;

            case R.id.action_ext_pictures:
                changeDirectoryExternalPictures();
                return true;

            case R.id.action_app_cache:
                changeDirectoryApplicationCache();
                return true;

            case R.id.action_app_pictures:
                changeDirectoryApplicationPictures();
                return true;

            case R.id.action_app_files:
                changeDirectoryApplicationFiles();
                return true;
        }

        return false;
    }

    public void changeDirectoryRoot() {
        fileAdapter.changeDir("/");
    }

    public void changeDirectoryUp() {
        fileAdapter.changeDirUp();
    }

    public void changeDirectorySecondaryStorage() {
        fileAdapter.changeDir(System.getenv("SECONDARY_STORAGE"));
    }

    public void changeDirectoryEnvironmentRoot() {
        fileAdapter.changeDir(Environment.getRootDirectory().getAbsolutePath());
    }

    public void changeDirectoryEnvironmentData() {
        fileAdapter.changeDir(Environment.getDataDirectory().getAbsolutePath());
    }

    public void changeDirectoryDownloadCache() {
        fileAdapter.changeDir(Environment.getDownloadCacheDirectory().getAbsolutePath());
    }

    public void changeSubDirectory(String subDirectory) {
        fileAdapter.changeSubDirectory(subDirectory);
    }

    public void changeDirectoryExternalDownloads() {
        fileAdapter.changeDir(Environment
                .getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
                .getAbsolutePath());
    }

    public void changeDirectoryExternalStorage() {
        fileAdapter.changeDir(Environment
                .getExternalStorageDirectory()
                .getAbsolutePath());
    }

    public void changeDirectoryExternalMusic() {
        fileAdapter.changeDir(Environment
                .getExternalStoragePublicDirectory(Environment.DIRECTORY_MUSIC)
                .getAbsolutePath());
    }

    public void changeDirectoryExternalDcim() {
        fileAdapter.changeDir(Environment
                .getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM)
                .getAbsolutePath());
    }

    public void changeDirectoryExternalPictures() {
        fileAdapter.changeDir(Environment
                .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
                .getAbsolutePath());
    }

    public void changeDirectoryApplicationCache() {
        File path = getActivity().getExternalCacheDir();
        if (path != null) {
            fileAdapter.changeDir(path.getAbsolutePath());
        }
    }

    public void changeDirectoryApplicationPictures() {
        File path = getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        if (path != null) {
            fileAdapter.changeDir(path.getAbsolutePath());
        }
    }

    public void changeDirectoryApplicationFiles() {
        fileAdapter.changeDir(getActivity()
                .getFilesDir().getAbsolutePath());
    }

    public void changeDirectory(String directory) {
        fileAdapter.changeDir(directory);
    }

    public void openFileItem(String filename) {
        fileAdapter.openFile(filename, getActivity());
    }

    public void deleteFileItem(String filename) {
        if (fileAdapter.deleteFile(filename)) {
            Toast.makeText(getActivity(), R.string.msg_file_deleted, Toast.LENGTH_SHORT)
                    .show();
        }
    }

    public void refresh() {
        fileAdapter.refresh();
    }

    private void refreshPath() {
        textViewCurrentDirectory.setText(getDataSource().getCurrentPath());
    }

    public void locateFile(String fileName) {
        String directory = FileObject.getParentDirectory(fileName);
        fileAdapter.changeDir(directory);
        int position = fileAdapter.getFilePosition(
                FileObject.getRelativeFileName(fileName));
        listViewItems.setSelection(position);
    }

    public void showProperties(String fileName) {
        Intent intent = new Intent(getActivity(), PropertiesActivity.class);
        fileName = FileObject.addPath(getDataSource().getCurrentPath(), fileName);
        intent.putExtra(PropertiesActivity.PARAM_EXTRA_NAME, fileName);
        getActivity().startActivity(intent);
    }

    @Override
    public void onResume() {
        super.onResume();

        refreshPath();
    }

    private FileDataSource getDataSource() {
        return ((FileDataSourceProvider) getActivity()).getFileDataSource();
    }

    public interface FileDataSourceProvider {
        FileDataSource getFileDataSource();
    }

    public interface OnFileItemClickListener {
        boolean onFileItemClick(FileItem fileItem);
    }

    public interface OnFileItemLongClickListener {
        boolean onFileItemLongClick(FileItem fileItem);
    }

    public interface PopupMenuListener {
        boolean onMenuItemClick(MenuItem item, FileItem fileItem);

        boolean includeDefaultItems();

        void inflateCustomMenu(MenuInflater inflater, Menu menu, FileItem fileItem);
    }
}
