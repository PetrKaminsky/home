package com.example.petr.files;

import java.util.ArrayList;

/**
 * Created by petr.kaminsky on 27.3.2017.
 */

public class FindDataSource extends ArrayList<String> {
    private FindDataSource() {
    }

    public static FindDataSource createInstance() {
        return new FindDataSource();
    }

    public void empty() {
        clear();
    }

    public void setMask(String mask) {
        addAll(FileObject.getFilesRecursively("/", mask, 6));
    }
}
