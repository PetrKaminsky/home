package com.example.petr.files;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.webkit.MimeTypeMap;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by petr on 3/20/17.
 */

public class FileObject {
    public static String getParentDirectory(String directory) {
        return new File(directory).getParent();
    }

    public static List<FileItem> getDirectoryContents(FileDataSource.FileListMode mode,
                                                      String directory) {
        ArrayList<FileItem> itemsFile = new ArrayList<>();
        ArrayList<FileItem> itemsDirectory = new ArrayList<>();
        File path = new File(directory);
        File[] items = path.listFiles();
        if (items != null) {
            for (File item : items) {
                if (item.isDirectory()) {
                    switch (mode) {
                        case FOLDERS:
                        case BOTH:
                            itemsDirectory.add(new FileItem(item.getName(), true));
                            break;
                    }
                } else {
                    switch (mode) {
                        case FILES:
                        case BOTH:
                            itemsFile.add(new FileItem(item.getName(), false));
                            break;
                    }
                }
            }
        }

        ArrayList<FileItem> result = new ArrayList<>();
        result.addAll(itemsDirectory);
        result.addAll(itemsFile);

        return result;
    }

    public static String addPath(String directory, String path) {
        return new File(directory, path).getAbsolutePath();
    }

    public static void openFile(File file, Activity activity) {
        openFile(file.getAbsolutePath(), activity);
    }

    public static void openFile(String fileName, Activity activity) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        Uri uri = Uri.fromFile(new File(fileName));
        String mime = getMimeType(fileName);
        intent.setDataAndType(uri, mime);

        activity.startActivity(intent);
    }

    private static String getMimeType(String fileName) {
        String retVal = null;
        int i = fileName.lastIndexOf('.');
        if (i != -1) {
            String extension = fileName.substring(i + 1);
            retVal = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
        }

        return retVal;
    }

    public static boolean deleteFile(String fileName) {
        return new File(fileName).delete();
    }

    public static List<String> getFilesRecursively(
            String directory, String fileMask, int maxDepth) {
        ArrayList<String> result = new ArrayList<>();
        File path = new File(directory);
        File[] items = path.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                if (dir.getAbsolutePath().equals("/") &&
                        name.equals("proc")) {
                    return false;
                }

                return true;
            }
        });
        if (items != null) {
            for (File item : items) {
                String fullFileName = new File(directory,
                        item.getName()).getAbsolutePath();
                if (item.isDirectory()) {
                    if (maxDepth > 0) {
                        result.addAll(getFilesRecursively(fullFileName, fileMask, maxDepth - 1));
                    }
                } else {
                    if (item.getName().matches(fileMask)) {
                        Log.d("find", fullFileName);

                        result.add(fullFileName);
                    }
                }
            }
        }

        return result;
    }

    public static String getRelativeFileName(String fullFileName) {
        return new File(fullFileName).getName();
    }

    public static void copyFile(String srcPath, String dstPath) throws IOException {
        copyFile(new File(srcPath), new File(dstPath));
    }

    public static void copyFile(File src, File dst) throws IOException {
        FileInputStream inStream = null;
        FileOutputStream outStream = null;
        try {
            inStream = new FileInputStream(src);
            outStream = new FileOutputStream(dst);
            FileChannel inChannel = inStream.getChannel();
            FileChannel outChannel = outStream.getChannel();
            long position = 0;
            long count = inChannel.size();
            while (count > 0) {
                long transferred = inChannel.transferTo(position, count, outChannel);
                position += transferred;
                count -= transferred;
            }
        } finally {
            if (inStream != null) {
                inStream.close();
            }
            if (outStream != null) {
                outStream.close();
            }
        }
    }

    public static boolean pathExists(String path) {
        return new File(path).exists();
    }
}
