package com.example.petr.files;

import java.util.ArrayList;

/**
 * Created by petr on 3/19/17.
 */

public class FileDataSource extends ArrayList<FileItem> {

    private String currentPath = null;
    private FileListMode fileListMode;

    private FileDataSource(FileListMode mode) {
        fileListMode = mode;
        setCurrentPath(System.getProperty("user.dir"));
    }

    public static FileDataSource createInstance(FileListMode mode) {
        return new FileDataSource(mode);
    }

    public String getCurrentPath() {
        return currentPath;
    }

    public void setCurrentPath(String path) {
        currentPath = path;
        refresh();
    }

    public void refresh() {
        clear();
        addAll(FileObject.getDirectoryContents(fileListMode, currentPath));
    }

    public enum FileListMode {
        FILES,
        FOLDERS,
        BOTH
    }

}
