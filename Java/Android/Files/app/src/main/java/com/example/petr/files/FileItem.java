package com.example.petr.files;

/**
 * Created by petr on 3/19/17.
 */

public class FileItem {
    private String fileName = null;
    private boolean directory = false;

    public FileItem(String fileName, boolean directory) {
        this.fileName = fileName;
        this.directory = directory;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public boolean isDirectory() {
        return directory;
    }

    public void setDirectory(boolean directory) {
        this.directory = directory;
    }
}
