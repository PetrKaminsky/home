package com.example.petr.files;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import java.io.File;

public class MainActivity extends Activity
        implements FileListFragment.FileDataSourceProvider {

    private static final int FIND_FILE_REQUEST_CODE = 1;
    private static final int SELECT_FOLDER_REQUEST_CODE = 2;
    private FileListFragment fileListFragment = null;
    private FileListFragment.OnFileItemClickListener onFileItemClickListener =
            new FileListFragment.OnFileItemClickListener() {
                @Override
                public boolean onFileItemClick(FileItem fileItem) {
                    return false;
                }
            };
    private FileListFragment.OnFileItemLongClickListener onFileItemLongClickListener =
            new FileListFragment.OnFileItemLongClickListener() {
                @Override
                public boolean onFileItemLongClick(FileItem fileItem) {
                    return false;
                }
            };
    private FileListFragment.PopupMenuListener popupMenuListener =
            new FileListFragment.PopupMenuListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item, FileItem fileItem) {
                    switch (item.getItemId()) {
                        case R.id.action_file_copy:
                            if (!fileItem.isDirectory()) {
                                MainActivityData.getInstance().setFileNameToCopy(
                                        fileItem.getFileName());
                                selectFolder();
                            }

                            return true;
                    }

                    return false;
                }

                @Override
                public boolean includeDefaultItems() {
                    return true;
                }

                @Override
                public void inflateCustomMenu(MenuInflater inflater, Menu menu, FileItem fileItem) {
                    inflater.inflate(R.menu.popup_menu_main, menu);

                    menu.findItem(R.id.action_file_copy).setVisible(!fileItem.isDirectory());
                }
            };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        fileListFragment = (FileListFragment) getFragmentManager().findFragmentById(
                R.id.fragmentFileList);
        fileListFragment.setOnFileItemClickListener(onFileItemClickListener);
        fileListFragment.setOnFileItemLongClickListener(onFileItemLongClickListener);
        fileListFragment.setPopupMenuListener(popupMenuListener);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        fileListFragment.onCreateOptionsMenu(getMenuInflater(), menu);
        getMenuInflater().inflate(R.menu.menu_main, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (fileListFragment.onOptionsItemSelected(item)) {
            return true;
        }

        switch (item.getItemId()) {
            case R.id.action_find_file:
                findFile();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void findFile() {
        Intent intent = new Intent(this, FindActivity.class);
        startActivityForResult(intent, FIND_FILE_REQUEST_CODE);
    }

    public void selectFolder() {
        Intent intent = new Intent(this, SelectFolderActivity.class);
        startActivityForResult(intent, SELECT_FOLDER_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case FIND_FILE_REQUEST_CODE:
                    String selectedFileName = data.getStringExtra(FindActivity.RESULT_EXTRA_NAME);
                    fileListFragment.locateFile(selectedFileName);
                    return;

                case SELECT_FOLDER_REQUEST_CODE:
                    String sourceFileName = MainActivityData.getInstance().getFileNameToCopy();
                    String destinationFolderPath = data.getStringExtra(
                            SelectFolderActivity.RESULT_EXTRA_NAME);
                    if (copyFile(sourceFileName, destinationFolderPath)) {
                        fileListFragment.locateFile(FileObject.addPath(
                                destinationFolderPath, sourceFileName));
                        Toast.makeText(this, R.string.msg_file_copied, Toast.LENGTH_SHORT).show();
                    }
                    MainActivityData.getInstance().setFileNameToCopy(null);
                    return;
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    private boolean copyFile(String fileName, String destinationFolderPath) {
        String sourceFolderPath = getFileDataSource().getCurrentPath();
        File sourceFile = new File(sourceFolderPath, fileName);
        File destinationFile = new File(destinationFolderPath, fileName);
        if (sourceFile.compareTo(destinationFile) == 0) {
            return false;
        }
        if (destinationFile.exists()) {
            Toast.makeText(this, R.string.msg_file_exists, Toast.LENGTH_SHORT).show();

            return false;
        }
        try {
            FileObject.copyFile(sourceFile, destinationFile);
        } catch (Exception e) {
            Log.e("io", "copyFile()", e);

            return false;
        }

        return true;
    }

    @Override
    public FileDataSource getFileDataSource() {
        return MainActivityData.getInstance().getDataSource();
    }

    private static class MainActivityData {
        private static final MainActivityData instance = new MainActivityData();
        private FileDataSource dataSource = FileDataSource.createInstance(
                FileDataSource.FileListMode.BOTH);
        private String fileNameToCopy = null;

        private MainActivityData() {
        }

        public static MainActivityData getInstance() {
            return instance;
        }

        public String getFileNameToCopy() {
            return fileNameToCopy;
        }

        public void setFileNameToCopy(String fileName) {
            fileNameToCopy = fileName;
        }

        public FileDataSource getDataSource() {
            return dataSource;
        }
    }
}
