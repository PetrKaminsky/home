package com.example.petr.files;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;

public class FindActivity extends Activity {

    public static final String RESULT_EXTRA_NAME = "SelectedFileName";
    private EditText editTextMask = null;
    private Button buttonFind = null;
    private FindAdapter adapter = null;
    private ProgressBar progressBar = null;
    private View.OnClickListener onClickFind = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String mask = editTextMask.getText().toString();
            if (!mask.isEmpty()) {
                if (!FindActivityData.getInstance().isWorking()) {
                    FindActivityData.getInstance().setWorking(true);
                    updateProgressBar();
                    updateButton();

                    adapter.startFindFiles(mask);
                }
            }
        }
    };
    private FindAdapter.FindListener onFind = new FindAdapter.FindListener() {
        @Override
        public void onFinish() {
            FindActivityData.getInstance().setWorking(false);
            updateProgressBar();
            updateButton();
        }
    };
    private AdapterView.OnItemLongClickListener onlongClick =
            new AdapterView.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                    String fileName = (String) adapter.getItem(position);
                    Intent intent = new Intent();
                    intent.putExtra(RESULT_EXTRA_NAME, fileName);
                    setResult(RESULT_OK, intent);
                    finish();

                    return true;
                }
            };

    private void updateProgressBar() {
        progressBar.setVisibility(FindActivityData.getInstance().isWorking()
                ? View.VISIBLE
                : View.GONE);
    }

    private void updateButton() {
        buttonFind.setEnabled(!FindActivityData.getInstance().isWorking());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_find);
        editTextMask = (EditText) findViewById(R.id.editTextMask);
        buttonFind = (Button) findViewById(R.id.buttonFind);
        buttonFind.setOnClickListener(onClickFind);
        adapter = new FindAdapter();
        adapter.setFindListener(onFind);
        adapter.setDataSource(FindActivityData.getInstance().getDataSource());
        final ListView listViewFiles = (ListView) findViewById(R.id.listViewFiles);
        listViewFiles.setAdapter(adapter);
        listViewFiles.setOnItemLongClickListener(onlongClick);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
    }

    @Override
    protected void onResume() {
        super.onResume();

        updateProgressBar();
        updateButton();
    }

    private static class FindActivityData {
        private static final FindActivityData instance = new FindActivityData();
        private FindDataSource dataSource = FindDataSource.createInstance();
        private boolean working = false;

        private FindActivityData() {
        }

        public static FindActivityData getInstance() {
            return instance;
        }

        public boolean isWorking() {
            return working;
        }

        public void setWorking(boolean w) {
            working = w;
        }

        public FindDataSource getDataSource() {
            return dataSource;
        }
    }
}
