package com.example.petrkaminsky.servicetest;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

import java.util.Random;

/**
 * Created by petr.kaminsky on 2.7.2018.
 */

public class CustomTask extends AsyncTask<Void, Void, Void> {

    private Context context = null;
    private int request = 0;

    public CustomTask(Context ctx, int r) {
        context = ctx;
        request = r;
    }

    @Override
    protected Void doInBackground(Void... params) {
        Log.d("trace", "Request start: " + request);

        long timeoutMsec = (long) ((1 + new Random().nextDouble()) * 10000);
        long startTime = System.currentTimeMillis();
        try {
            Thread.sleep(timeoutMsec);
        } catch (InterruptedException ex) {
            Log.e("error", "", ex);
        }
        double duration = (double) (System.currentTimeMillis() - startTime) / 1000;

        Log.d("trace", "Request stop: " + request + ", duration: " + Double.toString(duration) + " sec");

        broadcastResult(request, duration);

        return null;
    }

    private void broadcastResult(int r, double d) {
        Intent intent = new Intent(MainActivity.CUSTOM_INTENT);
        intent.putExtra(MainActivity.REQUEST_NUMBER_EXTRA_NAME, r);
        intent.putExtra(MainActivity.REQUEST_DURATION_EXTRA_NAME, d);
        context.sendBroadcast(intent);
    }
}
