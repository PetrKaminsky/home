package com.example.petrkaminsky.servicetest;

import android.app.Application;
import android.content.Context;

/**
 * Created by petr.kaminsky on 2.7.2018.
 */

public class CustomApplication extends Application {

    private static Context context = null;

    public static Context getContext() {
        return context;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        context = getApplicationContext();
    }
}
