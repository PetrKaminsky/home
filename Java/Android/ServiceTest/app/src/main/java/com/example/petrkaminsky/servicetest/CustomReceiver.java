package com.example.petrkaminsky.servicetest;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Created by petr.kaminsky on 2.7.2018.
 */

public class CustomReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("trace", "CustomReceiver.onReceive()");

        if (intent == null) {
            return;
        }
        if (!intent.getAction().equals(MainActivity.CUSTOM_INTENT)) {
            return;
        }
        if (!intent.hasExtra(MainActivity.REQUEST_NUMBER_EXTRA_NAME) ||
                !intent.hasExtra(MainActivity.REQUEST_DURATION_EXTRA_NAME)) {
            return;
        }

        int request = intent.getIntExtra(MainActivity.REQUEST_NUMBER_EXTRA_NAME, 0);
        double duration = intent.getDoubleExtra(MainActivity.REQUEST_DURATION_EXTRA_NAME, 0);

        Log.d("trace", "CustomReceiver, request: " + request + ", duration: " + Double.toString(duration) + " sec");
    }
}
