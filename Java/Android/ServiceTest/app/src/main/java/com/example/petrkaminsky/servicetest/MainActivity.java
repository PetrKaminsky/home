package com.example.petrkaminsky.servicetest;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    public static final String CUSTOM_INTENT = "MainActivity.CustomReceiver";
    public static final String REQUEST_NUMBER_EXTRA_NAME = "RequestNumber";
    public static final String REQUEST_DURATION_EXTRA_NAME = "RequestDuration";
    private CustomReceiver receiver = null;

    private View.OnClickListener onButtonStartClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            startService();
        }
    };
    private View.OnClickListener onButtonStopClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            stopService();
        }
    };
    private View.OnClickListener onButtonRequestClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            startRequest();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        findViewById(R.id.buttonStart).setOnClickListener(onButtonStartClickListener);
        findViewById(R.id.buttonStop).setOnClickListener(onButtonStopClickListener);
        findViewById(R.id.buttonRequest).setOnClickListener(onButtonRequestClickListener);
        receiver = new CustomReceiver();
    }

    @Override
    protected void onResume() {
        super.onResume();

        registerReceiver();
    }

    @Override
    protected void onPause() {
        super.onPause();

        unregisterReceiver();
    }

    private void startService() {
        Intent intent = new Intent(this, CustomService.class);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForegroundService(intent);
        } else {
            startService(intent);
        }
    }

    private void stopService() {
        Intent intent = new Intent(this, CustomService.class);
        stopService(intent);
    }

    private void registerReceiver() {
        registerReceiver(receiver, new IntentFilter(CUSTOM_INTENT));
    }

    private void unregisterReceiver() {
        unregisterReceiver(receiver);
    }

    private void startRequest() {
        new CustomServiceConnection().startRequest();
    }
}
