package com.example.petrkaminsky.servicetest;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;

import static android.app.NotificationManager.IMPORTANCE_DEFAULT;

public class CustomService extends Service implements ServiceInterface {

    private static final int NOTIFICATION_ID = 1;
    private static int requestCounter = 1;
    private Binder binder = new ServiceBinder();

    @Override
    public void onCreate() {
        super.onCreate();

        Log.d("trace", "CustomService.onCreate()");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        Log.d("trace", "CustomService.onDestroy()");

        stopForeground();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("trace", "CustomService.onStartCommand()");

        startForeground();

        return Service.START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.d("trace", "CustomService.onBind()");

        return binder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Log.d("trace", "CustomService.onUnbind()");

        return super.onUnbind(intent);
    }

    public void startRequest() {
        int request = requestCounter;
        requestCounter++;
        startRequest(request);
    }

    private void startRequest(int request) {
        CustomTask task = new CustomTask(CustomApplication.getContext(), request);
        task.execute();
    }

    private void startForeground() {
        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);
        Notification.Builder notificationBuilder = null;
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            notificationBuilder = new Notification.Builder(this);
        } else {
            ensureNotificationChannel(getNotificationChannelId());
            notificationBuilder = new Notification.Builder(this, getNotificationChannelId());
        }
        notificationBuilder.setContentTitle(getString(R.string.txt_service_title));
        notificationBuilder.setContentText(getString(R.string.txt_service_text));
        notificationBuilder.setContentIntent(pendingIntent);
        notificationBuilder.setSmallIcon(R.mipmap.ic_launcher);
        Notification notification = notificationBuilder.build();
        startForeground(NOTIFICATION_ID, notification);
    }

    private void stopForeground() {
        stopForeground(true);
    }

    @TargetApi(26)
    private void ensureNotificationChannel(String channelId) {
        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        if (notificationManager != null) {
            NotificationChannel notificationChannel = notificationManager.getNotificationChannel(channelId);
            if (notificationChannel == null) {
                String name = getString(R.string.app_name);
                notificationChannel = new NotificationChannel(channelId, name, IMPORTANCE_DEFAULT);
                notificationManager.createNotificationChannel(notificationChannel);
            }
        }
    }

    private String getNotificationChannelId() {
        return getPackageName();
    }

    public class ServiceBinder extends Binder {

        public ServiceInterface getService() {
            return CustomService.this;
        }
    }
}
