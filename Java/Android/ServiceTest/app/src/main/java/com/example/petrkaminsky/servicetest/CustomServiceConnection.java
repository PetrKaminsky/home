package com.example.petrkaminsky.servicetest;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.util.Log;

import static android.content.Context.BIND_AUTO_CREATE;

/**
 * Created by petr.kaminsky on 2.7.2018.
 */

public class CustomServiceConnection implements ServiceConnection {

    private boolean shouldUnbind = false;
    private ServiceInterface service = null;
    private Runnable serviceRunnable = null;

    @Override
    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        Log.d("trace", "CustomServiceConnection.onServiceConnected()");

        service = ((CustomService.ServiceBinder) iBinder).getService();
        serviceRunnable.run();
        unbindService();
    }

    @Override
    public void onServiceDisconnected(ComponentName componentName) {
        Log.d("trace", "CustomServiceConnection.onServiceDisconnected()");

        service = null;
    }

    private void bindService() {
        Context context = CustomApplication.getContext();
        Intent intent = new Intent(context, CustomService.class);
        if (context.bindService(intent, this, BIND_AUTO_CREATE)) {
            shouldUnbind = true;
        }
    }

    private void unbindService() {
        Context context = CustomApplication.getContext();
        if (shouldUnbind) {
            context.unbindService(this);
            shouldUnbind = false;
        }
        service = null;
        serviceRunnable = null;
    }

    public void startRequest() {
        serviceRunnable = new Runnable() {
            @Override
            public void run() {
                service.startRequest();
            }
        };
        bindService();
    }
}
