package com.example.petr.fragcolors;

import android.app.FragmentManager;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.app.Activity;
import android.widget.TextView;

public class MainActivity extends Activity {

    private static final int MIN_COLOR_VALUE = 0;
    private static final int MAX_COLOR_VALUE = 255;
    private TextView textViewColor = null;
    private ValueFragment fragmentRed = null;
    private ValueFragment fragmentGreen = null;
    private ValueFragment fragmentBlue = null;
    private SharedPreferences prefs = null;
    private SharedPreferences.Editor prefsEditor = null;

    final private ValueFragment.OnValueChangedListener listener =
            new ValueFragment.OnValueChangedListener() {
                @Override
                public void onValueChanged(int value) {
                    updateColor();
                }
            };

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        textViewColor = findViewById(R.id.textViewColor);

        FragmentManager fragmentManager = getFragmentManager();
        fragmentRed = (ValueFragment)
                fragmentManager.findFragmentById(R.id.fragmentRed);
        fragmentRed.setLabel(getString(R.string.label_red));
        fragmentRed.setMaxValue(MAX_COLOR_VALUE);
        fragmentRed.setListener(listener);
        fragmentGreen = (ValueFragment)
                fragmentManager.findFragmentById(R.id.fragmentGreen);
        fragmentGreen.setLabel(getString(R.string.label_green));
        fragmentGreen.setMaxValue(MAX_COLOR_VALUE);
        fragmentGreen.setListener(listener);
        fragmentBlue = (ValueFragment)
                fragmentManager.findFragmentById(R.id.fragmentBlue);
        fragmentBlue.setLabel(getString(R.string.label_blue));
        fragmentBlue.setMaxValue(MAX_COLOR_VALUE);
        fragmentBlue.setListener(listener);

        prefs = getSharedPreferences(getString(R.string.app_name), MODE_PRIVATE);
        prefsEditor = prefs.edit();
    }

    private void updateColor() {
        int red = fragmentRed.getValue();
        int green = fragmentGreen.getValue();
        int blue = fragmentBlue.getValue();

        textViewColor.setBackgroundColor(Color.rgb(red, green, blue));

        prefsEditor.putInt(getString(R.string.preference_red), red);
        prefsEditor.putInt(getString(R.string.preference_green), green);
        prefsEditor.putInt(getString(R.string.preference_blue), blue);
    }

    @Override
    protected void onResume() {
        super.onResume();

        int red = prefs.getInt(getString(R.string.preference_red), MIN_COLOR_VALUE);
        int green = prefs.getInt(getString(R.string.preference_green), MIN_COLOR_VALUE);
        int blue = prefs.getInt(getString(R.string.preference_blue), MIN_COLOR_VALUE);

        fragmentRed.setValue(red);
        fragmentGreen.setValue(green);
        fragmentBlue.setValue(blue);

        updateColor();
    }

    @Override
    protected void onPause() {
        super.onPause();

        prefsEditor.commit();
    }
}
