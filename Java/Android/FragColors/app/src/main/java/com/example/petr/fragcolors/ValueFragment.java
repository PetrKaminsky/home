package com.example.petr.fragcolors;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;
import android.widget.TextView;

public class ValueFragment extends Fragment {

    private int value;
    private OnValueChangedListener listener = null;
    private TextView textViewLabel = null;
    private SeekBar seekBarValue = null;
    private TextView textViewValue = null;

    public int getValue() {
        return value;
    }

    public void setValue(int v) {
        if (value != v) {
            value = v;
            if (listener != null)
                listener.onValueChanged(v);
            seekBarValue.setProgress(v);
            updateTextValue();
        }
    }

    public void setMaxValue(int v) {
        seekBarValue.setMax(v);
    }

    public void setLabel(String l) {
        textViewLabel.setText(l);
    }

    public void setListener(OnValueChangedListener l) {
        listener = l;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_value, container, false);
        textViewLabel = view.findViewById(R.id.textViewLabel);
        seekBarValue = view.findViewById(R.id.seekBarValue);
        seekBarValue.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                value = progress;
                if (listener != null)
                    listener.onValueChanged(progress);
                updateTextValue();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
        textViewValue = view.findViewById(R.id.textViewValue);
        updateTextValue();
        return view;
    }

    private void updateTextValue() {
        textViewValue.setText(String.valueOf(value));
    }

    public interface OnValueChangedListener {
        void onValueChanged(int value);
    }
}
