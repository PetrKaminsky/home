import sys

class Rod:
    def __init__(self, name):
        self.name = name

    def __str__(self):
        return self.name

def move_tower(height, from_rod, to_rod):
    if (height >= 1):
        if ((from_rod == Rod.a and to_rod == Rod.b) or
            (from_rod == Rod.b and to_rod == Rod.a)):
            temp_rod = Rod.c
        elif ((from_rod == Rod.a and to_rod == Rod.c) or
            (from_rod == Rod.c and to_rod == Rod.a)):
            temp_rod = Rod.b
        elif ((from_rod == Rod.b and to_rod == Rod.c) or
            (from_rod == Rod.c and to_rod == Rod.b)):
            temp_rod = Rod.a

        move_tower(height - 1, from_rod, temp_rod)

        print "(%d) %s --> %s" % (height, from_rod, to_rod)

        move_tower(height - 1, temp_rod, to_rod)
    return

towerHeight = 3

if (len(sys.argv) > 1):
    towerHeight = int(sys.argv[1])

print "Tower height: ", towerHeight

Rod.a = Rod("A")
Rod.b = Rod("B")
Rod.c = Rod("C")

move_tower(towerHeight, Rod.a, Rod.b)
